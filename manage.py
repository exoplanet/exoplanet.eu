#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
# Standard imports
import os
import sys


def main():
    """
    Modified manage.py function to be django-configuration compatible.

    See: https://django-configurations.readthedocs.io/en/stable/#quickstart
    """
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "common.settings")

    # pylint: disable=import-outside-toplevel
    # External imports
    from configurations.management import execute_from_command_line

    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
