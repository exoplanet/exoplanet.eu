---
author: Pierre-Yves Martin (LESIA)
copyright: Copyright (C) 2016-2017 ESEP LESIA, Observatoire de Paris-Meudon, LMD
credits:
    - Françoise Roques (LESIA)
    - Quentin Kral (LESIA)
    - Jean Schneider (LUTH)
    - Ulysse Chosson (LESIA)
maintainer: Pierre-Yves Martin (LESIA)
email: pierre-yves.martin@obspm.fr
---

# Exoplanet.eu CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## TODO

- handle our [matomo](Sihttps://matomo.org/) analytics via
  [django-analytics](https://github.com/jazzband/django-analytical)
- add embedded video support for django
  [django-embed-video](https://github.com/jazzband/django-embed-video)
- see if [django-smart-selects](https://github.com/jazzband/django-smart-selects) can
  be used for some of our complexe forms
- use [contrib sitemap](https://docs.djangoproject.com/en/3.0/ref/contrib/sitemaps/)
  to generate sitemap for the site
- use [django-robots](https://github.com/jazzband/django-robots) to generate
  `robots.txt` for the site
- if necessary use [django-pipeline](https://github.com/jazzband/django-pipeline) to
  speed-up access to the pages by compressing and grouping css and js dependencies
- use [django-model-utils](https://github.com/jazzband/django-model-utils) to improve
  some models (in particular in editorial app)
- see if [django-widget-tweaks](https://github.com/jazzband/django-widget-tweaks) can
  help to simplify the admin custom widgets
- add an `AUTHORS` file to the project
- use [Git Large File Storage](https://git-lfs.github.com/) to manage all large
  binary files in the repository (and migrate history too). See:
  [Git LFS for GitLab](https://docs.gitlab.com/ee/topics/git/lfs/) since it is activated
  on the [Obspm GitLab server](https://gitlab.obspm.fr).
  any django app
  <!-- WARNING DUE DATE mid may -->
- add an `irradiation` computed column to the catalog. The computation was already done
  in the `PolarPlotOutPut` but was only available to the polar plot page.
  (see line 1074).
- Add a "read only" launch mode for the site (without any admin). This is useful to make
  a simple "read-only mirror" of the site.
- XSS attack prevented on media urls: `/media/flatpages_documents/*` now we have ???
- XSS attack prevented on urls: `/polar_plot/*` now we have ???
- URGENT Remove the PADC watermarking from the plot display and plot file saved by users:
  - in histogram plot
  - in scatter plot
  - in polar plot ???
- URGENT get rid of the JS script parameter `urilogo` since it is not used anymore

## [Unreleased]

### Added

- Add informations on star parameters in catalogue filters.

### Fixed

- Fix default date for discovery date of exoplanet for the current year.

## 2.24.0

### Added

- Improve the preset/shortcut system:
  - Add a default shortcut
  - Add a shortcut for exoplanets of circumbinaries
  - Add a shortcut for exoplanets with mass between 0.1 Mmer and 13 Mjup
  - Add a shortcut for exoplanets with mass between 13 Mjup and 30 Mjup
  - Add a shortcut for exoplanets with mass above 30 Mjup
  - Add a shortcut for exoplanets with mass below 0.1 Mmer
  - Add a shortcut for planets of the Solar System
  - Add a shortcut for exoplanets of stars from post main sequence

### Removed

- Remove old shortcut "shortcut mass under 60 mjup"
- Remove old shortcut "shortcut mass under 30 mjup"

### Fixed

- Fix bug in `choices.py` preventing correct comparison if the choices element
  had an all uppercase name
- Fix crash when displaying `/bibliography/` and `/bibliography/*` pages if
  there was no publication in the DB

### Changed

- Change default shortcut from "All catalogue" to "Mass between 0.1 Mmer and 13
  Mjup"
- Change HTTP error code used in case of syntax error in the plot from 406 to
  400 and provide a readable reason for the error via the `reason` field of the
  HTTP response
- By default, only fast tests are launched by `pytest`
- Only fast tests are auto launched at each commit via pre-commit
- Fast and slow tests are auto launched at each push via pre-commit

## 2.23.0

### Fixed

- Fix filename of csv, dat and votable export.
- Fix csv and dat export on error.
- Fix missing parameters in planet detail page. `publication status`, `angular distance`
  , `star magnitude I`, `star magnitude J`, `star magnitude H` and `star magnitude K`
  are displayed in planet detail page now.
- Fix date validation for publication in admin.
- Fix displaying 0 when a parameter is equal to `0` and not `—` (like the value of this
  parameter is equal to `None`)
- Fix Age label in star admin.
- Fix `createFilter()` function to have a good behaviour with detection type filtering
  in catalogue and plots.
- Fix error in calculation of the mass if we only have a mass sin i and
  inclination for a planet (both in python and SQL)

## 2.22.0

### Changed

- Change "default" to "other" in legend of detection method color for scatter plot.

## 2.21.0

### Added

- Add color for detection method in scatter plot.
- Add an "Apply Filter" button in plots to applying filters.
- Add a planets index page usefull to debug.

## Fixed

- Fix filter in plot with Detection type.

## 2.20.1

### Fixed

- Fix admin to don't add planet when `detection_type` is wrong.

## 2.20.0

### Added

- new shortcut urls defined in `catalog_legacy/views/shortcuts_def.py` added to
  have direct access to specific selection in the catalog:
  - `/catalog/shortcuts/freefloating/`
  - `/catalog/shortcuts/mass_under_60_mjup/`
  - `/catalog/shortcuts/mass_under_30_mjup/`
- new UI in the catalog page to access the shortcuts

### Fixed

- access to the catalog json from by datatable now use full path (this allow
  clean redirection to the catalog)
- move update date higher (close to discovery date) in the detail page

### Changed

- rename `radius_detection_type` to `radius_measurement_type` in code, database, views and
  all descriptions or visible text
- rename `mass_detection_type` to `mass_measurement_type` in code, database, views and
  all descriptions or visible text
- update description on home page for a more exhaustive one

## 2.19.1

### Fixed

- replace the sample .env that had not been updated after the changes in the .env

## 2.19.0

### Fixed

- bibliography search by date is now functional (it used to weirdly mixup creation and
  modification date)
- better handling of the creation and modification dates of publication that can be
  unset

### Changed

- simplification of the `.env` file needed for the project

## 2.18.0

### Added

- new illustration for the meeting page jumbotron

### Changed

- improve lisibility of the text/title over the jumbotron by adding a wide centered
  black text shadow

### Fixed

- Bibliography pagination works correctly (it used to display correctly only the first
  page)
- Bibliography section switch now preserve current search
- alternate names of planets are now deleted if the planet is deleted
- alternate names of stars are now deleted if the planet is deleted

## 2.17.0

### Added

- All the site can now display MathML content (independently of the internet browser
  used)

### Fixed

- All value error displayed in the catalog and in detail page are now displayed
  (previously only the one of unitless values were displayed)

## 2.16.1

### Added

- add homepage image header

## 2.16.0

### Added

- Add measured temperature error field to planet. Only accessible for the admin at the
  moment.
- Add log entries in admin for super user.

## 2.15.2

### Fixed

- [BUG #132](https://gitlab.obspm.fr/exoplanet/exoplanet.eu/-/issues/132) where the polar
  no longer displayed because the related model generated in the output are not the
  good one.

## 2.15.1

### Fixed

- [BUG #121](https://gitlab.obspm.fr/exoplanet/exoplanet.eu/-/issues/121) where some
  floating point error on certain radec position could trigger an error 500 on the
  server due to an acosd() applied out of range inside the similarity check that is
  executed each time we save a star.

## 2.15.0

### Changed

- Change limit of field send from 2000 (default) to 5000 to prevent some admin page from
  sending blocking error message because we have a lot of publications/molecules.

### Fixed

- Add year 2024 to the possible years for the discovery year of planets.
- CSV and VO download are taking again into account the filtering applied to the
  catalog.

## 2.14.0

### Fixed

- Fix RA and DEC input from admin were not cleaned and checked properly.
- Fix DEC seconds were not displayed as a float.

## 2.13.1

### Fixed

- Fix RA and DEC input from admin were not cleaned and checked properly
- Fix DEC seconds were not displayed as a float

## 2.13.0

### Added

- Add (or more precisely restore) ability to download the catalog in VOTable format
- Add verification on alternate name with name on `AlternatePlanetName` model.
  It was missing 👉️ **it's a bug**
- Add verification on name with alternate name on `Planet` model and admin.
  It was missing 👉️ **it's a bug**
- Add verification on alternate name with name on `AlternateStarName` model.
  It was missing 👉️ **it's a bug**
- Add verification on name with alternate name on `Star` model and admin.
  It was missing 👉️ **it's a bug**
- Add verification on alternate name with name on Alternate Star Name model.
- Add verification on name with alternate name on Star model and admin.

### Changed

- Change limit of field send from 1000 (default) to 2000.

### Fixed

- Fix convertion of RA and DEc in star and planetary system admin.
- Fix dragging alternate name. Remove alternate star or planet name when we remove his
  foreign key to an object.
- Fix verification of star in admin to make the verification when we modify a star obj.
- Fix verification of positions on star with the correct formula.

## 2.12.0

### Changed

- Documentation for the installation of the project has been updated and is now
  automatized via bash scripts. See: `INSTALL.md` and the `install` directory.

## 2.11.3

### Added

- Add `django-jsonform` for better admin widget for detection method.
- Add kinematic detection method
- Add tool for tinyMCE:
  - strikethrough
  - backcolor
  - forecolor
  - superscript
  - subscript

### Changed

- Reverse version 2.11.1. The DIO change apache config and the website is in good
  config now: Prod.

### Fixed

- Fix `from` `action` of the catalog filter form. When we have a section in the url and
  we apply a filter. The page no longer takes us to this section but to the top of the
  catalogue

## 2.11.2

### Fixed

- Fix search on publication date in bibliography.
- Fix change of scale unit in polar and histogram plots.
- Fix size of plot when the screen is very small.

## 2.11.1

Temporary hotfix

### Fixed

- Fix dev headband displayed on the prod website.
  It's because the website is in dev configuration for the moment because we must wait
  for DIO to add the alias for the statics before passing the config to prod.

## 2.11.0

### Added

- Add update date in search date of bibliography form.
- Add local headband when we are in local configuration.
- Add dev headband when we are in dev configuration.

### Changed

- Change "last_update" badge in bliblography.
  If `publication.modifed` is equal to `publication.created` we display "creation date"
  instead "last_update"
- Change form of bibliography to take days, months and years separately.
- Alert headband go from alert warning to info level.
- Change date on bibliography, display date of publication and modified date.
  Can be sort by this 2 dates.

### Fixed

- We not longer display hidden publications, news, links, meetings and searches.

## 2.10.3

### Added

- Add a Not implemented page for VOTable downloading.

## 2.10.2

### Fixed

- Re install file-saver for plots.

## 2.10.1

### Added

- Add EPN view.
- Add [Matomo](https://matomo.org) tracking code to all pages.

## 2.10.0

### Added

- Add new comming soon tool: Exodam public API.
- Add `type` parameter in overview of publications admin.
- Add redirection for old planet details url to the new url.

### Changed

- Change look of message if we dont have:
  - mettings
  - links
  - bibliography
  - news
  - researches

### Fixed

- Fix addition of the Planet2Star link when we modified a planet with a main star.
- Fix displaying of news in home page if we dont have news.

### Removed

- EQ example in queries example of micro language.

## 2.9.6

### Removed

- Remove beta headband in all page. Use it only on beta page.

## 2.9.5

### Fixed

- Fix `Last update` for Researches. Typo in view meant that the last update was never
  searched for Researches.

## 2.9.4

### Fixed

- Fix `Last update` for polar plot link card in homepage.
  Now the last update is the max between the last planet table update and the last star
  table update.

## 2.9.3

### Removed

- Remove `Translation` part of the flatepage `exoplanet's team`.
  You can find the remove text in the file; `doc/remove_translation_from_exoplanet_team_page.md`

## 2.9.2

### Changed

- Change package to manage file, remove `django-filebrowser` and add `django-filer`.

## 2.9.1

### Fixed

- Fix path of media for the uploader.

### Removed

- Remove `Save Figure` button on observability predictor.
  Saving figure does not work for the moment.

## 2.9.0

### Added

- Add RGPD info to legal notice page
- Add the `Observability predicator` page (view, output and template).

### Fixed

- Fix uploader for image to be used in flatepage.
- Fix `Observability predicator` radial velocity scale.
- Fix `Observability predicator` slug in `GET` method.
- Fix scale of the polar plot.
- Fix postion, size and resize of polar plot.
- Fix color and box of Pan/Zoom on scatter and histogram plots.
- Fix `Save Figure` button on scatter and histogram plots.
- Fix `log scale` in polar plot, now you can deactivate or reactivate the log scale.
- Fix count of planets in description of the home page.

### Changed

- Change jumbotron image for a blue background color time to get images from the designer.
- Change the design of the research page for the new design.
- Change size of the text in the jumbotron.
- Change the default language to english (gb). The enable traduction (none for the moment)
are enable link, other are disable in the checkdown drop box menu.
- Change number of display element in planet catalog.
From `10, 100, 1000, All` to `10, 50, 100, 500, 1000`. `All` return a Http 400 error.
- Change, translate in french the `financements` part of `legal notice`.
- Change, add margin in the planets catalog table.
- Change, hide `tutorial` and `theory` flatpages.
- Change the title of the website for: `Encyclopaedia of exoplanetary systems`.

### Removed

- Remove `Vie privée` part of `Legal notice`.
- Remove `processing` bar on planet catalog.
- Remove `tutorial` and `theory` flatpages form the home page.

## 2.8.0-beta

### Added

- new tools page
- new amd stability listing page with direct access to all AMD enabled systems

### Changed

- amd stability page has new design
- amd stability tool now use the new slugified urls

### Removed

- Remove amd stability flatpage in favor of the new amd stability listing page

## 2.7.2

### Fixed

- Fix, now the catalog is sortable by planet name.

## 2.7.1-beta

### Fixed

- Fix number of planet in footer. Add filter to take only WebStatus active planets.

## 2.7.0-beta

### Changed

- Move some css config from plot scss to generic scss file to prevent some
  inconsistencies
- Change, add a raw_id_field for star in planet2star.
A popup windows will be open to add a star.
- Change descsription of plots.
- Change the design of polar plot for the new design.
- Change the description of home.

### Fixed

- Fix catalog could not be opened from the plot if the selection mode was not activated
- Fix planet without a proper distance were not shown anymore in polar plot
- Fix typo in polar plot "distance no defined" --> "distance not defined"
- Improve usability of the whole plot UI
  - Bootstrapize the plot menu
  - Bootstrapize the plot filter bar
  - fix some long standing UI bugs and papercuts
- Fix click event on circle on polar plot to be able to clic on a circle to access to
  the persoanl page of the planet.
- Fix scale of polar plot. I don't know why the scale was cumulative with all parameters.
- Fix scatter plot is the default plot like in the legacy site
- Fix scatter and histogram plots with a new html disposition and a **real** size adaptation.
- Fix menu of scatter and histogram plots
- Fix the `planetarysystem` admin.
- Fix verification of planetary system **OR** stars in planet admin.
- Fix color and size buttons on scatter plot.
- Fix, remove restriction on omega and lambda in planet admin.
- Fix flatepage edition, tinymce was missing in INSTALL_APPS in `settings.py`.
- Fix, Card header system detail  self close when we click 2 time on it.
- Fix adding planet in admin and link with star. Need to restore verification of
planetary system **OR** star. See issue #37
- Fix polar plot, it is displayed. There are still some bugs, see the gitlab
- Fix template content of the polar plot.
- Fix help question mark in planet catalog for syntax.
- Fix design of status, detection and filter button and input in planet catalog.
- Fix size of scatter plot and scatter plot location.
- Fix link of histogram and scatter plot in plot page.
- Fix `contact us` link in footer, It is temporary before we have a `contact us` page.
- Fix link of FAQ. This is temporary before we have an FAQ flatepage.
- Fix size of histogram plot and histogram plot location.

### Removed

- Remove usage of html entities in `polar.js` and `polar_plot.html` since they should
  not be used anymore according to internet standards.
- Remove `Swap interface` on histrogram and scatter plot.
- Remove `Exoris Simulation` on scatter plot.

## 2.6.0-beta

### Added

- Add disk part of the website. See [Disk APP changelog](#disk-app).

## 2.5.0-beta

### Changed

- new design for link page

## 2.4.0-beta

### Added

- dedicated news page and individual news page

### Changed

- new design for meeting page
- ability to see which meeting are happening right now in the meeting view
- pagination added to the meeting page
- updated info in the legal page

## 2.3.0-beta

### Added

- use of [django-crispy-forms](https://django-crispy-forms.readthedocs.io/en/latest/index.html)
  to handle the bootstrap rendering of django forms
- new design for:
  - home page
  - catalog page
  - plots page (except for polar plot)
  - planet detail page (with single or multiple stars and without stars)
  - legal info page
  - bibliography page
- use [Bootstrap Icons](https://icons.getbootstrap.com/) instead of individual png image
  for all icons
- Add European Union as a new sponsor to the site
- Add tool to check size of errors array fields.
- Foreign key to table import in models of `planet`, `star`, `planetarysystem` and
`publication`.
- sample apache config file to use for `mod_wsgi` deployment
- licence is fixed to [EUPL 1.2](https://eupl.eu)
- use of [poetry](https://python-poetry.org) for virtual env and installation
- documentation on what to do when the pre-commit test `makemigrations_check` fails, the
  documentation is directly in the file of the local hook
- `exobdd` tool to export database content in JSON format from the old Django1 version
  to new Django3 version
- HTML ids for the columns of the legacy research page to improve testability
- all database modifications (functions, trigger, view creation) are now located
  directly in the migrations directory of the relevant django app. This prevent from
  having SQL files placed in random directories all over the project and link them to
  the database evolution (in django term migration they refer to).
- all javascript and CSS ressources are real django static ressources (for the new
  design only)
- provide all favicons (PNG, SVG and ICO) at the root of the website (most browser and
  social media want them here)
- per application static file support
- `canary` application to easily test if the basic site functionality are ok
- `README.md` file to describe the project
- `CHANGELOG.md` file to describe the changes in the project
- [pre-commit](https://pre-commit.com/) support for the project with multiple plugins
- test via [pytest](https://docs.pytest.org/en/latest/) for creation/access for all
  models
- `doc` directory to store the documentation
- `common` directory that store all the common-to-all-app config
- `test` directory to store all the tests
- many [doctest](https://docs.python.org/3.6/library/doctest.html) to the function
  helpers all across the code
- support for web dependencies (css, jss...) using
  [npm dependency manager](https://www.npmjs.com/). This prevent us from integrating
  whole external libraries in our code tree.
- Add support for auto-compiling and link generation in templates for sass/scss files
  via [django_compressor](https://django-compressor.readthedocs.io/en/stable/) and
  [django-libsass](https://github.com/torchbox/django-libsass). It has been possible
  due to the very good doc
  [How to use SCSS/SASS in your Django project (Python Way)](https://www.accordbox.com/blog/how-use-scss-sass-your-django-project-python-way/)
- move `base.html` template and related resources to `exoplanet` directory
- add a `DEPLOY` file to the project
- add a `ressources` dir to store the resources used to generate actual graphic files
  used on the site. The final graphic files are located in `common/static/generated_img`
  and a python script located in `tools` dir is used to export said resources if the
  original svg are modified
- add generic xss prevention tests generated from the XSS attack urls provided by
  <pdelteil@protonmail.com> (see "Fixed" section)

### Changed

- Rename `_is_visible()` function in models into `is_visible()`.
- Object urls have been refactored to handle all possible conflict and edge case:
  - the site does not rely anymore on absolute url generation (which was unreliable and
    buggy)
  - planet and star slugs (url and json safe version of the planet/star name) are now
    generated in a standard and safe way
  - objects url are now using the pattern : `{object_slug}--{db_id}` where `object_slug`
    is a safe version of the name of the object where all non alphanumeric ascii char are
    replaced by their closest alpha numeric equivalent and special chars and spaces by `_`
    and `db_id` is the unique database id of the object
- AMD page is displayed even if the AMD server is unreachable displaying a clean error
  message instead of a Django error page
- url slugs of the planets are more reliable and generic: they now recognise correctly
  utf-8 characters (some are present in the planet names)
- diagrams page has been renamed plots page
- legacy pages using the new design (but with no new functionalities):
  - home page
  - plots page
- all deployment configuration values and especially security sensitive ones (password,
  allowed host) have been moved out of `common/settings.py` and are now settable via
  environment variables and/or via `.env` file located at the root of the project. There
  is a sample `SAMPLE.env` for reference.
- `_get_ra_sex` and `_get_dec_sex` methods/property in the `System` model are know named
  `ra_sex` and `dec_sex` and are in the abstract base class `WithCoordinateModel`
- some `admin` (now `core.admin`) functions have been renamed and made private:
  - `restrictAngle` ➡️ `_restrict_angle`
  - `check_strictly_positive_error_min` ➡️ `_check_strictly_positive_error_min`
  - `convert_dec` ➡️ `_convert_dec`
  - `convert_ra` ➡️ `_convert_ra`
- `tools/trigo.py` function `exist_star_at_coord` is replaced by the equivalent
  `core.star.all_stars_at_coord` function. Warning: the signature has one less param,
  it just doesn't have the first `star` param
  that was in fact not needed by `exist_star_at_coord`)
- python version targeted is now [Python 3.10](https://www.python.org/downloads/release/python-3100/)
- all ids now default to `django.db.models.AutoField` to prevent warning in Django 3+
  and an error in Django 4+
- `NullBooleanField` are all replaced by `BooleanField(null=True, ...)` to prevent
  warning in Django 3+ and an error in Django 4+
- the previously integrated `samp.js` script is now a **submodule** of the project
  located in `common`.
- the new version of the catalog (still work in progress!) is now located under the
  `/catalog_new/` url in order to prevent collision with legacy catalog under the
  `/catalog/` url that is already quite crowded.
- Move all definition of fields and field lists from `catalog_field_def.py` to a
  dedicated YAML file using a `globals().update(...)` trick to add the results to module
  and make the change invisible
- Replace use of deprecated `\xhh` and `\xhh\xhh` escape sequences by a simple usage of
  UTF-8 chars
- Change django template file name to fit the name of corresponding view:
  - `catalog_legacy/templates/object_list.html` ➡️
    `catalog_legacy/templates/catalog_html.html`
- Change python file names to increase the coherence of the naming:
  - `app/model_fields.py` ➡️ `core/catalog_field_def.py`
  - `app/output.py` ➡️ `core/outputs/*.py`
  - `app/templatetags/*` ➡️ `core/templatetags/*`
- Change misleading class and function names :
  - `app.model_field.Field` ➡️ `core.catalog_field_def.Column`
  - `app.model_field.get_fields_list_by_id` ➡️ `core.catalog_field_def.get_columns_by_id`
- HTML forms now use django forms as much as possible (previously we handled HTML forms
  directly via `request.GET` access which was error prone, cumbersome and is really an
  antipattern)
- legacy catalog now use scss instead of css for styling (this allow us to have a clean
  handling of the static resources used in the stylesheets). This is cleanly compiled
  via `django-libsass`
- legacy catalog use clean `static` machinery to handle all it's images, stylesheets,
  etc
- legacy catalog use the `npm` infrastructure to handle all of its (sometimes outdated...)
  dependencies
- change location for all legacy website resources from `/media/` to standardised
  static dir `/static/catalog_legacy/` in order to have a clean Django deployment
- switch from dirty hack to clean state of the art static file support
- switch from SVN to GIT
- split the site in multiple specialized apps
  - `amd` app contains all the computation for the AMD stability for some planets
  - `canary` app is just here to test if the site works
  - `catalog` app contains all the views and code related to the catalog table
    representation
  - `catalog_legacy` app contains all the views and code related to the old catalog and
    web site
  - `core` app contains all the scientific models and helper code
  - `editorial` app contains all the site publication models and views
  - `ephemeris` app contains all the computation for rendering an ephemeris page for
    each planet
- change class names of some models to reduce ambiguity :
  - `PlanetPublication` ➡️ `Planet2Publication`
  - `StarPublication` ➡️ `Star2Publication`
- change database model names to reflect the new apps names:
  <!-- cSpell:disable -->
  - `app_atmosphere_temperature` ➡️ `core_atmospheretemperature`
  - `app_alternate_planet_name` ➡️ `core_alternateplanetname`
  - `app_atmosphere_molecule` ➡️ `core_atmospheremolecule`
  - `app_molecule` ➡️ `core_molecule`
  - `app_planet` ➡️ `core_planet`
  - `app_planetstar` ➡️ `core_planet2star`
  - `app_planetary_system` ➡️ `core_planetarysystem`
  - `app_star` ➡️ `core_star`
  - `app_alternate_star_name` ➡️ `core_alternatestarname`
  - `app_starpublication` ➡️ `core_star2publication`
  - `app_publication` ➡️ `core_publication`
  - `app_planetpublication` ➡️ `core_planet2publication`
  - `app_link` ➡️ `editorial_link`
  - `app_meeting` ➡️ `editorial_meeting`
  - `app_news` ➡️ `editorial_news`
  - `app_news` ➡️ `editorial_news`
  - `app_research` ➡️ `editorial_research`
  - `planetMaterializedView` ➡️ `core_planetdbview`
  <!-- cSpell:enable -->
- change some database row names to correct inconsistencies:
  - `app_planet.main_star` ➡️ `core_planet.main_star_id`
- rename `exoplanet` folder to `common` to reduce coupling
- use a `Choice` class/enum for each "multiple choices" field instead of list of tuple
- replace all python2 specific syntax with idiomatic python3 syntax
- replace all direct `property()` call with a clean `@property` decorator
- use smarter class inheritance to factorize models in `core` app
- use a full module with dedicated python files in it to store the models instead of a
  thousands lines long models.py file
- use a full module with dedicated python files in it to store the views instead of a
  thousands lines long models.py file
- replace all Django 1.11 specificities by Django 3.0 idiomatic code
- split `requirements.txt` in two files: `requirements.txt` for dependencies of the
  project itself and `requirements_dev.txt` for dependencies needed only to develop
  the project
- replace all `__unicode__` with `__str__` since python3 str **is** unicode
- all code now use [black](https://github.com/psf/black) and line length is enforced at
  88 char
- replace `astrolib` with `astropy.coordinates` (as asked by `astrolib` dev on their
  web site)

### Deprecated

- usage of tables `*_flatpage_update`

### Removed

- the old `bibliography_books_theses.html` and `bibliography_reports.html` templates
  since the `bibliography.html` template now cover all cases
- the old `app_planet` had a column `detect_mode` that was used a long time ago to store
  the detection method used for a given planet. This has been long replaced by the
  `detection_type` column. It was not even mapped in the pre-Django3 planet model. We
  removed this unused column from the new `core_planet` table
- the dependency on the `exo_user` database user has been removed and now all reference
  to the database user are done via the `.env` configured database user
- some functions and classes in `admin.py` were not used anywhere and thus have been
  removed:
  - `getErrorUnicodeArray`
  - `check_strictly_positive`
  - `check_error_with_nan`
  - `cast_string_to_float`
  - `ExtendedFlatPageForm`
- `PlanetAdmin.get_admin_mass_sini` function that is used nowhere and was using so many
  undefined variables and functions
- email notification capabilities (they were not used) and configuration:
  - remove `EMAIL_HOST` from `settings.py`
  - remove `EMAIL_PORT` from `settings.py`
  - remove `EMAIL_HOST_USER` from `settings.py`
  - remove `EMAIL_HOST_PASSWORD` from `settings.py`
  - remove `SUGGEST_NOTIFICATIONS_TO` from `settings.py`
  - remove `send_mail` function from `admin.py`
- the `imports` and `pubimports` directories that are just old import code that won't
  ever be reused
- `paginator`, `search_qs` and `update` params in the context for the legacy
  bibliography view (they were not useful anymore)
<!-- cSpell:disable -->
- legacy tables inherited from the `PHP` to `Django` migration: links, lang, mol,
  news, mol_planet, nodefvalue, planet, planettype, publication, publiplanet,
  planetstatus, publirevue, research, tzerounit, star
<!-- cSpell:enable -->
- some never used fields in the models TODO precise which ones
- all unused imports

### Fixed

- Fix `is_visible()` function of planet model (`is` **NOT EQUAL AT** `==`).
- Fix menu of scatter plot:
  - X axis field selection.
  - X axis log scale.
  - Y axis field selection.
  - Y axis log scale.
  - Set labels.
  - Selection mode.
  - Planet Table button.
- Fix decode url in `DiagramTools.js`
- Fix menu of histogram plot:
  - X axis field selection.
  - X axis log scale.
  - Y axis field selection.
  - Y axis log scale.
  - Bars colors selection and displaying.
- Fix a bug on compressed Exoris compress js.
- Fix some errors in `scatter.js` and `histogram.js`
- Fix a bug with `float('inf')`, `float('-inf')` and `float('Nan')` in function
  `__get_planet_values_pack()` in plots output model.
- Fix column name in db (not in models) in model `PlanetAbstractModel`:
  - `K` become `k`
  - `K_error` become `k_error`
- Fix some mypy issue (import error). 313 solved issues.
- Fix amdf error in personal page of planet when they have a amdf link.
**When amdf will be integrated,you will have to remove the fix** in `/catalog_legacy/templates/planet_with_star_detail.html`
- XSS attack prevented on urls: `/catalog/*` now we have a regex to only match planet names
- XSS attack prevented on bibliography urls now we accept only `books`,
  `reports`, `theses` and `all` as possible child for `\bibliography\` url:
  - `/bibliography/books/*`
  - `/bibliography/reports/*`
  - `/bibliography/theses/*`
  - `/bibliography/all/*`
- XSS attack prevented on urls: `/meetings/*` (Django3 update was enough to prevent this)
- XSS attack prevented on urls: `/plots/*` (Django3 update was enough to prevent this)
  previously this was `/diagrams/*`
- XSS attack prevented on urls: `/legal_notice/*` (Django3 update was enough to prevent this)
- XSS attack prevented on urls: `/new_catalog/*` (Django3 update was enough to prevent this)
- XSS attack prevented on urls: `/news/rss/*` (Django3 update was enough to prevent this)
- XSS attack prevented on the root urls:
  - XSS attack prevented on urls: `/./*`
  - XSS attack prevented on urls: `/*`
- XSS attack prevented on flatpage urls:
  - `/planets_binary/*`
  - `/planets_binary_notes/*`
  - `/query_language/*`
  - `/readme/*`
  - `/research/*`
  - `/sites/*`
  - `/team/*`
  - `/theory/*`
  - `/tutorials/*`
  - `/vo/*`

### Security

- removed all sensible informations (password, secret keys...) from the `settings.py`
- replace hackish import based local configuration with
  [django-configuration](https://django-configurations.readthedocs.io/en/stable/) that
  allow us to provide security sensitive information via environment variables

## Disk App

### Unreleased

### [0.17.1] - 10-07-2023

#### Fixed

- Fix the displaying of list in individual page (alternate object names).

### [0.17.0] - 10-07-2023

#### Changed

- We no longer display internal card in individual pages.

### [0.16.0] - 10-07-2023

#### Added

- Add gravitational systems pages.

### [0.15.0] - 06-07-2023

#### Changed

- Change how to link disk and star, now they are linked by a gravitational system.

### [0.14.0] - 27-06-2023

#### Added

- Add stars in indiviudal disk pages.

### [0.13.0] - 23-06-2023

#### Changed

- Change individual disk page to corresponding to the new design of planet individual pages.

### [0.12.0] - 20-06-2023

#### Added

- Add log request on the disk catalog and disk individual pages.
- Add logging page.

### [0.11.2] - 16-06-2023

#### Fixed

- Fix the host and the port of the postrest API server.

### [0.11.1] - 14-06-2023

#### Fixed

- Fix the displaying of information of indentify in disk catalog.

### [0.11.0] - 14-06-2023

#### Added

- Add way to order the catalog.

### [0.10.1] - 14-06-2023

#### Fixed

- Fix the disk catalog and the view for the new format of data.

### [0.10.0] - 14-06-2023

#### Added

- Add real description in catalog and individual disk page.

### [0.9.2] - 14-06-2023

#### Fixed

- Fix condition to not display unit in detail opf individual page if unit or
definition is "".

### [0.9.1] - 13-06-2023

#### Fixed

- Fix javascript to adapt it with the new exodict format (the real format).

### [0.9.0] - 13-06-2023

#### Added

- Add management of headers with the list of headers in db, to display just the disk
headers in disk catalog.
- Add catalog headers tables in admin.
- Add catalog headers models and migration.

### [0.8.0] - 12-06-2023

#### Added

- Add disk, star and exodict table in admin.

### [0.7.1] - 09-06-2023

#### Fixed

- Fix unit displaying of None unit in disk individual pages.

### [0.7.0] - 08-06-2023

#### Added

- Add unit in the disk catalog if exist.

### [0.6.0] - 08-06-2023

#### Added

- Add management of nesting exodcit in the js for the disk catalog displaying.

### [0.5.2] - 02-06-2023

#### Fix

- Fix migrations for the disk app.

### [0.5.1] - 02-06-2023

#### Fix

- Fix request and js for disk catalog to display all data.

### [0.5.0] - 01-06-2023

#### Added

- Add a [DataTables](https://datatables.net/manual/) for the disk catalog

### [0.4.0] - 24-05-2023

#### Changed

- Modified individual of disk app to support new exodict version.

### [0.3.0] - 19-05-2023

#### Added

- Add individual page of disk in the new disk app.

### [0.2.0] - 09-05-2023

#### Added

- Add catalog page of the new disk app.

### [0.1.0] - 04-05-2023

#### Added

- Add a new app `disk`.
