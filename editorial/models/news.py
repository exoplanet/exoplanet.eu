"""New to be posted on the web site."""

# Django 💩 imports
from django.db.models import DateField, SmallIntegerField, TextField
from django_stubs_ext.db.models import TypedModelMeta

# First party imports
from core.models import TrackedModel, WebVisibleModel

# Local imports
from .choices import NewsLanguage


class News(WebVisibleModel, TrackedModel):
    """
    Model representing an handwriten news.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "editorial"

        verbose_name_plural = "News"

    date: DateField = DateField()

    # TODO replace with a RichTextField
    # See: https://pypi.org/project/django-richtextfield/
    content: TextField = TextField()

    lang: SmallIntegerField = SmallIntegerField(
        choices=NewsLanguage.choices,
        help_text="Language",
        default=NewsLanguage.EN,
    )
