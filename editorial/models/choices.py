"""All choices class needed by the editorial models."""

# Django 💩 imports
from django.db import models

# TODO use the exochoices lib instead


class NewsLanguage(models.IntegerChoices):
    """All possible languages to publish a news."""

    EN = 1, "en"
    FR = 2, "fr"
    ES = 3, "es"
    PT = 4, "pt"
    DE = 5, "de"
    PL = 6, "pl"
    IT = 7, "it"
    FA = 8, "fa"
    AR = 9, "ar"
    RU = 10, "ru"
    JA = 11, "ja"


class ResearchType(models.IntegerChoices):
    """All possible research type for the Research model."""

    GROUND = 1, "Ground"
    SPACE = 2, "Space"
