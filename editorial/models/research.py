"""Research programs about exoplanets."""

# Django 💩 imports
from django.db.models import SmallIntegerField, TextField
from django_stubs_ext.db.models import TypedModelMeta

# First party imports
from core.models import TrackedModel, WebVisibleModel

# Local imports
from .choices import ResearchType


class Research(WebVisibleModel, TrackedModel):
    """
    Model representing research program of some sort about exoplanets.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "editorial"

        verbose_name_plural = "Searches"

    # TODO rename this field since it conflict with a python builtin
    type: SmallIntegerField = SmallIntegerField(choices=ResearchType.choices)

    # TODO replace with a RichTextField
    # See: https://pypi.org/project/django-richtextfield/
    # WARNING this field contain awful raw HTML stuff
    text: TextField = TextField()

    # TODO add a start date maybe if revelent?
