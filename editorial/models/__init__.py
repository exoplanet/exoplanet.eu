# We need to import each model here in order for them to be well detected during
# django makemigrations and migrate command
# Local imports
from .choices import NewsLanguage  # noqa: F401
from .link import Link  # noqa: F401
from .meeting import Meeting  # noqa: F401
from .news import News  # noqa: F401
from .research import Research  # noqa: F401
