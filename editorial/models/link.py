"""Link to a web site about exoplanet."""

# Django 💩 imports
from django.db.models import TextField
from django_stubs_ext.db.models import TypedModelMeta

# First party imports
from core.models import TrackedModel, WebVisibleModel


class Link(WebVisibleModel, TrackedModel):
    """
    Model representing a web link about exoplanets.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "editorial"

    # TODO rename this to href
    # TODO make this an URLField for security reasons
    # WARNING this contains horrible things: html links formated with HTML <a>
    value: TextField = TextField()

    # TODO add a name field
    # TODO add a notes RichTextField to add some relevent info to the link
    # TODO mass convert all old crappy link content to new href/name/notes
