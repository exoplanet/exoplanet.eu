"""Model representing a meeting about exoplanets."""

# Django 💩 imports
from django.db.models import DateField, TextField
from django_stubs_ext.db.models import TypedModelMeta

# First party imports
from core.models import TrackedModel, WebVisibleModel


class Meeting(WebVisibleModel, TrackedModel):
    """
    Model representing a meeting about exoplanets.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "editorial"

    begin: DateField = DateField(null=True, blank=True)
    end: DateField = DateField(null=True, blank=True)
    title: TextField = TextField()
    place: TextField = TextField(null=True, blank=True)
