"""
Configuration of the django admin pages of the models.

There is also the admin pages and config for the flatpages.
"""

# Standard imports
from typing import Any

# Django 💩 imports
from django import forms
from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage

# External imports
from tinymce.widgets import TinyMCE

# First party imports
from core.admin import ExoplanetModelAdmin

# Local imports
from .models import Link, Meeting, News, Research

# Forms and widgets ###########################################################


# TODO add typing
# TODO add docstring
class TinyMCEWidget(TinyMCE):
    def use_required_attribute(self, *args: Any) -> bool:
        return False


# TODO add typing
# TODO add docstring
class PageForm(FlatpageForm):
    class Meta:
        model = FlatPage
        fields = "__all__"

    content = forms.CharField(
        widget=TinyMCEWidget(attrs=dict(required=False, cols=30, rows=10))
    )


# Admin pages themselves ######################################################


# TODO add typing
# TODO add docstring
class LinkAdmin(ExoplanetModelAdmin):
    list_display = ("id", "value", "modified", "is_visible")
    list_filter = ("status",)
    search_fields = ["value"]
    date_hierarchy = "modified"


# TODO add typing
# TODO add docstring
class MeetingAdmin(ExoplanetModelAdmin):
    list_display = ("id", "title", "place", "begin", "end", "modified", "is_visible")
    list_filter = ("status",)
    search_fields = ["title", "place"]
    date_hierarchy = "begin"


# TODO add typing
# TODO add docstring
class NewsAdmin(ExoplanetModelAdmin):
    list_display = ("id", "content", "lang", "created", "is_visible")
    list_filter = ("status", "lang")
    search_fields = ["content"]
    date_hierarchy = "created"


# TODO add typing
# TODO add docstring
class ResearchAdmin(ExoplanetModelAdmin):
    list_display = ("id", "text", "modified", "type", "is_visible")
    list_filter = ("status", "type")
    search_fields = ["text"]
    date_hierarchy = "modified"


# TODO add typing
# TODO improve docstring
class PageAdmin(FlatPageAdmin):
    """
    Page Admin.
    """

    form = PageForm


# Admin page registering ###########################################################
# TODO replace all this by use of @admin.register decorator on each class
# See: https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#the-register-decorator
admin.site.register(Link, LinkAdmin)
admin.site.register(Meeting, MeetingAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Research, ResearchAdmin)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, PageAdmin)

# TODO find why this doesn't work: KeyError: 'delete_selected'
# admin.site.disable_action("delete_selected")
