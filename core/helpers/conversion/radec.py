"""
Functions used to display RA (right ascension) and DEC (declination) coordinates that
are storedas float in their common form : sexagesimal strings :

-   hms (hour, minutes, seconds) for RA
-   dms (degrees, minutes, seconds) for DEC

See: definition here https://vizier.cds.unistra.fr/vizier/doc/catstd-3.3.htx
"""

# Standard imports
import math

# External imports
from astropy.coordinates import SkyCoord

REFERENCE_FRAME = "icrs"
INPUT_UNIT = "deg"
SEP = ":"  # Separator for the sexagesimal output
DUMMY_COORD = 0.0  # Placeholder for unused ra or dec


# TODO add more examples
def ra_float_to_hms_string(ra: float) -> str:
    """
    Convert a RA in degres as a float to its sexagecimal representation as str.

    Args:
        ra: Right ascension in degrees in the range [0.0, 360.0[.

    Returns:
        String representation of the right ascension in hours (integer), minutes
        (integer) and seconds (float) separated by the ":" char.

    Raises:
        ValueError: If ra is not a float or not in valid range or is NaN.

    Examples:
        >>> ra_float_to_hms_string(0.0)
        '00:00:00.0'

        >>> ra_float_to_hms_string(180.0)
        '12:00:00.0'
    """
    if not isinstance(ra, float):
        raise ValueError("ra must be a float.")

    if ra < 0.0 or ra >= 360.0:
        raise ValueError("ra must be in range [0.0, 360.0[.")

    if math.isnan(ra):
        raise ValueError("ra can not be NaN.")

    ra_coord = SkyCoord(
        ra=ra,
        dec=DUMMY_COORD,
        frame=REFERENCE_FRAME,  # I'm not sure is it equivalent to the old code
        unit=INPUT_UNIT,
    ).ra.hms

    return f"{ra_coord.h:02.0f}" f"{SEP}{ra_coord.m:02.0f}" f"{SEP}{ra_coord.s:04.1f}"


# TODO add more examples
def dec_float_to_dms_string(dec: float) -> str:
    """
    Convert a dec in degres as a float to its sexagecimal representation as str.

    Args:
        dec: Declination in degrees in the range [-90.0, 90.0].

    Returns:
        String representation of the declination in degrees preceded by a + or - sign
        (integer), minutes (integer) and seconds (integer) separated by the ":" char.
        Negative 0 is always converted to positive zero for backward compatibility
        reasons.

    Raises:
        ValueError: If dec is not a float or not in valid range or is NaN.

    Examples:
        >>> dec_float_to_dms_string(0.0)
        '+00:00:00.0'

        >>> dec_float_to_dms_string(-90.0)
        '-90:00:00.0'

        >>> dec_float_to_dms_string(+90.0)
        '+90:00:00.0'

        >>> dec_float_to_dms_string(12.5)
        '+12:30:00.0'
    """
    if not isinstance(dec, float):
        raise ValueError("dec must be a float.")

    if dec > 90.0 or dec < -90.0:
        raise ValueError("dec must be in range [90.0, -90.0].")

    if math.isnan(dec):
        raise ValueError("dec can not be NaN.")

    # Convert negative zero to positive zero
    if dec == 0.0 and math.copysign(1.0, dec) < 0.0:
        # raise ValueError("negative zero not allowed")
        dec -= dec

    dec_coord = SkyCoord(
        ra=DUMMY_COORD,
        dec=dec,
        frame=REFERENCE_FRAME,  # I'm not sure is it equivalent to the old code
        unit=INPUT_UNIT,
    ).dec.dms

    return (
        f"{dec_coord.d:+03.0f}"
        f"{SEP}{abs(dec_coord.m):02.0f}"
        f"{SEP}{abs(dec_coord.s):04.1f}"
    )
