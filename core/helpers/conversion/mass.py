"""All mass conversion utils."""

# Standard imports
import logging
from decimal import Decimal, InvalidOperation
from sys import float_info
from typing import Any

# External imports
from typing_extensions import Never

# Local imports
from ...models.choices import MassUnit
from ...models.similarity import _sind_decimal
from .utiltools import stupid_significant_nb_str

# Get a logger for the module
LOGGER = logging.getLogger(__name__)

# Useful constants
JUPITER_MASS_KG = Decimal("1.89813e27")
EARTH_MASS_KG = Decimal("5.97219e24")
M_JUP_TO_EARTH = JUPITER_MASS_KG / EARTH_MASS_KG
M_EARTH_TO_JUP = EARTH_MASS_KG / JUPITER_MASS_KG

# WTF we should not do such kind of stupid adjustments
# Arbitrary adaptation of significant numbers... just because this is what is used
# in the PLSQL code of the the planetdbview
DIGIT_BONUS_EARTH_TO_JUP = 3  # MAGIC NUMBER just because...
DIGIT_BONUS_JUP_TO_EARTH = -2  # MAGIC NUMBER just because...


# TODO: Change as soon as we swith to Python 3.12+
def assert_never(arg: Any) -> Never:
    """
    Waiting for python 3.12 we implement our own assert_never function

    See: https://typing.readthedocs.io/en/latest/guides/unreachable.html
    See: https://docs.python.org/3.12/library/typing.html#typing.assert_never
    """
    raise AssertionError("Expected code to be unreachable")


# BUG this should return a decimal
# BUG this should do smart computation of the significant digits
def mass_in_specific_unit(
    mass: str,
    unit: MassUnit,
    desired_unit: MassUnit,
    significant_nb: int | None = None,
) -> Decimal:
    """
    Accessor to get a mass in any unit. This should return a result compatible with
    what could be found in the planetdbview table.

    The planetdbview data are create via PLSQL code that do some conversion (not always
    very accurate...) and we need this function to give the same results. The reference
    code is the PLSQL function `round_multi_units_value()` located in
    `core/migrations/0006_add_planetdbview_create.sql`.

    Args:
        mass: Mass to convert as a str representing a decimal at a specific precision.
        unit: Unit in which mass is expressed.
        desired_unit: Unit in which we want to convert the mass.
        significant_nb: Wannabe significant digit to use (in fact this is just the
                        number of digits to keep after the dot and it will even be
                        altered by arbitrary numbers. This is just sad).
                        If not provided the number of digits after the dot in the input
                        mass is used.

    Returns:
        A decimal representing the mass in the desired unit.

    Raises:
        InvalidOperation: If mass is not a valid str representation of a decimal.

    Examples:
        See tests in `test_helpers_conversion_mass.py`.
    """
    assert isinstance(mass, str)
    assert isinstance(unit, MassUnit)
    assert isinstance(desired_unit, MassUnit)
    assert isinstance(significant_nb, int) or significant_nb is None

    # Early exit if mass is NaN
    if mass.lower() == "nan":
        return Decimal("NaN")

    # Compute the wannabe number of significant number if possible
    stupid_significant_nb = (
        significant_nb
        if significant_nb is not None
        else stupid_significant_nb_str(mass)
    )

    # Precomputations
    initial_unit = MassUnit(unit)
    try:
        initial_mass = Decimal(mass)
    except InvalidOperation as exc:
        raise InvalidOperation(
            "Provided mass str is not possible to convert to decimal."
        ) from exc

    match (initial_unit, desired_unit):
        case (MassUnit.MEARTH, MassUnit.MEARTH) | (MassUnit.MJUP, MassUnit.MJUP):
            return initial_mass
        case (MassUnit.MEARTH, MassUnit.MJUP):
            significant_digit_nb = max(
                0, stupid_significant_nb + DIGIT_BONUS_EARTH_TO_JUP
            )
            # We use the term significant digits very poorly here since its just a
            # number of digits after the dot... but this is what is done in the PLSQL
            # code T_T
            return round(initial_mass * M_EARTH_TO_JUP, significant_digit_nb)
        case (MassUnit.MJUP, MassUnit.MEARTH):
            significant_digit_nb = max(
                0, stupid_significant_nb + DIGIT_BONUS_JUP_TO_EARTH
            )
            # We use the term significant digits very poorly here since its just a
            # number of digits after the dot... but this is what is done in the PLSQL
            # code T_T
            return round(initial_mass * M_JUP_TO_EARTH, significant_digit_nb)
        case _ as unreachable:
            LOGGER.error(
                "Invalid mass units for conversion, this should never happens."
                "initial unit: <%s> desired unit: <%s>",
                initial_unit,
                desired_unit,
            )
            assert_never(unreachable)


# BUG this should return a decimal
# BUG this should do smart computation of the significant digits
# pylint: disable-next=too-many-arguments,too-many-return-statements
def mass_planet_from_data(
    desired_unit: MassUnit,
    mass_detected_str: str | None = None,
    mass_detected_unit: MassUnit | None = None,
    mass_sini_str: str | None = None,
    mass_sini_unit: MassUnit | None = None,
    inclination: float | None = None,
) -> Decimal | None:
    """
    Compute the mass of a planet in a specific unit from:

    - mass detected
    - mass*sin(i)
    - inclination

    ...and there respective unit.

    This should return a result compatible with what could be found in the planetdbview
    table.

    The planetdbview data are create via PLSQL code that do some conversion (not always
    very accurate...) and we need this function to give the exact same results. The
    reference code is the PLSQL function `get_mass()` located in
    `core/migrations/0027_fix_planetdbview_get_mass.sql`.

    MOTHER FUCKING WARNING: Computation of sini are subjects to float
    imprecision because of pi.

    Args:
        desired_unit: Unit in which we want to express the result mass.
        mass_detected_str: Mass detected (if the mesurement was directly done on mass
            and not `mass * sin(i)`). It must be a str representing a
            decimal at a specific precision.
        mass_detected_unit: Unit of the mass_detected_str parameter.
        mass_sini_str: `mass * sin(inclination)` (if the mesurement was done on
            `mass * sin(i)` and not the mass itself). It must be a str
            representing a decimal at a specific precision.
        mass_sini_unit: Unit of the mass_sini parameter.
        inclination: Orbital inclination in degrees.

    Returns:
        A decimal representing the mass in the desired unit or None if the mass is not
        computable for any reason.

    Examples:
        See tests in `test_helpers_conversion_mass.py`.
    """
    decimal_inclinaison = Decimal(str(inclination)) if inclination is not None else None

    # Early return if one of the unit is None
    if desired_unit is None or mass_detected_unit is None or mass_sini_unit is None:
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "no unit should be None but "
            "desired unit is <%s>"
            "and mass detected unit is <%s> "
            "and mass sini unit is <%s> ",
            desired_unit,
            mass_detected_unit,
            mass_sini_unit,
        )
        return None

    # Early return if mass is NaN and mass sini or inclination are NaN
    if (
        # mass is NaN
        mass_detected_str is not None
        and mass_detected_str.lower() == "nan"
    ) and (
        (
            # mass sini is NaN
            mass_sini_str is not None
            and mass_sini_str.lower() == "nan"
        )
        or (
            # inclination is NaN
            decimal_inclinaison is not None
            and decimal_inclinaison.is_nan()
        )
    ):
        return Decimal("NaN")

    # we return mass_detected simply converted to the good unit as a decimal
    if mass_detected_str is not None:
        try:
            return mass_in_specific_unit(
                mass=mass_detected_str,
                unit=mass_detected_unit,
                desired_unit=desired_unit,
            )
        except InvalidOperation:
            # Can't calculate mass if mass can't be converted to decimal
            LOGGER.warning(
                "Fail to calculate mass for planet so return None, "
                "Invalid mass string <%s>: not convertible to decimal",
                mass_detected_str,
            )
            return None

    # Can't calculate mass if no inclination, mass_sini
    if decimal_inclinaison is None or mass_sini_str is None:
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "mass is <%s> but "
            "inclination <%s> or mass sini <%s> or mass sini unit <%s> are None",
            mass_detected_str,
            decimal_inclinaison,
            mass_sini_str,
            mass_sini_unit,
        )
        return None

    # Can't calculate mass if inclination is not finite
    if not decimal_inclinaison.is_finite():
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "mass is <%s> and inclination is not a finite number <inclination: %s>",
            mass_detected_str,
            decimal_inclinaison,
        )
        return None

    # Can't calculate if inclination is subnormal (lower than min float)
    if (
        decimal_inclinaison != Decimal("0.0")
        and abs(decimal_inclinaison) < float_info.min
    ):
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "mass is <%s>, mass sini is <%s> but inclination is too small <%s>",
            mass_detected_str,
            mass_sini_str,
            decimal_inclinaison,
        )
        return None

    # Compute the necessary intermediate values
    sini = _sind_decimal(decimal_inclinaison)

    # Can't calculate mass if sini is zero
    if sini == Decimal("0.0"):
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "mass is <%s> and mass sini is <%s> but sini is zero"
            "<inclination: %s> <sini: %s>",
            mass_detected_str,
            mass_sini_str,
            decimal_inclinaison,
            sini,
        )
        return None

    # Can't calculate mass if mass_sini is invalid
    try:
        mass_sini = Decimal(mass_sini_str)
    except InvalidOperation:
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "mass is <%s> but Invalid mass sini string <%s>: "
            "not convertible to decimal",
            mass_detected_str,
            mass_sini_str,
        )
        return None

    # Can't calculate mass if mass_sini is not finite
    if not mass_sini.is_finite():
        LOGGER.warning(
            "Fail to calculate mass for planet so return None, "
            "mass is <%s> but mass sini is <%s> which is not a finite number",
            mass_detected_str,
            mass_sini,
        )
        return None

    # From this point we know we have computable values

    # we return mass computed from mass_sini and inclination and converted as a decimal
    mass_angle_corrected = mass_sini / abs(sini)

    mass_angle_corrected_str = str(mass_angle_corrected)

    try:
        return mass_in_specific_unit(
            mass=mass_angle_corrected_str,
            unit=mass_sini_unit,
            desired_unit=desired_unit,
            significant_nb=stupid_significant_nb_str(mass_sini_str),
        )
    except InvalidOperation as unreachable:
        LOGGER.error(
            "mass angle corrected <%s> can never be an invalid str representation "
            "since it is created from a decimal.",
            mass_angle_corrected_str,
        )
        assert_never(unreachable)
