"""Util tools for conversion."""

# Standard imports
import re
from decimal import InvalidOperation


def stupid_significant_nb_str(number: str) -> int:
    """
    Replicate exactly what is used as "significant number" in the SQL code creating the
    db view.

    Raises:
        ValueError: if the provided str is not in float-compatible format..

    Examples:
        >>> stupid_significant_nb_str('1.0')
        1
        >>> stupid_significant_nb_str('123456.0')
        1
        >>> stupid_significant_nb_str('0.1')
        1
        >>> stupid_significant_nb_str('0.001')
        3
        >>> stupid_significant_nb_str('1.23e+1')
        2
        >>> stupid_significant_nb_str('1.23e+5')
        2
        >>> stupid_significant_nb_str('1.23e-3')
        2
        >>> stupid_significant_nb_str('0.03e+1')
        2
        >>> stupid_significant_nb_str('-0.03e+1')
        2
    """
    # Regex is borrowed here: https://www.regular-expressions.info/floatingpoint.html
    float_regex = r"^[-+]?[0-9]*\.?(?P<digits_after_dot>[0-9]+)([eE][-+]?[0-9]+)?$"
    f_regex = re.compile(float_regex)
    match = f_regex.match(number)

    if match is None:
        raise InvalidOperation("Provided number can't be converted to float.")

    digits_after_dot = match.group("digits_after_dot")
    if digits_after_dot is None:
        raise InvalidOperation("Provided number can't be converted to float.")
    else:
        return len(digits_after_dot)
