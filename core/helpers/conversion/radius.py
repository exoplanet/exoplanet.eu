# Local imports
from ...models.choices import RadiusUnit
from .utiltools import stupid_significant_nb_str


# BUG this should return a decimal
# BUG this should do smart computation of the significant digits
def radius_in_specific_unit(
    radius: str, unit: RadiusUnit, desired_unit: RadiusUnit
) -> float:
    """
    Accessor to get a radius in any unit. This should return give a result compatible
    with what could be found in the planetdbview table.

    The planetdbview data are create via PLSQL code that do some conversion (not always
    very accurate...) and we need this function to give the same results. The reference
    code is the PLSQL function `round_multi_units_value()` located in
    `core/migrations/0006_add_planetdbview_create.sql`.

    MOTHER FUCKING WARNING: the conversion and the computation are subjects to float
    imprecision.

    Args:
        radius: the radius to convert as a str representing a float at a specific
                precision
        unit: the unit in which the radius is expressed
        desired_unit: the unit in which we want to convert the radius

    Returns:
        a float representing the radius in the desired unit.

    Raises:
        ValueError: if the provided radius is not in float-compatible format or if the
        units are not RJUP or REARTH.

    Examples:
        Using some shortcuts:

        >>> RJUP = RadiusUnit.RJUP
        >>> REARTH = RadiusUnit.REARTH

        Let's express that the Earth has a radius of one earth radius:

        >>> EARTH_RADIUS_IN_REARTH = "1.0"
        >>> radius_in_specific_unit(EARTH_RADIUS_IN_REARTH, REARTH, REARTH)
        1.0

        …and in radius of jupiter:

        >>> radius_in_specific_unit(EARTH_RADIUS_IN_REARTH, REARTH, RJUP)
        0.089

        Jupiter has a radius of one jupiter radius:

        >>> JUPITER_RADIUS_IN_RJUP = "1.0"
        >>> radius_in_specific_unit(JUPITER_RADIUS_IN_RJUP, RJUP, RJUP)
        1.0

        …and in radius of earth:

        >>> radius_in_specific_unit(JUPITER_RADIUS_IN_RJUP, RJUP, REARTH)
        11.0

        The number of significant digits used in the input matters:

        >>> LONG_RADIUS_IN_RJUP = "1.000"
        >>> radius_in_specific_unit(LONG_RADIUS_IN_RJUP, RJUP, REARTH)
        11.21
        >>> VERY_LONG_RADIUS_IN_RJUP = "1.000000"
        >>> radius_in_specific_unit(VERY_LONG_RADIUS_IN_RJUP, RJUP, REARTH)
        11.20892
    """
    assert isinstance(radius, str)
    assert isinstance(unit, RadiusUnit)
    assert isinstance(desired_unit, RadiusUnit)

    # WTF we should not do such kind of stupid adjustments
    # Arbitrary adaptation of significant numbers... just because this is what is used
    # in the PLSQL code of the the planetdbview
    DIGIT_BONUS_EARTH_TO_JUP = 2  # MAGIC NUMBER just because...
    DIGIT_BONUS_JUP_TO_EARTH = -1  # MAGIC NUMBER just because...

    # Useful constants
    JUPITER_RADIUS_METER = 7.1492e7
    EARTH_RADIUS_METER = 6.3781370e6
    R_JUP_TO_EARTH = JUPITER_RADIUS_METER / EARTH_RADIUS_METER
    R_EARTH_TO_JUP = EARTH_RADIUS_METER / JUPITER_RADIUS_METER

    # Some shortcuts
    RJUP = RadiusUnit.RJUP
    REARTH = RadiusUnit.REARTH

    # Precomputations
    initial_unit = RadiusUnit(unit)
    try:
        initial_radius = float(radius)
    except ValueError:
        raise ValueError("Provided radius str is not possible to convert to float.")

    if desired_unit == initial_unit:
        return initial_radius
    elif (initial_unit, desired_unit) == (REARTH, RJUP):
        SIGNIFICANT_DIGIT_NB = max(
            0, stupid_significant_nb_str(radius) + DIGIT_BONUS_EARTH_TO_JUP
        )

        # We use the term significant digits very poorly here since its just a number of
        # digits after the dot... but this is what is done in the PLSQL code T_T
        return round(initial_radius * R_EARTH_TO_JUP, SIGNIFICANT_DIGIT_NB)
    elif (initial_unit, desired_unit) == (RJUP, REARTH):
        SIGNIFICANT_DIGIT_NB = max(
            0, stupid_significant_nb_str(radius) + DIGIT_BONUS_JUP_TO_EARTH
        )
        # We use the term significant digits very poorly here since its just a number of
        # digits after the dot... but this is what is done in the PLSQL code T_T
        return round(initial_radius * R_JUP_TO_EARTH, SIGNIFICANT_DIGIT_NB)
    else:
        raise ValueError("Radius units conversion logic error.")
