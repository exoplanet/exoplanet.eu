"""
Small functions used to handle flatpages more easily.
"""
# Standard imports
from datetime import datetime

# Django 💩 imports
from django.contrib.admin.models import CHANGE, LogEntry
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.models import FlatPage
from django.db.models import Max
from django.utils import timezone


def flatpage_last_update(fp_url: str) -> datetime | None:
    """
    Get the last update date of the flatpage corresponding to provided url.

    Args:
        fp_url: url of the flatpage

    Returns:
        datetime corresponding to the last modification of the flatpage according to the
            logentry (or None if the flatpage exist but has no record in the logentry).

    Raise:
        FlatPage.DoesNotExist: if the no flatpage exist with the given url.
    """
    # First we need the flatpage content type
    fp_type = ContentType.objects.get(app_label="flatpages", model="flatpage")

    # The we get the id of the desired flatpage
    # pylint: disable-next=no-member
    fp_id = FlatPage.objects.get(url=fp_url).id  # type: ignore

    # Now we can retrieve the last update
    last = LogEntry.objects.filter(content_type=fp_type, object_id=fp_id).aggregate(
        Max("action_time")
    )

    # The actual value is stored in a dict with only one key...
    last_update: datetime | None = last.get("action_time__max", None)
    return last_update


def flatpage_fake_update(fp_url: str, user: User) -> None:
    """
    Add an entry to the LogEntry to mimic an CHANGE update to the specified flatpage.

    The time for the update is always timezone.now().

    This function is intended to be used in tests since it is not possible to do admin
    modification to a flatpage programmatically that are registered in the LogEntry
    (only the one made on the admin site are recorded).

    Args:
        fp_url: url of the flatpage

    Raise:
        FlatPage.DoesNotExist: if the no flatpage exist with the given url.
    """
    # First we need the flatpage content type
    fp_type = ContentType.objects.get(app_label="flatpages", model="flatpage")

    # The we get the desired flatpage
    flat_page = FlatPage.objects.get(url=fp_url)  # pylint: disable=no-member

    fake = LogEntry(
        action_time=timezone.now(),
        user=user,
        content_type=fp_type,
        object_id=flat_page.id,  # type: ignore
        object_repr=repr(flat_page),
        action_flag=CHANGE,
        change_message="A fake change.",
    )
    fake.save()
