# type: ignore
# TODO: check if this whole module is still usefull. Do we need it for correct planet
# and star ordering in the catalog taking account of the null values Django 💩 imports
# Django 💩 imports
from django.db import connections, models
from django.db.backends.base.base import BaseDatabaseWrapper
from django.db.models.sql.compiler import SQLCompiler


class NullsLastQuery(models.sql.query.Query):
    """
    Query that uses custom compiler,
    to utilize PostgreSQL feature of setting position of NULL records
    """

    def get_compiler(
        self, using: str | None = None, connection: BaseDatabaseWrapper = None
    ):
        if using is None and connection is None:
            raise ValueError("Need either using or connection")

        if using:
            connection = connections[using]

        # defining that class elsewhere results in import errors
        class NullsLastSQLCompiler(SQLCompiler):
            # BUG: this method does not exist since Django 1.8 so this does nothing
            # The new method is get_order_by() but it works very differently
            def get_ordering(self):
                # pylint: disable-next=no-member
                result, params, group_by = super().get_ordering()

                if self.connection.vendor == "postgresql" and result:
                    result = [line + " NULLS LAST" for line in result]

                return result, params, group_by

        return NullsLastSQLCompiler(self, connection, using)


class NullsLastQuerySet(models.query.QuerySet):
    def __init__(self, model=None, query=None, using=None):
        super().__init__(model, query, using)

        self.query = query or NullsLastQuery(self.model)


class NullsLastQueryManager(models.Manager):
    def get_query_set(self):
        return NullsLastQuerySet(self.model, using=self._db)
