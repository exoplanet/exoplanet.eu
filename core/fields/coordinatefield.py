# Django 💩 imports
from django.db import models
from django.db.backends.base.base import BaseDatabaseWrapper


class CoordinateField(models.Field):
    """Store coordinate value as a double precision float."""

    def db_type(self, connection: BaseDatabaseWrapper) -> str | None:
        return "double precision"

    def to_python(self, value: float | str | None) -> float | str | None:
        return value

    def clean(
        self, value: float | str | None, model_instance: models.Model | None
    ) -> float | str | None:
        """
        Convert the value's type and run validation. Validation errors
        from to_python and validate are propagated. The correct value is
        returned if no error is raised.
        """
        if value == "":
            value = None

        value = self.to_python(value)
        self.validate(value, model_instance)
        self.run_validators(value)

        return value
