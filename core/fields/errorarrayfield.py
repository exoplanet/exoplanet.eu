"""pecialized array based on PostgreQL array type."""

# Standard imports
from contextlib import contextmanager
from typing import Any, Iterator, cast

# Django 💩 imports
from django.contrib.postgres import fields, forms
from django.core.exceptions import ValidationError
from django.forms.fields import Field


# TODO Specify the type directly in the class
# TODO Replace this by a more specialized version with direct acces to errors via
#      attributes error, error_min, error_max and methods is_relative_error() and
#      is_min_max_error()
# BUG This should enforce the use of Decimal type for the array
# TODO repalce this by a more generic ValueWithError that also store the value like
#      this: (val, err/err_min, err_max) with clean accessor to relative err, min err
#      and max err
class ErrorArrayField(fields.ArrayField):
    """
    Represent the error (relative error or min and max error) on a floating point value.

    This use the PostgreSQL specific Array type.

    Actually when used its value type is charField with a specified length the represent
    floating point value with correct accuracy and prevent floating point errors.

    Possible values are:

    - no value: None
    - no error: []
    - a single relative error: [error]
    - a min and max error: [min_error, max_error]
    """

    def formfield(
        self,
        form_class: type[Field] | None = None,
        choices_form_class: type[Field] | None = None,
        **kwargs: dict[str, Any],
    ) -> Field:
        defaults = {
            "form_class": ErrorSimpleArrayField,
            "base_field": self.base_field.formfield(),
            "max_length": self.size,
        }

        defaults.update(kwargs)

        # Django is 💩. Dont ask!!!
        return super().formfield(**defaults)  # type: ignore[arg-type]


class ErrorSimpleArrayField(forms.SimpleArrayField):
    """
    Form corresponding to the `ErrorArrayField` custom array field.
    """

    def prepare_value(self, value: Any) -> str | Any:
        if value is None:
            return "[]"

        if isinstance(value, list):
            elements = [str(self.base_field.prepare_value(e)) for e in value]
            return f"[{self.delimiter.join(elements)}]"

        return value

    def to_python(  # type: ignore [override]
        self,
        value: list[Any] | None,
    ) -> list[float]:
        """
        Convert an ErrorArrayField to python.

        Args:
            values: None or a list of values stored in the ErrorArrayField. They are all
            of the same type but this type depend on the type specified when creating
            the ErrorArrayField. This list can be of size 0, 1 or 2.

        Returns:
            a list of 0, 1 or 2 floats representing the error values.
        """
        if value and value != "[]":
            return cast(list[float], super().to_python(value[1:-1]))

        return []

    def validate(self, value: Any) -> None:
        if value and value != "[]":
            for v in value:
                try:
                    if not (
                        isinstance(value, str)
                        and (v.lower() == "inf" or v.lower() == "nan")
                    ):
                        float(v)
                # TODO catch more specific exceptions please!
                except Exception as err:
                    raise ValidationError("Invalid list") from err

        super().validate(value)


def norm_error_array(python_error_array: list[Any]) -> list[float | None]:
    """
    Always return a consistent list of length 2 when using the python value of an
    ErrorArrayField.

    This is useful since ErrorArrayField are not always of the same size depending on
    the kind of error the store.

    Args:
        python_error_array: A list as the one returned by the `to_python()` method of
        the ErrorSimpleArrayField class. It must contain float an be of size 0~2.

    Returns:
        always a list of size 3 with same elements as input. Trailing missing element
        are replaced by None.

    Raises:
        ValueError: if the input is not a list of 0, 1 or 2 elements.

    Examples:
        >>> norm_error_array([1.0, 2.0])
        [1.0, 2.0]

        >>> norm_error_array([1.0])
        [1.0, None]

        >>> norm_error_array([])
        [None, None]
    """
    nb_elem = len(python_error_array)

    if nb_elem == 2:
        return python_error_array

    if nb_elem == 1:
        return [*python_error_array, None]

    if nb_elem == 0:
        return [None, None]

    raise ValueError("Invalid error array: it must be a 0, 1 or 2 element list.")


@contextmanager
def norm_error_array_ctx(python_error_array: list[Any]) -> Iterator[list[float | None]]:
    """
    Context manager to use in a with statement to alwas have a consistent list of length
    2 when using the python value of an ErrorArrayField.

    This is useful since ErrorArrayField are not always of the same size depending on
    the kind of error the store.

    Args:
        python_error_array: A list as the one returned by the `to_python()` method of
        the ErrorSimpleArrayField class. It must contain float an be of size 0~2.

    Returns:
        always a list of size 3 with same elements as input. Trailing missing element
        are replaced by None.

    Examples:
        >>> with norm_error_array_ctx([1., 2.]) as two:
        ...     assert two == [1., 2.]

        >>> with norm_error_array_ctx([1.]) as one:
        ...     assert one == [1., None]

        >>> with norm_error_array_ctx([]) as zero:
        ...     assert zero == [None, None]
    """
    yield norm_error_array(python_error_array)
