# Standard imports
from typing import Any

# Django 💩 imports
from django.core.exceptions import ValidationError
from django.db import models

# External imports
from unidecode import unidecode


class AsciiCharField(models.CharField):
    """A field that convert all extended utf-8 characters to their ascii equivalent."""

    def clean(self, value: str | None, model_instance: models.Model | None) -> Any:
        if value is None:
            raise ValidationError("Field can't be None")

        try:
            normalized_value = unidecode(value)
        except Exception as exc:
            raise ValidationError("Field contains invalid character") from exc

        return super().clean(normalized_value, model_instance)
