# Standard imports
from typing import Any

# Django 💩 imports
from django.core.exceptions import ValidationError
from django.db import models


def _only_positive_float(value: str | None) -> str | None:
    """
    Check if the provided value represents a float.

    Args:
        value: None or a string that should be representing a float.

    Returns:
        the original value or None.

    Raises:
        ValidationError: if the value can not be converted to float or if it can only
            be converted to negative float.

    Examples:
        >>> _only_positive_float("1.234")
        '1.234'
        >>> _only_positive_float("")
        >>> _only_positive_float(None)
        >>> _only_positive_float("-1.234")
        Traceback (most recent call last):
            ...
        django.core.exceptions.ValidationError: ['Value must be positive: -1.234']
        >>> _only_positive_float("abcd")  # doctest: +ELLIPSIS
        Traceback (most recent call last):
            ...
        django...ValidationError: ['Value must be able to be cast into float: abcd']
    """
    if value == "" or value is None:
        return None
    else:
        try:
            float_value = float(value)
        except ValueError as exc:
            # https://docs.djangoproject.com/fr/3.0/ref/forms/validation/#raising-validationerror
            raise ValidationError(
                "Value must be able to be cast into float: %(value)s",
                code="invalid",
                params=dict(value=value),
            ) from exc

        if float_value < 0:
            # https://docs.djangoproject.com/fr/3.0/ref/forms/validation/#raising-validationerror
            raise ValidationError(
                "Value must be positive: %(value)s",
                code="negative",
                params=dict(value=value),
            )

        return value


# TODO replace this by a simple Decimal field ??? ASKCYRIL
class PositiveFloatValueAsStringField(models.CharField):
    """
    Field to represent a positive float while storing it as string.
    """

    def pre_save(self, model_instance: models.Model, add: bool) -> Any:
        """
        Returns field's value just before saving.
        """
        value = getattr(model_instance, self.attname)
        if value == "":
            return None

        return value

    def clean(self, value: Any, model_instance: models.Model | None) -> Any:
        """
        Convert the value's type and run validation. Validation errors
        from to_python and validate are propagated. The correct value is
        returned if no error is raised.
        """
        value = _only_positive_float(value)

        value = self.to_python(value)
        self.validate(value, model_instance)
        self.run_validators(value)

        return value
