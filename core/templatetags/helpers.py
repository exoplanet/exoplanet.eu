"""
Custom filters to be used in our django templates
"""

# Standard imports
import math
from inspect import cleandoc  # See: https://stackoverflow.com/a/47417848/20450444
from json import loads
from json.decoder import JSONDecodeError
from typing import Any

# Django 💩 imports
from django import template

# First party imports
from core.models import PublicationStatus

register = template.Library()


@register.filter
def format_value_with_error(value: float | None, errors: list[float] | None) -> str:
    """
    Helper filter to format value errors into proper HTML/MathML.

    Args:
        value: the value to display
        errors: list of one or two values:
            - one value: the relative error
            - two values: the minimum and maximum value

    Returns:
        a string representing the value and its error(s) in a proper HTML/MathML way
        adapted to the error provided
    """
    match (value, errors):
        case (None, _):
            # No value mean noting to display
            return cleandoc(
                """
                <math>
                  <mrow>
                    <mn class="value no-value"></mn>
                  </mrow>
                </math>
                """
            )
        case (exact_value, None):
            return cleandoc(
                f"""
                <math>
                  <mrow>
                    <mn class="value no-err-value">{exact_value}</mn>
                  </mrow>
                </math>
                """
            )
        # test this with HD 69830 b
        # BUG: we should not allow positive infinity as min err
        # pylint: disable-next=used-before-assignment
        case (value, [err_min, _]) if math.isinf(err_min):
            # The value is a superior value
            return cleandoc(
                f"""
                <math>
                  <mrow>
                    <mo><</mo> <mn class="value max-value">{value}</mn>
                  </mrow>
                </math>
                """
            )
        # BUG: we should not allow negative infinity as max err
        # pylint: disable-next=used-before-assignment
        case (value, [_, err_max]) if math.isinf(err_max):
            # The value is a inferior value
            return cleandoc(
                f"""
                <math>
                  <mrow>
                    <mo>></mo> <mn class="value min-value">{value}</mn>
                  </mrow>
                </math>
                """
            )
        # BUG: we should check if both errors are positive
        # BUG: we should not allow errors with more than 2 elements
        case (value, [err_min, err_max, *_]):
            # value with min and max errors
            return cleandoc(
                f"""
                <math>
                  <mrow>
                    <mn class="value both-err-value">{value}</mn>
                    <mspace width="0.5em"></mspace>
                    <mo fence="true">(</mo>
                    <mrow>
                      <mtable>
                        <mtr>
                          <mtd class="err-max"><mo>+</mo><mn>{err_max}</mn></mtd>
                        </mtr>
                        <mtr>
                          <mtd class="err-min"><mo>-</mo><mn>{err_min}</mn></mtd>
                        </mtr>
                      </mtable>
                    </mrow>
                    <mo fence="true">)</mo>
                  </mrow>
                </math>
                """
            )
        # BUG: we should not allow nan or infinity as a relative error
        case (value, [rel_error]):
            # value with relative error
            return cleandoc(
                f"""
                <math>
                  <mrow>
                    <mn class="value rel-err-value">{value}</mn>
                    <mo fence="true">(</mo>
                    <mrow class="err-rel">
                      <mo>±</mo>
                      <mn>{rel_error}</mn>
                    </mrow>
                    <mo fence="true">)</mo>
                  </mrow>
                </math>
                """
            )
        # TODO: test this
        case _:
            # Default case: just show the value
            return f'<span class="value no-err-value">{value}</span>'

    # Default case: just show the value <-- added just for mypy sake
    return f'<span class="value no-err-value">{value}</span>'


@register.filter
def is_list_or_list_as_str(value: Any) -> bool:
    """
    Know if a string representing a list.

    Args:
        value: what we want to test (normally a string representing a list).

    Returns:
        True if value reprensenting a string, False otherwise.

    Examples:
        >>> is_list_or_list_as_str("[1, 2]")
        True

        >>> is_list_or_list_as_str([1, 2])
        True

        >>> is_list_or_list_as_str("toto")
        False

        >>> is_list_or_list_as_str(12)
        False
    """
    if not isinstance(value, (list, str)):
        return False

    try:
        isinstance(loads(str(value)), list)
        return True
    except JSONDecodeError:
        return False


@register.filter
def get_planet_pub_status_from_letter(letter: str) -> str:
    """
    Get planet publication status from associated letter.

    Args:
      letter: Given letter which we want the publication status.

    Returns:
      Publication status associated to the given letter if it's known, otherwise '—'.

    Examples:
      >>> stupid_prefix = '<math>\\n  <mrow>\\n    <mn class="value no-err-value">'
      >>> stupid_suffix = '</mn>\\n  </mrow>\\n</math>'

      >>> get_planet_pub_status_from_letter(f"{stupid_prefix}R{stupid_suffix}")
      'Published in a refereed paper'

      >>> get_planet_pub_status_from_letter(f"{stupid_prefix}S{stupid_suffix}")
      'Submitted to a professional journal'

      >>> get_planet_pub_status_from_letter(f"{stupid_prefix}C{stupid_suffix}")
      'Announced on a professional conference'

      >>> get_planet_pub_status_from_letter(f"{stupid_prefix}W{stupid_suffix}")
      'Announced on a website'

      >>> get_planet_pub_status_from_letter("toto")
      '—'

      >>> get_planet_pub_status_from_letter("R")
      '—'

      >>> get_planet_pub_status_from_letter("S")
      '—'

      >>> get_planet_pub_status_from_letter("C")
      '—'

      >>> get_planet_pub_status_from_letter("W")
      '—'
    """
    stupid_prefix = '<math>\n  <mrow>\n    <mn class="value no-err-value">'
    stupid_suffix = "</mn>\n  </mrow>\n</math>"
    if letter == f"{stupid_prefix}R{stupid_suffix}":
        return PublicationStatus.R.value
    elif letter == f"{stupid_prefix}S{stupid_suffix}":
        return PublicationStatus.S.value
    elif letter == f"{stupid_prefix}C{stupid_suffix}":
        return PublicationStatus.C.value
    elif letter == f"{stupid_prefix}W{stupid_suffix}":
        return PublicationStatus.W.value
    else:
        return "—"
