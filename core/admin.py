"""Configuration of the django admin pages of the models."""

# Standard imports
import logging
import math
from datetime import datetime
from decimal import Decimal
from typing import Any, NamedTuple, TypedDict, cast

# Django 💩 imports
from django import forms
from django.contrib import admin
from django.contrib.admin.models import DELETION, LogEntry
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.query import QuerySet
from django.http import HttpRequest
from django.urls import reverse
from django.utils.html import escape

# Local imports
from .models import (
    AlternatePlanetName,
    AlternateStarName,
    AtmosphereMolecule,
    AtmosphereTemperature,
    Molecule,
    Planet,
    Planet2Publication,
    Planet2Star,
    PlanetarySystem,
    PlanetDetection,
    Publication,
    Star,
    Star2Publication,
    WebStatus,
    dec_string_to_degree_value,
    is_at_the_same_position_of_other_star,
    planet_name_already_used_as_alternate_name,
    ra_string_to_degree_value,
    star_name_already_used_as_alternate_name,
)

LOGGER = logging.getLogger(__name__)

# Utility functions and classes ###############################################


# TODO add typing
# TODO add docstring
# TODO add doctest
def _check_strictly_positive_error_min(
    field_name: str,
    field_name_error: str,
    value: float | None,
    error: list[float] | None,
) -> None:
    # Early return
    if (
        value is None
        or error is None
        or error == []
        or math.isnan(error[0])
        or math.isinf(error[0])
    ):
        return

    error_min = error[0]
    if value - error_min <= 0:
        raise forms.ValidationError(
            f"{field_name} - {field_name_error}_min must be strictly positive"
        )


class FieldSet(NamedTuple):
    # pylint: disable=line-too-long
    """
    A small utility namedtuple to improve lisibility of fieldsets definitions

    See: https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.fieldsets # noqa: E501
    """

    name: str
    field_options: dict


class FieldOptions(TypedDict, total=False):
    # pylint: disable=line-too-long
    """
    A small utility typed dict to improve lisibility of fieldsets definitions

    See: https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.ModelAdmin.fieldsets # noqa: E501
    """

    classes: tuple[str]
    fields: tuple[str | tuple[str]]
    description: str


# Inline definitions ###########################################################


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class PlanetPublicationInline(admin.TabularInline):
    model = Planet2Publication
    raw_id_fields = ("publication",)
    extra = 1
    verbose_name_plural = "Publications"
    verbose_name = "Publication"


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class PlanetPublication2Inline(admin.TabularInline):
    """Seen from Planet_oneitem page"""

    model = Planet2Publication
    raw_id_fields = ("planet",)
    extra = 1


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class StarPublicationInline(admin.TabularInline):
    model = Star2Publication
    raw_id_fields = ("publication",)
    extra = 1
    verbose_name_plural = "Publications"
    verbose_name = "Publication"


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class AlternatePlanetNamesInline(admin.TabularInline):
    model = AlternatePlanetName
    extra = 1


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class AlternateStarNamesInline(admin.TabularInline):
    model = AlternateStarName
    extra = 1


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class AtmosphereTemperatureInLine(admin.TabularInline):
    model = AtmosphereTemperature
    raw_id_fields = ("publication",)
    fields = (
        "publication",
        "data_source",
        "type",
        "result_value",
        "result_value_error",
        "result_figure",
        "notes",
    )
    extra = 0


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class AtmosphereMoleculeInLine(admin.TabularInline):
    model = AtmosphereMolecule
    raw_id_fields = ("publication",)
    fields = (
        "molecule",
        "publication",
        "data_source",
        "type",
        "result_value",
        "result_value_error",
        "result_figure",
        "notes",
    )
    extra = 0


class Planet2StarInline(admin.TabularInline):
    # pylint: disable=line-too-long
    """
    We can not directly display a widget for the stars field in the admin since it's
    a custom many-to-many relationship. So we need this to replace it.

    See: https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#working-with-many-to-many-intermediary-models  # noqa: E501

    If we try to use de default widget (by not specifying this inline in the
    `PlanetAdmin` class) when launching the site, we trigger the error:

    <class 'core.admin.PlanetAdmin'>: (admin.E013)
    The value of 'fieldsets[0][1]["fields"][1]' cannot include the ManyToManyField
    'stars', because that field manually specifies a relationship model.
    """

    model = Planet2Star
    raw_id_fields = ("star",)
    extra = 1
    verbose_name_plural = "Other Stars (main star will appear here)"
    verbose_name = "Star"


# Admin classes ##############################################################


# TODO add typing
# TODO improve docstring
class ExoplanetModelAdmin(admin.ModelAdmin):
    """
    Convert all NumberInput widgets to TextInput to avoid decimal separators differences
    between languages/browser
    """

    formfield_overrides = {models.FloatField: {"widget": forms.TextInput()}}


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class PlanetAdminForm(forms.ModelForm):
    # pylint: disable-next=missing-class-docstring
    class Meta:
        model = Planet
        fields = "__all__"

    # TODO add typing
    # TODO add docstring
    # pylint: disable-next=missing-function-docstring
    def get_error_string(self, value: None | list[float]) -> str:
        if value is None:
            return "[]"
        else:
            try:
                return f"[{','.join(str(value))}]"
            except ValueError as exc:
                # Attribute `error_messages` is inherite from Dj Model (Django == 💩)
                raise forms.ValidationError(
                    # pylint: disable-next=no-member
                    self.error_messages["invalid_list"]  # type: ignore[attr-defined]
                ) from exc

    # TODO add typing
    # TODO add docstring
    def clean_name(self) -> str:
        """
        This is proof of concept for cleaning and validating `name` field of
        the Planet
        """
        self._validate_unique = True
        value = self.cleaned_data["name"]
        if "_" in value:
            raise forms.ValidationError("Invalid character _")

        # name with alternate name
        name_alreay_used, used_by = planet_name_already_used_as_alternate_name(value)
        if name_alreay_used:
            raise forms.ValidationError(
                f"Name: {value} already used as an alternate name " f"by {used_by}."
            )

        return cast(str, value)

    # TODO add typing
    # TODO add docstring
    def clean(self) -> dict[str, Any]:
        match (self.cleaned_data["main_star"], self.cleaned_data["planetary_system"]):
            case (None, None):
                raise forms.ValidationError(
                    "At least one star or a planetary system must be chosen"
                )
            case (_, None) | (None, _):
                pass
            case _:
                raise forms.ValidationError(
                    "You cannot choose both a star and a planetary system"
                )
        if (
            "mass_detected" in self.cleaned_data
            and "mass_detected_error" in self.cleaned_data
        ):
            _check_strictly_positive_error_min(
                "mass_detected",
                "mass_detected_error",
                self.cleaned_data["mass_detected"],
                self.cleaned_data["mass_detected_error"],
            )

        if "mass_sini" in self.cleaned_data and "mass_sini_error" in self.cleaned_data:
            _check_strictly_positive_error_min(
                "mass_sini",
                "mass_sini_error",
                self.cleaned_data["mass_sini"],
                self.cleaned_data["mass_sini_error"],
            )

        if "radius" in self.cleaned_data:
            _check_strictly_positive_error_min(
                "radius",
                "radius_error",
                self.cleaned_data["radius"],
                self.cleaned_data["radius_error"],
            )

        if "detection_type" in self.cleaned_data:
            if not all(
                element in PlanetDetection.all_values_number()
                for element in self.cleaned_data["detection_type"]
            ):
                raise forms.ValidationError(
                    "All detection type must be a valid detection type: "
                    f"{PlanetDetection.get_numeric_avaible_value()}"
                )

        return self.cleaned_data


class PlanetAdmin(ExoplanetModelAdmin):
    """Page to add/remove/edit a planet in the admin"""

    form = PlanetAdminForm
    list_display = (
        "id",
        "star",
        "name",
        "detection_type",
        "discovered",
        "modified",
        "_status",
    )
    ordering = ("-modified",)
    radio_fields = {"status": admin.HORIZONTAL}
    list_filter = ("status", "discovered")
    # TODO add alternate_names and stars__alternate_names to the search
    search_fields = ["name", "stars__name"]
    readonly_fields = ("modified",)
    date_hierarchy = "modified"

    raw_id_fields = ("planetary_system", "main_star")
    inlines = (
        Planet2StarInline,
        PlanetPublicationInline,
        AlternatePlanetNamesInline,
        AtmosphereTemperatureInLine,
        AtmosphereMoleculeInLine,
    )
    fieldsets = cast(
        tuple,
        (
            FieldSet(
                name="Planet identification details",
                field_options={
                    "fields": [
                        ("name", "main_star", "planetary_system"),
                        (
                            "planet_status",
                            "detection_type",
                            "publication_status",
                            "discovered",
                        ),
                        ("modified", "status"),
                    ],
                    "description": "<i>Main planet data</i>",
                },
            ),
            FieldSet(
                name="Physical parameters",
                field_options={
                    "classes": ("collapse", "wide"),
                    "fields": [
                        ("radius_string", "radius_error_string", "radius_unit"),
                        (
                            "mass_detected_string",
                            "mass_detected_error_string",
                            "mass_detected_unit",
                        ),
                        (
                            "mass_sini_string",
                            "mass_sini_error_string",
                            "mass_sini_unit",
                        ),
                        # ('mass_sini', 'mass_sini_error', 'mass_sini_original_unit'),
                        ("mass_measurement_type", "radius_measurement_type"),
                        ("tperi", "tperi_error"),
                        ("axis", "axis_error"),
                        ("period", "period_error"),
                        ("tconj", "tconj_error"),
                        ("inclination", "inclination_error"),
                        ("angular_distance"),
                        ("tzero_tr", "tzero_tr_error"),
                        ("tzero_vr", "tzero_vr_error"),
                        ("eccentricity", "eccentricity_error"),
                        ("omega", "omega_error"),
                        ("log_g"),
                        ("tzero_tr_sec", "tzero_tr_sec_error"),
                        ("lambda_angle", "lambda_angle_error"),
                        ("impact_parameter", "impact_parameter_error"),
                        ("k", "k_error"),
                    ],
                    "description": (
                        "<i>Parameters of the planet.</br /> Specify parameter "
                        "uncertainty in a form of an array, e.g. '[0.1,0.2]' for a\n"
                        "value<sub>-0.1</sub><sup>+0.2</sup> or simply '[0.1]' for "
                        "value&#177;0.1. Use nan and inf for special cases of upper/"
                        "lower limits and undefined value.</i>"
                    ),
                },
            ),
            FieldSet(
                name="Atmospheric data",
                field_options={
                    "classes": ["collapse", "wide"],
                    "fields": [
                        (
                            "temp_calculated",
                            "temp_calculated_error",
                        ),
                        (
                            "temp_measured",
                            "temp_measured_error",
                        ),
                        ("hot_point_lon",),
                        ("albedo", "albedo_error"),
                    ],
                    "description": "<i>Various data related to planet atmosphere</i>",
                },
            ),
            FieldSet(
                name="Additional information",
                field_options={
                    "classes": ["collapse", "wide"],
                    "fields": [("remarks",), ("other_web",)],
                    "description": "<i>Remarks, links to other websites</i>",
                },
            ),
        ),
    )

    # TODO add typing
    # TODO add docstring
    def save_formset(
        self,
        request: HttpRequest,
        form: forms.ModelForm,
        formset: admin.TabularInline,
        change: bool,
    ) -> None:
        # `.save` and `delete_objects` is inderictly inherited form a django model.
        instances = formset.save(commit=False)  # type: ignore [attr-defined]
        for obj in formset.deleted_objects:  # type: ignore [attr-defined]
            obj.delete()
        for instance in instances:
            # Pass if an other star is the same as the main star
            if (
                isinstance(instance, Planet2Star)
                and len(
                    # pylint: disable-next=no-member
                    Planet2Star.objects.filter(
                        # Planet2Star attributes `star_id` and `planet_id` are automatic
                        # attributes from django model (Django == 💩).
                        star_id=instance.star_id,  # type: ignore [attr-defined]
                        planet_id=instance.planet_id,  # type: ignore [attr-defined]
                    )
                )
                > 0
            ):
                LOGGER.info(
                    # Planet2Star attributes `star_id` are automatic attributes from
                    # django model (Django == 💩).
                    "A planetary system with: star_id:"
                    f"{instance.star_id} and "  # type: ignore [attr-defined]
                    f"planet_id:{instance.planet_id} "
                    "already exist. We just ignored this add."
                )
            elif isinstance(instance, Planet2Star):
                # Raise an error and remove planet if planet have a planetary system
                planet = form.instance
                if planet.planetary_system:
                    planet.delete()
                    raise forms.ValidationError(
                        "You can't have both a planetary system and a star."
                    )
                instance.save()
            else:
                instance.save()
        # `.save_m2m` is inderictly inherited form a django model.
        formset.save_m2m()  # type: ignore [attr-defined]

    # TODO add typing
    # TODO add docstring
    def save_model(
        self,
        request: HttpRequest,
        planet_obj: Planet,
        form: forms.ModelForm,
        change: bool,
    ) -> None:
        """
        Given a model instance save it to the database.
        """
        if not cast(AnonymousUser, request.user).is_superuser and cast(
            AnonymousUser, request.user
        ).has_perm("app.suggest_planets"):
            planet_obj.status = WebStatus.SUGGESTED

        main_star_id = form.cleaned_data.get("main_star", None)
        if main_star_id:
            planet_obj.main_star = main_star_id

        super().save_model(request, planet_obj, form, change)

        # Add a planet2star if we have a main_star_id and if the
        # link does not already exist.
        if (
            main_star_id
            and len(
                # pylint: disable-next=no-member
                Planet2Star.objects.filter(
                    planet_id=planet_obj.id, star_id=main_star_id.id
                )
            )
            < 1
        ):
            new_pla2st = Planet2Star(planet_id=planet_obj.id, star_id=main_star_id.id)
            new_pla2st.save()


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class StarAdminForm(forms.ModelForm):
    # TODO add typing
    # TODO add docstring
    # pylint: disable-next=missing-function-docstring
    def to_python(self, value: Any) -> Any:
        return value

    # TODO add typing
    # TODO add docstring
    def clean_name(self) -> str:
        """
        This is proof of concept for cleaning and validating `name` field of
        the Planet.
        """
        self._validate_unique = True
        value = self.cleaned_data["name"]
        if "_" in value:
            raise forms.ValidationError("Invalid character _")

        # name with alternate name
        name_alreay_used, used_by = star_name_already_used_as_alternate_name(value)
        if name_alreay_used:
            raise forms.ValidationError(
                f"Name: {value} already used as an alternate name " f"by {used_by}."
            )

        return cast(str, value)

    def clean_dec(self) -> Decimal:
        """
        Clean DEC value and convert it in degrees if given in dd:mm:ss.

        Returns:
            Cleaned DEC value.
        """
        try:
            new_dec = dec_string_to_degree_value(self.cleaned_data["dec"])
        except ValidationError as err:
            raise forms.ValidationError(
                err.message
                if err.message is not None
                else "Unknown error, please contact an administrator"
            ) from err

        return new_dec

    def clean_ra(self) -> Decimal:
        """
        Clean RA value and convert it in degrees if given in hh:mm:ss.

        Returns:
            Cleaned RA value.
        """
        try:
            new_ra = ra_string_to_degree_value(self.cleaned_data["ra"])
        except ValidationError as err:
            raise forms.ValidationError(
                err.message
                if err.message is not None
                else "Unknown error, please contact an administrator"
            ) from err

        return new_ra

    # TODO add typing
    # TODO add docstring
    def clean(self) -> dict[str, Any]:
        if self.cleaned_data["force"]:
            return self.cleaned_data

        if "ra" in self.cleaned_data and "dec" in self.cleaned_data:
            # pylint: disable=line-too-long
            # https://stackoverflow.com/questions/52481773/how-to-detect-if-obj-is-being-added-or-edited-inside-modelform-clean

            is_at_same_pos, raise_msg = is_at_the_same_position_of_other_star(
                Decimal(self.cleaned_data["ra"]),
                Decimal(self.cleaned_data["dec"]),
                star_id=self.instance.pk,
            )

            if is_at_same_pos:
                raise forms.ValidationError(
                    f"{raise_msg} If you want to add it anyway, "
                    "check the force star validation."
                )

        return self.cleaned_data

    force = forms.BooleanField(
        label="Force star coordinates validation", required=False
    )


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class StarAdmin(ExoplanetModelAdmin):
    form = StarAdminForm
    list_display = ("id", "name", "ra_sex", "dec_sex", "modified", "_status")
    list_filter = ("status",)
    ordering = ("-modified",)
    search_fields = ["name"]
    date_hierarchy = "modified"
    readonly_fields = ("modified", "get_planets")
    inlines = (StarPublicationInline, AlternateStarNamesInline)
    fieldsets = cast(
        tuple,
        (
            FieldSet(
                name="Star identification details",
                field_options={
                    "fields": [
                        ("name", "get_planets"),
                        ("ra", "dec", "force", "spec_type"),
                        ("modified", "status"),
                    ],
                    "description": "<i>Main star data</i>",
                },
            ),
            FieldSet(
                name="Physical parameters",
                field_options={
                    "classes": ["collapse", "wide"],
                    "fields": [
                        ("radius", "radius_error"),
                        ("mass", "mass_error"),
                        ("age", "age_error"),
                        ("distance", "distance_error"),
                        ("teff", "teff_error"),
                        ("metallicity", "metallicity_error"),
                        (
                            "magnitude_v",
                            "magnitude_i",
                            "magnitude_j",
                            "magnitude_h",
                            "magnitude_k",
                        ),
                        ("radvel_proj", "detected_disc", "magnetic_field"),
                    ],
                    "description": (
                        "<i>Parameters of the star.</br /> Specify parameter "
                        "uncertainty in a form of an array, e.g. '[0.1,0.2]' for a\n"
                        "value<sub>-0.1</sub><sup>+0.2</sup> or simply '[0.1]' for "
                        "value&#177;0.1. Use nan and inf for special cases of upper/"
                        "lower limits and undefined value.</i>"
                    ),
                },
            ),
            FieldSet(
                name="Additional information",
                field_options={
                    "classes": ["collapse", "wide"],
                    "fields": [
                        ("remarks",),
                        ("other_web",),
                        ("url_simbad",),
                    ],
                    "description": "<i>Remarks, links to other websites</i>",
                },
            ),
        ),
    )

    # TODO add typing
    # TODO add docstring
    # pylint: disable-next=missing-function-docstring
    def get_planets(self, star_obj: Star) -> str:
        planet_name_list = [
            planet.name
            # star_obj has an automatic attributte add by django with the
            # foreign key Field (Django == 💩)
            for planet in star_obj.planets.all()  # type: ignore[attr-defined]
        ]
        return ", ".join(planet_name_list)

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    get_planets.short_description = "Planets"  # type: ignore[attr-defined]

    # TODO add typing
    # TODO improve docstring
    def save_model(
        self,
        request: HttpRequest,
        star_obj: Star,
        form: forms.ModelForm,
        change: bool,
    ) -> None:
        """
        Given a model instance save it to the database.
        """
        if not cast(AnonymousUser, request.user).is_superuser and cast(
            AnonymousUser, request.user
        ).has_perm("app.suggest_stars"):
            star_obj.status = WebStatus.SUGGESTED
        force = form.cleaned_data["force"]
        # HACK in django 1.x we were using `save(force=force)`
        star_obj.save(force_update=force)


# TODO add docstring
# pylint: disable-next=missing-class-docstring
class PlanetarySystemAdminForm(forms.ModelForm):
    def clean_dec(self) -> Decimal:
        """
        Clean DEC value and convert it in degrees if given in dd:mm:ss.

        Returns:
            Cleaned DEC value.
        """
        try:
            new_dec = dec_string_to_degree_value(self.cleaned_data["dec"])
        except ValidationError as err:
            raise forms.ValidationError(
                err.message
                if err.message is not None
                else "Unknown error, please contact an administrator"
            ) from err

        return new_dec

    def clean_ra(self) -> Decimal:
        """
        Clean RA value and convert it in degrees if given in hh:mm:ss.

        Returns:
            Cleaned RA value.
        """
        try:
            new_ra = ra_string_to_degree_value(self.cleaned_data["ra"])
        except ValidationError as err:
            raise forms.ValidationError(
                err.message
                if err.message is not None
                else "Unknown error, please contact an administrator"
            ) from err

        return new_ra


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class PlanetarySystemAdmin(ExoplanetModelAdmin):
    form = PlanetarySystemAdminForm
    list_display = ("id", "ra", "dec", "distance")
    search_fields = ["planet__name"]


class PublicationAdminForm(forms.ModelForm):
    """Publication admin form."""

    def to_python(self, value: Any) -> Any:
        """
        Convert a value in it's python version.

        Args:
            value: Value to convert.

        Returns:
            Converted value.
        """
        return value

    def clean(self) -> dict[str, Any]:
        """
        Clean data before saving it in the db.

        Returns:
            Cleaned data.
        """
        _min_year = 1700
        _max_year = datetime.now().year + 1

        year_date = self.cleaned_data.get("date", None)
        if year_date is not None:
            try:
                date = datetime.strptime(f"01-01-{year_date}", "%m-%d-%Y").date()
            except ValueError as err:
                raise forms.ValidationError(
                    f"Wrong date: {year_date}. Given date must be a year"
                ) from err

            if not _min_year <= date.year <= _max_year:
                raise forms.ValidationError(
                    f"The given date <{year_date}> must be between {_min_year} and "
                    f"{_max_year}"
                )

        return self.cleaned_data


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class PublicationAdmin(ExoplanetModelAdmin):
    form = PublicationAdminForm
    list_display = (
        "id",
        "title",
        "author",
        "type",
        "journal_name",
        "date",
        "modified",
        "is_visible",
    )
    list_filter = ("status", "date")
    search_fields = ["title", "author", "journal_name", "date"]
    date_hierarchy = "modified"
    inlines = (PlanetPublication2Inline,)


# TODO add typing
# TODO add docstring
# pylint: disable-next=missing-class-docstring
class MoleculeAdmin(ExoplanetModelAdmin):
    list_display = ("id", "name", "modified", "is_visible")
    list_filter = ("status",)
    search_fields = ["name"]
    date_hierarchy = "modified"


class LogEntryAdmin(admin.ModelAdmin):
    """Log Entries admin."""

    date_hierarchy = "action_time"
    "Parameter to define date hierarchy."

    list_filter = ["user", "content_type", "action_flag"]
    "Field that can be filtered."

    search_fields = ["object_repr", "change_message"]
    "Searchable fields."

    list_display = [
        "action_time",
        "user",
        "content_type",
        "object_link",
        "action_flag",
        "change_message",
    ]
    "Field displayed in list view."

    def has_add_permission(self, request: HttpRequest) -> bool:
        """
        Define if an entry can be added.

        Args:
            request: The http request.

        Returns:
            True if an entry can be added. False otherwise.
        """
        return False

    def has_change_permission(
        self,
        request: HttpRequest,
        obj: LogEntry | None = None,
    ) -> bool:
        """
        Define if an entry can be modified.

        Args:
            request: The http request.

        Returns:
            True if an entry can be modified. False otherwise.
        """
        return False

    def has_delete_permission(
        self,
        request: HttpRequest,
        obj: LogEntry | None = None,
    ) -> bool:
        """
        Define if an entry can be deleted.

        Args:
            request: The http request.

        Returns:
            True if an entry can be deleted. False otherwise.
        """
        return False

    def object_link(self, obj: LogEntry) -> str:
        """
        Get an object's link:

        Args:
            obj: Object to get the link.

        Returns:
            Link to the object.
        """
        if obj.action_flag == DELETION:
            link = str(escape(obj.object_repr))
        else:
            content_type = obj.content_type
            view_name = reverse(
                f"admin:{content_type.app_label}_{content_type.model}_change",
                args=[obj.object_id],
            )
            link = f'<a href="{view_name}">{escape(obj.object_repr)}</a>'

        return link

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    object_link.allow_tags = True  # type: ignore[attr-defined]
    object_link.admin_order_field = "object_repr"  # type: ignore[attr-defined]
    object_link.short_description = "object"  # type: ignore[attr-defined]

    def queryset(self, request: HttpRequest) -> QuerySet[LogEntry]:
        """
        Get the queryset to display entries.

        Args:
            request: The http request.

        Returns:
            The Queryset to display entries
        """
        return cast(
            QuerySet[LogEntry],
            super(LogEntryAdmin, self)  # pylint: disable=no-member
            # From a django Model, it's inherite from an another class.
            .queryset(request).prefetch_related("content_type"),  # type: ignore[misc]
        )


# Admin page registering ###########################################################
# TODO replace all this by use of @admin.register decorator on each class
# See: https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#the-register-decorator

admin.site.register(Planet, PlanetAdmin)
admin.site.register(Star, StarAdmin)
admin.site.register(PlanetarySystem, PlanetarySystemAdmin)
admin.site.register(Publication, PublicationAdmin)
admin.site.register(Molecule, MoleculeAdmin)
admin.site.register(LogEntry, LogEntryAdmin)

# TODO find why this doesn't work: KeyError: 'delete_selected'
# admin.site.disable_action("delete_selected")
