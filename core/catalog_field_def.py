"""
Detailed definitions of all the catalog fields and group of fields.

For each field we define here all the informations needed to display them correctly in
an HTML environnement or to export them to a Virtual Observatory (VO) server/format.
"""

# Standard imports
# Standard library imports
from dataclasses import dataclass, field
from functools import partial
from pathlib import Path
from typing import Callable, Literal, TypedDict, cast

# External imports
import yaml

# Local imports
from .models import choices


# Type Definitions
class ValDepOnMassUnit(TypedDict):
    """A value that has 2 possible values depending on the chosen mass unit."""

    # Value if the the unit is in multiple of Mjup
    mjup: str

    # Value if the the unit is in multiple of Mearth
    mearth: str


class ValDepOnRadiusUnit(TypedDict):
    """A value that has 2 possible values depending on the chosen radius unit."""

    # Value if the the unit is in multiple of Rjup
    rjup: str

    # Value if the the unit is in multiple of Rearth
    rearth: str


MassUnit = Literal["mjup", "mearth"]
RadiusUnit = Literal["rjup", "rearth"]


# TODO replace the whole dict based system by clean YAML serializable class with
# intialisation of all the value at init time instead of computation via accessors
# through the proxy class Column. The current situation is pure madness!!!
class FieldDefinition(TypedDict, total=False):
    """Dictionnary used to have somewhat strong typing of field def YAML data."""

    id: str
    type: str
    desc: str
    catalog_model_property: str
    catalog_model_property_error: str
    model_property: str
    model_property_error: str
    default_unit: MassUnit | RadiusUnit
    model_field: str | ValDepOnMassUnit | ValDepOnRadiusUnit
    error: str | ValDepOnMassUnit | ValDepOnRadiusUnit
    error_desc: str
    filter_name: str

    related_model: str
    related_model_field: str
    related_model_field_error: str

    # New templates use only header_name and header_unit to build a valid header for the
    # column of the table
    # --> catalog_header is for temporary backward compatibility only
    catalog_header: str | ValDepOnMassUnit | ValDepOnRadiusUnit
    header_name: str
    header_unit: str | ValDepOnMassUnit | ValDepOnRadiusUnit
    table_property: str
    table_model: str
    table_error_property: str

    html_unit: str
    file_header: str
    file_header_error: str
    file_model_property: str
    planet_output_header: str

    votable_datatype: str
    votable_arraysize: str
    votable_unit: str
    votable_ucd: str

    diagram_x_default: bool
    diagram_y_default: bool
    catalog_information: bool
    reverse_model_property: Callable


# Useful typedef
FieldDefinitionGroup = list[FieldDefinition]

# Load the field definitions from file
YAML_PATH = Path(__file__).parent / "field_definitions.yaml"
with YAML_PATH.open("r", encoding="utf-8") as yaml_file:
    _field_definitions: dict = yaml.full_load(yaml_file)

# Small adjustement since we can not use a function object in YAML
PARTIAL_REVERSE = partial(choices.value_from_approx_name, choices.AllDetection)
_field_definitions["DETECTION_TYPE_FIELD"]["reverse_model_property"] = PARTIAL_REVERSE

# We add all the elements to the globals of the module make them appear as variables
globals().update(_field_definitions)


def is_multi_unit(field_def: FieldDefinition) -> bool:
    """Tell if a field can have different units."""
    header = field_def.get("header_unit", "")
    return isinstance(header, dict)


@dataclass
class Column:
    """
    A field representing the header of a specific column of values in a DataTable table.
    With a specific unit used.

    It provides convenient accessor for all importent data of the field description that
    gives revelent values whatever the way they were defined in the FieldDefinition
    used.
    """

    # TODO rename this to field_desc because at the moment it's misleading
    # Description of the field properties
    field_def: FieldDefinition

    # Unit to use to display that field
    unit: str | None = field(default=None)

    def __post_init__(self) -> None:
        """Set the good unit for multi unit field."""
        # Do we need specify a unit ? aka do we have a default unit ?
        if is_multi_unit(self.field_def) and not self.unit:
            self.unit = self.field_def["default_unit"]

    def get_model_field(self) -> str:
        """
        Get the name of the corresponding model field name in the db.

        If relevant, it takes in account the fact that the model field depends on the
        unit.

        Returns:
            The model field name

        Raises:
            ValueError: if the field definition is not coherent in any way
        """
        model_field = self.field_def["model_field"]

        if self.unit is None:
            match model_field:
                case str():
                    return model_field
                case dict():
                    raise ValueError(
                        "Model field has a dict value but the field_definition has no "
                        "unit. Those are not possible at the same time."
                    )
                case _:
                    raise ValueError("Wrong type for model field")

        match model_field:
            case {"mjup": _, "mearth": _}:
                mass_model_field = cast(ValDepOnMassUnit, model_field)
                mass_unit = cast(MassUnit, self.unit or self.field_def["default_unit"])
                return mass_model_field[mass_unit]
            case {"rjup": _, "rearth": _}:
                radius_model_field = cast(ValDepOnRadiusUnit, model_field)
                radius_unit = cast(
                    RadiusUnit, self.unit or self.field_def["default_unit"]
                )
                return radius_model_field[radius_unit]
            case str():
                raise ValueError(
                    "Model field has a string value but the field_definition has a "
                    "unit. Those are not possible at the same time."
                )
            case _:
                raise ValueError("Wrong type for model field")

    def get_error(self) -> str | None:
        """
        Get the name of the corresponding model field name for the error in the db.

        If relevant, it takes in account the fact that the model field error depends on
        the unit.

        Returns:
            The model field name the the error if it exists else None

        Raises:
            ValueError: if the field definition is not coherent in any way
        """
        if "error" not in self.field_def:
            return None

        model_error = self.field_def["error"]

        if self.unit is None:
            match model_error:
                case str():
                    return model_error
                case dict():
                    raise ValueError(
                        "Model error has a dict value but the field_definition has no "
                        "unit. Those are not possible at the same time."
                    )
                case _:
                    raise ValueError("Wrong type for model error")

        match model_error:
            case {"mjup": _, "mearth": _}:
                mass_model_error = cast(ValDepOnMassUnit, model_error)
                mass_unit = cast(MassUnit, self.unit or self.field_def["default_unit"])
                return mass_model_error[mass_unit]
            case {"rjup": _, "rearth": _}:
                radius_model_error = cast(ValDepOnRadiusUnit, model_error)
                radius_unit = cast(
                    RadiusUnit, self.unit or self.field_def["default_unit"]
                )
                return radius_model_error[radius_unit]
            case str():
                raise ValueError(
                    "Model field error has a string value but the field_definition has "
                    "a unit. Those are not possible at the same time."
                )
            case _:
                raise ValueError("Wrong type for model field error")

    def get_error_desc(self) -> str:
        """Description of the error of this field."""
        return self.field_def.get("error_desc", self.get_desc())

    # TODO find if this function do something usefull
    def get_error_ucd(self) -> str:
        """
        Not sure if this function does what it should do.

        but nobody complained so far
        """
        ucd = self.get_votable_ucd()

        if ucd == "":
            return "stat.error"

        return f"stat.error;{ucd}"

    def get_id(self) -> str:
        """
        Get a unique identifier for the column.

        Returns:
            Either the actual id, or related model id or model field id
        """
        if "id" in self.field_def:
            return self.field_def["id"]

        if "related_model_field" in self.field_def:
            return self.field_def["related_model_field"]

        return self.get_model_field()

    def get_votable_datatype(self) -> str:
        """Type of the field expressed as a VOtable type."""
        return self.field_def.get("votable_datatype", "double")

    def get_votable_arraysize(self) -> str | None:
        """Size of the data for array or string in VOtable standards."""
        return self.field_def.get("votable_arraysize", None)

    def get_votable_unit(self) -> str:
        """Unit of the field in a form compatible with VOtable standards."""
        return self.field_def.get("votable_unit", "")

    def get_votable_ucd(self) -> str:
        """Name of the field in the votable UCD format."""
        return self.field_def.get("votable_ucd", "")

    def get_desc(self) -> str:
        """Description of the field."""
        return self.field_def.get("desc", "")

    def get_queryset_value_field(self) -> tuple[str, str | None] | tuple[str]:
        """Return a tuple containing the name of the actual field storing value & err"""
        if "error" in self.field_def:
            return (self.get_model_field(), self.get_error())

        return (self.get_model_field(),)

    def get_value_field(self) -> str | None:
        """Name of the actual field storing the value in the model."""
        if "related_model" in self.field_def:
            return self.field_def["related_model_field"]

        return self.get_model_field()

    def get_value_field_error(self) -> str | None:
        """The name of the err associated to the field (eventually on another model)."""
        if "related_model" in self.field_def:
            return self.field_def.get("related_model_field_error", None)

        return self.get_error()

    def get_model_property(self) -> str:
        """Name of the property for this field in the fucking planetdbview from hell."""
        return self.field_def.get("model_property", self.get_model_field())

    def get_table_property(self) -> str:
        """Name of the property for this field in the actual model."""
        return self.field_def.get("table_property", self.get_model_property())

    def get_table_model(self) -> str:
        """
        Name of the actual column in the related model that store the information.

        This only exists for field that are in fact on a related table.
        """
        return self.field_def.get("table_model", self.get_table_property())

    def get_table_error_property(self) -> str:
        """
        Get the table_error_property name if it exist else the name of the field
        returned by get_error() and if all of this fail it returns an empty string.
        """
        return self.field_def.get("table_error_property", self.get_error()) or ""

    def get_html_unit(self) -> str | None:
        """
        Unit for the field as it could be used outside of the catalog headers.

        This only exists because there has been an horrible mess between the units
        written in the headers of the catalog and the units used in the Planet Details
        page
        """
        return self.field_def.get("html_unit", None)

    def get_raw_catalog_header(self) -> str:
        """
        Get the header of the field without any unit or link.

        Just the raw name. This is almost the same as `get_header_name()` method except
        it returns an empty string when there is no headewr provided instead of the
        "no header" string.

        This method was created to circunvent the weird way we were handling
        headers and units before r1134.
        """
        return self.field_def.get("header_name", "")

    def get_unit_header(self) -> str:
        """
        Get the unit of the field or None if not applicable.

        The unit could be pure text or an HTML link if needed (for exemple to
        allow the user to change the unit).

        This method was created to circunvent the weird way we were handling
        headers and units before r1134.

        Returns:
            the unit of the field as text or an empty string
        """
        if "header_unit" not in self.field_def:
            return ""

        header_unit = self.field_def["header_unit"]

        if self.unit is None:
            match header_unit:
                case str():
                    return header_unit
                case dict():
                    raise ValueError(
                        "header unit has a dict value but the field_definition has no "
                        "unit. Those are not possible at the same time."
                    )
                case _:
                    raise ValueError("Wrong type for header unit")

        match header_unit:
            case {"mjup": _, "mearth": _}:
                mass_header_unit = cast(ValDepOnMassUnit, header_unit)
                mass_unit = cast(MassUnit, self.unit)
                return mass_header_unit[mass_unit]
            case {"rjup": _, "rearth": _}:
                radius_header_unit = cast(ValDepOnRadiusUnit, header_unit)
                radius_unit = cast(RadiusUnit, self.unit)
                return radius_header_unit[radius_unit]
            case str():
                raise ValueError(
                    "Header unit is a string value but the field_definition has a "
                    "unit. Those are not possible at the same time."
                )
            case _:
                raise ValueError("Wrong type for header unit")

    def get_catalog_header(self) -> str:
        """
        Build the actual header used in the catalog for this field (title+unit).

        If there is a unit the header will be on 2 lines with an explicit cariage return
        between the name of the field and the unit.
        """

        txt = self.get_raw_catalog_header()
        link = self.get_unit_header()

        if link:
            return f"{txt}<br>{link}"

        return txt

    def get_planet_output_header(self) -> str:
        """Label for the field as it mwill appear in the Planet Detail poge."""
        return self.field_def.get("planet_output_header", self.get_catalog_header())

    def get_type(self) -> str | None:
        """Type of the field: number or string."""
        return self.field_def.get("type", None)

    def get_header_name(self) -> str:
        """
        Header for this field to use in the catalog (it does not include the unit).

        It may containe mathML or HTML.
        """
        return self.field_def.get("header_name", "no header")

    def get_related_model(self) -> str | None:
        """
        Name of the model actualy storing the value of this field.

        This is used only for field belonging to a related model.
        """
        return self.field_def.get("related_model", None)

    def get_catalog_model_property(self) -> str:
        """Name of the property to use to have a human readable version of the field."""
        if "catalog_model_property" in self.field_def:
            return self.field_def["catalog_model_property"]

        if "model_property" in self.field_def:
            return self.field_def["model_property"]

        return self.get_model_field()

    def get_catalog_error_property(
        self,
    ) -> str | ValDepOnMassUnit | ValDepOnRadiusUnit | None:
        """Field storing the error associated to this field."""
        if "catalog_model_property_error" in self.field_def:
            return self.field_def["catalog_model_property_error"]

        if "model_property_error" in self.field_def:
            return self.field_def["model_property_error"]

        if "error" in self.field_def:
            return self.get_error()

        return None

    def get_filter_name(self) -> str:
        """
        Name to use for for this field in the minilangage used to filter catalog.

        See: core.filter_grammar
        """
        if "filter_name" in self.field_def:
            return self.field_def["filter_name"]

        if "file_header" in self.field_def:
            return self.field_def["file_header"]

        return self.get_model_field()

    def get_file_header(self) -> str:
        """Name to use for the column header for this field in VOtable export."""
        return self.field_def.get("file_header", self.get_model_field())

    def get_file_header_error(self) -> str:
        """
        Name to use for the column header of the error for this field in VOtable export.
        """
        return self.field_def.get(
            "file_header_error", f"{self.get_file_header()}_error"
        )

    def get_file_model_property(self) -> str:
        """Name to use for the column header in the CSV and VOtable export."""
        return self.field_def.get("file_model_property", self.get_model_property())

    def get_reverse_model_property(self) -> Callable | None:
        """
        Foreign key opf the target model that point to this one.

        If the field is a foreign key to another model this returns the foreigh key
        property of the other model that point back to this one (if it exist).
        """
        return self.field_def.get("reverse_model_property", None)


# TODO create this dict once and for all as a const of the module
def get_columns_by_id() -> dict[str, Column]:
    """
    Return all the field instance generated from the provided list of field
    template as a dictionnary referenced by there ids.

    Returns:
        a dict from field id to corresponding actual Column instance

    Raises:
        ValueError: if the column can not be created from the field description for any
            reason or if no id could be generated for the column.
    """
    id_to_column = {}

    # We use globals() to access CATALOG_ALL_FIELDS since it is technically out of scope
    # because we added ourselves to the global scope after loading it from a YAML file
    for fld in globals()["CATALOG_ALL_FIELDS"]:
        actual_field = Column(fld)
        if actual_field is None:
            raise ValueError(
                f"no Column instance could be created from field description {fld}"
            )

        actual_id = actual_field.get_id()

        if actual_id is None:
            raise ValueError(f"no id could be generated from field {actual_field}")

        id_to_column[actual_id] = actual_field

    return id_to_column


def get_nb_columns_including_errors_col(catalog: FieldDefinitionGroup) -> int:
    """
    Returns the number of column defined by a FieldDefinitionGroup but counting every
    column that has an error field as 3 columns : value, err_min, err_max.
    """
    nb_col = 0
    for col in catalog:
        if "error" in col:
            nb_col = nb_col + 3
        else:
            nb_col = nb_col + 1
    return nb_col
