/*
This is the old version of the function as it was defined in migration 6
*/
CREATE OR REPLACE FUNCTION get_mass(
    mass_detected_string character varying,
    mass_detected_unit smallint,
    mass_sini_string character varying,
    mass_sini_unit smallint,
    inclination double precision,
    result_unit smallint
) RETURNS double precision AS $BODY$
DECLARE
  mass_detected double precision =  cast(mass_detected_string as double precision);
  mass_sini double precision = cast(mass_sini_string as double precision);
  mass double precision;
  significant_digits_nb integer;
   BEGIN
  IF mass_detected IS NOT NULL THEN
    significant_digits_nb = get_significant_digits_nb(mass_detected_string);
    mass = round_multi_units_value(
      mass_detected,
      significant_digits_nb,
      mass_detected_unit,
      'mass'::character varying,
      result_unit
    );
  ELSE
    IF mass_sini IS NOT NULL THEN
      significant_digits_nb = get_significant_digits_nb(mass_sini_string);
      IF inclination IS NOT NULL AND sin(radians(inclination)) != 0 THEN
        mass = cast(
          mass_sini_string as double precision) / abs(sin(radians(inclination))
        );
        mass = round_multi_units_value(
          mass,
          significant_digits_nb,
          mass_sini_unit,
          'mass'::character varying,
          result_unit
        );
      ELSE
        mass = round_multi_units_value(
          mass_sini,
          significant_digits_nb,
          mass_sini_unit,
          'mass'::character varying,
          result_unit
        );
      END IF;
    ELSE
      mass = NULL;
    END IF;
  END IF;
  RETURN mass;
   END;
 $BODY$ LANGUAGE plpgsql VOLATILE COST 100;
