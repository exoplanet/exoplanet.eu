ALTER TABLE public."core_planetdbview"
RENAME COLUMN mass_detection_type TO mass_measurement_type;

ALTER TABLE public."core_planetdbview"
RENAME COLUMN radius_detection_type TO radius_measurement_type;
