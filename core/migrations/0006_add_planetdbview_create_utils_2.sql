/*
We had to split the SQL file in multiple parts to prevent lint parser
(sqlfluff) to crash. It is also a bad idea to have too big files anyway.

First We create the utils functions used to create or update the view (second
part).
*/

CREATE OR REPLACE FUNCTION get_mass_sini_limits(
  mass_sini double precision,
  mass_sini_error double precision[]
) RETURNS double precision[] AS $BODY$
DECLARE
  mass_sini_error_min double precision;
  mass_sini_error_max double precision;
  mass_sini_min double precision;
  mass_sini_max double precision;
  mass_sini_minmax double precision [2];
BEGIN
  IF mass_sini_error IS NOT NULL THEN
    mass_sini_error = get_error_min_max(mass_sini_error);
    mass_sini_error_min = mass_sini_error[1];
    mass_sini_error_max = mass_sini_error[2];
    IF mass_sini_error_min = 'Infinity' THEN
      mass_sini_min = 0;
    ELSIF mass_sini_error_min = 'NaN' THEN
      mass_sini_min = mass_sini;
    ELSE
      mass_sini_min = mass_sini - mass_sini_error_min;
    END IF;
    IF mass_sini_error_max = 'NaN' THEN
      mass_sini_max = mass_sini;
    ELSIF mass_sini_error_max = 'Infinity' THEN
      mass_sini_max = 'Infinity';
    ELSE
      mass_sini_max = mass_sini + mass_sini_error_max;
    END IF;
  ELSE
    mass_sini_min = mass_sini;
    mass_sini_max = mass_sini;
  END IF;
  mass_sini_minmax [1] = mass_sini_min;
  mass_sini_minmax [2] = mass_sini_max;
  RETURN mass_sini_minmax;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_multi_units_value(
  value_string character varying,
  value_string_unit smallint,
  value_type character varying,
  result_unit smallint
) RETURNS double precision AS $BODY$
DECLARE
  value_double double precision;
  significant_digits_nb integer;
  result_significant_digits_nb integer;
  conversion_coef double precision;
BEGIN
  IF value_string IS NULL OR value_string = '' THEN
    RETURN NULL;
  ELSE
    value_double = cast(value_string as double precision);
    IF value_string_unit = result_unit THEN
      RETURN value_double;
    ELSE
      significant_digits_nb = get_significant_digits_nb(value_string);
      RETURN round_multi_units_value(
        value_double,
        significant_digits_nb,
        value_string_unit,
        value_type,
        result_unit
      );
    END IF;
  END IF;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_multi_units_value_error(
  value_error_string_array character varying[],
  value_string_unit smallint,
  value_type character varying,
  result_unit smallint
) RETURNS double precision[] AS $BODY$
DECLARE
  value_error_array double precision[];
  value_error character varying;
  value_error_converted double precision[];
BEGIN
  value_error_array = get_error_array(value_error_string_array);
  IF value_error_array IS NULL THEN
    RETURN NULL;
  ELSE
    FOREACH value_error IN ARRAY value_error_array
    LOOP
      value_error_converted = array_append(
        value_error_converted,
        get_multi_units_value(
          value_error,
          value_string_unit,
          value_type,
          result_unit
        )
      );
    END LOOP;
    RETURN value_error_converted;
  END IF;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_planet_status_string(
  planet_status smallint
) RETURNS character varying AS $BODY$
BEGIN
  CASE
    WHEN planet_status = 1 THEN
      RETURN 'Confirmed';
    WHEN planet_status = 2 THEN
      RETURN 'Candidate';
    WHEN planet_status = 3 THEN
      RETURN 'Controversial';
    WHEN planet_status = 4 THEN
      RETURN 'Unconfirmed';
    WHEN planet_status = 5 THEN
      RETURN 'Retracted';
    ELSE
      RETURN NULL;
  END CASE;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_publication_status_letter(
  publication_status smallint
) RETURNS character AS $BODY$
BEGIN
  CASE
    WHEN publication_status = 1 THEN
      RETURN 'R';
    WHEN publication_status = 2 THEN
      RETURN 'S';
    WHEN publication_status = 3 THEN
      RETURN 'C';
    WHEN publication_status = 4 THEN
      RETURN 'W';
    ELSE
      RETURN NULL;
  END CASE;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_significant_digits_nb(
  string_value character varying
) RETURNS integer AS $BODY$
DECLARE
  significant_digits_nb integer;
BEGIN
  significant_digits_nb = char_length(split_part(string_value, '.', 2));
  RETURN significant_digits_nb;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


/*
This function has an equivalent in the python code: in the core/models/planet
module you have the planet.is_mass_from_mass_sini() method
*/
CREATE OR REPLACE FUNCTION is_mass_from_mass_sini(
  mass_detected_string character varying,
  mass_sini_string character varying
) RETURNS boolean AS $BODY$
DECLARE
  mass_detected double precision =  cast(
    mass_detected_string as double precision
  );
  mass_sini double precision = cast(mass_sini_string as double precision);
BEGIN
  IF mass_detected IS NULL AND mass_sini IS NOT NULL THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION mass_earth_constant()
RETURNS double precision AS $BODY$
DECLARE
  MASS_earth CONSTANT double precision := 5.97219e+24;
BEGIN
  RETURN MASS_earth;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION mass_earth_unit_constant()
RETURNS smallint AS $BODY$
DECLARE
  MASS_EARTH_UNIT CONSTANT smallint := 2;
BEGIN
  RETURN MASS_EARTH_UNIT;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION mass_jup_constant()
RETURNS double precision AS $BODY$
DECLARE
  MASS_JUP CONSTANT double precision := 1.89813e+27;
BEGIN
  RETURN MASS_JUP;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION mass_jup_unit_constant()
RETURNS smallint AS $BODY$
DECLARE
  MASS_JUP_UNIT CONSTANT smallint := 1;
BEGIN
  RETURN MASS_JUP_UNIT;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION planet_has_star(
  current_planet_id integer
) RETURNS boolean AS $BODY$
DECLARE
  stars_number integer;
BEGIN
  stars_number := (
    SELECT COUNT(*)
    FROM core_planet2star
    WHERE core_planet2star.planet_id=current_planet_id AND star_id IS NOT NULL
  );
  IF stars_number > 0
    THEN RETURN true;
  ELSE
    RETURN false;
  END IF;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION radeg_2_rasex(
  ra_deg double precision
) RETURNS character varying AS $BODY$
DECLARE
  hours integer;
  minutes integer;
  seconds integer;
  mseconds integer;
  tmp double precision;
BEGIN
  -- put ra_deg in rounded ms
  --  87.1416667
  tmp = round(ra_deg *24 *60 *60 *10 /360);
  -- 209140
  tmp = tmp / 60 / 60 / 10;
  -- 5,809444444
  hours = floor(tmp);
  tmp = (tmp - hours) * 60;
  -- 48,56666664
  minutes = floor (tmp);
  tmp = (tmp - minutes) * 60;
  seconds = floor(tmp);
  tmp = (tmp -seconds)*10;
  mseconds = round(tmp);
  IF mseconds = 10 THEN
    mseconds = 0;
    seconds = seconds + 1;
    IF seconds = 60 THEN
      seconds = 0;
      minutes = minutes + 1;
      IF minutes = 60 THEN
        minutes = 0;
        hours = hours + 1;
      END IF;
    END IF;
  END IF;

  RETURN to_char(hours, 'FM00') ||
    ':' ||
    to_char(minutes, 'FM00') ||
    ':' ||
    to_char(seconds,'FM00') ||
    '.' ||
    to_char(mseconds,'FM0');

END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION radius_earth_constant()
RETURNS double precision AS $BODY$
DECLARE
  RADIUS_EARTH CONSTANT double precision := 6378.14;
BEGIN
  RETURN RADIUS_EARTH;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION radius_earth_unit_constant()
RETURNS smallint AS $BODY$
DECLARE
  RADIUS_EARTH_UNIT CONSTANT smallint := 2;
BEGIN
  RETURN RADIUS_EARTH_UNIT;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION radius_jup_constant()
RETURNS double precision AS $BODY$
DECLARE
  RADIUS_JUP CONSTANT double precision := 71492;
BEGIN
  RETURN RADIUS_JUP;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION radius_jup_unit_constant()
RETURNS smallint AS $BODY$
DECLARE
  RADIUS_JUP_UNIT CONSTANT smallint := 1;
BEGIN
  RETURN RADIUS_JUP_UNIT;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


/*
This function has an equivalent in the python code: in the
core/helpers/conversion module you have the mass.mass_in_specific_unit() and
radius.radius_in_specific_unit()
*/
CREATE OR REPLACE FUNCTION round_multi_units_value(
  value_double double precision,
  significant_digits_nb integer,
  value_string_unit smallint,
  value_type character varying,
  result_unit smallint
) RETURNS double precision AS $BODY$
DECLARE
  result_significant_digits_nb integer;
  conversion_coef double precision;
BEGIN
  IF value_double = 'Infinity' OR value_double = 'NaN' THEN
    RETURN value_double;
  ELSE
    CASE
      WHEN value_type = 'radius' THEN
        CASE
          WHEN result_unit = radius_jup_unit_constant()
          AND value_string_unit = radius_earth_unit_constant() THEN
            result_significant_digits_nb = significant_digits_nb + 2;
            conversion_coef = radius_earth_constant() / radius_jup_constant();
          WHEN result_unit = radius_earth_unit_constant()
          AND value_string_unit = radius_jup_unit_constant() THEN
            result_significant_digits_nb = significant_digits_nb - 1;
            conversion_coef = radius_jup_constant() / radius_earth_constant();
          ELSE
            result_significant_digits_nb = significant_digits_nb;
            conversion_coef = 1;
        END CASE;
      WHEN value_type = 'mass' THEN
        CASE
          WHEN result_unit = mass_jup_unit_constant()
          AND value_string_unit = mass_earth_unit_constant() THEN
            result_significant_digits_nb = significant_digits_nb + 3;
            conversion_coef = mass_earth_constant() / mass_jup_constant();
          WHEN result_unit = mass_earth_unit_constant()
          AND value_string_unit = mass_jup_unit_constant() THEN
            result_significant_digits_nb = significant_digits_nb - 2;
            conversion_coef = mass_jup_constant() / mass_earth_constant();
          ELSE
            result_significant_digits_nb = significant_digits_nb;
            conversion_coef = 1;
        END CASE;
    END CASE;
    IF result_significant_digits_nb < 0 THEN
      result_significant_digits_nb = 0;
    END IF;
    RETURN round(cast(value_double * conversion_coef as numeric), result_significant_digits_nb);
  END IF;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;
