-- First we drop the trigger (they use everything else)

DROP TRIGGER IF EXISTS refresh_planetdbview ON core_alternatestarname;
DROP TRIGGER IF EXISTS refresh_planetdbview ON core_planetarysystem;
DROP TRIGGER IF EXISTS refresh_planetdbview ON core_planet2star;
DROP TRIGGER IF EXISTS refresh_planetdbview ON core_molecule;
DROP TRIGGER IF EXISTS refresh_planetdbview ON core_alternateplanetname;
DROP TRIGGER IF EXISTS refresh_planetdbview ON core_planet;
DROP TRIGGER IF EXISTS refresh_planetdbview ON core_star;

-- Then the associated refresh function

DROP FUNCTION IF EXISTS trig_refresh_planetdbview;
