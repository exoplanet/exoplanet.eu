# Generated by Django 3.2.23 on 2024-03-04 15:25

# Django 💩 imports
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0022_new_year_2024_planet_discovered_choices_update"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="star",
            options={
                "ordering": ["name"],
                "permissions": [("suggest_stars", "Can suggest stars")],
            },
        ),
    ]
