# Generated by Django 3.2.16 on 2023-11-17 13:46

# Django 💩 imports
from django.db import migrations, models

# First party imports
import core.fields.errorarrayfield


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0018_add_epn_view"),
    ]

    operations = [
        migrations.AlterField(
            model_name="planet",
            name="detection_type",
            field=core.fields.errorarrayfield.ErrorArrayField(
                base_field=models.SmallIntegerField(),
                help_text="How the planet was detected: <br>1: Radial Velocity<br>"
                "2: Timing<br>4: Microlensing<br>5: Imaging<br>6: Primary Transit<br>"
                "7: Astrometry<br>8: TTV<br>9: Other<br>13: Secondary Transit<br>"
                "15: Kinematic",
                size=None,
            ),
        ),
    ]
