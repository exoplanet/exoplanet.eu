# Standard imports
from pathlib import Path

# Django 💩 imports
from django.db import migrations

MIGRATION_BASE = Path(__file__).parent
RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL_FILE = (
    MIGRATION_BASE / "0027_fix_planetdbview_get_mass.sql"
)
REVERSE_RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL_FILE = (
    MIGRATION_BASE / "0027_reverse_fix_planetdbview_get_mass.sql"
)

# SQL scripts used to create the epn view
with (
    open(RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL_FILE, encoding="UTF-8") as f_init,
    open(
        REVERSE_RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL_FILE, encoding="UTF-8"
    ) as f_uninit,
):
    RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL = f_init.read()
    REVERSE_RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL = f_uninit.read()


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0026_rename_mass_and_radius_detection_to_measurement"),
    ]

    operations = [
        migrations.RunSQL(
            sql=RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL,
            reverse_sql=REVERSE_RENAME_FIX_PLANETDBVIEW_GET_MASS_SQL,
        ),
    ]
