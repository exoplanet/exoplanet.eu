CREATE OR REPLACE FUNCTION pc2km(pc_value double precision)
RETURNS double precision AS $BODY$
DECLARE
  UA_IN_KILOMETER double precision := 149597870.7;
  PARSEC_IN_KILOMETER double precision := 648000/pi() * UA_IN_KILOMETER;
  value_precision integer;
  km_value double precision;
  div integer;
  tmp integer;
BEGIN
  IF pc_value <= 0
    THEN RETURN NULL;
  ELSE
    value_precision = char_length(replace(to_char(pc_value,'FM99999999.999999999'),'.' ,''));
    km_value = pc_value * PARSEC_IN_KILOMETER;
    div = floor(log(km_value)) - value_precision + 1 ;
    tmp = round(km_value / power(10,div));
    RETURN tmp * power(10,div);
  END IF;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE VIEW public.epn_view
AS SELECT -- noqa: L034
  'exoplanet'::text AS granule_gid,
  'ci'::text AS dataproduct_type,
  core_planet.id::text AS obs_id,
  core_planet.name::text AS target_name,
  CASE
    WHEN planet_has_star(core_planet.id)
      THEN core_star.ra
    ELSE core_planetarysystem.ra
  END AS c1min,
  CASE
    WHEN planet_has_star(core_planet.id)
      THEN core_star."dec"
    ELSE core_planetarysystem."dec"
  END AS c2min,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      CASE
        WHEN get_error_min_max(core_star.distance_error) IS NOT NULL
          AND (
            get_error_min_max(core_star.distance_error)
          ) [1] = 'NaN'::double precision
          THEN pc2km(core_star.distance)
        WHEN get_error_min_max(core_star.distance_error) IS NOT NULL
          AND (
            get_error_min_max(core_star.distance_error)
          ) [1] = 'Infinity'::double precision
          THEN 'Infinity'::double precision
        ELSE pc2km(
          core_star.distance - (get_error_min_max(core_star.distance_error)) [1]
        )
      END
    WHEN get_error_min_max(core_planetarysystem.distance_error) IS NOT NULL
      AND (
        get_error_min_max(core_planetarysystem.distance_error)
      ) [1] = 'NaN'::double precision
      THEN pc2km(core_planetarysystem.distance)
    WHEN get_error_min_max(core_planetarysystem.distance_error) IS NOT NULL
      AND (
        get_error_min_max(core_planetarysystem.distance_error)
      ) [1] = 'Infinity'::double precision
      THEN 'Infinity'::double precision
    ELSE pc2km(
      core_planetarysystem.distance - (
        get_error_min_max(core_planetarysystem.distance_error)
      ) [1]
    )
  END AS c3min,
  CASE
    WHEN get_error_min_max(core_star.distance_error) IS NOT NULL
      AND (
        get_error_min_max(core_star.distance_error)
      ) [2] = 'NaN'::double precision
      THEN pc2km(core_star.distance)
    WHEN get_error_min_max(core_star.distance_error) IS NOT NULL
      AND (
        get_error_min_max(core_star.distance_error)
      ) [2] = 'Infinity'::double precision
      THEN 'Infinity'::double precision
    ELSE pc2km(core_star.distance - (
      get_error_min_max(core_star.distance_error)
      ) [2])
  END AS c3max,
  'celestial'::text AS spatial_frame_type,
  core_planet.modified AS release_date,
  to_timestamp(core_planet.discovered::text, 'YYYY'::text) AS creation_date,
  core_planet.modified AS modification_date,
  array_to_string(array(
    SELECT core_molecule.name
    FROM
      core_molecule,
      core_atmospheremolecule
    WHERE core_atmospheremolecule.planet_id = core_planet.id
      AND core_atmospheremolecule.molecule_id = core_molecule.id), '#'::text
  ) AS species,
  replace(
    replace(
      replace(
        replace(
          replace(
            replace(
              replace(
                replace(
                  replace(
                    replace(
                      replace(
                        replace(
                          replace(
                            replace(
                              array_to_string(
                                core_planet.detection_type,
                                '#'::text
                              ),
                              '1'::text,
                              'Radial Velocity'::text
                            ),
                            '2'::text,
                            'Pulsar'::text
                          ),
                          '3'::text,
                          'Controversial'::text
                        ),
                        '4'::text,
                        'Microlensing'::text
                      ),
                      '5'::text,
                      'Imaging'::text
                    ),
                    '6'::text,
                    'Primary Transit'::text
                  ),
                  '7'::text,
                  'Astrometry'::text
                ),
                '8'::text,
                'TTV'::text
              ),
              '9'::text,
              'Other'::text
            ),
            '10'::text,
            'Spectrum'::text
          ),
          '11'::text,
          'Theoretical'::text
        ),
        '12'::text,
        'Flux'::text
      ),
      '13'::text,
      'Secondary Transit'::text
    ),
    '14'::text,
    'IR Excess'::text
  ) AS detection_type,
  CASE core_planet.publication_status
    WHEN 1::smallint THEN 'Published in a refereed paper'::text
    WHEN 2::smallint THEN 'Submitted to a professional journal'::text
    WHEN 3::smallint THEN 'Announced on a professional conference'::text
    WHEN 4::smallint THEN 'Announced on a website'::text
    ELSE NULL::text
  END AS publication_status,
  get_mass(
    core_planet.mass_detected_string,
    core_planet.mass_detected_unit,
    core_planet.mass_sini_string,
    core_planet.mass_sini_unit,
    core_planet.inclination,
    mass_jup_unit_constant()
  ) AS mass,
  (
    get_mass_error(
      core_planet.mass_detected_string,
      core_planet.mass_detected_error_string,
      core_planet.mass_detected_unit,
      core_planet.mass_sini_string,
      core_planet.mass_sini_error_string,
      core_planet.mass_sini_unit,
      core_planet.inclination,
      core_planet.inclination_error,
      mass_jup_unit_constant()
    )
  ) [1] AS mass_error_min,
  (
    get_mass_error(
      core_planet.mass_detected_string,
      core_planet.mass_detected_error_string,
      core_planet.mass_detected_unit,
      core_planet.mass_sini_string,
      core_planet.mass_sini_error_string,
      core_planet.mass_sini_unit,
      core_planet.inclination,
      core_planet.inclination_error,
      mass_jup_unit_constant()
    )
  ) [2] AS mass_error_max,
  get_multi_units_value(
    core_planet.radius_string,
    core_planet.radius_unit,
    'radius'::character varying,
    radius_jup_unit_constant()
  ) AS radius,
  (
    get_multi_units_value_error(
      core_planet.radius_error_string,
      core_planet.radius_unit,
      'radius'::character varying,
      radius_jup_unit_constant()
    )
  ) [1] AS radius_error_min,
  (
    get_multi_units_value_error(
      core_planet.radius_error_string,
      core_planet.radius_unit,
      'radius'::character varying,
      radius_jup_unit_constant()
    )
  ) [2] AS radius_error_max,
  core_planet.axis AS semi_major_axis,
  core_planet.axis_error[1] AS semi_major_axis_error_min,
  CASE
    WHEN core_planet.axis_error[2] IS NOT NULL
      THEN core_planet.axis_error[2]
    ELSE core_planet.axis_error[1]
  END AS semi_major_axis_error_max,
  core_planet.period,
  core_planet.period_error[1] AS period_error_min,
  CASE
    WHEN core_planet.period_error[2] IS NOT NULL
      THEN core_planet.period_error[2]
    ELSE core_planet.period_error[1]
  END AS period_error_max,
  core_planet.eccentricity,
  core_planet.eccentricity_error[1] AS eccentricity_error_min,
  CASE
    WHEN core_planet.eccentricity_error[2] IS NOT NULL
      THEN core_planet.eccentricity_error[2]
    ELSE core_planet.eccentricity_error[1]
  END AS eccentricity_error_max,
  core_planet.omega AS periastron,
  core_planet.omega_error[1] AS periastron_error_min,
  CASE
    WHEN core_planet.omega_error[2] IS NOT NULL
      THEN core_planet.omega_error[2]
    ELSE core_planet.omega_error[1]
  END AS periastron_error_max,
  core_planet.tzero_tr,
  core_planet.tzero_tr_error[1] AS tzero_tr_error_min,
  CASE
    WHEN core_planet.tzero_tr_error[2] IS NOT NULL
      THEN core_planet.tzero_tr_error[2]
    ELSE core_planet.tzero_tr_error[1]
  END AS tzero_tr_error_max,
  core_planet.tzero_vr,
  core_planet.tzero_vr_error[1] AS tzero_vr_error_min,
  CASE
    WHEN core_planet.tzero_vr_error[2] IS NOT NULL
      THEN core_planet.tzero_vr_error[2]
    ELSE core_planet.tzero_vr_error[1]
  END AS tzero_vr_error_max,
  core_planet.tperi AS t_peri,
  core_planet.tperi_error[1] AS t_peri_error_min,
  CASE
    WHEN core_planet.tperi_error[2] IS NOT NULL
      THEN core_planet.tperi_error[2]
    ELSE core_planet.tperi_error[1]
  END AS t_peri_error_max,
  core_planet.tconj AS t_conj,
  core_planet.tconj_error[1] AS t_conj_error_min,
  CASE
    WHEN core_planet.tconj_error[2] IS NOT NULL
      THEN core_planet.tconj_error[2]
    ELSE core_planet.tconj_error[1]
  END AS t_conj_error_max,
  core_planet.inclination,
  core_planet.inclination_error[1] AS inclination_error_min,
  CASE
    WHEN core_planet.inclination_error[2] IS NOT NULL
      THEN core_planet.inclination_error[2]
    ELSE core_planet.inclination_error[1]
  END AS inclination_error_max,
  core_planet.tzero_tr_sec,
  core_planet.tzero_tr_sec_error[1] AS tzero_tr_sec_error_min,
  CASE
    WHEN core_planet.tzero_tr_sec_error[2] IS NOT NULL
      THEN core_planet.tzero_tr_sec_error[2]
    ELSE core_planet.tzero_tr_sec_error[1]
  END AS tzero_tr_sec_error_max,
  core_planet.lambda_angle,
  core_planet.lambda_angle_error[1] AS lambda_angle_error_min,
  CASE
    WHEN core_planet.lambda_angle_error[2] IS NOT NULL
      THEN core_planet.lambda_angle_error[2]
    ELSE core_planet.lambda_angle_error[1]
  END AS lambda_angle_error_max,
  core_planet.discovered::integer AS discovered,
  core_planet.modified AS updated,
  core_planet.remarks,
  core_planet.other_web,
  core_planet.angular_distance,
  core_planet.temp_calculated,
  core_planet.temp_measured,
  core_planet.hot_point_lon,
  core_planet.log_g,
  core_planet.created,
  core_planet.modified,
  core_planet.albedo,
  core_planet.albedo_error[1] AS albedo_error_min,
  CASE
    WHEN core_planet.albedo_error[2] IS NOT NULL
      THEN core_planet.albedo_error[2]
    ELSE core_planet.albedo_error[1]
  END AS albedo_error_max,
  CASE core_planet.mass_detection_type
    WHEN 1::smallint THEN 'Radial Velocity'::text
    WHEN 2::smallint THEN 'Pulsar'::text
    WHEN 3::smallint THEN 'Controversial'::text
    WHEN 4::smallint THEN 'Microlensing'::text
    WHEN 5::smallint THEN 'Imaging'::text
    WHEN 6::smallint THEN 'Primary Transit'::text
    WHEN 7::smallint THEN 'Astrometry'::text
    WHEN 8::smallint THEN 'TTV'::text
    WHEN 9::smallint THEN 'Other'::text
    WHEN 10::smallint THEN 'Spectrum'::text
    WHEN 11::smallint THEN 'Theoretical'::text
    WHEN 12::smallint THEN 'Flux'::text
    WHEN 13::smallint THEN 'Secondary Transit'::text
    WHEN 14::smallint THEN 'IR Excess'::text
    ELSE NULL::text
  END AS mass_detection_type,
  CASE core_planet.radius_detection_type
    WHEN 1::smallint THEN 'Radial Velocity'::text
    WHEN 2::smallint THEN 'Pulsar'::text
    WHEN 3::smallint THEN 'Controversial'::text
    WHEN 4::smallint THEN 'Microlensing'::text
    WHEN 5::smallint THEN 'Imaging'::text
    WHEN 6::smallint THEN 'Primary Transit'::text
    WHEN 7::smallint THEN 'Astrometry'::text
    WHEN 8::smallint THEN 'TTV'::text
    WHEN 9::smallint THEN 'Other'::text
    WHEN 10::smallint THEN 'Spectrum'::text
    WHEN 11::smallint THEN 'Theoretical'::text
    WHEN 12::smallint THEN 'Flux'::text
    WHEN 13::smallint THEN 'Secondary Transit'::text
    WHEN 14::smallint THEN 'IR Excess'::text
    ELSE NULL::text
  END AS radius_detection_type,
  get_multi_units_value(
    core_planet.mass_sini_string,
    core_planet.mass_sini_unit,
    'mass'::character varying,
    mass_jup_unit_constant()
  ) AS mass_sin_i,
  (
    get_multi_units_value_error(
      core_planet.mass_sini_error_string,
      core_planet.mass_sini_unit,
      'mass'::character varying,
      mass_jup_unit_constant()
    )
  ) [1] AS mass_sin_i_error_min,
  (
    get_multi_units_value_error(
      core_planet.mass_sini_error_string,
      core_planet.mass_sini_unit,
      'mass'::character varying,
      mass_jup_unit_constant()
    )
  ) [2] AS mass_sin_i_error_max,
  core_planet.impact_parameter,
  core_planet.impact_parameter_error[1] AS impact_parameter_error_min,
  CASE
    WHEN core_planet.impact_parameter_error[2] IS NOT NULL
      THEN core_planet.impact_parameter_error[2]
    ELSE core_planet.impact_parameter_error[1]
  END AS impact_parameter_error_max,
  core_planet.k,
  core_planet.k_error[1] AS k_error_min,
  CASE
    WHEN core_planet.k_error[2] IS NOT NULL
      THEN core_planet.k_error[2]
    ELSE core_planet.k_error[1]
  END AS k_error_max,
  array_to_string(array(
    SELECT core_alternateplanetname_1.name
    FROM core_alternateplanetname AS core_alternateplanetname_1 -- noqa: L031
    WHERE core_planet.id = core_alternateplanetname_1.planet_id), '#'::text
  ) AS alternate_name,
  core_star.name AS star_name,
  core_star.distance AS star_distance,
  core_star.distance_error[1] AS star_distance_error_min,
  CASE
    WHEN core_star.distance_error[2] IS NOT NULL
      THEN core_star.distance_error[2]
    ELSE core_star.distance_error[1]
  END AS star_distance_error_max,
  core_star.spec_type AS star_spec_type,
  core_star.magnitude_v AS mag_v,
  core_star.magnitude_i AS mag_i,
  core_star.magnitude_j AS mag_j,
  core_star.magnitude_h AS mag_h,
  core_star.magnitude_k AS mag_k,
  core_star.metallicity AS star_metallicity,
  core_star.mass AS star_mass,
  core_star.radius AS star_radius,
  core_star.age AS star_age,
  core_star.teff AS star_teff,
  core_star.magnetic_field,
  CASE core_star.detected_disc
    WHEN 5::smallint THEN 'Imaging'::text
    WHEN 14::smallint THEN 'IR Excess'::text
    ELSE NULL::text
  END AS detected_disc,
  'http://exoplanet.eu/catalog/'::text
  || replace(core_planet.name::text, ' '::text, '_'::text) AS external_link
FROM core_planet
FULL JOIN core_star
  ON core_star.id = ((
    SELECT core_planet2star.star_id
    FROM core_planet2star
    WHERE core_planet2star.planet_id = core_planet.id
    ORDER BY core_planet2star.star_id
    LIMIT 1
    ))
FULL JOIN core_planetarysystem
  ON core_planet.planetary_system_id = core_planetarysystem.id
WHERE NOT core_planet.detection_type::integer[] @> ARRAY[3]
  AND core_planet.status = 1
  AND core_planet.planet_status = 1
ORDER BY core_planet.name;
