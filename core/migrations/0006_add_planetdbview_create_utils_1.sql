/*
We had to split the SQL file in multiple parts to prevent lint parser
(sqlfluff) to crash. It is also a bad idea to have too big files anyway.

First We create the utils functions used to create or update the view (first
part).
*/

CREATE OR REPLACE FUNCTION decdeg_2_decsex(dec_deg double precision)
RETURNS character varying AS $BODY$
DECLARE
  signus char;
  deg integer;
  minutes integer;
  seconds integer;
  mseconds integer;
  tmp double precision;
BEGIN
  IF dec_deg >=0 THEN
    signus = '+';
  ELSE
    signus = '-';
  END IF;
  tmp = abs(dec_deg);
  deg = floor(tmp);
  tmp = (tmp - deg) * 60;
  minutes = floor (tmp);
  tmp = (tmp - minutes) * 60;
  seconds = round(tmp);
  IF seconds = 60 THEN
    seconds = 0;
    minutes = minutes + 1;
    IF minutes = 60 THEN
      minutes = 0;
      deg = deg + 1;
    END IF;
  END IF;

  RETURN signus ||
    to_char(deg, 'FM00') ||
    ':' ||
    to_char(minutes, 'FM00') ||
    ':' ||
    to_char(seconds,'FM00');

END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_abssini_limits(
  inclination double precision,
  inclination_error double precision[]
) RETURNS double precision[] AS $BODY$
DECLARE
  incl_error_minmax double precision[];
  incl_error_min double precision;
  incl_error_max double precision;
  incl_min double precision;
  incl_max double precision;
  ei double precision [];
  dum double precision;
  res double precision[];
  i double precision;
  abssini_min double precision;
  abssini_max double precision;
  abssini_minmax double precision[];
  dum_array double precision [] := '{-360,-270,-180, -90, 0, 90, 180, 270, 360}';
   BEGIN
  IF (inclination_error IS NOT NULL AND inclination_error != '{}') THEN
    incl_error_minmax = get_error_min_max(inclination_error);
    incl_error_min = incl_error_minmax[1];
    incl_error_max = incl_error_minmax[2];
    IF incl_error_min = 'NaN' AND incl_error_max = 'Infinity' THEN
      incl_min = inclination;
      incl_max = inclination;
    ELSE
      incl_min = inclination - incl_error_min;
      incl_max = inclination + incl_error_max;
    END IF;
    ei [1] = incl_min;
    ei [2] = incl_max;
    IF NOT (cast(ei[1] as integer) % 90 < cast(ei[2] as integer) % 90
    AND cast(ei[1] as integer) / 90 = cast(ei[2] as integer) / 90) THEN
      FOREACH dum in ARRAY dum_array LOOP
        if ei[1] < dum AND ei [2] > dum THEN
          ei = array_append(ei, dum);
        END IF;
      END LOOP;
    END IF;
    FOREACH i in ARRAY ei LOOP
      IF cast(i as integer) % 180 = 0 THEN
        res = array_append(res,cast(0 as double precision));
      ELSE
        res = array_append(res,sin(radians(i)));
      END IF;
    END LOOP;
    SELECT min(res_c) INTO abssini_min FROM unnest(res) AS res_c;
    SELECT max(res_c) INTO abssini_max FROM unnest(res) AS res_c;
  ELSE
    abssini_min = abs(sin(radians(inclination)));
    abssini_max = abssini_min;
  END IF;
  abssini_minmax [1] = abssini_min;
  abssini_minmax [2] = abssini_max;
  RETURN abssini_minmax;



   END;
 $BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_error_array(
  value_error_string_array character varying[]
) RETURNS double precision[] AS $BODY$
DECLARE
  value_error double precision[];
  value_error_string character varying;
   BEGIN
  IF value_error_string_array IS NULL
  OR array_length(value_error_string_array,1) = 0 THEN
    RETURN NULL;
  ELSE
    FOREACH value_error_string IN ARRAY value_error_string_array
    LOOP
      IF value_error_string = 'Infinity' THEN
        value_error = array_append(value_error, 'Infinity');
      ELSIF value_error_string = 'NaN' THEN
        value_error = array_append(value_error, 'NaN');
      ELSE
        value_error = array_append(
          value_error,
          cast(value_error_string as double precision)
        );
      END IF;
    END LOOP;
    RETURN value_error;
  END IF;
   END;
 $BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_error_min_max(
  error double precision[]
) RETURNS double precision[] AS $BODY$
DECLARE
  error_array double precision [2];
   BEGIN
  IF error IS NOT NULL AND array_length(error,1) > 0 THEN
    IF array_length(error,1) = 1 THEN
      error_array[1] = error[1];
      error_array[2] = error[1];
    ELSE
      error_array = error;
    END IF;
  ELSE
    error_array = NULL;
  END IF;
  RETURN error_array;
   END;
 $BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_error_significant_digits_nb(
  string_value character varying,
  string_value_error character varying[]
) RETURNS integer AS $BODY$
DECLARE
  significant_digits_nb_value_max integer;
  significant_digits_nb_error integer;
  string_error character varying;
   BEGIN
  significant_digits_nb_value_max = get_significant_digits_nb(string_value);
  IF string_value_error IS NOT NULL THEN
    FOREACH string_error in ARRAY string_value_error LOOP
      IF string_error != ''
      AND string_error != 'NaN'
      AND string_error != 'Infinty' THEN
        significant_digits_nb_error = get_significant_digits_nb(string_error);
        IF significant_digits_nb_error > significant_digits_nb_value_max THEN
          significant_digits_nb_value_max = significant_digits_nb_error;
        END IF;
      END IF;
    END LOOP;
  END IF;
  RETURN significant_digits_nb_value_max;
   END;
 $BODY$ LANGUAGE plpgsql VOLATILE COST 100;


/*
This function has an equivalent in the python code in the
core/helpers/conversion module you have the mass.mass_planet_data()
*/
CREATE OR REPLACE FUNCTION get_mass(
  mass_detected_string character varying,
  mass_detected_unit smallint,
  mass_sini_string character varying,
  mass_sini_unit smallint,
  inclination double precision,
  result_unit smallint
) RETURNS double precision AS $BODY$
DECLARE
  mass_detected double precision =  cast(mass_detected_string as double precision);
  mass_sini double precision = cast(mass_sini_string as double precision);
  mass double precision;
  significant_digits_nb integer;
   BEGIN
  IF mass_detected IS NOT NULL THEN
    significant_digits_nb = get_significant_digits_nb(mass_detected_string);
    mass = round_multi_units_value(
      mass_detected,
      significant_digits_nb,
      mass_detected_unit,
      'mass'::character varying,
      result_unit
    );
  ELSE
    IF mass_sini IS NOT NULL THEN
      significant_digits_nb = get_significant_digits_nb(mass_sini_string);
      IF inclination IS NOT NULL AND sin(radians(inclination)) != 0 THEN
        mass = cast(
          mass_sini_string as double precision) / abs(sin(radians(inclination))
        );
        mass = round_multi_units_value(
          mass,
          significant_digits_nb,
          mass_sini_unit,
          'mass'::character varying,
          result_unit
        );
      ELSE
        mass = round_multi_units_value(
          mass_sini,
          significant_digits_nb,
          mass_sini_unit,
          'mass'::character varying,
          result_unit
        );
      END IF;
    ELSE
      mass = NULL;
    END IF;
  END IF;
  RETURN mass;
   END;
 $BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_mass_error(
  mass_detected_string character varying,
  mass_detected_error_string character varying[],
  mass_detected_unit smallint,
  mass_sini_string character varying,
  mass_sini_error_string character varying[],
  mass_sini_unit smallint,
  inclination double precision,
  inclination_error double precision[],
  result_unit smallint
) RETURNS double precision[] AS $BODY$
DECLARE
  mass_detected double precision =  cast(mass_detected_string as double precision);
  mass_sini double precision = cast(mass_sini_string as double precision);
  mass_sini_error double precision [] = get_error_array(mass_sini_error_string);
  mass double precision;
  mass_error double precision [];
  mass_error_unconverted double precision [];
  error double precision;
  significant_digits_nb integer;

BEGIN
  IF mass_detected IS NOT NULL THEN
    mass_error = get_multi_units_value_error(
      mass_detected_error_string,
      mass_detected_unit,
      'mass'::character varying,
      result_unit
    );
  ELSE
    IF mass_sini IS NOT NULL THEN
      IF inclination IS NOT NULL
      AND sin(radians(inclination)) != 0 THEN
        significant_digits_nb = get_error_significant_digits_nb(
          mass_sini_string,
          mass_sini_error_string
        );
        mass_error_unconverted = get_mass_error_from_msini(
          mass_sini,
          mass_sini_error,
          inclination,
          inclination_error
        );
        IF mass_error_unconverted IS NULL THEN
          mass_error = NULL;
        ELSE
          FOREACH error IN ARRAY mass_error_unconverted LOOP
            mass_error = array_append(
              mass_error,
              round_multi_units_value(
                error,
                significant_digits_nb,
                mass_sini_unit,
                'mass'::character varying,
                result_unit
              )
            );
          END LOOP;
        END IF;
      ELSE
        mass_error = get_multi_units_value_error(
          mass_sini_error_string,
          mass_sini_unit,
          'mass'::character varying,
          result_unit
        );
      END IF;
    ELSE
      mass_error = NULL;
    END IF;
  END IF;
  RETURN mass_error;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE OR REPLACE FUNCTION get_mass_error_from_msini(
  mass_sini double precision,
  mass_sini_error double precision[],
  inclination double precision,
  inclination_error double precision[]
) RETURNS double precision[] AS $BODY$
DECLARE
  mass double precision;
  mass_error_min double precision;
  mass_error_max double precision;
  mass_error double precision [2];
  mass_sin_minmax double precision [2];
  mass_sin_min double precision;
  mass_sin_max double precision;
  abssini_minmax double precision [2];
  abssini_min double precision;
  abssini_max double precision;
  mass_min double precision;
  mass_max double precision;

BEGIN
  mass = mass_sini/abs(sin(radians(inclination)));
  IF (inclination_error IS NOT NULL
  AND inclination_error != '{}')
  OR (mass_sini_error IS NOT NULL AND mass_sini_error!= '{}') THEN
    mass_sin_minmax = get_mass_sini_limits(mass_sini, mass_sini_error);
    mass_sin_min = mass_sin_minmax [1];
    mass_sin_max = mass_sin_minmax [2];
    abssini_minmax = get_abssini_limits(inclination, inclination_error);
    abssini_min = abssini_minmax [1];
    abssini_max = abssini_minmax [2];


    IF mass_sin_min = 0 THEN
      mass_min = 0;
    ELSE
      mass_min = mass_sin_min / abssini_max;
    END IF;

    IF abssini_min = 0 THEN
      mass_max = 'Infinity';
    ELSE
      mass_max = mass_sin_max/abssini_min;
    END IF;
    if mass_min = 0 AND mass_max = mass THEN
      mass_error_min = 'Infinity';
      mass_error_max = 'NaN';
    ELSIF mass_min = mass AND mass_max = 'Infinity' THEN
      mass_error_min = 'NaN';
      mass_error_max = 'Infinity';
    ELSE
      mass_error_min = cast(mass - mass_min as numeric);
      IF mass_max = 'Infinity' THEN
        mass_error_max = 'Infinity';
      ELSE
        mass_error_max = cast(mass_max - mass as numeric);
      END IF;
    END IF;
    mass_error [1] = mass_error_min;
    mass_error [2] = mass_error_max;
  ELSE
    mass_error = NULL;
  END IF;
  RETURN mass_error;

END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;
