-- At last we drop all the utils 2 functions

DROP FUNCTION IF EXISTS round_multi_units_value;
DROP FUNCTION IF EXISTS radius_jup_unit_constant;
DROP FUNCTION IF EXISTS radius_jup_constant;
DROP FUNCTION IF EXISTS radius_earth_unit_constant;
DROP FUNCTION IF EXISTS radius_earth_constant;
DROP FUNCTION IF EXISTS radeg_2_rasex;
DROP FUNCTION IF EXISTS planet_has_star;
DROP FUNCTION IF EXISTS mass_jup_unit_constant;
DROP FUNCTION IF EXISTS mass_jup_constant;
DROP FUNCTION IF EXISTS mass_earth_unit_constant;
DROP FUNCTION IF EXISTS mass_earth_constant;
DROP FUNCTION IF EXISTS is_mass_from_mass_sini;
DROP FUNCTION IF EXISTS get_significant_digits_nb;
DROP FUNCTION IF EXISTS get_publication_status_letter;
DROP FUNCTION IF EXISTS get_planet_status_string;
DROP FUNCTION IF EXISTS get_multi_units_value_error;
DROP FUNCTION IF EXISTS get_multi_units_value;
DROP FUNCTION IF EXISTS get_mass_sini_limits;
