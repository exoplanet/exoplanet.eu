-- At last we drop all the utils 1 functions

DROP FUNCTION IF EXISTS get_mass_error_from_msini;
DROP FUNCTION IF EXISTS get_mass_error;
DROP FUNCTION IF EXISTS get_mass;
DROP FUNCTION IF EXISTS get_error_significant_digits_nb;
DROP FUNCTION IF EXISTS get_error_min_max;
DROP FUNCTION IF EXISTS get_error_array;
DROP FUNCTION IF EXISTS get_abssini_limits;
DROP FUNCTION IF EXISTS decdeg_2_decsex;
