/*
We had to split the SQL file in multiple parts to prevent lint parser
(sqlfluff) to crash. It is also a bad idea to have too big files anyway.

In the previous files we created the utils functions and the the materialized
view, in this one we create the triggers to update the view.
*/

CREATE OR REPLACE FUNCTION trig_refresh_planetdbview()
RETURNS TRIGGER AS $BODY$
BEGIN
  REFRESH MATERIALIZED VIEW "core_planetdbview";
  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_star
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_planet
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_alternateplanetname
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_molecule
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_planet2star
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_planetarysystem
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();


CREATE TRIGGER refresh_planetdbview AFTER
INSERT
OR
UPDATE
OR
DELETE
OR TRUNCATE ON core_alternatestarname
FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_planetdbview();
