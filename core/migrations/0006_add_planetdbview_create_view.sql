/*
We had to split the SQL file in multiple parts to prevent lint parser
(sqlfluff) to crash. It is also a bad idea to have too big files anyway.

In the previous file we created the utils functions, in this one we create the
materialized view itself.
*/

-- noqa: disable=L034
CREATE MATERIALIZED VIEW public."core_planetdbview" AS
SELECT
  core_planet.id,
  core_planet.name,
  core_planet.detection_type,
  get_publication_status_letter(core_planet.publication_status)
  AS publication_status_letter,
  core_planet.axis,
  core_planet.axis_error,
  core_planet.period,
  core_planet.period_error,
  core_planet.eccentricity,
  core_planet.eccentricity_error,
  core_planet.omega,
  core_planet.omega_error,
  core_planet.tzero_tr,
  core_planet.tzero_tr_error,
  core_planet.tzero_vr,
  core_planet.tzero_vr_error,
  core_planet.tperi,
  core_planet.tperi_error,
  core_planet.tconj,
  core_planet.tconj_error,
  core_planet.inclination,
  core_planet.inclination_error,
  core_planet.discovered,
  core_planet.remarks,
  core_planet.other_web,
  core_planet.angular_distance,
  core_planet.temp_calculated,
  core_planet.temp_measured,
  core_planet.hot_point_lon,
  core_planet.log_g,
  core_planet.created,
  core_planet.modified,
  core_planet.status,
  core_planet.tzero_tr_sec,
  core_planet.tzero_tr_sec_error,
  core_planet.lambda_angle,
  core_planet.lambda_angle_error,
  core_planet.albedo,
  core_planet.albedo_error,
  core_planet.mass_detection_type,
  core_planet.radius_detection_type,
  core_planet.impact_parameter,
  core_planet.impact_parameter_error,
  core_planet."K",
  core_planet."K_error",
  core_planet.temp_calculated_error,
  get_planet_status_string(core_planet.planet_status) AS planet_status_string,
  get_multi_units_value(
    core_planet.mass_sini_string,
    core_planet.mass_sini_unit,
    'mass'::character varying,
    mass_jup_unit_constant()) AS mass_sini_mjup,
  get_multi_units_value_error(
    core_planet.mass_sini_error_string,
    core_planet.mass_sini_unit,
    'mass'::character varying,
    mass_jup_unit_constant()) AS mass_sini_error_mjup,
  get_multi_units_value(
    core_planet.mass_sini_string,
    core_planet.mass_sini_unit,
    'mass'::character varying,
    mass_earth_unit_constant()) AS mass_sini_mearth,
  get_multi_units_value_error(
    core_planet.mass_sini_error_string,
    core_planet.mass_sini_unit,
    'mass'::character varying,
    mass_earth_unit_constant()) AS mass_sini_error_mearth,
  get_multi_units_value(
    core_planet.radius_string,
    core_planet.radius_unit,
    'radius'::character varying,
    radius_jup_unit_constant()) AS radius_rjup,
  get_multi_units_value_error(
    core_planet.radius_error_string,
    core_planet.radius_unit,
    'radius'::character varying,
    radius_jup_unit_constant()) AS radius_error_rjup,
  get_multi_units_value(
    core_planet.radius_string,
    core_planet.radius_unit,
    'radius'::character varying,
    radius_earth_unit_constant()) AS radius_rearth,
  get_multi_units_value_error(
    core_planet.radius_error_string,
    core_planet.radius_unit,
    'radius'::character varying,
    radius_earth_unit_constant()) AS radius_error_rearth,
  get_mass(
    core_planet.mass_detected_string,
    core_planet.mass_detected_unit,
    core_planet.mass_sini_string,
    core_planet.mass_sini_unit,
    core_planet.inclination,
    mass_jup_unit_constant()) AS mass_mjup,
  get_mass_error(
    core_planet.mass_detected_string,
    core_planet.mass_detected_error_string,
    core_planet.mass_detected_unit,
    core_planet.mass_sini_string,
    core_planet.mass_sini_error_string,
    core_planet.mass_sini_unit,
    core_planet.inclination,
    core_planet.inclination_error,
    mass_jup_unit_constant()) AS mass_error_mjup,
  get_mass(
    core_planet.mass_detected_string,
    core_planet.mass_detected_unit,
    core_planet.mass_sini_string,
    core_planet.mass_sini_unit,
    core_planet.inclination,
    mass_earth_unit_constant()) AS mass_mearth,
  get_mass_error(
    core_planet.mass_detected_string,
    core_planet.mass_detected_error_string,
    core_planet.mass_detected_unit,
    core_planet.mass_sini_string,
    core_planet.mass_sini_error_string,
    core_planet.mass_sini_unit,
    core_planet.inclination,
    core_planet.inclination_error,
    mass_earth_unit_constant()) AS mass_error_mearth,
  is_mass_from_mass_sini(
    core_planet.mass_detected_string,
    core_planet.mass_sini_string) AS mass_from_mass_sini_flag,
  CASE
    WHEN (
        (
          SELECT count(*) AS alternate_names_count
          FROM core_alternateplanetname
          WHERE core_alternateplanetname.planet_id = core_planet.id
        )
      ) > 0 THEN
      array(
        SELECT core_alternateplanetname.name
        FROM core_alternateplanetname
        WHERE core_alternateplanetname.planet_id = core_planet.id
      )
    ELSE
      NULL::character varying[]
  END AS alternate_names,
  CASE
    WHEN (
      (
        SELECT count(*) AS molecule_count
        FROM core_atmospheremolecule
        WHERE core_planet.id = core_atmospheremolecule.planet_id
      )
    ) > 0 THEN
    array(
      SELECT DISTINCT core_molecule.name
      FROM core_molecule, core_atmospheremolecule
      WHERE core_planet.id = core_atmospheremolecule.planet_id
        AND core_atmospheremolecule.molecule_id = core_molecule.id
    )
    ELSE
      NULL::character varying[]
  END AS molecules,
  -- noqa: disable=L031
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT count(DISTINCT core_planet2star.planet_id) AS confirmed_plt_count
        FROM core_planet2star
        WHERE (
          core_planet2star.star_id IN
          (
            SELECT core_planet2star_1.star_id
            FROM core_planet2star AS core_planet2star_1,
              core_planet
            WHERE core_planet2star_1.planet_id = core_planet.id
              AND core_planet.id = core_planet2star_1.planet_id
              AND core_planet.planet_status = 1
          )
        )
      )
    ELSE
      (
        SELECT count(*) AS plt_count
        FROM core_planet AS coreplanet
        WHERE coreplanet.planetary_system_id = core_planet.planetary_system_id
      )
  END AS confirmed_planet_count,
  -- noqa: enable=L031
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.name
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::character varying
  END AS star_name,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.ra
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      (
        SELECT core_planetarysystem.ra
        FROM core_planetarysystem
        WHERE core_planet.planetary_system_id = core_planetarysystem.id
        LIMIT 1
      )
  END AS star_ra_deg,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      radeg_2_rasex(
        (
          SELECT core_star.ra
          FROM core_star
          WHERE core_star.id = core_planet.main_star_id
          LIMIT 1
        )
      )
    ELSE
      radeg_2_rasex(
        (
          SELECT core_planetarysystem.ra
          FROM core_planetarysystem
          WHERE core_planet.planetary_system_id = core_planetarysystem.id
          LIMIT 1
        )
      )
  END AS star_ra_sex,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star."dec"
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      (
        SELECT core_planetarysystem."dec"
        FROM core_planetarysystem
        WHERE core_planet.planetary_system_id = core_planetarysystem.id
        LIMIT 1
      )
  END AS star_dec_deg,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      decdeg_2_decsex(
        (
          SELECT core_star."dec"
          FROM core_star
          WHERE core_star.id = core_planet.main_star_id
          LIMIT 1
        )
      )
    ELSE
      decdeg_2_decsex(
        (
          SELECT core_planetarysystem."dec"
          FROM core_planetarysystem
          WHERE core_planet.planetary_system_id = core_planetarysystem.id
          LIMIT 1
        )
      )
  END AS star_dec_sex,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.magnitude_v
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_magnitude_v,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.magnitude_i
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_magnitude_i,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.magnitude_j
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_magnitude_j,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.magnitude_h
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_magnitude_h,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.magnitude_k
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_magnitude_k,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.distance
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      (
        SELECT core_planetarysystem.distance
        FROM core_planetarysystem
        WHERE core_planet.planetary_system_id = core_planetarysystem.id
        LIMIT 1
      )
  END AS star_distance,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.distance_error
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      (
        SELECT core_planetarysystem.distance_error
        FROM core_planetarysystem
        WHERE core_planet.planetary_system_id = core_planetarysystem.id
        LIMIT 1
      )
  END AS star_distance_error,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.metallicity
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_metallicity,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.metallicity_error
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision[]
  END AS star_metallicity_error,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.mass
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_mass,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.mass_error
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision[]
  END AS star_mass_error,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.radius
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_radius,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.radius_error
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision[]
  END AS star_radius_error,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.spec_type
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::character varying
  END AS star_spec_type,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.age
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_age,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.age_error
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision[]
  END AS star_age_error,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.teff
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision
  END AS star_teff,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.teff_error
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::double precision[]
  END AS star_teff_error,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.detected_disc
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::smallint
  END AS star_detected_disc,
  CASE
    WHEN planet_has_star(core_planet.id) THEN
      (
        SELECT core_star.magnetic_field
        FROM core_star
        WHERE core_star.id = core_planet.main_star_id
        LIMIT 1
      )
    ELSE
      NULL::boolean
  END AS star_magnetic_field,
  CASE
    WHEN planet_has_star(core_planet.id)
      AND (
        (
          SELECT count(*) AS star_alternet_name_count
          FROM core_alternatestarname
          WHERE core_alternatestarname.star_id = core_planet.main_star_id
        )
      ) > 0 THEN
      array(
        SELECT core_alternatestarname.name
        FROM core_alternatestarname
        WHERE core_alternatestarname.star_id = core_planet.main_star_id
      )
    ELSE
      NULL::character varying[]
  END AS star_alternate_names
FROM core_planet WITH DATA;
-- noqa: enable=L034

GRANT SELECT ON TABLE public."core_planetdbview" TO guest;
