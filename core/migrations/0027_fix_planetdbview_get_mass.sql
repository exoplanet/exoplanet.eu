/*
Fix get_mass to prevent it from computing mass from mass_sin_i when we do not
have inclination.
*/

/*
This function has an equivalent in the python code in the
core/helpers/conversion module you have the mass.mass_planet_data()
*/
CREATE OR REPLACE FUNCTION get_mass(
    mass_detected_string VARCHAR,
    mass_detected_unit SMALLINT,
    mass_sini_string VARCHAR,
    mass_sini_unit SMALLINT,
    inclination DOUBLE PRECISION,
    result_unit SMALLINT
)
-- noqa: disable=L003
/**
summary: >
  Calculates a mass of an exoplanet from the data provided and returns it in the
  specified unit.
param:
  - mass_detected_string: >
      VARCHAR, mass of the exoplanet as it was entered in the database but as a
      string (to preserve the number of digits), can be NULL.
  - mass_detected_unit: >
      SMALLINT, unit of the detected mass 1 for jupiter mass and 2 for earth
      mass, can be NULL.
  - mass_sini_string: >
      VARCHAR, mass * sin(inclination) of the exoplanet as it was entered in the
      database but as a string (to preserve the number of digits), can be NULL.
  - mass_sini_unit: >
      SMALLINT, unit of the mass sin(i) 1 for jupiter mass and 2 for earth mass,
      can be NULL.
  - inclination: >
      DOUBLE PRECISION, inclination of the exoplanet in degrees, can be NULL.
  - result_unit: >
      SMALLINT, desired units for the result 1 for jupiter mass and 2 for earth
      mass.
example:
  - code: SELECT get_mass('1.0', 2::SMALLINT, NULL, NULL, NULL, 2::SMALLINT);
returns: >
  Calculed mass if it is possible to calculate it from the provided data,
  otherwise NULL.
**/
-- noqa: enable=L003
RETURNS DOUBLE PRECISION AS $BODY$
DECLARE
  -- Variable for intermediate values intermediate values
  mass_detected DOUBLE PRECISION;
  mass_sini DOUBLE PRECISION;
  sini DOUBLE PRECISION;
  mass_angle_corrected DOUBLE PRECISION;
  -- Minimum positive value for DOUBLE PRECISION
  -- See: https://www.postgresql.org/docs/current/datatype-numeric.html#DATATYPE-FLOAT
  double_precision_min_value CONSTANT DOUBLE PRECISION := 1E-307;
  -- Maximum mass for a planet is 60 Mjup with some reasonnable margin let's take 100
  max_mass_in_mjup CONSTANT DOUBLE PRECISION := 100;
  max_mass_in_mearth CONSTANT DOUBLE PRECISION := 100 * 317.8281333983011;
  mass_unit_mjup CONSTANT SMALLINT := 1;
  mass_unit_mearth CONSTANT SMALLINT := 2;
BEGIN

  -- Early return if one of the unit is None
  IF result_unit IS NULL
  OR mass_detected_unit IS NULL
  OR mass_sini_unit IS NULL
  THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'no unit should be None but '
      'desired unit is <%>'
      'and mass detected unit is <%> '
      'and mass sini unit is <%> ',
      result_unit,
      mass_detected_unit,
      mass_sini_unit;
    RETURN NULL;
  END IF;

  -- Early return if mass is NaN and mass sini or inclination are NaN
  IF (
    -- mass is NaN
    mass_detected_string IS NOT NULL
    AND mass_detected_string ILIKE 'nan'
  ) AND (
    (
      -- mass sini is NaN
      mass_sini_string IS NOT NULL
      AND mass_sini_string ILIKE 'nan'
    ) OR (
      --inclination is NaN
      inclination IS NOT NULL
      AND inclination = 'NaN'::DOUBLE PRECISION
    )
  )
  THEN
    RETURN 'NaN'::DOUBLE PRECISION;
  END IF;

  -- Can't calculate mass if mass can't be converted to double precision
  BEGIN
    mass_detected =  mass_detected_string::DOUBLE PRECISION;
  EXCEPTION WHEN OTHERS THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'Invalid mass string <%>: not convertible to double precision',
      mass_detected_string;
    RETURN NULL;
  END;

  -- We return mass_detected simply converted to the good unit as a double precision
  IF mass_detected IS NOT NULL AND mass_detected_unit IS NOT NULL
  THEN
    -- if the mass is to big to be a planet it's an error
    IF (mass_detected_unit = mass_unit_mjup AND mass_detected > max_mass_in_mjup)
    OR (mass_detected_unit = mass_unit_mearth AND mass_detected > max_mass_in_mearth)
    THEN
      RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'Invalid mass string <%>: value superior to maximum possible mass for a planet',
      mass_detected_string;
      RETURN NULL;
    END IF;

    RETURN round_multi_units_value(
      mass_detected,
      get_significant_digits_nb(mass_detected_string),
      mass_detected_unit,
      'mass'::VARCHAR,
      result_unit
    );
  END IF;

  -- Can't calculate mass if no inclination, mass_sini or mass_sini_unit
  IF inclination IS NULL OR mass_sini_string IS NULL
  THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'mass is <%> but '
      'inclination <%> or mass sini <%> or mass sini unit <%> are NULL',
      mass_detected_string,
      inclination,
      mass_sini_string,
      mass_sini_unit;
    RETURN NULL;
  END IF;

  -- Can't calculate mass if inclination is not finite
  IF inclination = 'NaN'::DOUBLE PRECISION
  OR inclination = '+infinity'::DOUBLE PRECISION
  OR inclination = '-infinity'::DOUBLE PRECISION
  THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'mass is <%> and inclination is not a finite number'
      '<inclination: %>',
      mass_detected_string,
      inclination;
    RETURN NULL;
  END IF;

  -- Can't calculate if inclination is subnormal
  -- (lower than min DOUBLE PRECISION)
  IF inclination <> 0.0 AND abs(inclination) < double_precision_min_value
  THEN
    RAISE WARNING 'Fail to calculate mass for planet so return None, '
      'mass is <%>, mass sini is <%> but inclination is too small <%>',
      mass_detected_string,
      mass_sini_string,
      inclination;
    RETURN NULL;
  END IF;

  -- Compute the necessary intermediate values
  sini = sin(radians(inclination));

  -- Can't calculate mass if sini is zero
  IF sini = 0.0
  THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'mass is <%> and mass sini is <%> but sini is zero'
      '<inclination: %> <sini: %>',
      mass_detected_string,
      mass_sini_string,
      inclination,
      sini;
    RETURN NULL;
  END IF;

  -- Can't calculate mass if mass_sini is invalid
  BEGIN
    mass_sini = mass_sini_string::DOUBLE PRECISION;
  EXCEPTION WHEN OTHERS THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'mass is <%> but Invalid mass sini string <%>: '
      'not convertible to double precision',
      mass_detected_string,
      mass_sini_string;
    RETURN NULL;
  END;

  -- Can't calculate mass if mass_sini is not finite
  IF mass_sini = 'NaN'::DOUBLE PRECISION
  OR mass_sini = '+infinity'::DOUBLE PRECISION
  OR mass_sini = '-infinity'::DOUBLE PRECISION
  THEN
    RAISE WARNING 'Fail to calculate mass for planet so return NULL, '
      'mass is <%> and mass sini is <%> and inclination is <%> '
      'but calculated mass sini is not a finite number <%>',
      mass_detected_string,
      mass_sini_string,
      inclination,
      mass_sini;
    RETURN NULL;
  END IF;

  -- From this point we know we have computable values

  -- we return mass computed from mass_sini and inclination and converted
  -- as a DOUBLE PRECISION
  mass_angle_corrected = mass_sini / abs(sini);

  RETURN round_multi_units_value(
    mass_angle_corrected,
    get_significant_digits_nb(mass_sini_string),
    mass_sini_unit,
    'mass'::VARCHAR,
    result_unit
  );

END;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;
