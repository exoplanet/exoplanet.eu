ALTER TABLE public."core_planetdbview"
RENAME COLUMN mass_measurement_type TO mass_detection_type;

ALTER TABLE public."core_planetdbview"
RENAME COLUMN radius_measurement_type TO radius_detection_type;
