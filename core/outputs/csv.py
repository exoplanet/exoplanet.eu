# Standard imports
from dataclasses import dataclass
from itertools import chain
from typing import Any, Iterator

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    NAME_FIELD,
    OUTPUT_ALL_FIELDS,
    Column,
)
from ..models import PlanetDBView
from .base import OutputBase


@dataclass(init=False)
class CSVField:
    # Name of the Django model assoiated to this property (Planet, Star...)
    related_model: str | None

    # Name of the corresponding property in the Django model
    model_property: str

    # Name of the corresponding error property in the Django model
    error: str | None

    # Headers for this column or these columns (if there are some associated errors)
    headers: tuple[str, ...]

    def __init__(self, column: Column):
        self.model_property = column.field_def.get(
            "file_model_property", column.get_model_property()
        )
        self.related_model = column.field_def.get("related_model", None)
        self.error = column.get_error()

        name = column.field_def.get("file_header", column.get_model_field())
        self.headers = (
            (name,)
            if not self.error
            else (name, f"{name}_error_min", f"{name}_error_max")
        )

    def get_values_for_planet(
        self, planet: PlanetDBView
    ) -> tuple[Any] | tuple[Any, Any, Any]:
        """
        Returns the actual values (value and eventually errors values) of the field
        for a given planet.

        Args:
            planet: the planet for which we want the values for the différent fields.

        Returns:
            The value or the value and the associated errors
        """
        value = getattr(planet, self.model_property)

        # Can we have errors?
        if self.error:
            value_error = getattr(planet, self.error)

            # Do we actually have errors?
            if value_error:
                if len(value_error) == 1:
                    return (value, value_error[0], value_error[0])
                elif len(value_error) == 2:
                    return (value, value_error[0], value_error[1])

            return (value, None, None)

        return (value,)


# TODO replace this class by a simple function since it is used like one
@dataclass(init=False)
class CSVOutput(OutputBase):
    fields: list[CSVField]

    def __init__(
        self,
        filter_string: str | None,
        statusfilter: list[str] | None = None,
        detectionfilter: list[str] | None = None,
        search: str | None = None,
    ):
        super().__init__(
            filter_string=filter_string,
            statusfilter=statusfilter,
            detectionfilter=detectionfilter,
            search=search,
        )

        self.fields = [CSVField(Column(field_def)) for field_def in OUTPUT_ALL_FIELDS]

        # Update the queryset to have the desired sorting order
        self.planets = self.planets.order_by(NAME_FIELD["model_field"])

    def get_headers(self) -> list[str]:
        return list(chain.from_iterable([field.headers for field in self.fields]))

    def get_data_rows(self) -> Iterator[list[str]]:
        for planet in self.planets:
            row = chain.from_iterable(
                [field.get_values_for_planet(planet) for field in self.fields]
            )
            yield list(row)
