# Standard imports
from dataclasses import dataclass

# Django 💩 imports
from django.core.paginator import Paginator
from django.db.models import Q
from django.db.models.query import QuerySet

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
from ..filter_grammar import createFilter  # type: ignore[attr-defined]
from ..models import PlanetDBView
from ..models.choices import WebStatus


@dataclass(init=False)
class OutputBase:
    paginator: Paginator | None
    """Object to handle pagination"""

    # TODO Do our query on a Planet model not on PlanetDBView model
    planets: QuerySet[PlanetDBView]
    """All the planets to display in the output"""

    filter_syntax_error: bool
    """Mark if there was an error while processing the filter_string"""

    def __init__(
        self,
        filter_string: str | None = None,
        statusfilter: list[str] | None = None,
        detectionfilter: list[str] | None = None,
        search: str | None = None,
    ):
        self.paginator = None
        # TODO Do our query on a Planet model not on PlanetDBView model
        # HACK we are requesting on the PlanetDBView instead of Planet just because some
        # property/method gives us easy access to some propreties
        # pylint: disable-next=no-member
        self.planets = PlanetDBView.objects.filter(status=WebStatus.ACTIVE).all()
        self.planets = self.planets.distinct()
        self.filter_syntax_error = False

        if filter_string or statusfilter or detectionfilter:
            my_filter, self.filter_syntax_error = createFilter(
                filter_str=filter_string,
                statuses=statusfilter,
                detections=detectionfilter,
            )
            if not self.filter_syntax_error:
                # pylint: disable-next=invalid-unary-operand-type
                self.planets = self.planets.exclude(~my_filter)

        if search:
            self.planets = self.planets.filter(
                Q(name__icontains=search)
                | Q(alternate_names__icontains=search)
                | Q(star_name__icontains=search)
                | Q(star_alternate_names__icontains=search)
            )

    def get_count(self) -> int:
        return self.paginator.count if self.paginator else self.planets.count()

