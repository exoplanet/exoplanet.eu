"""Output to generate data for the polar plot."""

# mypy: disable-error-code="operator"
# We need to suppress the operator error since we stupidly try to apply math operation
# to var that can be empty strings and use TypeError exception to detect that...

# Standard imports
from dataclasses import dataclass
from math import sqrt
from typing import Any, NamedTuple, TypedDict
from urllib.parse import quote as url_encode

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    POLARPLOT_FIELD,
    POLARPLOT_VAR_FIELD,
    Column,
)
from ..models import PlanetDBView
from .base import OutputBase

# Some usefull constants for computation ##############################################

# Average temperature of the Sun
SUN_TEMP = 5778.0  # in K

# Radius of the Sun
SUN_RADIUS = 6.957e8  # in m

# Semi-major axis of the Earth
EARTH_A = 1.495978875e11  # in m

# Stefan Boltzmann constant
SIGMA = 5.67e-8  # in W m-2 K-4

# Utility Types ######################################################################


class _PolarPlotParameter(TypedDict):
    """
    Representation of a plot parameter for a PolarPlot.

    This corresponds to fields in `POLARPLOT_VAR_FIELD` (see
    `core/field_definitions.yaml`).
    """

    id: str | None
    desc: str
    datatype: str
    unit: str
    related_model: str


class _PolarPlotRowTuple(NamedTuple):
    """
    A row of data used by the PolarPlot.

    It can represent a row of actual data or a a header row.

    This corresponds to fields in `POLARPLOT_FIELD` (see `core/field_definitions.yaml`)
    plus one variable chosen by the user.
    """

    name: str
    radius: float | str
    mass: float | str
    star_distance: float | str
    star_ra: float | str
    url_keyword: str | None = None
    var: Any | None = None  # the value depends on the requested variable


# Utility functions ###################################################################


def _planet_irradiation(
    star_temp: float | Any,
    star_radius: float | Any,
    planet_sm_axis: float | Any,
) -> float:
    """
    Calculate the irradiation received by a given planet from it's star in W/m².

    It is equal to the planet irradiance divided by 4. Irradiance values are
    relatively easy to find for solar system's planets.

    Args:
      star_temp: temperature of the star (in K)
      star_radius: radius of the star (in m)
      planet_sm_axis: semi-major axis of the planet (in m)

    Returns:
      irradiation recieved by the planet in W/m²

    Raises:
      TypeError: if one of the args is not a positive float

    Examples:
      We know the irradiation for various planet in the solar system and it will
      be usefull to test the formula.

      >>> import math
      >>> SUN_TEMP = 5778.  # in K
      >>> SUN_RADIUS = 6.957e8  # in m

      Let's check for Earth:

      >>> EARTH_A = 1.495978875e11  # in m
      >>> EARTH_IRRAD = 340.  # in W/m²
      >>> EARTH_IRRAD_CALC = _planet_irradiation(SUN_TEMP, SUN_RADIUS, EARTH_A)
      >>> math.isclose(EARTH_IRRAD_CALC, EARTH_IRRAD, rel_tol=1e-2)
      True

      Let's check for Venus:

      >>> VENUS_A= 1.082e11  # in m
      >>> VENUS_IRRAD = 2601.3 / 4.  # in W/m²
      >>> VENUS_IRRAD_CALC = _planet_irradiation(SUN_TEMP, SUN_RADIUS, VENUS_A)
      >>> math.isclose(VENUS_IRRAD_CALC, VENUS_IRRAD, rel_tol=1e-2)
      True

      Let's check for Jupiter:

      >>> JUP_A= 7.78570e11  # in m
      >>> JUP_IRRAD = 50. / 4.  # in W/m²
      >>> JUP_IRRAD_CALC = _planet_irradiation(SUN_TEMP, SUN_RADIUS, JUP_A)
      >>> math.isclose(JUP_IRRAD_CALC, JUP_IRRAD, rel_tol=1e-2)
      True
    """
    if not all(
        [
            isinstance(star_temp, float),
            isinstance(star_radius, float),
            isinstance(planet_sm_axis, float),
            star_temp > 0.0,
            star_radius > 0.0,
            planet_sm_axis > 0.0,
        ]
    ):
        raise TypeError

    # Irradiation formula
    return (SIGMA / 4.0) * star_temp**4 * (star_radius / planet_sm_axis) ** 2


# The output class itself


@dataclass(init=False)
class PolarPlotOutput(OutputBase):
    """
    Class handling the data/JSON generation for the polar plots.
    """

    var: str | None
    parameters: list[_PolarPlotParameter]
    columns: list[Column]

    def __init__(self, filter_string: str | None, var: str | None = "irradiation"):
        super().__init__(filter_string)

        self.var = var
        self.columns = [
            Column(fld_def)
            for fld_def in sorted(POLARPLOT_VAR_FIELD, key=lambda fld: fld["desc"])
        ]

        self.parameters = []
        for column in self.columns:
            my_id = column.get_id()
            desc = column.get_desc()
            datatype = column.get_votable_datatype()
            unit = (
                column.get_votable_unit()
                if datatype == "int" or datatype == "double"
                else ""
            )

            rm = column.get_related_model()
            relmodel = ("planet" if rm is None else rm).capitalize()

            self.parameters.append(
                _PolarPlotParameter(
                    id=my_id,
                    desc=desc,
                    unit=unit,
                    datatype=datatype,
                    related_model=relmodel,
                )
            )

        # We manually add irriadiation since it is not in the model defs
        self.parameters.append(
            _PolarPlotParameter(
                id="irradiation",
                desc="Irradiation compared to the earth",
                unit="",
                datatype="double",
                related_model="Planet",
            )
        )

    def __get_var_model_property(self) -> str | None:
        """Get the exact model field name corresponding to the requested variable."""

        # Shortcut if the var is not defined
        if not self.var:
            return None

        id_to_model_prop = {
            col.get_id(): col.get_model_property() for col in self.columns
        }

        return id_to_model_prop.get(self.var, None)

    # TODO do not return "" but None instead
    def __get_or_compute_planet_attribute(self, planet: PlanetDBView) -> Any:
        # We will always need those values for computation
        period = getattr(planet, "period", "")
        axis = getattr(planet, "axis", "")
        star_mass = getattr(planet, "star_mass", "")
        star_teff = getattr(planet, "star_teff", "")
        star_radius = getattr(planet, "star_radius", "")

        try:
            if self.var == "irradiation":
                return _planet_irradiation(star_teff, star_radius, axis)

            if self.var == "period":
                # Computation based on the Kepler 3th law
                # TODO put this in a testable func
                return period or sqrt(axis**3 / (7.495e-6 * star_mass))

            if self.var == "axis":
                # Computation based on the Kepler 3th law
                # TODO put this in a testable func
                return axis or (7.495e-6 * star_mass * period**2) ** (1 / 3)

            return (
                getattr(planet, var_model_property, "")
                if (var_model_property := self.__get_var_model_property()) is not None
                else ""
            )

        except TypeError:
            return ""

    def get_values(self) -> list[_PolarPlotRowTuple]:
        """
        Returns the actual data needed by the PolarPlot :

        - one header row
        - many data rows
        """
        fields_from_fieldef = [
            "name",
            "radius",
            "mass",
            "star_distance",
            "star_ra",
        ]

        polar_plot_model_prop: dict[str, str] = {
            field: Column(field_def).get_model_property()
            for field, field_def in zip(fields_from_fieldef, POLARPLOT_FIELD)
        }

        values_display = [
            _PolarPlotRowTuple(
                name=getattr(planet, polar_plot_model_prop["name"], ""),
                radius=getattr(planet, polar_plot_model_prop["radius"], ""),
                mass=getattr(planet, polar_plot_model_prop["mass"], ""),
                star_distance=getattr(
                    planet, polar_plot_model_prop["star_distance"], ""
                ),
                star_ra=getattr(planet, polar_plot_model_prop["star_ra"], ""),
                url_keyword=getattr(planet, "url_keyword"),
                var=self.__get_or_compute_planet_attribute(planet),
            )
            for planet in self.planets
        ]

        # We encode planet name because the JSON parser of js doesn't accept some
        # characters like "'" and for historical reasons we need to pass the name with
        # all special characters :sad_face:
        for idx, value in enumerate(values_display):
            values_display[idx] = value._replace(name=url_encode(value.name))

        # TODO do not put the header in the first line... but use a dict instead
        # TODO get the header values from `polar_plot_model_prop`
        header = _PolarPlotRowTuple(
            "name",
            "radius",
            "mass",
            "star_distance",
            "star_ra",
            "url_keyword",
            self.var,
        )
        values_display.insert(0, header)

        return values_display
