# Standard imports
import logging
from dataclasses import dataclass
from math import isnan
from typing import Iterable, NamedTuple, cast
from urllib.parse import quote as url_encode

# External imports
from more_itertools import flatten

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    AXIS_FIELD,
    MASS_FIELD,
    NAME_FIELD,
    OUTPUT_ALL_FIELDS,
    PERIOD_FIELD,
    RADIUS_FIELD,
    Column,
    get_columns_by_id,
)
from ..models import PlanetDBView
from .base import OutputBase

LOGGER = logging.getLogger(__name__)

# Type definitions ####################################################################

PlanetFieldValueType = str | float | list[float] | None


# TODO check why the axis are optional
class AxisId(NamedTuple):
    x: str | None
    y: str | None
    z: str | None
    p: str | None


# TODO add a as_json() method
class PlotOutputHeaderForCol(NamedTuple):
    """Representation of a column header destined to be used by a plot."""

    id: str | None  # TODO check why the id is optional
    desc: str
    unit: str
    datatype: str
    related_model: str


# Utility functions ###################################################################


def _is_inf_or_nan(value: float) -> bool:
    """
    Know if a value is equal to inf, -inf or Nan.

    Args:
        value: Value to test.

    Returns:
        True if value is inf, -inf or Nan. False otherwise.

    Examples:
        >>> _is_inf_or_nan(12.2)
        False

        >>> _is_inf_or_nan(float('inf'))
        True

        >>> _is_inf_or_nan(float('-inf'))
        True

        >>> _is_inf_or_nan(float('Nan'))
        True

        >>> _is_inf_or_nan(None)
        False
    """
    if value is None:
        return False
    return True if abs(value) == float("inf") or isnan(value) else False


def _get_valid_js_value(value: float) -> float | None:
    """
    Get a value in valid format for js json parser.

    Args:
        value: Value to get in good format.

    Returns:
        The value if it's already a valid value, None otherwise.

    Examples:
        >>> _get_valid_js_value(12.2)
        12.2

        >>> _get_valid_js_value(float('inf'))
        None

        >>> _get_valid_js_value(float('-inf'))
        None

        >>> _get_valid_js_value(float('Nan'))
        None

        >>> _get_valid_js_value(None)
        None
    """
    return None if _is_inf_or_nan(value) else value


def get_scatter_headers() -> tuple[list[PlotOutputHeaderForCol], AxisId]:
    all_fields_def = sorted(OUTPUT_ALL_FIELDS, key=lambda fld: fld["desc"])

    default_axis = AxisId(
        x=Column(MASS_FIELD).get_id(),
        y=Column(PERIOD_FIELD).get_id(),
        z=Column(RADIUS_FIELD).get_id(),
        p=Column(AXIS_FIELD).get_id(),
    )

    # TODO use a list comprehension here
    headers = []
    for field_def in all_fields_def:
        headerCol = Column(field_def)
        datatype = headerCol.get_votable_datatype()

        if datatype in ["int", "double"]:
            relmodel = (headerCol.get_related_model() or "planet").capitalize()
            headers.append(
                PlotOutputHeaderForCol(
                    id=headerCol.get_id(),
                    desc=headerCol.get_desc(),
                    unit=headerCol.get_votable_unit(),
                    datatype=datatype,
                    related_model=relmodel,
                )
            )

    return headers, default_axis


# TODO Add a doctest
def json_header_from_axis_fields(axis_fields: list[str]) -> list[str]:
    """
    Return a list of fields ["x","x_emin","y_min"...]
    containing the header of data
    """
    json_header: list[str] = []
    for axis in axis_fields:
        json_header.extend((axis, f"{axis}_emin", f"{axis}_emax"))

    # We now add the mandatory elements at the end of the header
    # TODO change those realy stupid one letter keys, they are cryptic
    json_header.append("n")  # planet name
    json_header.append("u")  # planet url keyword (aka planet slug)
    json_header.append("detection_method")  # planet detection method

    return json_header


@dataclass(init=False)
class PlotsOutput(OutputBase):
    """
    Class handling the data/JSON generation for the histogram/scatter plots.
    """

    axis_fields: dict[str, Column]
    planets_as_dict: Iterable[dict[str, PlanetFieldValueType]]

    # TODO improve the types maybe using NamesTuple or TypedDict
    def __init__(self, filter_string: str | None, field_ids: AxisId):
        super().__init__(filter_string)

        self.axis_fields = {
            axis: cast(Column, get_columns_by_id().get(axis_id))
            for axis, axis_id in field_ids._asdict().items()
            if axis_id
        }

        # Update the planet order
        self.planets = self.planets.order_by(NAME_FIELD["model_field"])

        fields_names_to_retrieve = self._get_field_names_for_all_axis()

        # We add the name and the detction type to the columns
        fields_names_to_retrieve.append("detection_type")
        fields_names_to_retrieve.append("name")

        self.planets_as_dict = self.planets.values(
            *[
                field_name
                for field_name in fields_names_to_retrieve
                if field_name is not None
            ]
        )

    def _get_field_names_for_all_axis(self) -> list[str | None]:
        # Each queryset_value_field is a list so this is a list of lists
        all_queryset_value_lists = [
            v.get_queryset_value_field() for v in self.axis_fields.values()
        ]
        # We just flatten the list of lists
        # See: # See: https://stackoverflow.com/a/953097
        return list(flatten(all_queryset_value_lists))

    def _get_json_header(self) -> list[str]:
        """
        Return a list of fields ["x","x_emin","y_min"...]
        containing the header of data
        """
        return json_header_from_axis_fields(list(self.axis_fields.keys()))

    # TODO: Test this
    def _get_json_planet_row(
        self,
        planet_as_dict: dict[str, PlanetFieldValueType],
        planet: PlanetDBView,
    ) -> list[PlanetFieldValueType] | None:
        """
        Get values of axis filleds from a planet.

        Args:
            planet_as_dict: Planet to get values from represented as a dict (with only
                the needed fields)
            planet: Planet object to get values from

        Returns:
            Values from a planet for fields axis. Or None if x,y are not defined.
        """
        planet_values = []

        for axis, field in self.axis_fields.items():
            # Compute the values
            value = planet_as_dict[field.get_model_field()]

            if value is None and axis == "x":
                return None

            errors_value = (
                planet_as_dict.get(get_error_of_field, None)
                if (get_error_of_field := field.get_error()) is not None
                else None
            )

            error_min: int | float | None = None
            error_max: int | float | None = None

            if errors_value:
                match errors_value:
                    case [err_min, err_max]:
                        if err_min == float("inf") and isnan(cast(float, err_max)):
                            error_min = cast(float, value)
                            error_max = 0
                        else:
                            error_min = _get_valid_js_value(cast(float, err_min))
                            error_max = _get_valid_js_value(cast(float, err_max))
                    case [err]:
                        error_min = _get_valid_js_value(cast(float, err))
                        error_max = _get_valid_js_value(cast(float, err))
                    # BUG: No value with more than 2 error value.
                    case list() if len(errors_value) in [2, 3]:
                        LOGGER.warning(
                            "Len of errors_value is %d. Its a bug an error field must "
                            "be of size 0, 1 or 2",
                            len(errors_value),
                        )
                    case _:
                        raise IndexError(
                            "/!\\ wtf is this `errors_value`, we don't know how to "
                            f"treat that: {errors_value}"
                        )

            # Insert the computed values
            planet_values.append(value)
            planet_values.append(error_min)
            planet_values.append(error_max)

        # Add name, slug of name and detection type at the end
        # This is displayed on the plot

        # We encode planet name because the JSON parser of js doesn't accept some
        # characters like "'" and for historical reasons we need to pass the name with
        # all special characters :sad_face:
        planet_values.append(
            url_encode(cast(str, planet_as_dict["name"]))
        )  # planet.name

        # This is for the url request
        planet_values.append(planet.url_keyword)

        # This is for the detection type
        planet_values.append(planet.detection_type)

        return planet_values

    # WTF This structure is pure nonsense: the header is a list in a one element list!
    # TODO Replace that nonsense with something more standard (and keep this only for
    # backwarp compatibility)
    # See: http://htsql.org/blog/2011/discuss-headers.html
    def get_values(self) -> list[list[PlanetFieldValueType] | None]:
        """
        Create a list of list (JSON like but in pure python types).

        - The size of the list is the number of planets + 1.
        - The first element is the list of the field
          names ['x','x_emin','y_min'...] of the planets.
        - Then each element is a list of values for a given planet.

        When for a planet the value of 'x' are undefined, then
        the cooresponding list will be set to None.
        """
        # First we build the header
        # WTF We put the header list in... a list just to have a list of list
        header = self._get_json_header()

        # Then we build the values
        values = [
            self._get_json_planet_row(planet_as_dict, planet)
            for planet_as_dict, planet in zip(self.planets_as_dict, self.planets)
        ]

        # Return header and values join has a single list
        result = values
        result.insert(0, cast(list[PlanetFieldValueType], header))

        return result
