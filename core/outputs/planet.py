"""All the thing necessary to output a planet as list of dicts"""

# Standard imports
from dataclasses import dataclass
from itertools import groupby
from typing import TypedDict, cast

# Django 💩 imports
from django.db.models.query import QuerySet

# First party imports
from core.models import (
    AtmosphereMolecule,
    AtmosphereTemperature,
    Plan2PubParam,
    Planet,
    Planet2Publication,
    PlanetarySystem,
    PlanetDBView,
    Publication,
    Star,
    Star2Publication,
    Star2PubParam,
    WebStatus,
)

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    PLANET_FIELDS,
    PLANET_PLANETARY_SYSTEM_FIELDS,
    PLANET_STAR_FIELDS,
    Column,
)
from ..templatetags import format_value_with_error
from .base import OutputBase

# Type definitions


class SystemFieldDescDict(TypedDict):
    """Used to describe a field of a planetary system"""

    # Name of the field
    name: str | None

    # Table header as defined in Column class (most of the time by the
    # `planet_output_header` field in the `field_definitions.yaml` file
    header: str

    # Value of the field
    value: str


class PublicationDescDict(TypedDict):
    "Dict minimal representation of a publication"
    id: int
    title: str


class PlanetFieldDict(TypedDict):
    "Dict representation of planet field with name and value"
    name: str | None
    header: str
    value: str
    publications: list[Publication]


class StarFieldDict(TypedDict):
    "Dict representation of star field with name and value"
    name: str | None
    header: str
    value: str
    publications: list[PublicationDescDict]


class StarDict(TypedDict):
    "Dict representation of star with name and value"
    name: str
    fields: list[StarFieldDict]
    ads_search_parameter: str
    nb_planets_system: int
    remarks: str | None
    other_web: str | None
    url_simbad: str | None
    url_keyword: str


# Helper functions ####################################################################


def _by_planet_parameter(pub: Planet2Publication) -> Plan2PubParam:
    return cast(Plan2PubParam, pub.parameter)


def _by_star_parameter(pub: Star2Publication) -> Star2PubParam:
    return cast(Star2PubParam, pub.parameter)


# TODO check if typing is good for star_publications: Publication or Star2Publication
def _to_star_publications_dict(
    star2publications: QuerySet[Star2Publication],
) -> dict[Star2PubParam, list[Star2Publication]]:
    star_publications_sorted = sorted(star2publications, key=_by_star_parameter)

    star_publications_dict = {
        param: list(pubs)
        for param, pubs in groupby(star_publications_sorted, key=_by_star_parameter)
    }

    return star_publications_dict


def _to_planet_publications_dict(
    planet2publications: QuerySet[Planet2Publication],
) -> dict[Plan2PubParam, list[Publication]]:
    # We need to sort the QuerySet before doing the groupby
    planet2publications_sorted: list[Planet2Publication] = sorted(
        planet2publications,
        key=_by_planet_parameter,
    )

    planet_publications_dict: dict[Plan2PubParam, list[Publication]] = {
        param: [pl2pub.publication for pl2pub in all_pl2pub]
        for param, all_pl2pub in groupby(
            planet2publications_sorted,
            key=_by_planet_parameter,
        )
    }

    return planet_publications_dict


# TODO use a Planet instead of PlanetDBView ASA we changed it in the whole PlanetOutput
def _get_object_field_value(
    obj: Star | PlanetarySystem | PlanetDBView,
    field: Column,
) -> str:
    """
    Get a clean HTML compatible representation of the value with errors and units if
    available.
    """
    value = getattr(obj, field.get_table_property(), None)

    if value is None:
        return ""

    value_error = getattr(obj, field.get_table_error_property(), None)

    result_no_unit = format_value_with_error(value, value_error)

    html_unit = field.get_html_unit()
    if not html_unit:
        return result_no_unit

    return f"{result_no_unit} {html_unit}"


# Main class ##########################################################################


# TODO replace this class by a simple function since it is used like one
@dataclass(init=False)
class PlanetOutput(OutputBase):
    # TODO Do our query on a Planet model not on PlanetDBView model
    # See: OutputBase.planets and OutputBase.__init__()
    planet: PlanetDBView
    """The planet we want to represent"""

    planet_publications_dict: dict[Plan2PubParam, list[Publication]]
    """A mapping linking a Plan2PubParam to the corresponding publications"""

    atmosphere_molecules: QuerySet[AtmosphereMolecule]
    """Molecules present in the atmosphere of this planet"""

    stars: QuerySet[Star]
    """Stars of the system of this planet"""

    stars_publications: dict[
        str, dict[Star2PubParam, list[Star2Publication] | Publication]
    ]
    """Publications about the stars of the planet"""

    pubs_display: list[Publication]
    """All the publications to show for this planet"""

    planetary_system: PlanetarySystem
    """Planetary system of this planet"""

    planet_fields: list[Column]
    """Header information for the different fields describing a planet"""

    star_fields: list[Column]
    """Header information for the different fields describing a star"""

    system_fields: list[Column]
    """Header information for the different fields describing a planetary system"""

    def __init__(self, planet_id: int):
        super().__init__()

        self.planet = self.planets.get(id=planet_id)

        # TODO remove the filter: we should not retreive data only for active planets
        planet_object = Planet.objects.filter(status=WebStatus.ACTIVE).get(
            id=self.planet.id
        )

        planet2publications: QuerySet[Planet2Publication] = (
            planet_object.planet2publication_set.all()
            .prefetch_related("publication")
            .order_by("-publication__date")
        )

        # Let's add the planet publications to a temp set of publication for this planet
        # later we will convert that to a list (this prevent duplication)
        pub_display_set: set[Publication] = set()
        pub_display_set.update(pl2pub.publication for pl2pub in planet2publications)

        self.planet_publications_dict = _to_planet_publications_dict(
            planet2publications
        )

        # Atmospheric temperature of this planet
        atmosphere_temperatures: QuerySet[AtmosphereTemperature] = (
            planet_object.atmospheretemperature_set.all()
            .prefetch_related("publication")
            .order_by("type", "-publication__date")
        )
        pub_display_set.update(
            atm_temp.publication for atm_temp in atmosphere_temperatures
        )

        # Atmospheric molecule of this planet
        self.atmosphere_molecules = (
            planet_object.atmospheremolecule_set.all()
            .prefetch_related("molecule")
            .prefetch_related("publication")
            .order_by("molecule__name", "type", "-publication__date")
        )
        pub_display_set.update(
            atm_mol.publication for atm_mol in self.atmosphere_molecules
        )

        # Let's handle the stars of this planet now
        self.stars = planet_object.stars.all()

        self.stars_publications = {}
        for star in self.stars:
            star2publications: QuerySet[Star2Publication] = (
                # Star has an automatic attribute `star2publication_set` inherite from
                # django model (Django == 💩)
                star.star2publication_set.all()  # type: ignore[attr-defined]
                .prefetch_related("publication")
                .order_by("modified")
            )
            pub_display_set.update(
                star_pub.publication for star_pub in star2publications
            )

            self.stars_publications[star.name] = cast(
                dict[Star2PubParam, list[Star2Publication] | Publication],
                _to_star_publications_dict(star2publications),
            )

        # We can now get all the publications as a clean sorted list
        self.pubs_display = sorted(pub_display_set, key=lambda p: p.date, reverse=True)

        self.planetary_system = planet_object.planetary_system
        self.planet_fields = [Column(field_def) for field_def in PLANET_FIELDS]
        self.star_fields = [Column(field_def) for field_def in PLANET_STAR_FIELDS]
        self.system_fields = [
            Column(field_def) for field_def in PLANET_PLANETARY_SYSTEM_FIELDS
        ]

    def get_main_star_url_keyword(self) -> str:
        return cast(
            str,
            Star.objects.filter(status=WebStatus.ACTIVE)
            .get(name__iexact=self.planet.star_name)
            .url_keyword,
        )

    # TODO refactor this... it is way to complicated
    def get_planet_fields(self) -> list[PlanetFieldDict]:
        """
        Get the description of the fields of the planet as JSON-compatible structured
        list of dicts with clean key and values.
        """
        fields = []

        for field in self.planet_fields:
            pub_parameter = Plan2PubParam.from_label(field.get_id())

            planet_pub_for_param: list[Publication] = (
                self.planet_publications_dict[pub_parameter]
                if pub_parameter in self.planet_publications_dict
                else []
            )

            fields.append(
                PlanetFieldDict(
                    name=field.get_id(),
                    header=field.get_planet_output_header(),
                    value=_get_object_field_value(self.planet, field),
                    publications=planet_pub_for_param,
                )
            )

        return fields

    def get_stars(self) -> list[StarDict]:
        """
        Get the description of stars as JSON-compatible structured list of dicts with
        clean key and values.
        """
        stars: list[StarDict] = []

        for star in self.stars:
            star_publications: dict[
                Star2PubParam, list[Star2Publication] | Publication
            ] = self.stars_publications[star.name]

            fields: list[StarFieldDict] = []

            for field in self.star_fields:
                pub_parameter = Star2PubParam.from_label(field.get_table_model())

                pubs_as_dicts = [
                    PublicationDescDict(
                        id=pub.publication.id,
                        title=pub.publication.title,
                    )
                    for pub in cast(dict, star_publications).get(pub_parameter, [])
                ]

                fields.append(
                    StarFieldDict(
                        name=field.get_id(),
                        header=field.get_planet_output_header(),
                        value=_get_object_field_value(star, field),
                        publications=pubs_as_dicts,
                    )
                )

            stars.append(
                StarDict(
                    name=star.name,
                    fields=fields,
                    ads_search_parameter=star.ads_search_parameter,
                    nb_planets_system=(
                        # Star has an automatic attribute `planet` with the Foreign
                        # Key Field (Django == 💩)
                        star.planets.filter(  # type: ignore[attr-defined]
                            status=WebStatus.ACTIVE
                        )
                        .filter(planet_status=WebStatus.ACTIVE)
                        .count()
                    ),
                    remarks=star.remarks,
                    other_web=star.other_web,
                    url_simbad=star.url_simbad,
                    url_keyword=star.url_keyword,
                )
            )

        return stars

    def get_system_fields(self) -> list[SystemFieldDescDict]:
        """
        Get the description of the fields of the planetary system as JSON-compatible
        structured list of dicts with clean key and values.
        """
        fields = [
            SystemFieldDescDict(
                name=field.get_id(),
                header=field.get_planet_output_header(),
                value=_get_object_field_value(self.planetary_system, field),
            )
            for field in self.system_fields
        ]

        return fields
