# Standard imports
from collections import namedtuple
from typing import TypeAlias, cast

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    EPHEMERIDES_FIELDS,
    FieldDefinition,
    ValDepOnMassUnit,
    ValDepOnRadiusUnit,
)
from ..models import PlanetDBView
from .base import OutputBase

FieldModelPropType: TypeAlias = str | ValDepOnMassUnit | ValDepOnRadiusUnit
EphemeridesType: TypeAlias = dict[str, dict[str, str]]
AdaptedName = namedtuple("AdaptedName", ["old", "new"])


# TODO: Rename attribute with pla or st
class AdaptedFieldName:
    """
    AdaptedFieldName class.

    This class is used to get the old name of a field for the old ephemerides js.

    Attributes:
        pass
    """

    st_mass: AdaptedName = AdaptedName(old="stars__mass", new="star_mass")
    st_radius: AdaptedName = AdaptedName(old="stars__radius", new="star_radius")
    st_teff: AdaptedName = AdaptedName(old="stars__teff", new="star_teff")
    st_distance: AdaptedName = AdaptedName(old="stars__distance", new="star_distance")
    st_ra: AdaptedName = AdaptedName(old="longit", new="star_ra_deg")
    pla_radius: AdaptedName = AdaptedName(old="radius", new="rjup")

    def is_adapted_field_id(self, field_id: str) -> bool:
        """
        Define if a field id is a field who need to be adapted or not.

        Args:
            field_id: Id of the field which we want to know whether it needs to be
                adapted or not.

        Returns:
            True id the field need to be adapted, False otherwise.

        Examples:
            >>> adaptor = AdaptedFieldName()
            >>> adaptor.is_adapted_field_id("toto")
            False

            >>> adaptor = AdaptedFieldName()
            >>> adaptor.is_adapted_field_id("name")
            False

            >>> adaptor = AdaptedFieldName()
            >>> adaptor.is_adapted_field_id("star_mass")
            True
        """
        attributes = filter(
            lambda attr: (
                not attr.startswith("__") and not callable(getattr(self, attr))
            ),
            dir(self),
        )

        for attr in attributes:
            if field_id == self.__getattribute__(attr).new:
                return True

        return False

    def get_old_field_id(self, field_id: str) -> str:
        """
        Get the old field id form the new field id.

        Args:
            field_id: New field id of the field that we want to adapt.

        Returns:
            Old field id of the given field id.

        Raise:
            ValueError if the given field id does not need to be adapt.

        Examples:
            >>> adaptor = AdaptedFieldName()
            >>> try:
            ...     adaptor.get_old_field_id("name")
            ... except ValueError:
            ...     print("we raise")
            'we raise'

            >>> adaptor = AdaptedFieldName()
            >>> adaptor.get_old_field_id("star_mass")
            'stars__mass'

            >>> adaptor = AdaptedFieldName()
            >>> adaptor.get_old_field_id("rjup")
            'radius'

        """
        if not self.is_adapted_field_id(field_id):
            raise ValueError(f"{field_id} is not an old id who need to be adapted.")

        attributes = filter(
            lambda attr: (
                not attr.startswith("__") and not callable(getattr(self, attr))
            ),
            dir(self),
        )

        for attr in attributes:
            if field_id == self.__getattribute__(attr).new:
                return cast(str, self.__getattribute__(attr).old)

        raise ValueError(f"Can't find the old field id for {field_id}.")


def _get_field_model_prop(
    field: FieldDefinition,
) -> FieldModelPropType | None:
    """
    Get field model propertey.

    The model property is one of the values of the following field keys, in this order:
        - catalog_model_property
        - model_property
        - model_field

    Args:
        field: Given field from which we want the model property.

    Returns:
        The model property of the given field or `None`
        if the field have not a model property.

    Examples:
        >>> field = dict(
        ...     catalog_model_property = "toto",
        ...     model_property = "titi",
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_model_prop(field)
        'toto'

        >>> field = dict(
        ...     model_property = "titi",
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_model_prop(field)
        'titi'

        >>> field = dict(
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_model_prop(field)
        'tutu'

        >>> field = dict(
        ...     other = "tata",
        ... )
        >>> _get_field_model_prop(field)
        None

        >>> field = {
        ...     "model_field": {
        ...         "rjup": "radius_rjup",
        ...         "rearth": "radius_rearth",
        ...     },
        ...     "other": "tata",
        ... }
        >>> _get_field_model_prop(field)
        {'rjup': 'radius_rjup', 'rearth': 'radius_rearth'}
    """
    order = ["catalog_model_property", "model_property", "model_field"]

    for element in order:
        result = field.get(element, None)
        if result is not None:
            return cast(FieldModelPropType, result)

    return None


def _get_field_id(
    field: FieldDefinition,
) -> FieldModelPropType | None:
    """
    Get field id.

    The field id is one of the values of the following field keys, in this order:
        - id
        - catalog_model_property
        - model_property
        - model_field

    Args:
        field: Given field from which we want the id.

    Returns:
        The id of the given field or `None`
        if the field have not a id.

    Examples:
        >>> field = dict(
        ...     id = "its id",
        ...     catalog_model_property = "toto",
        ...     model_property = "titi",
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_id(field)
        'its id'

        >>> field = dict(
        ...     catalog_model_property = "toto",
        ...     model_property = "titi",
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_id(field)
        'toto'

        >>> field = dict(
        ...     model_property = "titi",
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_id(field)
        'titi'

        >>> field = dict(
        ...     model_field = "tutu",
        ...     other = "tata",
        ... )
        >>> _get_field_id(field)
        'tutu'

        >>> field = dict(
        ...     other = "tata",
        ... )
        >>> _get_field_id(field)
        None
    """
    field_id = field.get("id", None)
    return field_id if field_id is not None else _get_field_model_prop(field)


class EphermeridesOutput(OutputBase):
    """
    Ephemerides Output class.

    Attributes:
        fields: Fields used in ephemerides.
    """

    fields: list[FieldDefinition] = EPHEMERIDES_FIELDS

    def get_values(self, planet: PlanetDBView) -> EphemeridesType:
        """
        Get values from ephemerides form a planet.

        Args:
            planet: Planet from which we want the data.

        Returns:
            Ephemeride data from the given planet.

        Raise:
            ValueError: Indirect raised by: `AdaptedFieldName.get_old_field_id`.
        """
        data = {}
        adaptor = AdaptedFieldName()
        for current_field in self.fields:
            field_id = _get_field_id(current_field)
            model_prop = _get_field_model_prop(current_field)

            if model_prop is not None:
                properties: list[str] = []
                ids: list[str | None] = []
                match model_prop:
                    case dict():
                        _model_prop_with_unit = cast(dict[str, str], model_prop)
                        ids = list(_model_prop_with_unit.keys())
                        properties = list(_model_prop_with_unit.values())
                    case str():
                        # HACK: We know from the structure of the data that HERE
                        # field_id is str or None, But this code is bat crap crazzy
                        # "I know because I know 💩"
                        ids = [cast(str | None, field_id)]
                        properties = [model_prop]

                for f_id, prop in zip(ids, properties):
                    if f_id is not None:
                        if adaptor.is_adapted_field_id(f_id):
                            f_id = adaptor.get_old_field_id(f_id)
                    value = getattr(planet, prop)
                    if value is not None:
                        errors = current_field.get("error", None)
                        if errors is not None:
                            match errors:
                                case dict() if isinstance(model_prop, dict):
                                    _errors_with_unit = cast(dict[str, str], errors)
                                    error_properties = list(_errors_with_unit.values())
                                case str() if isinstance(model_prop, str):
                                    error_properties = [errors]
                                case _:
                                    raise ValueError(
                                        "model_property and errors should be of the "
                                        "same type"
                                    )

                            for err in error_properties:
                                value_error = getattr(planet, err)
                                if value_error is None or value_error == []:
                                    # HACK: HERE f_id can't be known because we (not me)
                                    # re-assign f_id if it's None at the line: 301
                                    data[cast(str, f_id)] = {"value": str(value)}
                                elif len(value_error) == 2:
                                    # HACK: HERE f_id can't be known because we (not me)
                                    # re-assign f_id if it's None at the line: 301
                                    data[cast(str, f_id)] = dict(
                                        value=str(value),
                                        error_min=str(value_error[0]),
                                        error_max=str(value_error[1]),
                                    )
                                else:
                                    # HACK: HERE f_id can't be known because we (not me)
                                    # re-assign f_id if it's None at the line: 301
                                    data[cast(str, f_id)] = dict(
                                        value=str(value),
                                        error_min=str(value_error[0]),
                                        error_max=str(value_error[0]),
                                    )
                        else:
                            # HACK: We know from the structure of the data that HERE
                            # field_id is an str, But this code is bat crap crazzy
                            # "I know because I know 💩"
                            data[cast(str, field_id)] = dict(value=str(value))
        return data
