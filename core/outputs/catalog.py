# Standard imports
from dataclasses import dataclass
from typing import NamedTuple, TypedDict, cast

# Django 💩 imports
from django.core.paginator import Page, Paginator
from django.db.models.query import QuerySet

# Local imports
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    CATALOG_ALL_FIELDS,
    CATALOG_COMPACT_FIELDS,
    MODIFIED_FIELD,
    Column,
    FieldDefinition,
)

# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..filter_grammar import createFilter  # type: ignore[attr-defined]
from ..filter_grammar import (  # type: ignore[attr-defined]
    DetectionFilter,
    PlanetStatusFilter,
)
from ..models import PlanetDBView
from ..templatetags import format_value_with_error
from .base import OutputBase

# Type definitions


@dataclass
class PaginationInfo:
    """To store pagination information"""

    start: int
    length: int

    def __post_init__(self) -> None:
        if self.length <= 0:
            raise ValueError("length must be strictly positive.")


class UnitsInfo(TypedDict, total=False):
    """Used to specify the units to use when creating an output."""

    # Unit to use for the "mass" values
    mass: str

    # Unit to use for the "mass sin i" values
    mass_sini: str

    # Unit to use for the "radius" values
    radius: str


class HeaderInfo(TypedDict):
    """Header info corresponding to each field we want to display."""

    # a unique string identifier (no spaces) for the field
    id: str

    # (for legacy templates) a full header with HTML formated name and units
    text: str

    # a string representing the data type of the field (to know how to sort)
    sort: str

    # a long version of the name of the field.
    desc: str

    # a short version of the unit of the field. It can be used to index the header_unit
    # in the field_desc.
    # For example:`fields_desc["header_unit"][unit]`
    unit: str

    # (for new templates) a clean full name with no HTML or units
    # In the old template this was integrated in the `text` field
    new_text: str

    # (for new templates) an HTML link for the unit
    # In the old template this was integrated in the `text` field
    new_unit_link: str


class SortInfo(TypedDict):
    """Information needed to express a sort for an output"""

    # Index of the column to use for sorting results
    column_id: int

    # Direction of the sort:
    # - True for descending
    # - False for ascending
    desc_order: bool


class TemplateData(NamedTuple):
    """All the info needed to display a DataTable table"""

    # A list of HeaderInfo each corresponding to a specific column to display
    all_header_info: list[HeaderInfo]

    # `all_header_info` index of the field that should be used to sort the view.
    default_sort: int

    # true if `filter_string` provided  has a syntax error.
    filter_syntax_error: bool


# Main class ###########################################################################


@dataclass(init=False)
class CatalogOutput(OutputBase):
    DEFAULT_SORT = Column(MODIFIED_FIELD)

    display_errors: bool
    columns: list[Column]
    page: Page | QuerySet

    def __init__(
        self,
        all_fields: bool,
        filter_string: str | None,
        sort: SortInfo | None = None,
        pagination: PaginationInfo | None = None,
        units: UnitsInfo = UnitsInfo(),
        search: str | None = None,
        statusfilter: DetectionFilter | None = None,
        detectionfilter: PlanetStatusFilter | None = None,
    ):
        """
        Args:
            all_fields: tells if all possible fields should be displayed or just enough
                for the compact view.
            filter_string: filter string to limit the planet to display
            sort: informations needed to sort the results
            units: dict that specify the units to use
            search: search string to specify which planets to display
            pagination: information needed to do the pagination
            statusfilter: list of PLANET_STATUSES values to use for filtering the
                results
            detectionfilter: list of PlanetDetection indexes used to filter the results
        """
        # We display errors only if we show all columns
        self.display_errors = all_fields

        self.columns = []
        for field_def in CatalogOutput._get_catalog_fields_list(all_fields):
            col = Column(field_def)
            col.unit = cast(str | None, units.get(col.get_id(), col.unit))
            self.columns.append(col)

        super().__init__(
            filter_string=filter_string,
            statusfilter=statusfilter,
            detectionfilter=detectionfilter,
            search=search,
        )

        if sort:
            self._set_sort(column_id=sort["column_id"], desc_order=sort["desc_order"])

        if pagination:
            self._set_paginator(pagination)

    @staticmethod
    def _get_catalog_fields_list(all_fields: bool) -> list[FieldDefinition]:
        return (
            cast(list[FieldDefinition], CATALOG_ALL_FIELDS)
            if all_fields
            else cast(list[FieldDefinition], CATALOG_COMPACT_FIELDS)
        )

    @staticmethod
    def get_template_data(all_fields: bool, filter_string: str | None) -> TemplateData:
        """
        Retrieve all the data needed to build a catalogue page template.

        To build any page representing the catalogue we use templates. Those
        templates need some pieces of information: field name, units, sort
        order, etc. This function retrieve those from the catalogue field
        descriptions.

        Args:
            all_fields: if true we will retrieve all possible field info (this
                correspond to the full view of the catalogue) else we retrieve only the
                field present in the compact view.
            filter_string: A string representing the filtering rules to apply to the
                catalogue.

        Returns:
            a TemplateData instance filed according to the input args and with
            eventually syntax error info
        """
        # This is what we need to build the TemplateData to return
        all_header_info = []
        default_sort = 0
        filter_syntax_error = False

        default_sort_id = CatalogOutput.DEFAULT_SORT.get_id()

        all_field_desc = CatalogOutput._get_catalog_fields_list(all_fields)

        for idx, field_desc in enumerate(all_field_desc):
            column = Column(field_desc)

            # If this field is the default sort then we store its index
            if column.get_id() == default_sort_id:
                default_sort = idx

            # Let's build the header...
            header = HeaderInfo(
                id=column.get_id(),
                text=column.get_catalog_header(),
                sort=column.get_type() or "nosort",
                desc=column.get_desc(),
                unit=column.unit or "",
                new_text=column.get_raw_catalog_header(),
                new_unit_link=column.get_unit_header(),
            )

            # ...and add it
            all_header_info.append(header)

        # We check if there is a syntax error in the filter string
        if filter_string:
            _, filter_syntax_error = createFilter(filter_string)

        # Finally we return everything we've built
        return TemplateData(
            all_header_info=all_header_info,
            default_sort=default_sort,
            filter_syntax_error=filter_syntax_error,
        )

    def _set_sort(self, column_id: int, desc_order: bool) -> None:
        model_field = self.columns[column_id].get_model_field()
        default_sort = self.DEFAULT_SORT.get_model_field()
        sorted_column = model_field
        if desc_order:
            sorted_column = f"-{sorted_column}"
            default_sort = f"-{default_sort}"

        self.planets = self.planets.order_by(sorted_column, default_sort)

    def _set_paginator(self, pagination: PaginationInfo) -> None:
        if pagination.length != -1:
            self.paginator = Paginator(self.planets, pagination.length)
            page_number = (pagination.start / pagination.length) + 1
            self.page = self.paginator.get_page(page_number)
        else:
            self.paginator = None
            self.page = self.planets

    def _get_planet_field_values(
        self,
        planet: PlanetDBView,
        column: Column,
    ) -> str | None:
        value = getattr(planet, column.get_catalog_model_property(), None)

        if (
            value is not None
            and self.display_errors
            and isinstance((error_method := column.get_catalog_error_property()), str)
        ):
            value_error = getattr(planet, error_method)
            value = format_value_with_error(value, value_error)

        return value

    def _get_planet_values(self, planet: PlanetDBView) -> list[str | None]:
        return [self._get_planet_field_values(planet, col) for col in self.columns]

    def get_values(self) -> list[list[str | None]]:
        return [self._get_planet_values(planet) for planet in self.page]
