"""All the things necessary to output the catalog as a standardized VOTable."""

# Standard imports
from dataclasses import dataclass
from itertools import chain
from typing import Any

# External imports
from astropy.io.votable.tree import CooSys as AstroCooSys
from astropy.io.votable.tree import Field as AstroField
from astropy.io.votable.tree import Resource as AstroResource
from astropy.io.votable.tree import Table as AstroTable
from astropy.io.votable.tree import VOTableFile as AstroVOTableFile

# Local imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
# pylint: disable=no-name-in-module
from ..catalog_field_def import (  # type: ignore[attr-defined]
    NAME_FIELD,
    OUTPUT_ALL_FIELDS,
    Column,
)
from ..models import PlanetDBView
from .base import OutputBase


@dataclass
class VOFieldInfo:
    """Represent all the caracteristics of a VOTable Field"""

    name: str
    """Name of the fied. Will be used as id for it so must be unique in the table."""

    datatype: str
    """
    Nature of the data that is described.

    See: https://www.ivoa.net/documents/VOTable/20191021/REC-VOTable-1.4-20191021.html#primitives  # noqa: E501
    """

    arraysize: str | None
    """
    Exists when the corresponding table cell contains more than one of the specified.
    datatype.

    See: https://www.ivoa.net/documents/VOTable/20191021/REC-VOTable-1.4-20191021.html#sec:dim  # noqa: E501
    """

    unit: str
    """
    Units in which the values of the corresponding column are expressed.

    See: https://www.ivoa.net/documents/VOTable/20191021/REC-VOTable-1.4-20191021.html#sec:unit  # noqa: E501
    """

    ucd: str
    """
    Standardized classification of the physical quantity expressed in the column.

    See: https://www.ivoa.net/documents/VOTable/20191021/REC-VOTable-1.4-20191021.html#sec:ucd  # noqa: E501
    """

    desc: str
    """
    Human readable description of the field.

    See: https://www.ivoa.net/documents/VOTable/20191021/REC-VOTable-1.4-20191021.html#ToC25  # noqa: E501
    """


@dataclass
class VOTableFieldPack:
    """
    Represent a group of actual VOTable field representing a specific column of the
    catalog.

    It represent either:

    - one VO Field representing the value
    - three VO Field: value, error min, error max
    """

    name: str
    """Name of the field in the model"""

    model_property: str
    """Name of the corresponding attribute in the model"""

    error_model_property: str | None
    """Name of the error attribute (if it exist) in the model"""

    vo_field_infos: tuple[VOFieldInfo] | tuple[VOFieldInfo, VOFieldInfo, VOFieldInfo]
    """
    All the fields info corresponding to the column:
    (value) or (value, err min, err max)
    """

    def __init__(self, field: Column):
        self.name = field.get_file_header()
        self.model_property = field.get_file_model_property()
        self.error_model_property = field.get_error()

        self._init_vo_field_infos(field)

    def _init_vo_field_infos(self, field: Column) -> None:
        value_info = VOFieldInfo(
            name=field.get_file_header(),
            datatype=field.get_votable_datatype(),
            arraysize=field.get_votable_arraysize(),
            unit=field.get_votable_unit(),
            ucd=field.get_votable_ucd(),
            desc=field.get_desc(),
        )

        if self.error_model_property:
            error_ucd = field.get_error_ucd()
            error_desc = field.get_error_desc()
            error_name = field.get_file_header_error()

            self.vo_field_infos = (
                value_info,
                VOFieldInfo(
                    name=f"{error_name}_min",
                    datatype=value_info.datatype,
                    arraysize=value_info.arraysize,
                    unit=value_info.unit,
                    ucd=error_ucd,
                    desc=error_desc,
                ),
                VOFieldInfo(
                    name=f"{error_name}_max",
                    datatype=value_info.datatype,
                    arraysize=value_info.arraysize,
                    unit=value_info.unit,
                    ucd=error_ucd,
                    desc=error_desc,
                ),
            )
        else:
            self.vo_field_infos = (value_info,)

    def get_values_for_planet(
        self, planet: PlanetDBView
    ) -> tuple[Any] | tuple[Any, Any, Any]:
        """
        Get the values of this field for a specified planet.

        It can be a list of :

        -   one value (just the value)
        -   three values (val, error min, error max) if the field has errors
        """
        value = getattr(planet, self.model_property)

        if not self.error_model_property:
            return (value,)

        match getattr(planet, self.error_model_property):
            case [err]:
                return (value, err, err)
            case [err_min, err_max]:
                return (value, err_min, err_max)
            case _:
                return (value, None, None)


@dataclass(init=False)
class VOTableOutput(OutputBase):
    """The actual output to generate the VOTable file content."""

    _vo_field_packs: list[VOTableFieldPack]
    """All the fields (some are triple fields with their errors)"""

    votable: AstroVOTableFile
    """XML representation of VOTable"""

    def __init__(
        self,
        vo_table_desc: str,
        vo_table_id: str,
        filter_string: str | None,
        statusfilter: list[str] | None = None,
        detectionfilter: list[str] | None = None,
        search: str | None = None,
    ):
        super().__init__(
            filter_string=filter_string,
            statusfilter=statusfilter,
            detectionfilter=detectionfilter,
            search=search,
        )

        # Filling the vo fields from colums definitions
        self._vo_field_packs = [
            VOTableFieldPack(Column(field_def)) for field_def in OUTPUT_ALL_FIELDS
        ]

        # Sorting the planets by name
        self.planets = self.planets.order_by(NAME_FIELD["model_field"])

        # Create a new VOTable file...
        self.votable = AstroVOTableFile()

        # ...with one resource...
        resource = AstroResource(name=vo_table_id)
        resource.extra_attributes["description"] = vo_table_desc
        coosys = AstroCooSys(ID="J2000", equinox="J2000", system="eq_FK5")
        resource.coordinate_systems.append(coosys)
        self.votable.resources.append(resource)

        # ... with one table
        table = AstroTable(self.votable)
        resource.tables.append(table)

        # Define all the table fields
        for vo_field_pack in self._vo_field_packs:
            for infos in vo_field_pack.vo_field_infos:
                vofield = AstroField(
                    self.votable,
                    name=infos.name,
                    datatype=infos.datatype,
                    ucd=infos.ucd,
                    arraysize=infos.arraysize,
                    unit=infos.unit,
                )
                # We need to add the description separately since it is a XML element
                # and not an attribute (descrption adding is not documented in astropy
                # so you just have to know it)
                vofield.description = infos.desc

                table.fields.append(vofield)

        # Now, use those field definitions to create the numpy record arrays, with
        # the given number of rows
        table.create_arrays(self.get_count())

        for index, planet in enumerate(self.planets):
            table.array[index] = self._get_values_for_planet(planet)

    def _get_values_for_planet(self, planet: PlanetDBView) -> tuple[Any]:
        return tuple(
            chain.from_iterable(
                pack.get_values_for_planet(planet) for pack in self._vo_field_packs
            )
        )

    def get_xml(self) -> AstroVOTableFile:
        """Get the XML representation of VOTable"""
        # Now write the whole thing to a file.
        # Note, we have to use the top-level votable file object
        return self.votable
