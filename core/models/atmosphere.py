# Django 💩 imports
from django.db.models import (
    SET_NULL,
    CharField,
    FloatField,
    ForeignKey,
    SmallIntegerField,
)
from django_stubs_ext.db.models import TypedModelMeta

# Local imports
from ..fields import ErrorArrayField
from .abstractbase import TrackedModel, WebVisibleModel
from .planet import Planet
from .publication import Publication


class Molecule(WebVisibleModel, TrackedModel):
    """Model representing molecule."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    name: CharField = CharField(max_length=100)

    def __str__(self) -> str:
        return str(self.name)


class AtmosphereAbstract(TrackedModel):
    """Abstract model with all the fields common to all Atmosphere models."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        abstract = True

    # TODO set the corect on_delete value here (and eventually remove the null)
    publication: ForeignKey = ForeignKey(Publication, null=True, on_delete=SET_NULL)

    # TODO set the corect on_delete value here (and eventually remove the null)
    planet: ForeignKey = ForeignKey(Planet, null=True, on_delete=SET_NULL)

    # TODO replace this with a clean Enumeration Type
    # See: https://docs.djangoproject.com/en/3.0/ref/models/fields/#enumeration-types
    DATA_SOURCES = {1: "Measurements", 2: "Modeling"}
    DATA_SOURCE_MEASUREMENT = 1

    data_source: SmallIntegerField = SmallIntegerField(
        choices=[kv for kv in DATA_SOURCES.items()]
    )

    # BUG this field is useless for molecule data... and it is here only for backward
    # compatibility with the pre django 3 base --> remove ASAP
    result_value: FloatField = FloatField(null=True, blank=True)

    # BUG this field is useless for molecule data... and it is here only for backward
    # compatibility with the pre django 3 base --> remove ASAP
    result_value_error = ErrorArrayField(FloatField(), null=True, blank=True)

    # BUG this field is useless for molecule data... and it is here only for backward
    # compatibility with the pre django 3 base --> remove ASAP
    result_figure: CharField = CharField(null=True, blank=True, max_length=255)

    notes: CharField = CharField(null=True, blank=True, max_length=255)


class AtmosphereMolecule(AtmosphereAbstract):
    """Model representing a molecule present in the atmosphere of an exoplanet."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    # TODO replace this with a clean Enumeration Type
    # See: https://docs.djangoproject.com/en/3.0/ref/models/fields/#enumeration-types
    MOLECULE_DATA_TYPES = {
        1: "Detected",
        2: "Disk average",
        3: "Vertical profile",
        4: "Detected 1σ",
        5: "Detected 2σ",
        6: "Detected 3σ",
        7: "Detected 4σ",
        8: "Detected 5σ",
        9: "Detected 6σ",
    }

    # TODO get rid of this as soon as we have a MOLECULE_DATA_TYPES enum
    MOLECULE_DATA_TYPE_DETECTED = 1

    # TODO set the corect on_delete value here (and eventually remove the null)
    molecule: ForeignKey = ForeignKey(Molecule, null=True, on_delete=SET_NULL)

    type: SmallIntegerField = SmallIntegerField(
        null=True,
        blank=True,
        choices=[kv for kv in MOLECULE_DATA_TYPES.items()],
        help_text="If you choose detected, you must not enter any result",
    )


class AtmosphereTemperature(AtmosphereAbstract):
    """Model representing the temperature of the atmosphere of an exoplanet."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    # TODO replace this with a clean Enumeration Type
    # See: https://docs.djangoproject.com/en/3.0/ref/models/fields/#enumeration-types
    ATMOSPHERE_DATA_TYPES = {
        1: "Disk Average",
        2: "Day Average",
        3: "Night Average",
        4: "Spatial Variations",
        5: "Vertical Profile",
        6: "3D Map",
        7: "Hottest point longitude",
    }

    type: SmallIntegerField = SmallIntegerField(
        choices=[kv for kv in ATMOSPHERE_DATA_TYPES.items()]
    )
