"""Core models package."""

# We need to import each model here in order for them to be well detected during
# django makemigrations and migrate command

# Local imports
from .abstractbase import TrackedModel, WebVisibleModel  # noqa: F401
from .atmosphere import (  # noqa: F401
    AtmosphereMolecule,
    AtmosphereTemperature,
    Molecule,
)
from .choices import (  # noqa: F401
    AllDetection,
    MassUnit,
    Plan2PubParam,
    PlanetDetection,
    PlanetStatus,
    PublicationStatus,
    PublicationType,
    ResearchType,
    Star2PubParam,
    WebStatus,
    title_name,
)
from .coords import dec_string_to_degree_value, ra_string_to_degree_value  # noqa: F401
from .dbimport import Import  # noqa: F401
from .planet import (  # noqa: F401
    AlternatePlanetName,
    Planet,
    Planet2Publication,
    Planet2Star,
    name_already_used_as_planet_name,
    planet_name_already_used_as_alternate_name,
    safe_name,
)
from .planetarysystem import PlanetarySystem  # noqa: F401
from .planetdbview import PlanetDBView  # noqa: F401
from .publication import Publication  # noqa: F401
from .similarity import is_at_the_same_position_of_other_star  # noqa: F401
from .star import (  # noqa: F401
    AlternateStarName,
    Star,
    Star2Publication,
    name_already_used_as_star_name,
    star_name_already_used_as_alternate_name,
)
from .utils import (  # noqa: F401
    QUERY_NOT_HIDDEN_STATUS,
    filter_query_set_status_is_not_hidden,
    id_from_slug,
    obj_slugify,
)
