"""
Conversion tools from hh:mm:ss or dd:mm:ss string format to degrees for RA (right
ascension) and DEC (declination).

We will be using https://vizier.cds.unistra.fr/vizier/doc/catstd-3.3.htx as reference
for the definition, format and domains.

Examples:

    RA can be expressed in hours:minutes:seconds without anything else:

    >>> assert RA_REGEX.match("1:2")
    >>> assert RA_REGEX.match("12:34")
    >>> assert RA_REGEX.match("12:34:56")
    >>> assert RA_REGEX.match("12:34:56.789")
    >>> assert RA_REGEX.match("+12:34:56.789")

    And those two are equivalent:

    >>> assert RA_REGEX.match("12:34:.567")
    >>> assert RA_REGEX.match("12:34:0.567")

    RA can also be expressed in degrees without anything else:

    >>> assert RA_REGEX.match("1")
    >>> assert RA_REGEX.match("12")
    >>> assert RA_REGEX.match("123")
    >>> assert RA_REGEX.match("123.456")
    >>> assert RA_REGEX.match("123.456e-7")
    >>> assert RA_REGEX.match("+123.456e7")

    And those two are equivalent:

    >>> assert RA_REGEX.match("123.456e7")
    >>> assert RA_REGEX.match("123.456E7")

    And those two are equivalent:

    >>> assert RA_REGEX.match(".456e7")
    >>> assert RA_REGEX.match("0.456e7")

Some valid DEC examples:

Examples:

    DEC can be expressed in degrees:minutes:seconds without anything else:

    >>> assert DEC_REGEX.match("1:2")
    >>> assert DEC_REGEX.match("12:3")
    >>> assert DEC_REGEX.match("123:4")
    >>> assert DEC_REGEX.match("12:34")
    >>> assert DEC_REGEX.match("12:34:56")
    >>> assert DEC_REGEX.match("12:34:56.789")
    >>> assert DEC_REGEX.match("+12:34:56.789")
    >>> assert DEC_REGEX.match("-12:34:56.789")

    And those two are equivalent:

    >>> assert DEC_REGEX.match("12:34:.567")
    >>> assert DEC_REGEX.match("12:34:0.567")

    DEC can also be expressed in degrees without anything else:

    >>> assert DEC_REGEX.match("1")
    >>> assert DEC_REGEX.match("12")
    >>> assert DEC_REGEX.match("123")
    >>> assert DEC_REGEX.match("123.456")
    >>> assert DEC_REGEX.match("123.456e-7")
    >>> assert DEC_REGEX.match("+123.456e7")
    >>> assert DEC_REGEX.match("-123.456e7")

    And those two are equivalent:

    >>> assert DEC_REGEX.match("123.456e7")
    >>> assert DEC_REGEX.match("123.456E7")

    And those two are equivalent:

    >>> assert DEC_REGEX.match(".456e7")
    >>> assert DEC_REGEX.match("0.456e7")
"""

# Standard imports
from decimal import Decimal, InvalidOperation
from enum import Enum
from re import compile as re_compile
from re import match as re_match

# Django 💩 imports
from django.core.exceptions import ValidationError

# External imports
from dotmap import DotMap

# TODO: replace the \d{...} by the more precise by regex:
#
# For degrees in [-360;360]
# [+-]?(?:36[0]|3[0-5][0-9]|[12][0-9][0-9]|[1-9]?[0-9])?
#
# For minutes or seconds in [0;60]
# [+]?(?:60|[0-5][0-9]|[0-9])
#
# For hours in [0;24]
# [+]?(?:1?[0-9]|2[0-4])
#
# For angle in [-90;90]
# [+-]?(?:90|[0-8][0-9]|[0-9])
#
# Thoses regex are tested in the file: test/no_db/test_coords_regex.py

RA_REGEX = re_compile(
    r"^"  # start of string
    r"(?P<RAhms>"  # begining of hours:minutes:seconds group
    r"(?P<RAh>[+]?\d{1,2})"  # hours on 1 or 2 digits (can be preceded by + but not -)
    r":(?P<RAm>\d{1,2})"  # minutes on 1 or 2 digits
    r"(:(?P<RAs>"  # begining of optional seconds group expressed as...
    r"(\d{0,2}([.]\d*)?)"  # seconds as a float without exposant
    r"|"  # or
    r"(\d{1,2})"  # seconds as a 1 or 2 digits
    r"))?"  # end of optional seconds
    r")"  # end of hours:minutes:seconds group
    r"|"  # or
    # floating point degree angle (can be preceded by + but not -)
    r"(?P<RAdeg>[+]?(\d{1,3}([.]\d*)?([eE][+-]?\d+)?|[.]\d+([eE][+-]?\d+)?))"
    r"$"  # end of string
)
# See module doctest for examples

DEC_REGEX = re_compile(
    r"^"  # start of string
    r"(?P<DECdms>"  # begining of degrees:minutes:seconds group
    r"(?P<DECd>[+-]?\d{1,3})"  # signed degree angle on 1, 2 or 3 digits
    r":(?P<DECm>\d{1,2})"  # minutes on 1 or 2 digits
    r"(:(?P<DECs>"  # begining of optional seconds group expressed as...
    r"(\d{0,2}([.]\d*)?)"  # seconds as a float without exposant
    r"|"  # or
    r"(\d{1,2})"  # seconds as a 1 or 2 digits
    r"))?"  # end of optional seconds
    r")"  # end of degrees:minutes:seconds group
    r"|"  # or
    # signed floating point degree angle
    r"(?P<DECdeg>[+-]?(\d{1,3}([.]\d*)?([eE][+-]?\d+)?|[.]\d+([eE][+-]?\d+)?))"
    r"$"  # end of string
)
# See module doctest for examples

RA_MIN = Decimal("0")
RA_MAX = Decimal("360")
HOURS_MIN = Decimal("0")
HOURS_MAX = Decimal("24")
MINUTES_MIN = Decimal("0")
MINUTES_MAX = Decimal("60")
SECONDS_MIN = Decimal("0")
SECONDS_MAX = Decimal("60")
DEC_DEGREES_MIN = Decimal("-90")
DEC_DEGREES_MAX = Decimal("90")
DEGREES_MIN = Decimal("0")
DEGREES_MAX = Decimal("360")


class AstroCoord(Enum):
    """Astro coordinates enumerate for RA and DEC."""

    RA = DotMap(param="RA", other_format="hh:mm:ss")
    DEC = DotMap(param="DEC", other_format="dd:mm:ss")


def _hms_hours_to_degrees(hours_str: str) -> Decimal:
    """
    Convert a string representing an HMS hour value into degrees.

    Args:
        hours_str: A string representing the value.

    Returns:
        A Decimal value in degree.

    Raises:
        ValidationError: If hour_str is not a valid Decimal string or is out of its
        bounds

    Examples:
        Integer values in range [0,24[ work:

        >>> _hms_hours_to_degrees("0")
        Decimal('0')

        >>> _hms_hours_to_degrees("6")
        Decimal('90')

        >>> _hms_hours_to_degrees("12")
        Decimal('180')

        >>> _hms_hours_to_degrees("18")
        Decimal('270')

        Integer outside the [0,24[ does not work:

        >>> from django.core.exceptions import ValidationError
        >>> try:
        ...     _hms_hours_to_degrees("-6")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for RA.hours=-6: must be in range [0,24['

        >>> try:
        ...     _hms_hours_to_degrees("24")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for RA.hours=24: must be in range [0,24['

        >>> try:
        ...     _hms_hours_to_degrees("99")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for RA.hours=99: must be in range [0,24['

        Float does not work:

        >>> try:
        ...     _hms_hours_to_degrees("6.5")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for RA.hours=6.5: must be an integer'

        Any non number string does not work:

        >>> try:
        ...     _hms_hours_to_degrees("toto")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for RA.hours=toto: must be an integer'
    """
    try:
        hours = Decimal(hours_str)
    except InvalidOperation as exc:
        raise ValidationError(
            f"Invalid format for RA.hours={hours_str}: must be an integer"
        ) from exc

    # Checking domain
    if not HOURS_MIN <= hours < HOURS_MAX:
        raise ValidationError(
            f"Invalid format for RA.hours={hours_str}: "
            f"must be in range [{HOURS_MIN},{HOURS_MAX}["
        )

    # Cheking if we have an integer
    if hours % 1 != 0:
        raise ValidationError(
            f"Invalid format for RA.hours={hours_str}: must be an integer"
        )

    degrees = hours * DEGREES_MAX / HOURS_MAX

    return degrees


def _hms_minutes_to_degrees(minutes_str: str) -> Decimal:
    """
    Convert a string representing an HMS minutes value into an angle in degrees.

    Args:
        minutes_str: A string representing the value.

    Returns:
        A Decimal value in degree.

    Raises:
        ValidationError: If minutes_str is not a valid Decimal string or is out of its
        bounds

    Examples:
        Integer values in range [0,60[ work:

        >>> _hms_minutes_to_degrees("0")
        Decimal('0')

        >>> _hms_minutes_to_degrees("1")
        Decimal('0.25')

        >>> _hms_minutes_to_degrees("2")
        Decimal('0.5')

        >>> _hms_minutes_to_degrees("3")
        Decimal('0.75')

        >>> _hms_minutes_to_degrees("4")
        Decimal('1')

        Integer outside the [0,60[ does not work:

        >>> try:
        ...     _hms_minutes_to_degrees("-1")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS minutes=-1: must be in range [0,60['

        >>> try:
        ...     _hms_minutes_to_degrees("60")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS minutes=60: must be in range [0,60['

        >>> try:
        ...     _hms_minutes_to_degrees("99")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS minutes=99: must be in range [0,60['

        Float does not work:

        >>> try:
        ...     _hms_minutes_to_degrees("1.5")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS minutes=1.5: must be an integer'

        Any non number string does not work:

        >>> try:
        ...     _hms_minutes_to_degrees("toto")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS minutes=toto: must be an integer'
    """
    try:
        minutes = Decimal(minutes_str)
    except InvalidOperation as exc:
        raise ValidationError(
            f"Invalid format for HMS minutes={minutes_str}: must be an integer"
        ) from exc

    # Checking domain
    if not MINUTES_MIN <= minutes < MINUTES_MAX:
        raise ValidationError(
            f"Invalid format for HMS minutes={minutes_str}: "
            f"must be in range [{MINUTES_MIN},{MINUTES_MAX}["
        )

    # Cheking if we have an integer
    if minutes % 1 != 0:
        raise ValidationError(
            f"Invalid format for HMS minutes={minutes_str}: must be an integer"
        )

    degrees = minutes * DEGREES_MAX / HOURS_MAX / MINUTES_MAX

    return degrees


def _hms_seconds_to_degrees(seconds_str: str) -> Decimal:
    """
    Convert a string representing an HMS seconds value into an angle in degrees.

    Args:
        seconds_str: A string representing the value.

    Returns:
        A Decimal value in degree.

    Raises:
        ValidationError: if seconds_str is not a valid Decimal string or out of its
        bounds

    Examples:
        Integer values in range [0,60[ work:

        >>> _hms_seconds_to_degrees("0")
        Decimal('0')

        >>> _hms_seconds_to_degrees("15")
        Decimal('0.0625')

        >>> _hms_seconds_to_degrees("30")
        Decimal('0.125')

        >>> _hms_seconds_to_degrees("45")
        Decimal('0.1875')

        Float values works too :

        >>> _hms_seconds_to_degrees("1.8")
        Decimal('0.0075')

        Integer outside the [0,60[ does not work:

        >>> try:
        ...     _hms_seconds_to_degrees("-1")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS seconds=-1: must be in range [0,60['

        >>> try:
        ...     _hms_seconds_to_degrees("60")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS seconds=60: must be in range [0,60['

        >>> try:
        ...     _hms_seconds_to_degrees("99")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS seconds=99: must be in range [0,60['

        Any non number string does not work:

        >>> try:
        ...     _hms_seconds_to_degrees("toto")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for HMS seconds=toto: must be an integer or float'
    """
    try:
        seconds = Decimal(seconds_str)
    except InvalidOperation as exc:
        raise ValidationError(
            f"Invalid format for HMS seconds={seconds_str}: must be an integer or float"
        ) from exc

    # Checking domain
    if not SECONDS_MIN <= seconds < SECONDS_MAX:
        raise ValidationError(
            f"Invalid format for HMS seconds={seconds_str}: "
            f"must be in range [{SECONDS_MIN},{SECONDS_MAX}["
        )

    degrees = seconds * DEGREES_MAX / HOURS_MAX / MINUTES_MAX / SECONDS_MAX

    return degrees


def _dms_degrees_to_degrees(degrees_str: str) -> Decimal:
    """
    Convert a string representing an DMS degrees value into an angle in degrees.

    Args:
        degrees_str: A string representing the value.

    Returns:
        A Decimal value in degree.

    Raises:
        ValidationError: if degrees_str is not a valid Decimal string or is out of its
        bounds

    Examples:
        Integer values in range [-90,90] work:

        >>> _dms_degrees_to_degrees("-90")
        Decimal('-90')

        >>> _dms_degrees_to_degrees("-60")
        Decimal('-60')

        >>> _dms_degrees_to_degrees("-30")
        Decimal('-30')

        >>> _dms_degrees_to_degrees("0")
        Decimal('0')

        >>> _dms_degrees_to_degrees("30")
        Decimal('30')

        >>> _dms_degrees_to_degrees("60")
        Decimal('60')

        >>> _dms_degrees_to_degrees("90")
        Decimal('90')

        Integer outside the [-90,90] does not work:

        >>> try:
        ...     _dms_degrees_to_degrees("-100")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DEC.degrees=-100: must be in range [-90,90]'

        >>> try:
        ...     _dms_degrees_to_degrees("100")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DEC.degrees=100: must be in range [-90,90]'

        Float does not work:

        >>> try:
        ...     _dms_degrees_to_degrees("6.5")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DEC.degrees=6.5: must be an integer'

        Any non number string does not work:

        >>> try:
        ...     _dms_degrees_to_degrees("toto")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DEC.degrees=toto: must be an integer'
    """
    try:
        degrees = Decimal(degrees_str)
    except InvalidOperation as exc:
        raise ValidationError(
            f"Invalid format for DEC.degrees={degrees_str}: must be an integer"
        ) from exc

    # Checking domain
    if not DEC_DEGREES_MIN <= degrees <= DEC_DEGREES_MAX:
        raise ValidationError(
            f"Invalid format for DEC.degrees={degrees_str}: "
            f"must be in range [{DEC_DEGREES_MIN},{DEC_DEGREES_MAX}]"
        )

    # Cheking if we have an integer
    if degrees % 1 != 0:
        raise ValidationError(
            f"Invalid format for DEC.degrees={degrees_str}: must be an integer"
        )

    return degrees


def _dms_minutes_to_degrees(minutes_str: str) -> Decimal:
    """
    Convert a string representing an DMS minutes value into an angle in degrees.

    Args:
        minutes_str: A string representing the value.

    Returns:
        A Decimal value in degree.

    Raises:
        ValidationError: if minutes_str is not a valid Decimal string, is out of its
        bounds or convert to a value greater than one degree.

    Examples:
        Integer values in range [0,60[ work:

        >>> _dms_minutes_to_degrees("0")
        Decimal('0')

        >>> _dms_minutes_to_degrees("3")
        Decimal('0.05')

        >>> _dms_minutes_to_degrees("6")
        Decimal('0.1')

        >>> _dms_minutes_to_degrees("9")
        Decimal('0.15')

        >>> _dms_minutes_to_degrees("12")
        Decimal('0.2')

        >>> _dms_minutes_to_degrees("30")
        Decimal('0.5')

        >>> _dms_minutes_to_degrees("45")
        Decimal('0.75')

        Integer outside the [0,60[ does not work:

        >>> try:
        ...     _dms_minutes_to_degrees("-1")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS minutes=-1: must be in range [0,60['

        >>> try:
        ...     _dms_minutes_to_degrees("60")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS minutes=60: must be in range [0,60['

        >>> try:
        ...     _dms_minutes_to_degrees("99")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS minutes=99: must be in range [0,60['

        Float does not work:

        >>> try:
        ...     _dms_minutes_to_degrees("1.5")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS minutes=1.5: must be an integer'

        Any non number string does not work:

        >>> try:
        ...     _dms_minutes_to_degrees("toto")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS minutes=toto: must be an integer'
    """
    try:
        minutes = Decimal(minutes_str)
    except InvalidOperation as exc:
        raise ValidationError(
            f"Invalid format for DMS minutes={minutes_str}: must be an integer"
        ) from exc

    # Checking domain
    if not MINUTES_MIN <= minutes < MINUTES_MAX:
        raise ValidationError(
            f"Invalid format for DMS minutes={minutes_str}: "
            f"must be in range [{MINUTES_MIN},{MINUTES_MAX}["
        )

    # Cheking if we have an integer
    if minutes % 1 != 0:
        raise ValidationError(
            f"Invalid format for DMS minutes={minutes_str}: must be an integer"
        )

    degrees = minutes / MINUTES_MAX

    # Ensure the conversion returns a value lower than a degree
    if degrees >= Decimal("1"):
        raise ValidationError(
            f"Conversion error for DMS minutes={minutes_str}: "
            f"it returns a value greater than a degree which is not possible for a "
            f"minute value in range [{MINUTES_MIN},{MINUTES_MAX}["
        )

    return degrees


def _dms_seconds_to_degrees(seconds_str: str) -> Decimal:
    """
    Convert a string representing an DMS seconds value into an angle in degrees.

    Args:
        seconds_str: A string representing the value.

    Returns:
        A Decimal value in degree.

    Raises:
        ValidationError: if seconds_str is not a valid Decimal string, is out of its
        bounds or convert to a value greater than one minutes.

    Examples:
        Integer values in range [0,60[ work:

        >>> _dms_seconds_to_degrees("0")
        Decimal('0')

        >>> _dms_seconds_to_degrees("9")
        Decimal('0.0025')

        >>> _dms_seconds_to_degrees("18")
        Decimal('0.005')

        >>> _dms_seconds_to_degrees("27")
        Decimal('0.0075')

        >>> _dms_seconds_to_degrees("36")
        Decimal('0.01')

        >>> _dms_seconds_to_degrees("45")
        Decimal('0.0125')

        >>> _dms_seconds_to_degrees("54")
        Decimal('0.015')

        Float values works too :

        >>> _dms_seconds_to_degrees("1.8")
        Decimal('0.0005')

        Integer outside the [0,60[ does not work:

        >>> try:
        ...     _dms_seconds_to_degrees("-1")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS seconds=-1: must be in range [0,60['

        >>> try:
        ...     _dms_seconds_to_degrees("60")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS seconds=60: must be in range [0,60['

        >>> try:
        ...     _dms_seconds_to_degrees("99")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS seconds=99: must be in range [0,60['

        Any non number string does not work:

        >>> try:
        ...     _dms_seconds_to_degrees("toto")
        ... except ValidationError as err:
        ...     print(err.message)
        'Invalid format for DMS seconds=toto: must be an integer or float'
    """
    try:
        seconds = Decimal(seconds_str)
    except InvalidOperation as exc:
        raise ValidationError(
            f"Invalid format for DMS seconds={seconds_str}: must be an integer or float"
        ) from exc

    # Checking domain
    if not SECONDS_MIN <= seconds < SECONDS_MAX:
        raise ValidationError(
            f"Invalid format for DMS seconds={seconds_str}: "
            f"must be in range [{SECONDS_MIN},{SECONDS_MAX}["
        )

    degrees = seconds / MINUTES_MAX / SECONDS_MAX

    # Ensure the conversion returns a value lower than a minute
    if degrees >= Decimal("1") / MINUTES_MAX:
        raise ValidationError(
            f"Conversion error for DMS seconds={seconds_str}: "
            f"it returns a value greater than a minutes which is not possible for a "
            f"seconds value in range [{SECONDS_MIN},{SECONDS_MAX}["
        )

    return degrees


def ra_string_to_degree_value(ra_str: str) -> Decimal:
    """
    Convert a string representing a RA angle to a Decimal value of that angle.

    The can be in different format :

    -   hours:minutes:seconds where
        -   hours is an integer in [0,24[
        -   minutes is an integer in [0,60[
        -   seconds is a float in [0,60[
    -   degrees as a float in [0,360[

    Args:
        ra_str: String representing the ra value.

    Returns:
        An angle in degree in [0,360[.

    Raises:
        ValidationError with a relevant message:
        -   if the format is not correct
        -   if the values of hours, minutes, secondes or degrees are out of their
            respective domains
    """
    bad_format_msg = (
        f"Invalid format for RA={ra_str}: hh:mm:ss or hh:mm or value in degrees"
    )

    if not (match := re_match(RA_REGEX, ra_str)):
        raise ValidationError(bad_format_msg)

    hours_str = match.group("RAh")
    minutes_str = match.group("RAm")
    seconds_str = match.group("RAs")
    degree_str = match.group("RAdeg")

    match (hours_str, minutes_str, seconds_str, degree_str):
        case (None, None, None, str()):
            result_angle = Decimal(degree_str)
        case (str(), str(), str(), None):
            result_angle = (
                _hms_hours_to_degrees(hours_str)
                + _hms_minutes_to_degrees(minutes_str)
                + _hms_seconds_to_degrees(seconds_str)
            )
        case (str(), str(), None, None):
            result_angle = _hms_hours_to_degrees(hours_str) + _hms_minutes_to_degrees(
                minutes_str
            )
        # we can't have only hours
        # we can't have only minutes
        # we can't have only seconds
        # we can't have everything
        # But that is covered by the regex
        # We still keep this ultimate guard for "all other cases"
        case _:
            raise ValidationError(bad_format_msg)

    if not RA_MIN <= result_angle < RA_MAX:
        raise ValidationError(
            f"Invalid format for RA={result_angle}: must be in [{RA_MIN},{RA_MAX}["
        )

    return result_angle


def dec_string_to_degree_value(dec_str: str) -> Decimal:
    """
    Convert a string representing a DEC angle to a Decimal value of that angle.

    The can be in different format :

    -   degrees:minutes:seconds where
        -   degrees is an integer in [-90,90]
        -   minutes is an integer in [0,60[
        -   seconds is a float in [0,60[
    -   degrees as a float in [-90,90]

    Args:
        dec_str: string representing the dec value

    Returns:
        An angle in degree in [-90,90[.

    Raises:
        ValidationError with a relevant message:
        -   if the format is not correct
        -   if the values of degrees, minutes, secondes or degrees are out of their
            respective domains
    """
    if not (match := re_match(DEC_REGEX, dec_str)):
        raise ValidationError(
            "Invalid format for DEC={dec_str}: dd:mm:ss or value in degrees"
        )

    degrees_str = match.group("DECd")
    minutes_str = match.group("DECm")
    seconds_str = match.group("DECs")
    full_degree_str = match.group("DECdeg")

    match (degrees_str, minutes_str, seconds_str, full_degree_str):
        # We only have full degrees
        case (None, None, None, str()):
            result_angle = Decimal(full_degree_str)
        # We have degrees and minutes and seconds but no full degrees
        case (str(), str(), str(), None):
            # We build the integer and fractionnal part of the result angle
            int_part_degrees = _dms_degrees_to_degrees(degrees_str)

            fraction_part_degrees = Decimal(
                sum(
                    [
                        _dms_minutes_to_degrees(minutes_str),
                        _dms_seconds_to_degrees(seconds_str),
                    ]
                )
            )

            # We build the final result taking in account the sign of the integer part
            result_angle = Decimal(
                sum(
                    [
                        int_part_degrees,
                        fraction_part_degrees.copy_sign(int_part_degrees),
                    ]
                )
            )

            # We check if the fractionnal won't change the integer part
            if fraction_part_degrees >= 1:
                raise ValidationError(
                    f"Invalid format for DEC input={dec_str}: "
                    f"we get a value {result_angle} "
                    f"where integer part {int_part_degrees} "
                    f"is not equal to the first part {int_part_degrees}."
                )

        case (str(), str(), None, None):
            # We build the integer and fractionnal part of the result angle
            int_part_degrees = _dms_degrees_to_degrees(degrees_str)

            fraction_part_degrees = _dms_minutes_to_degrees(minutes_str)

            # We build the final result taking in account the sign of the integer part
            result_angle = Decimal(
                sum(
                    [
                        int_part_degrees,
                        fraction_part_degrees.copy_sign(int_part_degrees),
                    ]
                )
            )

            # We check if the fractionnal won't change the integer part
            if fraction_part_degrees >= 1:
                raise ValidationError(
                    f"Invalid format for DEC input={dec_str}: "
                    f"we get a value {result_angle} "
                    f"where integer part {int_part_degrees} "
                    f"is not equal to the first part {int_part_degrees}."
                )

        # we can't have only minutes
        # we can't have only seconds
        # we can't have everything
        # But that is covered by the regex
        # We still keep this ultimate guard for "all other cases"
        case _:
            raise ValidationError(
                "Invalid format for DEC={dec_str}: dd:mm:ss or value in degrees"
            )

    if not DEC_DEGREES_MIN <= result_angle <= DEC_DEGREES_MAX:
        raise ValidationError(
            f"Invalid format for DEC={result_angle}: "
            f"must be in [{DEC_DEGREES_MIN},{DEC_DEGREES_MAX}]"
        )

    return result_angle
