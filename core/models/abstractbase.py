# Django 💩 imports
from django.db.models import DateTimeField, FloatField, Model, SmallIntegerField
from django_stubs_ext.db.models import TypedModelMeta

# Local imports
from ..fields import CoordinateField, ErrorArrayField
from ..helpers import dec_float_to_dms_string, ra_float_to_hms_string
from .choices import WebStatus


# TODO test this
class WebVisibleModel(Model):
    """
    Base model to inherit for all models that can be visible or hidden for the users.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        abstract = True

    # TODO replace this with a clean enum
    status: SmallIntegerField = SmallIntegerField(
        choices=WebStatus.choices,
        default=WebStatus.ACTIVE,
        help_text="Whether the record is visible on the website or not",
    )

    def is_visible(self) -> bool:
        """
        Tell Django if the object should be visible on the web site according to its
        status.
        """
        return bool(self.status == WebStatus.ACTIVE)

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    is_visible.boolean = True  # type: ignore[attr-defined]
    is_visible.admin_order_field = "status"  # type: ignore[attr-defined]
    is_visible.short_description = "Status"  # type: ignore[attr-defined]

    # TODO why do we need this? #ASKCYRIL
    def _status(self) -> str:
        return WebStatus(self.status).label

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    _status.admin_order_field = "status"  # type: ignore[attr-defined]
    _status.short_description = "Status"  # type: ignore[attr-defined]


# TODO test this
class TrackedModel(Model):
    """
    Base model to inherit for all models that need to maintain track of when their
    records have been created and updated.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        abstract = True

    created: DateTimeField = DateTimeField(auto_now_add=True)
    modified: DateTimeField = DateTimeField(auto_now=True)


# TODO test this
# Formerly this class was called "System" which was very misleading
class WithCoordinateModel(TrackedModel):
    """
    Base model to inherit for all model that have a position in the sky.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        abstract = True

    # TODO: this should be stored as a Decimal not double precision
    ra: CoordinateField = CoordinateField(
        verbose_name="RA (J2000)",
        help_text="in decimal degrees or in hh:mm:ss",
        max_length=60,
    )
    # TODO: this should be stored as a Decimal not double precision
    dec: CoordinateField = CoordinateField(
        verbose_name="Dec (J2000)",
        help_text="in decimal degrees or in dd:mm:ss",
        max_length=60,
    )
    distance: FloatField = FloatField(null=True, blank=True)
    distance_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    @property
    def ra_sex(self) -> str:
        return ra_float_to_hms_string(self.ra)

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    ra_sex.fget.short_description = "RA"  # type: ignore[attr-defined]
    ra_sex.fget.admin_order_field = "ra"  # type: ignore[attr-defined]

    @property
    def dec_sex(self) -> str:
        return dec_float_to_dms_string(self.dec)

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    dec_sex.fget.short_description = "Dec"  # type: ignore[attr-defined]
    dec_sex.fget.admin_order_field = "dec"  # type: ignore[attr-defined]
