# Standard imports
import math
from decimal import Decimal

# Django 💩 imports
from django.core.exceptions import ValidationError
from django.db.models import (
    CASCADE,
    PROTECT,
    BooleanField,
    CharField,
    FloatField,
    ForeignKey,
    ManyToManyField,
    Model,
    SmallIntegerField,
    TextField,
)
from django_stubs_ext.db.models import TypedModelMeta

# External imports
from astropy.coordinates import SkyCoord

# Local imports
from ..fields import AsciiCharField, ErrorArrayField
from ..postgresql import NullsLastQueryManager
from .abstractbase import TrackedModel, WebVisibleModel, WithCoordinateModel
from .choices import DetectedDiscDetection, Star2PubParam
from .coords import dec_string_to_degree_value, ra_string_to_degree_value
from .dbimport import Import
from .publication import Publication
from .similarity import is_at_the_same_position_of_other_star
from .utils import obj_slugify

# Maximum possible precision for float stored as double precision (that's the case for
# RA and DEC for the moment)
# See: https://en.wikipedia.org/wiki/Double-precision_floating-point_format
# See: abstractbase.py
DOUBLE_FLOAT_SIGNIFICANT_DIGIT = 17


def star_name_already_used_as_alternate_name(name_to_check: str) -> tuple[bool, str]:
    """
    Check if a given name is already in database as an alternate name of star.

    Args:
        name_to_check: Name to check.

    Returns:
        True and the correct error message if the name is already used.
        False and an empty error message otherwise.
    """
    # pylint: disable-next=no-member
    check_same_name = AlternateStarName.objects.filter(
        name__iexact=name_to_check.lower()
    )

    # AlternateStarName has an automatic attribute `star_id` with the ForeignKey Field
    if check_same_name:
        star_from_id = Star.objects.filter(
            pk=check_same_name[0].star_id,  # type: ignore[attr-defined]
        )

        return True, star_from_id[0].name

    return False, ""


def name_already_used_as_star_name(name_to_check: str) -> tuple[bool, str]:
    """
    Check if a given name is already in database as "main" name of a star.

    Args:
        name_to_check: Name to check.

    Returns:
        True and the correct error message if the name is already used.
        False and an empty error message otherwise.
    """
    if check_same_name := Star.objects.filter(name__iexact=name_to_check.lower()):
        return True, check_same_name[0].name

    return False, ""


class Star(WithCoordinateModel, WebVisibleModel):
    """
    Model representing host star of an exoplanet
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        ordering = ["name"]
        permissions = [
            ("suggest_stars", "Can suggest stars"),
        ]

    # This enable good null handling in postgresql
    objects = NullsLastQueryManager()

    name: AsciiCharField = AsciiCharField(max_length=60, unique=True)
    spec_type: CharField = CharField(
        max_length=15, null=True, blank=True, verbose_name="Spectral type"
    )
    magnitude_v: FloatField = FloatField(
        null=True, blank=True, verbose_name="V magnitude"
    )
    magnitude_i: FloatField = FloatField(
        null=True, blank=True, verbose_name="I magnitude"
    )
    magnitude_h: FloatField = FloatField(
        null=True, blank=True, verbose_name="H magnitude"
    )
    magnitude_j: FloatField = FloatField(
        null=True, blank=True, verbose_name="J magnitude"
    )
    magnitude_k: FloatField = FloatField(
        null=True, blank=True, verbose_name="K magnitude"
    )
    mass: FloatField = FloatField(null=True, blank=True)
    mass_error = ErrorArrayField(FloatField(), null=True, blank=True)
    age: FloatField = FloatField(null=True, blank=True, verbose_name="Age in Gyr")
    age_error = ErrorArrayField(FloatField(), null=True, blank=True)
    teff: FloatField = FloatField(null=True, blank=True)
    teff_error = ErrorArrayField(FloatField(), null=True, blank=True)
    radius: FloatField = FloatField(null=True, blank=True)
    radius_error = ErrorArrayField(FloatField(), null=True, blank=True)
    metallicity: FloatField = FloatField(null=True, blank=True)
    metallicity_error = ErrorArrayField(FloatField(), null=True, blank=True)
    detected_disc: SmallIntegerField = SmallIntegerField(
        null=True, blank=True, choices=DetectedDiscDetection.choices
    )
    magnetic_field: BooleanField = BooleanField(
        null=True, blank=True, choices=((True, "Yes"), (False, "No"))
    )
    remarks: TextField = TextField(null=True, blank=True)
    # WARNING contains raw HTML
    other_web: TextField = TextField(null=True, blank=True)
    # WARNING contains raw urls
    url_simbad: CharField = CharField(null=True, blank=True, max_length=255)
    radvel_proj: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Projected radial velocity",
        help_text="In km/s",
    )

    publications: ManyToManyField = ManyToManyField(
        Publication, through="Star2Publication", related_name="stars"
    )

    dbimport: ForeignKey = ForeignKey(Import, null=True, blank=True, on_delete=PROTECT)

    @property
    def l(self) -> str:  # noqa: E743 E741
        galac_l = (
            SkyCoord(ra=self.ra, dec=self.dec, frame="icrs", unit="deg").galactic().l
        )

        return f"{galac_l:.3f}"

    @property
    def b(self) -> str:
        galac_b = (
            SkyCoord(ra=self.ra, dec=self.dec, frame="icrs", unit="deg").galactic().b
        )

        return f"{galac_b:.3f}"

    @property
    def gal_x(self) -> str | None:
        if not self.distance:
            return None

        galac_x = (
            self.distance
            * math.cos(float(self.b) * math.pi / 180.0)
            * math.cos(float(self.l) * math.pi / 180.0)
        )

        return f"{galac_x:d}"

    @property
    def gal_y(self) -> str | None:
        if not self.distance:
            return None

        galac_y = (
            self.distance
            * math.cos(float(self.b) * math.pi / 180.0)
            * math.sin(float(self.l) * math.pi / 180.0)
        )

        return f"{galac_y:d}"

    @property
    def gal_z(self) -> str | None:
        if not self.distance:
            return None

        galac_z = self.distance * math.sin(float(self.b) * math.pi / 180.0)

        return f"{galac_z:d}"

    @property
    def url_keyword(self) -> str:
        """Generate URL keyword from name, i.e. Berkeley 19 --> berkeley_19"""
        return obj_slugify(
            obj_name=str(self.name),
            # Star has an automatic attribute `id` from the django model (Django == 💩)
            # pylint: disable-next=no-member
            obj_id=self.id,  # type: ignore [attr-defined]
        )

    @property
    def ads_search_parameter(self) -> str:
        return f'"{self.name.replace(" ", "+").lower()}"'  # pylint: disable=no-member

    @property
    def alternate_names(self) -> str:
        # Star has an automatic attribute `alternate_star_naems` with the
        # foreign_key field (Django == 💩)
        # pylint: disable-next=no-member
        return ", ".join(
            [
                m.name
                for m in self.alternate_star_names.all()  # type: ignore [attr-defined]
            ]
        )

    @property
    def detected_disc_human(self) -> str | None:
        if self.detected_disc is not None:
            return DetectedDiscDetection(self.detected_disc).label
        else:
            return None

    @property
    def magnetic_field_human(self) -> str | None:
        if self.magnetic_field is None:
            return None
        elif self.magnetic_field is True:
            return "Yes"
        else:
            return "No"

    def __str__(self) -> str:
        return str(self.name)

    def clean(self) -> None:
        """
        Function that will be automatically called before a model instance is saved.

        Warning: if calling `save()` manually on the model you have to manually call
        `full_clean()` before to have this method actuallly called.

        See:
        https://docs.djangoproject.com/en/3.0/ref/models/instances/#validating-objects
        """
        name_alreay_used, used_by = star_name_already_used_as_alternate_name(self.name)
        if name_alreay_used:
            raise ValidationError(
                f"Name: {self.name} already used as an alternate name by {used_by}."
            )

        self.ra = ra_string_to_degree_value(
            f"{self.ra:+.{DOUBLE_FLOAT_SIGNIFICANT_DIGIT}g}"
        )
        self.dec = dec_string_to_degree_value(
            f"{self.dec:+.{DOUBLE_FLOAT_SIGNIFICANT_DIGIT}g}"
        )

        is_at_same_pos, raise_msg = is_at_the_same_position_of_other_star(
            Decimal(self.ra),
            Decimal(self.dec),
            # Star has an automatic attribute `id` from the django model (Django == 💩)
            # pylint: disable-next=no-member
            star_id=self.id,  # type: ignore [attr-defined]
        )

        if is_at_same_pos:
            raise ValidationError(raise_msg)

        super().clean()


class Star2Publication(WebVisibleModel, TrackedModel):
    """Many-to-many relationship model used for the Star.publications relationship."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    # Foreign keys as declared in Star.publications
    publication: ForeignKey = ForeignKey(Publication, on_delete=CASCADE)
    star: ForeignKey = ForeignKey(Star, on_delete=CASCADE)

    # TODO remove these they must be from the very old mySQL model #ASKCYRIL
    # Tells if a publication is about a specific planet parameter
    parameter: SmallIntegerField = SmallIntegerField(
        null=False,
        blank=False,
        choices=Star2PubParam.choices,
        default=Star2PubParam.GENERIC_PUB,
    )


class AlternateStarName(Model):
    """Simple placeholder for the planet names since planets can have multiple names."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    name: AsciiCharField = AsciiCharField(max_length=60, unique=True)

    # WARNING previously the related name was singular: alternate_star_name
    star: ForeignKey = ForeignKey(
        Star,
        related_name="alternate_star_names",
        null=False,
        on_delete=CASCADE,
    )

    def clean(self) -> None:
        """
        Clean AlternateStarName model.

        - Auto delete if the foreign key is None.
        - Check if self.name is already used as a "main" star name.
        """
        if self.star is None:
            self.delete()

        name_alreay_used, used_by = name_already_used_as_star_name(self.name)
        if name_alreay_used:
            raise ValidationError(
                f"Name: {self.name} already used as a star name " f"by {used_by}."
            )

        super().clean()
