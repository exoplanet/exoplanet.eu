"""
All choices class needed by the core models.
"""

# Standard imports
from enum import Enum
from typing import Type

# Django 💩 imports
from django.db import models

# TODO use the exochoices lib instead


def _title_str(given_str: str) -> str:
    """
    Equivalent of str.title() that convert _ to space but that respect
    uppercase acronyms.

    Args:
        s: a _ separated string of words

    Returns:
        a space separated string with every first letter uppercase.

    Examples:
        >>> _title_str('titi_tata_tutu')
        'Titi Tata Tutu'
        >>> _title_str('titi_tata_TOTO_tutu')
        'Titi Tata TOTO Tutu'
        >>> _title_str('titi__tata___tutu_')
        'Titi Tata Tutu'
    """
    # We split and eventually drop empty words
    words = [word for word in given_str.split("_") if word]

    # Join the first letter uppercased words with spaces
    return " ".join([word[0].upper() + word[1:] for word in words])


def title_name(enum: models.IntegerChoices) -> str:
    """
    Equivalent of str.title() that convert _ to space but that respect
    uppercase acronyms.

    This allow us to convert enum labels to their old version (when they
    were just dirty dicts)

    Args:
        e: a Django Choices instance

    Returns:
        the enum name as a space separated string with every first letter uppercase.

    Examples:
        >>> from django.db import models
        >>> class Blah(models.IntegerChoices):
        ...     TITI_TATA_TUTU = 1, "titi_tata_tutu"
        ...     TITI_TATA_TOTO_TUTU = 2, "titi_tata_TOTO_tutu"
        ...     TITI__TATA___TUTU_ = 3, "titi__tata___tutu_"
        ...
        >>> title_name(Blah.TITI_TATA_TUTU)
        'Titi Tata Tutu'
        >>> title_name(Blah.TITI_TATA_TOTO_TUTU)
        'Titi Tata TOTO Tutu'
        >>> title_name(Blah.TITI__TATA___TUTU_)
        'Titi Tata Tutu'
    """
    return _title_str(enum.label)


def value_from_approx_name(cls: Type[models.IntegerChoices], approx_str: str) -> int:
    """
    Gets the value of the enum corresponding to the approximative string
    passed as param.

    Args:
        cls: an enum class in which to search the names.
        approx_str: an string that roughly correspond to one of the name of the enums.
                    It can have different case and can use space instead of "_".

    Raises:
        KeyError: if no enum name correspond to the given string.

    Examples:
        >>> value_from_approx_name(PlanetDetection, 'Timing')
        2
        >>> value_from_approx_name(PlanetDetection, 'Radial Velocity')
        1
        >>> value_from_approx_name(PlanetDetection, 'Radial_Velocity')
        1
        >>> value_from_approx_name(PlanetDetection, 'radial velocity')
        1
        >>> value_from_approx_name(PlanetDetection, 'TTV')
        8
        >>> value_from_approx_name(PlanetDetection, 'ttv')
        8
    """
    lower_str = approx_str.replace(" ", "_").lower()

    for detec in cls:
        if lower_str == detec.label.replace(" ", "_").lower():
            return detec.value

    raise KeyError


class CustomIntegerChoices(models.IntegerChoices):
    """Custom Integer Choices class."""

    @classmethod
    def all_values_number(cls) -> list[int]:
        """
        Get all numeric value for this class.

        Returns:
            All numeric value of this class.

        Examples:
            >>> class Toto(CustomIntegerChoices):
            ...     A = 1, "a"
            ...     B = 2, "a"
            ...     C = 3, "a"
            >>> Toto.all_values_number()
            [1, 2, 3]
        """
        return [choice.value for choice in cls]

    @classmethod
    def get_numeric_avaible_value(cls) -> str:
        """
        Get all numeric avaible values for this class in a single string.

        Returns:
            All numeric avaible values.

        Examples:
            >>> class Toto(CustomIntegerChoices):
            ...     A = 1, "a"
            ...     B = 2, "a"
            ...     C = 3, "a"
            >>> Toto.get_numeric_avaible_value()
            '1, 2, 3'
        """
        return ", ".join([str(elem) for elem in cls.all_values_number()])


class PublicationType(CustomIntegerChoices):
    """All possible publication support."""

    BOOK = (
        1,
        "book",
    )
    THESIS = (
        2,
        "thesis",
    )
    REFEREED_ARTICLE = (
        3,
        "refereed article",
    )
    PROCEEDING = (
        4,
        "proceeding",
    )
    UNKNOWN = (
        5,
        "unknown",
    )
    REPORT = (
        6,
        "report",
    )
    ARXIV = (
        7,
        "arXiv",
    )


class ResearchType(CustomIntegerChoices):
    """All possible research type."""

    GROUND = (
        1,
        "ground",
    )
    SPACE = (
        2,
        "space",
    )


# TODO add new statuses : STATISTIC and maybe IA ???
class PlanetStatus(CustomIntegerChoices):
    """All possible planet statuses."""

    CONFIRMED = 1, "Confirmed"
    CANDIDATE = 2, "candidate"  # BUG change this to have a capital letter
    CONTROVERSIAL = 3, "Controversial"
    UNCONFIRMED = 4, "Unconfirmed"
    RETRACTED = 5, "Retracted"


class WebStatus(CustomIntegerChoices):
    """Possible statuses for the visibility of a planet on the website."""

    ACTIVE = 1, "Active"
    HIDDEN = 3, "Hidden"
    IMPORTED = 4, "Imported"
    SUGGESTED = 5, "Suggested"


class AllDetection(CustomIntegerChoices, Enum):
    """
    All possible detection method.

    This class must never be used directly. It is only to ensure coerence beween the
    different specialised *Detection classes that reuse its values.
    """

    RADIAL_VELOCITY = 1, "Radial Velocity"
    TIMING = 2, "Timing"
    CONTROVERSIAL = 3, "Controversial"
    MICROLENSING = 4, "Microlensing"
    IMAGING = 5, "Imaging"
    PRIMARY_TRANSIT = 6, "Primary Transit"
    ASTROMETRY = 7, "Astrometry"
    TTV = 8, "TTV"  # BUG for consistency reason we should put this in lowercase
    OTHER = 9, "Other"
    SPECTRUM = 10, "Spectrum"
    THEORETICAL = 11, "Theoretical"
    FLUX = 12, "Flux"
    SECONDARY_TRANSIT = 13, "Secondary Transit"
    IR_EXCESS = (
        14,
        "IR Excess",
    )  # BUG for consistency reason we should put this in lowercase
    KINEMATIC = (15, "Kinematic")

    @property
    def val_label(self) -> tuple[int, str]:
        """
        Returns a tuple with value and label to easily re-use the enum as init value
        for another enum.
        """

        return self.value, self.label

    @classmethod
    def label_from_int(cls, val: int) -> str | None:
        """
        Return label from int value.

        Args:
            cal: int corresponding to an elem of the enum cls.

        Returns:
            The "full word" value of an element of the enum corresponding to the given
            int value or None if the given value correspond to nothing.

        Examples:
            >>> AllDetection.label_from_int(1)
            'Radial Velocity'

            >>> AllDetection.label_from_int(2)
            'Timing'

            >>> AllDetection.label_from_int(3)
            'Controversial'

            >>> AllDetection.label_from_int(4)
            'Microlensing'

            >>> AllDetection.label_from_int(5)
            'Imaging'

            >>> AllDetection.label_from_int(6)
            'Primary Transit'

            >>> AllDetection.label_from_int(7)
            'Astrometry'

            >>> AllDetection.label_from_int(8)
            'TTV'

            >>> AllDetection.label_from_int(9)
            'Other'

            >>> AllDetection.label_from_int(10)
            'Spectrum'

            >>> AllDetection.label_from_int(11)
            'Theoretical'

            >>> AllDetection.label_from_int(12)
            'Flux'

            >>> AllDetection.label_from_int(13)
            'Secondary Transit'

            >>> AllDetection.label_from_int(14)
            'IR Excess'

            >>> AllDetection.label_from_int(15)
            'Kinematic'

            >>> AllDetection.label_from_int(99)
            None
        """
        for choice in cls:
            if val == choice.value:
                return choice.val_label[1]

        return None


# WARNING Be carefull with detection, because of filter, we test substring so
#   try to not have a filter name included into an other
class PlanetDetection(CustomIntegerChoices):
    """Detection type applicable to planets."""

    RADIAL_VELOCITY = AllDetection.RADIAL_VELOCITY.val_label
    TIMING = AllDetection.TIMING.val_label
    MICROLENSING = AllDetection.MICROLENSING.val_label
    IMAGING = AllDetection.IMAGING.val_label
    PRIMARY_TRANSIT = AllDetection.PRIMARY_TRANSIT.val_label
    ASTROMETRY = AllDetection.ASTROMETRY.val_label
    TTV = AllDetection.TTV.val_label
    OTHER = AllDetection.OTHER.val_label
    SECONDARY_TRANSIT = AllDetection.SECONDARY_TRANSIT.val_label
    KINEMATIC = AllDetection.KINEMATIC.val_label


# WARNING Be carefull with detection, because of filter, we test substring so
#   try to not have a filter name included into an other
class MassMeasurement(CustomIntegerChoices):
    """
    Measurement type applicable to mass measurement.

    Was formerly called MassDetection for historical reasons.
    """

    RADIAL_VELOCITY = AllDetection.RADIAL_VELOCITY.val_label
    TIMING = AllDetection.TIMING.val_label
    CONTROVERSIAL = AllDetection.CONTROVERSIAL.val_label
    MICROLENSING = AllDetection.MICROLENSING.val_label
    ASTROMETRY = AllDetection.ASTROMETRY.val_label
    TTV = AllDetection.TTV.val_label
    SPECTRUM = AllDetection.SPECTRUM.val_label
    THEORETICAL = AllDetection.THEORETICAL.val_label


# WARNING Be carefull with detection, because of filter, we test substring so
#   try to not have a filter name included into an other
class RadiusMeasurement(CustomIntegerChoices):
    """
    Measurement type applicable to radius measurement.

    Was formerly called RadiusDetection for historical reasons.
    """

    PRIMARY_TRANSIT = AllDetection.PRIMARY_TRANSIT.val_label
    THEORETICAL = AllDetection.THEORETICAL.val_label
    FLUX = AllDetection.FLUX.val_label


# WARNING Be carefull with detection, because of filter, we test substring so
#   try to not have a filter name included into an other
class DetectedDiscDetection(CustomIntegerChoices):
    """Detection type applicable to detected disc."""

    IMAGING = AllDetection.IMAGING.val_label
    IR_EXCESS = AllDetection.IR_EXCESS.val_label


# TODO clean or remove this ASAP
class Star2PubParam(CustomIntegerChoices):
    """
    To tell if a publication is about a specific parameter of a star or more generic
    one.
    """

    GENERIC_PUB = 0, "<generic_pub>"
    DISTANCE = 1, "distance"
    MASS = 2, "mass"
    AGE = 3, "age"
    TEFF = 4, "teff"
    RADIUS = 5, "radius"
    METALLICITY = 6, "metallicity"
    MAGNETIC_FIELD = 7, "magnetic_field"
    DETECTED_DISC = 8, "detected_disc"

    @staticmethod
    def from_label(field_name: str) -> "Star2PubParam | None":
        """
        Get value param from a label.

        Args:
            field_name: Name of field we want the value.

        Returns:
            Value of given label or None if the label doesn't exist.
        """
        for param, label in [(param, param.label) for param in Star2PubParam]:
            if label == field_name:
                return param

        return None


# TODO clean or remove this ASAP
class Plan2PubParam(CustomIntegerChoices):
    """
    To tell if a publication is about a specific parameter of a planet or more generic
    one.
    """

    GENERIC_PUB = 0, "<generic_pub>"
    MASS = 1, "mass"
    RADIUS = 2, "radius"
    AXIS = 3, "axis"
    PERIOD = 4, "period"
    ECCENTRICITY = 5, "eccentricity"
    OMEGA = 6, "omega"
    INCLINATION = 7, "inclination"
    TZERO_TR = 8, "tzero_tr"
    TZERO_VR = 9, "tzero_vr"
    TPERI = 10, "tperi"
    TCONJ = 11, "tconj"
    TEMP_CALCULATED = 12, "temp_calculated"
    TEMP_MEASURED = 13, "temp_measured"
    HOT_POINT_LON = 14, "hot_point_lon"
    LOG_G = 15, "log_g"
    TZERO_TR_SEC = 16, "tzero_tr_sec"
    LAMBDA_ANGLE = 17, "lambda_angle"
    K = 18, "k"
    GEOMETRIC_ALBEDO = 19, "geometric_albedo"
    IMPACT_PARAMETER = 20, "impact_parameter"
    MASS_SINI = 21, "mass_sini"

    @staticmethod
    def from_label(field_name: str) -> "Plan2PubParam | None":
        """
        Get value param from a label.

        Args:
            field_name: Name of field we want the value.

        Returns:
            Value of given label or None if the label doesn't exist.
        """
        for param, label in [(param, param.label) for param in Plan2PubParam]:
            if label == field_name:
                return param

        return None


class MassUnit(CustomIntegerChoices):
    """Possible units of mass as multiple of mass of Jupiter or Earth."""

    MJUP = 1, "Mjup"
    MEARTH = 2, "Mearth"


class RadiusUnit(CustomIntegerChoices):
    """Possible units of radius as multiple of radius Jupiter or Earth."""

    RJUP = 1, "Rjup"
    REARTH = 2, "Rearth"


class PublicationStatus(models.TextChoices):
    """Possible value for publication status."""

    R = "Published in a refereed paper"
    S = "Submitted to a professional journal"
    C = "Announced on a professional conference"
    W = "Announced on a website"

    @staticmethod
    def from_letter(letter: str) -> "PublicationStatus | None":
        """
        Get element of the enum by the letter corresponding to this element.

        Args:
            letter: Letter corresponding to an element of the enum.

        Retuns:
            Element of the enum corresponding to the given letter or None if the given
            letter correspond to nothing.

        Examples:
            >>> PublicationStatus.from_letter("R").value
            'Published in a refereed paper'

            >>> PublicationStatus.from_letter("S").value
            'Submitted to a professional journal'

            >>> PublicationStatus.from_letter("C").value
            'Announced on a professional conference'

            >>> PublicationStatus.from_letter("W").value
            'Announced on a website'

            >>> PublicationStatus.from_letter("T")
            None

        """

        match letter:
            case "R":
                return PublicationStatus.R
            case "S":
                return PublicationStatus.S
            case "C":
                return PublicationStatus.C
            case "W":
                return PublicationStatus.W
            case _:
                return None
