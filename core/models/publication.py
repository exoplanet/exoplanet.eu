"""
Database models for the academic publication destined to be linked with planets, stars
or specific parameters.
"""

# Standard imports
from datetime import datetime

# Django 💩 imports
from django.core.exceptions import ValidationError
from django.db.models import (
    PROTECT,
    CharField,
    DateField,
    ForeignKey,
    SmallIntegerField,
    TextField,
)
from django_stubs_ext.db.models import TypedModelMeta

# External imports
import dateutil.parser
import pytz

# Local imports
from .abstractbase import TrackedModel, WebVisibleModel
from .choices import PublicationType
from .dbimport import Import


def _pub_short_name(author: str, date: str, pub_id: int) -> str:
    """
    Produce a short representaion of a publication from its author name, date and id.

    Args:
        author: name of the author or authors.
        date: date of the publication.
        pub_id: publication unique identifier.

    Returns:
        A short representation of the publication.

    Examples:
        >>> _pub_short_name("Bob", "2014", 123)
        'Bob2014'
        >>> _pub_short_name("Bob Denard", "2014", 123)
        'Bob2014'
        >>> _pub_short_name("", "2014", 123)
        '1232014'
        >>> _pub_short_name("", "", 123)
        '123'
    """
    valid_author = author and len(author.split()) and author.split()[0] or str(pub_id)

    return f"{valid_author!s}{date!s}"


class Publication(WebVisibleModel, TrackedModel):
    """
    Model representing scientific publication.
    """

    # pylint: disable-next=too-few-public-methods
    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Meta info about the model"""

        # This is needed since we put our models in a module folder
        app_label = "core"

        # We want the defaukt ordering of this table to be by decreasing date and
        # decreasing modification date
        ordering = ["-date", "-modified"]

    title: TextField = TextField()

    author: TextField = TextField(null=True, blank=True)

    # TODO: WTF this is a date not a string. Need to change this VERY SOON
    date: CharField = CharField(max_length=10)

    # TODO rename this to prevent colision with python builtin type()
    type: SmallIntegerField = SmallIntegerField(choices=PublicationType.choices)

    journal_name: CharField = CharField(max_length=200, null=True, blank=True)

    journal_volume: CharField = CharField(max_length=15, null=True, blank=True)

    journal_page: CharField = CharField(max_length=15, null=True, blank=True)

    bibcode: CharField = CharField(max_length=19, null=True, blank=True)

    keywords: CharField = CharField(max_length=150, null=True, blank=True)

    reference: TextField = TextField(null=True, blank=True)

    # BUG: we store thing that are not url in there... most of them are links not url
    url: TextField = TextField(null=True, blank=True)

    doi: CharField = CharField(null=True, blank=True, max_length=255, unique=True)

    # maintenance fields
    # TODO this field is not used anymore. Remove it
    updated: DateField = DateField(
        null=True, blank=True, help_text="Date of last update"
    )

    dbimport: ForeignKey = ForeignKey(Import, null=True, blank=True, on_delete=PROTECT)

    # We also have the stars field that is a backward relationship from Star
    # We also have the planets field that is a backward relationship from Planet
    # https://docs.djangoproject.com/en/3.0/topics/db/queries/#backwards-related-objects

    def __str__(self) -> str:
        # id is an automatic member of django models
        # pylint: disable-next=no-member, line-too-long
        return _pub_short_name(self.author, self.date, self.id)  # type: ignore[attr-defined] # noqa: E501

    @property
    def date_as_datetime(self) -> datetime | None:
        """
        Publication date field as a datetime.

        Returns:
            Date field as a datetime.
        """
        if self.date is None:
            return None

        return dateutil.parser.parse(f"{self.date}").replace(
            tzinfo=pytz.timezone("UTC")
        )

    def clean(self) -> None:
        """
        Clean data before saving it in the db.

        Returns:
            Cleaned data.
        """
        _min_year = 1700
        _max_year = datetime.now().year + 1

        year_date = self.date
        if year_date is not None:
            try:
                date = datetime.strptime(f"01-01-{year_date}", "%m-%d-%Y").date()
            except ValueError as err:
                raise ValidationError(
                    f"Wrong date: {year_date}. Given date must be a year"
                ) from err

            if not _min_year <= date.year <= _max_year:
                raise ValidationError(
                    f"The given date <{year_date}> must be between {_min_year} and "
                    f"{_max_year}"
                )

        super().clean()
