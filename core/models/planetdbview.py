"""Materialized view used as a bridge between exoplanet.eu representation of exoplanet
data and VO representation."""

# Standard imports
import datetime as dt
from typing import NamedTuple

# Django 💩 imports
from django.db.models import (
    BooleanField,
    CharField,
    FloatField,
    IntegerField,
    SmallIntegerField,
)
from django_stubs_ext.db.models import TypedModelMeta

# Local imports
from ..fields import ErrorArrayField
from .choices import (
    DetectedDiscDetection,
    MassMeasurement,
    PlanetDetection,
    PlanetStatus,
    RadiusMeasurement,
)
from .planet import Planet, PlanetAbstractModel


# TODO use this guide to handle a real Postgres DB view with this class
# https://resources.rescale.com/using-database-views-in-django-orm/
class PlanetDBView(PlanetAbstractModel):
    """
    Materialized view of the planet DB but with the VO nomenclature (and star info
    integrated)
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        # Those are needed since this table maps to a database view
        managed = False
        db_table = "core_planetdbview"

    class _FilterDesc(NamedTuple):
        """Use to describe a dropdown menu item for the HTML repr in the admin."""

        # Name to displat for the dropdown item
        name: str
        # Index of the HTML param used in status dropdown filter of the catalog
        # Must be named index and his type must be int, we can't change his type
        # because django 💩
        index: int  # type: ignore[assignment]

        # list of statuses corresponding the the dropdown value
        statuses: list[PlanetStatus]

    PLANET_STATUSES_FILTER = [
        _FilterDesc(name="Confirmed", index=1, statuses=[PlanetStatus.CONFIRMED]),
        _FilterDesc(name="Candidate", index=2, statuses=[PlanetStatus.CANDIDATE]),
        _FilterDesc(
            name="Other",
            index=4,
            statuses=[
                PlanetStatus.CONTROVERSIAL,
                PlanetStatus.UNCONFIRMED,
                PlanetStatus.RETRACTED,
            ],
        ),
    ]

    mass_mjup: FloatField = FloatField()

    mass_error_mjup: ErrorArrayField = ErrorArrayField(FloatField())

    mass_mearth: FloatField = FloatField()

    mass_error_mearth: ErrorArrayField = ErrorArrayField(FloatField())

    mass_sini_mjup: FloatField = FloatField()

    mass_sini_error_mjup: ErrorArrayField = ErrorArrayField(FloatField())

    mass_sini_mearth: FloatField = FloatField()

    mass_sini_error_mearth: ErrorArrayField = ErrorArrayField(FloatField())

    mass_from_mass_sini_flag: BooleanField = BooleanField()

    radius_rjup: FloatField = FloatField()

    radius_error_rjup: ErrorArrayField = ErrorArrayField(FloatField())

    radius_rearth: FloatField = FloatField()

    radius_error_rearth: ErrorArrayField = ErrorArrayField(FloatField())

    publication_status_letter: CharField = CharField(max_length=1)

    @property
    def publication_status_text(self) -> str | None:
        """Publication status as a human readable label and not a character"""
        for pub_status_tuple in Planet.PUBLICATION_STATUSES.values():
            if self.publication_status_letter == pub_status_tuple[0]:
                return pub_status_tuple[1]
        return None

    @property
    def modified_date(self) -> str:
        """Modification da  te formated as a string"""
        return dt.datetime.strftime(self.modified, "%Y-%m-%d")

    @property
    def detection_type_human(self) -> str:
        """Detection type as a human readable label and not a number"""
        return ", ".join([PlanetDetection(v).label for v in list(self.detection_type)])

    planet_status_string: CharField = CharField(max_length=100)

    # TODO check if the HTML is correct for the new design
    @property
    def publication_status_html(self) -> str | None:
        """Publication status as a HTML span string"""
        for pub_letter, pub_desc in Planet.PUBLICATION_STATUSES.values():
            if pub_letter == self.publication_status_letter:
                return f'<span title="{pub_desc!s}">{pub_letter!s}</span>'
        return None

    @property
    def mass_detection_type_human(self) -> str | None:
        """Mass detection type as a human readable label and not a number"""
        if self.mass_measurement_type:
            return MassMeasurement(self.mass_measurement_type).label

        return None

    @property
    def radius_measurement_type_human(self) -> str | None:
        """Radius detection type as a human readable label and not a number"""
        if self.radius_measurement_type:
            return RadiusMeasurement(self.radius_measurement_type).label

        return None

    # BUG This is not an ErrorArrayField but just an ArrayField
    alternate_names: ErrorArrayField = ErrorArrayField(CharField(max_length=60))

    @property
    def alternate_names_human(self) -> str | None:
        """
        List of all the alternate names of the planet as a long string separated by
        commas.
        """
        if self.alternate_names:
            return ", ".join(self.alternate_names)
        else:
            return None

    # BUG This is not an ErrorArrayField but just an ArrayField
    molecules: ErrorArrayField = ErrorArrayField(CharField(max_length=60))

    @property
    def molecules_human(self) -> str | None:
        """
        List of all the molecules of the planet as a long string separated by commas.
        """
        if self.molecules:
            return ", ".join(self.molecules)
        else:
            return None

    confirmed_planet_count: IntegerField = IntegerField()

    star_name: CharField = CharField(max_length=60)

    star_ra_deg: FloatField = FloatField()

    star_ra_sex: CharField = CharField(max_length=10)

    star_dec_deg: FloatField = FloatField()

    star_dec_sex: CharField = CharField(max_length=10)

    star_magnitude_v: FloatField = FloatField()

    star_magnitude_i: FloatField = FloatField()

    star_magnitude_j: FloatField = FloatField()

    star_magnitude_h: FloatField = FloatField()

    star_magnitude_k: FloatField = FloatField()

    star_distance: FloatField = FloatField()

    star_distance_error: ErrorArrayField = ErrorArrayField(FloatField())

    star_metallicity: FloatField = FloatField()

    star_metallicity_error: ErrorArrayField = ErrorArrayField(FloatField())

    star_mass: FloatField = FloatField()

    star_mass_error: ErrorArrayField = ErrorArrayField(FloatField())

    star_radius: FloatField = FloatField()

    star_radius_error: ErrorArrayField = ErrorArrayField(FloatField())

    star_spec_type: CharField = CharField(max_length=60)

    star_age: FloatField = FloatField()

    star_age_error: ErrorArrayField = ErrorArrayField(FloatField())

    star_teff: FloatField = FloatField()

    star_teff_error: ErrorArrayField = ErrorArrayField(FloatField())

    star_detected_disc: SmallIntegerField = SmallIntegerField(
        null=True,
        blank=True,
        choices=DetectedDiscDetection.choices,
    )

    @property
    def star_detected_disc_human(self) -> str | None:
        """tar detected disk as a human readable label and not a number"""
        if self.star_detected_disc is not None:
            return DetectedDiscDetection(self.star_detected_disc).label
        else:
            return None

    star_magnetic_field: BooleanField = BooleanField(
        null=True, blank=True, choices=((True, "Yes"), (False, "No"))
    )

    @property
    def star_magnetic_field_human(self) -> str | None:
        """Radius detection type as a human readable yes/no and not a boolean"""
        if self.star_magnetic_field is None:
            return None
        elif self.star_magnetic_field is True:
            return "Yes"
        else:
            return "No"

    # BUG This is not an ErrorArrayField but just an ArrayField
    star_alternate_names: ErrorArrayField = ErrorArrayField(CharField(max_length=60))

    @property
    def star_alternate_names_human(self) -> str | None:
        """
        List of all the star alternate names of the planet as a long string separated by
        commas.
        """
        if self.star_alternate_names:
            return ", ".join(self.star_alternate_names)
        else:
            return None
