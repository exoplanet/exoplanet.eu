# Standard imports
from decimal import Decimal
from typing import cast

# Django 💩 imports
from django.conf import settings

# External imports
import py_linq_sql as pls
from mpmath import cos as mpm_cos
from mpmath import libmp, mp, mpf
from mpmath import nstr as mpf_to_str
from mpmath import radians as mpm_radians
from mpmath import sin as mpm_sin
from psycopg import Connection
from psycopg.rows import Row
from py_linq import Enumerable
from py_linq_sql.utils.classes.magicdotpath import BaseMagicDotPath, MagicDotPathWithOp

mp.dps = 20


def _degree_to_radian_decimal(number: Decimal) -> Decimal:
    """
    Get an angle in radians and decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the radians.

    Returns:
        Radians and decimal value of given degrees number.

    Examples:
        >>> _degree_to_radian_decimal(Decimal(12))
        Decimal('0.20943951023931954923')
    """
    return Decimal(
        mpf_to_str(
            mpm_radians(mpf(libmp.from_Decimal(number))),
            20,
            strip_zeros=False,
        ),
    )


def _sind_decimal(number: Decimal) -> Decimal:
    """
    Get the sin in decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the sinus.

    Returns:
        Sinus in radians and decimal of a degrees value.

    Examples:
        >>> _sind_decimal(Decimal(12))
        Decimal('0.20791169081775933710')
    """
    number_in_radians = _degree_to_radian_decimal(number)

    return Decimal(
        mpf_to_str(
            mpm_sin(mpf(libmp.from_Decimal(number_in_radians))),
            20,
            strip_zeros=False,
        ),
    )


def _cosd_decimal(number: Decimal) -> Decimal:
    """
    Get the cos in decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the cosine.

    Returns:
        Cosinus in radians and decimal of a degrees value.

    Examples:
        >>> _cosd_decimal(Decimal(12))
        Decimal('0.97814760073380563793')
    """
    number_in_radians = _degree_to_radian_decimal(number)

    return Decimal(
        mpf_to_str(
            mpm_cos(mpf(libmp.from_Decimal(number_in_radians))),
            20,
            strip_zeros=False,
        ),
    )


def _get_error_message(same_pos_stars: list[Row]) -> str:
    """
    Get error message fo is_at_the_same_position_of_other_star.

    Args:
        same_pos_stars: Star(s) at the same position.

    Returns:
        Good error message for star(s) at the same position.
    """
    star_already_exist = (
        "A star already exist" if len(same_pos_stars) == 1 else "Stars already exists"
    )
    stars = [
        # Type: ignore because Row type is a NamedTuple whith "custom" attr can't be
        # defined before request
        f"{element.name}:{element.star_id}"  # type: ignore[attr-defined]
        for element in same_pos_stars
    ]
    return f"{star_already_exist} in the same region: {', '.join(stars)}."


def _return_stars_at_same_position_or_continue(
    same_pos_stars: Enumerable,
    star_id: int | None,
    conn_to_close: Connection | None,
) -> tuple[None, None] | tuple[bool, str]:
    """
    Return star(s) at the same position if exist(s).

    Args:
        same_pos_stars: Possible star(s) at the same position.
        star_id: Id of star to check. Can be None if it's a new star.
        conn_to_close: Connection to close. Can be None if no connection to close.

    Returns:
        True and error message if we have star(s) at the same position,
        otherwise (None, None).
    """
    if not same_pos_stars:
        return None, None

    same_pos_stars_as_list = cast(Enumerable, same_pos_stars).to_list()

    if (
        len(same_pos_stars_as_list) == 1
        and same_pos_stars_as_list[0].star_id == star_id
    ):
        return None, None

    if conn_to_close is not None:
        conn_to_close.close()

    return True, _get_error_message(same_pos_stars_as_list)


def _force_between(
    mdp: BaseMagicDotPath,
    min_val: float | int | Decimal,
    max_val: float | int | Decimal,
) -> MagicDotPathWithOp:
    assert min_val < max_val

    return pls.greatest(pls.least(mdp, max_val), min_val)


def is_at_the_same_position_of_other_star(
    ra: Decimal,
    dec: Decimal,
    star_id: int | None,
    epsilon: Decimal = Decimal(1) / Decimal(3600),
) -> tuple[bool, str]:
    """
    Check if a star exist at the given position in the database.

    See:
        https://en.wikipedia.org/wiki/Angular_distance
        For more details on the formula of Angular distance.

    For sin and cos we choose 20 significant digits (precision).

    Args:
        ra: Ra of the star to check.
        dec: Dec of the star to check.
        star_id: Id of the star to check. Can be None for a new star.
        epsilon: Tolerance on the position.

    Returns:
        True and the error message if a object already exist at the position,
        False and empty error message otherwise.

    Warnings:
        Postgresql make all trigonometric function with float in input and output.
        If the difference between the two position are very close of the epsilon
        you can have false positive. It's very specific case that can happen when the
        gap is approximately smaller or equal to 1,500983157e-6 raidans
        or 8.6e-5 degrees.
    """
    if ra is None or dec is None:
        raise ValueError(f"ra = {ra}, dec = {dec}. Can't be None.")

    conn_info = settings.DATABASES["default"]
    conn = pls.connect(
        user=conn_info["USER"],
        password=conn_info["PASSWORD"],
        host=conn_info["HOST"],
        port=conn_info["PORT"],
        database=conn_info["NAME"],
    )

    exactly_same_ra_dec = (
        pls.SQLEnumerable(conn, "core_star")
        .select(lambda x: {"star_id": x.id, "name": x.name})
        .where(lambda x: (x.dec == dec) & (x.ra == ra))
        .execute()
    )

    exact_result = _return_stars_at_same_position_or_continue(
        exactly_same_ra_dec,
        star_id,
        conn,
    )
    if exact_result != (None, None):
        return cast(tuple[bool, str], exact_result)

    same_position_near_epsilon = (
        pls.SQLEnumerable(conn, "core_star")
        .select(lambda x: {"star_id": x.id, "name": x.name})
        .where(
            lambda x: (x.dec >= dec - epsilon) & (x.dec <= dec + epsilon),
        )
        .where(
            lambda x: pls.acosd(
                # Due to floating point errors sometimes the computation gives
                # a result slightly over 1.0 or under -1.0 raising an exception
                # causing an internal server error. So we confine the result to
                # [-1, 1]
                _force_between(
                    pls.sind(x.dec) * _sind_decimal(dec)
                    + pls.cosd(x.dec) * _cosd_decimal(dec) * pls.cosd(x.ra - ra),
                    min_val=-1,
                    max_val=1,
                )
            )
            < epsilon,
        )
        .execute()
    )

    conn.close()

    near_result = _return_stars_at_same_position_or_continue(
        same_position_near_epsilon,
        star_id,
        None,
    )
    if near_result != (None, None):
        return cast(tuple[bool, str], near_result)

    return (False, "")
