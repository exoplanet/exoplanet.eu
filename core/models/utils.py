# Django 💩 imports
from django.db.models import Q, QuerySet

# External imports
from slugify import slugify

# Local imports
from .choices import WebStatus

DEFAULT_NAME_ID_SEPARATOR = "--"
DEFAULT_PLACEHOLDER = "_"
QUERY_NOT_HIDDEN_STATUS = ~Q(status=WebStatus.HIDDEN)


def safe_name(obj_name: str, char_place_holder: str = DEFAULT_PLACEHOLDER) -> str:
    """
    Returns version of the planet/star name perfectly safe for use in urls or JSON.

    The output is:
    - all lowercase
    - all non-ascii letters and numbers chars replaced by _
    - all space chars replaced by _
    - no prepending _
    - no postpending _
    - no more than one _ in a row

    Args:
        obj_name: name of the planet/star it can contains spaces or in fact any char
        char_place_holder: char to use to replace the invalid char in the name

    Examples:
        >>> safe_name('Berkeley 19')
        'berkeley_19'
        >>> safe_name('--ABC____DEF--')
        'abc_def'
        >>> safe_name('  X Y   Z   ')
        'x_y_z'
        >>> safe_name('影師嗎')
        'ying_shi_ma'
        >>> safe_name('ʻOumuamua')
        'oumuamua'
    """
    assert len(char_place_holder) == 1

    return slugify(obj_name, separator=char_place_holder)


def obj_slugify(
    obj_name: str,
    obj_id: int,
    char_place_holder: str = DEFAULT_PLACEHOLDER,
) -> str:
    """
    Return an url compatible version of the given planet or star name.

    Args:
        obj_name: name of the planet/star it can contains spaces or in fact any char
        obj_id: unique id of the object in the database
        char_place_holder: char to use to replace the invalid char in the name
        separator: string to use to separate the safe name from the id

    Examples:
        >>> obj_slugify("Berkeley 19", 12345)
        'berkeley_19--12345'
    """
    assert obj_id > 0
    assert char_place_holder not in DEFAULT_NAME_ID_SEPARATOR

    s_name = safe_name(obj_name, char_place_holder)

    return f"{s_name}{DEFAULT_NAME_ID_SEPARATOR}{obj_id!s}"


def id_from_slug(slug: str) -> int:
    """
    Return the integer id of an object from a slugified string generated by
    `obj_slugify()`.

    It is not possible to get the object name since there is information loss in the
    safe name generation.

    Args:
        slug: slugified name generated by `obj_slugify()`

    Returns:
        integer id of the object in the database as it was provided to `obj_slugify()`

    Examples:
        >>> id_from_slug("berkeley_19--12345")
        12345
    """
    return int(slug.split(DEFAULT_NAME_ID_SEPARATOR)[1])


def filter_query_set_status_is_not_hidden(query_set: QuerySet) -> QuerySet:
    """
    Filter a qeury set to remove all objects with hidden status.

    Args:
        query_set: Query set to filter.

    Returns:
        Given query set without object with hidden status.
    """
    return query_set.filter(QUERY_NOT_HIDDEN_STATUS)
