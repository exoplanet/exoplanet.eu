# Django 💩 imports
from django.db.models import PROTECT, AutoField, ForeignKey
from django_stubs_ext.db.models import TypedModelMeta

# Local imports
from .abstractbase import WithCoordinateModel
from .dbimport import Import


class PlanetarySystem(WithCoordinateModel):
    """
    Model representing a planet with only the field displayed in the compact view on the
    web site.

    Those fields are the most important ones to have a first idea of the planet.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    id: AutoField = AutoField(primary_key=True)

    dbimport: ForeignKey = ForeignKey(Import, null=True, blank=True, on_delete=PROTECT)

    def __str__(self) -> str:
        return f"RA:{self.ra} DEC:{self.dec}"

    # We also have the planets field that is a backward relationship from Planet
    # https://docs.djangoproject.com/en/3.0/topics/db/queries/#backwards-related-objects
    # WARNING it used to be a method that returned a stringified list of planet names
