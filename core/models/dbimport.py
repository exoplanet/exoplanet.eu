"""
Classes to have tables to store the import information registered by exoimport tool.
"""

# Django 💩 imports
from django.db.models import AutoField, BinaryField, CharField, DateTimeField, Model

# pylint: disable-next=import-error,no-name-in-module
from django_stubs_ext.db.models import TypedModelMeta


class Import(Model):
    """Store the import information registered by exoimport tool."""

    # pylint: disable-next=missing-class-docstring, too-few-public-methods
    class Meta(TypedModelMeta):
        # This is needed since we put our models in a module folder
        app_label = "core"

    id: AutoField = AutoField(primary_key=True)
    name: CharField = CharField(null=False, unique=True, max_length=100)
    created: DateTimeField = DateTimeField(null=False, auto_now_add=True)
    external_csv: BinaryField = BinaryField(null=True)
    mapping_yaml: BinaryField = BinaryField(null=True)
    internal_csv: BinaryField = BinaryField(null=True)
    exoimport_version: CharField = CharField(null=False, max_length=80)
