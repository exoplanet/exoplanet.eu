# Standard imports
from datetime import date
from decimal import Decimal
from typing import NamedTuple, cast

# Django 💩 imports
from django.core.exceptions import ValidationError
from django.db.models import (
    CASCADE,
    PROTECT,
    SET_NULL,
    AutoField,
    CharField,
    FloatField,
    ForeignKey,
    ManyToManyField,
    Model,
    SmallIntegerField,
    TextField,
)
from django.urls import reverse
from django_stubs_ext.db.models import TypedModelMeta

# External imports
from django_jsonform.models.fields import ArrayField

# Local imports
from ..fields import (
    AsciiCharField,
    ErrorArrayField,
    PositiveFloatValueAsStringField,
    norm_error_array,
)
from ..helpers import (
    mass_in_specific_unit,
    mass_planet_from_data,
    radius_in_specific_unit,
)
from ..postgresql import NullsLastQueryManager
from .abstractbase import TrackedModel, WebVisibleModel
from .choices import (
    MassMeasurement,
    MassUnit,
    Plan2PubParam,
    PlanetDetection,
    PlanetStatus,
    RadiusMeasurement,
    RadiusUnit,
    WebStatus,
)
from .dbimport import Import
from .planetarysystem import PlanetarySystem
from .publication import Publication
from .star import Star
from .utils import obj_slugify, safe_name


def planet_name_already_used_as_alternate_name(name_to_check: str) -> tuple[bool, str]:
    """
    Check if a given name is already in database as an alternate name of planet.

    Args:
        name_to_check: Name to check.

    Returns:
        True and the correct error message if the name is already used.
        False and an empty error message otherwise.
    """
    # pylint: disable-next=no-member
    check_same_name = AlternatePlanetName.objects.filter(
        name__iexact=name_to_check.lower()
    )

    # AlternatePlanetName has an automatic attribute `planet_id`
    # with the ForeignKey Field
    if check_same_name:
        planet_from_id = Planet.objects.filter(
            pk=check_same_name[0].planet_id,  # type: ignore[attr-defined]
        )

        return True, planet_from_id[0].name

    return False, ""


def name_already_used_as_planet_name(name_to_check: str) -> tuple[bool, str]:
    """
    Check if a given name is already in database as "main" name of a planet.

    Args:
        name_to_check: Name to check.

    Returns:
        True and the correct error message if the name is already used.
        False and an empty error message otherwise.
    """
    if check_same_name := Planet.objects.filter(name__iexact=name_to_check.lower()):
        return True, check_same_name[0].name

    return False, ""


class _FilterDesc(NamedTuple):
    """
    Describe a filter planet status.

    This prevent from using a raw tuple that could contain anything

    WARNING this used to be in the PlanetView class
    """

    name: str
    # Must be named index and his type must be int, we can't change his
    # type because django 💩
    index: int  # type: ignore[assignment]
    statuses: list[PlanetStatus]


# Describe the 3 possible filters to display for planets
# The indexes is for the HTML parameter used in status dropdown filter of the catalog
# statuses is the list of statuses corresponding the the dropdown value
# WARNING this used to be in the PlanetView class
PLANET_STATUSES_FILTER = [
    _FilterDesc(
        name="Confirmed",
        index=1,
        statuses=[PlanetStatus.CONFIRMED],
    ),
    _FilterDesc(
        name="Candidate",
        index=2,
        statuses=[PlanetStatus.CANDIDATE],
    ),
    _FilterDesc(
        name="Other",
        index=4,
        statuses=[
            PlanetStatus.CONTROVERSIAL,
            PlanetStatus.UNCONFIRMED,
            PlanetStatus.RETRACTED,
        ],
    ),
]

PLANET_NAME_MAX_LENGTH = 100


class PlanetCompactAbstractModel(TrackedModel):
    """
    Model representing a planet with only the field displayed in the compact view on the
    web site.

    Those fields are the most important ones to have a first idea of the planet.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

        abstract = True

    id: AutoField = AutoField(primary_key=True)

    # BUG we should validate the name to prevent unwanted character
    name = AsciiCharField(
        max_length=PLANET_NAME_MAX_LENGTH, unique=True, verbose_name="Name"
    )

    period: FloatField = FloatField(
        null=True, blank=True, verbose_name="Orbital Period", help_text="in day"
    )

    axis: FloatField = FloatField(
        null=True, blank=True, verbose_name="Semi-Major Axis", help_text="in AUs"
    )

    eccentricity: FloatField = FloatField(
        null=True, blank=True, verbose_name="Orbital Eccentricity"
    )

    # BUG this should be a float
    inclination: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Orbital Inclination",
        help_text="in degrees",
    )

    angular_distance: FloatField = FloatField(
        null=True, blank=True, verbose_name="Angular Distance", help_text="in arcsec"
    )

    discovered: SmallIntegerField = SmallIntegerField(
        null=True,
        blank=True,
        # WARNING this force us to do a migraton after each new year...
        # TODO: To change so that we no longer have to make a migration every year
        # Choices is all years from 1988 to this  years + 1 (+2 because in python range
        # is [1, 20[), with this we can put article/planets which will not be published
        # until the following year
        choices=[(y, y) for y in range(1988, date.today().year + 2)],
        default=date.today().year,
        verbose_name="Year of discovery",
        help_text="When the planet was detected",
    )

    # TODO this should be integrated in the WebVisibleModel
    # TODO give a less confusing name status --> web_status
    status: SmallIntegerField = SmallIntegerField(
        choices=WebStatus.choices,
        default=WebStatus.ACTIVE,
        help_text="Whether the object is visible on the website or not",
    )

    def is_visible(self) -> bool:
        return bool(self.status == WebStatus.ACTIVE)

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    is_visible.boolean = True  # type: ignore[attr-defined]
    is_visible.admin_order_field = "status"  # type: ignore[attr-defined]
    is_visible.short_description = "Status"  # type: ignore[attr-defined]

    # This is an array of PlanetDetection
    # TODO Specialize the ArrayField to a PlanetDetectionArrayField ?
    detection_type: ArrayField = ArrayField(
        SmallIntegerField(),
        help_text=(
            "How the planet was detected: <br>"
            + "<br>".join([f"{d.value}: {d.label}" for d in PlanetDetection])
        ),
    )

    @property
    def safe_name(self) -> str:
        """
        Return the slugified name of the planet, that is safe to use in any url.

        i.e. Berkeley 19 --> berkeley_19
        """
        return safe_name(self.name)

    # TODO Add some (doc)test to check if this works correctly
    @property
    def url_keyword(self) -> str:
        """
        Generate URL keyword from name:

        i.e. Berkeley 19 with id 12345--> berkeley_19--12345
        """
        return obj_slugify(self.name, self.id)

    @property
    def name_url_html(self) -> str:
        url = reverse(
            "legacy_planet_detail",
            kwargs=dict(
                planet_safe_name=safe_name(self.name),
                planet_id_str=str(self.id),
            ),
        )
        return f'<a href="{url}">{self.name}</a>'

    def _status(self) -> str:
        return WebStatus(self.status).label

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    _status.admin_order_field = "status"  # type: ignore[attr-defined]
    _status.short_description = "Status"  # type: ignore[attr-defined]


class PlanetAbstractModel(PlanetCompactAbstractModel):
    """
    Model representing exoplanet with all the field of the full view of the planet.
    """

    class Meta(TypedModelMeta):
        # This is needed since we put our models in a module folder
        app_label = "core"

        abstract = True
        ordering = ["stars__name", "name"]
        permissions = [
            ("import_planets", "Can import planets"),
            ("suggest_planets", "Can suggest planets"),
        ]

    axis_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True, size=2
    )

    period_error: ErrorArrayField = ErrorArrayField(FloatField(), null=True, blank=True)

    eccentricity_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    omega: FloatField = FloatField(
        null=True, blank=True, verbose_name="Argument of Periastron"
    )

    omega_error: ErrorArrayField = ErrorArrayField(FloatField(), null=True, blank=True)

    tzero_tr: FloatField = FloatField(
        null=True,
        blank=True,
        help_text="in JD 2,450,000 units",
        verbose_name="Primary Transit",
    )

    tzero_tr_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    tzero_vr: FloatField = FloatField(
        null=True,
        blank=True,
        help_text="in JD 2,450,000 units",
        verbose_name="Zero Radial Speed time",
    )

    tzero_vr_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    tperi: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Epoch of Periastron",
        help_text="in JD 2,450,000 units",
    )

    tperi_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True, help_text="in Julian days"
    )

    tconj: FloatField = FloatField(
        null=True, blank=True, help_text="in JD 2,450,000 units"
    )

    tconj_error: ErrorArrayField = ErrorArrayField(FloatField(), null=True, blank=True)

    inclination_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    # TODO We should add a validation since this field may contain arbitrary HTML tags
    remarks: TextField = TextField(null=True, blank=True)

    # TODO We should add a validation since this field to be sure to have real urls
    # TODO We need to cleanup those data, at the moment it full of crapy HTML
    other_web: TextField = TextField(
        null=True, blank=True, verbose_name="Link to other websites"
    )

    mass_measurement_type: SmallIntegerField = SmallIntegerField(
        null=True, blank=True, choices=MassMeasurement.choices
    )

    radius_measurement_type: SmallIntegerField = SmallIntegerField(
        null=True, blank=True, choices=RadiusMeasurement.choices
    )

    tzero_tr_sec: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Secondary transit",
        help_text="in JD 2,450,000 units",
    )

    tzero_tr_sec_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    lambda_angle: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Sky-projected anomaly angle",
        help_text="in degrees",
    )

    lambda_angle_error: ErrorArrayField = ErrorArrayField(
        FloatField(), null=True, blank=True
    )

    albedo: FloatField = FloatField(
        null=True, blank=True, verbose_name="Geometric albedo"
    )

    albedo_error: ErrorArrayField = ErrorArrayField(
        FloatField(),
        null=True,
        blank=True,
        verbose_name="Geometric albedo error",
    )

    temp_calculated: FloatField = FloatField(
        null=True, blank=True, verbose_name="Calculated temperature", help_text="in K"
    )

    temp_calculated_error: ErrorArrayField = ErrorArrayField(
        FloatField(),
        null=True,
        blank=True,
        verbose_name="Calculated temperature error",
        help_text="in K",
    )

    temp_measured: FloatField = FloatField(
        null=True, blank=True, verbose_name="Measured temperature", help_text="in K"
    )

    hot_point_lon: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Hottest point longitude",
        help_text="in degrees counted from substellar point",
    )

    log_g: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="log(g)",
        help_text="For planets detected by imaging",
    )

    impact_parameter: FloatField = FloatField(
        null=True, blank=True, verbose_name="Impact Parameter b", help_text="In %"
    )

    impact_parameter_error: ErrorArrayField = ErrorArrayField(
        FloatField(),
        null=True,
        blank=True,
        verbose_name="Impact Parameter b error",
    )

    k: FloatField = FloatField(
        null=True,
        blank=True,
        verbose_name="Velocity Semiamplitude K",
        help_text="In m/s",
    )

    k_error: ErrorArrayField = ErrorArrayField(
        FloatField(),
        null=True,
        blank=True,
        verbose_name="Velocity Semiamplitude K error",
    )

    def __str__(self) -> str:
        return str(self.name)

    # HACK To dislay in admin tab we need to add attribute to a proprety 🤯.
    # But it's very unclear code.
    # DJANGO == 💩. So we nned a type ignore.
    __str__.short_description = "Name of a planet"  # type: ignore[attr-defined]


class Planet(PlanetAbstractModel):
    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    # This enable good null handling in postgresql
    objects = NullsLastQueryManager()

    # TODO replace the 3 fields mass_sini_* by one ArrayField subclass providing clean
    #      OOP access to values, error_min, error_max and maybe even unit
    # TODO replace this with 2 fields: mass_sini_mjup and mass_sini_mearth when one is
    #      filled, the other should be cleanly computed with due respect to decimal
    #      length OR always store everything in kg and just remember the original unit
    #      and nb of significant digits
    # BUG this should be stored as a DecimalField (all the conversions str <-> float
    #     are sources of errors and honeypots for nasty bugs)
    mass_sini_string = PositiveFloatValueAsStringField(
        max_length=100, null=True, blank=True, verbose_name="Planetary Mass * sin(i)"
    )
    # BUG This should be an array of Decimal
    mass_sini_error_string: ErrorArrayField = ErrorArrayField(
        CharField(max_length=100),
        null=True,
        blank=True,
        verbose_name="Planetary Mass * sin(i) error",
    )
    mass_sini_unit: SmallIntegerField = SmallIntegerField(
        null=True,
        blank=False,
        choices=MassUnit.choices,
        verbose_name="Unit",
        default=MassUnit.MJUP,
    )

    def mass_sini_in_unit(self, desired_unit: MassUnit) -> Decimal:
        return mass_in_specific_unit(
            mass=self.mass_sini_string,
            unit=MassUnit(self.mass_sini_unit),
            desired_unit=desired_unit,
        )

    def mass_sini_error_in_unit(self, desired_unit: MassUnit) -> list[Decimal]:
        return [
            mass_in_specific_unit(
                mass=mass, unit=MassUnit(self.mass_sini_unit), desired_unit=desired_unit
            )
            for mass in self.mass_sini_error_string  # pylint: disable=not-an-iterable
        ]

    # TODO replace the 3 fields mass_sini_* by one ArrayField subclass providing clean
    #      OOP access to values, error_min, error_max and maybe even unit
    # TODO replace this with 2 fields: mass_sini_mjup and mass_sini_mearth when one is
    #      filled, the other should be cleanly computed with due respect to decimal
    #      length OR always store everything in kg and just remember the original unit
    #      and nb of significant digits
    # BUG this should be stored as a DecimalField (all the conversions str <-> float
    #     are sources of errors and honeypots for nasty bugs)
    mass_detected_string = PositiveFloatValueAsStringField(
        max_length=100, null=True, blank=True, verbose_name="Planetary Mass"
    )
    mass_detected_error_string: ErrorArrayField = ErrorArrayField(
        CharField(max_length=100),
        null=True,
        blank=True,
        verbose_name="Planetary Mass error",
    )
    mass_detected_unit: SmallIntegerField = SmallIntegerField(
        null=False,
        blank=False,
        choices=MassUnit.choices,
        verbose_name="Unit",
        default=MassUnit.MJUP,
    )

    def mass_in_unit(self, desired_unit: MassUnit) -> Decimal | None:
        return mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=self.mass_detected_string,
            mass_detected_unit=MassUnit(self.mass_detected_unit),
            mass_sini_str=self.mass_sini_string,
            mass_sini_unit=MassUnit(self.mass_sini_unit),
            inclination=self.inclination,
        )

    def mass_error_in_unit(self, desired_unit: MassUnit) -> list[Decimal | None]:
        return [
            mass_planet_from_data(
                desired_unit=desired_unit,
                mass_detected_str=str(mass_d),
                mass_detected_unit=MassUnit(self.mass_detected_unit),
                mass_sini_str=str(mass_s),
                mass_sini_unit=MassUnit(self.mass_sini_unit),
                inclination=self.inclination,
            )
            for mass_d, mass_s in zip(
                norm_error_array(self.mass_detected_error_string),
                norm_error_array(self.mass_sini_error_string),
            )
        ]

    def is_mass_from_mass_sini(self) -> bool:
        """
        The planetdbview data are create via PLSQL code that do some conversion (not
        always very accurate...) and we need this function to give the same results. The
        reference code is the PLSQL function `is_mass_from_mass_sini()` located in the
        SQL migration file `core/migrations/0006_add_planetdbview_create.sql`.
        """
        return not self.mass_detected_string and self.mass_sini_string

    # TODO replace the 3 fields radius_* by one ArrayField subclass providing clean
    #      OOP access to values, error_min, error_max and maybe even unit
    # TODO replace this with 2 fields: radius_rjup and radius_rearth when one is
    #      filled, the other should be cleanly computed with due respect to decimal
    #      length OR always store everything in km and just remember the original unit
    #      and nb of significant digits
    # BUG this should be stored as a DecimalField (all the conversions str <-> float
    #     are sources of errors and honeypots for nasty bugs)
    radius_string = PositiveFloatValueAsStringField(
        max_length=100, null=True, blank=True, verbose_name="Planetary Radius"
    )
    radius_error_string: ErrorArrayField = ErrorArrayField(
        CharField(max_length=100),
        null=True,
        blank=True,
        verbose_name="Planetary Radius error",
    )
    radius_unit: SmallIntegerField = SmallIntegerField(
        null=False,
        blank=False,
        choices=RadiusUnit.choices,
        verbose_name="Unit",
        default=RadiusUnit.RJUP,
    )

    # BUG this should compute and return a list of Decimal
    def radius_in_unit(self, desired_unit: RadiusUnit) -> float:
        return radius_in_specific_unit(
            radius=self.radius_string,
            unit=RadiusUnit(self.radius_unit),
            desired_unit=desired_unit,
        )

    # BUG this should compute and return a list of Decimal
    def radius_error_in_unit(self, desired_unit: RadiusUnit) -> list[float]:
        return [
            radius_in_specific_unit(
                radius=radius,
                unit=RadiusUnit(self.radius_unit),
                desired_unit=desired_unit,
            )
            for radius in self.radius_error_string  # pylint: disable=not-an-iterable
        ]

    temp_measured_error: ErrorArrayField = ErrorArrayField(
        FloatField(),
        null=True,
        blank=True,
        verbose_name="temp_measured error",
        help_text="in K",
    )

    # BUG This structure is overcomplicated it must be changed
    # TODO replace this with a clean Enumeration Type which will be possible only if we
    #      switch publication status to CharField
    # See: https://docs.djangoproject.com/en/3.0/ref/models/fields/#enumeration-types
    PUBLICATION_STATUSES = {
        1: ("R", "Published in a refereed paper"),
        2: ("S", "Submitted to a professional journal"),
        3: ("C", "Announced on a professional conference"),
        4: ("W", "Announced on a website"),
    }  # planetstatut table

    # BUG we should not use a SmallIntegerField but rather a CharField
    publication_status: SmallIntegerField = SmallIntegerField(
        choices=[(k, code) for k, (code, name) in PUBLICATION_STATUSES.items()],
        help_text="<br>".join(
            [f"{code} &mdash; {name}" for code, name in PUBLICATION_STATUSES.values()]
        ),
    )

    # WARNING this method is only useful since we store publication_status as an int and
    #         not as a char. As soon as publication_status type is CharField this method
    #         should be removed.
    def get_publication_status_letter(self) -> str:
        return self.PUBLICATION_STATUSES[self.publication_status][0]

    planet_status: SmallIntegerField = SmallIntegerField(
        blank=False, choices=PlanetStatus.choices, default=PlanetStatus.CONFIRMED
    )

    def get_planet_status_string(self) -> str:
        """
        Get the planet_status label.

        Used for compatibility with the planetdbview field `planet_status_string`.
        """
        return PlanetStatus(self.planet_status).label

    main_star: ForeignKey = ForeignKey(Star, null=True, blank=True, on_delete=PROTECT)

    # WARNING This is bad but should be temporary we don't need this we have the same
    #   info in planet to star
    planetary_system: ForeignKey = ForeignKey(
        PlanetarySystem,
        null=True,
        blank=True,
        help_text="System for rogue planet",
        on_delete=SET_NULL,
        related_name="planets",
    )

    publications: ManyToManyField = ManyToManyField(
        Publication, through="Planet2Publication", related_name="planets"
    )

    stars: ManyToManyField = ManyToManyField(
        Star,
        through="Planet2Star",
        related_name="planets",
        help_text="Name of a host star(s)",
        blank=True,
    )

    dbimport: ForeignKey = ForeignKey(Import, null=True, blank=True, on_delete=PROTECT)

    # We also have the alternate_planet_names field that is a backward relationship
    # from Planet
    # https://docs.djangoproject.com/en/3.0/topics/db/queries/#backwards-related-objects
    # WARNING it used to be called alternate_planet_name (singular instead of plural)

    @property
    def star(self) -> Star | None:
        try:
            return cast(Star, self.stars.all()[0])  # pylint: disable=no-member
        except IndexError:
            return None

    def clean(self) -> None:
        """
        Function that will be automatically called before a model instance is saved.

        Warning: if calling `save()` manually on the model you have to manually call
        `full_clean()` before to have this method actuallly called.

        See:
        https://docs.djangoproject.com/en/3.0/ref/models/instances/#validating-objects
        """

        # TODO move the test that are impossible to perform from here to the form
        # See: https://stackoverflow.com/a/7986937
        IS_WITH_AT_LEAST_A_STAR = (
            self.planetary_system is None and self.main_star is not None
        )
        IS_LONELY_PLANET = self.planetary_system is not None and self.main_star is None

        if IS_WITH_AT_LEAST_A_STAR or IS_LONELY_PLANET:
            # We have a planet attached to something, this is ok!
            pass
        else:
            # https://docs.djangoproject.com/fr/3.0/ref/forms/validation/#raising-validationerror
            raise ValidationError(
                "No main star and no planetary system", code="incomplete"
            )

        name_alreay_used, used_by = planet_name_already_used_as_alternate_name(
            self.name
        )
        if name_alreay_used:
            raise ValidationError(
                f"Name: {self.name} already used as an alternate name by {used_by}."
            )

        super().clean()


class Planet2Publication(WebVisibleModel, TrackedModel):
    """Many-to-many relationship model used for the Planet.publications relationship."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    # Foreign keys as declared in Planet.publications
    publication: ForeignKey = ForeignKey(Publication, on_delete=CASCADE)
    planet: ForeignKey = ForeignKey(Planet, on_delete=CASCADE)

    # Tells if a publication is about a specific planet parameter
    parameter: SmallIntegerField = SmallIntegerField(
        null=False,
        blank=False,
        choices=Plan2PubParam.choices,
        default=Plan2PubParam.GENERIC_PUB,
    )


class Planet2Star(Model):
    """Many-to-many relationship model used for the Planet.stars relationship."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    # Foreign keys as declared in Planet.stars
    planet: ForeignKey = ForeignKey(Planet, on_delete=CASCADE)
    star: ForeignKey = ForeignKey(Star, on_delete=CASCADE)

    # TODO set the correct on_delete value here
    # system: ForeignKey = ForeignKey(PlanetarySystem, null=True, on_delete=SET_NULL)


class AlternatePlanetName(Model):
    """Simple placeholder for the planet names since planets can have multiple names."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """This is needed since we put our models in a module folder"""

        app_label = "core"

    name = AsciiCharField(max_length=60, unique=True)

    # WARNING previously the related name was singular: alternate_planet_name
    planet: ForeignKey = ForeignKey(
        Planet,
        related_name="alternate_planet_names",
        null=False,
        on_delete=CASCADE,
    )

    def clean(self) -> None:
        """
        Clean AlternatePlanetName model.

        - Auto delete if the foreign key is None.
        - Check if self.name is already used as a "main" planet name.
        """
        if self.planet is None:
            self.delete()

        name_alreay_used, used_by = name_already_used_as_planet_name(self.name)
        if name_alreay_used:
            raise ValidationError(
                f"Name: {self.name} already used as a planet name " f"by {used_by}."
            )

        super().clean()
