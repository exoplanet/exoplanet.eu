# type: ignore
# Standard imports
import operator
from functools import reduce

# Django 💩 imports
from django.db.models import Q

# External imports
from booleano.operations.converters import BaseConverter
from booleano.parser import Grammar
from booleano.parser.core import ConvertibleParseManager

# Local imports
from . import catalog_field_def
from .models import choices

# Reference units for all the fields or models
UNITS = dict(
    mass=dict(default="mjup", msun=0.0009546, mearth=317.83),
    axis=dict(default="au", km=149598000, ly=1.58128588e-5),
    period=dict(default="day", hour=24.0, year=1 / 365.242199),
    radius=dict(default="rjup", km=71492, rearth=11.20898),
    omega=dict(default="deg", rad=0.017453293),
)

# A list of PlanetDetection indexes corresponding to the detection asked by the POST
# parameters.
DetectionFilter = list[int]

# A list of PLANET_STATUSES values.
PlanetStatusFilter = list[int]

gr = {
    "not": "not",
    "eq": "=",
    "belongs_to": "in",
    "is_subset": "intersect",
    "and": "and",
    "or": "or",
}

# This is a dynamic variable computed by reading a YAML file
# pylint: disable-next=no-member
FILTER_FIELDS = catalog_field_def.OUTPUT_ALL_FIELDS


class Variable:
    def __init__(self, field, type=None, coeff=None, method=None):
        self.field = field
        self.type = type
        self.coeff = None
        self.method = None
        self.coeff = coeff
        self.method = method

    def get_value(self, value):
        if self.coeff:
            value = value / self.coeff
        if self.method:
            value = (self.method)(value)
        return value


def getVariable(name, namespace_parts):
    if name == "null":
        return None

    unit = (None,)
    input = None
    if namespace_parts != ():
        input = namespace_parts[0]
        unit = name
    else:
        input = name

    for field in FILTER_FIELDS:
        current_field = catalog_field_def.Column(field)
        variable_input = current_field.get_filter_name()
        if input == variable_input:
            # Standard imports
            import copy

            current_field = copy.deepcopy(current_field)
            field = current_field.get_id()
            kwargs = {}

            if namespace_parts != ():
                if current_field.unit is not None:
                    current_field.unit = unit
                else:
                    if unit != UNITS[field]["default"]:
                        coeff = UNITS[field][unit]
                        kwargs["coeff"] = coeff

            if current_field.get_reverse_model_property() is not None:
                kwargs["method"] = current_field.get_reverse_model_property()

            kwargs["type"] = current_field.get_type()

            if kwargs["type"] is None:
                kwargs["type"] = "number"
            return Variable(current_field.get_value_field(), **kwargs)
    raise NameError(name)


class Converter(BaseConverter):
    def convert_and(self, master_operand, slave_operand):
        """
        Convert AND connective whose left-hand operand is master_operand and
        right-hand operand is slave_operand.

        Args:
            master_operand(object): The left-hand operand, already converted.
            slave_operand(object): The right-hand operand, already converted.

        Returns:
            object
        """
        concat = [master_operand, slave_operand]
        q_list = [Q(x) for x in concat]
        return reduce(operator.and_, q_list)

    # Now it doesn't work correctly with array integer fields like detection_type, it
    # only tests if detection_type = value and not
    # if value is included in detection_type
    def convert_belongs_to(self, master_operand, slave_operand):
        """
        Convert “belongs to” operation where the set is master_operand and the
        element is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        predicates = []
        if isinstance(slave_operand, str):
            db_value = master_operand.get_value(slave_operand)
            # predicates.append((master_operand.field + '__iexact',db_value))
            predicates.append((f"{master_operand.field}__icontains", db_value))
        else:
            for value in slave_operand:
                if value is None:
                    raise ValueError(value)
                db_value = master_operand.get_value(value)
                if isinstance(db_value, str):
                    predicates.append(("{master_operand.field}__icontains", db_value))
                else:
                    # predicates.append((master_operand.field + '__in',db_value))
                    predicates.append(("{master_operand.field}__exact", db_value))
        q_list = [Q(x) for x in predicates]
        return reduce(operator.and_, q_list)

    def convert_equal(self, master_operand, slave_operand):
        """
        Convert equality operation whose left-hand operand is master_operand
        and right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        db_value = master_operand.get_value(slave_operand)
        if isinstance(db_value, str):
            return Q((f"{master_operand.field}__iexact", db_value))
        else:
            return Q((master_operand.field, db_value))

    def convert_greater_equal(self, master_operand, slave_operand):
        """
        Convert “greater than or equal to” operation whose left-hand operand is
        master_operand and right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        if slave_operand is None:
            raise ValueError(slave_operand)
        return Q(
            (f"{master_operand.field}__gte", master_operand.get_value(slave_operand))
        )

    def convert_greater_than(self, master_operand, slave_operand):
        """
        Convert “greater than” operation whose left-hand operand is
        master_operand and right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        if slave_operand is None:
            raise ValueError(slave_operand)
        return Q(
            (f"{master_operand.field}__gt", master_operand.get_value(slave_operand))
        )

    def convert_is_subset(self, master_operand, slave_operand):
        """
        Convert “is subset of” operation where the superset is master_operand
        and the subset is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        predicates = []
        for value in slave_operand:
            if value is None:
                raise ValueError(value)
            db_value = master_operand.get_value(value)
            if isinstance(db_value, str):
                predicates.append((f"{master_operand.field}__iexact", db_value))
            else:
                predicates.append((f"{master_operand.field}__exact", db_value))
        q_list = [Q(x) for x in predicates]
        return reduce(operator.or_, q_list)

    def convert_less_equal(self, master_operand, slave_operand):
        """
        Convert “less than or equal to” operation whose left-hand operand is
        master_operand and right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        if slave_operand is None:
            raise ValueError(slave_operand)
        return Q(
            (f"{master_operand.field}__lte", master_operand.get_value(slave_operand))
        )

    def convert_less_than(self, master_operand, slave_operand):
        """
        Convert “less than” operation whose left-hand operand is master_operand
        and right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        if slave_operand is None:
            raise ValueError(slave_operand)
        return Q(
            (f"{master_operand.field}__lt", master_operand.get_value(slave_operand))
        )

    def convert_not(self, operand):
        """
        Convert negation function whose argument is operand.

        Args:
            operand(object): The argument already converted.

        Returns:
            object
        """
        return ~Q(operand)

    def convert_not_equal(self, master_operand, slave_operand):
        """
        Convert “not equal to” operation whose left-hand operand is
        master_operand and right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        return ~Q((master_operand.field, master_operand.get_value(slave_operand)))

    def convert_number(self, number):
        """
        Convert the literal number number.

        Args:
            number(float): The literal number.

        Returns:
            object
        """
        return number

    def convert_or(self, master_operand, slave_operand):
        """
        Convert OR connective whose left-hand operand is master_operand and
        right-hand operand is slave_operand.

        Args:
            master_operand(object): The set already converted.
            slave_operand(object): The element already converted.

        Returns:
            object
        """
        concat = [master_operand, slave_operand]
        q_list = [Q(x) for x in concat]
        return reduce(operator.or_, q_list)

    def convert_set(self, *elements):
        """
        Convert the literal set with the elements.

        Args:
            elements(list): The elements.

        Returns:
            object
        """
        list = []
        for elem in elements:
            list.append(elem)
        return tuple(list)

    def convert_string(self, text):
        """
        Convert the literal string text.

        Args:
            text(str): The contents of the literal string.

        Returns:
            object
        """
        return text

    def convert_variable(self, name, namespace_parts):
        """
        Convert the variable called name.

        Args:
            name(str): The name of the variable.
            namespace_parts(list): The namespace of the variable (if any),
                represented by the identifiers that make it up.

        Returns:
            object
        """
        return getVariable(name, namespace_parts)


def createFilter(
    filter_str: str | None,
    statuses: PlanetStatusFilter | None = None,
    detections: DetectionFilter | None = None,
) -> tuple[Q, bool]:
    english_grammar = Grammar(**gr)
    parse_manager = ConvertibleParseManager(english_grammar)
    converter = Converter()
    syntax_error = False
    filters = []
    try:
        if filter_str:
            parse_tree = parse_manager.parse(str(filter_str.lower()))
            res = parse_tree(converter)
            if not isinstance(res, Q):
                raise Exception()
            filters.append(res)
        if statuses:
            predicates = []
            for status in statuses:
                predicates.append(
                    ("planet_status_string__exact", choices.title_name(status))
                )
            q_list = [Q(x) for x in predicates]
            res = reduce(operator.or_, q_list)
            filters.append(res)
        if detections:
            predicates = []
            predicates.append(("detection_type__contains", detections))
            q_list = [Q(x) for x in predicates]
            res = reduce(operator.or_, q_list)
            filters.append(res)
        q_list = [Q(x) for x in filters]
        the_conversion_result = reduce(operator.and_, q_list)

    # TODO improve this exception handling
    # probably it'd better to list specific exceptions expected here:
    # - BooleanoException
    # - ParseException
    except Exception:
        the_conversion_result = None
        syntax_error = True

    return the_conversion_result, syntax_error
