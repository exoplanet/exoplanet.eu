---
author: Pierre-Yves Martin <pierre-yves.martin@obspm.fr>
---

# How to modify the exoplanet.eu DB

If ever you need to modify any of the models of the django project, you will need to do
a migration. The good procedure to do that on this project is as follows...

## Some important notes before anything else

1. The migration files, located in the `migrations` subfolders of each Django
   application, are integral part of the project and should always be added to the git
   repository!
1. Any file needed by the migration files, especialy the `.sql` files, should be located
   alongside the migration files and should be added to the git repository.
1. If any migration file need to execute some SQL statements (to import data, to add
   table, to add or activate triggers...) it's better to have those SQL pieces in a
   dedicated `.sql` file (or files!) that are loaded in the python migration file. This
   allows to have syntax highlighting and syntax check on those files, preventing many
   nasty bugs.

## When everything goes as planned

### Check if there is a migration to do

We have many django app in the project so a nice little script will do that for us
without forgeting any installed app (we assume your apps are correctly configured in the
`common/settings.py` for this to work) :

```shell
$ ./makemigrations_check.sh
Migrations for 'core':
  core/migrations/0003_planetdbview.py
    - Create model PlanetDBView
```

This **does not alter your project structure** after using this you have to do the
actual makemigration and migration!

### Generate the migration files

The previous script gave you the names of the apps that need migration (in our example
only the `core` app needs migration). For all these apps we need to create a migration
file (repalce `core` by a space separated list of apps if you have many apps to
migrate) and give this migration a **meaningful name**:

```shell
$ python manage.py makemigrations core --name migration_name --configuration=Local
Migrations for 'core':
  core/migrations/0003_migration_name.py
    - Create model PlanetDBView
```

This time actual migration files should have been created where indicated.

Apply `black` to the generated migration file since it does not conforme to PEP8
standards:

```shell
$ black core/migrations/0003_migration_name.py
reformatted core/migrations/0003_migration_name.py
All done! ✨ 🍰 ✨
1 file reformatted.
```

### Customize the migration

I some very rare cases you need to modify the migration (for example if you added an
unmanaged model that maps to a database view) this is the moment to do so by editing the
migration file. In our example `core/migrations/0003_migration_name.py`.

### Make the migrations

If you edited/customized your migration file you will need to "fake initial" (see:
[Using Database Views in Django ORM](https://resources.rescale.com/using-database-views-in-django-orm/))
 :

```shell
$ python manage.py migrate --fake-initial --configuration=Local
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, core, editorial, flatpages, sessions, sites
Running migrations:
  Applying core.0006_migration_name... OK
```

But if you simply apply the untouched migration file you simply use the migrate command:

```shell
$ python manage.py migrate --configuration=Local
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, core, editorial, flatpages, sessions, sites
Running migrations:
  Applying core.0006_add_planetdbview... OK
```

### Commit the files to repository

Once everything is done, and if no error/problem occured, you have to commit the
migration files to your git repository! It MUST be kept with all the code!

```shell
git add core/migrations/0003_migration_name.py
# Add all other relevent files
git commit
```

## If anything goes sideways

### If you want to revert the migration

Once a migrate command is applied you can revert to the previous migration state with
the migrate command. You need to know what is the name of the previous migration for the
given application (all migration for a given application are in the `migrations`
subfolder of the application). For our example the previous migration for the `core`
application is named `0002_previous_migration_name` and is located
`core/migrations/0002_previous_migration_name.py`:

```shell
$ python manage.py migrate core 0002_previous_migration_name --configuration=Local
Operations to perform:
  Target specific migration: 0002_previous_migration_name, from core
Running migrations:
  Rendering model states... DONE
  Unapplying core.0003_migration_name... OK
```

And if ever, after unapplying it, we want to remove the faulty migration, we just need
to delete it's python migration file. In our exemple we would remove the
`core/migrations/0003_migration_name.py` file.
