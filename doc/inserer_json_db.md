---
author: Ulysse Chosson <ulysse.chosson@obspm.fr>
---

# Insérer les données pour la base de disque

## Insérer les données

Pour cela il faut executer éxecuter les 4 commandes suivante depuis la racine du projet.

```sh
psql -h <host> -U <utilisateur> -d <base> -f doc/sql/insert_exodict.sql
psql -h <host> -U <utilisateur> -d <base> -f doc/sql/insert_disks.sql
psql -h <host> -U <utilisateur> -d <base> -f doc/sql/insert_stars.sql
psql -h <host> -U <utilisateur> -d <base> -f doc/sql/insert_catalog_headers.sql
```

## Lié les etoiles et les disques

WIP

## Lié les etoiles relationelle et json

WIP

## Créer les systemes gravitaionel

WIP
