# Server update process

1. `commit` and `push` on your local branch `feature/switch-to-django3`.
2. Connect with ssh on the server: `voparis-exoplanet-new`
3. On your user account :
   - `sudo -u exoplanet bash`
   - `cd /opt/exoplanet/exoplanet.eu`
    To connect to the exoplanet user account.
4. The "exoplanet.eu" folder is a git directory linked to the `feature/switch-to-django3`
   branch, so all you have to do is: `git pull` and provide the username and password.
5. Launch the poetry shell: `poetry shell`
6. Install dependancies: `just install`
7. Collect static files: `python manage.py collectstatic --configuration=Prod`.
   /!\ Please note that you must validate in the CLI by typing "yes".
8. Make migrations: `python manage.py migrate --configuration=Prod`
9. Disconnect from the user: `Ctl-D`
10. Restart the apache server: `sudo /usr/sbin/apachectl restart`
