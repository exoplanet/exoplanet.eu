# Standard imports
from getpass import getpass

# External imports
import psycopg
from py_linq_sql import SQLEnumerable


def connect():
    return psycopg.connect(
        user=input("db user: "),
        password=getpass("db user password: "),
        host=input("host: "),
        port=int(input("port: ")),
        dbname=input("db name: "),
    )


conn = connect()

sqle_disk = SQLEnumerable(conn, "disk_api.disk")
sqle_join = (
    SQLEnumerable(conn, "disk_api.star")
    .join(
        sqle_disk,
        lambda star: star.data.identify.name,
        lambda disk: disk.data.identify.name,
        lambda disk, star: {
            "disk_id": disk.id,
            "disk_name": disk.data.identify.name,
            "star_id": star.id,
            "star_name": star.data.identify.name,
        },
    )
    .execute()
)

data = []

for row in sqle_join:
    data.append(
        {
            "principal_obj": {"id": row.star_id, "type": "star"},
            "secondary_objs": [{"id": row.disk_id, "type": "disk"}],
        }
    )

sqle_insert = (
    SQLEnumerable(conn, "disk_api.gravitationalsystems").insert("data", data).execute()
)

conn.commit()
conn.close()

print("OK 👍")
