# Standard imports
from getpass import getpass

# External imports
import psycopg
from py_linq_sql import SQLEnumerable


def connect():
    return psycopg.connect(
        user=input("db user: "),
        password=getpass("db user password: "),
        host=input("host: "),
        port=int(input("port: ")),
        dbname=input("db name: "),
    )


conn = connect()
sqle_star_json = (
    SQLEnumerable(conn, "disk_api.star")
    .select(lambda x: {"jid": x.id, "name": x.data.identify.name})
    .execute()
)

sqle_star_rel = (
    SQLEnumerable(conn, "core_star")
    .select(lambda x: {"rid": x.id, "name": x.name})
    .execute()
)

res = []

for star_rel in sqle_star_rel.to_list():
    for star_json in sqle_star_json.to_list():
        if star_rel.name == star_json.name:
            res.append(
                {
                    "name": star_json.name,
                    "id_json": star_json.jid,
                    "id_rel": star_rel.rid,
                }
            )

for link in res:
    sqle_update = (
        SQLEnumerable(conn, "disk_api.star")
        .where(
            lambda x: (
                (x.data.identify.name == link["name"]) & (x.id == link["id_json"])
            )
        )
        .update(lambda x: x.data.internal.rel_star_id == link["id_rel"])
        .execute()
    )

conn.commit()
conn.close()

print("OK 👍")
