INSERT INTO disk_api.star(data)
VALUES
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "Fomalhaut", "discovery_date": 2005 },
    "parameters": {
      "position": { "distance": 7.2, "ra": 22, "dec": -29 },
      "magnitude": { "magnitude_R": 1.2, "magnitude_v": 1.16 },
      "physical": { "spectral_type": "A4V", "teff": 8760, "age": 220 },
      "observational": { "flux": 1274, "luminosity": 17.8 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HR 4796A",
      "moving_group": "TW Hydra",
      "discovery_date": 1999
    },
    "parameters": {
      "position": { "distance": 72.8, "ra": 12, "dec": -39 },
      "magnitude": { "magnitude_R": 5.8, "magnitude_v": 5.78 },
      "physical": {
        "mass": 2.4,
        "spectral_type": "A0",
        "teff": 9350,
        "age": 10
      },
      "observational": { "flux": 5.2, "luminosity": 22.8 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 15115", "discovery_date": 2007 },
    "parameters": {
      "position": { "distance": 45, "ra": 2, "dec": 6 },
      "magnitude": { "magnitude_R": 6.8, "magnitude_v": 7.15 },
      "physical": { "spectral_type": "F2", "teff": 7000, "age": 21 },
      "observational": { "flux": 4.7, "luminosity": 3.3 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 32297", "discovery_date": 2005 },
    "parameters": {
      "position": { "distance": 113, "ra": 5, "dec": 7 },
      "magnitude": { "magnitude_R": 12.6, "magnitude_v": 8.14 },
      "physical": { "spectral_type": "A0", "teff": 9600, "age": 30 },
      "observational": { "flux": 0.615, "luminosity": 24 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 15745", "discovery_date": 2007 },
    "parameters": {
      "position": { "distance": 64, "ra": 2, "dec": 37 },
      "magnitude": { "magnitude_R": 7.5, "magnitude_v": 7.49 },
      "physical": { "spectral_type": "F2", "teff": 6800, "age": 20 },
      "observational": { "flux": 3.74, "luminosity": 3.3 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 53143", "discovery_date": 2006 },
    "parameters": {
      "position": { "distance": 18.4, "ra": 6, "dec": -61 },
      "magnitude": { "magnitude_R": 6.9, "magnitude_v": 6.82 },
      "physical": { "spectral_type": "G9V", "teff": 5335, "age": 1000 },
      "observational": { "luminosity": 0.4 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 61005",
      "moving_group": "Argus association",
      "discovery_date": 2007
    },
    "parameters": {
      "position": { "distance": 35.4, "ra": 7, "dec": -32 },
      "magnitude": { "magnitude_R": 8.3, "magnitude_v": 8.9 },
      "physical": { "spectral_type": "G8", "teff": 5456, "age": 40 },
      "observational": { "flux": 1.65, "luminosity": 0.56 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 92945", "discovery_date": 2011 },
    "parameters": {
      "position": { "distance": 21.6, "ra": 10, "dec": -29 },
      "magnitude": { "magnitude_R": 7.2, "magnitude_v": 8.59 },
      "physical": { "spectral_type": "K1", "teff": 5125, "age": 200 },
      "observational": { "flux": 3.6, "luminosity": 0.32 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 107146", "discovery_date": 2004 },
    "parameters": {
      "position": { "distance": 28.5, "ra": 12, "dec": 16 },
      "magnitude": { "magnitude_R": 11.3 },
      "physical": { "spectral_type": "G2", "teff": 5859, "age": 200 },
      "observational": { "flux": 6.9, "luminosity": 1.1 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 139664", "discovery_date": 2006 },
    "parameters": {
      "position": { "distance": 17.5, "ra": 15, "dec": -44 },
      "magnitude": { "magnitude_R": 4.7, "magnitude_v": 4.6 },
      "physical": { "spectral_type": "F5 IV-V", "teff": 6700, "age": 300 },
      "observational": { "flux": 51.6, "luminosity": 2.7 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 30447",
      "moving_group": "Columba",
      "discovery_date": 2014
    },
    "parameters": {
      "position": { "distance": 80, "ra": 4, "dec": -26 },
      "magnitude": { "magnitude_v": 7.86 },
      "physical": { "spectral_type": "F3V", "teff": 6800, "age": 25 },
      "observational": { "flux": 2.3, "luminosity": 3.7 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 35841",
      "moving_group": "Columba",
      "discovery_date": 2014
    },
    "parameters": {
      "position": { "distance": 96, "ra": 5, "dec": -22 },
      "magnitude": { "magnitude_v": 8.9 },
      "physical": { "spectral_type": "F3V", "teff": 6500, "age": 25 },
      "observational": { "flux": 0.971, "luminosity": 2 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 141943 / NZ Lup",
      "moving_group": "UCL",
      "discovery_date": 2014
    },
    "parameters": {
      "position": { "distance": 60.24, "ra": 15, "dec": -42 },
      "magnitude": { "magnitude_v": 7.85 },
      "physical": {
        "mass": 1.244,
        "spectral_type": "G2V",
        "teff": 6000,
        "age": 16
      },
      "observational": { "flux": 3.215, "luminosity": 2.9 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 191089",
      "moving_group": "BPMG",
      "discovery_date": 2014
    },
    "parameters": {
      "position": { "distance": 52, "ra": 20, "dec": -26 },
      "magnitude": { "magnitude_v": 7.18 },
      "physical": { "spectral_type": "F5V", "teff": 6440, "age": 14 },
      "observational": { "flux": 4.721, "luminosity": 3.2 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 202917",
      "moving_group": "Tucana Horlogium",
      "discovery_date": 2014
    },
    "parameters": {
      "position": { "distance": 43, "ra": 21, "dec": -53 },
      "magnitude": { "magnitude_v": 8.67 },
      "physical": { "spectral_type": "G7V", "teff": 5553, "age": 25 },
      "observational": { "flux": 1.77, "luminosity": 0.7 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 181327", "discovery_date": 2006 },
    "parameters": {
      "position": { "distance": 50.6, "ra": 19, "dec": -54 },
      "magnitude": { "magnitude_R": 7.1, "magnitude_v": 7.04 },
      "physical": { "spectral_type": "F5", "teff": 6650, "age": 21 },
      "observational": { "flux": 5.9, "luminosity": 2 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD202628", "discovery_date": 2012 },
    "parameters": {
      "position": { "distance": 24.4, "ra": 21, "dec": -43 },
      "magnitude": { "magnitude_v": 6.75 },
      "physical": { "spectral_type": "G5V", "teff": 5680, "age": 2000 },
      "observational": { "flux": 7.4, "luminosity": 1 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HIP 79977",
      "moving_group": "Upper Sco",
      "discovery_date": 2013
    },
    "parameters": {
      "position": { "distance": 123, "ra": 16, "dec": -21 },
      "magnitude": { "magnitude_R": 8.7, "magnitude_v": 9.11 },
      "physical": { "spectral_type": "F2", "teff": 6750, "age": 7.5 },
      "observational": { "flux": 0.749, "luminosity": 4.2 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 207129", "discovery_date": 2010 },
    "parameters": {
      "position": { "distance": 16, "ra": 21, "dec": -47 },
      "magnitude": { "magnitude_R": 5.4, "magnitude_v": 5.58 },
      "physical": { "spectral_type": "G2V", "teff": 5912, "age": 2100 },
      "observational": { "flux": 21.74, "luminosity": 1.26 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD 10647 / q01 Eri", "discovery_date": 2011 },
    "parameters": {
      "position": { "distance": 17.35, "ra": 1, "dec": -53 },
      "magnitude": { "magnitude_R": 5.5, "magnitude_v": 5.52 },
      "physical": { "spectral_type": "F8 V", "teff": 6155, "age": 1000 },
      "observational": { "luminosity": 1.5 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "beta Pic",
      "moving_group": "BPMG",
      "discovery_date": 1984
    },
    "parameters": {
      "position": { "distance": 19.3, "ra": 5, "dec": -51 },
      "magnitude": { "magnitude_R": 3.9, "magnitude_v": 3.86 },
      "physical": { "spectral_type": "A5", "teff": 8200, "age": 23 },
      "observational": { "flux": 106, "luminosity": 8.79 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "TWA25",
      "moving_group": "TW Hydra",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 54, "ra": 12, "dec": -39 },
      "magnitude": { "magnitude_R": 10.5, "magnitude_v": 11.4 },
      "physical": { "spectral_type": "M0", "teff": 4020, "age": 10 },
      "observational": { "flux": 1.035, "luminosity": 0.25 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "TWA7",
      "moving_group": "TW Hydra",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 34.5, "ra": 10, "dec": -33 },
      "magnitude": { "magnitude_R": 9.9 },
      "physical": { "spectral_type": "M3.2", "teff": 3408, "age": 6.4 },
      "observational": { "flux": 1.467, "luminosity": 0.07 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD35650",
      "moving_group": "AB Doradus",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 18, "ra": 5, "dec": -38 },
      "magnitude": { "magnitude_R": 8.3 },
      "physical": { "spectral_type": "K6", "teff": 4000, "age": 100 },
      "observational": { "flux": 3.753, "luminosity": 0.13 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD377",
      "moving_group": "Field",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 39.1, "ra": 0, "dec": 6 },
      "magnitude": { "magnitude_R": 7, "magnitude_v": 7.6 },
      "physical": {
        "mass": 1.11,
        "spectral_type": "G2",
        "teff": 5890,
        "age": 200
      },
      "observational": { "flux": 4.3, "luminosity": 1.17 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD106906",
      "moving_group": "LCC",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 92.1, "ra": 12, "dec": -55 },
      "magnitude": { "magnitude_R": 7.5, "magnitude_v": 8.9 },
      "physical": {
        "mass": 1.5,
        "spectral_type": "F5V",
        "teff": 6906,
        "age": 13
      },
      "observational": { "luminosity": 5.6 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD115600",
      "moving_group": "LCC",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 110.5, "ra": 13, "dec": -59 },
      "magnitude": { "magnitude_R": 8, "magnitude_v": 8.2 },
      "physical": { "spectral_type": "F2V/F3V", "teff": 6867, "age": 15 },
      "observational": { "flux": 1.155, "luminosity": 5 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD131835",
      "moving_group": "UCL",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 122.7, "ra": 14, "dec": -35 },
      "magnitude": { "magnitude_v": 7.9 },
      "physical": { "spectral_type": "A2V", "teff": 9000, "age": 16 },
      "observational": { "luminosity": 9.2 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD110058",
      "moving_group": "LCC",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 107.41, "ra": 12, "dec": -49 },
      "magnitude": { "magnitude_R": 7.9, "magnitude_v": 7.97 },
      "physical": { "spectral_type": "A0V", "teff": 9000, "age": 17 },
      "observational": { "luminosity": 7.28 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD114082",
      "moving_group": "LCC",
      "discovery_date": 2016
    },
    "parameters": {
      "position": { "distance": 95.1, "ra": 13, "dec": -60 },
      "magnitude": { "magnitude_R": 8.2, "magnitude_v": 8.2 },
      "physical": { "spectral_type": "F3V", "teff": 6700, "age": 17 },
      "observational": { "flux": 1.337, "luminosity": 3.6 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "49 Ceti",
      "moving_group": "Argus association",
      "discovery_date": 2016
    },
    "parameters": {
      "position": { "distance": 59, "ra": 1, "dec": -15 },
      "magnitude": { "magnitude_R": 5.6, "magnitude_v": 5.6 },
      "physical": { "spectral_type": "A1V", "teff": 9970, "age": 40 },
      "observational": { "flux": 10.18, "luminosity": 20 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD120326",
      "moving_group": "UCL",
      "discovery_date": 2016
    },
    "parameters": {
      "position": { "distance": 107.4, "ra": 13, "dec": -50 },
      "magnitude": { "magnitude_R": 8.2, "magnitude_v": 8.39 },
      "physical": {
        "mass": 1.6,
        "spectral_type": "F0V",
        "teff": 7200,
        "age": 16
      },
      "observational": { "luminosity": 4.45 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD111520",
      "moving_group": "LCC",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 108, "ra": 12, "dec": -49 },
      "magnitude": { "magnitude_v": 8.87 },
      "physical": { "spectral_type": "F5V", "teff": 6500, "age": 17 },
      "observational": { "flux": 0.7663, "luminosity": 2.9 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD36546",
      "moving_group": "Taurus-Auriga",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 114, "ra": 5, "dec": 24 },
      "magnitude": { "magnitude_v": 6.95 },
      "physical": { "spectral_type": "B8", "teff": 10000, "age": 6.5 },
      "observational": { "flux": 1.768, "luminosity": 20.04 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD157587",
      "moving_group": "Field",
      "discovery_date": 2015
    },
    "parameters": {
      "position": { "distance": 98.13542689, "ra": 17, "dec": -18 },
      "magnitude": { "magnitude_R": 8, "magnitude_v": 8.5 },
      "physical": { "spectral_type": "F5V", "teff": 6457, "age": 2500 },
      "observational": { "luminosity": 3.86 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "Au Mic",
      "moving_group": "BPMG",
      "discovery_date": 2004
    },
    "parameters": {
      "position": { "distance": 9.9, "ra": 20, "dec": -31 },
      "magnitude": { "magnitude_v": 8.8 },
      "physical": {
        "mass": 0.6,
        "spectral_type": "M1",
        "teff": 3720,
        "age": 23
      },
      "observational": { "flux": 1.313, "luminosity": 0.11 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD129590",
      "moving_group": "UCL",
      "discovery_date": 2017
    },
    "parameters": {
      "position": { "distance": 141, "ra": 14, "dec": -39 },
      "magnitude": { "magnitude_v": 9.3 },
      "physical": { "mass": 1.3, "spectral_type": "G1V", "teff": 5945 },
      "observational": { "luminosity": 2.8 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "GSC 07396-00759",
      "moving_group": "BPMG",
      "discovery_date": 2018
    },
    "parameters": {
      "position": { "distance": 71.4, "ra": 18, "dec": -32 },
      "magnitude": { "magnitude_R": 12.01, "magnitude_v": 12.78 },
      "physical": { "spectral_type": "M1V", "teff": 3630, "age": 24 },
      "observational": { "luminosity": 0.14 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD192758",
      "moving_group": "Field/IC2391",
      "discovery_date": 2018
    },
    "parameters": {
      "position": { "distance": 67, "ra": 20, "dec": -42 },
      "magnitude": { "magnitude_R": 7, "magnitude_v": 7 },
      "physical": { "spectral_type": "F0V", "teff": 7200, "age": 400 },
      "observational": { "flux": 3.26 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD 104860",
      "moving_group": "Field",
      "discovery_date": 2018
    },
    "parameters": {
      "position": { "distance": 45, "ra": 12, "dec": 66 },
      "magnitude": { "magnitude_R": 7.8, "magnitude_v": 7.9 },
      "physical": {
        "mass": 1.04,
        "spectral_type": "F8 V",
        "teff": 5938.67,
        "age": 300
      },
      "observational": { "flux": 2.97, "luminosity": 1.16 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD105",
      "moving_group": "Tuc-Hor",
      "discovery_date": 2018
    },
    "parameters": {
      "position": { "distance": 38.85, "ra": 0, "dec": -41 },
      "magnitude": { "magnitude_v": 7.53 },
      "physical": { "spectral_type": "G0", "teff": 5940, "age": 28 },
      "observational": { "flux": 3.47, "luminosity": 1.219 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD160305",
      "moving_group": "BPMG",
      "discovery_date": 2019
    },
    "parameters": {
      "position": { "distance": 65.51, "ra": 17, "dec": -50 },
      "magnitude": { "magnitude_R": 8, "magnitude_v": 8.4 },
      "physical": { "spectral_type": "F9V", "teff": 5972.25, "age": 23 },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD156623",
      "moving_group": "UCL",
      "discovery_date": 2019
    },
    "parameters": {
      "position": { "distance": 111.41, "ra": 17, "dec": -45 },
      "magnitude": { "magnitude_v": 7.254 },
      "physical": { "spectral_type": "A1V", "teff": 9040, "age": 16 },
      "observational": { "luminosity": 13.71 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD111161",
      "moving_group": "LCC",
      "discovery_date": 2020
    },
    "parameters": {
      "position": { "distance": 109, "ra": 12, "dec": -67 },
      "magnitude": { "magnitude_R": 7.6, "magnitude_v": 7.6 },
      "physical": {
        "mass": 2.4,
        "spectral_type": "A3 III/IV",
        "teff": 8073,
        "age": 13
      },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD98363",
      "moving_group": "LCC",
      "discovery_date": 2020
    },
    "parameters": {
      "position": { "distance": 138.6, "ra": 11, "dec": -64 },
      "magnitude": { "magnitude_R": 7.83, "magnitude_v": 7.85 },
      "physical": {
        "mass": 1.92,
        "spectral_type": "A2V",
        "teff": 8830,
        "age": 13
      },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD143675",
      "moving_group": "UCL",
      "discovery_date": 2020
    },
    "parameters": {
      "position": { "distance": 113.4, "ra": 16, "dec": -35 },
      "magnitude": { "magnitude_R": 8, "magnitude_v": 8.04 },
      "physical": {
        "mass": 2,
        "spectral_type": "A5 IV/V",
        "teff": 8200,
        "age": 13
      },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD145560",
      "moving_group": "UCL",
      "discovery_date": 2020
    },
    "parameters": {
      "position": { "distance": 120.4, "ra": 16, "dec": -45 },
      "magnitude": { "magnitude_R": 8.7, "magnitude_v": 8.9 },
      "physical": {
        "mass": 1.4,
        "spectral_type": "F5V",
        "teff": 6500,
        "age": 13
      },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD117214", "discovery_date": 2019 },
    "parameters": {
      "position": { "distance": 107.6, "ra": 13, "dec": -58 },
      "magnitude": { "magnitude_R": 8, "magnitude_v": 8 },
      "physical": { "spectral_type": "F6V", "teff": 7508, "age": 17 },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD172555",
      "moving_group": "BPMG",
      "discovery_date": 2018
    },
    "parameters": {
      "position": { "distance": 28.33, "ra": 18, "dec": -64 },
      "magnitude": { "magnitude_R": 4.8, "magnitude_v": 4.8 },
      "physical": { "spectral_type": "A7V", "teff": 7816, "age": 23 },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD141011",
      "moving_group": "UCL",
      "discovery_date": 2021
    },
    "parameters": {
      "position": { "distance": 128.37, "ra": 15, "dec": -42 },
      "magnitude": { "magnitude_v": 8.97 },
      "physical": {
        "mass": 1.4,
        "spectral_type": "F5V",
        "teff": 6500,
        "age": 16
      },
      "observational": { "luminosity": 3.51 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "BD+45\u25e6 598", "discovery_date": 2021 },
    "parameters": {
      "position": { "distance": 73.18, "ra": 2, "dec": 47 },
      "magnitude": { "magnitude_R": 8.7, "magnitude_v": 9.06 },
      "physical": { "spectral_type": "K1", "teff": 5280, "age": 23 },
      "observational": { "luminosity": 1.4 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD121617",
      "moving_group": "UCL",
      "discovery_date": 2023
    },
    "parameters": {
      "position": { "distance": 128.2, "ra": 13, "dec": -47 },
      "magnitude": { "magnitude_R": 7.29, "magnitude_v": 7.29 },
      "physical": {
        "mass": 2.13,
        "spectral_type": "A1V",
        "teff": 8900,
        "age": 16
      },
      "observational": { "luminosity": 14 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD131488",
      "moving_group": "UCL",
      "discovery_date": 2023
    },
    "parameters": {
      "position": { "distance": 154, "ra": 14, "dec": -41 },
      "magnitude": { "magnitude_R": 8, "magnitude_v": 8 },
      "physical": { "spectral_type": "A1V", "age": 16 },
      "observational": { "flux": 0.754, "luminosity": 13.4 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": {
      "name": "HD112810",
      "moving_group": "LCC",
      "discovery_date": 2023
    },
    "parameters": {
      "position": { "distance": 133.6, "ra": 13, "dec": -50 },
      "magnitude": { "magnitude_R": 9.1, "magnitude_v": 9.14 },
      "physical": { "spectral_type": "F3V", "age": 17 },
      "observational": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "rel_star_id": null },
    "identify": { "name": "HD16743", "discovery_date": 2023 },
    "parameters": {
      "position": { "distance": 57.9, "ra": 2, "dec": -52 },
      "magnitude": { "magnitude_R": 6.7, "magnitude_v": 6.77 },
      "physical": {
        "mass": 1.53,
        "spectral_type": "F0V",
        "teff": 6900,
        "age": 18
      },
      "observational": { "flux": 4.3, "luminosity": 5.1 }
    }
  }');
