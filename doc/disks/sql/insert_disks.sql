INSERT INTO disk_api.disk(data)
VALUES
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "Fomalhaut",
      "alternate_names": ["HD-216956", "HD-216956"]
    },
    "parameters": {
      "orbital": { "inclination": 65.6 },
      "geometric": { "mean_radius_r0": 20.5, "delta_r": 1.88 },
      "observational": {
        "surface_brightness_peak": 2.13e-5,
        "fractional_luminosity": 7.4e-5
      },
      "atmospheric": { "albedo": 0.05 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HR 4796A",
      "alternate_names": ["HD-109573", "HIP-61498"]
    },
    "parameters": {
      "orbital": { "inclination": 73 },
      "geometric": {
        "mean_radius_r0": 1.05,
        "inner_edge_r1": 1.0,
        "outer_edge_r2": 1.1,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 0.0156,
        "polarized_fraction": 50,
        "fractional_luminosity": 0.005
      },
      "atmospheric": { "albedo": 0.28 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 15115",
      "alternate_names": ["HD-15115", "HIP-11360"]
    },
    "parameters": {
      "orbital": { "inclination": 86 },
      "geometric": { "mean_radius_r0": 2.0, "delta_r": 0.05 },
      "observational": {
        "surface_brightness_peak": 0.00236,
        "fractional_luminosity": 0.0005
      },
      "atmospheric": { "albedo": 0.39 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 32297",
      "alternate_names": ["HD-32297", "HIP-23451"]
    },
    "parameters": {
      "orbital": { "inclination": 79 },
      "geometric": {
        "mean_radius_r0": 0.97,
        "inner_edge_r1": 0.71,
        "outer_edge_r2": 1.24,
        "delta_r": 0.27
      },
      "observational": {
        "surface_brightness_peak": 0.0016,
        "fractional_luminosity": 0.0027
      },
      "atmospheric": { "albedo": 0.61 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 15745",
      "alternate_names": ["HD-15745", "HIP-11847"]
    },
    "parameters": {
      "orbital": { "inclination": 67 },
      "geometric": { "mean_radius_r0": 3.75, "inner_edge_r1": 0.94 },
      "observational": {
        "surface_brightness_peak": 2.8e-5,
        "fractional_luminosity": 0.0012
      },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 53143",
      "alternate_names": ["HD-53143", "HIP-33690"]
    },
    "parameters": {
      "orbital": { "inclination": 45 },
      "geometric": { "mean_radius_r0": 6.0 },
      "observational": { "fractional_luminosity": 0.00025 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 61005",
      "alternate_names": ["HD-61005", "HIP-36948"]
    },
    "parameters": {
      "orbital": { "inclination": 84.1 },
      "geometric": { "mean_radius_r0": 1.71, "delta_r": 0.1 },
      "observational": {
        "surface_brightness_peak": 0.00316,
        "fractional_luminosity": 0.0025
      },
      "atmospheric": { "albedo": 0.45 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 92945",
      "alternate_names": ["HD-92945", "HIP-52462"]
    },
    "parameters": {
      "orbital": { "inclination": 63 },
      "geometric": { "mean_radius_r0": 2.59, "delta_r": 1.0 },
      "observational": {
        "surface_brightness_peak": 1.4e-5,
        "fractional_luminosity": 0.000759
      },
      "atmospheric": { "albedo": 0.08 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 107146",
      "alternate_names": ["HD-107146", "HIP-60074"]
    },
    "parameters": {
      "orbital": { "inclination": 20.6 },
      "geometric": { "mean_radius_r0": 3.86, "delta_r": 1.75 },
      "observational": {
        "surface_brightness_peak": 1.12e-5,
        "fractional_luminosity": 0.0012
      },
      "atmospheric": { "albedo": 0.05 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 139664",
      "alternate_names": ["HD-139664", "HIP-76829"]
    },
    "parameters": {
      "orbital": { "inclination": 87 },
      "geometric": {
        "mean_radius_r0": 5.43,
        "inner_edge_r1": 4.74,
        "outer_edge_r2": 6.23,
        "delta_r": 0.57
      },
      "observational": {
        "surface_brightness_peak": 2.34e-5,
        "fractional_luminosity": 9e-5
      },
      "atmospheric": { "albedo": 0.09 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 30447",
      "alternate_names": ["HD-30447", "HIP-22226"]
    },
    "parameters": {
      "orbital": { "inclination": 90 },
      "geometric": {
        "mean_radius_r0": 1.25,
        "inner_edge_r1": 0.38,
        "outer_edge_r2": 1.25,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 0.0006,
        "fractional_luminosity": 0.00079
      },
      "atmospheric": { "albedo": 0.21 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD 35841", "alternate_names": ["HD-35841"] },
    "parameters": {
      "orbital": { "inclination": 90 },
      "geometric": {
        "mean_radius_r0": 1.0,
        "outer_edge_r2": 0.75,
        "delta_r": 0.2
      },
      "observational": {
        "surface_brightness_peak": 0.00015,
        "polarized_fraction": "25-30",
        "fractional_luminosity": 0.0013
      },
      "atmospheric": { "albedo": 0.13 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 141943 / NZ Lup",
      "alternate_names": ["HD-141943"]
    },
    "parameters": {
      "orbital": { "inclination": 90 },
      "geometric": {
        "mean_radius_r0": 1.0,
        "inner_edge_r1": 0.38,
        "outer_edge_r2": 1.25,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 0.00025,
        "fractional_luminosity": 0.00012
      },
      "atmospheric": { "albedo": 0.29 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 191089",
      "alternate_names": ["HD-191089", "HIP-99273"]
    },
    "parameters": {
      "orbital": { "inclination": 30 },
      "geometric": {
        "mean_radius_r0": 0.9,
        "outer_edge_r2": 0.7,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 0.0013,
        "polarized_fraction": "35-60",
        "fractional_luminosity": 0.0013
      },
      "atmospheric": { "albedo": 0.11 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 202917",
      "alternate_names": ["HD-202917", "HIP-105388"]
    },
    "parameters": {
      "orbital": { "inclination": 70 },
      "geometric": {
        "mean_radius_r0": 1.3,
        "outer_edge_r2": 1.25,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 0.0002,
        "fractional_luminosity": 0.00025
      },
      "atmospheric": { "albedo": 0.27 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 181327",
      "alternate_names": ["HD-181327", "HIP-95270"]
    },
    "parameters": {
      "orbital": { "inclination": 32 },
      "geometric": { "mean_radius_r0": 1.7, "delta_r": 0.5 },
      "observational": {
        "surface_brightness_peak": 0.001,
        "polarized_fraction": "25-35",
        "fractional_luminosity": 0.00293
      },
      "atmospheric": { "albedo": 0.24 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD202628",
      "alternate_names": ["HD-202628", "HIP-105184"]
    },
    "parameters": {
      "orbital": { "inclination": 64 },
      "geometric": { "mean_radius_r0": 6.48, "delta_r": 2.05 },
      "observational": {
        "surface_brightness_peak": 9.3e-7,
        "fractional_luminosity": 0.00014
      },
      "atmospheric": { "albedo": 0.07 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HIP 79977",
      "alternate_names": ["HD-146897", "HIP-79977"]
    },
    "parameters": {
      "orbital": { "inclination": 84 },
      "geometric": { "mean_radius_r0": 0.59, "delta_r": 0.3 },
      "observational": {
        "surface_brightness_peak": 0.0026,
        "fractional_luminosity": 0.00521
      },
      "atmospheric": { "albedo": 0.43 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 207129",
      "alternate_names": ["HD-207129", "HIP-107649"]
    },
    "parameters": {
      "orbital": { "inclination": 60 },
      "geometric": {
        "mean_radius_r0": 10.2,
        "inner_edge_r1": 11.15,
        "outer_edge_r2": 9.25,
        "delta_r": 1.9
      },
      "observational": {
        "surface_brightness_peak": 1.23e-6,
        "fractional_luminosity": 0.00014
      },
      "atmospheric": { "albedo": 0.05 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 10647 / q01 Eri",
      "alternate_names": ["HD-10647", "HIP-7978"]
    },
    "parameters": {
      "orbital": { "inclination": 52 },
      "geometric": { "mean_radius_r0": 6.74 },
      "observational": { "fractional_luminosity": 0.0003 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "beta Pic",
      "alternate_names": ["HD-39060", "HIP-27321"]
    },
    "parameters": {
      "orbital": { "inclination": 85.3 },
      "geometric": {
        "mean_radius_r0": 5.7,
        "inner_edge_r1": 1.04,
        "outer_edge_r2": 51.81,
        "delta_r": 3.11
      },
      "observational": {
        "surface_brightness_peak": 0.00316,
        "fractional_luminosity": 0.0016
      },
      "atmospheric": { "albedo": 0.67 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "TWA25" },
    "parameters": {
      "orbital": { "inclination": 75 },
      "geometric": { "mean_radius_r0": 1.44, "delta_r": 0.2 },
      "observational": { "surface_brightness_peak": 2.8e-5 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "TWA7" },
    "parameters": {
      "orbital": { "inclination": 28 },
      "geometric": { "mean_radius_r0": 1.16, "delta_r": 0.2 },
      "observational": {
        "surface_brightness_peak": 0.0002,
        "fractional_luminosity": 0.0022
      },
      "atmospheric": { "albedo": 0.08 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD35650", "alternate_names": ["HD-35650"] },
    "parameters": {
      "orbital": { "inclination": 89 },
      "geometric": { "mean_radius_r0": 3.0, "delta_r": 0.2 },
      "observational": {
        "surface_brightness_peak": 8.2e-5,
        "fractional_luminosity": 0.000175
      },
      "atmospheric": { "albedo": 0.32 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD377", "alternate_names": ["HD-377"] },
    "parameters": {
      "orbital": { "inclination": 85 },
      "geometric": { "mean_radius_r0": 2.2, "delta_r": 0.2 },
      "observational": {
        "surface_brightness_peak": 4.2e-5,
        "fractional_luminosity": 0.0002
      },
      "atmospheric": { "albedo": 0.12 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD106906",
      "alternate_names": ["HD-106906", "HIP-59960"]
    },
    "parameters": {
      "orbital": { "inclination": 85 },
      "geometric": { "mean_radius_r0": 0.9 },
      "observational": { "fractional_luminosity": 0.0014 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD115600",
      "alternate_names": ["HD-115600", "HIP-64995"]
    },
    "parameters": {
      "orbital": { "inclination": 79.5 },
      "geometric": { "mean_radius_r0": 0.43, "delta_r": 0.1 },
      "observational": {
        "surface_brightness_peak": 0.017,
        "fractional_luminosity": 0.0017
      },
      "atmospheric": { "albedo": 0.7 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD131835",
      "alternate_names": ["HD-131835", "HIP-73145"]
    },
    "parameters": {
      "orbital": { "inclination": 72.6 },
      "geometric": { "mean_radius_r0": 0.73 },
      "observational": { "fractional_luminosity": 0.0014 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD110058",
      "alternate_names": ["HD-110058", "HIP-61782"]
    },
    "parameters": {
      "orbital": { "inclination": 90 },
      "geometric": { "mean_radius_r0": 1.02 },
      "observational": { "fractional_luminosity": 0.0015 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD114082",
      "alternate_names": ["HD-114082", "HIP-64184"]
    },
    "parameters": {
      "orbital": { "inclination": 83 },
      "geometric": { "mean_radius_r0": 0.32, "delta_r": 0.05 },
      "observational": {
        "surface_brightness_peak": 0.15,
        "fractional_luminosity": 0.003
      },
      "atmospheric": { "albedo": 0.79 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "49 Ceti", "alternate_names": ["HD-9672"] },
    "parameters": {
      "orbital": { "inclination": 73 },
      "geometric": { "mean_radius_r0": 2.37, "delta_r": 1.0 },
      "observational": {
        "surface_brightness_peak": 0.000129,
        "fractional_luminosity": 0.0009
      },
      "atmospheric": { "albedo": 0.17 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD120326",
      "alternate_names": ["HD-120326", "HIP-67497"]
    },
    "parameters": {
      "orbital": { "inclination": 79.9 },
      "geometric": { "mean_radius_r0": 0.28 },
      "observational": { "fractional_luminosity": 0.0015 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD111520",
      "alternate_names": ["HD-111520", "HIP-62657"]
    },
    "parameters": {
      "orbital": { "inclination": 90 },
      "geometric": { "mean_radius_r0": 0.93, "delta_r": 0.05 },
      "observational": {
        "surface_brightness_peak": 0.027,
        "fractional_luminosity": 0.001
      },
      "atmospheric": { "albedo": 0.91 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD36546", "alternate_names": ["HD-36546"] },
    "parameters": {
      "orbital": { "inclination": 75 },
      "geometric": { "mean_radius_r0": 0.75, "delta_r": 0.1 },
      "observational": {
        "surface_brightness_peak": 0.0005,
        "fractional_luminosity": 0.004
      },
      "atmospheric": { "albedo": 0.03 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD157587", "alternate_names": ["HD-157587"] },
    "parameters": {
      "orbital": { "inclination": 72.2 },
      "geometric": {
        "mean_radius_r0": 0.8152,
        "inner_edge_r1": 0.3057,
        "outer_edge_r2": 5.095
      },
      "observational": { "fractional_luminosity": 0.00079 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "Au Mic" },
    "parameters": {
      "orbital": { "inclination": 90 },
      "geometric": {
        "mean_radius_r0": 2.42,
        "inner_edge_r1": 0.81,
        "outer_edge_r2": 4.04,
        "delta_r": 2.2
      },
      "observational": {
        "surface_brightness_peak": 0.002,
        "fractional_luminosity": 0.00039
      },
      "atmospheric": { "albedo": 0.99 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD129590",
      "alternate_names": ["HD-129590", "HIP-72070"]
    },
    "parameters": {
      "orbital": { "inclination": 75 },
      "geometric": { "mean_radius_r0": 0.46 },
      "observational": { "fractional_luminosity": 0.005 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "GSC 07396-00759" },
    "parameters": {
      "orbital": { "inclination": 82.7 },
      "geometric": {
        "mean_radius_r0": 0.98,
        "inner_edge_r1": 0.97,
        "outer_edge_r2": 0.99
      },
      "observational": { "fractional_luminosity": "<4e-3" },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD192758", "alternate_names": ["HD-192758"] },
    "parameters": {
      "orbital": { "inclination": 58 },
      "geometric": {
        "mean_radius_r0": 1.417910448,
        "inner_edge_r1": 1.28358209,
        "outer_edge_r2": 1.552238806,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 8.1e-5,
        "fractional_luminosity": 0.00057
      },
      "atmospheric": { "albedo": 0.04 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD 104860",
      "alternate_names": ["HD-104860", "HIP-58876"]
    },
    "parameters": {
      "orbital": { "inclination": 58 },
      "geometric": {
        "mean_radius_r0": 2.533333333,
        "inner_edge_r1": 2.4,
        "outer_edge_r2": 2.666666667,
        "delta_r": 0.1
      },
      "observational": {
        "surface_brightness_peak": 1.78e-5,
        "fractional_luminosity": 0.00063
      },
      "atmospheric": { "albedo": 0.01 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD105", "alternate_names": ["HD-105", "HIP-490"] },
    "parameters": {
      "orbital": { "inclination": 50 },
      "geometric": {
        "mean_radius_r0": 2.16,
        "inner_edge_r1": 1.79,
        "outer_edge_r2": 2.54,
        "delta_r": 0.75
      },
      "observational": {
        "surface_brightness_peak": 1.5e-5,
        "fractional_luminosity": 0.00026
      },
      "atmospheric": { "albedo": 0.14 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD160305",
      "alternate_names": ["HD-160305", "HIP-86598"]
    },
    "parameters": {
      "orbital": { "inclination": 82.3 },
      "geometric": {
        "mean_radius_r0": 1.39,
        "inner_edge_r1": 1.18,
        "outer_edge_r2": 1.59
      },
      "observational": { "fractional_luminosity": 0.00012 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD156623",
      "alternate_names": ["HD-156623", "HIP-84881"]
    },
    "parameters": {
      "orbital": { "inclination": 35 },
      "geometric": { "mean_radius_r0": 0.58, "outer_edge_r2": 45 },
      "observational": { "fractional_luminosity": 0.005 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD111161",
      "alternate_names": ["HD-1111161", "HIP-62482"]
    },
    "parameters": {
      "orbital": { "inclination": 62.1 },
      "geometric": { "mean_radius_r0": 0.63 },
      "observational": { "fractional_luminosity": 0.00055 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD98363",
      "alternate_names": ["HD-98363", "HIP-55188"]
    },
    "parameters": {
      "orbital": { "inclination": 80 },
      "geometric": {
        "mean_radius_r0": 0.5,
        "inner_edge_r1": 0.4,
        "outer_edge_r2": 0.65
      },
      "observational": { "fractional_luminosity": 0.00064 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD143675",
      "alternate_names": ["HD-143675", "HIP-78641"]
    },
    "parameters": {
      "orbital": { "inclination": 87.2 },
      "geometric": {
        "mean_radius_r0": 0.4,
        "inner_edge_r1": 0.35,
        "outer_edge_r2": 0.45
      },
      "observational": { "fractional_luminosity": 0.00041 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD145560",
      "alternate_names": ["HD-145560", "HIP-79516"]
    },
    "parameters": {
      "orbital": { "inclination": 43.9 },
      "geometric": { "mean_radius_r0": 0.67 },
      "observational": { "fractional_luminosity": 0.0014 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD117214",
      "alternate_names": ["HD-117214", "HIP-65875"]
    },
    "parameters": {
      "orbital": { "inclination": 71 },
      "geometric": { "mean_radius_r0": 0.42 },
      "observational": { "fractional_luminosity": 0.0025 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD172555",
      "alternate_names": ["HD-172555", "HIP-92024"]
    },
    "parameters": {
      "orbital": { "inclination": 76.5 },
      "geometric": {
        "mean_radius_r0": 0.36,
        "inner_edge_r1": 0.3,
        "outer_edge_r2": 0.46
      },
      "observational": { "fractional_luminosity": 0.00072 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD141011",
      "alternate_names": ["HD-141011", "HIP-77432"]
    },
    "parameters": {
      "orbital": { "inclination": 69.3 },
      "geometric": {
        "mean_radius_r0": 0.9815377425,
        "inner_edge_r1": 0.9550518034,
        "outer_edge_r2": 1.0493105,
        "delta_r": 0.0987
      },
      "observational": { "fractional_luminosity": 0.00018 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "BD+45\u25e6 598" },
    "parameters": {
      "orbital": { "inclination": 87 },
      "geometric": {
        "mean_radius_r0": 0.96,
        "inner_edge_r1": 0.68,
        "outer_edge_r2": 1.2
      },
      "observational": { "fractional_luminosity": 0.0006 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD121617", "alternate_names": ["HD-121617"] },
    "parameters": {
      "orbital": { "inclination": 43.1 },
      "geometric": { "mean_radius_r0": 0.66 },
      "observational": { "fractional_luminosity": 0.0048 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": { "name": "HD131488", "alternate_names": ["HD-131488"] },
    "parameters": {
      "orbital": { "inclination": 83 },
      "geometric": { "mean_radius_r0": 0.45, "delta_r": 0.15 },
      "observational": {
        "surface_brightness_peak": 0.002,
        "fractional_luminosity": 0.0012
      },
      "atmospheric": { "albedo": 0.49 }
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD112810",
      "alternate_names": ["HD-112810", "HIP-63439"]
    },
    "parameters": {
      "orbital": { "inclination": 70 },
      "geometric": null,
      "observational": { "fractional_luminosity": 0.0008 },
      "atmospheric": null
    }
  }'),
  ('{
    "internal": { "object_status": "confirmed", "main_star_id": null },
    "identify": {
      "name": "HD16743",
      "alternate_names": ["HD-16743", "HIP-12361"]
    },
    "parameters": {
      "orbital": { "inclination": 87.3 },
      "geometric": {
        "mean_radius_r0": 2.72,
        "inner_edge_r1": 2.04,
        "outer_edge_r2": 3.41,
        "delta_r": 1.38
      },
      "observational": {
        "surface_brightness_peak": 4e-5,
        "fractional_luminosity": 0.00032
      },
      "atmospheric": { "albedo": 0.41 }
    }
  }');
