INSERT INTO disk_api.catalogheaders(ref, data)
VALUES
(
  true,
  '{
    "planet": {
        "all_field": [],
        "compact": []
    },
    "star": {
        "all_field": [],
        "compact": []
    },
    "disk": {
        "sorting_order": [
            "Identity",
            "orbital parameters",
            "Model.",
            "Geometric",
            "atmo.",
            "Observational",
            "Observatory"
        ],
        "all_field": [
        "geometric",
        "inner_edge_r1",
        "outer_edge_r2",
        "mean_radius_r0",
        "delta_r",
        "gaussian_distribution_r0",
        "gaussian_distribution_sigma_r",
        "inclination",
        "eccentricity",
        "argument_of_periapsis",
        "pos_angle",
        "observational",
        "surface_brightness_peak",
        "slope_SB",
        "asymmetry_SB_or_gas",
        "integrated_flux",
        "polarized_fraction",
        "albedo",
        "fractional_luminosity",
        "spatial_resolution",
        "spectral_resolution",
        "modeling",
        "dust_mass",
        "gas_mass",
        "surface_density_sigma",
        "R0_sigma",
        "slope_sigma_rin",
        "slope_sigma_rout",
        "blowout_size",
        "minimum_grain_size",
        "dynamical_excitation_assumed",
        "vertical_optical_depth",
        "radial_optical_depth",
        "q_size_distribution",
        "observatory_used",
        "facility_name",
        "instrument_name",
        "links",
        "alma_archive",
        "herschel_archive",
        "spitzer_archive",
        "eso_archive",
        "aladin",
        "simbad",
        "mast",
        "diva+_database",
        "orbital",
        "atmospheric"
        ],
        "compact": []
    },
    "common": {
        "all_field": ["name", "identify", "alternate_names"],
        "compact": []
    }
}'
);
