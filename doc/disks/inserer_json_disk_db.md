---
author: Ulysse Chosson <ulysse.chosson@obspm.fr>
---

# Insérer les données pour la base de disque

## Insérer les données

Pour cela il faut executer éxecuter les 4 commandes suivante depuis la racine du projet.

```sh
psql -h <host> -U <utilisateur> -d <base> -f doc/disks/sql/insert_exodict.sql
psql -h <host> -U <utilisateur> -d <base> -f doc/disks/sql/insert_disks.sql
psql -h <host> -U <utilisateur> -d <base> -f doc/disks/sql/insert_stars.sql
psql -h <host> -U <utilisateur> -d <base> -f doc/disks/sql/insert_catalog_headers.sql
```

## Lié les etoiles relationelle et json

Lancer:

```sh
python doc/disks/py-scripts/link_rel_star_and_json_star.py
```

## Créer les systemes gravitaionel

Lancer:

```sh
python doc/disks/py-scripts/create_grav_sys.py
```
