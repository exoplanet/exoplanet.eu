"""
exoplanet.canary URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Django 💩 imports
from django.urls import path

# Local imports
from . import views

urlpatterns = [
    path(route="legal_notice/", view=views.LegalNotice.as_view(), name="legal notice"),
    path(route="tools/", view=views.Tools.as_view(), name="tools"),
    path(route="planet_index/", view=views.PlanetIndex.as_view(), name="planet_index"),
]
