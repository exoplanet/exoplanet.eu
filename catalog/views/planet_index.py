"""View for the page displaying a list of all planet in the database."""

# Standard imports
from typing import Any

# Django 💩 imports
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

# First party imports
from common.views import NewDesignBaseTemplateView
from core.models import Planet


class PlanetIndex(NewDesignBaseTemplateView):
    """Display a page with all planet with a link to their personal page."""

    template_name = "planet_index.html"
    "Name of the template of the page."

    title = "Planet Index"
    "Title of the page."

    description = "All planets in alphabetical order."
    "Description of the page."

    is_beta = True
    "This page is it in beta ?"

    # pylint: disable=arguments-differ
    def get(
        self,
        request: HttpRequest,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        View for planets index.

        Args:
            request: http request for the page.

        Returns:
            Page with all planet name and link to personal page.
        """

        planets_info: dict[str, list[str]] = {}
        other = []

        for planet in Planet.objects.order_by("name").all():
            first_letter = planet.name[0].upper()

            if not first_letter.isalpha():
                other.append(planet.name_url_html)
                continue

            # pylint: disable-next=consider-iterating-dictionary
            if first_letter not in planets_info.keys():
                planets_info[first_letter] = []

            planets_info[first_letter].append(planet.name_url_html)

        planets_info["OTHER"] = other

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                planets=planets_info,
            ),
        )
