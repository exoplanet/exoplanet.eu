"""All views for the catalog app"""

# Local imports
from .legal_notice import LegalNotice  # noqa: F401
from .planet_index import PlanetIndex  # noqa: F401
from .tools import Tools  # noqa: F401
