"""View for the page displaying all the tools of the project."""

# Standard imports
from dataclasses import dataclass

# Django 💩 imports
from django.urls import reverse_lazy

# First party imports
from common.views import NewDesignBaseTemplateView


@dataclass
class _ToolDesc:
    """Describe a tool to show in the tools page."""

    name: str
    description: str
    img_name: str  # must exist in catalog/static/img/ and be 600x225 px
    img_alt: str
    link: str


class Tools(NewDesignBaseTemplateView):
    """
    Display a page with links to all the tools developed in relation to the site.

    It's basically a static page.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "tools.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the tamplate through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Tools"
    description = "All tools developed using the exoplanets catalogue."

    is_beta = True

    all_tools = [
        _ToolDesc(
            name="AMDf",
            description="Exoplanet Stability Predictor",
            img_name="amd_tool_capture.png",
            img_alt="Capture of an AMD simulation graph",
            link=reverse_lazy(viewname="amd_list"),
        ),
        _ToolDesc(
            name="GCM 1D",
            description=(
                "General Circulation Model unidimensional, multi parameters simulation"
            ),
            img_name="gcm1d_tool_capture.png",
            img_alt="Capture of a GCM &D simulation",
            link="http://exoplanetes.esep.pro/esep_gcm1d?lang=en",
        ),
        _ToolDesc(
            name="Exodam",
            description="Exodam public tools",
            img_name="exodam_logo.png",
            img_alt="Exodam logo",
            link="#",
        ),
    ]
