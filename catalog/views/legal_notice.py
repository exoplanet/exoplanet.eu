# First party imports
from common.views import NewDesignBaseTemplateView


class LegalNotice(NewDesignBaseTemplateView):
    """
    Display a page with all legal information about le site.

    It's basically a static page.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "mentions_legales.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the tamplate through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Mentions Légales"
    description = (
        "Ce site est soumis au régime juridique français. À ce titre, il est un "
        "service de communication au public en ligne édité à titre professionnel au "
        "sens de l'<a href='https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000023711900&cidTexte=LEGITEXT000005789847'>"  # noqa: E501
        "article 6, III, 1° de la loi 2004-575 du 21 juin 2004 pour la confiance dans "
        "l’économie numérique.</a>."
    )
