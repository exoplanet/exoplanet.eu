#!/usr/bin/env bash

# Formating func
# See: https://www.shellhacks.com/bash-colors/
function title_echo()
{
    printf "\e[1m%s\n\e[0m" "$1"
}

# Usefull vars
DB_USER='exo_user'
DB_HOST='localhost'
DB_NAME='exoplanet'
export DB_USER
export DB_HOST
export DB_NAME

# Useful local directories
# See: https://stackoverflow.com/a/246128/20450444
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROJECT_DIR=$(realpath "${SCRIPT_DIR}/..")

# Confirmation
title_echo "You are going to wipe all data and users from the DB ${DB_NAME} on ${DB_HOST}"
read -p "Press any key to continue (Ctrl+C to abort)" -n 1 -r
echo

# Stop postgREST service if any
title_echo "👉️ Stop the postgREST service"
sudo systemctl stop postgrest && echo 'Service stopped 👍️' || echo 'Service was already stopped 👍️'
sudo systemctl disable postgrest && echo 'Service disabled 👍️' || echo 'Service was already disabled 👍️'

# Get a password for the main users
title_echo "👉️ We need to create a new password for the DB user ${DB_USER}"
echo -n "Enter new password: "; read -s -r EXO_USER_PWD; echo
echo -n "Confirm new password: "; read -s -r EXO_USER_PWD_CONFIRM; echo
if [ "${EXO_USER_PWD}" != "${EXO_USER_PWD_CONFIRM}" ]; then
    echo "Passwords do not match, exiting"
    exit 1
fi

title_echo "👉️ And we also need to create a new password for the DB user exo_authenticator of postgREST API"
echo -n "Enter new password: "; read -s -r AUTHENTICATOR_PWD; echo
echo -n "Confirm new password: "; read -s -r AUTHENTICATOR_PWD_CONFIRM; echo
if [ "${AUTHENTICATOR_PWD}" != "${AUTHENTICATOR_PWD_CONFIRM}" ]; then
    echo "Passwords do not match, exiting"
    exit 1
fi

# Cleanup any pre-existing DB or users
title_echo "👉️ Remove existing db and users if needed"
sudo -i -u postgres bash <<EOF
    psql -c "DROP DATABASE IF EXISTS ${DB_NAME};"
    psql -c "DROP SCHEMA IF EXISTS disk_api;"
    psql -c "DROP USER IF EXISTS test_exo_authenticator;"
    psql -c "DROP USER IF EXISTS exo_authenticator;"
    psql -c "DROP USER IF EXISTS exo_web_anon_test;"
    psql -c "DROP USER IF EXISTS exo_web_anon;"
    psql -c "DROP USER IF EXISTS guest;"
    psql -c "DROP USER IF EXISTS ${DB_USER};"
EOF

# Create and config db users
title_echo "👉️ Creation of DB users"
sudo -i -u postgres bash <<-EOF
    psql <<-EOFSQL
        -- to manipulate the database
        CREATE USER ${DB_USER};
        ALTER USER ${DB_USER} WITH ENCRYPTED PASSWORD '${EXO_USER_PWD}';
        ALTER USER ${DB_USER} WITH CREATEDB;
        -- to manipulate the materialized view
        CREATE USER guest;
        -- needed for the postgREST server
        CREATE USER exo_web_anon nologin;
        CREATE USER exo_web_anon_test nologin;
        -- public user for postgREST server
        CREATE ROLE exo_authenticator noinherit LOGIN ENCRYPTED PASSWORD '${AUTHENTICATOR_PWD}';
        GRANT exo_web_anon TO exo_authenticator;
        CREATE ROLE test_exo_authenticator noinherit LOGIN ENCRYPTED PASSWORD 'test_dummy_pw';
        GRANT exo_web_anon_test TO test_exo_authenticator;
EOFSQL
EOF

# Create .env file for automated access
title_echo "👉️ Creation .env file for the project with DB access"
# See why we need to this like that: https://stackoverflow.com/a/57678930/20450444
LOCAL_SECRET_KEY=$(python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')
TEST_SECRET_KEY=$(python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')
export EXO_USER_PWD
export LOCAL_SECRET_KEY
export TEST_SECRET_KEY
envsubst <"install/template.env" >".env" && echo ".env CREATED"

# Create config postgREST
title_echo "👉️ Creation config file for postgREST"
sudo mkdir -p /etc/postgrest
# See: https://stackoverflow.com/a/56934114/20450444
JWT_SECRET=$(openssl rand -base64 172 | tr -d '\n')
export JWT_SECRET
export AUTHENTICATOR_PWD
envsubst <"install/template.postgrest.config" >"/tmp/postgrest_config"
sudo mv /tmp/postgrest_config /etc/postgrest/config && echo "config CREATED"

# Create the databases
title_echo "👉️ Creation of the database"
sudo -i -u postgres bash <<EOF
    psql -c "CREATE DATABASE ${DB_NAME} OWNER ${DB_USER};"
EOF

# Create DB schema
title_echo "👉️ Creation of the DB schema by Django"
poetry run python manage.py migrate --configuration=Local
# To see the state of the migration use:
# python manage.py showmigrations --configuration=Local
# To revert migration use:
# python manage.py migrate admin zero --configuration=Local
# python manage.py migrate auth zero --configuration=Local
# python manage.py migrate contenttypes zero --configuration=Local
# python manage.py migrate core zero --configuration=Local
# python manage.py migrate disk zero --configuration=Local
# python manage.py migrate easy_thumbnails zero --configuration=Local
# python manage.py migrate editorial zero --configuration=Local
# python manage.py migrate filer zero --configuration=Local
# python manage.py migrate flatpages zero --configuration=Local
# python manage.py migrate sessions zero --configuration=Local
# python manage.py migrate sites zero --configuration=Local

# Cleanup image files
title_echo "👉️ Cleanup previous flatpage files"

FILER_DIR='media'

rm -rfv "${PROJECT_DIR}/${FILER_DIR}/filer_public*" && echo 'Cleanup OK 👍️' || echo '⚠️ Cleanup FAILED ⚠️'

# Config postgREST systemd service
# See: https://exoplanet.pages.obspm.fr/exodoc/sources/postgrest/postgrest_systemd.html
title_echo "👉️ Config systemd service config for postgREST"
sudo mkdir -p /etc/systemd/system
sudo cp "install/template.postgrest.service" "/etc/systemd/system/postgrest.service" && echo 'Config OK 👍️' || echo '⚠️ Config FAILED ⚠️'

sudo systemctl start postgrest && echo "postgREST service started 👍️" || echo "⚠️ Service start FAILED ⚠️"

echo "Do you want to autostart postgREST server at machine startup?"
select YN_SYSTEMD in "Yes" "No"; do
    case $YN_SYSTEMD in
        Yes )
            sudo systemctl enable postgrest && echo 'PostgREST autostart enabled 👍️' || echo '⚠️ PostgREST autostart FAILED ⚠️'
            break;;
        No )
            echo "nothing to do"
            break;;
    esac
done

# TODO: Need to be tested
# TODO: Add the clone and setup of exodam-api
# Config Exodam-API systemd service
# See: https://exoplanet.pages.obspm.fr/exodoc/sources/postgrest/postgrest_systemd.html
title_echo "👉️ Config systemd service config for Exodam-API"
sudo mkdir -p /etc/systemd/system
sudo cp "install/template.exodam-api.service" "/etc/systemd/system/exodam-api.service" && echo 'Config OK 👍️' || echo '⚠️ Config FAILED ⚠️'

sudo systemctl start exodam-api && echo "Exodam-API service started 👍️" || echo "⚠️ Service start FAILED ⚠️"

echo "Do you want to autostart Exodam-API server at machine startup?"
select YN_SYSTEMD in "Yes" "No"; do
    case $YN_SYSTEMD in
        Yes )
            sudo systemctl enable exodam-api && echo 'Exodam-API autostart enabled 👍️' || echo '⚠️ Exodam-API autostart FAILED ⚠️'
            break;;
        No )
            echo "nothing to do"
            break;;
    esac
done


# Info about ~/.pgpass file for automated access
title_echo "👉️ Add user to .pgpass for easy access"

# useful vars
LOCAL_DB_ACCESS_LINE="${DB_HOST}:5432:${DB_NAME}:${DB_USER}:${EXO_USER_PWD}"
PGPASS_FILE="${HOME}/.pgpass"

# A little RTFM
echo "ℹ️ Please read this doc if you haven't yet https://exoplanet.pages.obspm.fr/exodoc/sources/psql/psql_create_and_use_user.html#avoid-entering-your-password-each-time if you want to simplify your life."

echo "Do you want exo_user to be added to your .pgpass for easy access?"
select YN_PGPASS in "Yes" "No"; do
    case $YN_PGPASS in
        Yes )
            # Create file if not present
            if [ ! -e "$PGPASS_FILE" ] ; then
                touch "$PGPASS_FILE" && echo "${PGPASS_FILE} file created 👍️"
            fi

            # Give the recommanded rights
            # See: https://www.postgresql.org/docs/current/libpq-pgpass.html
            chmod 0600 "$PGPASS_FILE" && echo "Access rights changed 👍️"

            # Add the line
            echo "${LOCAL_DB_ACCESS_LINE}" >> "${PGPASS_FILE}" && echo 'Line written 👍️' || echo '⚠️ Write FAILED ⚠️'
            break;;
        No )
            echo "nothing to do"
            break;;
    esac
done

title_echo '🎉 ~~~ Database initialisation finished ~~~ 🎉'
