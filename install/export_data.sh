#!/usr/bin/env bash

# Input parameters
SERVER=$1
TO_LOCAL_DIR=$2

# Formating func
# See: https://www.shellhacks.com/bash-colors/
function title_echo()
{
    printf "\e[1m%s\n\e[0m" "$1"
}

# Some var
EXPORT_DIR=export_exoplanet_from_${SERVER}_at_$(date +"%Y-%m-%d-%H-%M-%S")
EXPORT_FULL_PATH="/tmp/${EXPORT_DIR}"

# Generate the csv on the server
title_echo "👉️ export all tables to csv"
# We need local var substitution
# shellcheck disable=SC2087
ssh -T "${SERVER}" <<-EOFSSH
    sudo -u exoplanet bash <<-EOFBASH
        mkdir -p "${EXPORT_FULL_PATH}"
        psql <<-EOFSQL
            -- scientific data
            \COPY (SELECT * FROM core_star) TO '${EXPORT_FULL_PATH}/core_star.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_planetarysystem) TO '${EXPORT_FULL_PATH}/core_planetarysystem.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_planet) TO '${EXPORT_FULL_PATH}/core_planet.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_planet2star) TO '${EXPORT_FULL_PATH}/core_planet2star.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_alternatestarname) TO '${EXPORT_FULL_PATH}/core_alternatestarname.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_alternateplanetname) TO '${EXPORT_FULL_PATH}/core_alternateplanetname.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_molecule) TO '${EXPORT_FULL_PATH}/core_molecule.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_publication) TO '${EXPORT_FULL_PATH}/core_publication.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_atmospheremolecule) TO '${EXPORT_FULL_PATH}/core_atmospheremolecule.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_planet2publication) TO '${EXPORT_FULL_PATH}/core_planet2publication.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_star2publication) TO '${EXPORT_FULL_PATH}/core_star2publication.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_atmospheretemperature) TO '${EXPORT_FULL_PATH}/core_atmospheretemperature.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM core_import) TO '${EXPORT_FULL_PATH}/core_import.csv' WITH DELIMITER ',' CSV HEADER;
            -- disk data
            \COPY (SELECT * FROM disk_api.exodict) TO '${EXPORT_FULL_PATH}/disk_api.exodict.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM disk_api.disk) TO '${EXPORT_FULL_PATH}/disk_api.disk.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM disk_api.star) TO '${EXPORT_FULL_PATH}/disk_api.star.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM disk_api.catalogheaders) TO '${EXPORT_FULL_PATH}/disk_api.catalogheaders.csv' WITH DELIMITER ',' CSV HEADER;
            -- editorial data
            \COPY (SELECT * FROM editorial_link) TO '${EXPORT_FULL_PATH}/editorial_link.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM editorial_meeting) TO '${EXPORT_FULL_PATH}/editorial_meeting.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM editorial_news) TO '${EXPORT_FULL_PATH}/editorial_news.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM editorial_research) TO '${EXPORT_FULL_PATH}/editorial_research.csv' WITH DELIMITER ',' CSV HEADER;
            -- flatpage data
            \COPY (SELECT * FROM django_flatpage_sites) TO '${EXPORT_FULL_PATH}/django_flatpage_sites.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM django_flatpage) TO '${EXPORT_FULL_PATH}/django_flatpage.csv' WITH DELIMITER ',' CSV HEADER;
            -- file upload data (without the files themselves)
            \COPY (SELECT * FROM filer_clipboard) TO '${EXPORT_FULL_PATH}/filer_clipboard.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM filer_clipboarditem) TO '${EXPORT_FULL_PATH}/filer_clipboarditem.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM filer_file) TO '${EXPORT_FULL_PATH}/filer_file.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM filer_folder) TO '${EXPORT_FULL_PATH}/filer_folder.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM filer_folderpermission) TO '${EXPORT_FULL_PATH}/filer_folderpermission.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM filer_image) TO '${EXPORT_FULL_PATH}/filer_image.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM filer_thumbnailoption) TO '${EXPORT_FULL_PATH}/filer_thumbnailoption.csv' WITH DELIMITER ',' CSV HEADER;
            -- admin data
            \COPY (SELECT * FROM django_admin_log) TO '${EXPORT_FULL_PATH}/django_admin_log.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM django_site WHERE id <> 1) TO '${EXPORT_FULL_PATH}/django_site.csv' WITH DELIMITER ',' CSV HEADER; -- We need to exclude site id=1 since it is already present in an empty DB
            -- user data
            \COPY (SELECT * FROM auth_group) TO '${EXPORT_FULL_PATH}/auth_group.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM auth_group_permissions) TO '${EXPORT_FULL_PATH}/auth_group_permissions.csv' WITH DELIMITER ',' CSV HEADER;
            -- auth_permission are handled by migrations already
            -- \COPY (SELECT * FROM auth_permission) TO '${EXPORT_FULL_PATH}/auth_permission.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM auth_user) TO '${EXPORT_FULL_PATH}/auth_user.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM auth_user_groups) TO '${EXPORT_FULL_PATH}/auth_user_groups.csv' WITH DELIMITER ',' CSV HEADER;
            \COPY (SELECT * FROM auth_user_user_permissions) TO '${EXPORT_FULL_PATH}/auth_user_user_permissions.csv' WITH DELIMITER ',' CSV HEADER;

EOFSQL
EOFBASH
EOFSSH

# Ensure the local dir exist
title_echo "👉️ Ensure ${TO_LOCAL_DIR} exist"
mkdir -p "${TO_LOCAL_DIR}" && echo 'Directory exist 👍️' || echo '⚠️ Directory creation FAILED ⚠️'

# Download the csv to local machine
title_echo "👉️ ${BOLD}download all csv to ${TO_LOCAL_DIR}/${EXPORT_DIR}${ENDBOLD}"
scp -r "${SERVER}:${EXPORT_FULL_PATH}" "${TO_LOCAL_DIR}" && echo 'Download OK 👍️' || echo '⚠️ FAILED ⚠️'

# Download the images files to local machine
title_echo "👉️ ${BOLD}download all img to ${TO_LOCAL_DIR}/${EXPORT_DIR}/media${ENDBOLD}"

FILER_BASE_FULL_PATH="/opt/exoplanet/exoplanet.eu/media"

scp -r "${SERVER}:${FILER_BASE_FULL_PATH}" "${TO_LOCAL_DIR}/${EXPORT_DIR}" && echo 'Download OK 👍️' || echo '⚠️ FAILED ⚠️'

# Remove the csv from the server
title_echo "👉️ ${BOLD}Cleanup tmp csv of remote server${ENDBOLD}"
# We need local var substitution
# shellcheck disable=SC2087
ssh -T "${SERVER}" <<-EOFSSH
    sudo -u exoplanet bash <<-EOFBASH
        rm -rf ${EXPORT_FULL_PATH} && echo 'Cleanup OK 👍️' || echo '⚠️ FAILED ⚠️'
EOFBASH
EOFSSH

echo
title_echo "All your exported files are in 📁 ${TO_LOCAL_DIR}/${EXPORT_DIR}"
