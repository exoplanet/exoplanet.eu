#!/usr/bin/env bash

# Formating func
# See: https://www.shellhacks.com/bash-colors/
function title_echo()
{
    printf "\e[1m%s\n\e[0m" "$1"
}

# Add the repo for nodejs
title_echo "👉️ Install repository for node.js"
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=18
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
sudo apt-get update

# Install all that is easy to install from the repo
title_echo "👉️ Install all lib and program available via apt"
sudo apt install git git-lfs curl python3.10 python3-pip python3-venv apache2 apache2-dev postgresql libpq-dev nodejs

# Install postgREST
curl https://github.com/PostgREST/postgrest/releases/download/v11.1.0/postgrest-v11.1.0-linux-static-x64.tar.xz -o /tmp/postgrest.tar.xz
sudo tar xJf /tmp/postgrest.tar.xz -C /usr/local/bin/
rm /tmp/postgrest.tar.xz
postgrest -h > /dev/null && echo "PostgREST installed 👍️"

# Install pyenv
#
# It is used to install any python version without depending on the system
# installed version or system available version.
title_echo "👉️ Install pyenv"
curl https://pyenv.run | bash
# TODO: check if python 3.10 is installed and if not install it with pyenv

# Install pipx
#
# It is used to install all python command line tools cleanly system wise.
title_echo "👉️ Install pipx"
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Install poetry
#
# It is used to hendle virtual environment and dependancies for python in a
# modern and ergonomic way.
title_echo "👉️ Install poetry"
pipx install poetry

# Install just
#
# It is used a a modern make replacement for the whole project.
title_echo "👉️ Install rust, cargo and just"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
cargo install just

# Install the project itself from git and run its self install script
title_echo "👉️ Install the project itself via git and all it dependancies"
(
    git clone git@gitlab.obspm.fr:exoplanet/exoplanet.eu.git
    cd exoplanet.eu || exit
    poetry shell
    just install
)

# Some advice in case of problem
title_echo "👉️ Some final piece of advice"
echo "If the last step failed because of missing compatible python version install it using pyenv"
echo "It could be a good ideal to also follow the instructions to integrate pyenv with your shell: https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv"
