#!/usr/bin/env bash

# Formating func
# See: https://www.shellhacks.com/bash-colors/
function title_echo()
{
    printf "\e[1m%s\n\e[0m" "$1"
}

# Usefull vars
DB_USER='exo_user'
DB_HOST='localhost'
DB_NAME='exoplanet'


# Confirmation
title_echo "You are going to nuke all data from DB ${DB_NAME} on ${DB_USER}@${DB_HOST}"
read -p "Press any key to continue (Ctrl+C to abort)" -n 1 -r
echo

# Fill the databases
title_echo "👉️ Nuke the database 💣️"
psql -U ${DB_USER} -h ${DB_HOST} -d ${DB_NAME} <<-EOFSQL
    -- user data
    TRUNCATE table auth_group_permissions CASCADE;
    TRUNCATE table auth_group CASCADE;
    -- auth_permission are handled by migrations already
    TRUNCATE table auth_user_user_permissions CASCADE;
    TRUNCATE table auth_user_groups CASCADE;
    TRUNCATE table auth_user CASCADE;
    TRUNCATE table auth_permission CASCADE;
    -- admin data
    TRUNCATE table django_site CASCADE;
    TRUNCATE table django_admin_log CASCADE;
    -- file upload data (without the files themselves)
    TRUNCATE table filer_clipboard CASCADE;
    TRUNCATE table filer_clipboarditem CASCADE;
    TRUNCATE table filer_image CASCADE;
    TRUNCATE table filer_file CASCADE;
    TRUNCATE table filer_folder CASCADE;
    TRUNCATE table filer_folderpermission CASCADE;
    TRUNCATE table filer_thumbnailoption CASCADE;
    -- flatpage data
    TRUNCATE table django_flatpage_sites CASCADE;
    TRUNCATE table django_flatpage CASCADE;
    -- editorial data
    TRUNCATE table editorial_link CASCADE;
    TRUNCATE table editorial_meeting CASCADE;
    TRUNCATE table editorial_news CASCADE;
    TRUNCATE table editorial_research CASCADE;
    -- disk data
    TRUNCATE table disk_api.catalogheaders CASCADE;
    TRUNCATE table disk_api.star CASCADE;
    TRUNCATE table disk_api.disk CASCADE;
    TRUNCATE table disk_api.exodict CASCADE;
    -- scientific data
    TRUNCATE table core_planet2star CASCADE;
    TRUNCATE table core_alternatestarname CASCADE;
    TRUNCATE table core_alternateplanetname CASCADE;
    TRUNCATE table core_atmospheremolecule CASCADE;
    TRUNCATE table core_molecule CASCADE;
    TRUNCATE table core_planet2publication CASCADE;
    TRUNCATE table core_star2publication CASCADE;
    TRUNCATE table core_atmospheretemperature CASCADE;
    TRUNCATE table core_publication CASCADE;
    TRUNCATE table core_planet CASCADE;
    TRUNCATE table core_planetarysystem CASCADE;
    TRUNCATE table core_star CASCADE;
    TRUNCATE table core_import CASCADE;
EOFSQL

title_echo "👉️ database is empty 💥"

# Create DB schema
title_echo "👉️ Creation of the DB schema by Django"
poetry run python manage.py migrate --configuration=Local

# Cleanup image files
title_echo "👉️ Cleanup previous flatpage files"

FILER_DIR='media'

rm -rfv "${PROJECT_DIR}/${FILER_DIR}/filer_public*" && echo 'Cleanup OK 👍️' || echo '⚠️ Cleanup FAILED ⚠️'
