#!/usr/bin/env bash

# Input parameters
FROM_DIR=$(realpath "$1")

# Formating func
# See: https://www.shellhacks.com/bash-colors/
function title_echo()
{
    printf "\e[1m%s\n\e[0m" "$1"
}

# Usefull vars
DB_USER='exo_user'
DB_HOST='localhost'
DB_NAME='exoplanet'

# Useful local directories
# See
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROJECT_DIR=$(realpath "${SCRIPT_DIR}/..")

# Confirmation
title_echo "You are going to fill DB ${DB_NAME} on ${DB_USER}@${DB_HOST} with data from ${FROM_DIR}"
read -p "Press any key to continue (Ctrl+C to abort)" -n 1 -r
echo

# Fill the databases
title_echo "👉️ Fill the database"
psql -U ${DB_USER} -h ${DB_HOST} -d ${DB_NAME} <<-EOFSQL
    -- user data
    \COPY auth_group FROM '${FROM_DIR}/auth_group.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY auth_group_permissions FROM '${FROM_DIR}/auth_group_permissions.csv' WITH DELIMITER ',' CSV HEADER;
    -- auth_permission are handled by migrations already
    -- \COPY auth_permission FROM '${FROM_DIR}/auth_permission.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY auth_user FROM '${FROM_DIR}/auth_user.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY auth_user_groups FROM '${FROM_DIR}/auth_user_groups.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY auth_user_user_permissions FROM '${FROM_DIR}/auth_user_user_permissions.csv' WITH DELIMITER ',' CSV HEADER;
    -- admin data
    \COPY django_admin_log FROM '${FROM_DIR}/django_admin_log.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY django_site FROM '${FROM_DIR}/django_site.csv' WITH DELIMITER ',' CSV HEADER;
    -- file upload data (without the files themselves)
    \COPY filer_thumbnailoption FROM '${FROM_DIR}/filer_thumbnailoption.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY filer_folderpermission FROM '${FROM_DIR}/filer_folderpermission.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY filer_folder FROM '${FROM_DIR}/filer_folder.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY filer_file FROM '${FROM_DIR}/filer_file.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY filer_image FROM '${FROM_DIR}/filer_image.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY filer_clipboarditem FROM '${FROM_DIR}/filer_clipboarditem.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY filer_clipboard FROM '${FROM_DIR}/filer_clipboard.csv' WITH DELIMITER ',' CSV HEADER;
    -- flatpage data
    \COPY django_flatpage FROM '${FROM_DIR}/django_flatpage.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY django_flatpage_sites FROM '${FROM_DIR}/django_flatpage_sites.csv' WITH DELIMITER ',' CSV HEADER;
    -- editorial data
    \COPY editorial_research FROM '${FROM_DIR}/editorial_research.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY editorial_news FROM '${FROM_DIR}/editorial_news.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY editorial_meeting FROM '${FROM_DIR}/editorial_meeting.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY editorial_link FROM '${FROM_DIR}/editorial_link.csv' WITH DELIMITER ',' CSV HEADER;
    -- disk data
    \COPY disk_api.exodict FROM '${FROM_DIR}/disk_api.exodict.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY disk_api.disk FROM '${FROM_DIR}/disk_api.disk.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY disk_api.star FROM '${FROM_DIR}/disk_api.star.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY disk_api.catalogheaders FROM '${FROM_DIR}/disk_api.catalogheaders.csv' WITH DELIMITER ',' CSV HEADER;
    -- scientific data
    \COPY core_import FROM '${FROM_DIR}/core_import.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_star FROM '${FROM_DIR}/core_star.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_planetarysystem FROM '${FROM_DIR}/core_planetarysystem.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_planet FROM '${FROM_DIR}/core_planet.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_publication FROM '${FROM_DIR}/core_publication.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_atmospheretemperature FROM '${FROM_DIR}/core_atmospheretemperature.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_star2publication FROM '${FROM_DIR}/core_star2publication.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_planet2publication FROM '${FROM_DIR}/core_planet2publication.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_molecule FROM '${FROM_DIR}/core_molecule.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_atmospheremolecule FROM '${FROM_DIR}/core_atmospheremolecule.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_alternateplanetname FROM '${FROM_DIR}/core_alternateplanetname.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_alternatestarname FROM '${FROM_DIR}/core_alternatestarname.csv' WITH DELIMITER ',' CSV HEADER;
    \COPY core_planet2star FROM '${FROM_DIR}/core_planet2star.csv' WITH DELIMITER ',' CSV HEADER;
EOFSQL

# Reset id counter to prevent insertion in the holes in id
title_echo "👉️ Reset id counter"
psql -U ${DB_USER} -h ${DB_HOST} -d ${DB_NAME} --no-align --tuples-only <<-EOFSQL
    -- editorial data
    SELECT setval('editorial_link_id_seq', (SELECT max(id) FROM editorial_link));
    SELECT setval('editorial_meeting_id_seq', (SELECT max(id) FROM editorial_meeting));
    SELECT setval('editorial_news_id_seq', (SELECT max(id) FROM editorial_news));
    SELECT setval('editorial_research_id_seq', (SELECT max(id) FROM editorial_research));
    -- scientific data
    SELECT setval('core_import_id_seq', (SELECT max(id) FROM core_import));
    SELECT setval('core_star_id_seq', (SELECT max(id) FROM core_star));
    SELECT setval('core_planetarysystem_id_seq', (SELECT max(id) FROM core_planetarysystem));
    SELECT setval('core_planet_id_seq', (SELECT max(id) FROM core_planet));
    SELECT setval('core_planet2star_id_seq', (SELECT max(id) FROM core_planet2star));
    SELECT setval('core_alternatestarname_id_seq', (SELECT max(id) FROM core_alternatestarname));
    SELECT setval('core_alternateplanetname_id_seq', (SELECT max(id) FROM core_alternateplanetname));
    SELECT setval('core_molecule_id_seq', (SELECT max(id) FROM core_molecule));
    SELECT setval('core_publication_id_seq', (SELECT max(id) FROM core_publication));
    SELECT setval('core_atmospheremolecule_id_seq', (SELECT max(id) FROM core_atmospheremolecule));
    SELECT setval('core_planet2publication_id_seq', (SELECT max(id) FROM core_planet2publication));
    SELECT setval('core_star2publication_id_seq', (SELECT max(id) FROM core_star2publication));
    SELECT setval('core_atmospheretemperature_id_seq', (SELECT max(id) FROM core_atmospheretemperature));
    -- disk data
    SELECT setval('disk_api.exodict_id_seq', (SELECT max(id) FROM disk_api.exodict));
    SELECT setval('disk_api.disk_id_seq', (SELECT max(id) FROM disk_api.disk));
    SELECT setval('disk_api.star_id_seq', (SELECT max(id) FROM disk_api.star));
    SELECT setval('disk_api.catalogheaders_id_seq', (SELECT max(id) FROM disk_api.catalogheaders));
    -- flatpage data
    SELECT setval('django_content_type_id_seq', (SELECT max(id) FROM django_content_type));
    SELECT setval('django_flatpage_id_seq', (SELECT max(id) FROM django_flatpage));
    SELECT setval('django_flatpage_sites_id_seq', (SELECT max(id) FROM django_flatpage_sites));
    -- file upload data (without the files themselves)
    SELECT setval('filer_clipboard_id_seq', (SELECT max(id) FROM filer_clipboard));
    SELECT setval('filer_clipboarditem_id_seq', (SELECT max(id) FROM filer_clipboarditem));
    SELECT setval('filer_file_id_seq', (SELECT max(id) FROM filer_file));
    SELECT setval('filer_folder_id_seq', (SELECT max(id) FROM filer_folder));
    SELECT setval('filer_folderpermission_id_seq', (SELECT max(id) FROM filer_folderpermission));
    -- filer_image does not seem to have an associated id sequence
    -- SELECT setval('filer_image_id_seq', (SELECT max(id) FROM filer_image));
    SELECT setval('filer_thumbnailoption_id_seq', (SELECT max(id) FROM filer_thumbnailoption));
    -- user data
    SELECT setval('auth_group_id_seq', (SELECT max(id) FROM auth_group));
    SELECT setval('auth_group_permissions_id_seq', (SELECT max(id) FROM auth_group_permissions));
    -- auth_permission are handled by migrations already
    -- SELECT setval('auth_permission_id_seq', (SELECT max(id) FROM auth_permission));
    SELECT setval('auth_user_id_seq', (SELECT max(id) FROM auth_user));
    SELECT setval('auth_user_groups_id_seq', (SELECT max(id) FROM auth_user_groups));
    SELECT setval('auth_user_user_permissions_id_seq', (SELECT max(id) FROM auth_user_user_permissions));
    -- admin data
    SELECT setval('django_admin_log_id_seq', (SELECT max(id) FROM django_admin_log));
    SELECT setval('django_site_id_seq', (SELECT max(id) FROM django_site));
EOFSQL
echo "RESET DONE 👍️"

title_echo "👉️ Copy image files for flatpages"

FILER_DIR='media'
FILER_GROUP='www-data'

cp -r "${FROM_DIR}/${FILER_DIR}" "${PROJECT_DIR}" && echo 'Copy OK 👍️' || echo '⚠️ Copy FAILED ⚠️'
sudo chown -R :${FILER_GROUP} "${PROJECT_DIR}/${FILER_DIR}"/filer_public* && echo 'Group change OK 👍️' || echo '⚠️ Group change FAILED ⚠️'
