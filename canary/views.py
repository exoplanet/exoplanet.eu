# Django 💩 imports
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

# First party imports
from common.views import NewDesignBaseTemplateView


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Piou piou. The canary is alive. Your Django seems to work…")


def allflatpages(request: HttpRequest) -> HttpResponse:
    return render(request, "allflatpages.html")


def pagewithstatic(request: HttpRequest) -> HttpResponse:
    return render(request, "pagewithstatic.html")


def pagewithscss(request: HttpRequest) -> HttpResponse:
    return render(request, "pagewithscss.html")


def pagewithbasetemplate(request: HttpRequest) -> HttpResponse:
    return render(request, "pagewithbasetemplate.html")


class PageWithBaseTemplate(NewDesignBaseTemplateView):
    template_name = "pagewithbasetemplate.html"
    title = "Canary page with base template"
    description = "A page to test the usage of base template."
