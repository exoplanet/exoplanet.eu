# Standard imports
import contextlib


@contextlib.contextmanager
def ctx(val):
    """
    A simple context manager that allow to define a variable just inside the with block.
    """
    yield val
