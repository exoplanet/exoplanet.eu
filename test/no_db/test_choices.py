# Standard imports
import random
from inspect import isclass

# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from hypothesis import example, given
from hypothesis import strategies as st
from pytest import param

# Django 💩 imports
from django.db.models import IntegerChoices

# First party imports
from core.models import choices
from core.models.choices import AllDetection, PlanetDetection, value_from_approx_name


@pytest.mark.parametrize(
    "choice",
    [
        choice
        for name, choice in vars(choices).items()
        if (
            isclass(choice)
            and issubclass(choice, IntegerChoices)
            and not name.startswith("_")
            and name.endswith("Detection")
        )
    ],
)
def test_all_detection_values_are_coherent(choice):
    # All elements in the choice class must also exist in the AllDetection class
    for detec in choice:
        expected = AllDetection[detec.name]

        with soft_assertions():
            assert_that(detec.value).is_equal_to(expected.value)
            assert_that(detec.name).is_equal_to(expected.name)
            assert_that(detec.label).is_equal_to(expected.label)


def test_no_detection_value_has_gone_missing():
    """
    This test check if no values has been removed from the detection method.

    This happened sometimes in 2019~2020 due to faulty code refactoring and must be
    avoided.

    This test MUST be updated when a new detection method is added!
    """

    expected = {
        "Radial Velocity",
        "Timing",
        "Controversial",
        "Microlensing",
        "Imaging",
        "Primary Transit",
        "Astrometry",
        "TTV",
        "Other",
        "Spectrum",
        "Theoretical",
        "Flux",
        "Secondary Transit",
        "IR Excess",
        "Kinematic",
    }

    actual = {detec.label for detec in AllDetection}

    assert_that(actual).is_equal_to(expected)


@pytest.mark.parametrize(
    "custom_class",
    [
        param(choices.PublicationType, id="PublicationType class"),
        param(choices.ResearchType, id="ResearchType class"),
        param(choices.PlanetStatus, id="PlanetStatus class"),
        param(choices.AllDetection, id="AllDetection class"),
        param(choices.PlanetDetection, id="PlanetDetection class"),
        param(choices.MassMeasurement, id="MassMeasurement class"),
        param(choices.Star2PubParam, id="Star2PubParam class"),
        param(choices.Plan2PubParam, id="Plan2PubParam class"),
        param(choices.MassUnit, id="MassUnit class"),
        param(choices.RadiusUnit, id="RadiusUnit class"),
    ],
)
@pytest.mark.parametrize(
    "inputs, expected",
    [
        param([1], [True], id="success 1 valid input"),
        param([1, 2], [True, True], id="success multi valid inputs"),
        param([999], [False], id="fails 1 invalid input"),
        param([999, 99], [False, False], id="fails multi invalid inputs"),
        param([1, 999], [True, False], id="fails multi valid and invalid inputs"),
    ],
)
def test_all_detection_number_of_custom_integer_choices(custom_class, inputs, expected):
    assert_that(len(inputs)).is_equal_to(len(expected))

    results = [element in custom_class.all_values_number() for element in inputs]

    assert_that(results).is_equal_to(expected)


@pytest.mark.parametrize(
    "custom_class, inputs, expected",
    [
        # WebStatus
        param(
            choices.WebStatus,
            [1],
            [True],
            id="WebStatus class-success 1 valid input",
        ),
        param(
            choices.WebStatus,
            [1, 3],
            [True, True],
            id="WebStatus class-success multi valid inputs",
        ),
        param(
            choices.WebStatus,
            [999],
            [False],
            id="WebStatus class-fails 1 invalid input",
        ),
        param(
            choices.WebStatus,
            [99, 9999],
            [False, False],
            id="WebStatus class-fails multi invalid inputs",
        ),
        param(
            choices.WebStatus,
            [1, 999],
            [True, False],
            id="WebStatus class-fails multi valid and invalid inputs",
        ),
        # RadiusMeasurement
        param(
            choices.RadiusMeasurement,
            [6],
            [True],
            id="RadiusMeasurement class-success 1 valid input",
        ),
        param(
            choices.RadiusMeasurement,
            [6, 11],
            [True, True],
            id="RadiusMeasurement class-success multi valid inputs",
        ),
        param(
            choices.RadiusMeasurement,
            [999],
            [False],
            id="RadiusMeasurement class-fails 1 invalid input",
        ),
        param(
            choices.RadiusMeasurement,
            [99, 999],
            [False, False],
            id="RadiusMeasurement class-fails multi invalid inputs",
        ),
        param(
            choices.RadiusMeasurement,
            [6, 999],
            [True, False],
            id="RadiusMeasurement class-fails multi valid and invalid inputs",
        ),
        # DetectedDiscDetection
        param(
            choices.DetectedDiscDetection,
            [5],
            [True],
            id="DetectedDiscDetection class-success 1 valid input",
        ),
        param(
            choices.DetectedDiscDetection,
            [5, 14],
            [True, True],
            id="DetectedDiscDetection class-success multi valid inputs",
        ),
        param(
            choices.DetectedDiscDetection,
            [999],
            [False],
            id="DetectedDiscDetection class-fails 1 invalid input",
        ),
        param(
            choices.DetectedDiscDetection,
            [99, 999],
            [False, False],
            id="DetectedDiscDetection class-fails multi invalid inputs",
        ),
        param(
            choices.DetectedDiscDetection,
            [5, 999],
            [True, False],
            id="DetectedDiscDetection class-fails multi valid and invalid inputs",
        ),
    ],
)
def test_all_detection_number_of_custom_integer_choices_specific_cases(
    custom_class,
    inputs,
    expected,
):
    assert_that(len(inputs)).is_equal_to(len(expected))

    results = [element in custom_class.all_values_number() for element in inputs]

    assert_that(results).is_equal_to(expected)


@pytest.mark.parametrize(
    "custom_class, expected",
    [
        param(
            choices.PublicationType,
            ", ".join(str(elem) for elem in range(1, 8)),
            id="PublicationType",
        ),
        param(
            choices.ResearchType,
            ", ".join(str(elem) for elem in range(1, 3)),
            id="ResearchType",
        ),
        param(
            choices.PlanetStatus,
            ", ".join(str(elem) for elem in range(1, 6)),
            id="PlanetStatus",
        ),
        param(choices.WebStatus, "1, 3, 4, 5", id="WebStatus"),
        param(
            choices.AllDetection,
            ", ".join(str(elem) for elem in range(1, 16)),
            id="AllDetection",
        ),
        param(
            choices.PlanetDetection,
            "1, 2, 4, 5, 6, 7, 8, 9, 13, 15",
            id="PlanetDetection",
        ),
        param(
            choices.MassMeasurement, "1, 2, 3, 4, 7, 8, 10, 11", id="MassMeasurement"
        ),
        param(choices.RadiusMeasurement, "6, 11, 12", id="RadiusMeasurement"),
        param(choices.DetectedDiscDetection, "5, 14", id="DetectedDiscDetection"),
        param(
            choices.Star2PubParam,
            ", ".join(str(elem) for elem in range(0, 9)),
            id="Star2PubParam",
        ),
        param(
            choices.Plan2PubParam,
            ", ".join(str(elem) for elem in range(0, 22)),
            id="Plan2PubParam",
        ),
        param(
            choices.MassUnit,
            ", ".join(str(elem) for elem in range(1, 3)),
            id="MassUnit",
        ),
        param(
            choices.RadiusUnit,
            ", ".join(str(elem) for elem in range(1, 3)),
            id="RadiusUnit",
        ),
    ],
)
def test_get_avaible_value_of_custom_integer_choices(custom_class, expected):
    assert custom_class.get_numeric_avaible_value() == expected


def _randomize_case(source: str) -> str:
    """Return a case randomized copy of a string."""
    return "".join(random.choice((str.upper, str.lower))(char) for char in source)


def _randomize_spaces(source: str) -> str:
    """Return a case randomized copy of a string."""
    return "".join(
        char if char != " " else random.choice((" ", "_")) for char in source
    )


@st.composite
def _randomized_name_and_expected_value_planet_detection(
    draw: st.DrawFn,
) -> tuple[str, PlanetDetection]:
    """Generate a randomized case version of PlanetDetection name."""
    detection = draw(st.sampled_from(PlanetDetection))
    approx_name_by_case = _randomize_case(detection.label)
    approx_name = _randomize_spaces(approx_name_by_case)

    return approx_name, detection


@given(approx_and_expected=_randomized_name_and_expected_value_planet_detection())
@example(("Timing", PlanetDetection.TIMING)).via("single word")
@example(("TiMiNg", PlanetDetection.TIMING)).via("single word")
@example(("Radial Velocity", PlanetDetection.RADIAL_VELOCITY)).via("two words")
@example(("Radial_Velocity", PlanetDetection.RADIAL_VELOCITY)).via("substitued space")
@example(("radial velocity", PlanetDetection.RADIAL_VELOCITY)).via("all lowercase")
@example(("RADIAL VELOCITY", PlanetDetection.RADIAL_VELOCITY)).via("all uppercase")
@example(("TTV", PlanetDetection.TTV)).via("acronym all uppercase")
@example(("ttv", PlanetDetection.TTV)).via("acronym all lowercase")
def test_choices_value_from_approx_name_works_as_expected(
    approx_and_expected: tuple[str, PlanetDetection]
):
    approx, expected_planet_detection = approx_and_expected

    val = value_from_approx_name(PlanetDetection, approx)

    assert_that(val).is_equal_to(expected_planet_detection.value)
