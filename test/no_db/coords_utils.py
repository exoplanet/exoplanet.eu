"""Useful constants and hypothesis strategies used by RA and DEC coords tests."""

# Standard imports
from decimal import Decimal

# Test imports
from hypothesis import strategies as st

PRECISION_EPSILON = 16
EPSILON = Decimal(f"1.e-{PRECISION_EPSILON}")

# Hypothesis strategies
VALID_HOURS = st.integers(min_value=0, max_value=23)
FULL_VALID_HOUR = st.integers(min_value=0, max_value=24)
VALID_MINUTES = st.integers(min_value=0, max_value=59)
VALID_SECONDS = VALID_MINUTES
FULL_VALID_MINUTES_SECONDES = st.integers(min_value=0, max_value=60)
VALID_SECONDS_FLOAT = st.floats(
    min_value=0.0,
    max_value=60.0,
    exclude_max=True,
    allow_infinity=False,
    allow_nan=False,
)
VALID_HOURS_DIGITS = st.sampled_from([1, 2])
VALID_MINUTES_DIGITS = st.sampled_from([1, 2])
VALID_SECONDS_DIGITS = st.sampled_from([1, 2])
VALID_SECONDS_DIGITS_FLOAT = st.sampled_from([1, 2, 3])
VALID_PLUS_SIGN = st.sampled_from(["+", ""])
VALID_DEGREES = st.integers(min_value=-360, max_value=360)
VALID_RA_DEGREES = st.floats(
    min_value=0.0,
    max_value=360.0,
    exclude_max=True,
    allow_infinity=False,
    allow_nan=False,
)
VALID_DEC_DEGREES = st.integers(min_value=-90.0, max_value=90.0)
VALID_DEGREES_MINUS_90_90 = VALID_DEC_DEGREES
VALID_DEC_DEGREES_DIGITS = st.sampled_from([1, 2])
INVALID_HOURS = st.integers(max_value=-1) | st.integers(min_value=24)
INVALID_MINUTES = st.integers(max_value=-1) | st.integers(min_value=60)
INVALID_SECONDS = st.integers(max_value=-1) | st.integers(min_value=60)
INVALID_SECONDS_FLOAT = (
    st.floats(
        max_value=0.0,
        exclude_max=True,
        allow_infinity=False,
    )
    | st.floats(
        min_value=60.0,
        exclude_min=True,
        allow_infinity=False,
    )
    | st.just(float("nan"))
)
INVALID_DEC_DEGREES = st.integers(max_value=-91) | st.integers(min_value=91)
INVALID_RA_FULL_DEGREES = (
    st.floats(
        max_value=0.0,
        exclude_max=True,
        allow_infinity=True,
    )
    | st.floats(
        min_value=360.0,
        exclude_min=False,
        allow_infinity=True,
    )
    | st.just(float("nan"))
)
INVALID_DEC_FULL_DEGREES = (
    st.floats(
        max_value=-90.0,
        exclude_max=True,
        allow_infinity=True,
    )
    | st.floats(
        min_value=90.0,
        exclude_min=True,
        allow_infinity=True,
    )
    | st.just(float("nan"))
)
