# Pytest imports
# Test imports
import pytest
from assertpy import assert_that
from hypothesis import given, settings
from hypothesis.strategies import characters, from_regex, integers

# First party imports
from core.models.utils import (
    DEFAULT_NAME_ID_SEPARATOR,
    DEFAULT_PLACEHOLDER,
    obj_slugify,
    safe_name,
)

_VALID_RAW_PLANET_NAME = r"^.*[a-zA-Z]+.*$"
_VALID_SAFE_OBJ_NAME_REGEX = r"^[0-9a-z_]+$"
_VALID_SLUG_REGEX = r"^([0-9a-z_]+)--([0-9]+)$"
_REGEX_SPECIAL_CHARS = r".^$*+?{}[]\|()"


def _replace_char_in_regex(regex_str: str, from_: str, to: str) -> str:
    if to in _REGEX_SPECIAL_CHARS:
        return regex_str.replace(from_, f"\\{to}")
    else:
        return regex_str.replace(from_, to)


@pytest.mark.parametrize(
    "raw_name, expected",
    [
        ("Berkeley 19", "berkeley_19"),
        ("--ABC____DEF--", "abc_def"),
        ("  X Y   Z   ", "x_y_z"),
        ("影師嗎", "ying_shi_ma"),
        ("ʻOumuamua", "oumuamua"),
    ],
)
def test_safe_name_works_on_basic_examples(raw_name: str, expected: str):
    assert_that(safe_name(raw_name)).is_equal_to(expected)


@given(raw_name=from_regex(_VALID_RAW_PLANET_NAME))
def test_safe_name_generate_valid_names_by_default(raw_name: str):
    (
        assert_that(safe_name(raw_name))
        .is_instance_of(str)
        .is_not_empty()
        .is_lower()
        .does_not_contain("-")
        .does_not_contain(" ")
        .matches(_VALID_SAFE_OBJ_NAME_REGEX)
    )


# We disable deadline since generation with from_regex and char categories is long
@settings(deadline=None)
@given(
    raw_name=from_regex(_VALID_RAW_PLANET_NAME),
    placeholder=characters(
        whitelist_categories=("L", "Nd", "P"), blacklist_characters=["\\"]
    ),
)
def test_safe_name_generate_valid_names_with_placeholder(
    raw_name: str, placeholder: str
):
    valid_safe_obj_name_regex_custom = _replace_char_in_regex(
        regex_str=_VALID_SAFE_OBJ_NAME_REGEX,
        from_=DEFAULT_PLACEHOLDER,
        to=placeholder,
    )

    (
        assert_that(safe_name(raw_name, placeholder))
        .is_instance_of(str)
        .is_not_empty()
        .matches(valid_safe_obj_name_regex_custom)
    )


@given(
    raw_name=from_regex(_VALID_RAW_PLANET_NAME),
    obj_id=integers(min_value=1),
)
def test_obj_slugify_generate_valid_slugs_by_default(raw_name: str, obj_id: int):
    slug = obj_slugify(raw_name, obj_id)

    (
        assert_that(slug)
        .is_instance_of(str)
        .is_not_empty()
        .is_lower()
        .starts_with(safe_name(raw_name))
        .ends_with(str(obj_id))
        .contains("--")
        .does_not_contain(" ")
        .matches(_VALID_SLUG_REGEX)
    )


# We disable deadline since generation with from_regex and char categories is long
@given(
    raw_name=from_regex(_VALID_RAW_PLANET_NAME),
    obj_id=integers(min_value=1),
    placeholder=characters(
        whitelist_categories=("L", "Nd", "P"), blacklist_characters=["-"]
    ),
)
@settings(deadline=None)
def test_obj_slugify_generate_valid_slugs_with_custom_separators(
    raw_name: str,
    obj_id: int,
    placeholder: str,
):
    slug = obj_slugify(raw_name, obj_id, placeholder)
    safe_obj_name = safe_name(raw_name, placeholder)

    valid_slug_regex_custom = _replace_char_in_regex(
        regex_str=_VALID_SLUG_REGEX,
        from_=DEFAULT_PLACEHOLDER,
        to=placeholder,
    )

    (
        assert_that(slug)
        .is_instance_of(str)
        .is_not_empty()
        .starts_with(safe_obj_name)
        .ends_with(str(obj_id))
        .contains(DEFAULT_NAME_ID_SEPARATOR)
        .does_not_contain(" ")
        .matches(valid_slug_regex_custom)
    )
