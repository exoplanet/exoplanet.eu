# Standard imports
import re

# Test imports
import pytest
from assertpy import assert_that
from hypothesis import assume, given, settings
from hypothesis import strategies as st

# Django 💩 imports
from django.core.exceptions import ValidationError

# First party imports
from core.models.coords import (
    DEC_REGEX,
    RA_REGEX,
    dec_string_to_degree_value,
    ra_string_to_degree_value,
)

# Local imports
from . import coords_utils as cu

# [-360; 360]
DEGREES_360_REGEX = re.compile(
    r"^[+-]?(?:36[0]|3[0-5][0-9]|[12][0-9][0-9]|[1-9]?[0-9])?$"
)

# [0;24]
HOUR_REGEX = re.compile(r"^[+]?(?:1?[0-9]|2[0-4])$")

# [-90;90]
ANGLE_90_REGEX = re.compile(r"^[+-]?(?:90|[0-8][0-9]|[0-9])$")

# [0;60]
MINUTES_OR_SECOND_REGEX = re.compile(r"^[+]?(?:60|[0-5][0-9]|[0-9])$")


@given(degrees=cu.VALID_DEGREES)
@settings(max_examples=400)
def test_valid_degrees_regex_minus_360_360(degrees):
    assert_that(re.match(DEGREES_360_REGEX, str(degrees))).is_not_none()


@given(hour=cu.FULL_VALID_HOUR)
@settings(max_examples=24)
def test_valid_hour_regex_0_24(hour):
    assert_that(re.match(HOUR_REGEX, str(hour))).is_not_none()


@given(min_or_second_integer=cu.FULL_VALID_MINUTES_SECONDES)
@settings(max_examples=61)
def test_valid_min_or_second_0_60(min_or_second_integer):
    assert_that(
        re.match(MINUTES_OR_SECOND_REGEX, str(min_or_second_integer))
    ).is_not_none()


@given(minus_90_90_deg=cu.VALID_DEGREES_MINUS_90_90)
@settings(max_examples=181)
def test_valid_Degrees_minus_90_90(minus_90_90_deg):
    assert_that(re.match(ANGLE_90_REGEX, str(minus_90_90_deg))).is_not_none()


# RA


@given(
    hours=cu.VALID_HOURS,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_regex_hms_for_valid(
    hours,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    hours_str = f"{hours:{plus_sign}0{hours_digits}}"
    minutes_str = f"{minutes:0{minutes_digits}}"
    seconds_str = f"{seconds:0{seconds_digits}}"
    hms_str = f"{hours_str}:{minutes_str}:{seconds_str}"

    match = re.match(RA_REGEX, hms_str)

    assert_that(match).is_not_none()
    assert_that(match.group("RAh")).is_equal_to(hours_str)
    assert_that(match.group("RAm")).is_equal_to(minutes_str)
    assert_that(match.group("RAs")).is_equal_to(seconds_str)
    assert_that(match.group("RAdeg")).is_none()


@given(
    hours=cu.VALID_HOURS,
    minutes=cu.VALID_MINUTES,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_regex_hms_for_valid_without_seconds(
    hours,
    minutes,
    hours_digits,
    minutes_digits,
    plus_sign,
):
    hours_str = f"{hours:{plus_sign}0{hours_digits}}"
    minutes_str = f"{minutes:0{minutes_digits}}"
    hms_str = f"{hours_str}:{minutes_str}"

    match = re.match(RA_REGEX, hms_str)

    assert_that(match).is_not_none()
    assert_that(match.group("RAh")).is_equal_to(hours_str)
    assert_that(match.group("RAm")).is_equal_to(minutes_str)
    assert_that(match.group("RAs")).is_none()
    assert_that(match.group("RAdeg")).is_none()


@given(
    hours=cu.VALID_HOURS,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS_FLOAT,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_regex_hms_for_valid_with_float_seconds(
    hours,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # The 1 one is to take account of the decimal point in the width
    sec_width = cu.PRECISION_EPSILON + seconds_digits

    hours_str = f"{hours:{plus_sign}0{hours_digits}}"
    minutes_str = f"{minutes:0{minutes_digits}}"
    seconds_str = f"{seconds:0{sec_width}.{cu.PRECISION_EPSILON}f}"
    hms_str = f"{hours_str}:{minutes_str}:{seconds_str}"

    match = re.match(RA_REGEX, hms_str)

    assert_that(match).is_not_none()
    assert_that(match.group("RAh")).is_equal_to(hours_str)
    assert_that(match.group("RAm")).is_equal_to(minutes_str)
    assert_that(match.group("RAs")).is_equal_to(seconds_str)
    assert_that(match.group("RAdeg")).is_none()


@given(candidate=st.text())
def test_ra_string_to_degree_value_fail_with_random_string(candidate):
    assume(not re.match(RA_REGEX, candidate))

    with pytest.raises(ValidationError):
        _ = ra_string_to_degree_value(candidate)


# DEC


@given(
    degrees=cu.VALID_DEC_DEGREES,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS,
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_regex_dms_for_valid(
    degrees,
    minutes,
    seconds,
    degrees_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    degrees_str = f"{degrees:{plus_sign}0{degrees_digits}}"
    minutes_str = f"{minutes:0{minutes_digits}}"
    seconds_str = f"{seconds:0{seconds_digits}}"
    dms_str = f"{degrees_str}:{minutes_str}:{seconds_str}"

    match = re.match(DEC_REGEX, dms_str)

    assert_that(match).is_not_none()
    assert_that(match.group("DECd")).is_equal_to(degrees_str)
    assert_that(match.group("DECm")).is_equal_to(minutes_str)
    assert_that(match.group("DECs")).is_equal_to(seconds_str)
    assert_that(match.group("DECdeg")).is_none()


@given(
    degrees=cu.VALID_DEC_DEGREES,
    minutes=cu.VALID_MINUTES,
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_regex_dms_for_valid_without_seconds(
    degrees,
    minutes,
    degrees_digits,
    minutes_digits,
    plus_sign,
):
    degrees_str = f"{degrees:{plus_sign}0{degrees_digits}}"
    minutes_str = f"{minutes:0{minutes_digits}}"
    dms_str = f"{degrees_str}:{minutes_str}"

    match = re.match(DEC_REGEX, dms_str)

    assert_that(match).is_not_none()
    assert_that(match.group("DECd")).is_equal_to(degrees_str)
    assert_that(match.group("DECm")).is_equal_to(minutes_str)
    assert_that(match.group("DECs")).is_none()
    assert_that(match.group("DECdeg")).is_none()


@given(
    degrees=cu.VALID_DEC_DEGREES,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS_FLOAT,
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_regex_dms_for_valid_with_float_seconds(
    degrees,
    minutes,
    seconds,
    degrees_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # The 1 one is to take account of the decimal point in the width
    sec_width = cu.PRECISION_EPSILON + seconds_digits

    degrees_str = f"{degrees:{plus_sign}0{degrees_digits}}"
    minutes_str = f"{minutes:0{minutes_digits}}"
    seconds_str = f"{seconds:0{sec_width}.{cu.PRECISION_EPSILON}f}"
    dms_str = f"{degrees_str}:{minutes_str}:{seconds_str}"

    match = re.match(DEC_REGEX, dms_str)

    assert_that(match).is_not_none()
    assert_that(match.group("DECd")).is_equal_to(degrees_str)
    assert_that(match.group("DECm")).is_equal_to(minutes_str)
    assert_that(match.group("DECs")).is_equal_to(seconds_str)
    assert_that(match.group("DECdeg")).is_none()


@given(candidate=st.text())
def test_dec_string_to_degree_value_fail_with_random_string(candidate):
    assume(not re.match(DEC_REGEX, candidate))

    with pytest.raises(ValidationError):
        _ = dec_string_to_degree_value(candidate)
