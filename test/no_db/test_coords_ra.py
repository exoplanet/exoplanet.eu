"""Tests for the RA coordinates input convertion and validation."""

# Standard imports
from decimal import Decimal

# Test imports
import pytest
from assertpy import assert_that
from hypothesis import given, settings

# Django 💩 imports
from django.core.exceptions import ValidationError

# First party imports
from core.models.coords import ra_string_to_degree_value

# Local imports
from . import coords_utils as cu


@given(
    hours=cu.VALID_HOURS,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_string_to_degree_value_works_with_integers_hms(
    hours,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    hms_str = (
        f"{hours:{plus_sign}0{hours_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{seconds_digits}}"
    )
    expected_hms_to_degrees = (
        Decimal(hours) * Decimal(360) / Decimal(24)
        + Decimal(minutes) * Decimal(360) / Decimal(24) / Decimal(60)
        + Decimal(seconds) * Decimal(360) / Decimal(24) / Decimal(60) / Decimal(60)
    )

    degrees = ra_string_to_degree_value(hms_str)

    (
        assert_that(degrees)
        .is_type_of(Decimal)
        .is_close_to(expected_hms_to_degrees, cu.EPSILON)
    )


@given(
    hours=cu.VALID_HOURS,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS_FLOAT,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_string_to_degree_value_works_with_floats_seconds_hms(
    hours,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # The 1 one is to take account of the decimal point in the width
    sec_width = cu.PRECISION_EPSILON + seconds_digits

    hms_str = (
        f"{hours:{plus_sign}0{hours_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{sec_width}.{cu.PRECISION_EPSILON}f}"
    )
    expected_hms_to_degrees = (
        Decimal(hours) * Decimal(360) / Decimal(24)
        + Decimal(minutes) * Decimal(360) / Decimal(24) / Decimal(60)
        + Decimal(seconds) * Decimal(360) / Decimal(24) / Decimal(60) / Decimal(60)
    )

    degrees = ra_string_to_degree_value(hms_str)

    (
        assert_that(degrees)
        .is_type_of(Decimal)
        .is_close_to(expected_hms_to_degrees, cu.EPSILON)
    )


@given(
    degrees=cu.VALID_RA_DEGREES,
    plus_sign=cu.VALID_PLUS_SIGN,
)
def test_ra_string_to_degree_value_works_with_floats_degrees(degrees, plus_sign):
    degrees_str = f"{degrees:{plus_sign}019.16f}"
    expected = Decimal(degrees)

    degrees_result = ra_string_to_degree_value(degrees_str)

    assert_that(degrees_result).is_type_of(Decimal).is_close_to(expected, cu.EPSILON)


@given(degrees=cu.INVALID_RA_FULL_DEGREES)
def test_ra_string_to_degree_value_fail_when_out_of_range_degree(degrees):
    degrees_str = str(degrees)

    with pytest.raises(ValidationError):
        _ = ra_string_to_degree_value(degrees_str)


@given(
    hours=cu.INVALID_HOURS,
    minutes=cu.INVALID_MINUTES,
    seconds=cu.INVALID_SECONDS,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_string_to_degree_value_fail_when_out_of_range_integer_hms(
    hours,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    hms_str = (
        f"{hours:{plus_sign}0{hours_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{seconds_digits}}"
    )

    with pytest.raises(ValidationError):
        _ = ra_string_to_degree_value(hms_str)


@given(
    hours=cu.INVALID_HOURS,
    minutes=cu.INVALID_MINUTES,
    seconds=cu.INVALID_SECONDS_FLOAT,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_ra_string_to_degree_value_fail_when_out_of_range_float_hms(
    hours,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # The 1 one is to take account of the decimal point in the width
    sec_width = cu.PRECISION_EPSILON + seconds_digits

    hms_str = (
        f"{hours:{plus_sign}0{hours_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{sec_width}.{cu.PRECISION_EPSILON}f}"
    )

    with pytest.raises(ValidationError):
        _ = ra_string_to_degree_value(hms_str)
