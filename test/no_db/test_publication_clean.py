# Standard imports
from datetime import datetime

# Test imports
import pytest
from pytest import param

# Django 💩 imports
from django.core.exceptions import ValidationError

# First party imports
from core.models import Publication


@pytest.mark.parametrize(
    "date",
    [
        param("Un monde en pièce", id="date is not a year"),
        param("1699", id="date lesser then 1700"),
        param(str(datetime.now().year + 2), id="date is greater then now().year + 1"),
    ],
)
def test_publication_date_wrong_format(date):
    new_pub = Publication(
        title="Un monde en pièce", author="Gaspard Gry & Ulysse Gry", date=date, type=1
    )
    with pytest.raises(ValidationError):
        new_pub.full_clean()
