# pylint: disable=no-name-in-module

# Test imports
import pytest
from assertpy import assert_that, soft_assertions

# First party imports
from core.catalog_field_def import (
    ALBEDO_FIELD,
    ALTERNATE_NAMES_FIELD,
    ANGULAR_DISTANCE_FIELD,
    AXIS_FIELD,
    CATALOG_ALL_FIELDS,
    CATALOG_COMPACT_FIELDS,
    DETECTION_TYPE_FIELD,
    DISCOVERED_FIELD,
    ECCENTRICITY_FIELD,
    EPHEMERIDES_FIELDS,
    HISTOGRAM_VAR_PLANET_FIELDS,
    HISTOGRAM_VAR_STARS_FIELDS,
    HOT_POINT_LON_FIELD,
    IMPACT_PARAMETER_FIELD,
    INCLINATION_FIELD,
    K_FIELD,
    LAMBDA_ANGLE_FIELD,
    LOG_G_FIELD,
    MASS_FIELD,
    MASS_MEASUREMENT_TYPE_FIELD,
    MASS_SIN_I_FIELD,
    MASS_SIN_I_FLAG_FIELD,
    MODIFIED_FIELD,
    MOLECULES_FIELD,
    NAME_FIELD,
    OMEGA_FIELD,
    OUTPUT_ALL_FIELDS,
    OUTPUT_COMPACT_FIELDS,
    PERIOD_FIELD,
    PLANET_FIELDS,
    PLANET_PLANETARY_SYSTEM_FIELDS,
    PLANET_STAR_FIELDS,
    PLANET_STATUS,
    POLARPLOT_FIELD,
    POLARPLOT_VAR_FIELD,
    PUBLICATION_FIELD,
    RADIUS_FIELD,
    RADIUS_MEASUREMENT_TYPE_FIELD,
    STAR_AGE_FIELD,
    STAR_ALTERNATE_NAMES_FIELD,
    STAR_DEC_FIELD,
    STAR_DETECTED_DISC_FIELD,
    STAR_DISTANCE_FIELD,
    STAR_MAGNETIC_FIELD,
    STAR_MAGNITUDE_H_FIELD,
    STAR_MAGNITUDE_I_FIELD,
    STAR_MAGNITUDE_J_FIELD,
    STAR_MAGNITUDE_K_FIELD,
    STAR_MAGNITUDE_V_FIELD,
    STAR_MASS_FIELD,
    STAR_METALLICITY_FIELD,
    STAR_NAME_FIELD,
    STAR_RA_FIELD,
    STAR_RADIUS_FIELD,
    STAR_SPEC_TYPE_FIELD,
    STAR_TEFF_FIELD,
    TCONJ_FIELD,
    TEMP_CALCULATED_FIELD,
    TEMP_MEASURED_FIELD,
    TPERI_FIELD,
    TZERO_TR_FIELD,
    TZERO_TR_SEC_FIELD,
    TZERO_VR_FIELD,
    FieldDefinitionGroup,
)

# pylint: enable=no-name-in-module


# Since we can not directly test with isinstance on a TypedDict
# See: https://www.python.org/dev/peps/pep-0589/#id16
FieldDefType = dict


@pytest.mark.parametrize(
    "field_def",
    [
        NAME_FIELD,
        MASS_FIELD,
        MASS_SIN_I_FIELD,
        MASS_SIN_I_FLAG_FIELD,
        RADIUS_FIELD,
        PERIOD_FIELD,
        AXIS_FIELD,
        ECCENTRICITY_FIELD,
        INCLINATION_FIELD,
        ANGULAR_DISTANCE_FIELD,
        PUBLICATION_FIELD,
        DISCOVERED_FIELD,
        MODIFIED_FIELD,
        OMEGA_FIELD,
        TPERI_FIELD,
        TCONJ_FIELD,
        TZERO_TR_FIELD,
        TZERO_TR_SEC_FIELD,
        LAMBDA_ANGLE_FIELD,
        IMPACT_PARAMETER_FIELD,
        TZERO_VR_FIELD,
        K_FIELD,
        TEMP_CALCULATED_FIELD,
        TEMP_MEASURED_FIELD,
        HOT_POINT_LON_FIELD,
        ALBEDO_FIELD,
        LOG_G_FIELD,
        DETECTION_TYPE_FIELD,
        MASS_MEASUREMENT_TYPE_FIELD,
        RADIUS_MEASUREMENT_TYPE_FIELD,
        ALTERNATE_NAMES_FIELD,
        MOLECULES_FIELD,
        PLANET_STATUS,
        STAR_NAME_FIELD,
        STAR_RA_FIELD,
        STAR_DEC_FIELD,
        STAR_MAGNITUDE_V_FIELD,
        STAR_MAGNITUDE_I_FIELD,
        STAR_MAGNITUDE_J_FIELD,
        STAR_MAGNITUDE_H_FIELD,
        STAR_MAGNITUDE_K_FIELD,
        STAR_DISTANCE_FIELD,
        STAR_METALLICITY_FIELD,
        STAR_MASS_FIELD,
        STAR_RADIUS_FIELD,
        STAR_SPEC_TYPE_FIELD,
        STAR_AGE_FIELD,
        STAR_TEFF_FIELD,
        STAR_DETECTED_DISC_FIELD,
        STAR_MAGNETIC_FIELD,
        STAR_ALTERNATE_NAMES_FIELD,
    ],
)
def test_catalog_field_def_yaml_works_for_definitions(field_def):
    assert_that(field_def).is_instance_of(FieldDefType)


@pytest.mark.parametrize(
    "definition_list",
    [
        OUTPUT_COMPACT_FIELDS,
        OUTPUT_ALL_FIELDS,
        CATALOG_COMPACT_FIELDS,
        CATALOG_ALL_FIELDS,
        PLANET_FIELDS,
        PLANET_STAR_FIELDS,
        PLANET_PLANETARY_SYSTEM_FIELDS,
        EPHEMERIDES_FIELDS,
        POLARPLOT_FIELD,
        POLARPLOT_VAR_FIELD,
        HISTOGRAM_VAR_PLANET_FIELDS,
        HISTOGRAM_VAR_STARS_FIELDS,
    ],
)
def test_catalog_field_def_yaml_works_for_definition_lists(definition_list):
    # pylint: disable=no-member
    assert_that(definition_list).is_instance_of(FieldDefinitionGroup.__origin__)

    with soft_assertions():
        for field_def in definition_list:
            assert_that(field_def).is_instance_of(FieldDefType)
