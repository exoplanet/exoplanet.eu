"""Tests for the DEC coordinates input convertion and validation."""

# Standard imports
from decimal import Decimal

# Test imports
import pytest
from assertpy import assert_that
from hypothesis import assume, example, given, settings
from hypothesis import strategies as st

# Django 💩 imports
from django.core.exceptions import ValidationError

# First party imports
from core.models.coords import dec_string_to_degree_value

# Local imports
from . import coords_utils as cu


@given(
    degrees=cu.VALID_DEC_DEGREES,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS,
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@example(
    # example given by Quentin Kral that was crashing the server
    degrees=60,
    minutes=47,
    seconds=14,
    degrees_digits=2,
    minutes_digits=2,
    seconds_digits=2,
    plus_sign="",
)
@example(
    # example given by Pierre-Yves Martin that was crashing the server
    degrees=-60,
    minutes=47,
    seconds=14,
    degrees_digits=2,
    minutes_digits=2,
    seconds_digits=2,
    plus_sign="",
)
@example(
    # example given by Pierre-Yves Martin that was crashing the server
    degrees=24,
    minutes=47,
    seconds=14,
    degrees_digits=2,
    minutes_digits=2,
    seconds_digits=2,
    plus_sign="",
)
@example(
    # example given by Pierre-Yves Martin that was crashing the server
    degrees=-24,
    minutes=47,
    seconds=14,
    degrees_digits=2,
    minutes_digits=2,
    seconds_digits=2,
    plus_sign="",
)
@example(
    # example given by Jean Schneider that was crashing the server
    degrees=8,
    minutes=50,
    seconds=58,
    degrees_digits=2,
    minutes_digits=2,
    seconds_digits=2,
    plus_sign="",
)
@settings(max_examples=400)
def test_dec_string_to_degree_value_works_with_integers_dms(
    degrees,
    minutes,
    seconds,
    degrees_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # To simplify testing we handle these separately
    assume(degrees != Decimal("-90"))
    assume(degrees != Decimal("90"))

    dms_str = (
        f"{degrees:{plus_sign}0{degrees_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{seconds_digits}}"
    )
    expected_dms_to_degrees = (
        Decimal(degrees)
        + Decimal(minutes).copy_sign(Decimal(degrees)) / Decimal(60)
        + Decimal(seconds).copy_sign(Decimal(degrees)) / Decimal(60) / Decimal(60)
    )

    full_degrees = dec_string_to_degree_value(dms_str)

    assert_that(full_degrees).is_type_of(Decimal).is_close_to(
        expected_dms_to_degrees,
        cu.EPSILON,
    )


@given(
    degrees=cu.VALID_DEC_DEGREES,
    minutes=cu.VALID_MINUTES,
    seconds=cu.VALID_SECONDS_FLOAT,
    hours_digits=cu.VALID_HOURS_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_string_to_degree_value_works_with_floats_seconds_hms(
    degrees,
    minutes,
    seconds,
    hours_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # To simplify testing we handle these separately
    assume(degrees != Decimal("-90"))
    assume(degrees != Decimal("90"))

    # The 1 one is to take account of the decimal point in the width
    sec_width = cu.PRECISION_EPSILON + seconds_digits

    dms_str = (
        f"{degrees:{plus_sign}0{hours_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{sec_width}.{cu.PRECISION_EPSILON}f}"
    )
    expected_dms_to_degrees = (
        Decimal(degrees)
        + Decimal(minutes).copy_sign(Decimal(degrees)) / Decimal(60)
        + Decimal(seconds).copy_sign(Decimal(degrees)) / Decimal(60) / Decimal(60)
    )

    full_degrees = dec_string_to_degree_value(dms_str)

    (
        assert_that(full_degrees)
        .is_type_of(Decimal)
        .is_close_to(expected_dms_to_degrees, cu.EPSILON)
    )


@given(
    degrees=st.sampled_from([Decimal("-90"), Decimal("90")]),
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_string_to_degree_value_works_with_integers_limits_dms(
    degrees,
    degrees_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    minutes = "0"
    seconds = "0"

    dms_str = (
        f"{degrees:{plus_sign}0{degrees_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{seconds_digits}}"
    )
    expected_dms_to_degrees = (
        Decimal(degrees)
        + Decimal(minutes).copy_sign(Decimal(degrees)) / Decimal(60)
        + Decimal(seconds).copy_sign(Decimal(degrees)) / Decimal(60) / Decimal(60)
    )

    full_degrees = dec_string_to_degree_value(dms_str)

    (
        assert_that(full_degrees)
        .is_type_of(Decimal)
        .is_close_to(expected_dms_to_degrees, cu.EPSILON)
    )


@given(
    # example given bien Jean Schneider that was crashing the server
    degrees=cu.VALID_DEC_DEGREES,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@example(
    degrees=-16.75583333333333,
    plus_sign="",
)
def test_dec_string_to_degree_value_works_with_floats_degrees(degrees, plus_sign):
    degrees_str = f"{degrees:{plus_sign}019.16f}"
    expected = Decimal(degrees)

    degrees_result = dec_string_to_degree_value(degrees_str)

    assert_that(degrees_result).is_type_of(Decimal).is_close_to(expected, cu.EPSILON)


@given(degrees=cu.INVALID_DEC_FULL_DEGREES)
def test_dec_string_to_degree_value_fail_when_out_of_range_degree(degrees):
    degrees_str = str(degrees)

    with pytest.raises(ValidationError):
        _ = dec_string_to_degree_value(degrees_str)


@given(
    degrees=cu.INVALID_DEC_DEGREES,
    minutes=cu.INVALID_MINUTES,
    seconds=cu.INVALID_SECONDS,
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_string_to_degree_value_fail_when_out_of_range_integer_hms(
    degrees,
    minutes,
    seconds,
    degrees_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    dms_str = (
        f"{degrees:{plus_sign}0{degrees_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{seconds_digits}}"
    )

    with pytest.raises(ValidationError):
        _ = dec_string_to_degree_value(dms_str)


@given(
    degrees=cu.INVALID_DEC_DEGREES,
    minutes=cu.INVALID_MINUTES,
    seconds=cu.INVALID_SECONDS_FLOAT,
    degrees_digits=cu.VALID_DEC_DEGREES_DIGITS,
    minutes_digits=cu.VALID_MINUTES_DIGITS,
    seconds_digits=cu.VALID_SECONDS_DIGITS_FLOAT,
    plus_sign=cu.VALID_PLUS_SIGN,
)
@settings(max_examples=400)
def test_dec_string_to_degree_value_fail_when_out_of_range_float_hms(
    degrees,
    minutes,
    seconds,
    degrees_digits,
    minutes_digits,
    seconds_digits,
    plus_sign,
):
    # The 1 one is to take account of the decimal point in the width
    sec_width = cu.PRECISION_EPSILON + seconds_digits

    dms_str = (
        f"{degrees:{plus_sign}0{degrees_digits}}"
        f":{minutes:0{minutes_digits}}"
        f":{seconds:0{sec_width}.{cu.PRECISION_EPSILON}f}"
    )

    with pytest.raises(ValidationError):
        _ = dec_string_to_degree_value(dms_str)
