# Standard imports
import re

# Test imports
from assertpy import assert_that
from hypothesis import given
from hypothesis.strategies import from_regex, integers

# First party imports
from catalog_legacy.urls import CATALOG_PLANET_NAME_URL_REGEX
from core.models.planet import PLANET_NAME_MAX_LENGTH
from core.models.utils import obj_slugify

# These regex are taken from exoimport package
_VALID_PLANET_NAME_BASIC = r"^[0-9a-zA-Z-_.()':/`\[\]*]+[0-9a-zA-Z-+_.()':/`\[\]* ]*$"
_VALID_PLANET_NAME_SECOND_CHECK = r"^.*[a-zA-Z]+.*$"


@given(
    planet_name=from_regex(_VALID_PLANET_NAME_BASIC, fullmatch=True)
    .filter(lambda x: len(x) <= PLANET_NAME_MAX_LENGTH)
    .filter(lambda x: re.fullmatch(_VALID_PLANET_NAME_SECOND_CHECK, x)),
    planet_id=integers(min_value=1),
)
def test_catalog_planet_name_url_regex(planet_name: str, planet_id: int):
    # Let's build a slug
    slug = obj_slugify(planet_name, planet_id)

    # ...and make an url with it
    url = f"catalog/{slug}/"

    assert_that(url).matches(CATALOG_PLANET_NAME_URL_REGEX)
