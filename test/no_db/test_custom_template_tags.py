# Standard imports
import math

# Test imports
from assertpy import assert_that
from hypothesis import given
from hypothesis.strategies import floats, lists, none, one_of, sampled_from

# External imports
from bs4 import BeautifulSoup

# First party imports
from core.templatetags.helpers import format_value_with_error


@given(
    errors=one_of(
        none(),
        lists(floats(), min_size=1, max_size=2),
    ),
)
def test_format_value_with_error_returns_empty_string_if_value_is_none(
    errors: list[float],
):
    # First we create the HTML formated value
    formated_val = format_value_with_error(None, errors)

    # Then we retrieve the info from the HTML
    soup = BeautifulSoup(formated_val, features="lxml")
    val_from_formated = soup.select("math mn.value.no-value")

    # Now it's time for the tests !
    assert_that(val_from_formated).is_length(1)
    assert_that(val_from_formated[0].string).is_none()


@given(
    value=floats(),
)
def test_format_value_with_error_returns_just_value_if_error_is_none(value: float):
    # First we create the HTML formated value
    formated_val = format_value_with_error(value, None)

    # Then we retrieve the info from the HTML
    soup = BeautifulSoup(formated_val, features="lxml")
    val_from_formated = soup.select("math mn.value.no-err-value")

    # Now it's time for the tests !
    assert_that(val_from_formated).is_length(1)
    assert_that(val_from_formated[0].string).is_equal_to(str(value))


@given(
    value=floats(),
    single_error=lists(floats(), min_size=1, max_size=1),
)
def test_format_value_with_error_returns_relative_error_if_only_one_error_is_provided(
    value: float, single_error: list[float]
):
    # preconditions
    assert_that(single_error).is_length(1)

    # First we create the HTML formated value
    formated_val = format_value_with_error(value, single_error)

    # Then we retrieve the info from the HTML
    soup = BeautifulSoup(formated_val, features="lxml")
    val_from_formated = soup.select("math mn.value.rel-err-value")
    par_from_formated = soup.select('math mo[fence="true"]')
    plus_minus_from_formated = soup.select("math .err-rel mo")
    rel_err_from_formated = soup.select("math .err-rel mn")

    # Now it's time for the tests !
    assert_that(par_from_formated).is_length(2)
    open_par_op, close_par_op = par_from_formated
    assert_that(open_par_op.string).is_equal_to("(")
    assert_that(close_par_op.string).is_equal_to(")")

    assert_that(plus_minus_from_formated).is_length(1)
    assert_that(plus_minus_from_formated[0].string).is_equal_to("±")

    assert_that(val_from_formated).is_length(1)
    assert_that(val_from_formated[0].string).is_equal_to(str(value))

    assert_that(rel_err_from_formated).is_length(1)
    assert_that(rel_err_from_formated[0].string).is_equal_to(str(single_error[0]))


@given(
    value=floats(),
    double_error=lists(floats(allow_infinity=False), min_size=2, max_size=2),
)
def test_format_value_with_error_returns_both_error_if_two_errors_are_provided(
    value: float, double_error: list[float]
):
    # preconditions
    assert_that(double_error).is_length(2)
    err_min, err_max = double_error

    # First we create the HTML formated value
    formated_val = format_value_with_error(value, double_error)

    # Then we retrieve the info from the HTML
    soup = BeautifulSoup(formated_val, features="lxml")
    val_from_formated = soup.select("math mn.value.both-err-value")
    op_par_from_formated = soup.select("math>mrow>mo")
    op_min_from_formated = soup.select("math .err-min mo")
    err_min_from_formated = soup.select("math .err-min mn")
    op_max_from_formated = soup.select("math .err-max mo")
    err_max_from_formated = soup.select("math .err-max mn")

    # Now it's time for the tests !
    assert_that(val_from_formated).is_length(1)
    assert_that(val_from_formated[0].string).is_equal_to(str(value))

    assert_that(op_par_from_formated).is_length(2)
    op_par_open, op_par_close = op_par_from_formated
    assert_that(op_par_open.string).is_equal_to("(")
    assert_that(op_par_close.string).is_equal_to(")")

    assert_that(op_min_from_formated).is_length(1)
    assert_that(op_min_from_formated[0].string).is_equal_to("-")

    assert_that(op_max_from_formated).is_length(1)
    assert_that(op_max_from_formated[0].string).is_equal_to("+")

    assert_that(err_min_from_formated).is_length(1)
    assert_that(err_min_from_formated[0].string).is_equal_to(str(err_min))
    assert_that(err_max_from_formated).is_length(1)
    assert_that(err_max_from_formated[0].string).is_equal_to(str(err_max))


@given(
    value=floats(),
    first_error=sampled_from([-math.inf, math.inf]),
    useless_second_error=floats(),
)
def test_format_value_with_error_returns_max_bound_if_first_error_is_infinite(
    value: float, first_error: float, useless_second_error: float
):
    # First we create the HTML formated value
    formated_val = format_value_with_error(value, [first_error, useless_second_error])

    # Then we retrieve the info from the HTML
    soup = BeautifulSoup(formated_val, features="lxml")
    operator_from_formated = soup.select("math mo")
    val_from_formated = soup.select("math mn.value.max-value")

    # Now it's time for the tests !
    assert_that(operator_from_formated).is_length(1)
    assert_that(operator_from_formated[0].string).is_equal_to("<")
    assert_that(val_from_formated).is_length(1)
    assert_that(val_from_formated[0].string).is_equal_to(str(value))


@given(
    value=floats(),
    useless_first_error=floats(allow_infinity=False),
    second_error=sampled_from([-math.inf, math.inf]),
)
def test_format_value_with_error_returns_min_bound_if_second_error_is_infinite(
    value: float, useless_first_error: float, second_error: float
):
    # First we create the HTML formated value
    formated_val = format_value_with_error(value, [useless_first_error, second_error])

    # Then we retrieve the info from the HTML
    soup = BeautifulSoup(formated_val, features="lxml")
    operator_from_formated = soup.select("math mo")
    val_from_formated = soup.select("math mn.value.min-value")

    # Now it's time for the tests !
    assert_that(operator_from_formated).is_length(1)
    assert_that(operator_from_formated[0].string).is_equal_to(">")
    assert_that(val_from_formated).is_length(1)
    assert_that(val_from_formated[0].string).is_equal_to(str(value))
