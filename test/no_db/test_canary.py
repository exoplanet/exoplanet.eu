# Standard imports
from http import HTTPStatus


def test_canary_page_is_reachable(client):
    response = client.get("/canary/")

    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]
    assert "charset=utf-8" in response["Content-Type"]


def test_canary_page_has_content(client):
    response = client.get("/canary/")

    html = response.content.decode(response.charset)

    assert "Piou piou" in html
