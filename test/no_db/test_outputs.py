# First party imports
from core.outputs.plots import json_header_from_axis_fields


# TODO move this to a doctest
def test_json_header_from_axis_fields_do_what_it_should():
    dummy_in = ["aaa", "bbb", "ccc"]
    expected = [
        "aaa",
        "aaa_emin",
        "aaa_emax",
        "bbb",
        "bbb_emin",
        "bbb_emax",
        "ccc",
        "ccc_emin",
        "ccc_emax",
        "n",
        "u",
        "detection_method",
    ]

    assert json_header_from_axis_fields(dummy_in) == expected
