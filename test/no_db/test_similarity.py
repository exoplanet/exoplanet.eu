# Standard imports
from decimal import Decimal

# Test imports
# Pytest imports
import pytest
from pytest import param

# First party imports
from core.models import is_at_the_same_position_of_other_star


@pytest.mark.parametrize(
    "ra, dec",
    [
        param(None, Decimal("56"), id="ra = None"),
        param(Decimal("56"), None, id="dec = None"),
        param(None, None, id="both = None"),
    ],
)
def test_similarity__value_error(ra, dec):
    with pytest.raises(ValueError):
        is_at_the_same_position_of_other_star(ra, dec, None)
