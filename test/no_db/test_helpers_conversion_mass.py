# Standard imports
from contextlib import contextmanager
from decimal import Decimal, InvalidOperation
from math import inf, nan, radians, sin
from sys import float_info
from typing import NamedTuple, cast

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that
from hypothesis import HealthCheck, assume, given, settings
from hypothesis import strategies as st

# First party imports
from core.helpers.conversion.mass import mass_in_specific_unit, mass_planet_from_data
from core.models.choices import MassUnit

# Float comparison tolerence
_EPSILON = 14

# Shortcuts
_MEARTH = MassUnit.MEARTH
_MJUP = MassUnit.MJUP

# value to generate 0+ and 0- as float (subnormal numbers)
ZERO_PLUS_MAX = float_info.min / 4
ZERO_MINUS_MIN = -ZERO_PLUS_MAX


@contextmanager
def _caplog_for_hypothesis(caplog):
    """
    Context manager that capure logs that is compatible with hypothesis.

    Still needs the caplog from pytest function-scoped fixture so the calling func needs
    to have that decorator:

    `@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])`
    """
    # caplog is function-scoped but hypothesis runs multiple test in one func scope
    caplog.clear()

    try:
        yield caplog
    finally:
        # caplog is function-scoped but hypothesis runs multiple test in one func scope
        caplog.clear()


def is_decimal(s: str) -> bool:
    """Tells if a string is convertible to a decimal number."""
    try:
        Decimal(s)
    except InvalidOperation:
        return False
    return True


# Utility parameter class
class _ParamsForMassInSpecificUnitTests(NamedTuple):
    """Test parameter helper for better readability of parametrization"""

    expected: Decimal
    mass: str
    unit: MassUnit
    desired_unit: MassUnit
    significant_nb: int | None = None


@pytest.mark.parametrize(
    _ParamsForMassInSpecificUnitTests._fields,
    [
        # Let's express that the Earth has a mass of one earth mass
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("1.0"),
            mass="1.0",
            unit=_MEARTH,
            desired_unit=_MEARTH,
        ),
        # …and in mass of jupiter
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("0.0031"),
            mass="1.0",
            unit=_MEARTH,
            desired_unit=_MJUP,
        ),
        # Jupiter has a mass of one jupiter mass
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("1.0"),
            mass="1.0",
            unit=_MJUP,
            desired_unit=_MJUP,
        ),
        # …and in mass of earth
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("318.0"),
            mass="1.0",
            unit=_MJUP,
            desired_unit=_MEARTH,
        ),
        # Four significant digit in input -> four in output
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("317.8"),
            mass="1.000",
            unit=_MJUP,
            desired_unit=_MEARTH,
        ),
        # Plenty significant digit in input -> plenty in output
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("317.8281"),
            mass="1.000000",
            unit=_MJUP,
            desired_unit=_MEARTH,
        ),
        # You can also specify explicitly significant digits (with so strange results
        # due to strange implementation)
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("0.003"),
            mass="1.0",
            unit=_MEARTH,
            desired_unit=_MJUP,
            significant_nb=0,
        ),
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("0.0031"),
            mass="1.0",
            unit=_MEARTH,
            desired_unit=_MJUP,
            significant_nb=1,
        ),
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("0.00315"),
            mass="1.0",
            unit=_MEARTH,
            desired_unit=_MJUP,
            significant_nb=2,
        ),
        _ParamsForMassInSpecificUnitTests(
            expected=Decimal("0.00314635"),
            mass="1.0",
            unit=_MEARTH,
            desired_unit=_MJUP,
            significant_nb=5,
        ),
    ],
)
def test_mass_in_specific_unit_works(
    expected,
    mass,
    unit,
    desired_unit,
    significant_nb,
):
    calculated_mass = mass_in_specific_unit(mass, unit, desired_unit, significant_nb)

    # assert_that(calculated_mass).is_close_to(expected, _EPSILON)
    assert_that(calculated_mass).is_equal_to(expected)


# Utility parameter class
class _ParamsForTests(NamedTuple):
    """Test parameter helper for better readability of parametrization"""

    expected: Decimal
    desired_unit: MassUnit
    mass_detected_str: str | None
    mass_detected_unit: MassUnit | None
    mass_sini_str: str | None
    mass_sini_unit: MassUnit | None
    inclination: float | None


@pytest.mark.parametrize(
    _ParamsForTests._fields,
    [
        # Let's get the mass of a planet in the most direct possible way
        _ParamsForTests(
            expected=Decimal("5.0"),
            desired_unit=_MEARTH,
            mass_detected_str="5.0",
            mass_detected_unit=_MEARTH,
            mass_sini_str=None,
            mass_sini_unit=_MEARTH,
            inclination=None,
        ),
        # And now let's try with an inclination of 90° (sin i = 1)
        _ParamsForTests(
            expected=Decimal("8.0"),
            desired_unit=_MEARTH,
            mass_detected_str=None,
            mass_detected_unit=_MEARTH,
            mass_sini_str="8.0",
            mass_sini_unit=_MEARTH,
            inclination=90.0,
        ),
        # And now let's try with an inclination of -90° (sin i = -1)
        _ParamsForTests(
            expected=Decimal("8.0"),
            desired_unit=_MEARTH,
            mass_detected_str=None,
            mass_detected_unit=_MEARTH,
            mass_sini_str="8.0",
            mass_sini_unit=_MEARTH,
            inclination=-90.0,
        ),
        # ...and with an inclination of 30° (sin i = 0.5):
        _ParamsForTests(
            expected=Decimal("16.0"),
            desired_unit=_MEARTH,
            mass_detected_str=None,
            mass_detected_unit=_MEARTH,
            mass_sini_str="8.0",
            mass_sini_unit=_MEARTH,
            inclination=30.0,
        ),
        # If all info are given, only the mass detected is used:
        _ParamsForTests(
            expected=Decimal("3.0"),
            desired_unit=_MEARTH,
            mass_detected_str="3.0",
            mass_detected_unit=_MEARTH,
            mass_sini_str="1000000.0",
            mass_sini_unit=_MEARTH,
            inclination=30.0,
        ),
        # And of course changing units should work perfectly:
        _ParamsForTests(
            expected=Decimal("318.0"),
            desired_unit=_MEARTH,
            mass_detected_str="1.0",
            mass_detected_unit=_MJUP,
            mass_sini_str=None,
            mass_sini_unit=_MEARTH,
            inclination=None,
        ),
    ],
)
def test_mass_planet_from_data_works_for_valid_entries(
    expected,
    desired_unit,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_str,
    mass_sini_unit,
    inclination,
):
    calculated_mass = mass_planet_from_data(
        desired_unit,
        mass_detected_str,
        mass_detected_unit,
        mass_sini_str,
        mass_sini_unit,
        inclination,
    )
    # assert_that(calculated_mass).is_close_to(expected, _EPSILON)
    assert_that(round(cast(Decimal, calculated_mass), _EPSILON)).is_equal_to(
        round(expected, _EPSILON)
    )


# Shortcuts
MASS = "5.0"
UNIT = _MJUP

# Usefull strategies to use in the tests
_VALID_MASS_UNIT = st.sampled_from([_MEARTH, _MJUP])
_VALID_INCLINATIONS = st.decimals(
    min_value=-90.0,
    max_value=90.0,
    allow_infinity=False,
    allow_nan=False,
)
_VALID_MASS_DETEC_STR = st.builds(
    str,
    st.decimals(
        min_value=0.0,
        allow_infinity=False,
        allow_nan=False,
    ),
)
_VALID_MASS_SINI_STR = st.builds(
    str,
    st.decimals(
        min_value=0.0,
        allow_infinity=False,
        allow_nan=False,
    ),
)


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=_VALID_INCLINATIONS | st.none(),
    mass_detected_pack=st.tuples(st.text(max_size=100), _VALID_MASS_UNIT),
    mass_sini_str=_VALID_MASS_SINI_STR | st.none(),
    mass_sini_unit=_VALID_MASS_UNIT,
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_mass_is_invalid_str(
    desired_unit,
    inclination,
    mass_detected_pack,
    mass_sini_str,
    mass_sini_unit,
    caplog,
):
    # unpack the mass and mass_sini values
    mass_detected_str, mass_detected_unit = mass_detected_pack

    # Remove the case where the string would be a valid decimal
    assume(not is_decimal(mass_detected_str))

    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"Invalid mass string <[\s\S]*>: not convertible to decimal"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=st.none(),
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_str=_VALID_MASS_SINI_STR | st.none(),
    mass_sini_unit=_VALID_MASS_UNIT,
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_no_inclination(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_str,
    mass_sini_unit,
    caplog,
):
    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None> but "
            r"inclination <None> or mass sini <.*> or mass sini unit <.*> are None"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=_VALID_INCLINATIONS | st.none(),
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_str=st.none(),
    mass_sini_unit=_VALID_MASS_UNIT,
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_no_mass_sini(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_str,
    mass_sini_unit,
    caplog,
):
    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None> but "
            r"inclination <.+> or mass sini <None> or mass sini unit <.+> are None"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=st.sampled_from([inf, -inf, nan]),
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_str=_VALID_MASS_SINI_STR,
    mass_sini_unit=_VALID_MASS_UNIT,
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_inclination_is_not_finite(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_str,
    mass_sini_unit,
    caplog,
):
    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None> and inclination is not a finite number "
            r"<inclination: (-Infinity|Infinity|NaN)>"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=st.floats(
        min_value=ZERO_MINUS_MIN,
        max_value=ZERO_PLUS_MAX,
        allow_subnormal=True,
    ),
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_str=_VALID_MASS_SINI_STR,
    mass_sini_unit=_VALID_MASS_UNIT,
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_inclination_is_too_small(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_str,
    mass_sini_unit,
    caplog,
):
    # Remove not desirable inputs
    assume(sin(radians(inclination)) != 0.0)
    assume(float(mass_sini_str) != 0.0)

    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        # Check the result
        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None>, mass sini is <.*> but inclination is too small <.*>"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=st.just(0.0),
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_str=_VALID_MASS_SINI_STR,
    mass_sini_unit=_VALID_MASS_UNIT,
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_sini_is_zero(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_str,
    mass_sini_unit,
    caplog,
):
    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None> and mass sini is <.*> but sini is zero"
            r"<inclination: .*> <sini: 0.0>"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=_VALID_INCLINATIONS,
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_pack=st.tuples(st.text(max_size=100), _VALID_MASS_UNIT),
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_mass_sini_is_invalid_str(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_pack,
    caplog,
):
    # unpack the mass and mass_sini values
    mass_sini_str, mass_sini_unit = mass_sini_pack

    # Remove not desirable inputs
    assume(not is_decimal(mass_sini_str))
    assume(sin(radians(inclination)) != 0.0)

    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None> but Invalid mass sini string <[\s\S]*>: "
            r"not convertible to decimal"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=_VALID_INCLINATIONS,
    mass_detected_str=st.none(),
    mass_detected_unit=_VALID_MASS_UNIT,
    mass_sini_pack=st.tuples(
        st.builds(str, st.sampled_from([inf, -inf, nan])),
        _VALID_MASS_UNIT,
    ),
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_no_mass_and_mass_sini_is_not_finite(
    desired_unit,
    inclination,
    mass_detected_str,
    mass_detected_unit,
    mass_sini_pack,
    caplog,
):
    # unpack the mass and mass_sini values
    mass_sini_str, mass_sini_unit = mass_sini_pack

    # Remove not desirable inputs
    assume(sin(radians(inclination)) != 0.0)

    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        # Check the result
        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"mass is <None> but mass sini is <(Infinity|-Infinity|NaN)> "
            r"which is not a finite number"
        )


@given(
    desired_unit=_VALID_MASS_UNIT,
    inclination=_VALID_INCLINATIONS,
    mass_detected_pack=st.tuples(
        st.builds(str, st.just(nan)),
        _VALID_MASS_UNIT,
    ),
    mass_sini_pack=st.tuples(
        st.builds(str, st.just(nan)),
        _VALID_MASS_UNIT,
    ),
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_nan_if_mass_and_mass_sini_are_nan(
    desired_unit,
    inclination,
    mass_detected_pack,
    mass_sini_pack,
    caplog,
):
    # unpack the mass and mass_sini values
    mass_detected_str, mass_detected_unit = mass_detected_pack
    mass_sini_str, mass_sini_unit = mass_sini_pack

    # Remove not desirable inputs
    assume(sin(radians(inclination)) != 0.0)

    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        # Check the result
        assert_that(mass_calculated).is_instance_of(Decimal)
        assert_that(mass_calculated.is_nan()).is_true()

        # Check the logs of the problem
        assert_that(logs.records).is_length(0)


@given(
    mass_detected_str=_VALID_MASS_DETEC_STR | st.none(),
    mass_sini_str=_VALID_MASS_SINI_STR | st.none(),
    inclination=_VALID_INCLINATIONS | st.none(),
    units=st.permutations([None, _MEARTH, _MEARTH])
    | st.permutations([None, None, _MEARTH])
    | st.just([None, None, None]),
)
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
def test_mass_planet_from_data_returns_none_if_one_of_the_unit_is_none(
    mass_detected_str,
    mass_sini_str,
    inclination,
    units,
    caplog,
):
    # unpack the mass and mass_sini values
    desired_unit, mass_detected_unit, mass_sini_unit = units

    with _caplog_for_hypothesis(caplog) as logs:
        mass_calculated = mass_planet_from_data(
            desired_unit=desired_unit,
            mass_detected_str=mass_detected_str,
            mass_detected_unit=mass_detected_unit,
            mass_sini_str=mass_sini_str,
            mass_sini_unit=mass_sini_unit,
            inclination=inclination,
        )

        # Check the result
        assert_that(mass_calculated).is_none()

        # Check the logs of the problem
        assert_that(logs.records).is_length(1)
        log_rec = logs.records[0]
        assert_that(log_rec.levelname).is_equal_to("WARNING")
        assert_that(log_rec.funcName).is_equal_to("mass_planet_from_data")
        assert_that(log_rec.message).matches(
            r"Fail to calculate mass for planet so return None, "
            r"no unit should be None but "
            r"desired unit is <.+>"
            r"and mass detected unit is <.+> "
            r"and mass sini unit is <.+> "
        )
