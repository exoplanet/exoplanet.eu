# Pytest imports
# Test imports
import pytest

# First party imports
from core.models import Publication


@pytest.mark.django_db
def test_start_with_empty_database():
    assert not Publication.objects.all().exists()
