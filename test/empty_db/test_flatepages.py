# Pytest imports
# Test imports
import pytest

# Django 💩 imports
from django.contrib.flatpages.models import FlatPage


@pytest.mark.django_db
def test_flatpages_are_installed():
    # Flatpages exists
    assert FlatPage

    # But there should be no flatpages by default
    assert not FlatPage.objects.all().exists()
