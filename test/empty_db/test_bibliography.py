# Standard importsdt
# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# First party imports
# First party importsBibliographyListView
from core.models import Publication

# Local imports
from ..read_only.catalog_legacy.test_bibliography import (
    assert_jumbotron_is_ok_for_bibliography,
)
from ..read_only.utils import assert_menu_is_ok


@pytest.mark.parametrize(
    "url, btn_id",
    [
        ("/bibliography/books/", "jumbotron-btn-books"),
        ("/bibliography/theses/", "jumbotron-btn-thesis"),
        ("/bibliography/reports/", "jumbotron-btn-reports"),
        ("/bibliography/all/", "jumbotron-btn-all-publications"),
        ("/bibliography/", "jumbotron-btn-all-publications"),
    ],
)
@pytest.mark.django_db
def test_legacy_bibliography_is_reachable_via_get_with_no_publication_in_db(
    client,
    url,
    btn_id,
):
    # precondition
    assert_that(list(Publication.objects.all())).is_empty()

    pub_title_css_selector = "section#bibliography-content h5.card-title"

    response = client.get(url, follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "Bibliography")
    assert_jumbotron_is_ok_for_bibliography(soup, btn_id)

    assert_that(soup.select(pub_title_css_selector)).is_empty()

    # Check the pagination
    assert_that(soup.select("nav>ul.pagination")).is_empty()
