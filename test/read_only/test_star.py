# Pytest imports
# Test imports
import pytest

# First party imports
from core.models import Star
from core.models.choices import PlanetDetection, PublicationType


@pytest.mark.django_db
def test_star_can_be_created(star):
    assert star is not None

    star_from_db = Star.objects.get(id=star.id)

    assert star_from_db.id == star.id


@pytest.mark.django_db
def test_star_can_have_multiple_names(star):
    assert star is not None

    # Let's add some alternate names
    alice = star.alternate_star_names.create(name="Alice")
    bob = star.alternate_star_names.create(name="Bob")
    cecil = star.alternate_star_names.create(name="Cecil")

    assert alice is not None
    assert bob is not None
    assert cecil is not None

    alt_names = [n.name for n in star.alternate_star_names.all()]

    assert "Alice" in alt_names
    assert "Bob" in alt_names
    assert "Cecil" in alt_names


@pytest.mark.django_db
def test_star_can_have_multiple_publications(star):
    assert star is not None

    # Let's add some publications
    good_article = star.publications.create(
        title="Good article", date="2020", type=PublicationType.ARXIV
    )
    old_article = star.publications.create(
        title="Old article", date="1912", type=PublicationType.ARXIV
    )
    next_article = star.publications.create(
        title="Next article", date="2050", type=PublicationType.ARXIV
    )

    assert good_article is not None
    assert old_article is not None
    assert next_article is not None

    articles = [pub.title for pub in star.publications.all()]

    assert "Good article" in articles
    assert "Old article" in articles
    assert "Next article" in articles


@pytest.mark.django_db
def test_star_can_have_multiple_planets(star):
    assert star is not None

    # Let's add some planets
    amateru = star.planets.create(
        name="Amateru",
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,
        main_star=star,
    )
    brahe = star.planets.create(
        name="Brahe",
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,
        main_star=star,
    )
    caleuche = star.planets.create(
        name="Caleuche",
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,
        main_star=star,
    )

    assert amateru is not None
    assert brahe is not None
    assert caleuche is not None

    planets = [pl.name for pl in star.planets.all()]

    assert "Amateru" in planets
    assert "Brahe" in planets
    assert "Caleuche" in planets
