# Pytest imports
# Test imports
import pytest

# First party imports
from core.models import PlanetarySystem


@pytest.mark.django_db
def test_planetary_sytem_can_be_created(plnt_sys):
    # We check that the planet can be stored correcdtly in DB
    syst_from_db = PlanetarySystem.objects.get(id=plnt_sys.id)

    assert syst_from_db.id == plnt_sys.id


@pytest.mark.django_db
def test_planetary_sytem_can_be_filled_with_planets(
    plnt_sys,
    some_planets_in_system,
):
    # We check the back links
    assert all([p in plnt_sys.planets.all() for p in some_planets_in_system])
