# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# Local imports
from .utils import assert_menu_is_ok


@pytest.mark.django_db
def test_404_error_shows_custom_page(client):
    response = client.get("/this_page_does_not_exist/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.NOT_FOUND)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, current_page_name=None)

    assert_that(soup.title.string).is_equal_to("Error 404")


@pytest.mark.django_db
def test_500_error_shows_custom_page(client):
    client.raise_request_exception = False
    response = client.get("/for_tests/error_500/")

    # Let's check if the original error is ok
    assert_that(str(response.exc_info[1])).is_equal_to("Fake error")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.INTERNAL_SERVER_ERROR)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, current_page_name=None)

    assert_that(soup.title.string).is_equal_to("Error 500")
