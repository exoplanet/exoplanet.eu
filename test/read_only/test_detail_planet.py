# Standard imports
import operator
from functools import reduce
from http import HTTPStatus

# Test imports
import pytest
from assertpy import assert_that
from pytest import param

# Django 💩 imports
from django.db.models import Q

# External imports
from bs4 import BeautifulSoup

# First party imports
from core.models import Planet


@pytest.mark.parametrize(
    "query, expected",
    [
        param(
            Q(reduce(operator.or_, [("axis", 0.462)])),
            "0.462",
            id="Axis value != 0 and not None",
        ),
        param(
            Q(reduce(operator.or_, [("axis", 0.0)])),
            "0.0",
            id="PlanetDBView - axis value = 0",
        ),
        param(
            Q(reduce(operator.or_, [("axis__isnull", True)])),
            "—",
            id="PlanetDBView - axis value is None",
        ),
    ],
)
@pytest.mark.django_db
def test_planet_detail_contains_good_info(
    client,
    planet,  # pylint: disable=unused-argument
    planets_with_axis_equal_to_0,  # pylint: disable=unused-argument
    some_planets,  # pylint: disable=unused-argument
    query,
    expected,
):
    planet = Planet.objects.filter(query)[0]
    response = client.get(
        f"/catalog/{planet.url_keyword}/",
        follow=True,
    )

    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")
    axis = soup.find("dd", {"class": "axis"}).find("span")

    if expected == "—":
        assert_that(axis.text).is_equal_to(expected)
    else:
        assert_that(str(axis)).matches(
            rf'.*<mn class="value (both|no)-err-value">{expected}</mn>.*'
        )
