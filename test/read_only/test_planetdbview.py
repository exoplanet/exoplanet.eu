# Standard imports
from math import isclose

# Test imports
# Pytest imports
import pytest

# First party imports
from core.fields.errorarrayfield import norm_error_array_ctx
from core.models import PlanetDBView
from core.models.choices import MassUnit, RadiusUnit


@pytest.mark.django_db
def test_planetdbview_can_see_planet(planet):
    assert planet is not None

    # Let's retreive the view of the planet
    v = PlanetDBView.objects.get(id=planet.id)
    p = planet
    MJUP = MassUnit.MJUP
    MEARTH = MassUnit.MEARTH
    RJUP = RadiusUnit.RJUP
    REARTH = RadiusUnit.REARTH

    # We check this is the same planet
    assert v.id == p.id

    # And we check all the properties

    assert v.publication_status_letter == p.get_publication_status_letter()

    assert v.planet_status_string == p.get_planet_status_string()

    assert isclose(v.mass_sini_mjup, p.mass_sini_in_unit(MJUP))

    with norm_error_array_ctx(v.mass_sini_error_mjup) as (v_err1, v_err2):
        with norm_error_array_ctx(p.mass_sini_error_in_unit(MJUP)) as (p_err1, p_err2):
            assert isclose(v_err1, p_err1)
            assert isclose(v_err2, p_err2)

    assert isclose(v.mass_sini_mearth, p.mass_sini_in_unit(MEARTH))

    with norm_error_array_ctx(v.mass_sini_error_mearth) as (v_err1, v_err2):
        with norm_error_array_ctx(p.mass_sini_error_in_unit(MEARTH)) as (
            p_err1,
            p_err2,
        ):
            assert isclose(v_err1, p_err1)
            assert isclose(v_err2, p_err2)

    assert isclose(v.radius_rjup, p.radius_in_unit(RJUP))

    with norm_error_array_ctx(v.radius_error_rjup) as (v_err1, v_err2):
        with norm_error_array_ctx(p.radius_error_in_unit(RJUP)) as (p_err1, p_err2):
            assert isclose(v_err1, p_err1)
            assert isclose(v_err2, p_err2)

    assert isclose(v.radius_rearth, p.radius_in_unit(REARTH))

    with norm_error_array_ctx(v.radius_error_rearth) as (v_err1, v_err2):
        with norm_error_array_ctx(p.radius_error_in_unit(REARTH)) as (p_err1, p_err2):
            assert isclose(v_err1, p_err1)
            assert isclose(v_err2, p_err2)

    assert isclose(v.mass_mjup, p.mass_in_unit(MJUP))

    with norm_error_array_ctx(v.mass_error_mjup) as (v_err1, v_err2):
        with norm_error_array_ctx(p.mass_error_in_unit(MJUP)) as (p_err1, p_err2):
            assert isclose(v_err1, p_err1)
            assert isclose(v_err2, p_err2)

    assert isclose(v.mass_mearth, p.mass_in_unit(MEARTH))

    with norm_error_array_ctx(v.mass_error_mearth) as (v_err1, v_err2):
        with norm_error_array_ctx(p.mass_error_in_unit(MEARTH)) as (p_err1, p_err2):
            assert isclose(v_err1, p_err1)
            assert isclose(v_err2, p_err2)

    assert v.mass_from_mass_sini_flag == p.is_mass_from_mass_sini()
