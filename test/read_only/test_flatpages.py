# Standard imports
import datetime

# Test imports
# Pytest imports
import pytest

# Django 💩 imports
from django.contrib.admin.models import ADDITION, LogEntry
from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.models import FlatPage
from django.db.models import Max
from django.utils import timezone

# First party imports
from core.helpers.flatpages_tools import flatpage_fake_update, flatpage_last_update


@pytest.mark.django_db
def test_can_create_a_flatpage(client, flatpage):
    # Can we retreive the flat page and all it's content?
    assert FlatPage.objects.all()  # pylint: disable=no-member
    # pylint: disable-next=no-member
    page_from_db = FlatPage.objects.get(id=flatpage.id)
    assert page_from_db.id == flatpage.id
    assert "Just a page" in page_from_db.title
    assert "Dummy content" in page_from_db.content

    # Do we have a corresponding url?
    response = client.get(flatpage.url)
    html = response.content.decode(response.charset)
    assert "Dummy content" in html


@pytest.mark.django_db
def test_can_access_content_type_of_flatpages(flatpage):
    fp_type = ContentType.objects.get(app_label="flatpages", model="flatpage")

    assert fp_type.name == "flat page"

    # Now can we retreive it via content type ?
    assert fp_type.get_object_for_this_type(url=flatpage.url).id == flatpage.id


@pytest.mark.django_db
def test_can_access_admin_log():
    fp_type = ContentType.objects.get(app_label="flatpages", model="flatpage")

    # It is not possible to really test LogEntry since it only lists action done via the
    # admin interface on the actual web site
    # HACK: DoesNotExist is inherited by LogEntry from its ancestor Model
    with pytest.raises(LogEntry.DoesNotExist):  # pylint: disable=no-member
        _ = LogEntry.objects.get(action_flag=ADDITION, content_type=fp_type)


@pytest.mark.django_db
def test_can_get_last_admin_log_date():
    fp_type = ContentType.objects.get(app_label="flatpages", model="flatpage")

    last = LogEntry.objects.filter(
        action_flag=ADDITION, content_type=fp_type
    ).aggregate(Max("action_time"))

    assert last
    # It is not possible to really test LogEntry since it only lists action done via the
    # admin interface on the actual web site
    assert last == {"action_time__max": None}


@pytest.mark.django_db
def test_can_get_flatpage_last_update(flatpage, admin_user):
    # We need to make a fake modification of the flatpage (since only web admin
    # modification are registered)
    flatpage_fake_update(flatpage.url, user=admin_user)

    # Let's do the actual request
    last = flatpage_last_update(flatpage.url)

    assert isinstance(last, datetime.datetime)
    assert last < timezone.now()
