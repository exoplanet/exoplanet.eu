"""Test for AMDf stability pages."""


# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# Django 💩 imports
from django.shortcuts import get_object_or_404

# External imports
from bs4 import BeautifulSoup

# First party imports
from amd.star_list import AMD_STAR_LIST
from core.models import Star

# Local imports
from .utils import assert_menu_is_ok

AMD_API_URL = "http://vmesep.obspm.fr/esep_amd/api/amd"
TITLE_CSS_SELECTOR = "section.jumbotron h1"
SUBTITLE_CSS_SELECTOR = "section.jumbotron p"


# there used to be a fixture to have planets around each star but it does not seem to be
# useful for the test to pass
@pytest.mark.parametrize("star_name", AMD_STAR_LIST)
@pytest.mark.parametrize("method", ["get", "post"])
@pytest.mark.django_db
@pytest.mark.slow
def test_amd_page_is_reachable(
    client,
    all_star4amd,  # pylint: disable=unused-argument
    star_name,
    method,
):
    url_keyword = get_object_or_404(Star, name=star_name).url_keyword
    client_get_or_post = getattr(client, method)
    response = client_get_or_post(f"/amdf/amd/{url_keyword}/")

    # Check if the page is accessible
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains(
        "text/html",
        "charset=utf-8",
    )

    # Now let's check the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_that(soup.select_one(TITLE_CSS_SELECTOR).text).contains(
        "AMDf-stability",
        "of the system",
        star_name,
    )


@pytest.mark.django_db
def test_amd_list_page_is_reachable_via_get(
    client,
    all_star4amd,  # pylint: disable=unused-argument
):
    system_links_css_selector = "section#amd-systems-list .list-group .list-group-item"

    response = client.get("/amdf/list/")

    # Check if the page is accessible
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains(
        "text/html",
        "charset=utf-8",
    )

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # No menu is selected
    assert_menu_is_ok(soup, None)

    # Examine jumbotron
    assert_that(soup.select_one(TITLE_CSS_SELECTOR).text).contains("AMDf-stability")
    assert_that(soup.select_one(SUBTITLE_CSS_SELECTOR).text).contains(
        "A tool for computation of long-term stability of planetary systems."
    )

    # Examine body
    links = soup.select(system_links_css_selector)
    names = [lnk.string for lnk in links]
    assert_that(names).contains_only(*AMD_STAR_LIST)
