# Standard imports
import operator
from functools import reduce

# Test imports
import pytest
from assertpy import assert_that
from pytest import param

# Django 💩 imports
from django.db.models import Q

# First party imports
from core.catalog_field_def import (
    AXIS_FIELD,
    STAR_DISTANCE_FIELD,
    STAR_MAGNITUDE_I_FIELD,
    Column,
)
from core.models import PlanetarySystem, PlanetDBView, Star
from core.outputs.planet import _get_object_field_value


@pytest.mark.parametrize(
    "model, field, query, expected",
    [
        # PlanetDBView
        param(
            PlanetDBView,
            Column(AXIS_FIELD),
            Q(reduce(operator.or_, [("axis", 0.462)])),
            "0.462",
            id="PlanetDBView - axis value != 0 and not None",
        ),
        param(
            PlanetDBView,
            Column(AXIS_FIELD),
            Q(reduce(operator.or_, [("axis", 0.0)])),
            "0.0",
            id="PlanetDBView - axis value = 0",
        ),
        param(
            PlanetDBView,
            Column(AXIS_FIELD),
            Q(reduce(operator.or_, [("axis__isnull", True)])),
            "",
            id="PlanetDBView - axis value is None",
        ),
        # Star
        param(
            Star,
            Column(STAR_DISTANCE_FIELD),
            Q(reduce(operator.or_, [("distance", 66.0)])),
            "66.0",
            id="Star - distance value != 0 and not None",
        ),
        param(
            Star,
            Column(STAR_DISTANCE_FIELD),
            Q(reduce(operator.or_, [("distance", 0.0)])),
            "0.0",
            id="Star - distance value = 0",
        ),
        param(
            Star,
            Column(STAR_MAGNITUDE_I_FIELD),
            Q(reduce(operator.or_, [("magnitude_i__isnull", True)])),
            "",
            id="Star - magnitude_i value is None",
        ),
        # PlanetarySystem
        param(
            PlanetarySystem,
            Column(STAR_DISTANCE_FIELD),
            Q(reduce(operator.or_, [("distance", 48.9)])),
            "48.9",
            id="PlanetarySystem - distance value != 0 and not None",
        ),
        param(
            PlanetarySystem,
            Column(STAR_DISTANCE_FIELD),
            Q(reduce(operator.or_, [("distance", 0.0)])),
            "0.0",
            id="PlanetarySystem - distance value = 0",
        ),
        param(
            PlanetarySystem,
            Column(STAR_DISTANCE_FIELD),
            Q(reduce(operator.or_, [("distance__isnull", True)])),
            "",
            id="PlanetarySystem - distance value is None",
        ),
    ],
)
@pytest.mark.django_db
def test_planet_output__get_object_field_value(
    planet,  # pylint: disable=unused-argument
    planets_with_axis_equal_to_0,  # pylint: disable=unused-argument
    some_planets,  # pylint: disable=unused-argument
    star,  # pylint: disable=unused-argument
    star_with_distance_equal_to_0,  # pylint: disable=unused-argument
    all_star4amd,  # pylint: disable=unused-argument
    plnt_sys,  # pylint: disable=unused-argument
    plnt_sys_with_distance_equal_0,  # pylint: disable=unused-argument
    plnt_sys_with_null_distance,  # pylint: disable=unused-argument
    model,
    field,
    query,
    expected,
):
    obj = model.objects.filter(query)[0]
    res = _get_object_field_value(obj, field)

    if expected == "":
        assert_that(res).is_equal_to(expected)
    else:
        assert_that(res).matches(
            rf'.*<mn class="value (both|no)-err-value">{expected}</mn>.*'
        )
