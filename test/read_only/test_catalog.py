# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from pytest_django.asserts import assertTemplateUsed


@pytest.mark.django_db
def test_mention_legale_exists(client):
    # BUG there should not be a /catalog prefix
    response = client.get("/catalog_new/legal_notice/")

    assertTemplateUsed(response, "mentions_legales.html")
    assert response.status_code == HTTPStatus.OK
    assert response["Content-Type"] == "text/html; charset=utf-8"
