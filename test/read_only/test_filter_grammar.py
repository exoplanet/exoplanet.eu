# Test imports
import pytest
from pytest import param

# External imports
from more_itertools import powerset

# First party imports
from core.filter_grammar import createFilter
from core.models import PlanetDBView, PlanetDetection, PlanetStatus

GIVEN_DT_ALL_COMBINATIONS = [
    param(comb, id=f"detection_type = {comb}")
    for comb in [list(elem) for elem in powerset(list(PlanetDetection)) if elem != ()]
]


@pytest.mark.parametrize("given_dt", GIVEN_DT_ALL_COMBINATIONS)
@pytest.mark.django_db
@pytest.mark.slow()
def test_createFilter__detection_type(
    some_planets_with_differents_detection_type,
    given_dt,
):
    assert some_planets_with_differents_detection_type is not None

    selected_planets = PlanetDBView.objects.filter(
        createFilter("", [PlanetStatus.CONFIRMED], given_dt)[0]
    )
    for planet in selected_planets:
        # `set | set`` is an union
        assert set(given_dt) | (set(planet.detection_type)) is not None
