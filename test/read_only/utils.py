# Test imports
from assertpy import assert_that

# Django 💩 imports
from django import urls

# External imports
from bs4 import BeautifulSoup


def reverse_if_exist(url_name: str, **kwargs) -> str:
    """
    Return the url corresponding to the url_name if it exist otherwise empty string.

    Just like `django.urls.reverse()` except it does not raise `NoReverseMatch` when not
    url is found.
    """
    try:
        return urls.reverse(url_name, kwargs=kwargs)
    except urls.NoReverseMatch:
        return ""


def assert_legacy_menu_is_ok(soup: BeautifulSoup, current_page_name: str):
    """
    Fail if the legacy menu does not have link to all existing and relevant pages or if
    the current page is not considered active.

    Args:
        soup: a BeautifulSoup representation of the page
        current_page_name: name of the page as it appears in the menu text

    Raises:
        AssertionError
    """
    menu_item_css_selector = ".top ul.menu li a.menu_item"
    active_item_class = "menu_item_act"

    menu = {a.string: a for a in soup.select(menu_item_css_selector)}

    assert_that(menu["Home"]).has_href(reverse_if_exist("legacy_home"))
    assert_that(menu["Catalogue"]).has_href(reverse_if_exist("legacy_catalog"))
    assert_that(menu["Plots"]).has_href(reverse_if_exist("legacy_plots"))
    assert_that(menu["Bibliography"]).has_href(reverse_if_exist("legacy_bibliography"))
    assert_that(menu["Searches"]).has_href(reverse_if_exist("legacy_research"))
    assert_that(menu["Meetings"]).has_href(reverse_if_exist("legacy_meetings"))
    assert_that(menu["Other Sites"]).has_href(reverse_if_exist("legacy_sites"))

    assert_that(menu["VO"]["href"]).ends_with("/vo/")

    # Do we have the correct menu active?
    assert_that(menu[current_page_name]["class"]).contains(active_item_class)


def assert_menu_is_ok(soup: BeautifulSoup, current_page_name: str | None):
    """
    Fail if the menu does not have link to all existing and relevant pages or if the
    current page is not considered active.

    Args:
        soup: a BeautifulSoup representation of the page
        current_page_name: name of the page as it appears in the menu text

    Raises:
        AssertionError
    """
    menu_item_css_selector = "nav#menu-bar .nav-item"
    active_item_class = "active"

    menu = {
        item.a.contents[0].strip(): item for item in soup.select(menu_item_css_selector)
    }

    assert_that(menu["Home"].a).has_href(reverse_if_exist("legacy_home"))
    assert_that(menu["Catalogue"].a).has_href(reverse_if_exist("legacy_catalog"))
    assert_that(menu["Plots"].a).has_href(reverse_if_exist("legacy_plots"))
    assert_that(menu["Tools"].a).has_href(reverse_if_exist("tools"))
    assert_that(menu["News"].a).has_href(reverse_if_exist("legacy_news"))
    assert_that(menu["Bibliography"].a).has_href(
        reverse_if_exist("legacy_bibliography")
    )
    assert_that(menu["Meetings"].a).has_href(reverse_if_exist("legacy_meetings"))
    assert_that(menu["Links"].a).has_href(reverse_if_exist("legacy_links"))

    # Do we have the correct menu active?
    if current_page_name:
        assert_that(menu[current_page_name]["class"]).contains(active_item_class)
