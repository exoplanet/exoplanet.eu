# Standard imports
from http import HTTPStatus

# Test imports
import pytest
from assertpy import assert_that, soft_assertions

# External imports
from bs4 import BeautifulSoup

# First party imports
from core.models import Star


@pytest.mark.django_db
def test_star_admin_contains_labels(admin_client, admin_user, star):
    a_star_id = Star.objects.all()[0].id
    response = admin_client.get(
        f"/exoplanet_admin/core/star/{a_star_id}/change",
        user=admin_user,
        follow=True,
    )

    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    fieldsets = soup.find_all("fieldset")

    expected_label = [
        "Name",
        "RA (J2000)",
        "Planets",
        "Dec (J2000)",
        "Force star coordinates validation",
        "Spectral type",
        "Modified",
        "Status",
        "Radius",
        "Radius error",
        "Mass",
        "Mass error",
        "Age in Gyr",
        "Age error",
        "Distance",
        "Distance error",
        "Teff",
        "Teff error",
        "Metallicity",
        "Metallicity error",
        "V magnitude",
        "I magnitude",
        "J magnitude",
        "H magnitude",
        "K magnitude",
        "Projected radial velocity",
        "Detected disc",
        "Magnetic field",
        "Remarks",
        "Other web",
        "Url simbad",
    ]

    labels = [
        label.text.split(":")[0]
        for fieldset in fieldsets
        for label in fieldset.find_all("label")
    ]

    with soft_assertions():
        assert_that(len(fieldsets)).is_equal_to(5)
        assert_that(labels).contains(*expected_label)
