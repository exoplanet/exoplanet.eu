"""Tests of the different models of the core of exoplanet.eu project."""

# Standard imports
from datetime import datetime

# Test imports
# Pytest imports
import pytest

# First party imports
from core.models import Publication
from core.models.choices import PlanetDetection, WebStatus


@pytest.mark.django_db
def test_publication_can_be_created(publication):
    assert publication is not None

    publi_from_db = Publication.objects.get(id=publication.id)

    assert publi_from_db.id == publication.id


@pytest.mark.django_db
def test_publication_can_be_tracked(publication):
    assert publication.created is not None
    assert isinstance(publication.created, datetime)

    assert publication.modified is not None
    assert isinstance(publication.modified, datetime)


@pytest.mark.parametrize(
    "status,expected_visibility",
    [(WebStatus.ACTIVE, True), (WebStatus.HIDDEN, False), (WebStatus.IMPORTED, False)],
)
def test_publication_can_be_visible_or_not(
    publication,
    status,
    expected_visibility,
):
    assert publication is not None

    publication.status = status

    assert publication.is_visible() == expected_visibility


@pytest.mark.django_db
def test_publication_can_refer_to_multiple_stars(publication):
    assert publication is not None

    # Let's add some publications
    absolutno = publication.stars.create(name="Absolutno", ra=164.0, dec=-10.0)
    baekdu = publication.stars.create(name="Baekdu", ra=18.0, dec=21.0)
    canopus = publication.stars.create(name="Canopus", ra=22.0, dec=-55.0)

    assert absolutno is not None
    assert baekdu is not None
    assert canopus is not None

    stars = [st.name for st in publication.stars.all()]

    assert "Absolutno" in stars
    assert "Baekdu" in stars
    assert "Canopus" in stars


@pytest.mark.django_db
def test_publication_can_refer_to_multiple_planets(publication, star):
    assert publication is not None

    # Let's add some planets
    amateru = publication.planets.create(
        name="Amateru",
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,
        main_star=star,
    )
    brahe = publication.planets.create(
        name="Brahe",
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,
        main_star=star,
    )
    caleuche = publication.planets.create(
        name="Caleuche",
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,
        main_star=star,
    )

    assert amateru is not None
    assert brahe is not None
    assert caleuche is not None

    planets = [pl.name for pl in publication.planets.all()]

    assert "Amateru" in planets
    assert "Brahe" in planets
    assert "Caleuche" in planets
