"""Test get_mass() SQL function to ensure it behave exacty like its python equivalent"""

# Standard imports
from decimal import Decimal
from math import isnan, nan

# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from hypothesis import given
from hypothesis import strategies as st
from pytest import param

# First party imports
from core.helpers.conversion.mass import mass_planet_from_data
from core.models.choices import MassUnit

# Local imports
from .conftest import execute_query

CALCULATION_MARGIN = 0.00000001


@pytest.mark.parametrize(
    "mass_detected, "
    "mass_detected_unit, "
    "mass_sini, "
    "mass_sini_unit, "
    "inclination, "
    "result_unit, "
    "expected, "
    "warning_regexes",
    [
        param(
            "1.0",
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            "1.0",
            [],
            id="all data but inclinason 90°",
        ),
        param(
            "1.0",
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "90",
            None,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"no unit should be None but "
                r"desired unit is <<NULL>>"
                r"and mass detected unit is <.+> "
                r"and mass sini unit is <.+> "
            ],
            id="missing result unit",
        ),
        param(
            "1.0",
            None,
            "1.0",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"no unit should be None but "
                r"desired unit is <.+>"
                r"and mass detected unit is <<NULL>> "
                r"and mass sini unit is <.+> "
            ],
            id="missing mass detected unit",
        ),
        param(
            "1.0",
            MassUnit.MEARTH,
            "1.0",
            None,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"no unit should be None but "
                r"desired unit is <.+>"
                r"and mass detected unit is <.+> "
                r"and mass sini unit is <<NULL>> "
            ],
            id="missing mass sini unit",
        ),
        param(
            "1.0",
            MassUnit.MEARTH,
            None,
            MassUnit.MEARTH,
            None,
            MassUnit.MEARTH,
            "1.0",
            [],
            id="only mass",
        ),
        param(
            "not a decimal",
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"Invalid mass string <.+>: not convertible to double precision"
            ],
            id="mass not convertible to decimal",
        ),
        param(
            None,
            MassUnit.MEARTH,
            None,
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> but "
                r"inclination <.+> or mass sini <<NULL>> "
                r"or mass sini unit <.+> are NULL"
            ],
            id="no mass but no mass sini",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            None,
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> but "
                r"inclination <<NULL>> or mass sini <.+> "
                r"or mass sini unit <.+> are NULL"
            ],
            id="no mass but no inclination",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "NaN",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> and inclination is not a finite number"
                r"<inclination: NaN>"
            ],
            id="no mass but inclination is NaN",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "+infinity",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> and inclination is not a finite number"
                r"<inclination: Infinity>"
            ],
            id="no mass but inclination is +inf",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "-infinity",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> and inclination is not a finite number"
                r"<inclination: -Infinity>"
            ],
            id="no mass but inclination is -inf",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "1E-308",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return None, "
                r"mass is <<NULL>>, mass sini is <.+> but inclination is too small <.+>"
            ],
            id="no mass but inclination is too small for decimal precision",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "0.0",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> and mass sini is <.+> but sini is zero"
                r"<inclination: 0> <sini: 0>"
            ],
            id="no mass but inclination is zero (so sini = 0)",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "not a decimal",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> but Invalid mass sini string <.+>: "
                r"not convertible to double precision"
            ],
            id="no mass but mass sini not convertible to decimal",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "NaN",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> and mass sini is <NaN> and inclination is <90> "
                r"but calculated mass sini is not a finite number <NaN>"
            ],
            id="no mass but mass sini is NaN",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "+infinity",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> "
                r"and mass sini is <\+infinity> "
                r"and inclination is <90> "
                r"but calculated mass sini is not a finite number <Infinity>"
            ],
            id="no mass but mass sini is +inf",
        ),
        param(
            None,
            MassUnit.MEARTH,
            "-infinity",
            MassUnit.MEARTH,
            "90",
            MassUnit.MEARTH,
            None,
            [
                r"Fail to calculate mass for planet so return NULL, "
                r"mass is <<NULL>> "
                r"and mass sini is <-infinity> "
                r"and inclination is <90> "
                r"but calculated mass sini is not a finite number <-Infinity>"
            ],
            id="no mass but mass sini is -inf",
        ),
        param(
            "NaN",
            MassUnit.MEARTH,
            "NaN",
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "NaN",
            [],
            id="mass and mass sini are NaN",
        ),
        param(
            "NaN",
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "NaN",
            MassUnit.MEARTH,
            "NaN",
            [],
            id="mass and inclination are NaN",
        ),
        param(
            "NaN",
            MassUnit.MEARTH,
            "0.0",
            MassUnit.MEARTH,
            "NaN",
            MassUnit.MEARTH,
            "NaN",
            [],
            id="mass and inclination are NaN but mass sini is zero",
        ),
        param(
            "1.0",
            MassUnit.MEARTH,
            "NaN",
            MassUnit.MEARTH,
            "1.0",
            MassUnit.MEARTH,
            "1.0",
            [],
            id="only mass sini is NaN",
        ),
    ],
)
@pytest.mark.django_db
def test_get_mass_works_on_specific_example(
    worker_id,
    mass_detected,
    mass_detected_unit,
    mass_sini,
    mass_sini_unit,
    inclination,
    result_unit,
    expected,
    warning_regexes,
):

    # Execution of the SQL function
    response, notices = execute_query(
        worker_id,
        "SELECT get_mass("
        "%(mass_detected_string)s::VARCHAR, "
        "%(mass_detected_unit)s, "
        "%(mass_sini_string)s::VARCHAR, "
        "%(mass_sini_unit)s, "
        "%(inclination)s, "
        "%(result_unit)s"
        ")",
        {
            "mass_detected_string": mass_detected,
            "mass_detected_unit": mass_detected_unit,
            "mass_sini_string": mass_sini,
            "mass_sini_unit": mass_sini_unit,
            "inclination": inclination,
            "result_unit": result_unit,
        },
    )

    sql_result = response["get_mass"]

    # Get the python expected result
    py_expected = mass_planet_from_data(
        desired_unit=result_unit,
        mass_detected_str=mass_detected,
        mass_detected_unit=mass_detected_unit,
        mass_sini_str=mass_sini,
        mass_sini_unit=mass_sini_unit,
        inclination=None if inclination is None else float(inclination),
    )

    # Test the result
    with soft_assertions():
        if expected is None:
            assert_that(sql_result).is_none()
            assert_that(notices).is_not_empty()

            # Check if python version gives the same result
            assert_that(py_expected).is_none()
        elif expected.lower() == "nan":
            assert_that(sql_result).is_nan()

            # Check if python version gives the same result
            assert_that(py_expected).is_instance_of(Decimal)
            assert_that(py_expected.is_nan()).is_true()
        else:
            assert_that(sql_result).is_close_to(
                float(expected),
                CALCULATION_MARGIN,
            )
            assert_that(notices).is_empty()

            # Check if python version gives the same result
            assert_that(py_expected).is_equal_to(sql_result)

        # Check for warning messages
        for warn, expected_warn_regex in zip(notices, warning_regexes, strict=True):
            assert_that(warn).matches(expected_warn_regex)


# BUG: find why this test can't work (it works only if restricted to one example)
@pytest.mark.skip(reason="can't get this to work due to DB connexion error")
@given(
    mass_detected=st.builds(
        str,
        st.floats(
            min_value=0.0,
            allow_infinity=True,
            allow_nan=False,
        )
        | st.just(nan),
    )
    | st.none(),
    mass_detected_unit=st.sampled_from([MassUnit.MEARTH, MassUnit.MJUP]),
    mass_sini=st.builds(
        str,
        st.floats(
            min_value=0.0,
            allow_infinity=True,
            allow_nan=False,
        )
        | st.just(nan),
    )
    | st.none(),
    mass_sini_unit=st.sampled_from([MassUnit.MEARTH, MassUnit.MJUP]),
    inclination=st.builds(
        str,
        st.floats(
            min_value=-90.0,
            max_value=90.0,
            allow_infinity=False,
            allow_nan=False,
            allow_subnormal=True,
        )
        | st.just([nan]),
    )
    | st.none(),
    result_unit=st.sampled_from([MassUnit.MEARTH, MassUnit.MJUP]),
)
@pytest.mark.django_db
def test_get_mass_works_like_the_python_version(
    worker_id,
    mass_detected,
    mass_detected_unit,
    mass_sini,
    mass_sini_unit,
    inclination,
    result_unit,
):

    # Execution of the SQL function
    response, notices = execute_query(
        worker_id,
        "SELECT get_mass("
        "%(mass_detected_string)s::VARCHAR, "
        "%(mass_detected_unit)s, "
        "%(mass_sini_string)s::VARCHAR, "
        "%(mass_sini_unit)s, "
        "%(inclination)s, "
        "%(result_unit)s"
        ")",
        {
            "mass_detected_string": mass_detected,
            "mass_detected_unit": mass_detected_unit,
            "mass_sini_string": mass_sini,
            "mass_sini_unit": mass_sini_unit,
            "inclination": inclination,
            "result_unit": result_unit,
        },
    )

    result = response["get_mass"]

    # Get the python expected result
    py_expected = mass_planet_from_data(
        desired_unit=result_unit,
        mass_detected_str=mass_detected,
        mass_detected_unit=mass_detected_unit,
        mass_sini_str=mass_sini,
        mass_sini_unit=mass_sini_unit,
        inclination=None if inclination is None else float(inclination),
    )

    # Test the result
    if py_expected is None:
        assert_that(result).is_none()
    elif isnan(py_expected):
        assert_that(result).is_nan()
    else:
        assert_that(result).is_close_to(
            float(py_expected),
            CALCULATION_MARGIN,
        )
