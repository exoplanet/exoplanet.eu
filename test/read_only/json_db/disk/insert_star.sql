INSERT INTO disk_api.star(data)
VALUES
('{
  "internal": { "object_status": "confirmed", "rel_star_id": null },
  "identify": { "name": "Fomalhaut", "discovery_date": 2005 },
  "parameters": {
    "position": { "distance": 7.2, "ra": 22, "dec": -29 },
    "magnitude": { "magnitude_R": 1.2, "magnitude_v": 1.16 },
    "physical": { "spectral_type": "A4V", "teff": 8760, "age": 220 },
    "observational": { "flux": 1274, "luminosity": 17.8 }
  }
}'),
('{
  "internal": { "object_status": "confirmed", "rel_star_id": null },
  "identify": {
    "name": "HR 4796A",
    "moving_group": "TW Hydra",
    "discovery_date": 1999
  },
  "parameters": {
    "position": { "distance": 72.8, "ra": 12, "dec": -39 },
    "magnitude": { "magnitude_R": 5.8, "magnitude_v": 5.78 },
    "physical": {
      "mass": 2.4,
      "spectral_type": "A0",
      "teff": 9350,
      "age": 10
    },
    "observational": { "flux": 5.2, "luminosity": 22.8 }
  }
}'),
('{
"internal": { "object_status": "confirmed", "rel_star_id": null },
"identify": {
  "name": "HR 1234",
  "moving_group": "TW Hydra",
  "discovery_date": 1999
},
"parameters": {
  "position": { "distance": 72.8, "ra": 12, "dec": -39 },
  "magnitude": { "magnitude_R": 5.8, "magnitude_v": 5.78 },
  "physical": {
    "mass": 2.4,
    "spectral_type": "A0",
    "teff": 9350,
    "age": 10
  },
  "observational": { "flux": 5.2, "luminosity": 22.8 }
}
}');
