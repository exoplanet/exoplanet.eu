# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Django 💩 imports
from django.urls import reverse

# External imports
from bs4 import BeautifulSoup

# First party imports
from disk.views.utils.system import (
    _when_disk_request_answers_ok,
    _when_star_request_answers_ok,
    when_grav_sys_request_answers_ok,
)

# Local imports
from .conftest import ObjectLikeResponse


@pytest.mark.django_db
@pytest.mark.parametrize(
    "sys_id",
    [
        param(1, id="id 1"),
        param(2, id="id 2"),
        param(3, id="id 3"),
    ],
)
def test_system_page_is_reachable(
    sys_id,
    client,
    postgrest_api_server,
):
    response = client.get(f"/disk/system/{sys_id}", follow=True)
    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html")
        assert_that(response["Content-Type"]).contains("charset=utf-8")


@pytest.mark.django_db
@pytest.mark.parametrize(
    "sys_id",
    [
        param(1, id="id 1"),
        param(2, id="id 2"),
        param(3, id="id 3"),
    ],
)
def test_system_page_has_content_without_logged_user(
    sys_id,
    client,
    postgrest_api_server,
):
    response = client.get(f"/disk/system/{sys_id}", follow=True)
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to("Login")


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name, sys_id",
    [
        param("Fomalhaut", 1, id="id 1"),
        param("HR 4796A", 2, id="id 2"),
        param("HR 1234", 3, id="id 3"),
    ],
)
def test_system_page_has_content_with_logged_user(
    name,
    sys_id,
    client,
    create_user,
    postgrest_api_server,
):
    user = create_user
    client.force_login(user)
    response = client.get(f"/disk/system/{sys_id}", follow=True)
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to(f"{name}'s system")
        container = soup.find_all("div", {"class": "container"})
        disk_dc = soup.find_all("div", {"id": "system-info-system"})
        assert_that(container).is_not_empty()
        assert_that(disk_dc).is_not_empty()


# Tests for useful functions


def test_utils_system__grav_sys_request_answers_ok_success():
    response = ObjectLikeResponse(
        [
            {
                "update": "2023-05-12",
                "data": {
                    "principal_obj": {"id": 47, "type": "star"},
                    "secondary_objs": [{"id": 212, "type": "disk"}],
                },
            }
        ]
    )
    result = when_grav_sys_request_answers_ok(response)
    assert_that(result).is_equal_to(
        {
            "update": "2023-05-12",
            "data": {
                "principal_obj": {"id": 47, "type": "star"},
                "secondary_objs": [{"id": 212, "type": "disk"}],
            },
        }
    )


def test_utils_system__star_request_answers_ok_success():
    response = ObjectLikeResponse(
        [
            {
                "id": 9,
                "name": "HD 107146",
                "rel_star_id": 1466,
            },
        ]
    )
    result = _when_star_request_answers_ok(response)
    assert_that(result).is_equal_to(
        {
            "id": 9,
            "name": "HD 107146",
            "rel_star_id": 1466,
        },
    )


def test_utils_system__disk_request_answers_ok_success():
    response = ObjectLikeResponse(
        [
            {
                "id": 9,
                "name": "HD 107146",
                "url": "/disk/catalog/HD-107146__9",
            },
        ]
    )
    result = _when_disk_request_answers_ok(response)
    assert_that(result).is_equal_to(
        {
            "id": 9,
            "name": "HD 107146",
            "url": reverse(
                "disk_details",
                kwargs=dict(
                    disk_slug="HD-107146",
                    disk_id="9",
                ),
            ),
        },
    )
