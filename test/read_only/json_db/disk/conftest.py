"""Conftest for disk app tests"""
# Standard imports
import json
from dataclasses import dataclass
from os import getenv
from time import sleep
from typing import Any

# Test imports
# Pytest imports
import pytest

# External imports
import psycopg
import sh
from filelock import FileLock
from requests import models as rq_models


@dataclass
class ObjectLikeResponse(rq_models.Response):
    json_content: list[dict[str, Any]]

    def __init__(self, json_cont):
        self.json_content = json_cont

    def json(self):
        return self.json_content


def _db_connection(worker_id):
    db_name = f"test_{getenv('DJANGO_TEST_DB_NAME')}"
    if worker_id is not None:
        db_name = f"{db_name}_{worker_id}"

    return psycopg.connect(
        user=getenv("DJANGO_TEST_DB_USER"),
        host="localhost",
        password=getenv("DJANGO_TEST_DB_PASSWORD"),
        port=5432,
        dbname=db_name,
    )


def _give_select_access(worker_id: str | None = None):
    conn = _db_connection(worker_id)
    cursor = conn.cursor()

    cursor.execute("grant usage on schema disk_api to exo_web_anon_test")
    cursor.execute("grant select on disk_api.disk to exo_web_anon_test")
    cursor.execute("grant select on disk_api.star to exo_web_anon_test")
    cursor.execute("grant select on disk_api.exodict to exo_web_anon_test")
    cursor.execute("grant select on disk_api.gravitationalsystems to exo_web_anon_test")
    conn.commit()
    conn.close()


@pytest.fixture(scope="session")
def postgrest_api_server(django_db_setup, tmp_path_factory, worker_id):
    """Launch a postgrest server for a session and kill it at the end of the session."""

    if worker_id == "master":
        # not executing in with multiple workers, just produce the data and let
        # pytest's fixture caching do its job
        exec_file = getenv("POSTGREST_TEST_EXEC_LOCATION")
        config_file = getenv("POSTGREST_TEST_CONFIG_FILE")

        try:
            sh.Command(exec_file)(config_file, _bg=True)
            _give_select_access()

            # we wait a second while the server is well launched and configure.
            sleep(1)
            yield
        finally:
            sh.Command("pkill")("postgrest")
    else:
        # get the temp directory shared by all workers
        root_tmp_dir = tmp_path_factory.getbasetemp().parent

        fn = root_tmp_dir / "data.json"
        with FileLock(str(fn) + ".lock"):
            if fn.is_file():
                _give_select_access(worker_id)
                yield
            else:
                with open("toto.txt", "a+") as file:
                    file.write("toto\n")

                fn.write_text(json.dumps({"executed": True}))
                exec_file = getenv("POSTGREST_TEST_EXEC_LOCATION")
                config_file = getenv("POSTGREST_TEST_CONFIG_FILE")

                try:
                    sh.Command(exec_file)(config_file, _bg=True)
                    _give_select_access(worker_id)

                    # we wait a second while the server is well launched and configure.
                    sleep(1)
                    yield
                finally:
                    sh.Command("pkill")("postgrest")


@pytest.fixture(scope="function")
def create_user(django_user_model):
    """Create a user and return it."""
    return django_user_model.objects.create_user(
        username="test_user", password="nxAH5Yj9Pr2t82b"
    )
