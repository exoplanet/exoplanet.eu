INSERT INTO disk_api.gravitationalsystems(data)
VALUES
('
    {"principal_obj": {"id": 1, "type": "star"},
    "secondary_objs": [{"id": 1, "type": "disk"}]}
'),
('
    {"principal_obj": {"id": 2, "type": "star"},
    "secondary_objs": [{"id": 2, "type": "disk"}]}
'),
('
    {"principal_obj": {"id": 3, "type": "star"},
    "secondary_objs": [{"id": 3, "type": "disk"}]}
');
