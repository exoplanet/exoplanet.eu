INSERT INTO disk_api.disk(data)
VALUES
(
  '{
    "internal": { "object_status": "confirmed" },
    "identify": {
      "name": "Fomalhaut",
      "alternate_names": ["HD-216956", "HD-216956"]
    },
    "parameters": {
      "orbital": { "inclination": 65.6 },
      "geometric": { "mean_radius_r0": 20.5, "delta_r": 1.88 },
      "observational": {
        "surface_brightness_peak": 2.13e-5,
        "fractional_luminosity": 7.4e-5
      },
      "atmospheric": { "albedo": 0.05 }
    }
  }'),
('{
  "internal": { "object_status": "confirmed" },
  "identify": {
    "name": "HR 4796A",
    "alternate_names": ["HD-109573", "HIP-61498"]
  },
  "parameters": {
    "orbital": { "inclination": 73 },
    "geometric": {
      "mean_radius_r0": 1.05,
      "inner_edge_r1": 1.0,
      "outer_edge_r2": 1.1,
      "delta_r": 0.1
    },
    "observational": {
      "surface_brightness_peak": 0.0156,
      "polarized_fraction": 50,
      "fractional_luminosity": 0.005
    },
    "atmospheric": { "albedo": 0.28 }
  }
}'),
('{
  "internal": { "object_status": "confirmed" },
  "identify": {
    "name": "HR 1234",
    "alternate_names": ["HD-109573", "HIP-61498"]
  },
  "parameters": {
    "orbital": { "inclination": 73 },
    "geometric": {
      "mean_radius_r0": 1.05,
      "inner_edge_r1": 1.0,
      "outer_edge_r2": 1.1,
      "delta_r": 0.1
    },
    "observational": {
      "surface_brightness_peak": 0.0156,
      "polarized_fraction": 50,
      "fractional_luminosity": 0.005
    },
    "atmospheric": { "albedo": 0.28 }
  }
}');
