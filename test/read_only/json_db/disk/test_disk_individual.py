# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# External imports
from bs4 import BeautifulSoup

# First party imports
from disk.views.utils.individual import (
    grouped_info,
    when_exodict_request_answers_ok,
    when_object_request_answers_ok,
    when_principal_object_request_answers_ok,
)

# Local imports
from .conftest import ObjectLikeResponse


@pytest.mark.django_db
@pytest.mark.parametrize(
    "uri",
    [
        param("Fomalhaut__1", id="id 1 with good uri"),
        param("toto__1", id="id 1 with redirect uri"),
        param("HR-4796A__2", id="id 2 with good uri"),
        param("toto__2", id="id 2 with redirect uri"),
        param("HR-1234__3", id="id 3 with good uri"),
        param("toto__3", id="id 3 with redirect uri"),
    ],
)
def test_disk_individual_page_is_reachable(
    uri,
    client,
    postgrest_api_server,
):
    response = client.get(f"/disk/catalog/{uri}", follow=True)
    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html")
        assert_that(response["Content-Type"]).contains("charset=utf-8")


@pytest.mark.django_db
@pytest.mark.parametrize(
    "redirect_uri, good_uri",
    [
        param("toto__1", "Fomalhaut__1", id="id 1"),
        param("toto__2", "HR-4796A__2", id="id 2"),
        param("toto__3", "HR-1234__3", id="id 3"),
    ],
)
def test_disk_redirect_equals_good_uri(
    redirect_uri,
    good_uri,
    client,
    create_user,
    postgrest_api_server,
):
    user = create_user
    client.force_login(user)
    response_redir = client.get(f"/disk/catalog/{redirect_uri}", follow=True)
    response_good = client.get(f"/disk/catalog/{good_uri}", follow=True)
    html_redir = response_redir.content.decode(response_redir.charset)
    html_good = response_good.content.decode(response_good.charset)

    with soft_assertions():
        assert_that(response_good.status_code).is_equal_to(response_redir.status_code)
        assert_that(html_good).is_equal_to(html_redir)


@pytest.mark.django_db
def test_disk_individual_page_is_unreachable_wrong_id(
    client,
    create_user,
    postgrest_api_server,
):
    user = create_user
    client.force_login(user)
    with pytest.raises(TypeError):
        client.get("/disk/catalog/toto__9999", follow=True)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "slug, name, id",
    [
        param("Fomalhaut", "Fomalhaut", 1, id="id 1"),
        param("HR-4796A", "HR 4796A", 2, id="id 2"),
        param("HR-1234", "HR 1234", 3, id="id 3"),
    ],
)
def test_disk_individual_page_has_content_without_logged_user(
    slug,
    name,
    id,
    client,
    postgrest_api_server,
):
    response = client.get(f"/disk/catalog/{slug}__{id}", follow=True)
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to("Login")


@pytest.mark.django_db
@pytest.mark.parametrize(
    "slug, name, id",
    [
        param("Fomalhaut", "Fomalhaut", 1, id="id 1"),
        param("HR-4796A", "HR 4796A", 2, id="id 2"),
        param("HR-1234", "HR 1234", 3, id="id 3"),
    ],
)
def test_disk_individual_page_has_content_with_logged_user(
    slug,
    name,
    id,
    client,
    create_user,
    postgrest_api_server,
):
    user = create_user
    client.force_login(user)
    response = client.get(f"/disk/catalog/{slug}__{id}", follow=True)
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to(name)
        container = soup.find_all("div", {"class": "container"})
        disk_dc = soup.find_all("div", {"id": "disk-detail-content"})
        assert_that(container).is_not_empty()
        assert_that(disk_dc).is_not_empty()


# Tests for useful functions


def test_utils_individual__principal_object_request_answers_ok_success():
    response = ObjectLikeResponse(
        [
            {"id": 37},
        ]
    )
    result = when_principal_object_request_answers_ok(response)
    assert_that(result).is_equal_to(37)


def test_utils_individual__object_request_answers_ok_success():
    response = ObjectLikeResponse(
        [
            {
                "update": "2023-05-12 12:01:47",
                "data": {"parameters": {"titi": {"tutu": 13, "tata": {"toto": 14}}}},
            }
        ]
    )
    result = when_object_request_answers_ok(response)
    assert_that(result).is_equal_to(
        {
            "update": "2023-5-12",
            "data": {"titi": {"tutu": 13, "tata_toto": 14}},
        }
    )


@pytest.mark.parametrize(
    "json_content",
    [
        param([], id="len error: len = 0"),
        param([{"a": 1}, {"b": 2}], id="len error: len > 1"),
    ],
)
def test_utils_individual__disk_request_answers_ok_fails(json_content):
    with pytest.raises(TypeError):
        when_object_request_answers_ok(ObjectLikeResponse(json_content))


def test_utils_individual__exodict_request_answers_ok():
    response = ObjectLikeResponse(
        [
            {
                "update": "2023-05-12 12:01:47",
                "data": {
                    "titi": {
                        "definition": "toto",
                        "label": {
                            "short": "ti.",
                            "long": "toto of titi",
                            "full_ascii": "titi",
                        },
                    }
                },
            }
        ]
    )
    result = when_exodict_request_answers_ok(response)
    assert_that(result).is_equal_to(
        {
            "update": "2023-5-12",
            "data": {
                "titi": {
                    "definition": "toto",
                    "label": {
                        "short": "ti.",
                        "long": "toto of titi",
                        "full_ascii": "titi",
                    },
                }
            },
        }
    )


def test_utils_individual__group_info():
    disk_info = {"geometric": {"inner_edge_r1": 13}}

    exodict_info = {
        "parameters": {
            "geometric": {
                "label": {
                    "short": "geo",
                    "long": "geometric",
                    "full_ascii": "geometric",
                },
                "inner_edge_r1": {
                    "definition": "inner_edge_r1 defintion",
                    "unit": [
                        {"name": "Toto unit", "symbol": "toto", "wiki_data_rdf": None}
                    ],
                    "wiki_data_rdf": None,
                    "label": {
                        "short": "r1",
                        "long": "inner edge r1",
                        "full_ascii": "inner_edge_r1",
                    },
                },
            }
        }
    }

    result = grouped_info(disk_info, exodict_info)

    # Ultra specific testing.
    # It's because the function make many shortcut conditional expression
    with soft_assertions():
        assert_that(result).contains_key("geometric")
        geometric = result["geometric"]
        assert_that(geometric).contains_key("inner_edge_r1")
        inner_edge_r1 = geometric["inner_edge_r1"]
        assert_that(inner_edge_r1).contains_key(
            "value",
            "label",
            "definition",
            "unit_symbol",
            "unit_name",
        )
        value = inner_edge_r1["value"]
        label = inner_edge_r1["label"]
        definition = inner_edge_r1["definition"]
        unit_symbol = inner_edge_r1["unit_symbol"]
        unit_name = inner_edge_r1["unit_name"]
        assert_that(value).is_equal_to(13)
        assert_that(label).is_equal_to("inner edge r1")
        assert_that(definition).is_equal_to("inner_edge_r1 defintion")
        assert_that(unit_symbol).is_equal_to("toto")
        assert_that(unit_name).is_equal_to("Toto unit")
