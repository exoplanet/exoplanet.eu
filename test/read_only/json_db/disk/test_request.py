# Pytest imports
# Test imports
import pytest
from pytest import param

# External imports
import responses

# First party imports
from disk.views.utils.requests import make_get_request


@responses.activate
@pytest.mark.parametrize(
    "html_response_code, expected",
    [
        param(200, True, id="OK"),
        param(400, False, id="bad request"),
        param(404, False, id="not found"),
        param(500, False, id="internal serveur error"),
    ],
)
def test_make_get_request(html_response_code, expected):
    responses.add(
        responses.Response(
            method="GET",
            url="http://tests.com",
            json={},
            status=html_response_code,
        )
    )

    assert (
        make_get_request(
            "http://tests.com",
            {},
            10,
            treatment_if_ok=lambda x: True,
            treatment_if_error=lambda x: False,
        )
        == expected
    )
