# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# External imports
from bs4 import BeautifulSoup


@pytest.mark.django_db
@pytest.mark.parametrize(
    "uri",
    [
        param("/disk/", id="index"),
        param("/disk/catalog", id="with catalog"),
    ],
)
def test_disk_catalog_page_is_reachable(uri, client, postgrest_api_server):
    response = client.get(uri, follow=True)

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html")
        assert_that(response["Content-Type"]).contains("charset=utf-8")


@pytest.mark.django_db
def test_disk_catalog_page_has_content_without_logged_user(
    client, postgrest_api_server
):
    response = client.get("/disk/catalog/", follow=True)

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to("Login")


@pytest.mark.django_db
def test_disk_catalog_page_has_content_with_logged_user(
    client,
    create_user,
    postgrest_api_server,
):
    user = create_user
    client.force_login(user)
    response = client.get("/disk/catalog/", follow=True)
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to("Disk Catalogue")
        div_catalog_table = soup.find_all("div", {"id": "disk-catalog-div"})
        assert_that(div_catalog_table).is_not_empty()
