# Standard imports
from http import HTTPStatus

# Test imports
import pytest
from assertpy import assert_that

# Django 💩 imports
from django.urls import reverse

# First party imports
from core.models.planet import safe_name

XSS_EXPLOITS_URL_SUFFIX = [
    r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
    r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
    r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
    r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
    r";%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
    r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
    r"./;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
]


@pytest.mark.slow
@pytest.mark.parametrize("xss_suffix", XSS_EXPLOITS_URL_SUFFIX)
@pytest.mark.parametrize(
    "base_url",
    [
        "/catalog/",
        "/bibliography/books/",
        "/bibliography/reports/",
        "/bibliography/theses/",
        "/bibliography/all/",
        "/plots/",
        "/meetings/",
        "/meetings/future/",
        "/meetings/past/",
        # "/news/rss/",
        "/",
        "/catalog_new/legal_notice/",
    ],
)
@pytest.mark.django_db
def test_xss_attempt_on_simple_pages_fail(client, base_url, xss_suffix):
    # Then we test the xss attack
    response_xss = client.get(f"{base_url}{xss_suffix}", follow=True)
    assert_that(response_xss.status_code).is_equal_to(HTTPStatus.NOT_FOUND)


@pytest.mark.slow
@pytest.mark.parametrize("xss_suffix", XSS_EXPLOITS_URL_SUFFIX)
@pytest.mark.django_db
def test_xss_attempt_on_flatpage_fail(client, flatpage, xss_suffix):
    # Test the xss attack
    response_xss = client.get(f"{flatpage.url}{xss_suffix}", follow=True)
    assert_that(response_xss.status_code).is_equal_to(HTTPStatus.NOT_FOUND)


@pytest.mark.slow
@pytest.mark.parametrize("xss_suffix", XSS_EXPLOITS_URL_SUFFIX)
@pytest.mark.django_db
def test_xss_attempt_on_planet_page_fail(client, planet, xss_suffix):
    planet_url = f"/catalog/{planet.url_keyword}/"

    # Test the xss attack
    response_xss = client.get(f"{planet_url}{xss_suffix}", follow=True)
    assert_that(response_xss.status_code).is_equal_to(HTTPStatus.NOT_FOUND)


@pytest.mark.slow
@pytest.mark.parametrize(
    "xss_url",
    # Actual XSS url that should not pass on planets
    # Provided by pdelteil@protonmail.com via the DIO
    # These have been adapted to conform to new version of the site
    [
        r"/catalog/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/bibliography/books/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/bibliography/books/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/bibliography/reports/;%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r"/bibliography/reports/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/bibliography/theses/;%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r"/bibliography/theses/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/plots/;%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r"/plots/;%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r"/meetings/;%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r"/meetings/future/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/meetings/past/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/catalog_new/legal_notice/;%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r"/./;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/;%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r"/./;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501,
        r"/catalog/;%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r"/polar_plot/;%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
    ],
)
@pytest.mark.django_db
def test_actual_xss_attempt_fail_on_pages(client, xss_url):
    # Test the xss attack
    response_xss = client.get(xss_url, follow=True)
    assert_that(response_xss.status_code).is_equal_to(HTTPStatus.NOT_FOUND)


@pytest.mark.slow
@pytest.mark.parametrize(
    "xss_suffix",
    [
        # Actual XSS url that should not pass on planets
        # Provided by pdelteil@protonmail.com via the DIO
        # These have been adapted to conform to new version of the site
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r";%27%22%3E%3Csvg/class=dalfox%20onload=&?=#97&%23108&%23101&%23114&%2300116&%2340&%2341&%23x2f&%23x2f",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/class=%22dalfox%22onLoad=alert%2845%29%3E?=",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%27%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
    ],
)
@pytest.mark.django_db
def test_actual_xss_attempt_fail_on_planet(
    client,
    planet,
    xss_suffix,
):
    valid_url = reverse(
        "legacy_planet_detail",
        kwargs={
            "planet_safe_name": safe_name(planet.name),
            "planet_id_str": str(planet.id),
        },
    )

    # Test the xss attack
    response_xss = client.get(f"{valid_url}{xss_suffix}", follow=True)
    assert_that(response_xss.status_code).is_equal_to(HTTPStatus.NOT_FOUND)


@pytest.mark.slow
@pytest.mark.parametrize(
    "xss_suffix",
    [
        # Actual XSS url that should not pass on flatpage
        # Provided by pdelteil@protonmail.com via the DIO
        # These have been adapted to conform to new version of the site
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3Cimg/src/onerror=.1%7Calert%60%60%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3Csvg/OnLoad=%22%60$%7Bprompt%60%60%7D%60%22%20class=dalfox%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
        r";%22%3E%3CiFrAme/src=jaVascRipt:alert%2845%29%20class=dalfox%3E%3C/iFramE%3E?=",  # noqa: E501
    ],
)
@pytest.mark.django_db
def test_actual_xss_attempt_fail_on_flatpage(
    client,
    xss_suffix,
    flatpage,
):
    # Test the xss attack
    response_xss = client.get(f"{flatpage.url}{xss_suffix}", follow=True)
    assert_that(response_xss.status_code).is_equal_to(HTTPStatus.NOT_FOUND)
