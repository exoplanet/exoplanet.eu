# Pytest imports
# Test imports
import pytest

# First party imports
from editorial.models import Research
from editorial.models.choices import ResearchType


@pytest.mark.django_db
def test_research_can_be_created():
    research = Research(
        type=ResearchType.GROUND,
        text=(
            '<a target="_blank" '
            'href="http://msc.caltech.edu/workshop/2007/Deming_epoxi.pdf">'
            "<b>Extrasolar Planet Observations and Characterization</b></a> "
            "(EPOCh)<br>Transits"
        ),
    )

    assert research is not None

    research.save()

    # We check that the planet can be stored correcdtly in DB
    research_from_db = Research.objects.get(id=research.id)

    assert research_from_db.id == research.id
