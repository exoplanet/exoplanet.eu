# pylint: disable=redefined-outer-name

# Standard imports
import datetime
from itertools import chain
from os import getenv
from pathlib import Path
from random import choices, seed
from typing import Any

# Test imports
import pytest

# Django 💩 imports
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.db.models import Model

# External imports
import psycopg
from more_itertools import powerset
from psycopg.rows import dict_row

# First party imports
from amd.star_list import AMD_STAR_LIST
from core.models import Molecule, Planet, PlanetarySystem, Publication, Star
from core.models.choices import (
    DetectedDiscDetection,
    MassMeasurement,
    MassUnit,
    PlanetDetection,
    PlanetStatus,
    PublicationType,
    RadiusMeasurement,
    RadiusUnit,
    WebStatus,
)
from editorial.models import Link, Meeting, News

DISK_PATH = Path(__file__).parent / "json_db/disk"


@pytest.fixture(scope="session")
def django_db_setup(init_db):  # pylint: disable=unused-argument
    """
    We have to override this fixture in our own conftest.py to customize how test
    databases are constructed.

    See:
    https://pytest-django.readthedocs.io/en/latest/database.html#django-db-setup
    """


@pytest.fixture
def db_access_without_rollback_and_truncate(
    request,
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
):
    """
    See: https://pytest-django.readthedocs.io/en/latest/database.html#use-a-read-only-database # noqa: E501
    """
    django_db_blocker.unblock()
    request.addfinalizer(django_db_blocker.restore)


@pytest.fixture(scope="session")
def init_db(
    worker_id,
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    planet_data,
    planets_with_axis_equal_to_0_data,
    some_planets_data,
    some_planets_with_differents_detection_type_data,
    star_data,
    star_with_distance_equal_to_0_data,
    flatpage_data,
    lonely_planet_data,
    plnt_sys_data,
    plnt_sys_with_null_distance_data,
    plnt_sys_with_distance_equal_0_data,
    publication_data,
    hidden_publication_data,
    molecule_data,
    some_planets_in_system_data,
    all_pub_data,
    news_data,
    hidden_news_data,
    meeting_past_data,
    meeting_present_data,
    meeting_future_data,
    hidden_meeting_data,
    links_data,
    hidden_links_data,
) -> None:
    """Initialize the database. Only with multi worker.."""
    if worker_id != "master":
        with django_db_blocker.unblock():
            clean_and_save([star_data])
            clean_and_save([star_with_distance_equal_to_0_data])

            some_planets = some_planets_data
            clean_and_save(some_planets)
            for s_pla in some_planets:
                s_pla.stars.add(star_data)

            some_planets_with_diff_dt = some_planets_with_differents_detection_type_data
            clean_and_save(some_planets_with_diff_dt)
            for s_pla in some_planets_with_diff_dt:
                s_pla.stars.add(some_planets_with_diff_dt)

            planet = planet_data
            clean_and_save([planet])
            planet.stars.add(star_data)

            planets_with_axis_equal_to_0 = planets_with_axis_equal_to_0_data
            clean_and_save([planets_with_axis_equal_to_0])
            planets_with_axis_equal_to_0.stars.add(star_data)

            clean_and_save([flatpage_data])
            flatpage_data.sites.add(Site.objects.get_current().id)

            clean_and_save([plnt_sys_data])
            clean_and_save([plnt_sys_with_null_distance_data])
            clean_and_save([plnt_sys_with_distance_equal_0_data])
            clean_and_save([lonely_planet_data])
            clean_and_save([publication_data])
            clean_and_save([molecule_data])
            clean_and_save(some_planets_in_system_data)
            clean_and_save(all_pub_data)
            clean_and_save(hidden_publication_data)
            clean_and_save(news_data)
            clean_and_save(hidden_news_data)
            clean_and_save(
                meeting_past_data + meeting_present_data + meeting_future_data
            )
            clean_and_save(hidden_meeting_data)
            clean_and_save(links_data)
            clean_and_save(hidden_links_data)

    insert_data_in_db(worker_id, DISK_PATH / "insert_disk.sql")
    insert_data_in_db(worker_id, DISK_PATH / "insert_star.sql")
    insert_data_in_db(worker_id, DISK_PATH / "insert_exodict.sql")
    insert_data_in_db(worker_id, DISK_PATH / "insert_grav_sys.sql")


@pytest.fixture(scope="session")
def flatpage_data() -> FlatPage:
    """A dummy flatpage for the site with no real content."""
    return FlatPage(
        url="/dummy_url/",
        title="Just a page",
        content="<h1>Dummy page</h1>Dummy content.",
    )


@pytest.fixture(scope="session")
def flatpage(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    flatpage_data,
) -> FlatPage:
    """Get a dummy flatpage, if we only have one worker save it in the database."""
    flatpage = flatpage_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([flatpage])

    return flatpage_data


@pytest.fixture(scope="session")
def star_data():
    """A dummy star for the site with no real content."""
    return Star(
        name="BD-10 3166",
        ra=164.6166667,
        dec=-10.7702778,
        distance=66.0,
        distance_error=[2.6, 2.7],
        spec_type="G4 V",
        magnitude_v=10.08,
        magnitude_i=15.72,
        magnitude_h=5.41,
        magnitude_j=5.74,
        magnitude_k=5.33,
        mass=1.4,
        mass_error=[0.012, 0.012],
        age=3.83,
        age_error=[1.2, 1.2],
        teff=6406.0,
        teff_error=[61, 65],
        radius=1.27,
        radius_error=[0.09, 0.09],
        metallicity=0.22,
        metallicity_error=[0.08],
        detected_disc=DetectedDiscDetection.IMAGING,
        magnetic_field=True,
        remarks="Alias 2MASS 19322220+4121198",
        other_web="Some raw text",
        url_simbad=(
            "http://simbad.u-strasbg.fr/simbad/sim-id?"
            "Ident=gsc+06214-00210&NbIdent=1&Radius=2"
            "&Radius.unit=arcmin&submit=submit+id"
        ),
        radvel_proj=18.22,
    )


@pytest.fixture(scope="session")
def star(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    star_data,
) -> Star:
    """Get a dummy star, if we only have one worker save it in the database."""

    star = star_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([star])

    return star


@pytest.fixture(scope="session")
def star_with_distance_equal_to_0_data():
    """A dummy star with a distance equal to 0 for the site with no real content."""
    return Star(
        name="django c'est de la merde Etoile",
        ra=12.32542532,
        dec=-84.1309,
        distance=0.0,
        distance_error=[2.6, 2.7],
    )


@pytest.fixture(scope="session")
def star_with_distance_equal_to_0(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    star_with_distance_equal_to_0_data,
) -> Star:
    """
    Get a dummy star with a distance equal to 0.

    If we only have one worker save it in the database.
    """

    star = star_with_distance_equal_to_0_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([star])

    return star


@pytest.fixture(scope="session")
def all_star4amd_data() -> list[Star]:
    """
    A list of fake stars with name corresponding to the official AMD compatible star
    list.
    """
    return [
        Star(
            name=star_name,
            ra=(index / len(AMD_STAR_LIST)) * 360,  # fake position
            dec=((index / len(AMD_STAR_LIST)) * 180) - 90,  # fake position
            distance=150.0,
            distance_error=[20],
            spec_type="K0V",
            magnitude_v=11.7,
            magnitude_i=None,
            magnitude_h=None,
            magnitude_j=None,
            magnitude_k=None,
            mass=0.93,
            mass_error=[0.03],
            age=1.5,
            age_error=[0.3, 0.3],
            teff=5313,
            teff_error=[73],
            radius=0.87,
            radius_error=[0.04],
            metallicity=0.03,
            metallicity_error=[0.07],
            detected_disc=None,
            magnetic_field=None,
            remarks=f"fake AMD star {index}",
            other_web=None,
            url_simbad=(
                "http://simweb.u-strasbg.fr/simbad/sim-id"
                "?protocol=html&Ident=GSC+4799-1733"
                "&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id"
            ),
            radvel_proj=None,
        )
        for index, star_name in enumerate(AMD_STAR_LIST)
    ]


@pytest.fixture(scope="session")
def all_star4amd(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    all_star4amd_data,
) -> list[Star]:
    """Get a dummy AMD stars, if we only have one worker save it in the database."""

    amd_stars = all_star4amd_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(amd_stars)

    return amd_stars


@pytest.fixture(scope="session")
def planet_data(star_data):
    """A dummy planet for the site with no real content."""
    return Planet(
        # Compact info
        name="Kepler-11 g",
        period=118.37774,
        axis=0.462,
        eccentricity=0.352,
        inclination=163.1,
        angular_distance=0.000316,
        discovered=2012,
        status=WebStatus.ACTIVE,
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        # Full info
        axis_error=[0.0093, 0.0084],
        period_error=[0.0, float("inf")],
        eccentricity_error=[float("-inf"), 0.0],
        omega=316.4,
        omega_error=[2.8, 2.8],
        tzero_tr=2454975.6227,
        tzero_tr_error=[0.00012, 0.00011],
        tzero_vr=2457360.52,
        tzero_vr_error=[0.1],
        tperi=2457326.62183,
        tperi_error=[0.000319],
        tconj=2456416.40138,
        tconj_error=[0.00026],
        inclination_error=[0.32, 0.05],
        remarks=(
            "<b>28 Sep 2015 :</b> The planet 51 Eri b has been confirmed by "
            "astrometry of the planet position (De Rosa et al. 2015)"
        ),
        other_web=(
            '<a target="_blank" '
            'href="http://obswww.unige.ch/exoplanets/wasp14.html">'
            "Geneva Observatory data</a>"
        ),
        mass_measurement_type=MassMeasurement.TIMING,
        radius_measurement_type=RadiusMeasurement.THEORETICAL,
        tzero_tr_sec=2455561.549,
        tzero_tr_sec_error=[0.01],
        lambda_angle=151.0,
        lambda_angle_error=[23.0, 16.0],
        albedo=0.07,
        albedo_error=[0.03],
        temp_calculated=1451.0,
        temp_calculated_error=[70, 60],
        temp_measured=1600,
        temp_measured_error=[75, 65],
        hot_point_lon=1.0,
        log_g=4.35,
        impact_parameter=0.25,
        impact_parameter_error=[0.007],
        k=91.8,
        k_error=[4.7],
        # Actual planet info
        mass_sini_string="22.0",
        mass_sini_error_string=["0.15", "0.15"],
        mass_sini_unit=MassUnit.MJUP,
        mass_detected_string="2.75",
        mass_detected_error_string=["0.15", "0.15"],
        mass_detected_unit=MassUnit.MEARTH,
        radius_string="1.415",
        radius_error_string=["0.067", "0.084"],
        radius_unit=RadiusUnit.REARTH,
        publication_status=1,  # in Planet.PUBLICATION_STATUSES
        planet_status=PlanetStatus.CONFIRMED,
        main_star=star_data,
    )


@pytest.fixture(scope="session")
def planet(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    planet_data,
    star,
) -> Planet:
    """Get a dummy planet, if we only have one worker save it in the database."""

    planet = planet_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            # clean_and_save([star_data])
            clean_and_save([planet])
            planet.stars.add(star)

    return planet


@pytest.fixture(scope="session")
def planets_with_axis_equal_to_0_data(star_data) -> Planet:
    """Dummy planets with an axis equal to 0 for the site with no real content."""
    return Planet(
        name="Fake planet number with axis equal to 0",
        status=WebStatus.ACTIVE,
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,  # in Planet.PUBLICATION_STATUSES
        planet_status=PlanetStatus.CONFIRMED,
        main_star=star_data,
        axis=0.0,
    )


@pytest.fixture(scope="session")
def planets_with_axis_equal_to_0(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    planets_with_axis_equal_to_0_data,
    star,
) -> list[Planet]:
    """Get dummy planets, if we only have one worker save it in the db."""

    planet = planets_with_axis_equal_to_0_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([planet])
            planet.stars.add(star)

    return planet


@pytest.fixture(scope="session")
def some_planets_in_system_data(plnt_sys_data) -> list[Planet]:
    """Dummy planets in system for the site with no real content."""

    NB_PLANETS = 30
    return [
        Planet(
            name=f"Fake planet_sys number {i}",
            status=WebStatus.ACTIVE,
            detection_type=[PlanetDetection.PRIMARY_TRANSIT],
            publication_status=1,  # in Planet.PUBLICATION_STATUSES
            planet_status=PlanetStatus.CONFIRMED,
            planetary_system=plnt_sys_data,
        )
        for i in range(NB_PLANETS)
    ]


@pytest.fixture(scope="session")
def some_planets_in_system(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    some_planets_in_system_data,
) -> list[Planet]:
    """Get dummy planets in system, if we only have one worker save it in the db."""

    planets_in_sys = some_planets_in_system_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(planets_in_sys)

    return planets_in_sys


@pytest.fixture(scope="session")
def some_planets_data(star_data) -> list[Planet]:
    """Dummy planets for the site with no real content."""
    NB_PLANETS = 30
    return [
        Planet(
            name=f"Fake planet number some planets {i}",
            status=WebStatus.ACTIVE,
            detection_type=[PlanetDetection.PRIMARY_TRANSIT],
            publication_status=1,  # in Planet.PUBLICATION_STATUSES
            planet_status=PlanetStatus.CONFIRMED,
            main_star=star_data,
        )
        for i in range(NB_PLANETS)
    ]


@pytest.fixture(scope="session")
def some_planets(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    some_planets_data,
    star,
) -> list[Planet]:
    """Get dummy planets, if we only have one worker save it in the db."""

    some_planets = some_planets_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(some_planets)
            for s_pla in some_planets:
                s_pla.stars.add(star)

    return some_planets


@pytest.fixture(scope="session")
def some_planets_with_differents_detection_type_data(star_data) -> list[Planet]:
    """Dummy planets for the site with no real content."""
    seed(13287432790)

    all_comb = [list(elem) for elem in powerset(list(PlanetDetection)) if elem != ()]

    comb_choosen = choices(all_comb, k=75)

    return [
        Planet(
            name=f"Fake planet number some planets dt {idx}",
            status=WebStatus.ACTIVE,
            detection_type=list(comb),
            publication_status=1,  # in Planet.PUBLICATION_STATUSES
            planet_status=PlanetStatus.CONFIRMED,
            main_star=star_data,
        )
        for idx, comb in enumerate(comb_choosen)
    ]


@pytest.fixture(scope="session")
def some_planets_with_differents_detection_type(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    some_planets_with_differents_detection_type_data,
    star,
) -> list[Planet]:
    """Get dummy planets, if we only have one worker save it in the db."""

    all_planets = some_planets_with_differents_detection_type_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(all_planets)
            for s_pla in all_planets:
                s_pla.stars.add(star)

    return all_planets


@pytest.fixture(scope="session")
def plnt_sys_data() -> PlanetarySystem:
    """A dummy planetary system for the site with no real content."""

    return PlanetarySystem(
        ra=42.484952078,
        dec=-5.95983636252,
        distance=48.9,
        distance_error=[5.4, 4.4],
    )


@pytest.fixture(scope="session")
def plnt_sys(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    plnt_sys_data,
) -> PlanetarySystem:
    """Get a dummy planetary system, if we only have one worker save it in the db."""
    planetary_sys = plnt_sys_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([planetary_sys])

    return planetary_sys


@pytest.fixture(scope="session")
def plnt_sys_with_null_distance_data() -> PlanetarySystem:
    """
    Get a dummy planetary system with a null distance for the site with no real content.

    If we only have one worker save it in the db.
    """

    return PlanetarySystem(
        ra=128.42348098,
        dec=-47.4209424,
        distance=None,
        distance_error=None,
    )


@pytest.fixture(scope="session")
def plnt_sys_with_null_distance(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    plnt_sys_with_null_distance_data,
) -> PlanetarySystem:
    """
    Get a dummy planetary system with a null distance.

    If we only have one worker save it in the db.
    """

    planetary_sys = plnt_sys_with_null_distance_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([planetary_sys])

    return planetary_sys


@pytest.fixture(scope="session")
def plnt_sys_with_distance_equal_0_data() -> PlanetarySystem:
    """
    Get a dummy planetary system with a 0 distance for the site with no real content.

    If we only have one worker save it in the db.
    """

    return PlanetarySystem(
        ra=128.42348098,
        dec=-47.4209424,
        distance=0.0,
        distance_error=[0.0, 0.0],
    )


@pytest.fixture(scope="session")
def plnt_sys_with_distance_equal_0(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    plnt_sys_with_distance_equal_0_data,
) -> PlanetarySystem:
    """
    Get a dummy planetary system with a 0 distance.

    If we only have one worker save it in the db.
    """

    planetary_sys = plnt_sys_with_distance_equal_0_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([planetary_sys])

    return planetary_sys


@pytest.fixture(scope="session")
def lonely_planet_data(plnt_sys_data) -> Planet:
    """A dummy lonely planet for the site with no real content."""
    return Planet(
        # Compact info
        name="metal hurlant",
        period=118.37774,
        axis=0.462,
        eccentricity=0.352,
        inclination=163.1,
        angular_distance=0.000316,
        discovered=2012,
        status=WebStatus.ACTIVE,
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        # Full info
        axis_error=[0.0093, 0.0084],
        period_error=[0.05, 0.07],
        eccentricity_error=[0.6, 0.32],
        omega=316.4,
        omega_error=[2.8, 2.8],
        tzero_tr=2454975.6227,
        tzero_tr_error=[0.00012, 0.00011],
        tzero_vr=2457360.52,
        tzero_vr_error=[0.1],
        tperi=2457326.62183,
        tperi_error=[0.000319],
        tconj=2456416.40138,
        tconj_error=[0.00026],
        inclination_error=[0.32, 0.05],
        remarks=(
            "<b>28 Sep 2015 :</b> The planet 51 Eri b has been confirmed by "
            "astrometry of the planet position (De Rosa et al. 2015)"
        ),
        other_web=(
            '<a target="_blank" '
            'href="http://obswww.unige.ch/exoplanets/wasp14.html">'
            "Geneva Observatory data</a>"
        ),
        mass_measurement_type=MassMeasurement.TIMING,
        radius_measurement_type=RadiusMeasurement.THEORETICAL,
        tzero_tr_sec=2455561.549,
        tzero_tr_sec_error=[0.01],
        lambda_angle=151.0,
        lambda_angle_error=[23.0, 16.0],
        albedo=0.07,
        albedo_error=[0.03],
        temp_calculated=1451.0,
        temp_calculated_error=[70, 60],
        temp_measured=1600,
        temp_measured_error=[75, 65],
        hot_point_lon=1.0,
        log_g=4.35,
        impact_parameter=0.25,
        impact_parameter_error=[0.007],
        k=91.8,
        k_error=[4.7],
        # Actual planet info
        mass_sini_string="22.0",
        mass_sini_error_string=["0.15", "0.15"],
        mass_sini_unit=MassUnit.MJUP,
        mass_detected_string="2.75",
        mass_detected_error_string=["0.15", "0.15"],
        mass_detected_unit=MassUnit.MEARTH,
        radius_string="1.415",
        radius_error_string=["0.067", "0.084"],
        radius_unit=RadiusUnit.REARTH,
        publication_status=1,  # in Planet.PUBLICATION_STATUSES
        planet_status=PlanetStatus.CONFIRMED,
        planetary_system=plnt_sys_data,
    )


@pytest.fixture(scope="session")
def lonely_planet(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    lonely_planet_data,
    plnt_sys,  # pylint: disable=unused-argument
) -> Planet:
    """Get a dummy lonely planet, if we only have one worker save it in the db."""

    lonely_planet = lonely_planet_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([lonely_planet])

    return lonely_planet


@pytest.fixture(scope="session")
def publication_data() -> Publication:
    """A dummy publication for the site with no real content."""
    return Publication(
        title="Quantization nature of planet distributions.",
        author="NIE Q. & CHENG C.",
        date="2001",
        type=PublicationType.THESIS,
        journal_name="ApJ. Letters",
        journal_volume="411 (Sept)",
        journal_page="114",
        bibcode="2007Rech..411..114V",
        keywords="test",
        reference="Cambridge University Press",
        url="<a href=papers/rubic.abs>abstract</a>",
        updated=None,  # Not used anymore, should be removed from DB
        status=WebStatus.ACTIVE,
        doi="10.26133/NEA1",
    )


@pytest.fixture(scope="session")
def publication(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    publication_data,
) -> Publication:
    """Get a dummy publication, if we only have one worker save it in the db."""

    publication = publication_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([publication_data])

    return publication


@pytest.fixture(scope="session")
def hidden_publication_data() -> Publication:
    """A dummy hidden publication."""
    return Publication(
        title="HIDDEN publication",
        author="NIE Q. & CHENG C.",
        date="2001",
        type=PublicationType.THESIS,
        journal_name="ApJ. Letters",
        journal_volume="411 (Sept)",
        journal_page="114",
        bibcode="2007Rech..411..114V",
        keywords="test",
        reference="Cambridge University Press",
        url="<a href=papers/rubic.abs>abstract</a>",
        updated=None,  # Not used anymore, should be removed from DB
        status=WebStatus.HIDDEN,
        doi="10.26133/NEA1TOTO",
    )


@pytest.fixture(scope="session")
def hidden_publication(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    hidden_publication_data,
) -> Publication:
    """Get a dummy hidden publication, if we only have one worker save it in the db."""

    publication = hidden_publication_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([hidden_publication_data])

    return publication


@pytest.fixture(scope="session")
def molecule_data() -> Molecule:
    """A dummy molecule for the site with no real content."""
    return Molecule(name="H2O", status=WebStatus.ACTIVE)


@pytest.fixture(scope="session")
def molecule(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    molecule_data,
) -> Molecule:
    """Get a dummy molecule, if we only have one worker save it in the db."""

    molecule = molecule_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([molecule_data])

    return molecule


@pytest.fixture(scope="session")
def pub_books() -> list[Publication]:
    """Bunch of book publication with minimal characteristics."""

    NB_BOOKS = 60

    return [
        Publication(
            title=f"Dummy book #{i}",
            date=str(2000 - i),
            type=PublicationType.BOOK,
        )
        for i in range(NB_BOOKS)
    ]


@pytest.fixture(scope="session")
def pub_thesis() -> list[Publication]:
    """Bunch of thesis publication with minimal characteristics."""

    NB_THESIS = 60

    return [
        Publication(
            title=f"Dummy thesis #{i}",
            date=str(2000 - i),
            type=PublicationType.THESIS,
        )
        for i in range(NB_THESIS)
    ]


@pytest.fixture(scope="session")
def pub_reports() -> list[Publication]:
    """Bunch of report publication with minimal characteristics."""

    NB_REPORT = 60

    return [
        Publication(
            title=f"Dummy report #{i}",
            date=str(2000 - i),
            type=PublicationType.REPORT,
        )
        for i in range(NB_REPORT)
    ]


@pytest.fixture(scope="session")
def all_pub_data(
    pub_books: list[Publication],
    pub_thesis: list[Publication],
    pub_reports: list[Publication],
) -> list[Publication]:
    """Dummy publications for the site with no real content."""

    # We interleave the publications to have a predictable mix of different type
    return list(chain.from_iterable(zip(pub_books, pub_thesis, pub_reports)))


@pytest.fixture(scope="session")
def all_pub(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    all_pub_data,
) -> list[Publication]:
    """Get dummy publications, if we only have one worker save it in the db."""

    all_pub = all_pub_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(all_pub)

    return all_pub


@pytest.fixture(scope="session")
def news_data() -> list[News]:
    """Dummy news for the site with no real content."""

    nb_years = 5
    day = 7

    return [
        News(
            date=datetime.date(year=year, month=month, day=day),
            content=f"Dummy news content dated {year}/{month}/{day}.",
        )
        for year in range(2000, 2000 + nb_years)
        for month in range(1, 13)
    ]


@pytest.fixture(scope="session")
def news(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    news_data,
) -> list[News]:
    """Get dummy news, if we only have one worker save it in the db."""

    news = news_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(news)

    return news


@pytest.fixture(scope="session")
def hidden_news_data() -> list[News]:
    """Dummy hidden news."""

    year = 2023
    month = 10
    day = 25

    return News(
        date=datetime.date(year=year, month=month, day=day),
        content="HIDDEN news.",
        status=WebStatus.HIDDEN,
    )


@pytest.fixture(scope="session")
def hidden_news(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    hidden_news_data,
) -> list[News]:
    """Get dummy hidden news, if we only have one worker save it in the db."""

    hidden_news = hidden_news_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([hidden_news])

    return hidden_news


@pytest.fixture(scope="session")
def meeting_past_data() -> list[Meeting]:
    """Some meetings in the past (til last year)."""

    nb_years = 30
    day_begin = 6
    day_end = 18
    today = datetime.date.today()

    # TODO refactor this using numeric_range on datetime objects
    # See: https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.numeric_range  # noqa: E501
    return [
        Meeting(
            begin=datetime.date(year=year, month=month, day=day_begin),
            end=datetime.date(year=year, month=month, day=day_end),
            title=(
                f"Dummy past meeting from {year}/{month}/{day_begin} "
                f"to {year}/{month}/{day_end}."
            ),
            place="in the middle of nowhere",
        )
        for year in range(today.year - nb_years, today.year)
        for month in range(1, 13)
    ]


@pytest.fixture(scope="session")
def meeting_past(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    meeting_past_data,
) -> list[Meeting]:
    """Get dummy past meeting, if we only have one worker save it in the db."""

    meeting_past = meeting_past_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(meeting_past)

    return meeting_past


@pytest.fixture(scope="session")
def meeting_future_data() -> list[Meeting]:
    """Some meetings in the future (starting next year)."""

    nb_years = 2
    day_begin = 6
    day_end = 18
    today = datetime.date.today()

    # TODO refactor this using numeric_range on datetime objects
    # See: https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.numeric_range  # noqa: E501
    return [
        Meeting(
            begin=datetime.date(year=year, month=month, day=day_begin),
            end=datetime.date(year=year, month=month, day=day_end),
            title=(
                f"Dummy future meeting from {year}/{month}/{day_begin} "
                f"to {year}/{month}/{day_end}."
            ),
            place="in the middle of nowhere",
        )
        for year in range(today.year + 1, today.year + nb_years + 2)
        for month in range(1, 13)
    ]


@pytest.fixture(scope="session")
def meeting_future(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    meeting_future_data,
) -> list[Meeting]:
    """Get dummy future meeting, if we only have one worker save it in the db."""

    meeting_future = meeting_future_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(meeting_future)

    return meeting_future


@pytest.fixture(scope="session")
def meeting_present_data() -> list[Meeting]:
    """Just one meeting already started and not yet finished."""

    today = datetime.date.today()
    margin_days = 3
    date_begin = datetime.date(
        year=today.year, month=today.month, day=today.day
    ) - datetime.timedelta(days=margin_days)
    date_end = datetime.date(
        year=today.year, month=today.month, day=today.day
    ) + datetime.timedelta(days=margin_days)

    return [
        Meeting(
            begin=date_begin,
            end=date_end,
            title=(f"Dummy present meeting from {date_begin} to {date_end}."),
            place="in the middle of nowhere",
        )
    ]


@pytest.fixture(scope="session")
def meeting_present(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    meeting_present_data,
) -> list[Meeting]:
    """Get a dummy present meeting, if we only have one worker save it in the db."""

    meeting_present = meeting_present_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(meeting_present)

    return meeting_present


@pytest.fixture(scope="session")
def hidden_meeting_data() -> list[Meeting]:
    """Just one hidden meeting."""

    today = datetime.date.today()
    margin_days = 3
    date_begin = datetime.date(
        year=today.year, month=today.month, day=today.day
    ) - datetime.timedelta(days=margin_days)
    date_end = datetime.date(
        year=today.year, month=today.month, day=today.day
    ) + datetime.timedelta(days=margin_days)

    return Meeting(
        begin=date_begin,
        end=date_end,
        title="HIDDEN meeting.",
        place="in the middle of nowhere",
        status=WebStatus.HIDDEN,
    )


@pytest.fixture(scope="session")
def hidden_meeting(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    hidden_meeting_data,
) -> list[Meeting]:
    """Get a dummy hidden meeting, , if we only have one worker save it in the db."""

    hidden_meeting = hidden_meeting_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([hidden_meeting])

    return hidden_meeting


@pytest.fixture(scope="session")
def links_data():
    """Dummy links for the site with no real content."""

    nb_links = 15
    return [
        Link(value=f'<a href="https://dontexist.com/link{i}">Link to a site #{i}</a>')
        for i in range(nb_links)
    ]


@pytest.fixture(scope="session")
def links(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    links_data,
) -> list[Link]:
    """Get dummy links, if we only have one worker save it in the db."""

    all_links = links_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save(all_links)

    return all_links


@pytest.fixture(scope="session")
def hidden_links_data():
    """Dummy hidden links."""

    return Link(
        value='<a href="https://dontexist.com/link">HIDDEN link</a>',
        status=WebStatus.HIDDEN,
    )


@pytest.fixture(scope="session")
def hidden_links(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
    worker_id,
    hidden_links_data,
) -> list[Link]:
    """Get dummy hidden links, if we only have one worker save it in the db."""

    hidden_links = hidden_links_data

    if worker_id == "master":
        with django_db_blocker.unblock():
            clean_and_save([hidden_links])

    return hidden_links


def clean_and_save(all_objs: list[Model]):
    """
    Clean and save a model in the database.

    Args:
        all_objs: Models to save.
    """
    for obj in all_objs:
        obj.full_clean()
        obj.save()


@pytest.fixture
def get_number_of_planets(
    django_db_setup,  # pylint: disable=unused-argument
    django_db_blocker,
) -> int:
    """
    Get the number of planets in database.

    Returns:
        The number of planets in database.
    """
    with django_db_blocker.unblock():
        return len(Planet.objects.all())


def insert_data_in_db(worker_id, file: Path):
    """Insert data in db."""
    conn = _db_connection(worker_id)
    with open(file, "rb") as sql_file:
        data = sql_file.read()
        with conn.cursor() as cursor:
            cursor.execute(data)
            conn.commit()
        conn.close()


def _db_connection(worker_id: str):
    db_name = f"test_{getenv('DJANGO_TEST_DB_NAME')}"
    if worker_id != "master":
        db_name = f"{db_name}_{worker_id}"
    return psycopg.connect(
        user=getenv("DJANGO_TEST_DB_USER"),
        host="localhost",
        password=getenv("DJANGO_TEST_DB_PASSWORD"),
        port=5432,
        dbname=db_name,
    )


def execute_query(
    worker_id: str, query: str, params: tuple
) -> tuple[dict[str, Any], list[str]]:
    """
    Execute an SQL query.

    Args:
        worker_id: Id of the worker for the connection.
        query: Query to execute.
        params: Parameters of the query.

    Returns:
        Return the response of the query.
    """
    conn = _db_connection(worker_id)
    notices = []
    conn.add_notice_handler(lambda notif: notices.append(notif.message_primary))

    with conn.cursor(row_factory=dict_row) as curs:
        curs.execute(query, params)
        resp = curs.fetchone()

    conn.close()
    return resp, notices
