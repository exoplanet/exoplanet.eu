# Standard imports
from http import HTTPStatus

# Test imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup


@pytest.mark.django_db
def test_planets_index_page_is_reachable_via_get(client):
    response = client.get("/catalog_new/planet_index/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")


@pytest.mark.django_db
def test_planets_index_page_has_planets_links(client, some_planets):
    response = client.get("/catalog_new/planet_index/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    listing = soup.find("ul", {"id": "planetIndex"})
    assert_that(listing).is_not_none()
    assert_that(
        len(listing.find_all("li", {"class": "link_to_planet"}))
    ).is_greater_than_or_equal_to(len(some_planets))
