# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# First party imports
from catalog.views import Tools

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.django_db
def test_tools_page_is_reachable_via_get(client):
    response = client.get("/catalog_new/tools/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, current_page_name="Tools")


@pytest.mark.django_db
def test_tools_page_has_all_tools(client):
    tool_card_selector = "#tools .tool"

    response = client.get("/catalog_new/tools/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    cards = soup.select(tool_card_selector)
    assert_that(cards).is_length(len(Tools.all_tools))
    for card, tool in zip(cards, Tools.all_tools):
        assert_that(card.img["src"]).ends_with(tool.img_name)
        assert_that(card.img["alt"]).is_equal_to(tool.img_alt)
        card_body = card.select_one(".card-body")
        assert_that(card_body.h2.string).is_equal_to(tool.name)
        assert_that(card_body.p.string).is_equal_to(tool.description)
        assert_that(card_body.a.string).is_in(f"Go to {tool.name} page", "Coming Soon")
