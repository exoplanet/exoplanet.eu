# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that


@pytest.mark.django_db
def test_legal_notice_is_reachable_via_get(client):
    response = client.get("/catalog_new/legal_notice/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")
