# Pytest imports
# Test imports
import pytest

# First party imports
from core.models import Planet
from core.models.choices import PublicationType


@pytest.mark.django_db
def test_planet_can_be_created(planet, star):  # noqa: F811
    assert planet is not None

    # We check the links
    assert planet.main_star.id == star.id
    assert star in planet.stars.all()
    assert planet.planetary_system is None

    # We check that the planet can be stored correcdtly in DB
    planet_from_db = Planet.objects.get(id=planet.id)

    assert planet_from_db.id == planet.id
    assert star in planet_from_db.stars.all()


@pytest.mark.django_db
def test_lonely_planet_can_be_created(lonely_planet, plnt_sys):
    assert lonely_planet is not None

    # We check the links
    assert lonely_planet.main_star is None
    assert not list(lonely_planet.stars.all())
    assert lonely_planet.planetary_system.id == plnt_sys.id

    # We check that the planet can be stored correcdtly in DB
    planet_from_db = Planet.objects.get(id=lonely_planet.id)

    assert planet_from_db.id == lonely_planet.id
    assert not list(planet_from_db.stars.all())
    assert planet_from_db.planetary_system.id == plnt_sys.id


@pytest.mark.django_db
def test_planet_can_have_multiple_names(planet):
    assert planet is not None

    # Let's add some alternate names
    alice = planet.alternate_planet_names.create(name="Alice")
    bob = planet.alternate_planet_names.create(name="Bob")
    cecil = planet.alternate_planet_names.create(name="Cecil")

    assert alice is not None
    assert bob is not None
    assert cecil is not None

    alt_names = [n.name for n in planet.alternate_planet_names.all()]

    assert "Alice" in alt_names
    assert "Bob" in alt_names
    assert "Cecil" in alt_names


@pytest.mark.django_db
def test_planet_can_have_multiple_publications(planet):
    assert planet is not None

    # Let's add some publications
    good_article = planet.publications.create(
        title="Good article", date="2020", type=PublicationType.ARXIV
    )
    old_article = planet.publications.create(
        title="Old article", date="1912", type=PublicationType.ARXIV
    )
    next_article = planet.publications.create(
        title="Next article", date="2050", type=PublicationType.ARXIV
    )

    assert good_article is not None
    assert old_article is not None
    assert next_article is not None

    articles = [pub.title for pub in planet.publications.all()]

    assert "Good article" in articles
    assert "Old article" in articles
    assert "Next article" in articles


# TODO test also from the star side
@pytest.mark.django_db
def test_planet_can_have_multiple_stars(planet):
    assert planet is not None

    # Let's add some stars
    absolutno = planet.stars.create(name="Absolutno", ra=164.0, dec=-10.0)
    baekdu = planet.stars.create(name="Baekdu", ra=18.0, dec=21.0)
    canopus = planet.stars.create(name="Canopus", ra=22.0, dec=-55.0)

    assert absolutno is not None
    assert baekdu is not None
    assert canopus is not None

    stars = [st.name for st in planet.stars.all()]

    assert "Absolutno" in stars
    assert "Baekdu" in stars
    assert "Canopus" in stars
