# Standard imports
from http import HTTPStatus
from pathlib import PurePath

# Django 💩 imports
from django.conf import settings

# External imports
from bs4 import BeautifulSoup, SoupStrainer

# Local imports
from ..utils import ctx


def test_canary_page_with_scss_is_reachable(client):
    response = client.get("/canary/pagewithscss/")

    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]
    assert "charset=utf-8" in response["Content-Type"]


def test_canary_page_has_scss_link_converted_to_css(client):
    response = client.get("/canary/pagewithscss/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml", parse_only=SoupStrainer("link"))
    scss_link = soup.link

    # Do we have a valid css file?
    assert scss_link["rel"] == ["stylesheet"]
    assert scss_link["type"] == "text/css"

    with ctx(PurePath(scss_link["href"])) as href_path:
        # Is the file generated name corect and human readableish?
        assert href_path.suffix == ".css"
        assert href_path.name.startswith("canary")
        # Was the css file generated in the compress output directory?
        assert settings.COMPRESS_OUTPUT_DIR in href_path.parts
