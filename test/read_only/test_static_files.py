# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from pytest_django.asserts import assertTemplateUsed

# External imports
from bs4 import BeautifulSoup

# Local imports
from ..utils import ctx


@pytest.mark.django_db
def test_canary_can_list_flatpages(client):
    response = client.get("/canary/allflatpages/")

    assertTemplateUsed(response, "allflatpages.html")
    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]
    assert "charset=utf-8" in response["Content-Type"]

    html = response.content.decode(response.charset)

    assert "Here are all the flat pages:" in html


@pytest.mark.django_db
def test_canary_has_page_for_static_urls(client):
    response = client.get("/canary/pagewithstatic/")

    assertTemplateUsed(response, "pagewithstatic.html")

    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]
    assert "charset=utf-8" in response["Content-Type"]

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Check for specific
    with ctx(list(soup.stripped_strings)) as strings:
        assert "Here are some static links:" == strings[0]
        assert "...the end." == strings[-1]

    # Check the presence of figures
    assert soup("figure")

    # Check if each figure has...
    # See: https://stackoverflow.com/a/21489636
    for fig in soup("figure"):
        # ...an image
        assert fig.img
        # ...a licence
        with ctx(fig.footer.small) as licence:
            assert licence
            assert " by " in str(licence)
            assert " can be reused under the " in str(licence)
            assert any(["license" in a["rel"] for a in licence("a", rel=True)])
        # ...and a caption
        assert fig.caption


@pytest.mark.parametrize(
    "elem_id,remote_path",
    [
        ("canary_app_static", "/static/canary/app-canary.png"),
        ("canary_common_static", "/static/common/img/common-canary.png"),
        ("canary_catalog_static", "/static/catalog/img/catalog-canary.png"),
    ],
)
def test_static_urls_are_correctly_rendered(client, elem_id, remote_path):
    response = client.get("/canary/pagewithstatic/")
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    fig = soup.find("figure", id=elem_id)
    assert fig, f"Could not find a figure element with id={elem_id}"
    assert fig.img["src"].endswith(remote_path)


# TODO check if all static resources are used
# TODO rename the exoplanet directory to common for consistency and to reduce coupling
