# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions

# External imports
from bs4 import BeautifulSoup

# First party imports
from catalog_legacy.views import FutureMeetingListView, PastMeetingListView

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.django_db
def test_legacy_future_meeting_is_reachable_via_get(
    client,
    meeting_present,
    meeting_future,
):
    page_length = FutureMeetingListView.paginate_by
    meet_title_css_selector = "section#meetings-content h5.card-title"

    response = client.get("/meetings/future/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "Meetings")

    expected_nb_of_meeting = min([page_length, len(meeting_future + meeting_present)])
    assert_that(soup.select(meet_title_css_selector)).is_length(expected_nb_of_meeting)

    # Check the pagination if needed
    if page_length < len(meeting_future + meeting_present):
        assert_that(soup.select("nav>ul.pagination")).is_not_empty()


@pytest.mark.django_db
def test_legacy_past_meeting_is_reachable_via_get(client, meeting_past):
    page_length = PastMeetingListView.paginate_by
    meet_title_css_selector = "section#meetings-content h5.card-title"

    response = client.get("/meetings/past/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "Meetings")

    expected_nb_of_meeting = min([page_length, len(meeting_past)])
    assert_that(soup.select(meet_title_css_selector)).is_length(expected_nb_of_meeting)

    # Check the pagination if needed
    if page_length < len(meeting_past):
        assert_that(soup.select("nav>ul.pagination")).is_not_empty()


@pytest.mark.django_db
def test_legacy_meetings_redirects_to_future_meetings(client):
    response = client.get("/meetings/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Did we have a redirection?
    assert_that(response.redirect_chain).is_equal_to(
        [
            ("/meetings/future/", HTTPStatus.MOVED_PERMANENTLY),
        ]
    )


@pytest.mark.django_db
@pytest.mark.slow
def test_legacy_meeting_hidden_hidden(client, hidden_meeting):
    meet_title_css_selector = "section#meetings-content h5.card-title"
    response = client.get("/news/", follow=True)

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        all_meetings = soup.select(meet_title_css_selector)
        for metting in all_meetings:
            assert_that(metting).does_not_contain("HIDDEN meeting")
