# Standard imports
import json
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.parametrize(
    "url",
    [
        "/catalog/json/",
        "/catalog/all_fields/json/",
    ],
)
@pytest.mark.django_db
def test_legacy_catalog_json_is_reachable_via_post(
    client, get_number_of_planets, some_planets, url  # pylint: disable=unused-argument
):
    # TODO Add a real page length...
    nb_planets = get_number_of_planets
    nb_planet_to_display = 15  # TODO retrieve this number with real code
    assert_that(nb_planet_to_display).is_less_than(nb_planets)
    echo = 123456

    response = client.post(
        url,
        data=dict(
            sSearch="",
            iSortCol_0="0",
            sSortDir_0="",
            iDisplayStart="0",
            iDisplayLength=nb_planet_to_display,
            sEcho=echo,
        ),
    )

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")

    # Let's examine the content
    json_str = response.content.decode(response.charset)
    data = json.loads(json_str)

    assert_that(data["aaData"]).is_length(nb_planet_to_display)
    (
        assert_that(data)
        .has_iTotalRecords(nb_planets)
        .has_iTotalDisplayRecords(nb_planets)
        .has_sEcho(echo)
    )
    assert_that(data["stats"]).has_planets(nb_planets).has_systems(1).has_multiple(1)


@pytest.mark.django_db
def test_legacy_catalog_html_is_reachable_via_get(client):
    response = client.get("/catalog/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Let's examine the content

    # We have a menu
    assert_menu_is_ok(soup, current_page_name="Catalogue")

    # We have a title
    assert_that(soup.h1.stripped_strings).contains("Catalogue of Exoplanets")

    # We have the required sections?
    section_titles = [list(h2.stripped_strings) for h2 in soup.find_all("h2")]
    assert_that(section_titles).extracting(0).contains(
        "Downloads",
        "Criteria for inclusion in the catalogue",
    )

    # We have a (poorly placed) Readme
    # TODO move that link into the jumbotron
    # assert "/readme/" == soup.h2.find_all("a", id="readme")[0]["href"]

    # We have a datatable
    assert_that(soup.find_all("div", class_="catalog_div")).is_length(1)
    assert_that(soup.find_all("table", id="data")).is_length(1)
    assert_that(soup.find_all("a", id="planetIndexLink")).is_length(1)
