# pylint: disable=missing-module-docstring
# Standard imports
import json
from http import HTTPStatus

# Test imports
import pytest
from assertpy import assert_that, soft_assertions

# Django 💩 imports
from django.urls import get_resolver

# External imports
from bs4 import BeautifulSoup

# First party imports
from catalog_legacy.views import ALL_SHORTCUTS

# Local imports
from ..utils import assert_menu_is_ok


def test_one_and_only_one_shortcut_is_default():
    all_default = [sc for sc in ALL_SHORTCUTS if sc.default is True]

    assert_that(all_default).is_length(1)


def test_shortcut_are_unique():
    names = [sc.url_name for sc in ALL_SHORTCUTS]
    suffixes = [sc.url_suffix for sc in ALL_SHORTCUTS]
    queries = [sc.query_f for sc in ALL_SHORTCUTS]
    texts = [sc.link_text for sc in ALL_SHORTCUTS]

    with soft_assertions():
        assert_that(names).does_not_contain_duplicates()
        assert_that(suffixes).does_not_contain_duplicates()
        assert_that(queries).does_not_contain_duplicates()
        assert_that(texts).does_not_contain_duplicates()


@pytest.mark.parametrize(
    "url_suffix, expected_url_name",
    [(shortcut.url_suffix, shortcut.url_name) for shortcut in ALL_SHORTCUTS],
)
def test_shortcut_pages_all_have_a_django_url(
    url_suffix,
    expected_url_name,
):
    # Check for the presence of a JSON url
    url = f"/catalog/shortcuts/{url_suffix}"
    resolved_name = get_resolver().resolve(url).url_name

    assert_that(resolved_name).is_equal_to(expected_url_name)


@pytest.mark.parametrize(
    "url_suffix",
    [shortcut.url_suffix for shortcut in ALL_SHORTCUTS],
)
@pytest.mark.parametrize(
    "url_post_suffix, expected_url_name",
    [
        ("json/", "legacy_catalog_shortcuts_json"),
        ("all_fields/json/", "legacy_catalog_all_fields_shortcuts_json"),
    ],
)
def test_shortcut_pages_all_have_a_django_url_for_json_catalog_access(
    url_suffix,
    url_post_suffix,
    expected_url_name,
):
    # Each shortcut url has an associated url to retrieve its JSON data let's build it
    json_url = f"/catalog/shortcuts/{url_suffix}{url_post_suffix}"

    # let's resolve this url via django and get the name of the url...
    json_resolved_name = get_resolver().resolve(json_url).url_name

    # ...and check if it is the expected one
    assert_that(json_resolved_name).is_equal_to(expected_url_name)


@pytest.mark.parametrize(
    "url_suffix, query_f, link_text",
    [(sc.url_suffix, sc.query_f, sc.link_text) for sc in ALL_SHORTCUTS],
)
@pytest.mark.django_db
def test_shortcut_pages_are_reachable_via_get(
    client, planet, lonely_planet, url_suffix, query_f, link_text
):
    url = f"/catalog/shortcuts/{url_suffix}"
    response = client.get(url)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Let's examine the content

    with soft_assertions():
        # We have a menu
        assert_menu_is_ok(soup, current_page_name="Catalogue")

        # We have a title
        assert_that(soup.h1.stripped_strings).contains("Catalogue of Exoplanets")

        # We have a datatable
        assert_that(soup.select("div.catalog_div")).is_length(1)
        assert_that(soup.select("table#data")).is_length(1)

        # We have the right filter set
        assert_that(soup.select_one("input#query_f").get("value")).is_equal_to(query_f)

        # We have a filter shortcut title
        assert_that(soup.select("#filterPresetTitle")).is_length(1)
        assert_that(
            soup.select_one("#filterPresetTitle h4").decode_contents().strip()
        ).is_equal_to(link_text)

        # it's not possible to check the number of planet returrned since this is
        # displayed via JS


@pytest.mark.parametrize(
    "url_suffix, link_text, default",
    [(sc.url_suffix, sc.link_text, sc.default) for sc in ALL_SHORTCUTS],
)
@pytest.mark.django_db
def test_shortcut_links_are_present_in_catalog_ui(
    client,
    url_suffix,
    link_text,
    default,
):
    response = client.get("/catalog/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Let's examine the content

    with soft_assertions():
        # Do we have filter button ?
        filter_button = soup.select_one("button#dropdownPresetFilters")
        assert_that(filter_button.string.strip()).is_equal_to("Filter presets")

        # Do we have shortcut link in the dropdown menu ?
        preset_menu = soup.select_one("div[aria-labelledby=dropdownPresetFilters]")
        assert_that(preset_menu).is_not_none()

        menu_items = soup.select("div[aria-labelledby=dropdownPresetFilters]>a")
        assert_that(menu_items).is_length(len(ALL_SHORTCUTS))

        items_desc = [
            (
                item.decode_contents().strip(),
                item.get("href"),
            )
            for item in menu_items
        ]

        assert_that(items_desc).contains(
            (link_text, f"/catalog/shortcuts/{url_suffix}")
        )


@pytest.mark.django_db
def test_shortcut_link_have_default_link_active_on_default_catalog_page(client):
    response = client.get("/catalog/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Let's examine the content
    menu_items = soup.select(
        "div[aria-labelledby=dropdownPresetFilters]>a.dropdown-item"
    )
    default_items = soup.select(
        "div[aria-labelledby=dropdownPresetFilters]>a.dropdown-item.default"
    )

    with soft_assertions():
        assert_that(default_items).is_length(1)

        the_default = default_items[0]

        assert_that(the_default.attrs["class"]).contains("default")
        assert_that(menu_items).contains(the_default)


@pytest.mark.parametrize(
    "url_suffix, link_text",
    [(sc.url_suffix, sc.link_text) for sc in ALL_SHORTCUTS],
)
@pytest.mark.django_db
def test_shortcut_link_have_link_active_for_the_current_page(
    client,
    url_suffix,
    link_text,
):
    full_url = f"/catalog/shortcuts/{url_suffix}"

    response = client.get(full_url)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Let's examine the content
    menu_items = soup.select(
        "div[aria-labelledby=dropdownPresetFilters]>a.dropdown-item"
    )
    active_items = soup.select(
        "div[aria-labelledby=dropdownPresetFilters]>a.dropdown-item.active"
    )

    with soft_assertions():
        assert_that(active_items).is_length(1)

        the_active = active_items[0]

        assert_that(the_active.attrs["class"]).contains("active")
        assert_that(menu_items).contains(the_active)
        assert_that(the_active.decode_contents().strip()).is_equal_to(link_text)
        assert_that(the_active.get("href")).is_equal_to(full_url)


@pytest.mark.parametrize("url_suffix", [sc.url_suffix for sc in ALL_SHORTCUTS])
@pytest.mark.parametrize(
    "url_final_suffix",
    [
        "json/",
        "all_fields/json/",
    ],
)
@pytest.mark.django_db
def test_legacy_catalog_json_for_shortcuts_is_reachable_via_post(
    client,
    get_number_of_planets,
    some_planets,  # pylint: disable=unused-argument
    url_suffix,
    url_final_suffix,
):
    # TODO Add a real page length...
    nb_planets = get_number_of_planets
    nb_planet_to_display = 15  # TODO retrieve this number with real code
    assert_that(nb_planet_to_display).is_less_than(nb_planets)
    echo = 123456

    # We build the url base from url_suffix only if the url suffix is not already a
    #  full url
    url_base = (
        url_suffix if url_suffix.startswith("/") else f"/catalog/shortcuts/{url_suffix}"
    )
    url = f"{url_base}{url_final_suffix}"

    response = client.post(
        url,
        data=dict(
            sSearch="",
            iSortCol_0="0",
            sSortDir_0="",
            iDisplayStart="0",
            iDisplayLength=nb_planet_to_display,
            sEcho=echo,
        ),
    )

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")

    # Let's examine the content
    json_str = response.content.decode(response.charset)
    data = json.loads(json_str)

    assert_that(data["aaData"]).is_length(nb_planet_to_display)
    (
        assert_that(data)
        .has_iTotalRecords(nb_planets)
        .has_iTotalDisplayRecords(nb_planets)
        .has_sEcho(echo)
    )
    assert_that(data["stats"]).has_planets(nb_planets).has_systems(1).has_multiple(1)
