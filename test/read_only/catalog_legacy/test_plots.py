# Standard imports
import re
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions

# External imports
from bs4 import BeautifulSoup

# First party imports
# pylint: disable=no-name-in-module
from core.catalog_field_def import (
    HISTOGRAM_VAR_PLANET_FIELDS,
    HISTOGRAM_VAR_STARS_FIELDS,
)

# Local imports
from ..utils import assert_menu_is_ok

EXPECTED_RELATED_MODEL_IN_JS = ["Planet", "Star"]

# Regex to find an element like:
# data-parameters='[{"titi": "toto"}, {"tutu": "tata", "foo": "bar"}]'
DATA_PARAMETERS_REGEX = r"""data-parameters=('\[({(".+": ".+")+})+\]'){1}"""

# Regex to find an element like:
# [{"titi": "toto"}, {"tutu": "tata", "foo": "bar"}]
DATA_PARAMETERS_LIST_REGEX = r"""(\[({(".+": ".+")+})+\]){1}"""


# TODO test for both possible values for the t GET param --> parametrize
# TODO test for possible values for the f GET param --> parametrize
@pytest.mark.django_db
def test_legacy_plots_is_reachable_via_get(client):
    response = client.get("/plots/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, current_page_name="Plots")


@pytest.mark.django_db
def test_histogram_with_no_argument(client):
    response = client.get("/plots_request_ajax/")

    # Do we have a JSON response?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")


ALL_POSSIBLE_HISTOGRAM_VAR_IDS = [
    (
        field["related_model_field"]
        if isinstance(field["model_field"], str)
        and field["model_field"].startswith("star_")
        else (
            field["model_field"]
            if isinstance(field["model_field"], str)
            else field["id"]
        )
    )
    for field in HISTOGRAM_VAR_PLANET_FIELDS + HISTOGRAM_VAR_STARS_FIELDS
]


# TODO test failure with dummy axis values
@pytest.mark.slow
@pytest.mark.django_db
@pytest.mark.parametrize("plot_var", ["x", "y", "z", "p"])
@pytest.mark.parametrize("field", ALL_POSSIBLE_HISTOGRAM_VAR_IDS)
def test_histogram_with_only_one_axis(client, plot_var, field):
    response = client.get("/plots_request_ajax/", {plot_var: field})

    # Do we have a JSON response?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")


# TODO test all possible planet status values
# TODO test failure with dummy planet status
@pytest.mark.django_db
def test_histogram_with_planet_status_confirmed(client):
    params = dict(f='"confirmed" IN planet_status')
    response = client.get("/plots_request_ajax/", data=params)

    # Do we have a JSON response?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")


@pytest.mark.django_db
def test_histogram_fails_with_dummy_detection(client):
    params = dict(f='"dummy" IN detection')
    response = client.get("/plots_request_ajax/", data=params)

    # Do we have a JSON response?
    assert_that(response.status_code).is_equal_to(HTTPStatus.BAD_REQUEST)
    assert_that(response.reason_phrase).contains("syntax error")
    assert_that(response["Content-Type"]).contains("application/json")


@pytest.mark.django_db
def test_histogram_with_radial_velocity_detection(client):
    params = dict(f='"Radial Velocity" IN detection')
    response = client.get("/plots_request_ajax/", data=params)

    # Do we have a JSON response?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")


@pytest.mark.django_db
def test_legacy_polar_plot_is_reachable_via_get(client):
    response = client.get("/polar_plot/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        assert_that(soup.title.string).is_equal_to("Polar Plots")

        # Check if the div for plot is here
        diagram_image_div = soup.find("div", {"id": "diagram_image_id"})
        assert_that(diagram_image_div).is_not_none()

        # Check if the div "plothere" is here
        plothere_div = soup.find("div", {"id": "plothere"})
        assert_that(plothere_div).is_not_none()


@pytest.mark.django_db
@pytest.mark.slow
def test_legacy_polar_plot_content_via_get(client):
    response = client.get("/polar_plot/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # TODO add more test
    with soft_assertions():
        assert_that(soup.title.string).is_equal_to("Polar Plots")

        # Check if the div for plot is here
        diagram_image_div = soup.find("div", {"id": "diagram_image_id"})
        assert_that(diagram_image_div).is_not_none()

        # Check if the div "plothere" is here
        plothere_div = diagram_image_div.find("div", {"id": "plothere"})
        assert_that(plothere_div).is_not_none()

        # Check if data pass to the script contain the good values.
        script = soup.find(
            "script", {"src": "/static/catalog_legacy/js/Plots/polar.js"}
        )
        data_parameters = re.search(DATA_PARAMETERS_REGEX, str(script)).group(0)
        parameters = re.search(DATA_PARAMETERS_LIST_REGEX, data_parameters).group(0)
        python_data_parameters = eval(parameters)
        for item in python_data_parameters:
            if (related_model := item.get("related_model", None)) is not None:
                assert_that(related_model).is_in(*EXPECTED_RELATED_MODEL_IN_JS)


# TODO parametrize to test all parameters configurations
@pytest.mark.django_db
def test_legacy_polar_plot_ajax_is_reachable_via_get(client):
    response = client.get("/polar_plot_ajax/")

    # Do we have a JSON response?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/json")
    # TODO add more test about the json content
