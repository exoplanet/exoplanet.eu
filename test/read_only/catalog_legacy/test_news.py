# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions

# External imports
from bs4 import BeautifulSoup

# First party imports
from catalog_legacy.views import NewsListView

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.django_db
def test_legacy_news_detail_is_reachable_via_get(client, news):
    first_news = news[0]
    url = f"/news/news__{first_news.id}/"
    response = client.get(url, follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "News")

    # Examine jumbotron
    (
        assert_that(soup.select("section.jumbotron h1")[0].contents[0])
        .starts_with("News #")
        .ends_with(str(first_news.id))
    )
    (
        assert_that(soup.select("section.jumbotron p")[0].contents[0])
        .starts_with("Publication date:")
        .ends_with(str(first_news.date))
    )

    # Examine content
    assert_that(
        soup.select("section#news-detail-content p")[0].contents[0]
    ).is_equal_to(first_news.content)


@pytest.mark.django_db
def test_legacy_news_is_reachable_via_get(client):
    page_length = NewsListView.paginate_by
    news_title_css_selector = "section#news-content h5.card-title"

    response = client.get("/news/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "News")

    assert_that(soup.select(news_title_css_selector)).is_length(page_length)

    # Check the pagination
    assert_that(soup.select("nav>ul.pagination")).is_not_empty()


@pytest.mark.xfail(
    reason="we can't generate RSS feed if we don't have url for each news",
)
@pytest.mark.django_db
def test_legacy_rss_is_reachable_via_get(client):
    response = client.get("/news/rss/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains(
        "application/rss+xml", "charset=utf-8"
    )


@pytest.mark.django_db
@pytest.mark.slow
def test_legacy_news_hidden_news(client, hidden_news):
    news_css_selector = "section#news-content h5.card-text"
    response = client.get("/news/", follow=True)

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        all_news = soup.select(news_css_selector)
        for news in all_news:
            assert_that(news).does_not_contain("HIDDEN news")
