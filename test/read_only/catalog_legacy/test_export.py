# Standard imports
import csv
from http import HTTPStatus
from io import BytesIO, StringIO

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# External imports
import astropy
from numpy.ma import masked

# First party imports
# pylint: disable=no-name-in-module
from core.catalog_field_def import (
    OUTPUT_ALL_FIELDS,
    Column,
    get_nb_columns_including_errors_col,
)
from core.models import AllDetection, PlanetDBView, PublicationStatus


@pytest.mark.parametrize(
    "url, content_type",
    [
        ("/catalog/csv/", "text/csv"),
        ("/catalog/dat/", "text/tab-separated-values"),
    ],
)
@pytest.mark.parametrize("method", ["get", "post"])
@pytest.mark.django_db
def test_legacy_catalog_csv_is_reachable(
    client,
    url,
    # We need to add this fixture in order to have planets even if we executes this test
    # without all the read_only directory that has the conftest tha tadd all the planets
    # to the DB
    some_planets,  # pylint: disable=unused-argument
    content_type,
    method,
):
    response = client.get(url) if method == "get" else client.post(url)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains(content_type)
    assert_that(response.streaming).is_true()


@pytest.mark.parametrize(
    "url, separator",
    [
        param("/catalog/csv/", ",", id="from csv"),
        param("/catalog/dat/", "\t", id="from dat"),
    ],
)
@pytest.mark.parametrize("method", ["get", "post"])
@pytest.mark.django_db
def test_legacy_catalog_csv_content_is_valid(
    client,
    url,
    # We need to add this fixture in order to have planets even if we executes this test
    # without all the read_only directory that has the conftest tha tadd all the planets
    # to the DB
    some_planets,  # pylint: disable=unused-argument
    planet,  # pylint: disable=unused-argument
    get_number_of_planets,
    method,
    separator,
):
    response = client.get(url) if method == "get" else client.post(url)

    # We get the content after decoding it since it's not automatic
    csv_str = response.getvalue().decode(response.charset)

    # Let's examine the content
    csv_reader = csv.reader(StringIO(csv_str), delimiter=separator, strict=True)
    csv_data = [row for row in csv_reader]

    # The first row contains the headers
    headers = csv_data[0]
    nb_cols = len(headers)
    all_rows = csv_data[1:]

    param_csv_db_correspondencies = {}
    for field_def in OUTPUT_ALL_FIELDS:
        col = Column(field_def)
        csv_name = col.field_def.get("file_header", col.get_model_field())
        param_csv_db_correspondencies[csv_name] = col.get_model_field()
        if col.get_error() is not None:
            param_csv_db_correspondencies[f"{csv_name}_error_min"] = col.get_error()
            param_csv_db_correspondencies[f"{csv_name}_error_max"] = col.get_error()

    with soft_assertions():
        assert_that(len(csv_str)).is_greater_than(0)
        assert_that(headers).does_not_contain("")

        # We have the rows of data
        assert_that(all_rows).is_length(get_number_of_planets)
        assert_that(all(len(row) == nb_cols for row in all_rows)).is_true()

        # Contents
        for row in all_rows:
            planet_name = row[0]
            tested_planet = PlanetDBView.objects.filter(name=planet_name)[0]
            for idx, header in enumerate(headers):
                data_from_csv = row[idx]
                data_from_db = getattr(
                    tested_planet, param_csv_db_correspondencies[header]
                )

                def _adapt_data_from_db(header, db_data):
                    if db_data is None:
                        return ""

                    res = db_data

                    if header.endswith("error_min"):
                        res = db_data[0]
                    elif header.endswith("error_max"):
                        if len(db_data) == 1:
                            res = db_data[0]
                        else:
                            res = db_data[1]
                    elif header == "updated":
                        res = db_data.strftime("%Y-%m-%d")
                    elif isinstance(db_data, bool) and db_data is True:
                        res = "Yes"
                    elif isinstance(db_data, bool) and db_data is False:
                        res = "No"
                    elif header == "publication":
                        res = PublicationStatus.from_letter(db_data).value
                    elif header == "detection_type":
                        sub_res = []
                        for data in db_data:
                            sub_res.append(f"{AllDetection.label_from_int(data)}")
                        res = ", ".join(sub_res)
                    elif header in [
                        "star_detected_disc",
                        "mass_measurement_type",
                        "radius_measurement_type",
                    ]:
                        res = AllDetection.label_from_int(db_data)

                    return str(res) if res is not None else ""

                assert_that(data_from_csv).is_equal_to(
                    _adapt_data_from_db(header, data_from_db)
                )


@pytest.mark.parametrize("method", ["get", "post"])
@pytest.mark.django_db
def test_legacy_votable_is_reachable(
    client,
    method,
    # We need to add this fixture in order to have planets even if we executes this test
    # without all the read_only directory that has the conftest tha tadd all the planets
    # to the DB
    some_planets,  # pylint: disable=unused-argument
):
    url = "/catalog/votable/"

    response = client.get(url) if method == "get" else client.post(url)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("application/x-votable+xml")
    assert_that(response.streaming).is_true()


@pytest.mark.parametrize("method", ["get", "post"])
@pytest.mark.django_db
def test_legacy_votable_content_is_valid(
    client,
    method,
    # We need to add this fixture in order to have planets even if we executes this test
    # without all the read_only directory that has the conftest tha tadd all the planets
    # to the DB
    some_planets,  # pylint: disable=unused-argument
    get_number_of_planets,
):
    url = "/catalog/votable/"

    response = client.get(url) if method == "get" else client.post(url)

    # Let's examine the content
    xml_file = response.getvalue()

    # TODO: enable the strict verification with verify="exception"
    table = astropy.io.votable.parse_single_table(BytesIO(xml_file))
    expected_nb_cols = get_nb_columns_including_errors_col(OUTPUT_ALL_FIELDS)

    param_votable_db_correspondencies = {}
    for field_def in OUTPUT_ALL_FIELDS:
        col = Column(field_def)
        votable_name = col.field_def.get("file_header", col.get_model_field())
        param_votable_db_correspondencies[votable_name] = col.get_model_field()
        if col.get_error() is not None:
            param_votable_db_correspondencies[
                f"{votable_name}_error_min"
            ] = col.get_error()
            param_votable_db_correspondencies[
                f"{votable_name}_error_max"
            ] = col.get_error()

    with soft_assertions():
        assert_that(len(xml_file)).is_greater_than(0)

        # The first row contains the headers
        assert_that(table.fields).is_length(expected_nb_cols)

        # All headers have a description
        for field in table.fields:
            assert_that(field.description).is_not_empty()

        # We have the rows of data
        assert_that(get_number_of_planets).is_greater_than(0)
        assert_that(table.nrows).is_equal_to(get_number_of_planets)

        # Contents
        data = table.array
        for row in data:
            planet_name = row["name"]
            tested_planet = PlanetDBView.objects.filter(name=planet_name)[0]

            for idx, field in enumerate(table.fields):
                header = field.name
                data_from_votable = row[header]
                data_from_db = getattr(
                    tested_planet, param_votable_db_correspondencies[header]
                )

                def _adapt_data_from_db(header, db_data):
                    if db_data is None:
                        return None

                    res = db_data

                    if header.endswith("error_min"):
                        res = db_data[0]
                    elif header.endswith("error_max"):
                        if len(db_data) == 1:
                            res = db_data[0]
                        else:
                            res = db_data[1]
                    elif header == "updated":
                        res = db_data.strftime("%Y-%m-%d")
                    elif isinstance(db_data, bool) and db_data is True:
                        res = "Yes"
                    elif isinstance(db_data, bool) and db_data is False:
                        res = "No"
                    elif header == "publication":
                        res = PublicationStatus.from_letter(db_data).value
                    elif header == "detection_type":
                        sub_res = []
                        for data in db_data:
                            sub_res.append(f"{AllDetection.label_from_int(data)}")
                        res = ", ".join(sub_res)
                    elif header in [
                        "star_detected_disc",
                        "mass_measurement_type",
                        "radius_measurement_type",
                    ]:
                        res = AllDetection.label_from_int(db_data)

                    return res

                if data_from_db is None:
                    assert data_from_votable is masked or data_from_votable == "None"
                else:
                    assert_that(data_from_votable).is_equal_to(
                        _adapt_data_from_db(header, data_from_db)
                    )
