# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.django_db
def test_legacy_home_is_reachable_via_get(client):
    response = client.get("/home/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, current_page_name="Home")


@pytest.mark.django_db
def test_legacy_root_redirects_to_home(client):
    response = client.get("/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Did we have a redirection?
    assert_that(response.redirect_chain).is_equal_to(
        [
            ("/home/", HTTPStatus.MOVED_PERMANENTLY),
        ]
    )

    # Check if the menu is ok
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, current_page_name="Home")
