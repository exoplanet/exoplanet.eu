# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions

# External imports
from bs4 import BeautifulSoup

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.django_db
def test_legacy_links_is_reachable_via_get(client, links):
    site_link_css_selector = "section#links-content ul li.link a"

    response = client.get("/links/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "Links")
    assert_that(soup.select(site_link_css_selector)).is_length(len(links))


@pytest.mark.django_db
def test_legacy_sites_redirects_to_links(client):
    response = client.get("/sites/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Did we have a redirection?
    assert_that(response.redirect_chain).is_equal_to(
        [
            ("/links/", HTTPStatus.MOVED_PERMANENTLY),
        ]
    )


@pytest.mark.django_db
@pytest.mark.slow
def test_legacy_links_hidden_links(client, hidden_links):
    site_link_css_selector = "section#links-content ul li.link a"
    response = client.get("/links/", follow=True)

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        all_news = soup.select(site_link_css_selector)
        for news in all_news:
            assert_that(news).does_not_contain("HIDDEN link")
