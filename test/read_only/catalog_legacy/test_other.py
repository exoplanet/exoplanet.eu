# Standard imports
import random
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# First party imports
from editorial.models import Research
from editorial.models.choices import ResearchType

# Local imports
from ..utils import assert_menu_is_ok


@pytest.mark.django_db
def test_legacy_research_is_reachable_via_get(client):
    research_ground_css_selector = ".ground"
    research_space_css_selector = ".space"
    nb_research_ground = 12  # TODO retrieve number with real code
    nb_research_space = 8  # TODO retrieve number with real code

    # First let's create some research
    res_ground = [
        Research(type=ResearchType.GROUND, text=f"Ground research #{i}")
        for i in range(nb_research_ground)
    ]
    res_space = [
        Research(type=ResearchType.SPACE, text=f"Space research #{i}")
        for i in range(nb_research_space)
    ]

    # We randomize the different research to have a mix of different type
    all_res = res_ground + res_space
    random.shuffle(all_res)
    for res in all_res:
        res.save()

    response = client.get("/research/", follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, None)

    assert_that(soup.select(research_ground_css_selector)).is_length(nb_research_ground)
    assert_that(soup.select(research_space_css_selector)).is_length(nb_research_space)


# exoris_request_ajax/
# ephemeris/(?P<slug>[\w|\W]+)/
# i18n/
