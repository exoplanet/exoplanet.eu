# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that

# External imports
from bs4 import BeautifulSoup

# First party imports
from core.models.utils import obj_slugify

# Local imports
from ..utils import assert_menu_is_ok


def _test_planet_detail_is_reachable(client, planet) -> None:
    response = client.get(f"/catalog/{planet.url_keyword}/")

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Let's examine the content

    # We have a menu
    assert_menu_is_ok(soup, "Catalog")

    # We have a title
    assert_that(soup.h1.string).contains("Planet").contains(planet.name)

    sub_titles = soup.select("h2")

    assert_that(sub_titles).is_length(4)

    # We have planetary system part or a stars part
    # TODO write a assertpy extension .contains_one_of()
    # See: https://github.com/assertpy/assertpy#extension-system---adding-custom-assertions # noqa: E501
    assert_that(
        "System" in sub_titles[0].string or "Star" in sub_titles[0].string
    ).is_true()

    # We have an observability predictor part
    assert_that(sub_titles[1].a.string).contains("Observability Predictor")

    # We have a publications part
    assert_that(sub_titles[2].string).contains("Related publications")

    # Do we have the fields?
    assert_that(soup.select("table.planet_details tr")).is_not_empty()


DELTA_ERROR = 1.0e-2


# TODO test all other error case
# See: format_value_with_error() in core/templatetags/helpers.py
@pytest.mark.django_db
def test_planet_detail_shows_field_with_both_min_max_values(client, planet) -> None:
    response = client.get(f"/catalog/{planet.url_keyword}/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Do we have separate min and max values for axis?
    # (the test planet axis has a min and max error defined for its axis property)

    min_err_selector = "#planet-detail-basic-info dd.axis .err-min mn"
    max_err_selector = "#planet-detail-basic-info dd.axis .err-max mn"
    min_err_from_html = soup.select(min_err_selector)
    max_err_from_html = soup.select(max_err_selector)

    assert_that(min_err_from_html).is_length(1)
    assert_that(max_err_from_html).is_length(1)

    min_err = float(min_err_from_html[0].string)
    max_err = float(max_err_from_html[0].string)

    assert_that(min_err).is_positive()
    assert_that(max_err).is_positive()

    # Do we have the good values?

    assert_that(planet.axis_error).is_length(2)
    assert_that(min_err).is_close_to(planet.axis_error[0], DELTA_ERROR)
    assert_that(max_err).is_close_to(planet.axis_error[1], DELTA_ERROR)


@pytest.mark.django_db
def test_planet_detail_shows_field_with_relative_error_value(client, planet):
    response = client.get(f"/catalog/{planet.url_keyword}/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Do we have relative error for tperi?
    # (the test planet tperi has a relative error defined for its tperi property)

    rel_error_selector = "#planet-detail-basic-info dd.tperi .err-rel mn"
    rel_error_from_html = soup.select(rel_error_selector)

    assert_that(rel_error_from_html).is_length(1)

    rel_error = float(rel_error_from_html[0].string)

    assert_that(rel_error).is_positive()

    # Do we have the good values?

    assert_that(planet.tperi_error).is_length(1)
    assert_that(rel_error).is_close_to(planet.tperi_error[0], DELTA_ERROR)


@pytest.mark.django_db
def test_planet_detail_shows_field_with_inferior_value(client, planet):
    response = client.get(f"/catalog/{planet.url_keyword}/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Do we have an inferior value for period?
    # (the test planet period is an inferior value)

    lower_than_op_selector = "#planet-detail-basic-info dd.period mo"
    lower_than_op_from_html = soup.select(lower_than_op_selector)

    assert_that(lower_than_op_from_html).is_length(1)

    assert_that(lower_than_op_from_html[0].string).is_equal_to(">")


@pytest.mark.django_db
def test_planet_detail_shows_field_with_superior_value(client, planet):
    response = client.get(f"/catalog/{planet.url_keyword}/")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    # Do we have a superior value for eccentricity?
    # (the test planet eccentricity is an inferior value)

    greater_than_op_selector = "#planet-detail-basic-info dd.eccentricity mo"
    greater_than_op_from_html = soup.select(greater_than_op_selector)

    assert_that(greater_than_op_from_html).is_length(1)

    assert_that(greater_than_op_from_html[0].string).is_equal_to("<")


@pytest.mark.django_db
def test_planet_detail_old_name(client, planet):
    response = client.get(
        f"/catalog/{planet.name.replace(' ', '_').lower()}", follow=True
    )

    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    alert = soup.find_all("div", {"class": "alert alert-info"})
    assert_that(alert).is_not_empty()
    assert_that(str(alert)).contains(str(planet.name))


# TODO add some publications about the planet and it's star
@pytest.mark.django_db
@pytest.mark.xfail(reason="wait for new design to be finalized to update test logic")
def test_legacy_planet_detail_with_star_is_reachable_via_get(client, planet):
    _test_planet_detail_is_reachable(client, planet)


# TODO add some publications about the planet
@pytest.mark.django_db
@pytest.mark.xfail(reason="wait for new design to be finalized to update test logic")
def test_legacy_planet_detail_without_star_is_reachable_via_get(
    client,
    lonely_planet,
):
    _test_planet_detail_is_reachable(client, lonely_planet)


@pytest.mark.django_db
def test_legacy_planet_detail_is_redirected_if_no_trailing_slash(
    client,
    planet,
):
    url_without_trailing_slash = f"/catalog/{planet.url_keyword}"
    good_url = url_without_trailing_slash + "/"

    response = client.get(url_without_trailing_slash, follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Did we have a redirection?
    assert_that(response.redirect_chain).is_equal_to(
        [
            (good_url, HTTPStatus.MOVED_PERMANENTLY),
        ]
    )


@pytest.mark.django_db
def test_legacy_planet_detail_is_redirected_if_wrong_name_is_used(
    client,
    planet,
):
    fake_name = "toto"

    assert_that(fake_name).is_not_equal_to(planet.name)

    bad_keyword = obj_slugify(obj_name=fake_name, obj_id=planet.id)
    bad_url = f"/catalog/{bad_keyword}/"
    good_url = f"/catalog/{planet.url_keyword}/"

    response = client.get(bad_url, follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Did we have a redirection?
    assert_that(response.redirect_chain).is_equal_to(
        [
            (good_url, HTTPStatus.FOUND),
        ]
    )
