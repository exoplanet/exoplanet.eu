# Standard imports
from datetime import datetime as dt
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions

# External imports
from bs4 import BeautifulSoup

# First party imports
from catalog_legacy.views import BibliographyListView
from catalog_legacy.views.bibliography import filter_id_by_date
from core.models import Publication

# Local imports
from ..utils import assert_menu_is_ok, reverse_if_exist


def assert_jumbotron_is_ok_for_bibliography(soup: BeautifulSoup, current_btn_id: str):
    """
    Fail if the section tabs do not have link to all sections or if the current section
    is not considered active.

    Args:
        soup: a BeautifulSoup representation of the page
        current_btn_id: name of the section as it appears in the tab text

    Raises:
        AssertionError
    """
    section_item_css_selector = "section.jumbotron a.btn"
    active_item_class = "btn-primary"

    menu = {a["id"]: a for a in soup.select(section_item_css_selector)}
    section = "legacy_bibliography_section"

    assert_that(menu).is_length(4)
    assert_that(menu["jumbotron-btn-all-publications"]).has_href(
        reverse_if_exist(section, section="all")
    )
    assert_that(menu["jumbotron-btn-books"]).has_href(
        reverse_if_exist(section, section="books")
    )
    assert_that(menu["jumbotron-btn-thesis"]).has_href(
        reverse_if_exist(section, section="theses")
    )
    assert_that(menu["jumbotron-btn-reports"]).has_href(
        reverse_if_exist(section, section="reports")
    )

    # Do we have the correct menu active?
    assert_that(menu[current_btn_id]["class"]).contains(active_item_class)


# TODO test bibliography with get params title, author, year_start, year_end, page
@pytest.mark.parametrize(
    "url, btn_id",
    [
        ("/bibliography/books/", "jumbotron-btn-books"),
        ("/bibliography/theses/", "jumbotron-btn-thesis"),
        ("/bibliography/reports/", "jumbotron-btn-reports"),
        ("/bibliography/all/", "jumbotron-btn-all-publications"),
        ("/bibliography/", "jumbotron-btn-all-publications"),
    ],
)
@pytest.mark.django_db
def test_legacy_bibliography_is_reachable_via_get_with_publication_in_db(
    client,
    all_pub,
    url,
    btn_id,
):
    # precondition
    assert_that(list(Publication.objects.all())).is_not_empty()

    page_length = min(len(all_pub), BibliographyListView.paginate_by)
    pub_title_css_selector = "section#bibliography-content h5.card-title"

    response = client.get(url, follow=True)

    # Do we have a page?
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    # Let's examine the content
    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    assert_menu_is_ok(soup, "Bibliography")
    assert_jumbotron_is_ok_for_bibliography(soup, btn_id)

    assert_that(soup.select(pub_title_css_selector)).is_length(page_length)

    # Check the pagination
    assert_that(soup.select("nav>ul.pagination")).is_not_empty()


@pytest.mark.django_db
@pytest.mark.slow
def test_legacy_bibilography_hidden_publication(client, hidden_publication):
    pub_title_css_selector = "section#bibliography-content h5.card-title"
    response = client.get("/bibliography/", follow=True)

    with soft_assertions():
        assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
        assert_that(response["Content-Type"]).contains("text/html", "charset=utf-8")

    html = response.content.decode(response.charset)
    soup = BeautifulSoup(html, features="lxml")

    with soft_assertions():
        all_publications = soup.select(pub_title_css_selector)
        for pub in all_publications:
            assert_that(pub).does_not_contain("HIDDEN publication")


def _dty(year: int) -> dt:
    return dt(year, 1, 1)


# We build tuples with id = year since its easier to read/understand like this
IDS = [2001, 2002, 2003, 2004, 2005]


@pytest.mark.parametrize(
    "pub_dates, modif_dates",
    [
        (
            [_dty(2001), _dty(2002), _dty(2003), _dty(2004), _dty(2005)],
            [None, None, None, None, None],
        ),
        (
            [None, None, None, None, None],
            [_dty(2001), _dty(2002), _dty(2003), _dty(2004), _dty(2005)],
        ),
        (
            [_dty(2001), _dty(2002), _dty(2003), _dty(2004), _dty(2005)],
            [_dty(2001), _dty(2002), _dty(2003), _dty(2004), _dty(2005)],
        ),
        (
            [_dty(1801), _dty(1802), _dty(1803), _dty(1804), _dty(1805)],
            [_dty(2001), _dty(2002), _dty(2003), _dty(2004), _dty(2005)],
        ),
        (
            [_dty(2001), _dty(2002), _dty(2003), _dty(2004), _dty(2005)],
            [_dty(9001), _dty(9002), _dty(9003), _dty(9004), _dty(9005)],
        ),
    ],
)
@pytest.mark.parametrize(
    "start, end, expected",
    [
        (None, None, [2001, 2002, 2003, 2004, 2005]),
        (None, _dty(2005), [2001, 2002, 2003, 2004, 2005]),
        (_dty(2001), None, [2001, 2002, 2003, 2004, 2005]),
        (_dty(2001), _dty(2005), [2001, 2002, 2003, 2004, 2005]),
        (None, _dty(3000), [2001, 2002, 2003, 2004, 2005]),
        (_dty(1900), None, [2001, 2002, 2003, 2004, 2005]),
        (_dty(1900), _dty(3000), [2001, 2002, 2003, 2004, 2005]),
        (_dty(2002), _dty(2004), [2002, 2003, 2004]),
        (_dty(2003), _dty(2003), [2003]),
        (_dty(1900), _dty(2003), [2001, 2002, 2003]),
        (_dty(2003), _dty(3000), [2003, 2004, 2005]),
    ],
)
def test_filter_id_by_date_works(pub_dates, modif_dates, start, end, expected):
    in_data = list(zip(IDS, pub_dates, modif_dates))

    filtered = filter_id_by_date(in_data, start, end)

    assert_that(filtered).is_equal_to(expected)
