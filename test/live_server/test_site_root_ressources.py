# Pytest imports
# Test imports
import pytest

# External imports
import requests


@pytest.mark.slow
def test_favicon_files_are_reachable(live_server):
    FILES = [
        # Android, Chrome and Opera favicons
        "android-chrome-192x192.png",
        "android-chrome-512x512.png",
        # Apple touch favicons
        "apple-touch-icon-120x120.png",
        "apple-touch-icon-152x152.png",
        "apple-touch-icon-180x180.png",
        "apple-touch-icon-60x60.png",
        "apple-touch-icon-76x76.png",
        # Old school favicon
        "favicon.ico",
        # Browser favicons
        "favicon-16x16.png",
        "favicon-32x32.png",
        "favicon-48x48.png",
        "favicon_safari_pinned_tab.svg",
        # Windows 8 and 8.1
        "mstile-144x144.png",
        "mstile-150x150.png",
        "mstile-310x150.png",
        "mstile-310x310.png",
        "mstile-70x70.png",
        # Config files
        "browserconfiguration.xml",
        "site.webmanifest",
    ]

    for remote_file in FILES:
        url = f"{live_server.url}/{remote_file}"
        response = requests.get(url)

        # All possible redirect... since Django does not exactly follow the standards
        REDIRECT_STATUSES = [
            requests.codes.temporary_redirect,
            requests.codes.permanent_redirect,
            requests.codes.moved_permanently,
            requests.codes.found,
        ]

        assert response.status_code == requests.codes.ok, f"{url} not accessible."

        assert response.history
        first_rep = response.history[0]
        assert first_rep.status_code in REDIRECT_STATUSES, f"{url} not a redirection"
