# Standard imports
import collections
import mimetypes
from itertools import product
from typing import NamedTuple
from urllib.parse import urlparse

# Test imports
# Pytest imports
import pytest

# Django 💩 imports
from django.conf import settings

# External imports
import requests
from bs4 import BeautifulSoup, SoupStrainer


@pytest.fixture(scope="session")
def collected_file_src():
    return collections.Counter()


@pytest.mark.slow
def test_app_static_files_directly_are_reachable(live_server):
    PATHS_TO_TEST = [
        ("/static/canary/app-canary.png", "canary/static/app-canary.png"),
        (
            "/static/common/img/common-canary.png",
            "common/static/img/common-canary.png",
        ),
        (
            "/static/catalog/img/catalog-canary.png",
            "catalog/static/img/catalog-canary.png",
        ),
    ]

    for remote_path, local_path in PATHS_TO_TEST:
        # We need to use requests instead of builtin client since it does not support
        # correctly static files
        full_url = f"{live_server.url}{remote_path}"
        response = requests.get(full_url)

        # Can we access the file?
        assert response.status_code == requests.codes.ok
        assert response.headers["Content-Type"] == mimetypes.guess_type(full_url)[0]

        # Is it the same file?
        LOCAL_PATH = f"{settings.BASE_DIR}/{local_path}"
        assert response.content == open(LOCAL_PATH, "rb").read()


def _is_local_url(url: str) -> bool:
    """
    Tell if an url is local or remote.

    Args:
        url: a string containing a valid url

    Returns:
        whether or not the url is local (without a domaine and scheme)
    """
    scheme, netloc, _, _, _, _ = urlparse(url)
    return not scheme and not netloc


@pytest.mark.slow
def test_all_static_files_are_reachable(live_server, collected_file_src):
    """
    Test if all local resources linked in the pages are available as static resources.
    External resources are only allowed on very specific domains.
    """
    PAGES_TO_TEST = [
        "/canary/pagewithbasetemplate/",
        "/catalog_new/legal_notice/",
        "/",
        "/catalog/",
        "/plots/",
    ]

    ALLOWED_EXTERNAL_DOMAINS = ["img.shields.io"]

    class Resource(NamedTuple):
        name: str
        html_elem: str
        attr: str
        attrs_filter: dict[str, str] | None

    STATIC_LINKS_TO_SEARCH = [
        Resource(name="image", html_elem="img", attr="src", attrs_filter=None),
        Resource(
            name="css", html_elem="link", attr="href", attrs_filter={"type": "text/css"}
        ),
        Resource(
            name="favicon",
            html_elem="link",
            attr="href",
            attrs_filter={"type": "image/png"},
        ),
        Resource(name="javascript", html_elem="script", attr="src", attrs_filter=None),
    ]

    # live_server fixture is extremely slow especially if launched many times that's why
    # we try to avoid using parametrized tested here
    for page, (res_name, elem, attr, attrs_filter) in product(
        PAGES_TO_TEST, STATIC_LINKS_TO_SEARCH
    ):
        # We need to use requests instead of builtin client since it does not support
        # correctly static files
        full_url = f"{live_server.url}{page}"
        response = requests.get(full_url)
        assert response.status_code == requests.codes.ok
        html = response.text
        soup = BeautifulSoup(html, features="lxml", parse_only=SoupStrainer(elem))

        # Let's retrieve all the desired elements of the page
        all_elem = soup.find_all(elem, attrs=attrs_filter)
        assert (
            all_elem
        ), f"No <{elem}> matching filter {attrs_filter} found in page '{page}'."

        # ...and their source
        all_src = [elem[attr] for elem in all_elem if attr in elem.attrs]

        # Printing the collected url is very useful when something goes wrong. To show
        # this while running the texts use the -s option of pytest
        print(f"Collected images: {all_src}")

        for src in all_src:
            if not _is_local_url(src):
                parsed_url = urlparse(src)
                assert parsed_url.netloc in ALLOWED_EXTERNAL_DOMAINS
                assert parsed_url.scheme == "https"
                print(f"External resource {src} is in a valid domain.")
                # We don't test reachability of external resources to prevent tests from
                # relying on internet connectivity
                # TODO count the external resources
                continue

            # Now we only have local resources so we can test them
            if collected_file_src[src] == 0:
                full_src = f"{live_server.url}{src}"
                rep = requests.get(full_src)

                # Is the image source reachable
                assert (
                    rep.status_code == requests.codes.ok
                ), f"In page {page} remote {res_name} file {src} is not reachable"
            else:
                print(f"⚠️  {src} already tested.")

            collected_file_src[src] += 1

        tot_tested = len(collected_file_src)
        tot_col = sum(collected_file_src.values())
        print(f"{tot_tested}/{tot_col} files tested so far")
