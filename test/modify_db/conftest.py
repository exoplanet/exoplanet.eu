# Pytest imports
# Test imports
import pytest

# First party imports
from core.models import (
    AtmosphereMolecule,
    Import,
    Molecule,
    Planet,
    PlanetarySystem,
    Publication,
    Star,
)
from core.models.choices import (
    DetectedDiscDetection,
    MassMeasurement,
    MassUnit,
    PlanetDetection,
    PlanetStatus,
    PublicationType,
    RadiusMeasurement,
    RadiusUnit,
    WebStatus,
)


@pytest.fixture
def publication(
    db,  # pylint: disable=unused-argument invalid-name
) -> Publication:
    """A scientific publication with quite realistic characteristics."""
    the_publication = Publication(
        title="Quantization nature of planet distributions.",
        author="NIE Q. & CHENG C.",
        date="2001",
        type=PublicationType.THESIS,
        journal_name="ApJ. Letters",
        journal_volume="411 (Sept)",
        journal_page="114",
        bibcode="2007Rech..411..114V",
        keywords="test",
        reference="Cambridge University Press",
        url="<a href=papers/rubic.abs>abstract</a>",
        updated=None,  # Not used anymore, should be removed from DB
        status=WebStatus.ACTIVE,
        doi="10.26133/NEA1",
    )

    the_publication.full_clean()
    the_publication.save()

    return the_publication


@pytest.fixture
def molecule(
    db,  # pylint: disable=unused-argument invalid-name
) -> Molecule:
    """A Molecule with quite realistic characteristics."""
    the_molecule = Molecule(name="H2O", status=WebStatus.ACTIVE)

    the_molecule.full_clean()
    the_molecule.save()

    return the_molecule


@pytest.fixture
def atm_mol(
    db,  # pylint: disable=unused-argument invalid-name
    molecule,  # pylint: disable=redefined-outer-name
    planet,  # pylint: disable=redefined-outer-name
    publication,  # pylint: disable=redefined-outer-name
) -> AtmosphereMolecule:
    """A Molecule from a specific planet with quite realistic characteristics."""
    amol = AtmosphereMolecule(
        molecule_id=molecule.id,
        planet_id=planet.id,
        type=1,  # in AtmosphereMolecule.MOLECULE_DATA_TYPES
        result_value=None,
        result_value_error=[],
        result_figure="",
        data_source=1,  # in AtmosphereAbstract.DATA_SOURCES
        notes="He is used to constrain atmospheric mass loss rate to 10^9-10^11 g/s",
        publication_id=publication.id,
    )

    amol.full_clean()
    amol.save()

    return amol


@pytest.fixture
def planet(
    db,  # pylint: disable=unused-argument invalid-name
    star,  # pylint: disable=redefined-outer-name
) -> Planet:
    """A planet with quite realistic characteristics."""
    the_planet = Planet(
        # Compact info
        name="Kepler-11 g",
        period=118.37774,
        axis=0.462,
        eccentricity=0.352,
        inclination=163.1,
        angular_distance=0.000316,
        discovered=2012,
        status=WebStatus.ACTIVE,
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        # Full info
        axis_error=[0.0093, 0.0084],
        period_error=[0.05, 0.07],
        eccentricity_error=[0.6, 0.32],
        omega=316.4,
        omega_error=[2.8, 2.8],
        tzero_tr=2454975.6227,
        tzero_tr_error=[0.00012, 0.00011],
        tzero_vr=2457360.52,
        tzero_vr_error=[0.1],
        tperi=2457326.62183,
        tperi_error=[0.000319],
        tconj=2456416.40138,
        tconj_error=[0.00026],
        inclination_error=[0.32, 0.05],
        remarks=(
            "<b>28 Sep 2015 :</b> The planet 51 Eri b has been confirmed by "
            "astrometry of the planet position (De Rosa et al. 2015)"
        ),
        other_web=(
            '<a target="_blank" '
            'href="http://obswww.unige.ch/exoplanets/wasp14.html">'
            "Geneva Observatory data</a>"
        ),
        mass_measurement_type=MassMeasurement.TIMING,
        radius_measurement_type=RadiusMeasurement.THEORETICAL,
        tzero_tr_sec=2455561.549,
        tzero_tr_sec_error=[0.01],
        lambda_angle=151.0,
        lambda_angle_error=[23.0, 16.0],
        albedo=0.07,
        albedo_error=[0.03],
        temp_calculated=1451.0,
        temp_calculated_error=[70, 60],
        temp_measured=1600,
        temp_measured_error=[75, 65],
        hot_point_lon=1.0,
        log_g=4.35,
        impact_parameter=0.25,
        impact_parameter_error=[0.007],
        k=91.8,
        k_error=[4.7],
        # Actual planet info
        mass_sini_string="22.0",
        mass_sini_error_string=["0.15", "0.15"],
        mass_sini_unit=MassUnit.MJUP,
        mass_detected_string="2.75",
        mass_detected_error_string=["0.15", "0.15"],
        mass_detected_unit=MassUnit.MEARTH,
        radius_string="1.415",
        radius_error_string=["0.067", "0.084"],
        radius_unit=RadiusUnit.REARTH,
        publication_status=1,  # in Planet.PUBLICATION_STATUSES
        planet_status=PlanetStatus.CONFIRMED,
        main_star=star,
    )

    the_planet.full_clean()
    the_planet.save()

    # We need to manually add the main star to the stars of the planet
    the_planet.stars.add(star)  # pylint: disable=no-member

    return the_planet


@pytest.fixture
def plnt_sys(
    db,  # pylint: disable=unused-argument invalid-name
) -> PlanetarySystem:
    """A planetary system with quite realistic characteristics."""
    system = PlanetarySystem(
        ra=42.484952078,
        dec=-5.95983636252,
        distance=48.9,
        distance_error=[5.4, 4.4],
    )

    system.full_clean()
    system.save()

    return system


@pytest.fixture
def star(
    db,  # pylint: disable=unused-argument invalid-name
) -> Star:
    """A star with quite realistic characteristics."""
    the_star = Star(
        name="BD-10 3166",
        ra=164.6166667,
        dec=-10.7702778,
        distance=66.0,
        distance_error=[2.6, 2.7],
        spec_type="G4 V",
        magnitude_v=10.08,
        magnitude_i=15.72,
        magnitude_h=5.41,
        magnitude_j=5.74,
        magnitude_k=5.33,
        mass=1.4,
        mass_error=[0.012, 0.012],
        age=3.83,
        age_error=[1.2, 1.2],
        teff=6406.0,
        teff_error=[61, 65],
        radius=1.27,
        radius_error=[0.09, 0.09],
        metallicity=0.22,
        metallicity_error=[0.08],
        detected_disc=DetectedDiscDetection.IMAGING,
        magnetic_field=True,
        remarks="Alias 2MASS 19322220+4121198",
        other_web="Some raw text",
        url_simbad=(
            "http://simbad.u-strasbg.fr/simbad/sim-id?"
            "Ident=gsc+06214-00210&NbIdent=1&Radius=2"
            "&Radius.unit=arcmin&submit=submit+id"
        ),
        radvel_proj=18.22,
    )

    the_star.full_clean()
    the_star.save()

    return the_star


@pytest.fixture
def dbimport(
    db,  # pylint: disable=unused-argument invalid-name
) -> Import:
    """An Import."""
    test_dbimport = Import(
        name="import_test_01",
        external_csv=b"This is an external csv",
        mapping_yaml=b"This is a mapping file",
        internal_csv=b"This is an internal csv",
        exoimport_version="version-test",
    )

    test_dbimport.full_clean()
    test_dbimport.save()

    return test_dbimport
