# Standard imports
from datetime import date

# Test imports
# Pytest imports
import pytest

# First party imports
from editorial.models import Meeting


@pytest.mark.django_db
def test_meeting_can_be_created():
    workshop = Meeting(
        begin=date(2020, 3, 2),
        end=date(2020, 3, 6),
        title="Ground and space observatories: a joint venture to planetary sciences",
        place="Santiago, Chile",
    )

    assert workshop is not None

    workshop.save()

    # We check that the planet can be stored correcdtly in DB
    # pylint: disable-next=no-member
    workshop_from_db = Meeting.objects.get(id=workshop.id)

    # pylint: disable-next=no-member
    assert workshop_from_db.id == workshop.id
