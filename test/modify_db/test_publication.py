"""Tests of the different models of the core of exoplanet.eu project."""

# Standard imports
from datetime import datetime

# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# First party imports
from core.models import Publication


@pytest.mark.django_db
def test_publication_can_have_dbimport_fk(publication, dbimport):
    assert publication is not None

    # Add a FK to import table
    publication.dbimport = dbimport
    publication.save()

    assert publication.dbimport is not None
    assert publication.dbimport.name == "import_test_01"


@pytest.mark.parametrize(
    "date",
    [
        param(str(date), id=f"valid date = {date}")
        for date in range(1700, datetime.now().year + 2)
    ],
)
@pytest.mark.django_db
def test_publication_valid_dates(date):
    old_nb_publication = len(Publication.objects.all())

    new_pub = Publication(title="Fables", author="Bill Willingham", date=date, type=1)
    new_pub.full_clean()
    new_pub.save()

    with soft_assertions():
        assert_that(
            Publication.objects.filter(
                title="Fables", author="Bill Willingham", date=date
            )
        ).is_not_none()
        assert_that(len(Publication.objects.all())).is_equal_to(old_nb_publication + 1)
