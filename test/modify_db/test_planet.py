# Pytest imports
# Test imports
import pytest
from assertpy import assert_that

# First party imports
from core.models import AlternatePlanetName, Planet
from core.models.choices import PlanetDetection, PlanetStatus, WebStatus


@pytest.mark.django_db
@pytest.mark.parametrize(
    "webstatus,expected_active",
    [
        (WebStatus.ACTIVE, True),
        (WebStatus.HIDDEN, False),
        (WebStatus.IMPORTED, False),
        (WebStatus.SUGGESTED, False),
    ],
)
def test_planet_can_be_listed_via_planets_status_active(
    planet,
    webstatus,
    expected_active,
):
    planet.status = webstatus
    planet.save()

    is_active = planet in Planet.objects.filter(status=WebStatus.ACTIVE).all()

    assert is_active == expected_active


@pytest.mark.django_db
def test_planet_can_have_dbimport_fk(planet, dbimport):
    assert planet is not None

    # Add a FK to import table
    planet.dbimport = dbimport
    planet.save()

    assert planet.dbimport is not None
    assert planet.dbimport.name == "import_test_01"


@pytest.mark.django_db
def test_planet_can_be_deleted_and_its_alternate_name_erased(star):
    the_alt_name = "the alternate name"

    the_planet = Planet(
        # Compact info
        name="planet to remove",
        status=WebStatus.ACTIVE,
        detection_type=[PlanetDetection.PRIMARY_TRANSIT],
        publication_status=1,  # in Planet.PUBLICATION_STATUSES
        planet_status=PlanetStatus.CONFIRMED,
        main_star=star,
    )

    the_planet.full_clean()
    the_planet.save()
    # We need to manually add the main star to the stars of the planet
    the_planet.stars.add(star)  # pylint: disable=no-member

    the_alternate_name = AlternatePlanetName(name=the_alt_name, planet_id=the_planet.id)
    the_alternate_name.full_clean()
    the_alternate_name.save()
    the_planet.alternate_planet_names.add(  # pylint: disable=no-member
        the_alternate_name
    )

    # Check the alternate name exist before delete
    found_alt_before = AlternatePlanetName.objects.filter(  # pylint: disable=no-member
        name=the_alt_name
    )
    assert_that(found_alt_before).is_length(1)

    # Remove the planet
    the_planet.delete()

    # Check the alternate name don't exist after delete
    found_alt_after = AlternatePlanetName.objects.filter(  # pylint: disable=no-member
        name=the_alt_name
    )
    assert_that(found_alt_after).is_length(0)
