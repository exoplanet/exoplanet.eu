"""Tests of the different models of the core of exoplanet.eu project."""

# Standard imports
from datetime import datetime

# Test imports
# Pytest imports
import pytest

# First party imports
from core.models import AtmosphereMolecule, AtmosphereTemperature, Molecule
from core.models.choices import WebStatus


@pytest.mark.django_db
def test_molecule_can_be_created(molecule):
    assert molecule is not None

    mol_from_db = Molecule.objects.get(id=molecule.id)

    assert mol_from_db.id == molecule.id


@pytest.mark.django_db
def test_molecule_can_be_tracked(molecule):
    assert molecule.created is not None
    assert isinstance(molecule.created, datetime)

    assert molecule.modified is not None
    assert isinstance(molecule.modified, datetime)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "status,expected_visibility",
    [(WebStatus.ACTIVE, True), (WebStatus.HIDDEN, False), (WebStatus.IMPORTED, False)],
)
def test_molecule_can_be_visible_or_not(molecule, status, expected_visibility):
    assert molecule is not None

    molecule.status = status

    assert molecule.is_visible() == expected_visibility


@pytest.mark.django_db
@pytest.mark.usefixtures("star")
def test_atmosphere_molecule_can_be_created(
    atm_mol, molecule, planet, publication  # noqa: F811
):
    assert atm_mol is not None

    assert atm_mol.molecule.id == molecule.id
    assert atm_mol.planet.id == planet.id
    assert atm_mol.publication.id == publication.id

    atm_mol_from_db = AtmosphereMolecule.objects.get(id=atm_mol.id)

    assert atm_mol_from_db.id == atm_mol.id


@pytest.mark.django_db
def test_atmosphere_molecule_can_be_tracked(atm_mol):
    atm_mol.save()

    assert atm_mol.created is not None
    assert isinstance(atm_mol.created, datetime)

    assert atm_mol.modified is not None
    assert isinstance(atm_mol.modified, datetime)


@pytest.fixture
def atm_temp(publication, planet):  # noqa: F811
    return AtmosphereTemperature(
        publication_id=publication.id,
        planet_id=planet.id,
        type=1,  # AtmosphereTemperature.ATMOSPHERE_DATA_TYPES
        result_value=4050.0,
        result_value_error=[1.0, 2.0, 3.0],
        result_figure="",
        data_source=1,  # AtmosphereAbstract.DATA_SOURCES
        notes="Some meaningful note.",
    )


@pytest.mark.django_db
def test_atmosphere_temperature_can_be_created(
    atm_temp, publication, planet  # noqa: F811
):
    atm_temp.save()

    # We check the links
    assert atm_temp.publication.id == publication.id
    assert atm_temp.planet.id == planet.id

    # We check that the planet can be stored correcdtly in DB
    atm_temp_from_db = AtmosphereTemperature.objects.get(id=atm_temp.id)

    assert atm_temp_from_db.id == atm_temp.id


@pytest.mark.django_db
def test_atmosphere_temperature_can_be_tracked(atm_temp):
    atm_temp.save()

    assert atm_temp.created is not None
    assert isinstance(atm_temp.created, datetime)

    assert atm_temp.modified is not None
    assert isinstance(atm_temp.modified, datetime)
