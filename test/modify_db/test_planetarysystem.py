# Pytest imports
# Test imports
import pytest

# First party imports
from core.models import PlanetarySystem


@pytest.mark.django_db
def test_planetary_sytem_can_be_created_empty(plnt_sys):
    # We check the back links
    assert len(plnt_sys.planets.all()) == 0

    # We check that the planet can be stored correcdtly in DB
    syst_from_db = PlanetarySystem.objects.get(id=plnt_sys.id)

    assert syst_from_db.id == plnt_sys.id


@pytest.mark.django_db
def test_planetary_system_can_have_dbimport_fk(plnt_sys, dbimport):
    assert plnt_sys is not None

    # Add a FK to import table
    plnt_sys.dbimport = dbimport
    plnt_sys.save()

    assert plnt_sys.dbimport is not None
    assert plnt_sys.dbimport.name == "import_test_01"
