# Pytest imports
# Test imports
import pytest

# First party imports
from editorial.models import Link


@pytest.mark.django_db
def test_link_can_be_created():
    link = Link(
        value='<a target="_blank" href="http://www.exomol.com/"><b>ExoMol</b></a>'
    )

    assert link is not None

    link.save()

    # We check that the planet can be stored correcdtly in DB
    link_from_db = Link.objects.get(id=link.id)

    assert link_from_db.id == link.id
