# Standard imports
from http import HTTPStatus

# Test imports
# Pytest imports
import pytest

# External imports
from bs4 import BeautifulSoup

###############################################################################
# Helper functions to make tests writing less verbose/cumbersome


def _assert_is_reachable_html(response):
    """
    Test if a page is reachable and has a html content.

    Args:
        response(HttpResponse): http response returned by a ``client.get(url)`
            or `admin_client.get(url)`.
    """
    assert response.status_code == HTTPStatus.OK
    assert "text/html" in response["Content-Type"]


def _assert_has_django3_admin_header(html, charset):
    """
    Test if a page has the correct admin header

    Args:
        html(bytes): HTML content of the page to test.
        charset(str): string denoting the charset in which the page is encoded.
    """
    decoded_html = html.decode(charset)
    soup = BeautifulSoup(decoded_html, features="lxml")

    # Page head
    assert "Django site admin" in soup.title.string

    # Page header
    header = soup.find(id="header")
    assert header
    assert "Django administration" == header.h1.a.string

    # User tools
    user_tools_links = header.select("#user-tools a")
    assert len(user_tools_links) == 3
    vw_site, ch_pwd, logout = user_tools_links
    assert "View site" == vw_site.string
    assert "Change password" == ch_pwd.string
    assert "Log out" == logout.string

    # buttons = header.select("#user-tools button")
    # assert len(buttons) == 1
    # logout = buttons[0]


###############################################################################
# Tests themselves


@pytest.mark.django_db
def test_admin_page_without_login_redirect_to_login_page(client):
    response = client.get("/exoplanet_admin/")

    assert response.status_code == HTTPStatus.FOUND
    assert "text/html" in response["Content-Type"]

    assert "/exoplanet_admin/login/?next=/exoplanet_admin/" == response["Location"]


# TODO generalise this test by integrating it to all admin urls test
@pytest.mark.django_db
def test_admin_page_with_non_admin_login_fails(client, django_user_model):
    # Create a user to login
    username = "bob"
    password = "bob_password"
    django_user_model.objects.create_user(username=username, password=password)

    client.login(username=username, password=password)
    response = client.get("/exoplanet_admin/")

    assert response.status_code == HTTPStatus.FOUND
    assert "text/html" in response["Content-Type"]

    assert "/exoplanet_admin/login/?next=/exoplanet_admin/" == response["Location"]


@pytest.mark.django_db
def test_login_page_is_reachable(client):
    response = client.get("/exoplanet_admin/login/")

    _assert_is_reachable_html(response)

    html = response.content
    decoded_html = html.decode(response.charset)
    assert "username" in decoded_html
    assert "password" in decoded_html


@pytest.mark.django_db
def test_default_admin_page_is_unreachable_when_not_logged(client):
    response = client.get("/admin/")

    assert response.status_code == HTTPStatus.NOT_FOUND
    assert "text/html" in response["Content-Type"]


@pytest.mark.django_db
def test_default_admin_page_is_unreachable_by_admin_user(admin_client):
    response = admin_client.get("/admin/")

    assert response.status_code == HTTPStatus.NOT_FOUND
    assert "text/html" in response["Content-Type"]


@pytest.mark.parametrize(
    "url",
    [
        "/exoplanet_admin/",
        "/exoplanet_admin/auth/",
        "/exoplanet_admin/flatpages/",
        "/exoplanet_admin/sites/",
    ],
)
@pytest.mark.django_db
def test_admin_basic_pages_are_reachable(admin_client, url):
    response = admin_client.get(url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


@pytest.mark.parametrize(
    "url",
    [
        "/exoplanet_admin/core/",
        "/exoplanet_admin/editorial/",
    ],
)
@pytest.mark.django_db
def test_admin_django_app_pages_are_reachable(admin_client, url):
    response = admin_client.get(url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


AUTH_PAGES_URLS = [
    "/exoplanet_admin/auth/group/",
    "/exoplanet_admin/auth/user/",
]


@pytest.mark.parametrize("url", AUTH_PAGES_URLS)
@pytest.mark.django_db
def test_admin_authentication_and_authorization_pages_are_reachable(admin_client, url):
    response = admin_client.get(url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


@pytest.mark.parametrize("url", AUTH_PAGES_URLS)
@pytest.mark.django_db
def test_admin_authentication_and_authorization_add_pages_are_reachable(
    admin_client,
    url,
):
    add_url = "{}add/".format(url)

    response = admin_client.get(add_url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


# TODO split by apps ?
EXOPLANET_APP_PAGES_URLS = [
    "/exoplanet_admin/editorial/link/",
    "/exoplanet_admin/editorial/meeting/",
    "/exoplanet_admin/editorial/news/",
    "/exoplanet_admin/editorial/research/",
    "/exoplanet_admin/core/molecule/",
    "/exoplanet_admin/core/planetarysystem/",
    "/exoplanet_admin/core/planet/",
    "/exoplanet_admin/core/publication/",
    "/exoplanet_admin/core/star/",
]


@pytest.mark.parametrize("url", EXOPLANET_APP_PAGES_URLS)
@pytest.mark.django_db
def test_admin_exoplanet_app_pages_are_reachable(admin_client, url):
    response = admin_client.get(url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


# TODO: voir ce test
@pytest.mark.parametrize("url", EXOPLANET_APP_PAGES_URLS)
@pytest.mark.django_db
# @pytest.mark.xfail(reason="BUG ??")
# https://stackoverflow.com/questions/49814669/django-call-missing-1-required-keyword-only-argument-manager
def test_admin_exoplanet_app_add_pages_are_reachable(admin_client, url):
    add_url = "{}add/".format(url)

    response = admin_client.get(add_url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


@pytest.mark.parametrize(
    "url",
    [
        "/exoplanet_admin/flatpages/flatpage/",
        "/exoplanet_admin/sites/site/",
    ],
)
@pytest.mark.django_db
def test_admin_other_pages_are_reachable(admin_client, url):
    response = admin_client.get(url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)


@pytest.mark.parametrize(
    "url",
    [
        pytest.param("/exoplanet_admin/flatpages/flatpage/"),
        "/exoplanet_admin/sites/site/",
    ],
)
@pytest.mark.django_db
def test_admin_other_add_pages_are_reachable(admin_client, url):
    add_url = "{}add/".format(url)

    response = admin_client.get(add_url)

    _assert_is_reachable_html(response)
    _assert_has_django3_admin_header(response.content, response.charset)
