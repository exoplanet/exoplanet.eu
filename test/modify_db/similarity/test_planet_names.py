# Pytest imports
# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# First party imports
from core.models import (
    name_already_used_as_planet_name,
    planet_name_already_used_as_alternate_name,
)

# Local imports
from .utils import insert_alternate_planet_name_for_simi, insert_planet_for_simi


@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_names__already_used_as_planet_name__not_similar():
    insert_planet_for_simi(name="I hate django")

    res, error_msg = name_already_used_as_planet_name("toto")
    with soft_assertions():
        assert_that(res).is_false()
        assert_that(error_msg).is_empty()


@pytest.mark.parametrize(
    "name",
    [
        param("I hate django", id="exctly same"),
        param("i hate django", id="lower case"),
        param("I HATE DJANGO", id="upper case"),
        param("I hate django", id="title case"),
        param("i hATe DjaNgo", id="random case"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_names__already_used_as_planet_name__similar(name):
    insert_planet_for_simi(name="I hate django")

    res, error_msg = name_already_used_as_planet_name(name)
    with soft_assertions():
        assert_that(res).is_true()
        assert_that(error_msg).is_not_empty()


@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_names__already_used_as_alternate_planet_name__not_similar():
    insert_alternate_planet_name_for_simi(name="I hate django")

    res, error_msg = planet_name_already_used_as_alternate_name("toto")
    with soft_assertions():
        assert_that(res).is_false()
        assert_that(error_msg).is_empty()


@pytest.mark.parametrize(
    "name",
    [
        param("I hate django", id="exctly same"),
        param("i hate django", id="lower case"),
        param("I HATE DJANGO", id="upper case"),
        param("I hate django", id="title case"),
        param("i hATe DjaNgo", id="random case"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_names__already_used_as_alternate_planet_name__similar(name):
    insert_alternate_planet_name_for_simi(name="I hate django")

    res, error_msg = planet_name_already_used_as_alternate_name(name)
    with soft_assertions():
        assert_that(res).is_true()
        assert_that(error_msg).is_not_empty()
