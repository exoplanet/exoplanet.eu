# Standard imports
from decimal import Decimal

# Test imports
# Pytest imports
import pytest
from pytest import param

# Local imports
from .utils import assert_is_similar, insert_star_for_simi


@pytest.mark.parametrize(
    "ra, dec",
    [
        param(
            Decimal("219.3375"),  # ra
            Decimal("-24.95861111111111"),  # dec
            id="param from TOI-3127 db id=7060 mentionned in bug #121",
        ),
        param(
            Decimal("134.958333333333345"),  # ra
            Decimal("-48.48840555555556"),  # dec
            id="param from TOI-2368 db id=7069 mentionned in bug #121",
        ),
        param(
            Decimal("205.35416666666666"),  # ra
            Decimal("17.68722222222222"),  # dec
            id="param from TOI-1855 db id=7067 mentionned in bug #121",
        ),
        param(
            Decimal("219.3375"),  # ra
            Decimal("-24.95861111111111"),  # dec
            id="param from TOI-3127 db id=7060 mentionned in bug #121",
        ),
        param(
            Decimal("57.3375"),  # ra
            Decimal("-16.75583333333333"),  # dec
            id="param from TOI-2416 db id=7056 mentionned in bug #121",
        ),
        param(
            Decimal("61.6875"),  # ra
            Decimal("-80.80378574223857"),  # dec
            id="param from TOI-2373 db id=7053 mentionned in bug #121",
        ),
        param(
            Decimal("234.57340576353246"),  # ra
            Decimal("-80.80378574223857"),  # dec
            id="param from TOI-913 db id=6835 mentionned in bug #121",
        ),
        param(
            Decimal("142.17328792322346"),  # ra
            Decimal("-12.165486864666425"),  # dec
            id="param from TOI-620 db id=6486 mentionned in bug #121",
        ),
        param(
            Decimal("347.979166685"),  # ra
            Decimal("33.047500012"),  # dec
            id="param from TOI-6135 db id=7051 mentionned in bug #121",
        ),
        param(
            Decimal("317.900000012"),  # ra
            Decimal("68.401944454"),  # dec
            id="param from TOI-6034 db id=7046 mentionned in bug #121",
        ),
        param(
            Decimal("340.0"),  # ra
            Decimal("60.823611133"),  # dec
            id="param from TOI-6022 db id=7043 mentionned in bug #121",
        ),
        param(
            Decimal("325.300000004"),  # ra
            Decimal("9.599166691"),  # dec
            id="param from TOI-5916 db id=7042 mentionned in bug #121",
        ),
        param(
            Decimal("59.016666668"),  # ra
            Decimal("3.6100000199999998"),  # dec
            id="param from TOI-4198 db id=7035 mentionned in bug #121",
        ),
        param(
            Decimal("21.187500015"),  # ra
            Decimal("21.513055576"),  # dec
            id="param from TOI-4515 db id=7019 mentionned in bug #121",
        ),
        param(
            Decimal("328.908333346"),  # ra
            Decimal("28.179444458"),  # dec
            id="param from TOI-3540 db id=6537 mentionned in bug #121",
        ),
        param(
            Decimal("292.16666668"),  # ra
            Decimal("53.487500012999995"),  # dec
            id="param from TOI-2010 db id=7015 mentionned in bug #121",
        ),
        param(
            Decimal("43.433333348"),  # ra
            Decimal("69.101388892"),  # dec
            id="param from TOI-1736 db id=6936 mentionned in bug #121",
        ),
        param(
            Decimal("164.933333348"),  # ra
            Decimal("-82.221388897"),  # dec
            id="param from TOI-4377 db id=7011 mentionned in bug #121",
        ),
        param(
            Decimal("186.275000002"),  # ra
            Decimal("60.418333343"),  # dec
            id="param from TOI-2068 db id=7007 mentionned in bug #121",
        ),
        param(
            Decimal("164.112500009"),  # ra
            Decimal("-72.985277799"),  # dec
            id="param from TOI-771 db id=7004 mentionned in bug #121",
        ),
        param(
            Decimal("349.229166685"),  # ra
            Decimal("-18.606666684"),  # dec
            id="param from TOI-238 db id=7003 mentionned in bug #121",
        ),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_for_acos_out_of_range_bug_121(
    ra,
    dec,
):
    """
    See: BUG https://gitlab.obspm.fr/exoplanet/exoplanet.eu/-/issues/121
    """
    # inserting does not raise but...
    insert_star_for_simi(ra=ra, dec=dec)

    # ...simply testing similarity of the star should not raise because of acosd()
    # applied out of range
    assert_is_similar(ra, dec, None)
