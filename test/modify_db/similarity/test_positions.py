# Standard imports
from decimal import Decimal

# Test imports
# Pytest imports
import pytest
from pytest import param

# External imports
from dotmap import DotMap

# Local imports
from .utils import (
    assert_is_not_similar,
    assert_is_similar,
    assert_similarity,
    insert_star_for_simi,
)

SMALL_DELTA = Decimal("0.0001")

# The real number for BIG_DELTA is 0.00028 but postgresql make its trigonometric
# functions with double precision (float) and not Numeric (Decimal), 0.000363 is the
# number that works and is closest to the true delta.
BIG_DELTA = Decimal("0.000363")

NO_DELTA = Decimal("0.0000")

# We choose the numbering of octants with the Gay code.
# For more details on the naming and numbering of octants:
# https://en.wikipedia.org/wiki/Octant_(solid_geometry)
OCTANTS = [
    DotMap({"ra": Decimal("40"), "dec": Decimal("40")}),  # 0
    DotMap({"ra": Decimal("130"), "dec": Decimal("40")}),  # 1
    DotMap({"ra": Decimal("220"), "dec": Decimal("40")}),  # 2
    DotMap({"ra": Decimal("310"), "dec": Decimal("40")}),  # 3
    DotMap({"ra": Decimal("310"), "dec": Decimal("-40")}),  # 4
    DotMap({"ra": Decimal("220"), "dec": Decimal("-40")}),  # 5
    DotMap({"ra": Decimal("130"), "dec": Decimal("-40")}),  # 6
    DotMap({"ra": Decimal("40"), "dec": Decimal("-40")}),  # 7
]

SPECIAL_CASES = [
    DotMap({"ra": Decimal("50"), "dec": Decimal("90")}),
    DotMap({"ra": Decimal("50"), "dec": Decimal("-90")}),
]


@pytest.mark.xfail(
    reason="Trigonometric function postgresql take and return floating number "
    "(double precision) and not Decimal (numeric). "
    "They are converting these functions to numeric.",
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__real_delta():
    ra_oct_0 = OCTANTS[0].ra
    dec_oct_0 = OCTANTS[0].dec

    insert_star_for_simi(ra=ra_oct_0, dec=dec_oct_0)
    assert_is_not_similar(ra_oct_0 + Decimal("0.00028"), dec_oct_0, None)


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(SPECIAL_CASES[0], id="dec = 90"),
        param(SPECIAL_CASES[1], id="dec = -90"),
    ],
)
@pytest.mark.parametrize(
    "delta_ra",
    [
        param(Decimal(+SMALL_DELTA), id="little more ra"),
        param(Decimal(-SMALL_DELTA), id="little less ra"),
        param(Decimal(+BIG_DELTA), id="much more ra"),
        param(Decimal(-BIG_DELTA), id="much less ra"),
        param(Decimal(NO_DELTA), id="exactly same position"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__special_case_dec_is_90(star_at_octant, delta_ra):
    insert_star_for_simi(ra=star_at_octant.ra, dec=star_at_octant.dec)
    assert_is_similar(star_at_octant.ra + delta_ra, star_at_octant.dec, None)


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(OCTANTS[0], id="octan 0"),
        param(OCTANTS[1], id="octan 1"),
        param(OCTANTS[2], id="octan 2"),
        param(OCTANTS[3], id="octan 3"),
        param(OCTANTS[4], id="octan 4"),
        param(OCTANTS[5], id="octan 5"),
        param(OCTANTS[6], id="octan 6"),
        param(OCTANTS[7], id="octan 7"),
    ],
)
@pytest.mark.parametrize(
    "delta_ra, delta_dec, expected",
    [
        # RA
        param(Decimal(+SMALL_DELTA), Decimal(NO_DELTA), True, id="bit more ra"),
        param(Decimal(-SMALL_DELTA), Decimal(NO_DELTA), True, id="little less ra"),
        param(Decimal(+BIG_DELTA), Decimal(NO_DELTA), False, id="much more ra"),
        param(Decimal(-BIG_DELTA), Decimal(NO_DELTA), False, id="much less ra"),
        # DEC
        param(Decimal(NO_DELTA), Decimal(+SMALL_DELTA), True, id="bit more dec"),
        param(Decimal(NO_DELTA), Decimal(-SMALL_DELTA), True, id="little less dec"),
        param(Decimal(NO_DELTA), Decimal(+BIG_DELTA), False, id="much more dec"),
        param(Decimal(NO_DELTA), Decimal(-BIG_DELTA), False, id="much less dec"),
        # RA & DEC
        param(
            Decimal(+SMALL_DELTA),
            Decimal(+SMALL_DELTA),
            True,
            id="bit more ra & dec",
        ),
        param(
            Decimal(-SMALL_DELTA),
            Decimal(-SMALL_DELTA),
            True,
            id="litte less ra & dec",
        ),
        param(Decimal(+BIG_DELTA), Decimal(+BIG_DELTA), False, id="much more ra & dec"),
        param(Decimal(-BIG_DELTA), Decimal(-BIG_DELTA), False, id="much less ra & dec"),
        param(
            Decimal(-SMALL_DELTA),
            Decimal(+SMALL_DELTA),
            True,
            id="litlle less ra & bit more dec",
        ),
        param(
            Decimal(-BIG_DELTA),
            Decimal(+BIG_DELTA),
            False,
            id="much less ra & much more dec",
        ),
        param(
            Decimal(+SMALL_DELTA),
            Decimal(-SMALL_DELTA),
            True,
            id="bit more ra & litlle less dec",
        ),
        param(
            Decimal(+BIG_DELTA),
            Decimal(-BIG_DELTA),
            False,
            id="much more ra & much less dec",
        ),
        # Exactly same position
        param(Decimal(NO_DELTA), Decimal(NO_DELTA), True, id="exactly same position"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__at_the_same_pos(
    star_at_octant, delta_ra, delta_dec, expected
):
    insert_star_for_simi(ra=star_at_octant.ra, dec=star_at_octant.dec)
    assert_similarity(
        star_at_octant.ra + delta_ra,
        star_at_octant.dec + delta_dec,
        None,
        expected,
    )


@pytest.mark.parametrize(
    "star_one, star_two",
    [
        # Octant 0
        param(OCTANTS[0], OCTANTS[1], id="0 and 1"),
        param(OCTANTS[0], OCTANTS[2], id="0 and 2"),
        param(OCTANTS[0], OCTANTS[3], id="0 and 3"),
        param(OCTANTS[0], OCTANTS[4], id="0 and 4"),
        param(OCTANTS[0], OCTANTS[5], id="0 and 5"),
        param(OCTANTS[0], OCTANTS[6], id="0 and 6"),
        param(OCTANTS[0], OCTANTS[7], id="0 and 7"),
        # Octant 1
        param(OCTANTS[1], OCTANTS[2], id="1 and 2"),
        param(OCTANTS[1], OCTANTS[3], id="1 and 3"),
        param(OCTANTS[1], OCTANTS[4], id="1 and 4"),
        param(OCTANTS[1], OCTANTS[5], id="1 and 5"),
        param(OCTANTS[1], OCTANTS[6], id="1 and 6"),
        param(OCTANTS[1], OCTANTS[7], id="1 and 7"),
        # Octant 2
        param(OCTANTS[2], OCTANTS[3], id="2 and 3"),
        param(OCTANTS[2], OCTANTS[4], id="2 and 4"),
        param(OCTANTS[2], OCTANTS[5], id="2 and 5"),
        param(OCTANTS[2], OCTANTS[6], id="2 and 6"),
        param(OCTANTS[2], OCTANTS[7], id="2 and 7"),
        # Octant 3
        param(OCTANTS[3], OCTANTS[4], id="3 and 4"),
        param(OCTANTS[3], OCTANTS[5], id="3 and 5"),
        param(OCTANTS[3], OCTANTS[6], id="3 and 6"),
        param(OCTANTS[3], OCTANTS[7], id="3 and 7"),
        # Octant 4
        param(OCTANTS[4], OCTANTS[5], id="4 and 5"),
        param(OCTANTS[4], OCTANTS[6], id="4 and 6"),
        param(OCTANTS[4], OCTANTS[7], id="4 and 7"),
        # Octant 5
        param(OCTANTS[5], OCTANTS[6], id="5 and 6"),
        param(OCTANTS[5], OCTANTS[7], id="5 and 7"),
        # Octant 6
        param(OCTANTS[6], OCTANTS[7], id="6 and 7"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__same_pos_in_different_octants(star_one, star_two):
    insert_star_for_simi(ra=star_one.ra, dec=star_one.dec)
    assert_is_not_similar(star_two.ra, star_two.dec, None)


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(OCTANTS[0], id="octan 0"),
        param(OCTANTS[1], id="octan 1"),
        param(OCTANTS[2], id="octan 2"),
        param(OCTANTS[3], id="octan 3"),
        param(OCTANTS[4], id="octan 4"),
        param(OCTANTS[5], id="octan 5"),
        param(OCTANTS[6], id="octan 6"),
        param(OCTANTS[7], id="octan 7"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__at_the_same_pos_modify_object_same_obj(star_at_octant):
    star_id = insert_star_for_simi(ra=star_at_octant.ra, dec=star_at_octant.dec)
    assert_is_not_similar(
        star_at_octant.ra,
        star_at_octant.dec,
        star_id,
    )


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(SPECIAL_CASES[0], id="dec = 90"),
        param(SPECIAL_CASES[1], id="dec = -90"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__special_case_dec_is_90_modify_object_same_obj(
    star_at_octant,
):
    star_id = insert_star_for_simi(ra=star_at_octant.ra, dec=star_at_octant.dec)
    assert_is_not_similar(star_at_octant.ra, star_at_octant.dec, star_id)


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(OCTANTS[0], id="octan 0"),
        param(OCTANTS[1], id="octan 1"),
        param(OCTANTS[2], id="octan 2"),
        param(OCTANTS[3], id="octan 3"),
        param(OCTANTS[4], id="octan 4"),
        param(OCTANTS[5], id="octan 5"),
        param(OCTANTS[6], id="octan 6"),
        param(OCTANTS[7], id="octan 7"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__at_the_same_pos_modify_object_not_the_same_obj(
    star_at_octant,
):
    star_id = insert_star_for_simi(ra=star_at_octant.ra, dec=star_at_octant.dec)
    assert_is_similar(
        star_at_octant.ra,
        star_at_octant.dec,
        star_id + 1,
    )


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(SPECIAL_CASES[0], id="dec = 90"),
        param(SPECIAL_CASES[1], id="dec = -90"),
    ],
)
@pytest.mark.django_db(transaction=True)
@pytest.mark.slow
def test_similarity_positions__special_case_dec_is_90_modify_object_not_the_same_obj(
    star_at_octant,
):
    star_id = insert_star_for_simi(ra=star_at_octant.ra, dec=star_at_octant.dec)
    assert_is_similar(star_at_octant.ra, star_at_octant.dec, star_id + 1)
