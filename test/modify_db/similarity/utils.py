# Standard imports
from decimal import Decimal

# Test imports
# Pytest imports
import pytest
from assertpy import assert_that, soft_assertions

# First party imports
from core.models import (
    AlternatePlanetName,
    AlternateStarName,
    Planet,
    Star,
    is_at_the_same_position_of_other_star,
)


@pytest.mark.django_db
def insert_star_for_simi(
    name: str = "toto",
    ra: Decimal = Decimal("160"),
    dec: Decimal = Decimal("40"),
) -> None:
    """
    Insert star for similarity tests.

    Args:
        name: Name of the star to insert.
        ra: Ra of the star to insert.
        dec: Dec of the star to insert.
    """
    my_star = Star(name=name, ra=ra, dec=dec)
    my_star.full_clean()
    my_star.save()
    return my_star.id  # pylint: disable=no-member


@pytest.mark.django_db
def insert_alternate_star_name_for_simi(name: str) -> None:
    """
    Insert star for similarity tests.

    Args:
        name: Name of the star to insert.
    """
    star_id = insert_star_for_simi(name="tutu3000")

    my_alter_star_name = AlternateStarName(name=name, star_id=star_id)
    my_alter_star_name.full_clean()
    my_alter_star_name.save()


@pytest.mark.django_db
def insert_planet_for_simi(
    name: str = "toto",
) -> None:
    """
    Insert planet for similarity tests.

    Args:
        name: Name of the planet to insert.
    """
    star_id = insert_star_for_simi()
    my_star = Star.objects.filter(pk=star_id)

    my_planet = Planet(
        name=name,
        detection_type=[4],
        publication_status=1,
        main_star=my_star[0],
    )
    my_planet.full_clean()
    my_planet.save()
    return my_planet.id


@pytest.mark.django_db
def insert_alternate_planet_name_for_simi(name: str) -> None:
    """
    Insert planet for similarity tests.

    Args:
        name: Name of the planet to insert.
    """
    planet_id = insert_planet_for_simi(name="tutu3000")

    my_alter_planet_name = AlternatePlanetName(name=name, planet_id=planet_id)
    my_alter_planet_name.full_clean()
    my_alter_planet_name.save()


def assert_is_not_similar(ra: Decimal, dec: Decimal, star_id: int | None) -> None:
    """
    Assert is not similar for star similarity.

    Args:
        ra: Ra of the new star.
        dec: Dec of the new star.
        star_id: Id of the star. Can be None for a new star.
    """
    res, error_msg = is_at_the_same_position_of_other_star(
        ra,
        dec,
        star_id,
    )

    with soft_assertions():
        assert_that(res).is_false()
        assert_that(error_msg).is_empty()


def assert_is_similar(ra: Decimal, dec: Decimal, star_id: int | None) -> None:
    """
    Assert is not similar for star similarity.

    Args:
        ra: Ra of the new star.
        dec: Dec of the new star.
        star_id: Id of the star. Can be None for a new star.
    """
    res, error_msg = is_at_the_same_position_of_other_star(
        ra,
        dec,
        star_id,
    )

    with soft_assertions():
        assert_that(res).is_true()
        assert_that(error_msg).is_not_empty()


def assert_similarity(
    ra: Decimal,
    dec: Decimal,
    star_id: int | None,
    expected: bool,
) -> None:
    """
    Assert is not similar for star similarity.

    Args:
        ra: Ra of the new star.
        dec: Dec of the new star.
        star_id: Id of the star. Can be None for a new star.
        expected: True for similar star, False otherwise.
    """
    if expected:
        assert_is_similar(ra, dec, star_id)
    else:
        assert_is_not_similar(ra, dec, star_id)
