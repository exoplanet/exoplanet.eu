# Standard imports
from datetime import date

# Test imports
# Pytest imports
import pytest

# First party imports
from editorial.models import News
from editorial.models.choices import NewsLanguage


@pytest.mark.django_db
def test_news_can_be_created():
    breaking_news = News(
        date=date(2004, 2, 18),
        content=(
            ' First "japanese planet candidate" around '
            '<a href="./planet.php?p1=HD+104985&p2=b">HD 104985</a> (Sato et al)'
        ),
        lang=NewsLanguage.EN,
    )

    assert breaking_news is not None

    breaking_news.save()

    # We check that the planet can be stored correcdtly in DB
    breaking_news_from_db = News.objects.get(id=breaking_news.id)

    assert breaking_news_from_db.id == breaking_news.id
