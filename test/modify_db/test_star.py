# Pytest imports
# Test imports
import pytest
from assertpy import assert_that

# First party imports
from core.models import AlternateStarName, Star


@pytest.mark.django_db
def test_star_can_have_dbimport_fk(star, dbimport):
    assert star is not None

    # Add a FK to import table
    star.dbimport = dbimport
    star.save()

    assert star.dbimport is not None
    assert star.dbimport.name == "import_test_01"


@pytest.mark.django_db
def test_star_can_be_deleted_and_its_alternate_name_erased():
    the_alt_name = "the alternate name"

    the_star = Star(
        name="star to remove",
        ra=164.6166667,
        dec=-10.7702778,
    )

    the_star.full_clean()
    the_star.save()

    the_alternate_name = AlternateStarName(
        name=the_alt_name,
        star_id=the_star.id,  # pylint: disable=no-member
    )
    the_alternate_name.full_clean()
    the_alternate_name.save()
    the_star.alternate_star_names.add(the_alternate_name)  # pylint: disable=no-member

    # Check the alternate name exist before delete
    found_alt_before = AlternateStarName.objects.filter(  # pylint: disable=no-member
        name=the_alt_name
    )
    assert_that(found_alt_before).is_length(1)

    # Remove the planet
    the_star.delete()

    # Check the alternate name don't exist after delete
    found_alt_after = AlternateStarName.objects.filter(  # pylint: disable=no-member
        name=the_alt_name
    )
    assert_that(found_alt_after).is_length(0)
