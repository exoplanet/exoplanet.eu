/* global test, expect */

const lib = require("../../../../disk/static/js/utils_init_table_catalog")

test.each([
	{ labels: { short: "to.", long: "", full_ascii: "" }, name: "toto", expected: "to." },
	{ labels: { short: "", long: "toto toto", full_ascii: "" }, name: "toto", expected: "toto toto" },
	{ labels: { short: "", long: "", full_ascii: "toto_toto" }, name: "toto", expected: "toto_toto" },
	{ labels: { short: "", long: "", full_ascii: "" }, name: "toto", expected: "toto" },
	{ labels: { short: "to.", long: "toto toto", full_ascii: "" }, name: "toto", expected: "to." },
	{ labels: { short: "to.", long: "", full_ascii: "toto_toto" }, name: "toto", expected: "to." },
	{ labels: { short: "", long: "toto toto", full_ascii: "toto_toto" }, name: "toto", expected: "toto toto" },
	{ labels: { short: "to.", long: "toto toto", full_ascii: "" }, name: "toto", expected: "to." },
	{ labels: { short: "to.", long: "", full_ascii: "toto_toto" }, name: "toto", expected: "to." },
	{ labels: { short: "", long: "toto toto", full_ascii: "toto_toto" }, name: "toto", expected: "toto toto" },
	{ labels: { short: "to.", long: "toto toto", full_ascii: "toto_toto" }, name: "toto", expected: "to." },
])("getLabel($labels, $name) = $expected", ({ labels, name, expected }) => {
	expect(lib.getLabel(labels, name)).toBe(expected)
})

test.each([
	{ unit: { name: "meter", symbol: "m" }, expected: "m" },
	{ unit: { name: "meter", symbol: ["m", "me"] }, expected: "m" },
	{
		unit: [
			{ name: "meter", symbol: "m" },
			{ name: "centimeter", symbol: "cm" },
		],
		expected: "m",
	},
	{
		unit: [
			{ name: "meter", symbol: ["m", "me"] },
			{ name: "centimeter", symbol: "cm" },
		],
		expected: "m",
	},
	{
		unit: { name: "sigma", symbol: "<math><ms>&sigma;</ms></math>" },
		expected: "<math><ms>&sigma;</ms></math>",
	},
])("getUnit($unit) = $expected", ({ unit, expected }) => {
	expect(lib.getUnit(unit)).toBe(expected)
})

test.each([
	{ dbId: 1, expected: "tutu-titi__1" },
	{ dbId: 2, expected: "toto-titi__2" },
	{ dbId: 3, expected: "tata-titi__3" },
	{ dbId: 4, expected: "titi-titi__4" },
	{ dbId: 5, expected: "tete-titi__5" },
])("getUri of id number $dbId = $expected", ({ dbId, expected }) => {
	expect(
		lib.getUri(
			[
				{ id: 1, uri: "tutu-titi__1" },
				{ id: 2, uri: "toto-titi__2" },
				{ id: 3, uri: "tata-titi__3" },
				{ id: 4, uri: "titi-titi__4" },
				{ id: 5, uri: "tete-titi__5" },
			],
			dbId,
		),
	).toBe(expected)
})

test.each([
	{ obj: {}, expected: {} },
	{
		obj: {
			a: { label: "a", dataName: "a" },
			b: { label: "b", dataName: "b" },
		},
		expected: {
			a: { label: "a", dataName: "a" },
			b: { label: "b", dataName: "b" },
		},
	},
	{
		obj: {
			a: { label: "a a a", dataName: "a" },
			b: {
				label: "b_b_b",
				dataName: "b",
				columns: {
					c: { label: "c", dataName: "c" },
				},
			},
			d: { label: "d", dataName: "d" },
		},
		expected: {
			a: { label: "a a a", dataName: "a" },
			b: { label: "b_b_b", dataName: "b" },
			b_c: { label: "b_b_b.c", dataName: "c" },
			d: { label: "d", dataName: "d" },
		},
	},

	{
		obj: {
			a: { label: "a a a", dataName: "a" },
			b: {
				label: "b_b_b",
				dataName: "b",
				columns: {
					c: {
						label: "c",
						dataName: "c",
						columns: {
							d: { label: "d", dataName: "d" },
							e: { label: "e", dataName: "e" },
						},
					},
				},
			},
		},
		expected: {
			a: { label: "a a a", dataName: "a" },
			b: { label: "b_b_b", dataName: "b" },
			b_c: { label: "b_b_b.c", dataName: "c" },
			b_c_d: { label: "b_b_b.c.d", dataName: "d" },
			b_c_e: { label: "b_b_b.c.e", dataName: "e" },
		},
	},
])("flattenObject($obj) == $expected", ({ obj, expected }) => {
	expect(lib.flattenObject(obj, "")).toStrictEqual(expected)
})

test.each([
	{ prefix: "", label: "a", expected: "a" },
	{ prefix: "a", label: "a", expected: "a.a" },
	{ prefix: "b", label: "b", expected: "b.b" },
	{ prefix: "c", label: "c", expected: "c.c" },
	{ prefix: "a.b", label: "c", expected: "a.b.c" },
	{ prefix: "a.b.c", label: "d", expected: "a.b.c.d" },
])("labelWithPrefix($prefix, $label) = $expected", ({ prefix, label, expected }) => {
	expect(lib.labelWithPrefix(prefix, label)).toBe(expected)
})

test("filterExodict", () => {
	const rawExodict = {
		toto: {
			label: { short: "to.", long: "", full_ascii: "" },
			a: {
				label: { short: "", long: "a a a", full_ascii: "" },
				unit: { name: "meter", symbol: "m" },
			},
			b: {
				label: { short: "", long: "", full_ascii: "b_b_b" },
				c: {
					label: { short: "", long: "", full_ascii: "" },
					unit: [
						{ name: "meter", symbol: "m" },
						{ name: "centimeter", symbol: "cm" },
					],
				},
			},
			d: { label: { short: "d", long: "", full_ascii: "" } },
		},
		titi: {
			label: { short: "", long: "titi_titi", full_ascii: "" },
			e: { label: { short: "e", long: "", full_ascii: "" } },
		},
		tutu: { tyty: "tata" },
		f: { label: { short: "f", long: "", full_ascii: "" } },
	}
	expect(lib.initFilterExodict(rawExodict, ["toto", "a", "b", "c", "d", "titi", "e"])).toEqual({
		toto: {
			label: "to.",
			dataName: "toto",
			columns: {
				a: { label: "a a a", dataName: "a", unit: "m" },
				b: {
					label: "b_b_b",
					dataName: "b",
					columns: {
						c: { label: "c", dataName: "c", unit: "m" },
					},
				},
				d: { label: "d", dataName: "d" },
			},
		},
		titi: {
			label: "titi_titi",
			dataName: "titi",
			columns: {
				e: { label: "e", dataName: "e" },
			},
		},
	})
})

test("flattenExodict", () => {
	const filteredExodict = {
		toto: {
			label: "to.",
			dataName: "toto",
			columns: {
				a: { label: "a a a", dataName: "a" },
				b: {
					label: "b_b_b",
					dataName: "b",
					columns: {
						c: { label: "c", dataName: "c" },
					},
				},
				d: { label: "d", dataName: "d" },
			},
		},
		titi: {
			label: "titi_titi",
			dataName: "titi",
			columns: {
				e: { label: "e", dataName: "e" },
			},
		},
		tutu: { label: "tutu", dataName: "tutu" },
	}
	expect(lib.flattenExodict(filteredExodict)).toStrictEqual({
		toto: {
			label: "to.",
			dataName: "toto",
			columns: {
				a: { label: "a a a", dataName: "a" },
				b: { label: "b_b_b", dataName: "b" },
				b_c: { label: "b_b_b.c", dataName: "c" },
				d: { label: "d", dataName: "d" },
			},
		},
		titi: {
			label: "titi_titi",
			dataName: "titi",
			columns: {
				e: { label: "e", dataName: "e" },
			},
		},
		tutu: { label: "tutu", dataName: "tutu" },
	})
})
