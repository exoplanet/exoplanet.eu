/* global test, expect */

const lib = require("../../../../disk/static/js/utils_init_table_catalog")

test.each([
	{ array1: [], array2: [], expected: [] },
	{ array1: [1], array2: [2], expected: [1, 2] },
	{ array1: [1, 2, 3, 4], array2: [5, 6], expected: [1, 2, 3, 4, 5, 6] },
	{ array1: [1, 2], array2: [3, 4, 5, 6], expected: [1, 2, 3, 4, 5, 6] },
	{ array1: [], array2: [1, 2, 3], expected: [1, 2, 3] },
	{ array1: [1, 2, 3], array2: [], expected: [1, 2, 3] },
])("extendArrays($array1, $array2) = $expected", ({ array1, array2, expected }) => {
	expect(lib.extendArrays(array1, array2)).toStrictEqual(expected)
})

test.each([
	{
		data: { label: "to.", dataName: "toto", unit: "m" },
		expected: { title: "to.<br>(m)", data: "data.toto", defaultContent: '<span class="no-data">No data</span>' },
	},
	{
		data: { label: "toto toto", dataName: "toto" },
		expected: { title: "toto toto", data: "data.toto", defaultContent: '<span class="no-data">No data</span>' },
	},
	{
		data: { label: "to.", dataName: "toto", a: 1, b: 2, c: 3 },
		expected: { title: "to.", data: "data.toto", defaultContent: '<span class="no-data">No data</span>' },
	},
])("buildOneColumn($data, data) = $expected", ({ data, expected }) => {
	expect(lib.buildOneColumn([], data, "data")).toStrictEqual(expected)
})

test("buildColumns", () => {
	const filteredExodict = {
		toto: {
			label: "to.",
			dataName: "toto",
			columns: {
				a: { label: "a a a", dataName: "a", unit: "m" },
				b: {
					label: "b_b_b",
					dataName: "b",
					columns: {
						c: { label: "c", dataName: "c", unit: "m" },
					},
				},
				d: { label: "d", dataName: "d" },
			},
		},
		titi: {
			label: "titi_titi",
			dataName: "titi",
			columns: {
				e: { label: "e", dataName: "e" },
			},
		},
	}
	expect(lib.buildColumns([], filteredExodict, "data")).toStrictEqual([
		{ title: "to.", data: "data.toto", defaultContent: '<span class="no-data">No data</span>', nbCols: 3 },
		{ title: "a a a<br>(m)", data: "data.toto.a", defaultContent: '<span class="no-data">No data</span>' },
		{ title: "b_b_b", data: "data.toto.b", defaultContent: '<span class="no-data">No data</span>', nbCols: 1 },
		{ title: "c<br>(m)", data: "data.toto.b.c", defaultContent: '<span class="no-data">No data</span>' },
		{ title: "d", data: "data.toto.d", defaultContent: '<span class="no-data">No data</span>' },
		{ title: "titi_titi", data: "data.titi", defaultContent: '<span class="no-data">No data</span>', nbCols: 1 },
		{ title: "e", data: "data.titi.e", defaultContent: '<span class="no-data">No data</span>' },
	])
})

test.each([
	{
		columnsAndColSpan: [
			{ title: "a", data: "a" },
			{ title: "b", data: "b" },
			{ title: "c", data: "c" },
		],
		expected: [],
	},
	{
		columnsAndColSpan: [
			{ title: "a", data: "a", nbCols: 1 },
			{ title: "b", data: "b", nbCols: 1 },
			{ title: "c", data: "c", nbCols: 1 },
		],
		expected: [
			{ title: "a", data: "a", nbCols: 1 },
			{ title: "b", data: "b", nbCols: 1 },
			{ title: "c", data: "c", nbCols: 1 },
		],
	},
	{
		columnsAndColSpan: [
			{ title: "a", data: "a" },
			{ title: "b", data: "b", nbCols: 1 },
			{ title: "c", data: "c" },
		],
		expected: [{ title: "b", data: "b", nbCols: 1 }],
	},
])("getColSpan($columnsAndColSpan) = $expected", ({ columnsAndColSpan, expected }) => {
	expect(lib.getColSpan(columnsAndColSpan)).toStrictEqual(expected)
})

test.each([
	{
		columnsAndColSpan: [
			{ title: "a", data: "a" },
			{ title: "b", data: "b" },
			{ title: "c", data: "c" },
		],
		expected: [
			{ title: "a", data: "a" },
			{ title: "b", data: "b" },
			{ title: "c", data: "c" },
		],
	},
	{
		columnsAndColSpan: [
			{ title: "a", data: "a", nbCols: 1 },
			{ title: "b", data: "b", nbCols: 1 },
			{ title: "c", data: "c", nbCols: 1 },
		],
		expected: [],
	},
	{
		columnsAndColSpan: [
			{ title: "a", data: "a" },
			{ title: "b", data: "b", nbCols: 1 },
			{ title: "c", data: "c" },
		],
		expected: [
			{ title: "a", data: "a" },
			{ title: "c", data: "c" },
		],
	},
])("getColumns($columnsAndColSpan) = $expected", ({ columnsAndColSpan, expected }) => {
	expect(lib.getColumns(columnsAndColSpan)).toStrictEqual(expected)
})

test("sortColumnsAndColSpan", () => {
	const colAndColSpan = [
		{ title: "a", data: "a", nbCols: 3 },
		{ title: "a.a", data: "a.a" },
		{ title: "a.b", data: "a.b" },
		{ title: "a.c", data: "a.c" },
		{ title: "b", data: "b", nbCols: 1 },
		{ title: "b.a", data: "b.a" },
		{ title: "c", data: "c", nbCols: 2 },
		{ title: "c.a", data: "c.a" },
		{ title: "c.b", data: "c.b" },
	]
	expect(lib.sortColumnsAndColSpan(colAndColSpan, ["c", "a", "b"])).toStrictEqual([
		{ title: "c", data: "c", nbCols: 2 },
		{ title: "c.a", data: "c.a" },
		{ title: "c.b", data: "c.b" },
		{ title: "a", data: "a", nbCols: 3 },
		{ title: "a.a", data: "a.a" },
		{ title: "a.b", data: "a.b" },
		{ title: "a.c", data: "a.c" },
		{ title: "b", data: "b", nbCols: 1 },
		{ title: "b.a", data: "b.a" },
	])
})
