/* global test, expect */

const lib = require("../../../../disk/static/js/utils_init_table_catalog")

test.each([
	{ colNumber: 1, expected: '<tr><th class="final_headers"></th></tr>' },
	{ colNumber: 2, expected: '<tr><th class="final_headers"></th><th class="final_headers"></th></tr>' },
	{
		colNumber: 3,
		expected: '<tr><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th></tr>',
	},
	{
		colNumber: 5,
		expected:
			'<tr><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th></tr>',
	},
])("domHtmlTable($colNumber) = $colNumber th", ({ colNumber, expected }) => {
	expect(lib.domHtmlTableColumns(colNumber)).toBe(expected)
})

test("domHtmlTableColSpan", () => {
	const colSpan = [
		{ title: "a", data: "a", nbCols: 1 },
		{ title: "b", data: "b", nbCols: 5 },
		{ title: "c", data: "c", nbCols: 2 },
		{ title: "d", data: "d", nbCols: 8 },
		{ title: "e", data: "e", nbCols: 7 },
	]
	expect(lib.domHtmlTableColSpan(colSpan)).toStrictEqual(
		"<tr><th colspan=1>a</th><th colspan=5>b</th><th colspan=2>c</th><th colspan=8>d</th><th colspan=7>e</th></tr>",
	)
})

test("domHtmlTable", () => {
	const colSpan = [
		{ title: "a", data: "a", nbCols: 1 },
		{ title: "b", data: "b", nbCols: 5 },
		{ title: "c", data: "c", nbCols: 2 },
		{ title: "d", data: "d", nbCols: 8 },
		{ title: "e", data: "e", nbCols: 7 },
	]
	const columnsNumber = 23
	expect(lib.domHtmlTable(colSpan, columnsNumber)).toStrictEqual(
		'<table id="disk-catalog-table"><thead><tr><th colspan=1>a</th><th colspan=5>b</th><th colspan=2>c</th><th colspan=8>d</th><th colspan=7>e</th></tr><tr><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th><th class="final_headers"></th></tr></thead></table>',
	)
})
