"""
This is the version of the exoplanet.eu site.

It use the semantic version system 2.0.0 https://semver.org/
"""

# Standard imports
from pathlib import Path

# External imports
import toml

_pyproject_path = Path(__file__).parent / "pyproject.toml"

__version__ = toml.load(_pyproject_path)["tool"]["poetry"]["version"]
