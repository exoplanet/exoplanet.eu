"""
exoplanet.amd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Django 💩 imports
from django.urls import path, re_path

# Local imports
from . import views

# BUG a slash in the url ?! WTF
# BUG possible code injection here ?! WTF
# almost same regex is used in catalog_legacy.uls for the planets
AMD_STAR_NAME_URL_REGEX = r"^amd/(?P<star_slug>[0-9a-z-+_.()':/`\[\]*]+)/$"

urlpatterns = [
    path(
        route="list/",
        view=views.AmdList.as_view(),
        name="amd_list",
    ),
    re_path(
        route=AMD_STAR_NAME_URL_REGEX,
        view=views.AmdView.as_view(),
        name="amd_detail",
    ),
]
