"""
System stability calculations using AMD method.
"""

# Standard imports
from dataclasses import dataclass, field
from typing import Any, NamedTuple, TypedDict

# Django 💩 imports
from django.conf import settings
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy

# External imports
import requests
from requests.exceptions import Timeout

# First party imports
from catalog_legacy.views import get_complex_attr
from common.settings import ConfigurationChoice
from common.views import NewDesignBaseTemplateView
from core.models import (
    PlanetDBView,
    PlanetStatus,
    Star,
    WebStatus,
    id_from_slug,
    obj_slugify,
)

# Local imports
from .star_list import AMD_STAR_LIST

# TODO add the possibility to overload the url via a VENV
# It use to have 2 other possible values :
# "http://127.0.0.1:5000/api/amd" for local testing if you have the amd server running
# locally
# "http://exoplanets.esep.pro/esep_amd/api/amd" as an alternative url
AMD_API_URL = "http://vmesep.obspm.fr/esep_amd/api/amd"

# The AMD API may be really slow to answer so we need a very long timeout
AMD_API_TIMEOUT = 2.0  # in seconds
AMD_API_TIMEOUT_MSG = "AMD server unreachable at the moment. Please try again later."


class _AmdParam(NamedTuple):
    name: str
    amd_name: str
    name_error: str | None = None


# TODO in Python 3.11+ use typing.NotRequired instead of inheritance
class _AmdErrDict(TypedDict, total=False):
    error_min: str
    error_max: str


# TODO in Python 3.11+ use typing.NotRequired instead of inheritance
class _AmdValErrDict(_AmdErrDict):
    value: str


class _AmdContextDict(TypedDict):
    view: "AmdView"
    data: dict[str, _AmdValErrDict]
    script: str
    div: str


_STAR_AMD_PARAMS = [
    _AmdParam(
        name="name",
        amd_name="name",
    ),
    _AmdParam(
        name="mass",
        amd_name="mass",
        name_error="mass_error",
    ),
]


_PLANET_PARAMS_AMD = (
    _AmdParam(
        name="name",
        amd_name="name",
    ),
    _AmdParam(
        name="period",
        amd_name="period",
        name_error="period_error",
    ),
    _AmdParam(
        name="mass_mjup",
        amd_name="mass",
        name_error="mass_error_mjup",
    ),
    _AmdParam(
        name="eccentricity",
        amd_name="eccentricity",
        name_error="eccentricity_error",
    ),
)


# pylint: disable=invalid-name
def _AmdValErrDict_from_db(
    stellar_obj: Star | PlanetDBView,
    name: str,
    name_error: str | None,
) -> _AmdValErrDict:
    value = get_complex_attr(stellar_obj, name)

    if value is not None and name_error:
        error = get_complex_attr(stellar_obj, name_error)
    else:
        error = None

    match error:
        case [err_min, err_max]:
            return _AmdValErrDict(
                value=str(value),
                error_min=str(err_min),
                error_max=str(err_max),
            )
        case [err]:
            return _AmdValErrDict(
                value=str(value),
                error_min=str(err),
                error_max=str(err),
            )
        case None | []:
            return _AmdValErrDict(value=str(value))
        case _:
            raise ValueError(
                f"Misformated values and error for AMD parameter {name} "
                f"of {stellar_obj}.\n"
                f"Impossible combination of value and error "
                f"{value =} {error =}"
            )


# pylint: disable=invalid-name
def _AmdValErrDict_from_post_request(
    full_amd_param: str,
    request: HttpRequest,
) -> _AmdValErrDict:
    value = request.POST.get(f"{full_amd_param}_value", default=None)
    raw_error_min = request.POST.get(f"{full_amd_param}_error_min", default=None)
    raw_error_max = request.POST.get(f"{full_amd_param}_error_max", default=None)

    match (raw_error_min, raw_error_max):
        case (None, None):
            return _AmdValErrDict(
                value=str(value),
            )
        case (error_min, None):
            return _AmdValErrDict(
                value=str(value),
                error_min=str(error_min),
                error_max=str(error_min),
            )
        case (None, error_max):
            return _AmdValErrDict(
                value=str(value),
                error_min=str(error_max),
                error_max=str(error_max),
            )
        case (error_min, error_max):
            return _AmdValErrDict(
                value=str(value),
                error_min=str(error_min),
                error_max=str(error_max),
            )
        case _:
            raise ValueError(
                f"Misformated values and error for AMD parameter {full_amd_param}.\n"
                f"Impossible combination of value and error "
                f"{value =} {raw_error_min =} {raw_error_max =}"
            )


def _get_amd_data_from_db(star: Star) -> dict[str, _AmdValErrDict]:
    amd_data = {}

    # First data about the star
    for name, amd_name, name_error in _STAR_AMD_PARAMS:
        param_html = f"star_{amd_name}"

        amd_data[param_html] = _AmdValErrDict_from_db(star, name, name_error)

    # Then data about the planets
    # Star has an automatic attribute `planets` because of the foreign key field.
    # django model (Django == 💩)
    active_confirmed_planets = star.planets.filter(  # type: ignore[attr-defined]
        status=WebStatus.ACTIVE,
        planet_status=PlanetStatus.CONFIRMED,
    ).order_by("axis")

    for i, planet in enumerate(active_confirmed_planets):
        # pylint: disable-next=no-member
        planet_v = PlanetDBView.objects.get(id=planet.id)

        # The name prefixed with double _ is used as a label and the one with simple _
        # here to generate a hidden field in order to pass the value as post data... and
        # the double _ HAS TO be inserted as the first parameter since it has to appear
        # before on the html page that rely on keys order...
        # Yes, this is insane!
        amd_data[f"planet{i}__name"] = _AmdValErrDict(
            value=str(get_complex_attr(planet_v, "name"))
        )

        for name, amd_name, name_error in _PLANET_PARAMS_AMD:
            param_html = f"planet{i}_{amd_name}"

            amd_data[param_html] = _AmdValErrDict_from_db(planet_v, name, name_error)

    return amd_data


def _get_amd_data_from_post_data(
    star: Star,
    request: HttpRequest,
) -> dict[str, _AmdValErrDict]:
    amd_data = {}

    # First data about the star
    for name, _amd_name, _name_error in _STAR_AMD_PARAMS:
        param_html = f"star_{name}"

        amd_data[param_html] = _AmdValErrDict_from_post_request(param_html, request)

    # Then data about the planets

    # Since the POST data are stupidly arranged we need to get a list of the planet
    # contained in the post data... from the datatbase
    # Star has an automatic attribute `planets` because of the foreign key field.
    # django model (Django == 💩)
    active_confirmed_planets = star.planets.filter(  # type: ignore[attr-defined]
        status=WebStatus.ACTIVE,
        planet_status=PlanetStatus.CONFIRMED,
    ).order_by("axis")

    for i, _planet in enumerate(active_confirmed_planets):
        # The name prefixed with double _ is used as a label and the one with simple _
        # here to generate a hidden field in order to pass the value as post data... and
        # the double _ HAS TO be inserted as the first parameter since it has to appear
        # before on the html page that rely on keys order...
        # Yes, this is insane!
        amd_data[f"planet{i}__name"] = _AmdValErrDict(
            value=str(request.POST.get(f"planet{i}_name_value", default=None))
        )
        for name, amd_name, _name_error in _PLANET_PARAMS_AMD:
            param_html = f"planet{i}_{amd_name}"

            amd_data[param_html] = _AmdValErrDict_from_post_request(param_html, request)

    return amd_data


SHOULD_BE_SET_IN_GET_OR_POST = "Should be set in get or post"


class AmdView(NewDesignBaseTemplateView):
    """
    Display a page with links planets handled by the AMD stability predictor.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "amdf.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the tamplate through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = SHOULD_BE_SET_IN_GET_OR_POST
    description = "A tool for computation of long-term stability of planetary systems."

    def _get_or_post(
        self,
        request: HttpRequest,
        star_name: str,
        amd_data: dict[str, _AmdValErrDict],
    ) -> HttpResponse:
        # set the page title
        self.title = f"AMDf-stability <br> of the system {star_name}"

        # HACK: this is the only way to have fast tests at the moment
        # TODO: Find a better solution
        if not settings.IN_WHICH_CONFIG == ConfigurationChoice.TESTS:
            try:
                answer = requests.post(
                    AMD_API_URL,
                    json=amd_data,
                    headers={"content-type": "application/json"},
                    timeout=AMD_API_TIMEOUT,
                ).json()
            except Timeout:
                answer = dict(
                    script="",
                    div=AMD_API_TIMEOUT_MSG,
                )

            context = _AmdContextDict(
                view=self,
                data=amd_data,
                script=answer["script"],
                div=answer["div"],
            )
        else:
            context = _AmdContextDict(
                view=self,
                data={"star_name": {"value": "CoRoT-7"}},
                script="",
                div=AMD_API_TIMEOUT_MSG,
            )

        return render(
            request=request,
            template_name=self.template_name,
            context=context,
        )

    # pylint: disable=arguments-differ
    def get(
        self,
        request: HttpRequest,
        star_slug: str,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Display a page with links planets handled by the AMD stability predictor
        requested via get (no system parameter modified in the form is taken in account
        here since it is a get request).

        Args:
            request: the GET http request
            star_slug: the full slug of the main star of the system as passed in the
                last part of the url

        Returns:
            page with a form with all the params of the systems and some graphs
            representing the stability of the system.

        Raises:
            Http404: if slug do not correspond to any star
        """
        # Get the star from the database
        star = get_object_or_404(Star, id=id_from_slug(star_slug))

        # retreive the AMD specific data
        amd_data = _get_amd_data_from_db(star)

        return self._get_or_post(
            request=request,
            star_name=star.name,
            amd_data=amd_data,
        )

    # pylint: disable=arguments-differ
    def post(
        self,
        request: HttpRequest,
        star_slug: str,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Display a page with links planets handled by the AMD stability predictor
        requested via post (with potentially some system parameters modified by the
        user).

        Args:
            request: the POST http request
            star_slug: the full slug of the main star of the system as passed in the
                last part of the url

        Returns:
            page with a form with all the params of the systems and some graphs
            representing the stability of the system.

        Raises:
            Http404: if slug do not correspond to any star
        """
        # Get the star from the database
        star = get_object_or_404(Star, id=id_from_slug(star_slug))

        # retreive the AMD specific data, using the post data
        amd_data = _get_amd_data_from_post_data(star, request)

        return self._get_or_post(
            request=request,
            star_name=star.name,
            amd_data=amd_data,
        )


@dataclass
class _AmdSystemDesc:
    """Describe a valid exoplanetary system for the AMD simulation"""

    star_name: str
    amd_url: str = field(init=False)

    def __post_init__(self) -> None:
        star = get_object_or_404(Star, name=self.star_name)
        # Star has an attribute `id` automaticly set by django model.
        # django model (Django == 💩)
        star_slug = obj_slugify(self.star_name, star.id)  # type: ignore[attr-defined]

        self.amd_url = reverse_lazy(
            viewname="amd_detail", kwargs=dict(star_slug=star_slug)
        )


class AmdList(NewDesignBaseTemplateView):
    """
    Display a page with links planets handled by the AMD stability predictor.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "amd_list.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the tamplate through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "AMDf-stability Systems"
    description = "A tool for computation of long-term stability of planetary systems."

    # List of AMD compatible planetary systems descriptions
    _memoized_amd_list: list[_AmdSystemDesc] = []

    @property
    def all_amd(self) -> list[_AmdSystemDesc]:
        # Memoization of the computation to prevent heavy db access every time we access
        # the property
        if not self._memoized_amd_list:
            self._memoized_amd_list = [
                _AmdSystemDesc(star_name=name) for name in AMD_STAR_LIST
            ]

        return self._memoized_amd_list
