"""
All stars of the catalog for whiche the AMD/AMDf stability algorithm is valid.

Each name must be a perfect match to the main star name of the système as registered in
the exoplanet.eu database (not the alternate name).
"""

AMD_STAR_LIST = [
    "24 Sex",
    "47 Uma",
    "55 Cnc",
    "61 Vir",
    "BD-06 1339",
    "BD-08 2823",
    "CoRoT-7",
    "eta Cet",
    "GJ 163",
    "GJ 180",
    "GJ 581",
    "GJ 649",
    "GJ 667 C",
    "GJ 676 A",
    "GJ 682",
    "GJ 785",
    "GJ 832",
    "GJ 3293",
    "GJ 876",
    "HAT-P-13",
    "HD 10180",
    "HD 102272",
    "HD 108874",
    "HD 109271",
    "HD 110014",
    "HD 113538",
    "HD 11506",
    "HD 117618",
    "HD 11964",
    "HD 125612",
    "HD 12661",
    "HD 128311",
    "HD 133131 A",
    "HD 134060",
    "HD 134606",
    "HD 134987",
    "HD 136352",
    "HD 13808",
    "HD 13908",
    "HD 141399",
    "HD 142 A",
    "HD 143761",
    "HD 1461",
    "HD 147018",
    "HD 147873",
    "HD 154857",
    "HD 155358",
    "HD 159243",
    "HD 159868",
    "HD 1605",
    "HD 163607",
    "HD 164922",
    "HD 168443",
    "HD 169830",
    "HD 177830 A",
    "HD 181433",
    "HD 183263",
    "HD 187123",
    "HD 190360",
    "HD 20003",
    "HD 200964",
    "HD 202206 A",
    "HD 204313",
    "HD 20781",
    "HD 207832 A",
    "HD 20794",
    "HD 215152",
    "HD 215497",
    "HD 21693",
    "HD 217107",
    "HD 31527",
    "HD 33844",
    "HD 3651",
    "HD 37124",
    "HD 37605",
    "HD 38529",
    "HD 39194",
    "HD 40307",
    "HD 45364",
    "HD 47186",
    "HD 4732 A",
    "HD 47366",
    "HD 51608",
    "HD 52265",
    "HD 5319",
    "HD 60532",
    "HD 65216 A",
    "HD 67087",
    "HD 69830",
    "HD 73526",
    "HD 74156",
    "HD 7449 A",
    "HD 75784",
    "HD 7924",
    "HD 85390",
    "HD 87646 A",
    "HD 89744",
    "HD 92788",
    "HD 93385 A",
    "HD 9446",
    "HD 96700",
    "HIP 57274",
    "HIP 14810",
    "HIP 5158",
    "HIP 65407",
    "HIP 67851",
    "K2-3",
    "KELT-6",
    "Kapteyn's",
    "Kepler-10",
    "Kepler-11",
    "Kepler-23",
    "Kepler-289",
    "Kepler-419",
    "Kepler-454",
    "Kepler-46",
    "Kepler-51",
    "Kepler-56",
    "Kepler-68",
    "Kepler-79",
    "Kepler-87",
    "NN Ser (AB)",
    "Pr 0211",
    "TYC+1422-614-1",
    "WASP-41",
    "WASP-47",
    "Wolf 1061",
    "XO-2S",
    "mu Ara",
    "nu Oph",
    "ups And",
    "TRAPPIST-1",
]
