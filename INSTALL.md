
# Installation

## Prerequisites

The project is developped to run in an environment with the following prerequisites:

- GNU/linux based
- `amd64` architecture (we never tested it on 32bit systems or on any ARM systems)
- `systemd` available (to manage the running servers.... you can do it another way but
  we won't provide any help for that, sorry)

The main reason for those limitation is: we do not have enough time or ressources to
expand the scope of the project.

## (optional but useful) Easy ssh connection

Access to `exoplanet` and `exoplanet-dev` are not for everyone due to security concerns.
But if you **need** to access those servers ask your project tech lead to have such
access. If you dont need it (most dev don't need such access) you can skip this part.

It can be a good idea to configure ssh on your machine in order to easily connect to
remote servers from obspm and LESIA.

First generate a ssh key according to [ANSSI recommandation](https://cyber.gouv.fr/en/publications/openssh-secure-use-recommendations)
if you don't already have one:

```shell
ssh-keygen -t ecdsa -b 256
```

Then add this to `~/.ssh/config` to benefit from easy connexion to servers:

```ssh-config
# First Observatoire de Paris ssh proxy server
Host styx
    Hostname styx.obspm.fr
    User pmartin
    ForwardAgent yes

# Second Observatoire de Paris ssh proxy server
Host rubicon
    Hostname rubicon.obspm.fr
    User pmartin
    ForwardAgent yes

# Exoplanet.eu prod DB and WEB server
Host exoplanet
    Hostname voparis-exoplanet-new.obspm.fr
    User pmartin
    ForwardAgent yes

# Exoplanet.eu dev DB and WEB server
Host exoplanet-dev
    Hostname voparis-exoplanet-new-dev.obspm.fr
    User pmartin
    ForwardAgent yes

# Exoplanet.eu prod DB and WEB server (but from home)
Host exoplanet-from-home
    Hostname voparis-exoplanet-new.obspm.fr
    User pmartin
    ForwardAgent yes
    ProxyCommand ssh styx -W %h:22
    # ProxyCommand ssh rubicon -W %h:22

# Exoplanet.eu dev DB and WEB server (but from home)
Host exoplanet-dev-from-home
    Hostname voparis-exoplanet-new-dev.obspm.fr
    User pmartin
    ForwardAgent yes
    ProxyCommand ssh styx -W %h:22
    # ProxyCommand ssh rubicon -W %h:22
```

You can now grant yourself passwordless access to proxy servers (**rubycon** and
**styx**) used for external access:

```shell
ssh-copy-id -i ~/.ssh/id_ecdsa styx
ssh-copy-id -i ~/.ssh/id_ecdsa rubicon
```

## Install the project

### Ubuntu

If you on ubuntu simply run the dedicated script (on any other platform you will have to
adapt it a bit):

```shell
bash install/install_from_scratch_ubuntu.sh
```

### Other OS

We don't have yet a script for other OS or linux flavor but here is a list of the
dependancies to install (by whatever means necessary) :

- [git](https://git-scm.com)
- [git-lfs](https://git-lfs.com)
- [curl](https://curl.se)
- [python3.10](https://www.python.org/downloads/release/python-3100/)
- [pip](https://pip.pypa.io/en/stable/) for python3
- [python3-venv](https://docs.python.org/3.10/library/venv.html) that is not always
  packaged with python (on ubuntu it is not)
- [apache2](https://httpd.apache.org) and its developpemnt headers
  [apache2-dev](https://packages.debian.org/fr/sid/apache2-dev)
- [postgresql](https://www.postgresql.org) client, server and developpement header `libpq-dev`
- [postgREST](https://postgrest.org/en/stable/) api server
- [node.js](https://nodejs.org/en) version 18 and corresponding `npm` tool
- [pyenv](https://github.com/pyenv/pyenv)
- `python3.10` via pyenv if needed
- [pipx](https://pipx.pypa.io/stable/) via pip
- [rust](https://www.rust-lang.org), `cargo` and the whole rust toolchain
- [just](https://just.systems) via `cargo`

Then you can clone the project and install it's local dependancies :

```shell
git clone git@gitlab.obspm.fr:exoplanet/exoplanet.eu.git
cd exoplanet.eu
poetry shell
just install
```

## Install the local database

To create database and initialise it (users, schema, etc) you need to run:

```shell
install/init_db.sh
```

To fill the db with data you then need to obtain an export of the db as csv format (ask
ulysse or pierre-yves since they have the needed rights on the production servers).

Then you need to run:

```shell
install/import_data.sh DIRECTORY_WHERE_YOU_PUT_THE_CSV_FILES
```
