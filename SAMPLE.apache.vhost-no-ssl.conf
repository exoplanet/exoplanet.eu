# Author: Pierre-Yves Martin and Ulysse Chosson for exoplanet.eu

# Should be installed  in /etc/apache2/sites-available like this:
#
# sudo cp SAMPLE.apache.vhost-no-ssl.conf /etc/apache2/sites-available/exoplanet-no-ssl.conf
# sudo a2ensite exoplanet-no-ssl

<VirtualHost *:80>
	## Server name and possible aliases

	# ServerName voparis-exoplanet-new.obspm.fr
	ServerName exoplanet-devlesia.obspm.fr
	# ServerAlias new.exoplanet.eu

	## Vhost docroot

	DocumentRoot /var/www/html/scholar/

	## Logging

	ErrorLog ${APACHE_LOG_DIR}/scholar-error.log
	LogFormat "%v %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" vhostcombined
	CustomLog ${APACHE_LOG_DIR}/scholar-access.log vhostcombined

	# Security it prevents the server from being used as a proxy by other people.
	# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_proxy.html#proxyrequests
	ProxyRequests Off

	# Proxy for posgREST, redirect request on /postgrest to http postgREST server
	<IfModule mod_rewrite.c>
		# Enable Rewrite
		RewriteEngine on

		# Set a condition for the rewrite rule.
		# `%{REQUEST_METHOD} ^{TRACE|TRACK}` means: if the request method is of the type TRACE or TRACK
		#
		# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_rewrite.html#rewritecond
		# DOC: https://httpd.apache.org/docs/2.4/en/expr.html#vars
		RewriteCond %{REQUEST_METHOD} ^{TRACE|TRACK}

		# Set the rule if condition is validated
		# `.* - [F]` means: if somethings validate the regex .* (all validate this regex)
		# and no substitution should be performed returns a 403 FORBIDDEN response
		# to the client browser (F flag).
		#
		# From apache2 documentation: "A dash indicates that no substitution should be performed
		# (the existing path is passed through untouched). This is used when a flag
		# (see below) needs to be applied without changing path."
		#
		# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_rewrite.html#rewriterule
		# DOC: https://httpd.apache.org/docs/2.4/en/rewrite/flags.html
		RewriteRule .* - [F]

		<IfModule mod_proxy.c>
			# Toggle the usage of the SSL/TLS Protocol Engine for proxy.
			#
			# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_ssl.html#sslproxyengine
			SSLProxyEngine On

			# Setup the proxy routes
			#
			# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_proxy.html#proxypass
			# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_proxy.html#proxypassreverse
			ProxyPass /postgrest http://exoplanet-devlesia.obspm.fr:3000
			ProxyPassReverse /postgrest http://exoplanet-devlesia.obspm.fr:3000

			# Rewrite rule to get https reponse of a http server.
			#
			# DOC: https://httpd.apache.org/docs/2.4/en/mod/mod_rewrite.html#rewriterule
			RewriteRule ^/(.*) http://exoplanet.eu-devlesia.obspm.fr:3000/$1 [P,L]
		</IfModule>
	</IfModule>

	## We just redirect everything from HTTP yo HTTPS

	Redirect permanent / https://exoplanet-devlesia.obspm.fr/

</VirtualHost>
