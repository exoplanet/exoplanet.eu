# Deployment

## Install

See [INSTALL.md](INSTALL.md) for detailed instructions.

## Export generated images

You will need execute the following script:

```shell
$ ressources/gen_all_favicons.py
Exoplanet.eu - Favicon generator
Input SVG file   : /home/pmartin/Projets/exoplanet.eu/git-exoplanet.eu/ressources/exoplanet_eu_favicon.svg ✓
Output Directory : /home/pmartin/Projets/exoplanet.eu/git-exoplanet.eu/common/static/generated_img ✓
Inkscape         : ✓

=== Browser favicons ===
➡ Generating favicon-16x16.png ✓
➡ Generating favicon-32x32.png ✓
➡ Generating favicon-48x48.png ✓

=== Apple Touch icons ===
➡ Generating apple-touch-icon-60x60.png ✓
➡ Generating apple-touch-icon-76x76.png ✓
➡ Generating apple-touch-icon-120x120.png ✓
➡ Generating apple-touch-icon-152x152.png ✓
➡ Generating apple-touch-icon-180x180.png ✓

=== Windows 8 and 8.1 tiles ===
⚠ These files are purposely generated with fake size in their names ⚠
➡ Generating mstile-70x70.png ✓
➡ Generating mstile-150x150.png ✓
➡ Generating mstile-310x310.png ✓

=== Android, Chrome and Opera ===
➡ Generating android-chrome-192x192.png ✓
➡ Generating android-chrome-512x512.png ✓

Export successful 🍰🍰🍰
```

## Local deployment

If you need to test locally the site you can deploy it locally using Django's admin tool
`runserver` but you often need to first create a super user (WARNING you must have
activated the virtual environment of the project):

```shell
$ python manage.py createsuperuser --configuration=Local
Username (leave blank to use 'yourusername'): yoursuperusername
Email address:
Password:
Password (again):
Superuser created successfully.
$ python manage.py runserver --configuration=Local
django-configurations version 2.4, using configuration Local
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
December 01, 2022 - 13:20:44
Django version 4.1.3, using settings 'common.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

TODO add a command to clean the local db from everything (user, planets...)
