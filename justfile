set dotenv-load := false

# List all just commands
list:
    just --list --unsorted

# Remove all python artefacts files and folders
@clean:
    printf '%-40s' 'Cleaning python artefact: '
    find . \( -name __pycache__ -o -name "*.pyc" \) -delete
    echo '✅'
    printf '%-40s' 'Cleaning pytest artefact: '
    rm -rf .pytest_cache
    echo '✅'

# Install python dependencies for development
@_install-python-dev:
    printf '%-40s' 'Install python dependancy dev: '
    # TODO: remove the --no-root option as soon as there is a pyproject.toml option for that
    poetry install --no-interaction --quiet --sync  --no-root
    echo '✅'

# Install web dependencies for each django app
@_install-web:
    printf '%-40s' 'Install web dependencies (npm): '
    npm ci --production --no-progress --silent --loglevel error > /dev/null
    cd common/static; npm ci --production --no-progress --silent --loglevel error > /dev/null; cd - > /dev/null
    cd catalog_legacy/static; npm ci --production --no-progress --silent --loglevel error > /dev/null; cd - > /dev/null
    cd ephemeris/static; npm ci --production --no-progress --silent --loglevel error > /dev/null; cd - > /dev/null
    echo '✅'

# Install web dependencies for each django app
@_install-web-dev:
    printf '%-40s' 'Install web dependencies (npm): '
    npm ci --no-progress --silent --loglevel error > /dev/null
    cd common/static; npm ci --no-progress --silent --loglevel error > /dev/null; cd - > /dev/null
    cd catalog_legacy/static; npm ci --no-progress --silent --loglevel error > /dev/null; cd - > /dev/null
    cd ephemeris/static; npm ci --no-progress --silent --loglevel error > /dev/null; cd - > /dev/null
    echo '✅'

# Install pre-commit hooks
@_install-pre-commit:
    printf '%-40s' 'Install pre-commit hooks: '
    pre-commit gc > /dev/null
    pre-commit install > /dev/null
    pre-commit install-hooks > /dev/null
    echo '✅'

# Install minimal dependencies (for deployment or tests)
install-server: clean _install-python-dev _install-web

# Install all dependencies (for development)
install: clean _install-python-dev _install-web-dev _install-pre-commit

# Generate all the icons and favicons for different platforms
gen_icons:
    printf '%-40s' 'Generate icons: '
    tools/exogen.py generate ressources/conf.yaml > /dev/null
    echo '✅'

# Remove generated icons and favicons (see gen_icons command)
clean_icons:
    printf '%-40s' 'Clean generated icons: '
    tools/exogen.py clean ressources/conf.yaml > /dev/null
    echo '✅'

# Uninstall pre-commit hooks
@_uninstall-pre-commit:
    printf '%-40s' 'Uninstall pre-commit hooks: '
    pre-commit uninstall > /dev/null
    pre-commit clean > /dev/null
    pre-commit gc > /dev/null
    echo '✅'

# Unistall web dependencies
@_uninstall-web:
    printf '%-40s' 'Uninstall web dependencies: '
    rm -rf catalog_legacy/static/node_modules
    rm -rf common/static/node_modules
    rm -rf ephemeris/static/node_modules
    rm -rf node_modules
    # We DO NOT uninstall git submodules
    echo '✅'

# # Uninstall python dependencies for development
# @_uninstall-python-dev:
#     printf '%-40s' 'Uninstall python dependancy dev: '
#     pip uninstall -y -q -r requirements_dev.txt
#     echo '✅'
# # Uninstall python dependencies
# @_uninstall-python:
#     printf '%-40s' 'Uninstall python dependancy: '
#     pip uninstall -y -q -r requirements.txt
#     # We DO NOT uninstall pip
#     echo '✅'

uninstall: clean _uninstall-pre-commit _uninstall-web

## LINTER ALIAS

# Launch mypy on amd
@_mypy-amd:
    mypy --pretty -p amd --config-file pyproject.toml
    echo 'amd: ✅'

# Launch mypy on canary
@_mypy-canary:
    mypy --pretty -p canary --config-file pyproject.toml
    echo 'canary: ✅'

# Launch mypy on catalog
@_mypy-catalog:
    mypy --pretty -p catalog --config-file pyproject.toml
    echo 'catalog: ✅'

# Launch mypy on catalog_legacy
@_mypy-catalog_legacy:
    mypy --pretty -p catalog_legacy --config-file pyproject.toml
    echo 'catalog_legacy: ✅'

# Launch mypy on common
@_mypy-common:
    mypy --pretty -p common --config-file pyproject.toml
    echo 'common: ✅'

# Launch mypy on core
@_mypy-core:
    mypy --pretty -p core --config-file pyproject.toml
    echo 'core: ✅'

# Launch mypy on editorial
@_mypy-editorial:
    mypy --pretty -p editorial --config-file pyproject.toml
    echo 'editorial: ✅'

# Launch mypy on ephemeris
@_mypy-ephemeris:
    mypy --pretty -p ephemeris --config-file pyproject.toml
    echo 'ephemeris: ✅'

# Launch mypy on tools
@_mypy-tools:
    mypy --pretty -p tools --config-file pyproject.toml
    echo 'tools: ✅'

# Launch mypy on tools
@_mypy-disk:
    mypy --pretty -p disk --config-file pyproject.toml
    echo 'disk: ✅'

# Launch the mypy type linter on all module
mypy-all: _mypy-amd _mypy-canary _mypy-catalog _mypy-catalog_legacy _mypy-common _mypy-core _mypy-disk _mypy-editorial _mypy-ephemeris _mypy-tools

# Run markdownlint
lintmd path='"**/*.md" "#**/node_modules/**"': clean
    markdownlint-cli2-config ".markdownlint.yaml" {{ path }}

# Run black and isort
onmy31 path="amd canary catalog catalog_legacy common core disk editorial ephemeris tools test": clean
    black {{ path }}
    echo 'black: ✅'
    isort {{ path }}
    echo 'isort: ✅'

printlinter path=".":
    printlinter lint
    echo 'printlinter ✅'

# biome check (apply format and lint)
biome-check path=".":
    npx --no-install biome check {{ path }}
    echo 'biome check ✅'

# biome check fix (apply format and lint)
biome-check-fix path=".":
    npx --no-install biome check --apply {{ path }}
    echo 'biome check and fix ✅'

# biome check fix unsafe (apply format and lint)
biome-check-fix-unsafe path=".":
    npx --no-install biome check --apply-unsafe {{ path }}
    echo 'biome check and fix unsafe ✅'

# Launch sql on `path`
sqlfluff path=".":
    sqlfluff lint --dialect postgres {{ path }}
    echo 'sqlfluff ✅'

# Run all linter
lint: clean onmy31 mypy-all printlinter biome-check sqlfluff lintmd

# Show all urls of the website
show-urls-local:
    python manage.py show_urls --configuration=Local

# Run local server
run-server-local:
    python manage.py runserver --configuration=Local

# Debug local server with pudb
debug-server-local:
    pudb --continue manage.py runserver --configuration=Local

# Open a local PostgreSQL shell on the exoplanet.eu database
psql-shell user="${USER}":
    psql --host=localhost --dbname=exoplanet --user="{{ user }}"
