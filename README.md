---
author: Pierre-Yves Martin (LESIA)
copyright: Copyright (C) 2016-2017 ESEP LESIA, Observatoire de Paris-Meudon, LMD
credits:
    - Françoise Roques (LESIA)
    - Quentin Kral (LESIA)
    - Jean Schneider (LUTH)
maintainer: Pierre-Yves Martin (LESIA)
email: pierre-yves.martin@obspm.fr
---
# Exoplanet.eu web site

This catalogue is a working tool providing all the latest detections and data
announced by professional astronomers, useful to facilitate progress in
exoplanetology. Given the heterogeneity of observational papers, a
uniform catalogue (with uniform degree of credibility of planets) is
impossible. Therefore, ultimately, researchers willing to make a  quantitative,
scientific, use of the catalogue can make their own  judgement on the likelihood
of data and detections.

See what's new in the [CHANGELOG](CHANGELOG.md)

Git repository: [Gitlab exoplanet.eu repository](https://gitlab.obspm.fr/exoplanet/exoplanet.eu.git)

## Project structure

<!-- Generated using the command:
  tree -a -d
  ...and for the files:
  ls -1a
  ...and for the exoplanet files:
  ls -1a exoplanet
  ....and some manual cleanup!
-->

```shell
$ tree
.
├── amd  # A django app with AMD prediction computation.
├── canary  # A django app dir used to check that django works as expected during the Django 1.11 to Django 3.0 migration.
├── catalog  # A django app dir with all the API and page for the catalog (new design)
├── catalog_legacy  # A django app dir with all the pages for the catalog (legacy design)
├── core  # A django app dir holding anything related to the heart of the site: most of the models (stars, planets...) are here.
├── editorial  # A django app dir holding all the exoplanet.eu site itself (except the catalog pages)
├── ephemeris  # A django app with ephemeris computation for the planets.
├── doc  # is the place for all the documentation about the project.
├── ...some other app dirs soon...
│
├── common  # is the project configuration dir.
│  ├── ...
│  ├── locale  # place for all the localisation files
│  ├── settings.py  # Django configuration file
│  ├── static  # Place for all static files common to all app
│     ├── img  # standalone image files
│     └── generated_img  # image files generated from ressources from ressources dir
│  ├── urls.py  # Global url config
│  └── wsgi.py  # wsgi file to pass to Apache config
│
├── tools  # place for some tools used in the project
├── ressources  # is the place for all the media used to generate lowres version of our images
├── static  # empty dir that is a placeholder for the static ressources when the site is used in production
│
├── CHANGELOG.md  # contains a curated, chronologically ordered list of notable changes for each version of a project
├── conftest.py  # configuration and fixatures declaration for all pytest tests.
├── exoplanet.eu.sublime-project
├── exoplanet.eu.sublime-workspace
├── makemigrations_check.sh
├── manage.py  # command line tool from Django used to control the project.
├── markdownlintstyle.rb  # configuration of the markdownlint checker for pre-commit.
├── pre_django3  # is the place where we store all the files from the pre Django 3 era for reference during the Django 1.11 to Django 3.0 migration.
├── pytest.ini  # configuration file for the pytest framework we use for testing
├── README.md  # this very file you are reading!
├── requirements.txt  # python packages needed by the project to work.
├── requirements_dev.txt  # python packages needed to develop the project.
├── test  # all regression and unit test are placed here for pytest to find them.
├── venv36  #  is the dir to store the python virtual environnement. Nothing should ever be commited here!
├── version.py  # just a place to store the version of the project.
│
├── .flake8  # configuration file for the flake8 tool that check python source code for pep8 conformity.
├── .gitignore  # the one and only place to tell git what file to ignore/include.
├── .mdlrc  # configuration file for the markdownlint checker for precommit. It simply point to the actual config file markdownlintstyle.rb.
├── .pre-commit-config.yaml  # configuration file for all the checks that will
└── .stylelintrc.json  # configuration file for stylelint CSS/SCSS checker

```

## Developers

### Install

See the [INSTALL](INSTALL.md) dedicated documentation.

### Deployment

See the [DEPLOY](DEPLOY.md) dedicated documentation.

### Running the project

By default exoplanet.eu is configured to run using the **production**
configuration. So you need to tell it if you want other config (and you really
want to !).

Once you have settup your virtual environnement and it is properly sourced, you need
to setup your postgresql database:

```shell
yourname@yourmachine:~$ sudo su - postgres
postgres@yourmachine:~$ psql
postgres=# create database exoplanet_dj3_local;
```

Once it is done you can create the database schema using the migrate tool from django:

```shell
python manage.py migrate --configuration=Local
```

You can now launch the website as a local instance:

```shell
python manage.py runserver localhost:8000 --configuration=Local
```

or same thing using environment variable.

```shell
DJANGO_CONFIGURATION=LocalPym python manage.py runserver localhost:8000
```

This will use the `LocalPym` configuration. If you need to define a new specific
configuration for your local usage, add it to the `exoplanet/settings.py` file.

### Testing the project

```shell
pytest
```

## Contributing

When contributing code or documentation to the project you must respect some standards:

- commit messages must follow
  [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- any significative modification must be added to the [CHANGELOG](CHANGELOG.md) which
  follows [Keep a Changelog](https://keepachangelog.com)
- any change to project structure must be noted in this README

## Documentation

- [How to create a valid exoplanet database (before Django 3 and in Django 3)](doc/creation_bdd_exoplanet_valide.md)
  (in french)
- [How to modify the exoplanet.eu database](doc/comment_modifier_la_bdd.md)
