"""Views usedfor tests purpose only."""


def fake_internal_error_view(request):
    """Temp view created specialy for the error 500 test"""
    raise Exception("Fake error")  # pylint: disable=broad-exception-raised
