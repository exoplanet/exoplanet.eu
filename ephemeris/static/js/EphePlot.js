// path: js/EphePlot.js

/**
 * @author MMancini
 * Ephemerids Plot
 */

//Module containing Ephemerids plot
define(["Ephemerids", "Tip", "OutputPart", "Tools"], (Ephemerids, Tip, OutputPart, Tools) => {
	const EphePlot = function (plotWhere, cmdWhere, options) {
		const plotSelect = d3.select(plotWhere)
		const cmdSelect = d3.select(cmdWhere)
		if (plotSelect.empty()) {
			console.log("EphePlot: plot selection is empty :", plotWhere)
			return
		}
		if (cmdSelect.empty()) {
			console.log("EphePlot: cmd selection is empty :", cmdWhere)
		}

		const AxisVar = new OutputPart.OutVector()

		const Precision = new Tools.Sensibility(1e-14)

		// default
		const vars = {
			X: AxisVar.date,
			Y: AxisVar.theta,
			Z: AxisVar.alpha,
		}

		// plot size
		const wcanvas = 900
		const hcanvas = 650
		const padd = { left: 80, right: 100, top: 50, bottom: 120 }
		const wplot = 650 - padd.right
		const hplot = hcanvas - padd.bottom

		// defaults
		let xdom = [Infinity, -Infinity]
		let ydom = [Infinity, -Infinity]
		let zdom = [Infinity, -Infinity]
		let dataset = []
		let allDataset = []
		let scale_x = "linear"
		let scale_y = "linear"
		const nticks = 5

		// delete old plot
		plotSelect.selectAll("#plotcontainer").remove()

		// get options
		for (const ii in options) {
			this[ii] = options[ii]
		}

		// space containing all the plot+axis
		const svg = plotSelect
			.append("svg")
			.attr("class", "plotcontainer")
			.attr("id", "plotcontainer")
			.attr("width", wcanvas)
			.attr("height", hcanvas)

		// space containing axis+grid
		const contour = svg.append("g").attr("id", "contour")

		/* Add logo*/
		Tools.AddLogoAndDate("contour", [wcanvas - 390, padd.top + 12], [30, 17])

		// space containing plot only
		const clip = svg
			.append("defs")
			.attr("id", "clip-mother")
			.append("svg:clipPath")
			.attr("id", "clip")
			.append("svg:rect")
			.attr("id", "clip-rect")
			.attr("x", padd.left)
			.attr("y", padd.top)
			.attr("width", wplot - padd.left)
			.attr("height", hplot - padd.top)

		// space containing plot only
		const visiblePlot = svg.append("g").attr("clip-path", "url(#clip)").attr("id", "visiblePlot")

		// define curve
		const lineFunction = d3.svg.line().interpolate("basis")

		// create tip
		const cursor = new Tip(svg)
		cursor.init()

		this.load = function (epheVar) {
			if (epheVar === null) {
				return
			}
			this.planet = epheVar.planet
			this.allPlanet = epheVar.planet_uncer
			this.outLivel = epheVar.outLivel
			this.today = epheVar.today

			// set period threshold for date depending on period
			AxisVar.date.cycled = 0.9 * epheVar.planet.T

			this.okError = this.allPlanet === [] ? false : true

			// Init
			this.setDataSeries() // Set Data
			this.setScale() // Set scale
			this.draw()
		}

		this.formatData = (obj, array) => {
			let point
			let point_old = { x: 0, y: 0, z: 0 }
			let locarray = []
			let base = 0
			if (vars.X.cycled !== Infinity || vars.Y.cycled !== Infinity) {
				for (let ii = 0; ii < obj.size; ii++) {
					point = {
						x: Precision.toSens(obj[vars.X.name][ii] * vars.X.factor),
						y: Precision.toSens(obj[vars.Y.name][ii] * vars.Y.factor),
						z: Precision.toSens(obj[vars.Z.name][ii] * vars.Z.factor),
					}

					xdom = [Math.min(xdom[0], point.x), Math.max(xdom[1], point.x)]
					ydom = [Math.min(ydom[0], point.y), Math.max(ydom[1], point.y)]
					zdom = [Math.min(zdom[0], point.z), Math.max(zdom[1], point.z)]

					if (
						ii > 0 &&
						(Math.abs(point.x - point_old.x) > vars.X.cycled || Math.abs(point.y - point_old.y) > vars.Y.cycled)
					) {
						array.push(locarray)
						locarray = []
						base = ii
					}

					locarray[ii - base] = point
					point_old = point
				}
			} else {
				for (ii = 0; ii < obj.size; ii++) {
					point = {
						x: Precision.toSens(obj[vars.X.name][ii] * vars.X.factor),
						y: Precision.toSens(obj[vars.Y.name][ii] * vars.Y.factor),
						z: Precision.toSens(obj[vars.Z.name][ii] * vars.Z.factor),
					}

					xdom = [Math.min(xdom[0], point.x), Math.max(xdom[1], point.x)]
					ydom = [Math.min(ydom[0], point.y), Math.max(ydom[1], point.y)]
					zdom = [Math.min(zdom[0], point.z), Math.max(zdom[1], point.z)]

					locarray[ii] = point
				}
			}

			array.push(locarray)

			// if no variation set line in the center
			if (xdom[0] === xdom[1]) {
				const a = 0.15 * xdom[0]
				xdom[0] -= a
				xdom[1] += a
			}
			if (ydom[0] === ydom[1]) {
				const a = 0.15 * ydom[0]
				ydom[0] -= a
				ydom[1] += a
			}
		}

		this.setDataSeries = function () {
			// private function to set data
			xdom = [Infinity, -Infinity]
			ydom = [Infinity, -Infinity]
			zdom = [Infinity, -Infinity]

			dataset = []
			allDataset = []

			this.formatData(this.planet, dataset)

			if (this.okError) {
				for (let jj = 0; jj < this.allPlanet.length; jj++) {
					this.formatData(this.allPlanet[jj], allDataset)
				}
			}
			let mean = 0.5 * (xdom[0] + xdom[1])
			let delta = 0.6 * Math.abs(xdom[0] - xdom[1])

			if (xdom[0] * (mean - delta) > 0) {
				xdom[0] = mean - delta
			} else {
				xdom[0] = xdom[0] * 0.9
			}
			xdom[1] = mean + delta

			mean = 0.5 * (ydom[0] + ydom[1])
			delta = 0.6 * Math.abs(ydom[0] - ydom[1])

			if (ydom[0] * (mean - delta) > 0) {
				ydom[0] = mean - delta
			} else {
				ydom[0] = ydom[0] * 0.9
			}
			ydom[1] = mean + delta

			//      console.log('setDataSeries');
		}
		this.setXScale = () => {
			// Set x scaling function

			if (scale_x === "log" && (xdom[0] <= 0 || vars.X.name === "date")) {
				alert(`The variable ${vars.X.euname} cannot be plotted in log scale!`)
				scale_x = "linear"
				contour.selectAll(".X.scale-button").attr("fill", (d) => (d === scale_x ? "#009900" : "black"))
			}

			vars.X.scalex = d3.scale[scale_x]().domain(xdom).range([padd.left, wplot])

			lineFunction.x((d) => vars.X.scalex(d.x))
		}
		this.setXAxis = () => {
			//Define X axis

			const axisTab = [
				[
					d3.svg.axis().scale(vars.X.scalex).orient("bottom").tickFormat(vars.X.format2).ticks(nticks),
					`translate(0,${hplot})`,
				],
				[
					d3.svg.axis().scale(vars.X.scalex).orient("top").tickFormat(vars.X.format2).ticks(nticks),
					`translate(0,${padd.top})`,
				],
			]

			// remove old axis
			contour.selectAll(".axis.x").remove()

			// Plot X axis
			for (const d of axisTab) {
				contour.append("g").attr("class", "axis x").attr("transform", d[1]).call(d[0])
			}
		}

		this.setYScale = () => {
			if (scale_y === "log" && (ydom[0] <= 0 || vars.Y.name === "date")) {
				alert(`The variable ${vars.Y.euname} cannot be plotted in log scale!`)
				scale_y = "linear"
				contour.selectAll(".Y.scale-button").attr("fill", (d) => (d === scale_y ? "#009900" : "black"))
			}

			// Set y  scaling function
			vars.Y.scaley = d3.scale[scale_y]().domain(ydom).range([hplot, padd.top])

			lineFunction.y((d) => vars.Y.scaley(d.y))
		}

		this.setYAxis = () => {
			// console.log("setYAxis");
			//Define X-Y axis
			const axisTab = [
				[
					d3.svg.axis().scale(vars.Y.scaley).tickFormat(vars.Y.format2).ticks(nticks).orient("left"),
					`translate(${padd.left},0)`,
				],
				[
					d3.svg.axis().scale(vars.Y.scaley).tickFormat(vars.Y.format2).ticks(nticks).orient("right"),
					`translate(${wplot},0)`,
				],
			]

			// remove old axis
			contour.selectAll(".axis.y").remove()

			// Plot axis
			// axisTab.forEach((d) => {
			// 	contour.append("g").attr("class", "axis y").attr("transform", d[1]).call(d[0])
			// })
			for (const d of axisTab) {
				contour.append("g").attr("class", "axis y").attr("transform", d[1]).call(d[0])
			}
		}

		this.setZScale = () => {
			// console.log("setZ");
			// vars.Z.scale1 = d3.scale['linear']().domain(zdom).range([padd.left, wplot]);
			// TODO: See to fix it
			// biome-ignore lint/complexity/useLiteralKeys: <I don't know how to fix it, `.linear` doees not work.>
			vars.Z.scale2 = d3.scale["linear"]()
				.domain([zdom[0], 0.5 * (zdom[0] + zdom[1]), zdom[1]])
				.range(["#4946EB", "#D62207", "#E3D80D"])
				.interpolate(d3.interpolateLab)

			// remove old axis
			contour.selectAll(".axis.z").remove()

			// Plot Z axis
			const zntick = 48
			const ext_color_domain = []
			for (ii = 0; ii < zntick; ii++) {
				ext_color_domain.push(zdom[0] + (ii * (zdom[1] - zdom[0])) / (zntick - 1))
			}

			const legend = contour.selectAll(".axis.z").data(ext_color_domain).enter().append("g").attr("class", "axis z")

			const ls_w = Math.ceil((wplot - padd.left) / zntick)
			const ls_h = 10

			legend
				.append("rect")
				.attr("x", (d, i) => padd.left + i * ls_w)
				.attr("y", hplot + padd.bottom / 2)
				.attr("width", ls_w)
				.attr("height", ls_h)
				.style("fill", (d) => vars.Z.scale2(d))
				.style("stroke-width", 0.5)
				.style("stroke", (d) => vars.Z.scale2(d))
				.style("opacity", 1)

			legend
				.append("text")
				.attr("x", (d, i) => padd.left + i * ls_w - 4)
				.attr("y", hplot + padd.bottom / 2 - ls_h / 2)
				.text((d, i) => (i % 6 ? "" : vars.Z.format2(d)))

			contour
				.append("text")
				.attr("class", "axis label z")
				.attr("x", wplot)
				.attr("y", hplot + (padd.bottom * 2) / 3 + 4)
				.attr("title", vars.Z.title)
				.text(`${vars.Z.euname} ${vars.Z.sunit}`)
		}

		// set indicator for today
		this.setToday = function () {
			// remove old
			visiblePlot.selectAll(".todaytip").remove()

			const x = vars.X.scalex(this.today[vars.X.name][1] * vars.X.factor)
			const y = vars.Y.scaley(this.today[vars.Y.name][1] * vars.Y.factor)

			visiblePlot.append("circle").attr("class", "todaytip").attr("cx", x).attr("cy", y).attr("r", 5)
			visiblePlot.append("circle").attr("class", "todaytip").attr("cx", x).attr("cy", y).attr("r", 1)
			visiblePlot
				.append("text")
				.attr("class", "todaytip")
				.attr("x", x + 5)
				.attr("y", y + 10)
				.text("Today")
		}

		this.setLabels = () => {
			// Label x
			contour
				.append("text")
				.attr("class", "axis label x")
				.attr("x", wplot - 5)
				.attr("y", hplot - 5)
				.text(`${vars.X.euname} ${vars.X.sunit}`)

			// Label y
			contour
				.append("text")
				.attr("class", "axis label y")
				.attr("x", -60)
				.attr("y", padd.right - 5)
				.attr("transform", "rotate(-90)")
				.text(`${vars.Y.euname} ${vars.Y.sunit}`)
		}

		this.setGrid = () => {
			//console.log("setGrid");

			contour.selectAll(".gridlines").remove()

			// Set gridlines grid Line
			contour
				.selectAll(".gridlines.vertical")
				.data(vars.X.scalex.ticks(nticks))
				.enter() //use x-ticks as data
				.append("line")
				.attr({
					class: "gridlines vertical",
					x1: (d) => vars.X.scalex(d),
					x2: (d) => vars.X.scalex(d),
					y1: hplot,
					y2: padd.top,
				})

			// Set horizontal grid Line
			contour
				.selectAll(".gridlines.horizontal")
				.data(vars.Y.scaley.ticks(nticks))
				.enter() //use y-ticks as data
				.append("line")
				.attr({
					class: "gridlines horizontal",
					x1: padd.left,
					x2: wplot,
					y1: (d) => vars.Y.scaley(d),
					y2: (d) => vars.Y.scaley(d),
				})
		}

		this.clean = () => {
			//      console.log("clean");
			contour.selectAll(".curve").remove()
			contour.selectAll(".curveError").remove()
			contour.selectAll(".axis.label.x,.axis.label.y").remove()
		}

		this.setScale = function () {
			this.setXScale()
			this.setYScale()
			this.setZScale()
			this.setGrid()
		}

		this.draw = function () {
			// main function to plot
			this.clean()

			if (this.okError) {
				contour
					.selectAll(".curveError")
					.data(allDataset)
					.enter()
					.append("path")
					.attr("class", "curveError")
					.attr("d", (d, i) => lineFunction(d))
			}

			this.setToday()

			for (ii = 0; ii < dataset.length; ii++) {
				contour
					.selectAll(`.curve.part${ii}`)
					.data(dataset[ii])
					.enter()
					.append("svg:line")
					.attr("class", `curve part${ii}`)
					.attr("x1", (d, i) => vars.X.scalex(d.x))
					.attr("y1", (d, i) => vars.Y.scaley(d.y))
					.attr("x2", (d, i) => {
						if (i < dataset[ii].length - 1) {
							return vars.X.scalex(dataset[ii][i + 1].x)
						}
						return vars.X.scalex(d.x)
					})
					.attr("y2", (d, i) => {
						if (i < dataset[ii].length - 1) {
							return vars.Y.scaley(dataset[ii][i + 1].y)
						}
						return vars.Y.scaley(d.y)
					})
					.attr({
						stroke: (d, i) => vars.Z.scale2(d.z),
					})
					.on("mousemove", function () {
						cursor.set(d3.mouse(this), [vars.X, vars.Y])
					})
					.on("mouseover", () => {
						cursor.off()
					})
					.on("mouseout", () => {
						cursor.on()
					})
			}

			//      console.log('draw');
			this.setXAxis()
			this.setYAxis()
			this.setLabels()
		}

		//to plot new variable on Y
		this.replotOnAxis = function (axis, newvar) {
			vars[axis] = newvar
			this.setDataSeries()
			switch (axis) {
				case "X":
					this.setXScale()
					break
				case "Y":
					this.setYScale()
					break
				case "Z":
					this.setZScale()
					break
			}
			this.setGrid()
			this.draw()
		}

		//to plot new variable on X
		this.replot = function () {
			if (allDataset.length === [] && this.okError) {
				this.setDataSeries()
				this.setScale()
			}
			//console.log("replot");
			this.draw()
		}

		/**
		 * Create scale buttons for switching the axis scale
		 */
		this.buttonChangeScale = function () {
			let cumulativeLength = padd.left / 2 + 25
			// append a group to contain all lines
			svg
				.append("svg:g")
				.attr("class", "scale-button-group")
				.selectAll("g")
				.data(["linear", "log"])
				.enter()
				.append("g")
				.append("svg:text")
				.attr("class", "scale-button Y")
				.attr("text-anchor", "middle")
				.attr("cursor", "pointer")
				.text((d) => d)
				.attr("font-size", ".8em") // this must be before "x" which dynamically determines width
				.attr("fill", (d) => (d === scale_y ? "#009900" : "black"))
				.classed("selected", (d) => d === scale_y)
				.attr("x", function (d, i) {
					// return it at the width of previous labels (where the last one ends)
					const returnX = cumulativeLength
					// increment cumulative to include this one
					cumulativeLength += this.getComputedTextLength() + 2
					return returnX
				})
				.attr("dx", "-2em")
				.attr("y", padd.top)
				.attr("dy", "-2em")
				.on("click", function (d, i) {
					handleMouseClickScaleButtonY(this, i)
				})

			cumulativeLength = wplot
			// append a group to contain all lines
			svg
				.append("svg:g")
				.attr("class", "scale-button-group")
				.selectAll("g")
				.data(["linear", "log"])
				.enter()
				.append("g")
				.append("svg:text")
				.attr("class", "scale-button X")
				.attr("cursor", "pointer")
				.text((d) => d)
				.attr("font-size", ".8em") // this must be before "x" which dynamically determines width
				.attr("fill", (d) => (d === scale_x ? "#009900" : "black"))
				.classed("selected", (d) => d === scale_x)
				.attr("x", function (d, i) {
					// return it at the width of previous labels (where the last one ends)
					const returnX = cumulativeLength
					// increment cumulative to include this one
					cumulativeLength += this.getComputedTextLength() + 10
					return returnX
				})
				.attr("dx", "-2.5em")
				.attr("y", hplot)
				.attr("dy", "+3em")
				.on("click", function (d, i) {
					handleMouseClickScaleButtonX(this, i)
				})

			const handleMouseClickScaleButtonY = (button, index) => {
				scale_y = index === 1 ? "log" : "linear"

				this.setYScale()
				this.setGrid()
				this.draw()

				// change text decoration
				svg
					.selectAll(".Y.scale-button")
					.attr("fill", (d) => (d === scale_y ? "#009900" : "black"))
					.classed("selected", (d) => d === scale_y)
			}
			const handleMouseClickScaleButtonX = (button, index) => {
				scale_x = index === 1 ? "log" : "linear"

				this.setXScale()
				this.setGrid()
				this.draw()

				// change text decoration
				svg
					.selectAll(".X.scale-button")
					.attr("fill", (d) => (d === scale_x ? "#009900" : "black"))
					.classed("selected", (d) => d === scale_x)
			}
		}

		//Selector of axis
		this.buttonChangeVar = function (axis, where) {
			const that = this

			const selection = d3.select(where)
			if (selection.empty()) {
				console.log("buttonChangeAxisVar: plot selection is empty :", where)
				return
			}

			if (axis !== "X" && axis !== "Y" && axis !== "Z") {
				//	console.log("buttonChangeAxisVar: strange choise for axis");
			}

			// callaback function to replot
			function change() {
				const locvar = this.options[this.selectedIndex].__data__

				if (locvar.reqLivel > that.outLivel) {
					alert(`The set of input values does not have results for this output variable :\n"${locvar.euname}"`)

					nino.property("value", () => vars[axis].name)
					return
				}

				that.replotOnAxis(axis, locvar)
			}

			selection
				.append("label")
				.attr("class", "btn_changeVar scatt")
				.text(() => {
					let a = ""
					if (axis !== "Z") {
						a = `${axis} axis `
					} else {
						a = "Color"
					}
					return a
				})
				.attr("for", axis)

			// Add lines corresponding to the axis
			const nino = selection
				.append("select")
				.attr("class", "input_text btn_changeVar scatt")
				.attr("name", axis)
				.on("change", change)

			nino
				.selectAll("option")
				.data(Object.keys(AxisVar).map((key) => AxisVar[key]))
				.enter()
				.append("option")
				.text((d) => d.euname)
				.attr("value", (d) => d.name)
				.attr("title", (d) => d.title)

			// set default on dropDown
			nino.property("value", () => vars[axis].name)
		}

		// Insert button
		this.buttonChangeScale()
		this.buttonChangeVar("X", cmdWhere)
		this.buttonChangeVar("Y", cmdWhere)
		this.buttonChangeVar("Z", cmdWhere)
	}

	return EphePlot
})
