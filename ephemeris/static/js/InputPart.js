// path: js/InputPart.js

/**
 * @author MMancini
 * InputPart for Ephemerides
 */
//
//Module containing Input Part for Ephemerids
define(["Tools", "InVars", "InPeriods"], (Tools, InVars, InPeriods) => {
	// Class for Input parameter
	function InputPart() {
		this.outLivel = 0 // Livel for output: [1..6]

		this.distance = new InVars("distance", "stars__distance", {
			title: "Distance",
			symbol: "d",
			unit: "pc",
			livel: 3,
			minm: 0,
			defaut: 10,
			factor: Tools.Consts.pctometer,
		})
		this.eccent = new InVars("eccent", "eccentricity", {
			title: "Eccentricity, values in [0,1)",
			symbol: "e",
			livel: 1,
			min: 0,
			maxm: 1,
			defaut: "0",
		}) // e
		this.inclin = new InVars("inclin", "inclination", {
			title: "Inclination, values in [0,180)",
			symbol: "i",
			unit: "\xB0",
			livel: 1,
			min: 0,
			maxm: 180,
			defaut: "30",
		}) // i
		this.omegaP = new InVars("omegaP", "omega", {
			title: "Argument of periapsis, periodic on [0,360)",
			symbol: "\u03C9<sub>P</sub>",
			unit: "\xB0",
			livel: 1,
			min: 0,
			maxm: 360,
			defaut: "0",
		}) // w
		this.semiax = new InVars("semiax", "axis", {
			title: "Semi-major axis",
			symbol: "a",
			unit: "AU",
			livel: 2,
			minm: 0,
			defaut: 1,
			factor: Tools.Consts.autometer,
		}) // a
		this.T = new InVars("Period", "period", {
			title: "Period",
			symbol: "T",
			unit: "day",
			livel: 4,
			minm: 0,
			defaut: 365.25,
		}) // t

		this.tperi = new InVars("tperi", "tperi", {
			title: "Time at periapsis",
			symbol: "t<sub>P</sub>",
			unit: "day",
			livel: 5,
			minm: 0,
			defaut: 2451545.0,
		}) // tperi
		this.radiusP = new InVars("radiusP", "radius", {
			title: "Planetary radius",
			symbol: "R<sub>P</sub>",
			unit: "R<sub>jup</sub>",
			livel: 6,
			minm: 0,
			defaut: 0.091058632,
			factor: Tools.Consts.jup_radius,
		})

		this.albedo_geo = new InVars("albedo_geo", "albedo", {
			title: "Geometric albedo, values in [0,1]",
			symbol: "A",
			livel: 6,
			min: 0,
			max: 1,
			defaut: 0.367,
		})
		this.albedo_scat = new InVars("albedo_scat", "albedo_scat", {
			title: "Single-scattering albedo for Rayleigh, values in [0,1] ",
			symbol: "\u03C9",
			livel: 7,
			min: 0,
			max: 1,
			defaut: 0.45,
		})
		this.longit = new InVars("longit", "longit", {
			title: "Longitude of the ascending node, periodic on [0,360)",
			symbol: "\u03A9",
			unit: "\xB0",
			livel: 8,
			min: 0,
			maxm: 360,
			defaut: 90,
		})

		this.tempP = new InVars("tempP", "temp_calculated", {
			title: "Planetary caluclated temperature",
			symbol: "T<sub>calc</sub>",
			unit: "K",
			livel: 7,
			defaut: "computed",
			min: 0,
			factor: Tools.Consts.jup_radius,
		})

		this.star_r = new InVars("star_r", "stars__radius", {
			title: "Star radius",
			symbol: "R<sub>S</sub>",
			unit: "R<sub>sun</sub>",
			livel: 7,
			defaut: 1,
			factor: Tools.Consts.sun_radius,
		})

		this.star_m = new InVars("star_m", "stars__mass", {
			title: "Star mass",
			symbol: "M<sub>S</sub>",
			unit: "M<sub>sun</sub>",
			livel: 7,
			defaut: 1,
			factor: 1,
		})

		this.star_t = new InVars("star_t", "stars__teff", {
			title: "Effective temperature of a host star",
			symbol: "T<sub>S</sub>",
			unit: "K",
			livel: 7,
			min: 0,
			defaut: 5778,
		})

		this.central_wl = new InVars("central_wl", "", {
			title: "Central band wavelength",
			symbol: "\u03BB<sub>c</sub>",
			unit: "\u03BCm",
			livel: 7,
			min: 0,
			immutable: 0.88,
		})

		this.band_wl = new InVars("band_wl", "", {
			title: "bandwidth",
			symbol: "\u03BB<sub>m</sub>",
			unit: "\u03BCm",
			livel: 7,
			min: 0,
			immutable: 0.55,
		})

		this.date = new InPeriods("date", "date", { title: "Date", symbol: "t", unit: "date", defaut: "YYYY-MM-DD" })
		this.fraction = new InPeriods("fraction", "fraction", {
			title: "Period fraction",
			symbol: "f<sub>T</sub>",
			min: 0,
			max: 1,
		})
		this.anomaly = new InPeriods("anomaly", "anomaly", { title: "True anomaly", symbol: "\u03BD", unit: "\xB0" })
	}

	InputPart.prototype = {
		// get Input informations. For the moment they are manually set.
		getInput: function () {
			this.T.initByGetParam()

			this.semiax.initByGetParam()

			this.eccent.initByGetParam()

			this.inclin.initByGetParam()

			this.omegaP.initByGetParam()

			this.tperi.initByGetParam()

			this.tempP.initByGetParam()

			this.distance.initByGetParam()

			this.star_t.initByGetParam()

			this.star_r.initByGetParam()

			this.star_m.initByGetParam()

			this.radiusP.initByGetParam()

			this.albedo_scat.initByGetParam()

			this.albedo_geo.initByGetParam()

			this.longit.initByGetParam()

			/* does not exist on the DB */
			this.central_wl.initByGetParam()
			this.band_wl.initByGetParam()

			// put value in the form
			for (const ii in this) {
				if (this[ii] instanceof InVars) {
					this[ii].put()
				}
			}
		},

		readInput: function () {
			//console.log("readInput");
			let defaultIsUsed = false
			for (const ii in this) {
				if (this[ii] instanceof InVars) {
					defaultIsUsed = this[ii].extract() || defaultIsUsed
				} else if (this[ii] instanceof InPeriods) {
					//defaultIsUsed = this[ii].extract();
					this[ii].extract()
				}
			}
			if (defaultIsUsed) {
				document.getElementById("warning_default").innerHTML =
					"WARNING: For some variable which is not present in the database, the default" + " value will be used."
			} else {
				document.getElementById("warning_default").innerHTML = ""
			}
		},

		controlInput: function () {
			//console.log("control");

			// initialize at max outLivel
			this.outLivel = 8

			for (const ii in this) {
				/* This part is commented because from now, using default values
				 * all outvaiables have to be plotted */
				// 	// verifiy 1-livel
				// 	if(this[ii].livel==1){
				// 	  if(this[ii].value===""){
				// 	    alert("For the computation of ephemerids,"+
				// 		  " the variable \""+this[ii].euname+"\""+
				// 		  " is mandatory  but"
				// 		  + " it is not present in the database.\n" +
				// 		  "Please give it a value! \n"
				// 		  +"(info: "+this[ii].title+")");
				// 	    this.outLivel = 0;
				// 	    document.getElementById("var_"+this[ii].name).focus();
				// 	    return;
				// 	  }
				// 	}
				// 	// verify livel > 1
				// 	else{
				// 	  if(this[ii].fromdef){
				// 	    this.outLivel = Math.min(this.outLivel,this[ii].livel-1);
				// 	  }
				// 	}
				// control on limits
				if (
					this[ii].value > this[ii].max ||
					this[ii].value >= this[ii].maxm ||
					this[ii].value < this[ii].min ||
					this[ii].value <= this[ii].minm
				) {
					alert(this[ii].title)
					// alert("The variable \""+this[ii].euname+"\""+
					// 	  "must be in "+
					// 	  ((this[ii].minm==-Infinity) ? ('['+this[ii].min) : ('('+this[ii].minm))
					// 	  +','+
					// 	  ((this[ii].maxm==Infinity) ? (this[ii].max+']') : (this[ii].maxm+')'))
					// 	 );
					this.outLivel = 0
					document.getElementById(`var_${this[ii].name}`).focus()
					return
				}
			}
		},

		formInVars: function (where) {
			const selection = d3.select(where || this.formWhere)
			if (selection.empty()) {
				console.log("createForm: selection is empty :", where)
				return
			}

			// set names
			const buttonNames = []

			for (const ii in this) {
				if (this[ii] instanceof InVars) {
					buttonNames.push(this[ii])
				}
			}

			selection.append("p").text("")
			const table = selection.append("table").attr("class", "inputtable")

			// title
			table.append("caption").text("Parameters of the planet:").attr("class", "inputtable")

			// thead
			table
				.append("thead")
				.attr("class", "inputtable")
				.selectAll("th")
				.data(["", "value", "min err", "max err"])
				.enter()
				.append("th")
				.attr("class", "inputtable")
				.text((d) => d)

			const tablelines = table.selectAll(".invars").data(buttonNames).enter().append("tr").attr("class", "invars")

			tablelines
				.append("td")
				.append("xhtml:body")
				.attr("class", "symbols")
				.attr("title", (d) => d.title)
				.html((d) => `${d.name} (${d.symbol}) : `)
			// 	.append("text")
			// 	.attr("title",function(d){return d.title;})
			// 	.text(function(d){return (d.name)+" ("+d.symbol+") : ";})
			// ;

			tablelines
				.append("td")
				.append("input")
				.attr("class", "input_var")
				//.attr("class",function(d){return (d.livel===1)? 'input_var needed':'input_var';})
				.attr("id", (d) => `var_${d.name}`)
				.attr("title", (d) => d.title)
				.attr("placeholder", (d) => d.defaut)

			tablelines
				.append("td")
				.append("input")
				.attr("class", "input_err")
				.attr("id", (d) => `minerr_${d.name}`)

			tablelines
				.append("td")
				.append("input")
				.attr("class", "input_err")
				.attr("id", (d) => `maxerr_${d.name}`)

			tablelines
				.append("td")
				// .attr("class","input_unit")
				.append("xhtml:body")
				.attr("class", "symbols")
				.attr("id", (d) => `unit_${d.name}`)
				.attr("title", (d) => d.title)
				.html((d) => (d.unit === undefined ? "" : d.unit))

			// .append("text")
			// .attr("id",function(d){return "unit_"+d.name;})
			// .text(function(d){
			//   return (d.unit===undefined) ? "" : d.unit;
			// })
		},

		formInPeriods: function (where) {
			// table for period variables

			const selection = d3.select(where || this.formWhere)
			if (selection.empty()) {
				console.log("tableInPeriods: selection is empty :", where)
				return
			}

			// lines
			const buttonNames = [this.fraction, this.anomaly, this.date]

			// table for period variables
			selection.append("p").text("")
			const table = selection.append("table").attr("class", "inputtable")

			// title
			table.append("caption").text("Observation period:").attr("class", "inputtable")

			// thead
			table
				.append("thead")
				.attr("class", "inputtable")
				.selectAll("th")
				.data(["", "min", "max", "steps"])
				.enter()
				.append("th")
				.attr("class", "inputtable")
				.text((d) => d)

			// setting fraction input
			table
				.selectAll(".inperiod")
				.data(buttonNames)
				.enter()
				.append("tr")
				.attr("class", "inperiod")
				.append("td")
				.append("xhtml:body")
				.attr("class", "symbols")
				.attr("title", (d) => d.title)
				.html((d) => `${d.name} (${d.symbol}) : `)

			const tablelines = table.selectAll(".inperiod")

			tablelines
				.append("td")
				.append("input")
				.attr("class", "input_date")
				.attr("type", (d) => (d.name === "date" ? "date" : "text"))
				.attr("id", (d) => `var_${d.name}_t0`)
				.attr("title", (d) => `${d.title} (min)`)
				.attr("placeholder", (d) => d.defaut)

			tablelines
				.append("td")
				.append("input")
				.attr("class", "input_date")
				.attr("type", (d) => (d.name === "date" ? "date" : "text"))
				.attr("id", (d) => `var_${d.name}_t1`)
				.attr("title", (d) => `${d.title} (max)`)
				.attr("placeholder", (d) => d.defaut)

			tablelines
				.append("td")
				.append("input")
				.attr("class", "input_unit")
				.attr("id", (d) => `var_${d.name}_nstep`)
				.attr("title", (d) => `${d.title} (nstep)`)
				.attr("placeholder", 50)

			tablelines
				.append("td")
				.attr("class", "input_unit")
				.append("text")
				.attr("id", (d) => `unit_${d.name}`)
				.text((d) => (d.unit === undefined ? "" : d.unit))

			table.select("#var_fraction_t0").attr("value", 0)
			table.select("#var_fraction_t1").attr("value", 1)
			table.select("#var_fraction_nstep").attr("value", 100)
		},
	}

	return InputPart
})
