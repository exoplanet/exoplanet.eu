// path: js/Tip_histo.js

/**
 * @author MMancini
 * Tip_histo for Plot
 */

//Module containing Tip_histo for Plot
define(() => {
	function Tip_histo(svg) {
		const formatCount = d3.format(",.5g")

		// Set pointer for the curve
		this.remove = () => {
			svg.select(".groupTip").remove()
		}

		this.init = function () {
			this.group = svg.append("g").attr("id", "tiptool").attr("class", "groupTip").style("display", "none")

			const rect = svg
				.select("#tiptool")
				.append("rect")
				.attr("class", "groupTip")
				.attr("opacity", "1")
				.attr("x", 90)
				.attr("y", 60)
				.attr("width", 250)
				.attr("height", 50)
				.attr("rx", 10)
				.attr("ry", 10)

			this.text = svg
				.select("#tiptool")
				.selectAll("#cursortext")
				.data([
					{ x: 102, y: 82 },
					{ x: 102, y: 97 },
				])
				.enter()
				.append("text")
				.attr("class", "groupTip")
				.attr("x", (d) => d.x)
				.attr("y", (d) => d.y)
		}

		this.set = function (pos, val, vars) {
			this.group.attr("transform", `translate(${pos})`)
			this.text.text((d, i) =>
				i === 1
					? `${vars.euname} : ${vars.format6(vars.scale.invert(pos[0]))} ${vars.sunit}`
					: `% : ${formatCount(val.y)}`,
			)
		}

		this.on = function () {
			this.group.style("display", "none")
		}

		this.off = function () {
			this.group.style("display", null)
		}
	}

	return Tip_histo
})
