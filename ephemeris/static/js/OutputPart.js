// path: js/OutoutPart.js

/*
 * @author MMancini
 * OutPart for Ephemerides
 */

//Module containing Output Part for Ephemerids
define(["Tools", "TimeTools"], (Tools, TT) => {
	// Class for Output  variable
	const OutVars = function (name, options) {
		this.name = name
		this.euname = name
		this.sname = name
		this.unit = ""
		this.sunit = ""
		this.factor = 1
		this.symbol = ""
		this.title = ""
		this.reqLivel = 1
		this.format2 = undefined
		this.format6 = undefined
		this.scale = null
		this.scalex = null
		this.scaley = null
		this.cycled = Infinity //treshold value of the period if periodic

		// get options
		for (const ii in options) {
			this[ii] = options[ii]
		}
	}

	const OutVarsAngle = function (name, options) {
		OutVars.call(this, name, options)
		this.unit = "\xB0"
		this.sunit = "(\xB0)"
		this.factor = 1
		this.format2 = (d) => Math.floor(d)
		this.format6 = (d) => Math.floor(d)
		this.cycled = 330
	}

	const OutVarsGen = function (name, options) {
		OutVars.call(this, name, options)
		this.format2 = d3.format(`,.${2}e`)
		this.format6 = d3.format(`,.${6}e`)
	}

	const OutVarsGen2 = function (name, options) {
		OutVars.call(this, name, options)
		this.format2 = d3.format(`,.${4}e`)
	}

	function OutScalar() {
		this.ps_flux_thermal_ratio = new OutVarsGen2("ps_flux_thermal_ratio", {
			title: "Star and planet thermal flux",
			symbol: "",
			unit: "",
			reqLivel: 8,
		})

		this.primary_depth = new OutVarsGen2("primary_depth", {
			title: "Primary transit depth in stellar flux",
			symbol: "",
			unit: "",
			reqLivel: 8,
		})
		this.secondary_depth = new OutVarsGen2("secondary_depth", {
			title: "Secondary transit depth in stellar flux",
			symbol: "",
			unit: "",
			reqLivel: 8,
		})

		this.secondary_depth = new OutVarsGen2("secondary_depth", {
			title: "Secondary transit depth in stellar flux",
			symbol: "",
			unit: "",
			reqLivel: 8,
		})

		this.tempP = new OutVarsGen2("tempP", {
			title: "Planetary caluclated temperature",
			symbol: "Tcalc",
			unit: "K",
			livel: 7,
			min: 0,
			factor: Tools.Consts.jup_radius,
		})

		this.array = [this.ps_flux_thermal_ratio, this.primary_depth, this.secondary_depth, this.tempP]
	}

	OutScalar.prototype = {
		setOutput: function (where) {
			const selection = d3.select(where)
			if (selection.empty()) {
				console.log("OutScalar.setOutput: selection is empty :", where)
				return
			}
			// creation of variables

			selection.select("#scalarresult").remove()
			const table = selection.append("table").attr("id", "scalarresult").attr("class", "inputtable")

			// title
			table.append("caption").text("Output variables:")

			const righe = table
				.selectAll("tr")
				.data(this.array)
				.enter()
				.append("tr")
				.attr("id", (d) => `var_${d.name}`)
				.attr("title", (d) => d.title)
				.attr("class", "inputtable")

			righe
				.append("td")
				// .append("text")
				.attr("class", "symbols")
				.text((d) => `${d.name + d.sunit} = `)

			righe
				.append("td")
				.attr("class", "symbols")
				// .append("text")
				.text((d) => d.format2(d.value))
			righe
				.append("td")
				.attr("class", "symbols")
				// .append("text")
				.text((d) => d.unit)
		},
	}

	// Class for Output parameter
	function OutVector() {
		this.anomaly = new OutVarsAngle("anomaly", {
			euname: "True anomaly",
			title: "True anomaly",
			symbol: "",
			reqLivel: 1,
		})

		this.alpha = new OutVarsAngle("alpha", {
			euname: "Phase angle",
			title: "Phase angle",
			symbol: "",
			reqLivel: 1,
		})

		this.date = new OutVars("date", {
			euname: "Date",
			title: "Date",
			symbol: "",
			reqLivel: 4,
			format2: TT.julianToFormatDate,
			format6: TT.julianToFormatDate,
		})

		this.fraction = new OutVarsGen("fraction", {
			euname: "Orbital fraction",
			title: "Orbital fraction",
			symbol: "f_T",
			reqLivel: 1,
			cycled: 0.9,
		})

		this.theta = new OutVarsGen("theta", {
			euname: "Angular separation",
			title: "Angular separation",
			unit: '"',
			sunit: '(")',
			symbol: "",
			reqLivel: 3,
		})

		this.rad_speed = new OutVarsGen("rad_vel", {
			euname: "Radial velocity",
			title: "Radial velocity",
			unit: "km/s",
			sunit: "(km/s)",
			symbol: "",
			reqLivel: 3,
		})

		this.r_proj = new OutVarsGen("r_proj", {
			euname: "Separation",
			title: "Axial Star-planet distance",
			unit: "AU",
			sunit: "(AU)",
			factor: 1 / Tools.Consts.autometer,
			symbol: "",
			reqLivel: 3,
		})

		this.radius = new OutVarsGen("radius", {
			euname: "Star-planet distance",
			title: "Star-planet distance",
			unit: "AU",
			sunit: "(AU)",
			factor: 1 / Tools.Consts.autometer,
			symbol: "",
			reqLivel: 3,
		})

		this.pol = new OutVarsGen("pol", {
			euname: "Polarization",
			title: "Polarization",
			unit: "%",
			sunit: "(%)",
			symbol: "",
			reqLivel: 7,
		})

		this.cr = new OutVarsGen("cr", {
			euname: "Contrast Ratio",
			title: "Contrast Ratio",
			symbol: "",
			reqLivel: 6,
		})

		// this.total_cr = new OutVarsGen('total_cr',{
		//   euname   : 'Total Contrast Ratio',
		//   title    : 'Total Contrast Ratio',
		//   symbol   : '',
		//   reqLivel : 8
		// });

		this.crpol = new OutVarsGen("crpol", {
			euname: "Polarized Contrast",
			title: "Polarized Contrast",
			unit: "%",
			sunit: "(%)",
			symbol: "",
			reqLivel: 7,
		})

		this.north_angle = new OutVarsAngle("north_angle", {
			euname: "Angle to the north",
			title: "Planet-Star angle with North",
			symbol: "",
			reqLivel: 8,
		})

		this.det_mov = new OutVarsGen("det_mov", {
			euname: "Detector displacement",
			title: "Detector displacement",
			unit: '"/h',
			sunit: '("/h)',
			symbol: "",
			reqLivel: 8,
		})

		this.pol_ang = new OutVarsAngle("pol_ang", {
			euname: "Polarization angle",
			title: "Polarization angle",
			symbol: "",
			reqLivel: 8,
		})
	}

	return {
		OutVector: OutVector,
		OutScalar: OutScalar,
	}
})
