// path: js/EpheInterface.js

/**
 * @author MMancini
 * Interface for Ephemerids
 */

//Module containing Ephemerids Interfaces
define(["EphePlot", "EpheHisto", "Ephemerids", "InputPart", "Base64", "Tools"], (
	EphePlot,
	EpheHisto,
	Ephemerids,
	InputPart,
	Base64,
	Tools,
) => {
	// Class for Input parameter
	function EpheInterface(options) {
		// Extend InputPart when input variables are defined
		InputPart.call(this)

		// default errors off
		this.okError = false

		// default Plot (scatt) or histogram (hist)
		this.type = "scatt"

		if (options instanceof Object) {
			// get fields from input object
			for (const key in options) {
				this[key] = options[key]
			}
		}
	}

	EpheInterface.prototype = {
		formInPeriods: InputPart.prototype.formInPeriods,
		formInVars: InputPart.prototype.formInVars,
		readInput: InputPart.prototype.readInput,
		controlInput: InputPart.prototype.controlInput,
		getInput: InputPart.prototype.getInput,

		plotInit: function (plotWhere, cmdWhere) {
			this.plotWhere = plotWhere || this.plotWhere
			if (d3.select(this.plotWhere).empty()) {
				console.log("EpheInterface.plotInit: selection is empty :", this.plotWhere)
				return
			}
			this.cmdWhere = cmdWhere || this.cmdWhere
			if (d3.select(this.cmdWhere).empty()) {
				console.log("EpheInterface.plotInit: cmd selection is empty :", this.cmdWhere)
				return
			}

			// delete all buttons
			d3.selectAll(this.cmdWhere).selectAll(".btn_changeVar").remove()

			if (this.type === "scatt") {
				this.plot = new EphePlot(this.plotWhere, this.cmdWhere)

				this.plot.load(this.readFormAndRun())
			} else {
				this.histo = new EpheHisto(this.plotWhere, this.cmdWhere)

				this.histo.load(this.readFormAndRun())
			}
		},

		plotReload: function () {
			document.body.style.cursor = "progress"
			if (this.type === "scatt") {
				this.plot.load(this.readFormAndRun())
			} else {
				this.histo.load(this.readFormAndRun())
			}
			this.planet.ScalarOut.setOutput("#epheTimeIn")
			document.body.style.cursor = "auto"
		},

		// read values from input form and launch Epehemerids
		readFormAndRun: function () {
			// get inputs from foms
			this.readInput()

			// launch controls to verify input variables
			this.controlInput()
			if (this.outLivel === 0) {
				return null
			}

			// compute ephemerides
			const dataephe = new Ephemerids(
				{
					fraction: this.fraction.list,
					date: this.date.list,
					anomaly: this.anomaly.list,
					T: this.T.value,
					semiax: this.semiax.value * this.semiax.factor,
					eccent: this.eccent.value,
					inclin: this.inclin.value,
					omegaP: this.omegaP.value,
					tperi: this.tperi.value,
					distance: this.distance.value * this.distance.factor,
					radiusP: this.radiusP.value * this.radiusP.factor,
					tempP: this.tempP.value,
					albedo_scat: this.albedo_scat.value,
					albedo_geo: this.albedo_geo.value,
					longit: this.longit.value,
					star_r: this.star_r.value * this.star_r.factor,
					star_m: this.star_m.value * this.star_m.factor,
					star_t: this.star_t.value * this.star_t.factor,
					central_wl: this.central_wl.value,
					band_wl: this.band_wl.value,
				},
				{
					T: this.T.err,
					semiax: { min: this.semiax.err.min * this.semiax.factor, max: this.semiax.err.max * this.semiax.factor },
					eccent: this.eccent.err,
					inclin: this.inclin.err,
					omegaP: this.omegaP.err,
					tperi: this.tperi.err,
					distance: {
						min: this.distance.err.min * this.distance.factor,
						max: this.distance.err.max * this.distance.factor,
					},
					radiusP: { min: this.radiusP.err.min * this.radiusP.factor, max: this.radiusP.err.max * this.radiusP.factor },
					albedo_scat: this.albedo_scat.err,
					albedo_geo: this.albedo_geo.err,
					longit: this.longit.err,
				},
				this.okError,
			)

			// needed to save data
			this.planet = dataephe.planet

			// PROVA !!!!!!!!!!!!!!!!!!!!!!!!!!!
			dataephe.outLivel = this.outLivel

			// this.ScalarOut.ps_flux_thermal_ratio.value = this.ps_flux_thermal_ratio;
			// this.ScalarOut.primary_depth.value = this.primary_depth;
			// this.ScalarOut.secondary_depth.value = this.secondary_depth;

			return dataephe
		},

		// function switch histogram/scatter
		switchPlot: function (type, where) {
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("switchPlot: selection is empty :", where)
				return
			}
			const title = type === "hist" ? "Histogram" : "Curves"
			selection
				.append("a")
				.attr("data-toggle", "tab")
				.text(title)
				.attr("href", where)
				.on("click", () => {
					this.type = type
					this.plotInit()
				})
		},

		// Button to switch between histogram and scatter
		buttonSwitchScatterHisto: function (where) {
			const that = this
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("buttonLoadData: selection is empty :", where)
				return
			}
			selection
				.append("input")
				.attr("type", "button")
				.attr("class", "submit_btn_right btn btn-default")
				.attr("value", "Set Histogram")
				.attr("title", "Toogle Histogram/Scatter plot")
				.on("click", function () {
					if (that.type === "hist") {
						that.type = "scatt"
						this.value = "Set histogram   "
					} else {
						that.type = "hist"
						this.value = "Set scatter plot"
					}
					that.plotInit()
				})
		},

		// checkbox error
		buttonSwitchError: function (where) {
			const that = this
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("buttonSwitchError: selection is empty :", where)
				return
			}

			selection
				.append("input")
				.attr("class", "submit_btn btn btn-default")
				.attr("name", "buttonError")
				.attr("type", "button")
				.attr("value", "Set errors on")
				.attr("title", "Toogle errors")
				.on("click", function () {
					if (that.okError) {
						that.okError = false
						this.value = "Set errors on"
					} else {
						that.okError = true
						this.value = "Set errors off"
					}
					that.plotReload()
				})
		},

		// reload button
		buttonLoadData: function (where) {
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("buttonLoadData: selection is empty :", where)
				return
			}

			selection
				.append("input")
				.attr("class", "submit_btn btn btn-default")
				.attr("name", "buttonLoadData")
				.attr("type", "button")
				.attr("value", "Update")
				.attr("title", "Reload the input parameters and recompute the results")
				.on("click", () => this.plotReload())
		},

		// Button to save data
		buttonToSaveData: function (where) {
			const that = this
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("buttonLoadData: selection is empty :", where)
				return
			}

			selection
				.append("input")
				.attr("name", "buttonSaveData")
				.attr("type", "button")
				.attr("class", "submit_btn btn btn-default")
				.attr("value", "Save data")
				.attr("title", "Save data")
				.attr("name", "saveData")
				.on("click", createDataToSave)

			// create formatted data
			function createDataToSave() {
				let data = ""
				for (const ii in that) {
					if (that[ii].print) {
						data += `${that[ii].print()}\n`
					}
				}

				window.location.href = `data:application/octet-stream;base64,${Base64.encode(data)}`
				//	var dataWindow = window.open("data:text/html,"+ encodeURIComponent(data));
				//	dataWindow.focus();
			}
		},

		// Add button to save image
		buttonToSaveImage: function (where) {
			const that = this
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("buttonLoadData: selection is empty :", where)
				return
			}

			// selection.append("input")
			//   .attr("name","buttonSave")
			//   .attr("type","button")
			//   .attr("class","submit_btn btn btn-default")
			//   .attr("value","Save Figure")
			//   .attr("title","Save Figure in png")
			//   .attr("name","saveImage")
			//   .on("click", saveaspng);

			function saveaspng() {
				// Transforms svg image in canvas and then puts it into another windows
				d3.select("body")
					.append("canvas")
					.attr("width", 700 - 100)
					.attr("height", 650)
					.attr("id", "canvas")
					.style("display", "none")

				// This function clone a d3 element recoursively
				function cloneAll(selector) {
					const nodes = d3.selectAll(selector)
					nodes.each(function (d, i) {
						nodes[0][i] = this.parentNode.insertBefore(this.cloneNode(true), this.nextSibling)
					})
					return nodes
				}

				const a = cloneAll("#plotcontainer")

				// Setting svg properties because css style file is not taken into account
				a.selectAll("text").style("stroke", "none").style("font-size", 10).style("font-weight", "normal")
				a.selectAll(".label").style("font-size", 13)
				a.selectAll("text.axis").style("text-anchor", "end")
				a.selectAll(".tick")
					.selectAll("line")
					.style("stroke", "black")
					.style("fill", "none")
					.style("stroke-width", "1px")
				a.selectAll(".domain")
					.style("stroke", "black")
					.style("fill", "none")
					.style("stroke-width", "1px")
					.style("shape-rendering", "crispEdges")
				a.selectAll(".gridlines")
					.style("stroke", "lightgrey")
					.style("opacity", 0.95)
					.style("shape-rendering", "crispEdges")
					.style("stroke-dasharray", 3)

				if (that.type === "scatt") {
					a.selectAll("line.curve").style("stroke-width", 3).style("fill", "none")
					a.selectAll("path.curveError").style({
						stroke: "#009900",
						opacity: ".1",
						fill: "none",
						"stroke-width": "1.5px",
						"shape-rendering": "crispEdges",
					})
					a.select("#visiblePlot").remove()
					a.selectAll(".scale-button-group").remove()
					a.select("#clip-mother").remove()
				} else {
					// some elements have to be deleted because of canvg
					a.selectAll("#horizmakeup").remove()
					a.selectAll("rect.bar").style("fill", "#009900").style("stroke", "none")
				}
				a.select("#tiptool").remove()

				/* removing logo image from figure: it has to be added to the canvas */
				a.select("#logoImg").remove()
				a.select("#LogoDateId").selectAll("text").style("font-size", 13)

				// Transformation SVG to CANVAS
				canvg("canvas", a.html(), { ignoreMouse: true })

				const canvas = document.getElementById("canvas")

				Tools.AddImageToCanvas(canvas, [510, 42], [30, 17])

				// Canvas2Image.saveAsPNG(canvas);
				//var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
				// here is the most important part because if you dont replace you will get a DOM 18 exception.
				//window.location.href = image;
				// draw to canvas...
				canvas.toBlob((blob) => {
					saveAs(blob, "exoplanet_eu.png")
				})

				d3.selectAll("canvas").remove()
				a.remove()

				//window.location = document.getElementById("canvas").toDataURL("image/png");
				//window.location = document.getElementById("canvas").toDataURL("image/svg+xml;base64");
			}
		},

		buttonReloadInitalPlanet: function (where) {
			const selection = d3.select(where || this.cmdWhere)
			if (selection.empty()) {
				console.log("buttonReloadInitalPlanet: selection is empty :", where)
				return
			}
			selection
				.append("input")
				//	  .attr("name","buttonSave")
				.attr("type", "button")
				.attr("class", "submit_btn btn btn-default")
				.attr("value", "Restore Planet")
				.attr("title", "Restore the initial values for planet")
				.on("click", () => {
					this.getInput()
					this.plotReload()
				})
		},
	}

	return EpheInterface
})
