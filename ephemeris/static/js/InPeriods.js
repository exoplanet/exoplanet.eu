// path: js/InPeriods.js

/**
 * @author MMancini
 * Contains class for Period variables in Ephemerides (EpheInterface)
 */

//Module containing Ephemerids Interfaces
define(["TimeTools", "Tools"], (TT, Tools) => {
	// Class for time interval
	function InPeriods(name, euname, options) {
		const eu = euname || name

		this.name = name
		this.euname = eu

		this.t0 = 0
		this.t1 = 0
		this.nstep = 50
		this.unit = ""
		this.title = ""
		this.symbol = ""
		this.list = []

		// get options
		for (const ii in options) {
			this[ii] = options[ii]
		}

		if (this.unit === "date" && !TT.dateSupported()) {
			// no native support for <input type="date">
			this.unit = "JD"
		}

		// Extract value from a input form
		this.extract = function () {
			if (this.unit === "date") {
				this.t0 = document.getElementById(`var_${this.name}_t0`).valueAsDate
				this.t1 = document.getElementById(`var_${this.name}_t1`).valueAsDate
			} else {
				this.t0 = Number(document.getElementById(`var_${this.name}_t0`).value)
				this.t1 = Number(document.getElementById(`var_${this.name}_t1`).value)
			}
			this.nstep = Number(document.getElementById(`var_${this.name}_nstep`).value)

			if (this.t0 !== this.t1) {
				if (this.unit === "date") {
					this.t0 = TT.calendar_to_julian(this.t0)
					this.t1 = TT.calendar_to_julian(this.t1)
				}
				this.list = Tools.linspace(this.t0, this.t1, this.nstep)
			}
		}

		this.print = function () {
			if (this.list.length > 0) {
				return `${this.euname} : ${this.unit} : ${this.list.map((d) => d.toPrecision(7))} ; `
			}
			return ""
		}
	}

	return InPeriods
})
