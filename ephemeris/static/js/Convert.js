// path: js/Convert.js

/**
 * @author MMancini
 * Convert functions
 */

define(() => {
	const degtoradian = Math.PI / 180 // from degrees => radian
	const asectoradian = degtoradian / 3600 // from second => radian
	const mastoradian = asectoradian / 1000 // from mili-arc-second (mas) => radian
	const daytosec = 24 * 3600 // from day to second

	function nu_to_fr(nu, eccentricity) {
		// converts true anomaly angles [deg] to orbital period fraction [% of period]
		// input: nu [deg] (can be vector), eccentricity [0-1]
		// output: fr [% of period] (same vector-size as nu)

		const E = 2 * Math.atan(Math.sqrt((1 - eccentricity) / (1 + eccentricity)) * Math.tan((degtoradian * nu) / 2))

		//fraction de T ecoulee depuis le dernier periastre
		return ((E - eccentricity * Math.sin(E)) / (2 * Math.PI)) % 1
	}

	function fr_to_nu(fr, eccentricity, prec) {
		// converts orbital period fraction [% of period] to true anomaly
		// angles [deg], with a given precision
		// input: fr [% of period] (can be vector), eccentricity [0-1];
		// output: nu [deg]  (same vector-size as fr)

		const newPrec = prec || 1e-10

		const M = fr * 2 * Math.PI
		let E = M + eccentricity * Math.sin(M)
		const inter = Math.sqrt((1 + eccentricity) / (1 - eccentricity))

		let Ebis = M + eccentricity * Math.sin(E)
		let ii = 0
		while (Math.max(Math.abs(Ebis - E)) >= newPrec * E && ii < 100) {
			E = Ebis
			Ebis = M + eccentricity * Math.sin(E)
			ii++
		}
		return ((2 * Math.atan(inter * Math.tan(0.5 * Ebis))) / degtoradian) % 360
	}

	function app_size(length, distance, unit) {
		// Compute the apparent size of a length seen at a distance, in the angular unit
		// Takes length & distance [same unit], returns [angle unit]
		// input length [whatever], distance [same as length], unit ['rad','deg','mas','arcsec']
		// output: angular size [unit]

		const newUnit = unit || "ARCSEC"

		const asize = []
		let factor = 0

		switch (newUnit.toUpperCase()) {
			case "RAD":
				factor = 1
				break
			case "DEG":
				factor = 1 / degtoradian
				break
			case "MAS":
				factor = 1 / mastoradian
				break
			case "ARCSEC":
				factor = 1 / asectoradian
				break
			default:
				console.warn(`The used unit: ${newUnit.toUpperCase()} does not exist. The default ARCSEC will be used.`)
		}

		for (let ii = 0; ii < length.length; ii++) {
			asize[ii] = 2 * Math.atan(length[ii] / (2 * distance)) * factor
		}
		return asize
	}

	return {
		daytosec: daytosec,
		degtoradian: degtoradian,
		asectoradian: asectoradian,
		mastoradian: mastoradian,
		nu_to_fr: nu_to_fr,
		fr_to_nu: fr_to_nu,
		app_size: app_size,
	}
})
