// path: js/InVars.js

/**
 * @author MMancini
 * Contains class for Variables in Ephemerides (EpheInterface)
 */

//Module containing Ephemerids Interfaces
define(() => {
	// Class for Input  variable
	function InVars(name, euname, options) {
		const eu = euname || name

		this.name = name
		this.euname = eu
		this.unit = ""
		this.factor = 1
		this.defaut = undefined //default value
		this.value = undefined
		this.fromdef = false
		this.title = ""
		this.symbol = ""
		this.err = { min: 0, max: 0 } //max and min error
		this.min = -Infinity
		this.max = Infinity
		this.minm = -Infinity
		this.maxm = Infinity
		this.livel = 10
		this.immutable = undefined

		// get options
		for (const ii in options) {
			this[ii] = options[ii]
		}

		this.init = function (value, error) {
			// this.euname = name;
			if (value === "None") {
				return
			}
			this.value = value
			if (Array.isArray(error)) {
				if (error.length === 0) {
					this.err = { min: 0, max: 0 }
				} else {
					this.err = { min: error[0], max: error[1] }
				}
			} else if (typeof error === "number") {
				this.err = { min: error, max: error }
			} else {
				this.err = error
			}
		}

		this.initByGetParam = function () {
			let value = ""
			let error = []
			const euname = this.euname

			const id_param = `#${euname}`
			const id_param_value = `#${euname}_value`
			const id_param_error_min = `#${euname}_error_min`
			const id_param_error_max = `#${euname}_error_max`

			if ($(id_param).length !== 0) {
				value = $(id_param_value).val()
				if ($(id_param_error_min).length !== 0 && $(id_param_error_max).length !== 0) {
					const error_min = $(id_param_error_min).val()
					const error_max = $(id_param_error_max).val()
					error = [error_min, error_max]
				}
			}
			// particular case where the input variable are fixed !!
			else if (this.immutable !== undefined) {
				value = this.immutable
			}

			this.init(value, error)
		}

		// Set value on a input form
		this.put = function () {
			//console.log(this.name,this.value,this.fromdef,this.defaut)
			if (this.value !== "" && this.value !== undefined && this.fromdef === false) {
				document.getElementById(`var_${this.name}`).value = this.value
			} else {
				document.getElementById(`var_${this.name}`).value = ""
			}
			document.getElementById(`minerr_${this.name}`).value = this.err.min
			document.getElementById(`maxerr_${this.name}`).value = this.err.max
			document.getElementById(`unit_${this.name}`).value = this.unit
		}

		// Extract value from a input form
		this.extract = function () {
			const readVal = document.getElementById(`var_${this.name}`).value

			if (readVal) {
				this.value = Number(readVal)
				this.fromdef = false
			} else {
				this.value = Number(this.defaut)
				this.fromdef = true
			}
			this.err.min = Number(document.getElementById(`minerr_${this.name}`).value)
			this.err.max = Number(document.getElementById(`maxerr_${this.name}`).value)

			return this.fromdef //return message concerning if default is used
		}

		this.print = function () {
			let str
			if (this.value) {
				str = `${this.euname} : ${this.unit} : ${this.value.toPrecision(9)} : ${this.err.min.toPrecision(
					9,
				)} : ${this.err.max.toPrecision(9)} ; `
			} else if (this.defaut) {
				str = `${this.euname} : ${this.unit} : ${this.defaut} : ${this.err.min.toPrecision(
					9,
				)} : ${this.err.max.toPrecision(9)} ; `
			} else {
				str = `${this.euname} : NOT DEFINED ;`
			}

			return str
		}
	}

	return InVars
})
