// path: js/Rayleigh.js

/**
 * @author MMancini
 * Rayleigh tools
 */

//Module containing Rayleigh tools
define(["Tools", "Convert", "Orbit"], (Tools, Convert, Orbit) => {
	//Column 1: Phase angle [deg],
	//Column 2: phase integral at albedo 0,
	//Column 3: phase integral at albedo 1.
	const phase_integral_from_orbital_phase = [
		[0.0, 1.0, 1.0],
		[2.0e1, 8.654427e-1, 9.114073e-1],
		[4.0e1, 6.20842e-1, 7.08274e-1],
		[6.0e1, 3.892428e-1, 4.900848e-1],
		[8.0e1, 2.399216e-1, 3.060673e-1],
		[1.0e2, 1.570253e-1, 1.788695e-1],
		[1.2e2, 1.098726e-1, 9.407792e-2],
		[1.4e2, 6.561321e-2, 3.978817e-2],
		[1.6e2, 2.395879e-2, 1.062003e-2],
		[1.8e2, 0.00004e-3, 0.000439e-3],
	]

	const polarization_peak_from_scat_albedo = [
		[0.0, 1.0],
		[5.555556e-2, 9.906116e-1],
		[1.111111e-1, 9.772637e-1],
		[1.666667e-1, 9.633329e-1],
		[2.222222e-1, 9.469246e-1],
		[2.777778e-1, 9.301797e-1],
		[3.333333e-1, 9.12691e-1],
		[3.888889e-1, 8.93844e-1],
		[4.444444e-1, 8.732083e-1],
		[5.0e-1, 8.51818e-1],
		[5.555556e-1, 8.273997e-1],
		[6.111111e-1, 8.000507e-1],
		[6.666667e-1, 7.69098e-1],
		[7.222222e-1, 7.335296e-1],
		[7.777778e-1, 6.950759e-1],
		[8.333333e-1, 6.45556e-1],
		[8.888889e-1, 5.872632e-1],
		[9.444444e-1, 5.038479e-1],
		[1.0, 3.2762e-1],
	]

	const polarization_from_orbital_phase = [
		[0.0, 0.00001],
		[1.0e1, 1.620658e-2],
		[2.0e1, 6.557188e-2],
		[3.0e1, 1.469884e-1],
		[4.0e1, 2.616836e-1],
		[5.0e1, 4.153725e-1],
		[6.0e1, 5.960456e-1],
		[7.0e1, 7.772495e-1],
		[8.0e1, 9.204579e-1],
		[9.0e1, 9.765196e-1],
		[10.0e1, 9.204579e-1],
		[11.0e1, 7.772495e-1],
		[12.0e1, 5.960456e-1],
		[13.0e1, 4.153725e-1],
		[14.0e1, 2.616836e-1],
		[15.0e1, 1.469884e-1],
		[16.0e1, 6.557188e-2],
		[17.0e1, 1.620658e-2],
		[18.0e1, 0.00001],
	]

	function set_Rayleigh_func(x_frac, kind) {
		const newX_frac = x_frac || 0
		const inter1d_vec = []
		const length = phase_integral_from_orbital_phase.length

		for (let ii = 0; ii < length; ii++) {
			inter1d_vec[ii] = [
				phase_integral_from_orbital_phase[ii][0],
				phase_integral_from_orbital_phase[ii][1] * (1 - newX_frac) +
					newX_frac * phase_integral_from_orbital_phase[ii][2],
			]
		}

		if (kind === "cubic") {
			return (val) => Tools.interpol_cub_1D(inter1d_vec, val)
		}
		// defailt: linear
		return (val) => Tools.interpol_lin_1D(inter1d_vec, val)
	}

	function pol_inter(kind) {
		if (kind === "cubic") {
			return (val) => Tools.interpol_cub_1D(polarization_from_orbital_phase, val)
		}
		// defailt: linear
		return (val) => Tools.interpol_lin_1D(polarization_from_orbital_phase, val)
	}

	function pol_peak(kind) {
		if (kind === "cubic") {
			return (val) => Tools.interpol_cub_1D(polarization_peak_from_scat_albedo, val)
		}
		// defailt: linear
		return (val) => Tools.interpol_lin_1D(polarization_peak_from_scat_albedo, val)
	}

	return {
		set_Rayleigh_func: set_Rayleigh_func,
		pol_peak: pol_peak,
		pol_inter: pol_inter,
	}
})
