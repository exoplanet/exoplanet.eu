// path: js/main.js

// This is how you use modules without defining a new module at the same time. Think
// of this as "import ..." or "include ...".
require(["Tools", "TimeTools", "Ephemerids", "Planet", "EphePlot", "EpheInterface"], (
	Tools,
	TT,
	Ephemerids,
	Planet,
	EphePlot,
	EpheInterface,
) => {
	const timing = new Tools.Timing().start()

	// Initialize variable
	const Interface = new EpheInterface({
		plotWhere: "#plot",
		formWhere: "#epheParameterIn",
		cmdWhere: "#epheMainCmd",
	})

	// Create forms for visualize (and change) the input parameters
	Interface.formInPeriods("#epheTimeIn")
	Interface.formInVars("#epheParameterIn")

	// Get value from "I DONT KNOW WHERE" and push the value in the
	// form of interface.
	Interface.getInput()

	// Compute ephemerides and plot it
	Interface.plotInit()

	document.getElementById("timing2").innerHTML = `Plotting : ${timing.stop().print()} ms`

	// Create reload buttons

	Interface.buttonSwitchScatterHisto("#epheMainCmd0")

	Interface.buttonSwitchError("#epheMainCmd2")
	Interface.buttonToSaveData("#epheMainCmd2")
	Interface.buttonToSaveImage("#epheMainCmd2")
	Interface.buttonReloadInitalPlanet("#epheMainCmd2")

	Interface.buttonLoadData("#epheTimeIn")
	TT.Converter_JD_DAY("#epheTimeIn")

	Interface.planet.ScalarOut.setOutput("#epheTimeIn")
	//    console.log("end");

	//------------------------------------------
	//debug
	//document.getElementById( 'plot' ).scrollIntoView();
	//enddebug
})
