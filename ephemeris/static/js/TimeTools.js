// path: js/TimeTools.js

/**
 * @author MMancini
 * Convert functions
 */

//Module containing tools for timing and date
define(() => {
	let today = undefined

	// Test to control input-date ion html
	const dateSupported = () => {
		const i = document.createElement("input")
		i.setAttribute("type", "date")
		return i.type !== "text"
	}

	// function isDate(d,m,y){
	//   var date = new Date(y,m-1,d);
	//   var convertedDate =
	// 	  ""+date.getUTCFullYear() + (date.getUTCMonth()+1) + date.getDate();
	//   var givenDate = "" + y + m + d;
	//   return ( givenDate == convertedDate);
	// }

	// Integer division
	function idiv(a, b) {
		return (a - (a % b)) / b
	}

	// function to set to b the length of an number (fullfiling with 0s)
	function pad(a, b) {
		return `${1e15 + a}`.slice(-b)
	}

	// Truncate to integer (equivalent to python int()
	function float2int(value) {
		return value | 0
	}

	// Transform calendar to Julian
	function julian_to_calendar(JD) {
		let newJD = undefined
		if (JD === "") {
			alert("Please enter a meaningful Julian Day!")
			newJD = 0
		} else {
			newJD = JD * 1
		}
		let jf = float2int(newJD + 0.5) + 1401

		jf += idiv(idiv(4 * float2int(newJD + 0.5) + 274277, 146097) * 3, 4) - 38
		const je = 4 * jf + 3
		const jg = idiv(je % 1461, 4)
		const jh = 5 * jg + 2
		const jm = ((idiv(jh, 153) + 2) % 12) + 1
		const jt = (newJD + 0.5) % 1

		// var sec = float2int(Math.round(jt*86400%60));
		// var min = float2int(jt*1440%60);
		// var hour = float2int(jt*24);

		// [yy,mm,jj]
		return [idiv(je, 1461) - 4716 + idiv(14 - jm, 12), jm, idiv(jh % 153, 5) + 1]
	}

	// Julian Day Converter
	function calendar_to_julian(formJD) {
		const date = formJD || new Date()

		let JD = date.getDate()
		let JM = date.getUTCMonth() + 1

		let JY = date.getUTCFullYear()
		const JH = 12
		const jmn = 0
		const js = 0
		const ja = Math.floor((14 - JM) / 12)
		JY += +4800 - ja
		JM += 12 * ja - 3

		JD +=
			Math.floor((153 * JM + 2) / 5) +
			365 * JY +
			Math.floor(JY / 4) -
			Math.floor(JY / 100) +
			Math.floor(JY / 400) -
			32045 +
			(JH - 12) / 24 +
			jmn / 1440 +
			js / 86400

		return JD
	}

	function julianToFormatDate3(d) {
		const a = julian_to_calendar(d)
		return `${a[0]}-${pad(a[1], 2)}-${pad(a[2], 2)}`
	}

	function julianToFormatDate(d) {
		const a = julian_to_calendar(d)
		return `${pad(a[2], 2)}-${pad(a[1], 2)}-${a[0]}`
	}

	function formatDateToJulian(d) {
		const dateParts = d.split("-")
		const date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2])
		return calendar_to_julian(date)
	}

	// convert JD to DAY
	function Converter_JD_DAY(where) {
		const selection = d3.select(where)
		if (selection.empty()) {
			alert.log("buttonConvert_JD_DAY: selection is empty :", where)
			return
		}

		const todayjj = calendar_to_julian()
		today = julianToFormatDate3(today)

		// selection.append("p").text("");

		// create table(and form)
		const table = selection
			.append("form")
			.attr("name", "calConverter")
			.append("table")
			.attr("class", "inputtable")
			.attr("id", "converter_jd_day")

		// put caption
		table.append("caption").text("Converter :").attr("class", "inputtable")

		// head
		table
			.append("thead")
			.append("tr")
			.selectAll("th")
			.data(["JD", "< >", "Day"])
			.enter()
			.append("th")
			.text((d) => d)

		const line = table.append("tbody").append("tr")

		line
			.append("td")
			.append("input")
			.attr("class", "input_date")
			.attr("name", "JDDAYconverter")
			.attr("placeholder", "JD")
			.attr("title", "Convert JD/date or date/JD")
			.on("blur", jd2day_func)
			.on("focus", () => {
				document.calConverter.DAYJDconverter.value = null
			})

		line
			.append("td")
			.append("input")
			.attr("name", "submitcalConverter")
			.attr("type", "button")
			.attr("class", "submit_btn_inline")
			.style("width", "100%")
			.style("height", "20px")
			.attr("value", "convert")
			.attr("title", "convert date")

		line
			.append("td")
			.append("input")
			.attr("type", "date")
			.attr("class", "input_date")
			.attr("name", "DAYJDconverter")
			.attr("placeholder", "YYYY-MM-DD")
			.attr("title", "Convert date/JD")
			.on("blur", day2jd_func)
			.on("focus", () => {
				document.calConverter.JDDAYconverter.value = null
			})

		function jd2day_func() {
			let JD = document.calConverter.JDDAYconverter.value
			if (JD === "") {
				JD = todayjj
				document.calConverter.JDDAYconverter.value = JD
			}
			if (Number.isNaN(JD)) {
				alert("JD would be a number!")
				return
			}
			document.calConverter.DAYJDconverter.value = julianToFormatDate3(JD)
		}

		function day2jd_func() {
			const date = document.calConverter.DAYJDconverter.value
			document.calConverter.JDDAYconverter.value = formatDateToJulian(date)
		}
	}

	return {
		//    julianToFormatDate3 : julianToFormatDate3,
		//    formatDateToJulian : formatDateToJulian,
		//    julian_to_calendar : julian_to_calendar,
		idiv: idiv,
		dateSupported: dateSupported,
		julianToFormatDate: julianToFormatDate,
		calendar_to_julian: calendar_to_julian,
		Converter_JD_DAY: Converter_JD_DAY,
	}
})
