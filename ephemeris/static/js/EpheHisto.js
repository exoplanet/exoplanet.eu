// path: js/EpheHisto.js

/**
 * @author MMancini
 * Ephemerids Histo
 */

//Module containing Ephemerids plot
define(["Ephemerids", "Tip", "Tip_histo", "Tools", "OutputPart"], (Ephemerids, Tip, Tip_histo, Tools, OutputPart) => {
	const EpheHisto = function (plotWhere, cmdWhere, options) {
		const plotSelect = d3.select(plotWhere)
		const cmdSelect = d3.select(cmdWhere)
		if (plotSelect.empty()) {
			console.log("EpheHisto: plot selection is empty :", plotWhere)
			return
		}
		if (cmdSelect.empty()) {
			console.log("EphePlot: cmd selection is empty :", cmdWhere)
		}

		// nbin for histogram
		const nticks = 5
		let nbin = 10
		// default
		const AxisVar = new OutputPart.OutVector()
		let varX = AxisVar.cr

		// plot size
		const wcanvas = 900
		const hcanvas = 400
		const padd = { left: 80, right: 100, top: 50, bottom: 50 }
		const wplot = 650 - padd.right
		const hplot = hcanvas - padd.bottom
		const width = wplot - padd.left
		const height = hplot - padd.bottom

		// delete other graphic
		plotSelect.selectAll("#plotcontainer").remove()

		// get options
		for (const ii in options) {
			this[ii] = options[ii]
		}

		const svg = plotSelect
			.append("svg")
			.attr("class", "plotcontainer")
			.attr("id", "plotcontainer")
			.attr("width", wcanvas)
			.attr("height", hcanvas)

		const contour = svg.append("g").attr("id", "contour").attr("transform", `translate(${padd.left},${padd.top})`)

		/* Add logo*/
		Tools.AddLogoAndDate("contour", [wcanvas - 470, 10], [30, 17])

		// A formatter for counts.
		const formatCount = d3.format(",.2f")

		// create tip
		const cursor = new Tip_histo(svg)
		cursor.init()

		// Load data in histogram space and draw it
		this.load = function (epheVar) {
			if (epheVar === null) {
				return
			}
			//      console.log("EpheHisto");

			this.planet = epheVar.planet
			this.outLivel = epheVar.outLivel

			this.draw()
		}

		this.draw = function () {
			//      console.log('histogram:draw');
			//delete old
			contour.selectAll(".bar").remove()
			contour.selectAll(".axis").remove()
			contour.selectAll(".gridlines").remove()
			contour.selectAll(".axis.label").remove()

			const values = this.planet[varX.name].map((d) => d * varX.factor)
			const extent = d3.extent(values)

			varX.scale = d3.scale.linear().domain(extent).range([0, width]).nice()

			const xAxis = d3.svg.axis().scale(varX.scale).orient("bottom").ticks(nticks).tickFormat(varX.format2)

			// Generate a histogram using twenty uniformly-spaced bins.
			const data = d3.layout.histogram().frequency(false).bins(varX.scale.ticks(nbin))(values)

			const y = d3.scale
				.linear()
				.domain([0, d3.max(data, (d) => d.y)])
				.range([height, 0])
				.nice()

			const yAxis = d3.svg.axis().scale(y).orient("left").ticks(10).tickFormat(formatCount)

			// Set horizontal grid Line
			contour
				.selectAll(".gridlines")
				.data(y.ticks(10))
				.enter() //use y-ticks as data
				.append("line")
				.attr({ class: "gridlines", x1: 0, x2: wplot - padd.left, y1: (d) => y(d), y2: (d) => y(d) })

			const wbin = varX.scale(data[1].x) - varX.scale(data[0].x)

			const bar = contour
				.selectAll(".bar")
				.data(data)
				.enter()
				.append("rect")
				.attr("class", "bar")
				.attr("x", (d) => varX.scale(d.x) + 1)
				.attr("y", (d) => y(d.y))
				.attr("width", (d) => wbin - 1)
				.attr("height", (d) => height - y(d.y))
				.on("mousemove", function (d) {
					cursor.set(d3.mouse(this), { x: d.x + d.dx / 2, y: d.y }, varX)
					contour.append("line").attr({
						id: "horizmakeup",
						x1: varX.scale(d.x) + wbin,
						x2: 0,
						y1: y(d.y),
						y2: y(d.y),
					})
				})

				.on("mouseover", () => {
					cursor.off()
					contour.selectAll("#horizmakeup").remove()
				})
				.on("mouseout", () => {
					cursor.on()
					contour.selectAll("#horizmakeup").remove()
				})

			contour.append("g").attr("class", "axis x").attr("transform", `translate(${+0},${height})`).call(xAxis)

			contour.append("g").attr("class", "axis y").attr("transform", `translate(${0},${0})`).call(yAxis)

			contour
				.append("text")
				.attr("class", "axis label")
				.attr("title", varX.title)
				.text(`${varX.euname} ${varX.sunit}`)
				.attr("x", (width + padd.left) / 2)
				.attr("y", height + padd.bottom / 2 + 10)
			// .attr("dy", "+1em")
			// .attr("dx", "+1em")

			contour
				.append("text")
				.attr("class", "axis labelhisto y")
				.attr("x", -height / 2 - padd.top)
				.attr("y", -padd.left / 2)
				.attr("transform", "rotate(-90)")
				.text("Time (% of Period)")
		}

		//Selector of axis
		this.buttonChangeVar = function (axis, where) {
			const that = this
			const selection = d3.select(where)
			if (selection.empty()) {
				console.log("buttonChangeAxisVar: plot selection is empty :", where)
				return
			}

			// callaback function to draw
			function change() {
				const locvar = this.options[this.selectedIndex].__data__

				if (locvar.reqLivel > that.outLivel) {
					alert(`The set of input values does not have results for this output variable :\n"${locvar.euname}"`)

					nino.property("value", varX.name)
					return
				}
				varX = locvar
				that.draw()
			}

			selection.append("label").attr("class", "hist btn_changeVar").text("Histogram ").attr("for", "X")

			// Add lines corresponding to the axis
			const nino = selection
				.append("select")
				.attr("class", "input_text btn_changeVar hist")
				.attr("name", "X")
				.on("change", change)

			nino
				.selectAll("option")
				.data(Object.keys(AxisVar).map((key) => AxisVar[key]))
				.enter()
				.append("option")
				.text((d) => d.euname)
				.attr("value", (d) => d.name)
				.attr("title", (d) => d.title)

			// set default on dropDown
			nino.property("value", varX.name)
		}

		//Selector of axis
		this.buttonBins = function (where) {
			const that = this
			const selection = d3.select(where)
			if (selection.empty()) {
				console.log("buttonBins: plot selection is empty :", where)
				return
			}

			// callaback function to draw
			function change() {
				nbin = this.value
				that.draw()
			}

			selection.append("label").attr("class", "hist btn_changeVar").text("Bins").attr("for", "Nbins")

			// Add lines corresponding to the axis
			const nino = selection
				.append("input")
				.attr("class", "input_text btn_changeVar")
				.attr("title", "Number of bins")
				.attr("name", "Nbins")
				.on("keyup", change)

			// set default on dropDown
			nino.property("value", nbin)
		}

		// Insert buttons
		this.buttonChangeVar("X", cmdWhere)
		this.buttonBins(cmdWhere)
	}

	return EpheHisto
})
