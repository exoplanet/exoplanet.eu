// path: js/Tip.js

/**
 * @author MMancini
 * Tip for Plot
 */

//Module containing Tip for Plot
define(() => {
	function Tip(svg) {
		// Set pointer for the curve
		this.remove = () => {
			svg.select(".groupTip").remove()
		}

		this.init = function () {
			this.group = svg.append("g").attr("id", "tiptool").attr("class", "groupTip").style("display", "none")

			const rect = svg
				.select("#tiptool")
				.append("rect")
				.attr("class", "groupTip")
				.attr("x", 5)
				.attr("y", -50)
				.attr("width", 250)
				.attr("height", 45)
				.attr("rx", 10)
				.attr("ry", 10)

			this.text = svg
				.select("#tiptool")
				.selectAll("#cursortext")
				.data([
					{ x: 15, y: -30 },
					{ x: 15, y: -15 },
				])
				.enter()
				.append("text")
				.attr("class", "groupTip")
				.attr("x", (d) => d.x)
				.attr("y", (d) => d.y)

			const x = svg
				.select("#tiptool")
				.append("line")
				.attr("class", "groupTip")
				.attr("x1", 0)
				.attr("x2", 0)
				.attr("y1", -10)
				.attr("y2", +10)

			const y = svg
				.select("#tiptool")
				.append("line")
				.attr("class", "groupTip")
				.attr("x1", +10)
				.attr("x2", -10)
				.attr("y1", 0)
				.attr("y2", 0)

			const circle = svg
				.select("#tiptool")
				.append("circle")
				.attr("class", "groupTip")
				.attr("cx", 0)
				.attr("cy", 0)
				.attr("r", 3)
		}

		this.set = function (pos, vars) {
			this.group.attr("transform", `translate(${pos})`)
			this.text.text((d, i) => {
				const loscale = i === 0 ? vars[i].scalex : vars[i].scaley
				return `${vars[i].euname} : ${vars[i].format6(loscale.invert(pos[i]))} ${vars[i].unit}`
			})
		}

		this.on = function () {
			this.group.style("display", "none")
		}

		this.off = function () {
			this.group.style("display", null)
		}
	}

	return Tip
})
