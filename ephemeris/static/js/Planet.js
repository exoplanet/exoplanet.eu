// path: js/Planet.js

/**
 * @author MMancini
 * Planet Class
 */

//Module containing Planet class
define(["Tools", "Convert", "OutputPart", "Orbit", "Rayleigh"], (Tools, Convert, OutputPart, Orbit, Rayleigh) => {
	function Planet(forerror) {
		// Orbit
		Orbit.call(this, forerror)

		// 3-level
		if (forerror === "error") {
			this.distance = 0 //(m)
		} else {
			this.distance = Tools.Consts.pctometer //(m)
		}

		// 6-level
		this.radiusP = 0 //(m)
		this.albedo_geo = 0
		// 7-level
		this.albedo_scat = 0
		this.tempP = undefined
		this.star_r = 0
		this.star_m = 0
		this.star_t = 0
		this.central_wl = 0
		this.band_wl = 0
		// output variables
		this.theta = []
		this.cr = []
		this.total_cr = []
		this.pol = []
		this.crpol = []
		this.det_mov = []

		this.ps_flux_thermal_ratio = 0
		this.size = 0

		this.ScalarOut = new OutputPart.OutScalar()

		return this
	}

	// // obscure method to copy methods from Orbit class
	// Planet.prototype = new Orbit();
	// Planet.prototype.superclass = Orbit;
	// Planet.prototype.constructor = Planet;

	// my method : copy only needed methods
	Planet.prototype = {
		compute_orbit: Orbit.prototype.compute_orbit,
		reset: Orbit.prototype.reset,
		set: Orbit.prototype.set,

		planet_star_flux_thermal_ratio: function () {
			if (this.tempP === undefined) {
				// console.log("computing planet Temperature from star");

				const r_proj_mean = this.r_proj.reduce((a, b) => a + b) / this.size //separation

				//var star_r_meter = this.star_r * Tools.Consts.sun_radius;

				this.tempP = this.star_t * (1 - this.albedo_geo) ** 0.25 * Math.sqrt((0.5 * this.star_r) / r_proj_mean)
				//console.log("tempP=",this.tempP);
			}

			const nbin = 100
			const wl = Tools.linspace(this.central_wl - 0.5 * this.band_wl, this.central_wl + 0.5 * this.band_wl, nbin)
			// console.log("To verify blackbody_spectral_irradiance:")
			const bb_star = Tools.blackbody_spectral_irradiance(this.star_t, wl, this.star_r)
			const bb_planet = Tools.blackbody_spectral_irradiance(this.tempP, wl, this.radiusP)

			this.ScalarOut.ps_flux_thermal_ratio.value = bb_planet / bb_star

			// Primary transit depth in stellar flux
			const rratio = this.radiusP / this.star_r
			this.ScalarOut.primary_depth.value = rratio ** 2 - this.ps_flux_thermal_ratio
			// Secondary transit depth in stellar flux
			this.ScalarOut.secondary_depth.value = this.ps_flux_thermal_ratio + Math.max.apply(Math, this.cr)
			this.ScalarOut.tempP.value = this.tempP
		},

		compute: function (forerror) {
			// launch orbit simulation in first
			this.compute_orbit(forerror)

			// angular separation in [arcsec]
			this.theta = Convert.app_size(this.r_proj, this.distance, "arcsec")

			// Set function for interpolation
			const Rayleigh_phase_f = Rayleigh.set_Rayleigh_func(this.albedo_scat, "linear")
			const peak_pol = Rayleigh.pol_peak("linear")
			const pol = Rayleigh.pol_inter("linear")

			const peak_fact = peak_pol(this.albedo_scat) / peak_pol(0.1)

			// albedo_geo_from_scat cannot be lower than albedo_geo
			const ss = Math.sqrt(1 - this.albedo_scat)
			let albedo_geo_from_scat =
				(0.7977 * ((1 - 0.23 * ss) * (1 - ss))) / ((1 + 0.72 * ss) * (0.95 + 0.08 * this.albedo_scat))
			albedo_geo_from_scat = Math.max(albedo_geo_from_scat, this.albedo_geo)

			this.size = this.size

			// main loop
			for (let ii = 0; ii < this.size; ii++) {
				const localpha = this.alpha[ii]

				const radalpha = Convert.degtoradian * localpha

				const ph_alb_R = Rayleigh_phase_f(localpha) * albedo_geo_from_scat

				const ph_alb_L =
					((Math.sin(radalpha) + (Math.PI - radalpha) * Math.cos(radalpha)) *
						(this.albedo_geo - albedo_geo_from_scat)) /
					Math.PI

				this.cr[ii] = (this.radiusP / this.radius[ii]) ** 2 * (ph_alb_R + ph_alb_L)

				//total_contrast_ratio
				this.total_cr[ii] = this.cr[ii] * this.ps_flux_thermal_ratio

				this.pol[ii] = (pol(localpha) * peak_fact) / (1 + ph_alb_L / ph_alb_R)

				this.crpol[ii] = this.pol[ii] * this.cr[ii]

				/* detector movement*/
				const up_idx = ii < this.size - 1 ? ii + 1 : 0

				// temporary variable for det_mov : detector movement
				const na1 = this.north_angle[ii]
				const t1 = this.theta[ii]
				const date1 = this.date[ii]
				const na2 = this.north_angle[up_idx]
				const t2 = this.theta[up_idx]
				const date2 = this.date[up_idx]

				const delta = ((na1 - na2) % 360) * Convert.degtoradian
				const ddate = (Math.abs(date2 - date1) % this.T) * 24
				const move = Math.sqrt(t1 * t1 + t2 * t2 - 2 * t1 * t2 * Math.cos(delta))
				// unit conversion, to get arcsec per hour
				this.det_mov[ii] = ddate !== 0 ? move / ddate : 0
				/* end detector movement*/
			}

			return this
		},

		print: function () {
			return Tools.printArray(this)
		},
	}

	return Planet
})
