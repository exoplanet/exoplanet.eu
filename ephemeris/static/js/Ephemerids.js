// path: js/Ephemerids.js

/**
 * @author MMancini
 * Ephemerids Computation
 */

//Module containing Ephemerids computation
define(["Tools", "TimeTools", "Planet"], (Tools, TT, Planet) => {
	function Ephemerids(obj, obj_err, okError) {
		// orbit variables
		this.omegaP = undefined // w
		this.tperi = undefined // tperi
		this.longit = undefined // o
		this.eccent = undefined // e
		this.inclin = undefined // i
		this.semiax = undefined // a
		this.T = undefined // t
		this.date = [] //date
		this.anomaly = [] //nu
		this.fraction = [] //fr

		// planet variables
		this.distance = undefined
		this.radiusP = undefined
		this.tempP = undefined
		this.albedo_geo = undefined
		this.albedo_scat = undefined

		// star variables
		this.star_r = undefined
		this.star_m = undefined
		this.star_t = undefined

		// wavelength
		this.central_wl = undefined
		this.band_wl = undefined

		// Error Variables
		this.err_min = {}
		this.err_max = {}

		// console.log("Ephemerids");

		// extract values from input for Planet
		for (const key in obj) {
			if (Object.hasOwn(this, key) && obj[key] !== undefined) {
				this[key] = obj[key]
			}
		}

		// Define planet and set
		const planet = new Planet("noerror").set(this).compute()

		// compute the flux_thermal_ratio (only on orbit)
		planet.planet_star_flux_thermal_ratio()

		// Compute today
		const tdate = TT.calendar_to_julian()
		const today = new Planet("noerror").set(this, { date: [tdate - 1, tdate, tdate + 1] }).compute()

		// Array containing Error Planets
		const planet_uncer = []

		// Computation for Errors
		if (okError) {
			// extract values from input for Errors
			for (const key in obj_err) {
				if (Object.hasOwn(this, key) && obj_err[key] !== undefined) {
					this.err_min[key] = obj_err[key].min
					this.err_max[key] = obj_err[key].max
				}
			}
			// console.log(this.err_max,this.err_min)
			const min_planet = new Planet("error").set(this.err_min)
			const max_planet = new Planet("error").set(this.err_max)

			let count = 0
			const distance_unc = Tools.unc_iter(planet.distance, [min_planet.distance, max_planet.distance])
			const radiusP_unc = Tools.unc_iter(planet.radiusP, [min_planet.radiusP, max_planet.radiusP])
			const al_geo_unc = Tools.unc_iter(planet.albedo_geo, [min_planet.albedo_geo, max_planet.albedo_geo])
			const al_sca_unc = Tools.unc_iter(planet.albedo_scat, [min_planet.albedo_scat, max_planet.albedo_scat])
			const t_unc = Tools.unc_iter(planet.T, [min_planet.T, max_planet.T])
			const a_unc = Tools.unc_iter(planet.semiax, [min_planet.semiax, max_planet.semiax])
			const e_unc = Tools.unc_iter(planet.eccent, [min_planet.eccent, max_planet.eccent], 0, 1)
			const i_unc = Tools.unc_iter(planet.inclin, [min_planet.inclin, max_planet.inclin], 0, 90)
			const w_unc = Tools.unc_iter(planet.omegaP, [min_planet.omegaP, max_planet.omegaP], -Infinity)
			const o_unc = Tools.unc_iter(planet.longit, [min_planet.longit, max_planet.longit], -Infinity)
			const tperi_unc = Tools.unc_iter(planet.tperi, [min_planet.tperi, max_planet.tperi])

			for (const ee of distance_unc) {
				for (const ff of radiusP_unc) {
					for (const gg of al_geo_unc) {
						for (const hh of al_sca_unc) {
							for (const ii of t_unc) {
								for (const jj of a_unc) {
									for (const kk of e_unc) {
										for (const ll of i_unc) {
											for (const mm of w_unc) {
												for (const nn of o_unc) {
													for (const oo of tperi_unc) {
														planet_uncer[count] = new Planet()
															.set({
																date: planet.date,
																T: ii,
																semiax: jj,
																eccent: kk,
																inclin: ll,
																omegaP: mm,
																longit: nn,
																tperi: oo,
																distance: ee,
																radiusP: ff,
																albedo_geo: gg,
																albedo_scat: hh,
															})
															.compute()
														count++
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		this.planet = planet
		this.today = today
		this.planet_uncer = planet_uncer
	}

	return Ephemerids
})
