// path: js/Tools.js

/**
 * @author MMancini
 * Convert functions
 */

//Module containing generic tools used to efemeride computation

define(() => {
	// class containing web browser
	const isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)
	const isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)
	const isFirefox = typeof InstallTrigger !== "undefined"
	const isOpera = !!window.opera || navigator.userAgent.indexOf(" OPR/") >= 0

	// class for timing
	const Timing = function () {
		// if it is possible to use performance
		this.use_perfomance = isChrome || isFirefox || isOpera

		this.t0 = undefined
		this.t1 = undefined
	}

	Timing.prototype = {
		start: function () {
			if (this.use_perfomance) {
				this.t0 = performance.now()
				this.t1 = 0
			}
			return this
		},

		stop: function () {
			if (this.use_perfomance) {
				this.t1 = performance.now()
			}
			return this
		},

		print: function () {
			if (this.use_perfomance) {
				return (this.t1 - this.t0).toFixed(2)
			}
			return "no timing"
		},
	}

	// Physical constants
	const Consts = {
		pctometer: 3.08567758e16, // from parsec => meter
		autometer: 149597871000, // from AU => meter
		autopc: 149597871000 / 3.08567758e16, // from AU => parsec
		jup_radius: 69911000, // [m]
		earth_radius: 6366000, // [m]
		sun_radius: 696000000, // [m]
		earth_mass: 5.98e24, // [kg]
	}

	// Truncate to integer (equivalent to python int()
	function float2int(value) {
		return value | 0
	}

	function cloneObject(obj) {
		if (obj === null || typeof obj !== "object") {
			return obj
		}
		const temp = obj.constructor() // give temp the original obj's constructor
		for (const key in obj) {
			temp[key] = cloneObject(obj[key])
		}
		return temp
	}

	// Write composents of object
	function wrtObject(object, initial) {
		let newinitial = initial || ""
		for (const ii in object) {
			if (object[ii] instanceof Object !== Array.isArray(object[ii])) {
				newinitial = wrtObject(object[ii], newinitial)
			}

			if (!(object[ii] instanceof Function)) {
				if (Array.isArray(object[ii])) {
					newinitial += `<dt>${ii} = ${object[ii].map((i) => ` ${i.toPrecision(7)} `)}</td>`
				} else {
					newinitial += `<dt>${ii} = ${object[ii]}</td>`
				}
			}
		}
		return newinitial
	}

	// Write object elements no arrays
	function wrtObjectScalar(object, initial) {
		let newinitial = initial || ""
		for (const ii in object) {
			if (!(object[ii] instanceof Function) && !Array.isArray(object[ii])) {
				newinitial += `<dt>${ii} = ${object[ii]}</td>`
			}
		}
		return newinitial
	}

	// Write composents of object
	function printArray(object) {
		let data = ""
		for (const ii in object) {
			if (Array.isArray(object[ii])) {
				data += `${ii} : ${object[ii].map((d) => ` ${d.toPrecision(9)}`)} ; \n`
			}
		}
		return data
	}

	// Create linear space
	function linspace(start, end, nstep) {
		const newStart = start || 0
		const newEnd = end || 1
		const newNstep = nstep || 100

		const space = []

		for (let ii = 0; ii < newNstep; ii++) {
			space[ii] = newStart + (ii * (end - newStart)) / (newNstep - 1)
		}
		return space
	}

	function interpol_lin_1D(vec, val) {
		const newVal = val || vec[0][0]

		const length = vec.length
		const delta = (vec[length - 1][0] - vec[0][0]) / (length - 1)
		const idx = float2int((newVal - vec[0][0]) / delta)

		if (idx === -1) {
			return vec[0][1] - ((newVal - vec[0][0]) / delta) * (vec[0][1] - 2 * vec[0][1] - vec[1][1])
		}
		if (idx === length - 1) {
			return (
				vec[length - 1][1] +
				((newVal - vec[length - 1][0]) / delta) * (2 * vec[length - 1][1] - vec[length - 2][1] - vec[length - 1][1])
			)
		}
		if (idx >= 0 && idx < length) {
			return vec[idx][1] + ((newVal - vec[idx][0]) / delta) * (vec[idx + 1][1] - vec[idx][1])
		}

		return undefined
	}

	function interpol_cub_1D(vec, val) {
		const length = vec.length
		const delta = (vec[length - 1][0] - vec[0][0]) / (length - 1)
		const idx = float2int((val - vec[0][0]) / delta)

		if (idx >= 0 && idx < length - 1) {
			const p = []
			const x = (val - vec[idx][0]) / delta

			if (idx === 0) {
				p[0] = 2 * vec[0][1] - vec[1][1]
			} else {
				p[0] = vec[idx - 1][1]
			}

			p[1] = vec[idx][1]
			p[2] = vec[idx + 1][1]

			if (idx === length - 2) {
				p[3] = 2 * vec[length - 1][1] - vec[length - 2][1]
			} else {
				p[3] = vec[idx + 2][1]
			}

			return (
				p[1] +
				0.5 *
					x *
					(p[2] - p[0] + x * (2.0 * p[0] - 5.0 * p[1] + 4.0 * p[2] - p[3] + x * (3.0 * (p[1] - p[2]) + p[3] - p[0])))
			)
		}

		return undefined
	}

	// Returns the (val-unc, val+unc) array that lies within the range(vmin-vmax)
	function unc_iter(val, unc, vmin, vmax) {
		const newVmin = vmin || 0
		const newVmax = vmax || Infinity

		let result
		if (unc !== [0, 0]) {
			result = [Math.max(val - unc[0], newVmin), Math.min(val + unc[1], newVmax)]
		} else {
			result = [val]
		}
		return result
	}

	// normRand: returns normally distributed random numbers
	function normRand(mean, std) {
		const newMean = mean || 0
		const newStd = std || 1

		let x1
		let x2
		let rad
		do {
			x1 = 2 * Math.random() - 1
			x2 = 2 * Math.random() - 1
			rad = x1 * x1 + x2 * x2
		} while (rad >= 1 || rad === 0)

		return newMean + newStd * x1 * Math.sqrt((-2 * Math.log(rad)) / rad)
	}

	// Gives a random gauss number (mean, std) within the range(vmin-vmax)
	function rgauss(mean, std, vmin, vmax) {
		const newVmin = vmin || 0
		const newVmax = vmax || Infinity

		return Math.min([Math.max([normRand(mean, std), newVmin]), newVmax])
	}

	// Sensibility in the graphics
	function Sensibility(myprec) {
		this.prec = 21

		this.init = function (myprec) {
			this.prec = -Math.log(myprec) / Math.LN10
			if (this.prec < 7) {
				this.prec = 21
			}
		}

		if (myprec) {
			this.init(myprec)
		}

		this.toSens = function (val) {
			return Number(val.toPrecision(this.prec))
		}
	}

	// Add Logo to something
	const AddLogoAndDate = (where, position, size) => {
		const newPosition = position || [-9, -12]
		const newSize = size || [20, 24]

		let today = new Date()
		let dd = today.getDate()
		let mm = today.getMonth() + 1 //January is 0!
		const yyyy = today.getFullYear()

		if (dd < 10) {
			dd = `0${dd}`
		}
		if (mm < 10) {
			mm = `0${mm}`
		}

		today = `exoplanet.eu, ${yyyy}-${mm}-${dd}`

		const selectedfig = d3.select(`#${where}`).append("g").attr("id", "LogoDateId")

		selectedfig
			.append("svg:image")
			.attr("id", "logoImg")
			.attr("x", newPosition[0] + 5)
			.attr("y", newPosition[1] - newSize[1] / 2)
			.attr("width", newSize[0])
			.attr("height", newSize[1])
			.attr("xlink:href", "/media/images/logo_padc.png")

		selectedfig
			.append("text")
			.attr("text-anchor", "end")
			.attr("x", newPosition[0])
			.attr("y", newPosition[1] + 5)
			.text(today)
			.attr("fill", "black")
	}

	/* adding logo to a canvas*/
	function AddImageToCanvas(canvas, position, size) {
		const newPosition = position || [-9, -12]
		const newSize = size || [20, 24]

		const context = canvas.getContext("2d")
		const base_image = new Image()
		base_image.src = "/media/images/logo_padc.png"

		context.drawImage(base_image, newPosition[0] + 5, newPosition[1] + newSize[1] / 2 + 3, newSize[0], newSize[1])
	}

	function blackbody_spectral_irradiance(teff, wl, radius) {
		/*
    Returns the emitted (at surface!) flux in W/m2/m assuming black body behaviour.
    pi*2*h*c**2 = 3.741771524664128e-16
    h*c/kb = 0.014387769599838155
     */
		//console.log("TO verify blackbody_spectral_irradiance:")
		//console.log("what is the factor :1.e-6?")
		//console.log("A term 8pi is missing?")
		//console.log("why the sum/integrale is replace by a mean");
		let sum = 0
		for (let ii = 0; ii < wl.length; ii++) {
			const rescaled = wl[ii] * 1e-6
			const exp = Math.exp(0.014387769599838155 / (rescaled * teff)) - 1
			sum += 3.741771524664128e-16 / rescaled ** 5 / exp
		}
		return (sum / wl.length) * radius * radius
	}

	return {
		Timing: Timing,
		Consts: Consts,
		cloneObject: cloneObject,
		wrtObject: wrtObject,
		wrtObjectScalar: wrtObjectScalar,
		printArray: printArray,
		float2int: float2int,
		linspace: linspace,
		interpol_lin_1D: interpol_lin_1D,
		interpol_cub_1D: interpol_cub_1D,
		unc_iter: unc_iter,
		rgauss: rgauss,
		Sensibility: Sensibility,
		AddImageToCanvas: AddImageToCanvas,
		AddLogoAndDate: AddLogoAndDate,
		blackbody_spectral_irradiance: blackbody_spectral_irradiance,
	}
})
