// path: /js/Euler.js

/**
 * @author MMancini
 * Simple rotation in 3D with respect ZXZ angles
 */

define(() => {
	function Euler(z1, x, z2) {
		this._z1 = z1 || 0
		this._x = x || 0
		this._z2 = z2 || 0
		this.setMatrix()
	}

	Euler.prototype = {
		set: function (z1, x, z2) {
			this._z1 = z1 || 0
			this._x = x || 0
			this._z2 = z2 || 0
			this.setMatrix()
		},

		copy: function (euler) {
			this._z1 = euler._z1
			this._x = euler._x
			this._z2 = euler._z2
			this.v1 = euler.v1
			this.v2 = euler.v2
			this.v3 = euler.v3
		},

		clone: function () {
			const tmp = new Euler()
			return tmp.copy(this)
		},

		setMatrix: function () {
			// assumes the upper 3x3 of m is a pure rotation matrix(i.e, unscaled)
			this.v1 = [
				Math.cos(this._z1) * Math.cos(this._z2) - Math.cos(this._x) * Math.sin(this._z1) * Math.sin(this._z2),
				-Math.cos(this._z1) * Math.sin(this._z2) - Math.cos(this._x) * Math.cos(this._z2) * Math.sin(this._z1),
				Math.sin(this._z1) * Math.sin(this._x),
			]
			this.v2 = [
				Math.cos(this._z2) * Math.sin(this._z1) + Math.cos(this._z1) * Math.cos(this._x) * Math.sin(this._z2),
				Math.cos(this._z1) * Math.cos(this._x) * Math.cos(this._z2) - Math.sin(this._z1) * Math.sin(this._z2),
				-Math.cos(this._z1) * Math.sin(this._x),
			]
			this.v3 = [Math.sin(this._x) * Math.sin(this._z2), Math.cos(this._z2) * Math.sin(this._x), Math.cos(this._x)]
		},

		applyRot: function (v) {
			const out = [0, 0, 0]
			for (let ii = 0; ii < 3; ii++) {
				out[0] += this.v1[ii] * v[ii]
				out[1] += this.v2[ii] * v[ii]
				out[2] += this.v3[ii] * v[ii]
			}
			return out
		},
	}

	return Euler
})
