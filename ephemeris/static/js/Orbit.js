// path: js/Orbit.js

/**
 * @author MMancini
 * Orbit class
 */

//Module containing Orbit class
define(["TimeTools", "Tools", "Convert", "Euler"], (TT, Tools, Convert, Euler) => {
	function Orbit(forerror) {
		const newForerror = forerror || "noerror"

		// 1-level
		this.eccent = 0 // e
		this.omegaP = 0 // w (deg)
		// 5-level
		this.tperi = 0 // tperi (day)

		if (newForerror === "error") {
			// set default when error
			// 1-level
			this.inclin = 0 // i (deg)
			// 2-level
			this.semiax = 0 // a (m)
			// 4-level
			this.T = 0 // t  (day)
			// 8-level
			this.longit = 0 // o (deg)
		} else {
			// 1-level
			this.inclin = 45 // i (deg)
			// 2-level
			this.semiax = 1 // a  (m)
			// 4-level
			this.T = 365.25 // t (day)
			// 8-level
			this.longit = 90 // o (deg)
		}

		// input optional in order
		this.date = [] //date
		this.anomaly = [] //nu
		this.fraction = [] //fr

		// outputs
		this.r_proj = []
		this.north_angle = []
		this.pol_ang = []
		this.rad_vel = []
		this.radius = []
		this.alpha = []

		// size of arrays
		this.size = 0

		return this
	}

	Orbit.prototype = {
		set: function (obj, obj2) {
			if (obj instanceof Object) {
				// get fields from input object
				for (const key in obj) {
					// if (this.hasOwnProperty(key) && obj[key] !== undefined && obj[key]) {
					if (Object.hasOwn(this, key) && obj[key] !== undefined && obj[key]) {
						this[key] = Tools.cloneObject(obj[key])
					}
				}
			}
			// Second set of option which can overwrite the first
			if (obj2 instanceof Object) {
				// get fields from input object
				for (key in obj2) {
					if (Object.hasOwn(this, key) && obj2[key] !== undefined && obj2[key]) {
						this[key] = Tools.cloneObject(obj2[key])
					}
				}
			}
			return this
		},

		copy: function (tmp) {
			for (const key in tmp) {
				if (Object.hasOwn(this, key)) {
					this[key] = Tools.cloneObject(tmp[key])
				}
			}
			return this
		},

		reset: function () {
			this.date = []
			this.anomaly = []
			this.fraction = []
			this.r_proj = []
			this.north_angle = []
			this.pol_ang = []
			this.radius = []
			this.alpha = []
			this.rad_vel = []
			this.size = 0
		},

		compute_orbit: function (forerror) {
			const newForerror = forerror || "noerror"

			// correction of tperi to do it, as is possible, near today
			if (this.tperi !== 0 && newForerror !== "error") {
				//
				const today = TT.calendar_to_julian()
				this.tperi += TT.idiv(today - this.tperi, this.T) * this.T
			}
			if (this.date.length !== 0) {
				this.size = this.date.length
				for (let ii = 0; ii < this.size; ii++) {
					this.fraction[ii] = ((this.date[ii] - this.tperi) / this.T) % 1 //[0-1]
					this.anomaly[ii] = Convert.fr_to_nu(this.fraction[ii], this.eccent, 1e-15) //[deg]
				}
			} else if (this.anomaly.length !== 0) {
				this.size = this.anomaly.length
				for (ii = 0; ii < this.size; ii++) {
					this.anomaly[ii] %= 360 //[deg]
					this.fraction[ii] = Convert.nu_to_fr(this.anomaly[ii], this.eccent) //[0-1]
					this.date[ii] = this.fraction[ii] * this.T + this.tperi //[days]
				}
			} else if (this.fraction.length !== 0) {
				this.size = this.fraction.length
				for (ii = 0; ii < this.size; ii++) {
					this.fraction[ii] %= 1 //[0-1]
					this.anomaly[ii] = Convert.fr_to_nu(this.fraction[ii], this.eccent, 1e-15) //[deg]
					this.date[ii] = this.fraction[ii] * this.T + this.tperi //[days]
				}
			} else {
				this.size = 50
				for (ii = 0; ii < this.size; ii++) {
					this.fraction[ii] = ii / (this.size - 1) //[0-1]
					this.anomaly[ii] = Convert.fr_to_nu(this.fraction[ii], this.eccent, 1e-15) //[deg]
					this.date[ii] = this.fraction[ii] * this.T //[days]
				}
			}

			const rad_num = this.semiax * (1 - this.eccent ** 2)

			const inc = Convert.degtoradian * this.inclin
			const ome = Convert.degtoradian * this.omegaP
			const euler = new Euler(Convert.degtoradian * this.longit, inc, ome)

			for (ii = 0; ii < this.size; ii++) {
				const nu = Convert.degtoradian * this.anomaly[ii]

				const rad = rad_num / (1 + this.eccent * Math.cos(nu))
				const position = euler.applyRot([rad * Math.cos(nu), rad * Math.sin(nu), 0])
				let z_old = position[2] // store old z

				const rxy = position[0] * position[0] + position[1] * position[1]
				//	this.radius[ii] = Number(Math.sqrt(rxy+position[2]*position[2]).toPrecision(8));
				this.radius[ii] = Math.sqrt(rxy + position[2] * position[2])
				this.r_proj[ii] = Math.sqrt(rxy)
				const a = Math.atan2(position[1], position[0]) / Convert.degtoradian
				this.north_angle[ii] = a
				this.pol_ang[ii] = (2 * a) % 360 //  polarization angle
				this.alpha[ii] = Math.acos(Math.sin(nu + ome) * Math.sin(inc)) / Convert.degtoradian

				if (ii > 0) {
					const ddate = (Math.abs(this.date[ii] - this.date[ii - 1]) % this.T) * Convert.daytosec
					// this.rad_vel[ii] = (position[2]-z_old)/ddate*(Tools.Consts.autometer/1000);
					this.rad_vel[ii] = (position[2] - z_old) / ddate / 1000
				} else {
					const z_0 = position[2]
				}

				if (ii === this.size - 1) {
					this.rad_vel[0] = this.rad_vel[1]
				}
				z_old = position[2] // store old z

				// var up_idx =  (ii < this.size-1) ? ii+1 : 0;
				// var ddate = (Math.abs(this.date[ii]-this.date[up_idx])%this.T) * Convert.daytosec;
				// this.rad_vel[ii] = ddate!=0 ? position[2] * Tools.Consts.autometer/ddate/1000 : 0;
			}

			// console.log("THERE IS A PROBLEM IN THE COMPUTATION OF radial"
			// 		  + " velocity vel_rad. Please look at Orbit.js");
		},
	}

	return Orbit
})
