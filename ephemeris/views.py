# Django 💩 imports
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render

# First party imports
from common.views import NewDesignBaseTemplateView
from core.models import PlanetDBView, id_from_slug
from core.outputs import EphermeridesOutput


class Ephemerides(NewDesignBaseTemplateView):
    """Display a page with the ephemerides of a planet."""

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "ephemerides.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Observability Predictor"
    description = ""
    is_beta = True

    def _set_description(self, name: str) -> None:
        """
        Set the descirption of the page with the name of the planet.

        Args:
            - name: Slug of the object.
        """
        self.description = f"Observability predicator of the planet {name}."

    def get(self, request: HttpRequest, obj_slug: str) -> HttpResponse:
        """
        Support for /ephemeris/{objname} URLs
        {objname} can be a 'koposov 27', 'koposov_27', or 'KoPoSov 27', a browser
        will be redirected to the canonical URL.
        It's a bit ugly, but user friendly; variables naming inferred from Django
        object_detail generic view

        Args:
            request (HttpRequest): http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.
            slug (str): name of the object "slugified" (all space replaced by _)
            template_name (str): name of the template to be used to render the page

        Returns:
            HttpResponse: page with an ephemeris corresponding to the provided
                planet.

        Raises:
            Http404: if slug do not correspond to any planet
        """
        output = EphermeridesOutput()
        planet = get_object_or_404(PlanetDBView, id=id_from_slug(obj_slug))

        self._set_description(planet.name)

        # If the provided slug is an old one we redirect to the same planet but wit the
        # correct modern slug
        if planet.url_keyword != obj_slug:
            return redirect(
                "planet_ephemeris",
                obj_slug=planet.url_keyword,
            )

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                planet_info=dict(
                    safe_name=planet.safe_name,
                    name=planet.name,
                    id=planet.id,
                ),
                data=output.get_values(planet),
            ),
        )
