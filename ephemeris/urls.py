"""
exoplanet.ephemeris URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Django 💩 imports
from django.urls import re_path

# Local imports
from .views import Ephemerides

# BUG a slash in the url ?! WTF
# BUG possible code injection here ?! WTF
# This is the same regex that the one we use in CATALOG_PLANET_NAME_URL_REGEX
EPHEMERIS_PLANET_NAME_URL_REGEX = r"^(?P<obj_slug>[0-9a-z-+_.()':/`\[\]*]+)/$"

urlpatterns = [
    # BUG We must replace the slug here with a simple id
    re_path(
        route=EPHEMERIS_PLANET_NAME_URL_REGEX,
        view=Ephemerides.as_view(),
        name="planet_ephemeris",
    ),
]
