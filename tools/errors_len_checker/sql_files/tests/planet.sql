SELECT id, name, axis_error, cardinality(axis_error) AS "axis_error_len" FROM core_planet WHERE cardinality(axis_error) > 2;
SELECT id, name, period_error, cardinality(period_error) AS "period_error_len" FROM core_planet WHERE cardinality(period_error) > 2;
