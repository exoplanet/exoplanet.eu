SELECT id, name, teff_error, cardinality(teff_error) AS "teff_error_len" FROM core_star WHERE cardinality(teff_error) > 2;
SELECT id, name, distance_error, cardinality(distance_error) AS "distance_error_len" FROM core_star WHERE cardinality(distance_error) > 2;
