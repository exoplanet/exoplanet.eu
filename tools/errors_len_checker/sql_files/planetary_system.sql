-- NEW DATABASE

SELECT id, distance_error, cardinality(distance_error) AS "distance_error_len" FROM core_planetarysystem WHERE cardinality(distance_error) > 2;

-- OLD DATABASE

-- SELECT id, distance_error, cardinality(distance_error) AS "distance_error_len" FROM app_planetary_system WHERE cardinality(distance_error) > 2;
