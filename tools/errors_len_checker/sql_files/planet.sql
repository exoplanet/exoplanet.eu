-- NEW DATABASE

SELECT id, name, axis_error, cardinality(axis_error) AS "axis_error_len" FROM core_planet WHERE cardinality(axis_error) > 2;
SELECT id, name, period_error, cardinality(period_error) AS "period_error_len" FROM core_planet WHERE cardinality(period_error) > 2;
SELECT id, name, eccentricity_error, cardinality(eccentricity_error) AS "eccentricity_error_len" FROM core_planet WHERE cardinality(eccentricity_error) > 2;
SELECT id, name, omega_error, cardinality(omega_error) AS "omega_error_len" FROM core_planet WHERE cardinality(omega_error) > 2;
SELECT id, name, tzero_tr_error, cardinality(tzero_tr_error) AS "tzero_tr_error_len" FROM core_planet WHERE cardinality(tzero_tr_error) > 2;
SELECT id, name, tzero_vr_error, cardinality(tzero_vr_error) AS "tzero_vr_error_len" FROM core_planet WHERE cardinality(tzero_vr_error) > 2;
SELECT id, name, tperi_error, cardinality(tperi_error) AS "tperi_error_len" FROM core_planet WHERE cardinality(tperi_error) > 2;
SELECT id, name, tconj_error, cardinality(tconj_error) AS "tconj_error_len" FROM core_planet WHERE cardinality(tconj_error) > 2;
SELECT id, name, inclination_error, cardinality(inclination_error) AS "inclination_error_len" FROM core_planet WHERE cardinality(inclination_error) > 2;
SELECT id, name, tzero_tr_sec_error, cardinality(tzero_tr_sec_error) AS "tzero_tr_sec_error_len" FROM core_planet WHERE cardinality(tzero_tr_sec_error) > 2;
SELECT id, name, lambda_angle_error, cardinality(lambda_angle_error) AS "lambda_angle_error_len" FROM core_planet WHERE cardinality(lambda_angle_error) > 2;
SELECT id, name, impact_parameter_error, cardinality(impact_parameter_error) AS "impact_parameter_error_len" FROM core_planet WHERE cardinality(impact_parameter_error) > 2;
SELECT id, name, k_error, cardinality(k_error) AS "k_error_len" FROM core_planet WHERE cardinality(k_error) > 2;
SELECT id, name, temp_calculated_error, cardinality(temp_calculated_error) AS "temp_calculated_error_len" FROM core_planet WHERE cardinality(temp_calculated_error) > 2;
SELECT id, name, mass_sini_error_string, cardinality(mass_sini_error_string) AS "mass_sini_error_string_len" FROM core_planet WHERE cardinality(mass_sini_error_string) > 2;
SELECT id, name, mass_detected_error_string, cardinality(mass_detected_error_string) AS "mass_detected_error_string_len" FROM core_planet WHERE cardinality(mass_detected_error_string) > 2;
SELECT id, name, radius_error_string, cardinality(radius_error_string) AS "radius_error_string_len" FROM core_planet WHERE cardinality(radius_error_string) > 2;

-- OLD DATABASE

-- SELECT id, name, axis_error, cardinality(axis_error) AS "axis_error_len" FROM app_planet WHERE cardinality(axis_error) > 2;
-- SELECT id, name, period_error, cardinality(period_error) AS "period_error_len" FROM app_planet WHERE cardinality(period_error) > 2;
-- SELECT id, name, eccentricity_error, cardinality(eccentricity_error) AS "eccentricity_error_len" FROM app_planet WHERE cardinality(eccentricity_error) > 2;
-- SELECT id, name, omega_error, cardinality(omega_error) AS "omega_error_len" FROM app_planet WHERE cardinality(omega_error) > 2;
-- SELECT id, name, tzero_tr_error, cardinality(tzero_tr_error) AS "tzero_tr_error_len" FROM app_planet WHERE cardinality(tzero_tr_error) > 2;
-- SELECT id, name, tzero_vr_error, cardinality(tzero_vr_error) AS "tzero_vr_error_len" FROM app_planet WHERE cardinality(tzero_vr_error) > 2;
-- SELECT id, name, tperi_error, cardinality(tperi_error) AS "tperi_error_len" FROM app_planet WHERE cardinality(tperi_error) > 2;
-- SELECT id, name, tconj_error, cardinality(tconj_error) AS "tconj_error_len" FROM app_planet WHERE cardinality(tconj_error) > 2;
-- SELECT id, name, inclination_error, cardinality(inclination_error) AS "inclination_error_len" FROM app_planet WHERE cardinality(inclination_error) > 2;
-- SELECT id, name, tzero_tr_sec_error, cardinality(tzero_tr_sec_error) AS "tzero_tr_sec_error_len" FROM app_planet WHERE cardinality(tzero_tr_sec_error) > 2;
-- SELECT id, name, lambda_angle_error, cardinality(lambda_angle_error) AS "lambda_angle_error_len" FROM app_planet WHERE cardinality(lambda_angle_error) > 2;
-- SELECT id, name, impact_parameter_error, cardinality(impact_parameter_error) AS "impact_parameter_error_len" FROM app_planet WHERE cardinality(impact_parameter_error) > 2;
-- SELECT id, name, "K_error", cardinality("K_error") AS "K_error_len" FROM app_planet WHERE cardinality("K_error") > 2;
-- SELECT id, name, temp_calculated_error, cardinality(temp_calculated_error) AS "temp_calculated_error_len" FROM app_planet WHERE cardinality(temp_calculated_error) > 2;
-- SELECT id, name, mass_sini_error_string, cardinality(mass_sini_error_string) AS "mass_sini_error_string_len" FROM app_planet WHERE cardinality(mass_sini_error_string) > 2;
-- SELECT id, name, mass_detected_error_string, cardinality(mass_detected_error_string) AS "mass_detected_error_string_len" FROM app_planet WHERE cardinality(mass_detected_error_string) > 2;
-- SELECT id, name, radius_error_string, cardinality(radius_error_string) AS "radius_error_string_len" FROM app_planet WHERE cardinality(radius_error_string) > 2;
