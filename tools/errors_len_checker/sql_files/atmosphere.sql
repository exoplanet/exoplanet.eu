-- NEW DATABASE

SELECT id AS "atmosphere_molecule_id", result_value_error, cardinality(result_value_error) AS "result_value_error_len" FROM core_atmospheremolecule WHERE cardinality(result_value_error) > 2;
SELECT id AS "atmosphere_temperature_id", result_value_error, cardinality(result_value_error) AS "result_value_error_len" FROM core_atmospheretemperature WHERE cardinality(result_value_error) > 2;

-- OLD DATABASE

-- SELECT id AS "atmosphere_molecule_id", result_value_error, cardinality(result_value_error) AS "result_value_error_len" FROM app_atmosphere_molecule WHERE cardinality(result_value_error) > 2;
-- SELECT id AS "atmosphere_temperature_id", result_value_error, cardinality(result_value_error) AS "result_value_error_len" FROM app_atmosphere_temperature WHERE cardinality(result_value_error) > 2;
