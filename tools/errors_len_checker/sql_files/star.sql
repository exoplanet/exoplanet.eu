-- NEW DATABASE

SELECT id, name, distance_error, cardinality(distance_error) AS "distance_error_len" FROM core_star WHERE cardinality(distance_error) > 2;
SELECT id, name, mass_error, cardinality(mass_error) AS "mass_error_len" FROM core_star WHERE cardinality(mass_error) > 2;
SELECT id, name, age_error, cardinality(age_error) AS "age_error_len" FROM core_star WHERE cardinality(age_error) > 2;
SELECT id, name, teff_error, cardinality(teff_error) AS "teff_error_len" FROM core_star WHERE cardinality(teff_error) > 2;
SELECT id, name, radius_error, cardinality(radius_error) AS "radius_error_len" FROM core_star WHERE cardinality(radius_error) > 2;
SELECT id, name, metallicity_error, cardinality(metallicity_error) AS "metallicity_error_len" FROM core_star WHERE cardinality(metallicity_error) > 2;

-- OLD DATABASE

-- SELECT id, name, distance_error, cardinality(distance_error) AS "distance_error_len" FROM app_star WHERE cardinality(distance_error) > 2;
-- SELECT id, name, mass_error, cardinality(mass_error) AS "mass_error_len" FROM app_star WHERE cardinality(mass_error) > 2;
-- SELECT id, name, age_error, cardinality(age_error) AS "age_error_len" FROM app_star WHERE cardinality(age_error) > 2;
-- SELECT id, name, teff_error, cardinality(teff_error) AS "teff_error_len" FROM app_star WHERE cardinality(teff_error) > 2;
-- SELECT id, name, radius_error, cardinality(radius_error) AS "radius_error_len" FROM app_star WHERE cardinality(radius_error) > 2;
-- SELECT id, name, metallicity_error, cardinality(metallicity_error) AS "metallicity_error_len" FROM app_star WHERE cardinality(metallicity_error) > 2;
