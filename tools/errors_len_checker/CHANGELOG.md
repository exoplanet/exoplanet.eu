# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 27-04-2023

### Added

- Add check for `result_value_error` field in table `core_atmospheretemperature`.
- Add check for `result_value_error` field in table `core_atmospheremolecule`.
- Add check for `axis_error` field in table `core_planet`.
- Add check for `period_error` field in table `core_planet`.
- Add check for `eccentricity_error` field in table `core_planet`.
- Add check for `omega_error` field in table `core_planet`.
- Add check for `tzero_tr_error` field in table `core_planet`.
- Add check for `tzero_vr_error` field in table `core_planet`.
- Add check for `tperi_error` field in table `core_planet`.
- Add check for `tconj_error` field in table `core_planet`.
- Add check for `inclination_error` field in table `core_planet`.
- Add check for `tzero_tr_sec_error` field in table `core_planet`.
- Add check for `lambda_angle_error` field in table `core_planet`.
- Add check for `impact_parameter_error` field in table `core_planet`.
- Add check for `k_error` field in table `core_planet`.
- Add check for `temp_calculated_error` field in table `core_planet`.
- Add check for `mass_sini_error_string` field in table `core_planet`.
- Add check for `mass_detected_error_string` field in table `core_planet`.
- Add check for `radius_error_string` field in table `core_planet`.
- Add check for `distance_error` field in table `core_planetarysystem`.
- Add check for `distance_error` field in table `core_star`.
- Add check for `mass_error` field in table `core_star`.
- Add check for `age_error` field in table `core_star`.
- Add check for `teff_error` field in table `core_star`.
- Add check for `radius_error` field in table `core_star`.
- Add check for `metallicity_error` field in table `core_star`.
