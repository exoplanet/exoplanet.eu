#!/usr/bin/env python3
"""
Error-len-checker is a tool to check if all size of errors field in database are valid.
Valid size is 0, 1 or 2. If we have size > 2 there is a good chance that there is an
input error when register data. Check if a number had a comma `,`  instead of a dot `.`.
"""

# Standard imports
from pathlib import Path
from typing import Any

# External imports
from psycopg import Connection
from psycopg.rows import DictRow
from rich.console import Console

# Local imports
from .src import connect, get_cursor, read_execute_and_fetch

CONSOLE = Console()

__version__ = "1.0.0"
__app_name__ = "Error-Len-Checker"
__status__ = "developement"
__author__ = "Ulysse CHOSSON for LESIA"
__email__ = "ulysse.chosson@obspm.fr"
__copyright__ = "LESIA"
__licence__ = "EUPL v1.2"
__contributors__: list[str] = []


def _info() -> tuple[str, str]:
    """
    Return all info on the tool.

    Returns:
        All info on the error-check-len tool.
    """
    return (
        f"[bold]{__app_name__}[/bold]: {__version__}, [bold]status[/bold]: "
        f"{__status__}. [bold]Copyright[/bold]: {__copyright__}, [bold]licence[/bold]:"
        f" {__licence__}, [bold]maintainer[/bold]: {__email__}",
        f"[bold]{__app_name__}[/bold] is a tool to check if all size of errors field "
        "in database are valid. Valid size is 0, 1 or 2. If we have size > 2 there is "
        "a good chance that there is an input error when register data. Check if a "
        "number had a comma `,`  instead of a dot `.`.",
    )


def _enumerate_file(path: Path) -> list[Path]:
    """
    Enumerate all sql files in the given path.

    Args:
        path: Path to enumerate.

    Returns:
        Path of all sql files in the given path.
    """
    return list(path.glob("*.sql"))


def _get_invalid_error(
    path: Path,
    given_conn: Connection | None = None,
) -> list[DictRow] | None:
    """
    Get invalid error from the database.

    Args:
        path: Path of the SQL file to execute.
        given_conn: Connection. It's for tests.

    Returns:
        All result from the SQL request or None if all errors are good.
    """
    conn = connect() if given_conn is None else given_conn
    cursor = get_cursor(conn)
    result = read_execute_and_fetch(path, cursor)
    cursor.close()
    if given_conn is None:
        conn.close()
    return result if result else None


def _display_result(issues: dict[str, list[dict[str, Any]] | None]) -> None:
    """
    Display result of the tool.

    Display informations of the tools and result of all requests.

    Args:
        issues: Issues to display from requests in all sql files.
    """
    info_on_app, app_description = _info()

    CONSOLE.print(info_on_app)
    print()
    CONSOLE.print(app_description)
    print()

    if not issues:
        CONSOLE.print("[bold]Issues is empty.[/bold]")
        return

    max_key_len = len(max(issues.keys(), key=len))

    if not any(issues.values()):
        CONSOLE.print("[bold]No errors in len of error_values in database.[/bold]")
        for key in issues.keys():
            CONSOLE.print(f"\t- [bold]{key}[/bold]:{' '*(max_key_len - len(key))} ✅")

    else:
        CONSOLE.print(
            "[bold]Errors detected in len of error_values in database.[/bold]",
        )
        for key, value in issues.items():
            check_symbol = "✅" if not value else "❌"
            CONSOLE.print(
                f"\t- [bold]{key}[/bold]:{' '*(max_key_len - len(key))} {check_symbol}",
            )
            if value:
                for issue in value:
                    msg = ", ".join(
                        [
                            f"{iss_key}: {iss_value}"
                            for iss_key, iss_value in issue.items()
                        ]
                    )
                    CONSOLE.print(f"\t\t• {msg}")


def main() -> None:
    """Launch to tool to detect errors in size of errors fields in database."""

    sql_files = _enumerate_file(Path(__file__).parent / "sql_files")
    issues = {sql_file.stem: _get_invalid_error(sql_file) for sql_file in sql_files}

    _display_result(issues)


if __name__ == "__main__":
    main()
