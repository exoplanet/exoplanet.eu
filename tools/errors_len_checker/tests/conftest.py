# type: ignore
"""Conftest for test of tool error-len-checker."""

# Standard imports
from pathlib import Path

# Test imports
import pytest

# External imports
import psycopg

# pylint: disable-next=unused-import
import pytest_postgresql  # noqa: F401
from psycopg.conninfo import make_conninfo
from psycopg_pool import ConnectionPool
from pytest_postgresql.executor import PostgreSQLExecutor
from pytest_postgresql.janitor import DatabaseJanitor


def _load_schema(db: ConnectionPool) -> None:
    """
    Fill a database with a basic db schema.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    # Read the SQL file contains the schema of the database.
    with open(Path(__file__).parent / "schema.sql", "rb") as sql_file:
        schema = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(schema)
                conn.commit()


def _load_data(db: ConnectionPool) -> None:
    """
    Fill a database with some data for tests.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    # Read the SQL file contains the schema of the database.
    with open(Path(__file__).parent / "load_data.sql", "rb") as sql_file:
        data = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(data)
                conn.commit


@pytest.fixture(scope="session")
def db_password() -> str:
    """Return a stupid password for the temporary database created during the tests."""
    return "dummypassword"


@pytest.fixture(scope="session")
def db_empty(postgresql_proc: PostgreSQLExecutor, db_password: str) -> ConnectionPool:
    """
    Return a temporary empty database name shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "sql_test_db_empty_rel"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            yield pool


@pytest.fixture
def db_connection_empty(db_empty: ConnectionPool) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty database.

    Args:
        - db_empty: connection pool to get a connection to an empty db

    Returns:
        A connection to the database
    """
    with db_empty.connection() as conn:
        yield conn


@pytest.fixture(scope="session")
def db_with_data(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary db with schema and data shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "sql_test_db_with_data"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_schema(pool)
            _load_data(pool)
            yield pool


@pytest.fixture(scope="session")
def db_connection_with_data(
    db_with_data: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary db with schema and data.

    Suitable for modification tests since All modification will be rolled back at the
    end of the test function.

    Args:
        - db_with_only_schema: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_data.connection() as conn:
        yield conn
        conn.rollback()
