# type: ignore

# Standard imports
from pathlib import Path

# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Local imports
from ..src import get_cursor, read_execute_and_fetch


@pytest.mark.parametrize(
    "file_path, expected_len, expected_entry",
    [
        param(
            Path("tests/planet.sql"),
            0,
            [],
            id="planet period error",
        ),
        param(
            Path("tests/star.sql"),
            1,
            [
                {"id": 2},
                {"name": "test_star_2"},
                {"teff_error_len": 4},
                {"teff_error": [0.0, 76.0, 0.0, 78.0]},
            ],
            id="star teff error",
        ),
    ],
)
def test_read_execute_fetch(
    db_connection_with_data,
    file_path,
    expected_len,
    expected_entry,
):
    cursor = get_cursor(db_connection_with_data)
    result = read_execute_and_fetch(file_path, cursor)

    with soft_assertions():
        assert_that(len(result)).is_equal_to(expected_len)
        for entry in expected_entry:
            assert_that(result[0]).contains_entry(entry)
