# type: ignore

# Standard imports
from pathlib import Path

# Test imports
import pytest
from pytest import param

# Local imports
from ..errors_len_checker import (
    _display_result,
    _enumerate_file,
    _get_invalid_error,
    _info,
)

TESTS_FILES_PATH = Path(__file__).parent.parent / "sql_files" / "tests"


def test_info():
    assert _info()


@pytest.mark.parametrize(
    "path, expected",
    [
        param(Path(__file__), [], id="on file"),
        param(
            TESTS_FILES_PATH,
            [
                TESTS_FILES_PATH / "planet.sql",
                TESTS_FILES_PATH / "star.sql",
            ],
            id="on folder",
        ),
    ],
)
def test_enumerate_file(path, expected):
    assert _enumerate_file(path) == expected


@pytest.mark.parametrize(
    "file_path, expected",
    [
        param(
            Path("tests/planet.sql"),
            None,
            id="planet period error",
        ),
        param(
            Path("tests/star.sql"),
            [
                {
                    "id": 2,
                    "name": "test_star_2",
                    "teff_error_len": 4,
                    "teff_error": [0.0, 76.0, 0.0, 78.0],
                }
            ],
            id="star teff error",
        ),
    ],
)
def test_get_invalid_error(db_connection_with_data, file_path, expected):
    result = _get_invalid_error(file_path, db_connection_with_data)
    assert result == expected


def test_display_result():
    _display_result(None)
    assert True
