CREATE TABLE core_star(
  id serial NOT NULL PRIMARY KEY,
  name varchar(60) UNIQUE NOT NULL,
  distance double precision,
  distance_error double precision[],
  teff double precision,
  teff_error double precision[]
);

CREATE TABLE core_planet (
  id serial NOT NULL PRIMARY KEY,
  name varchar(100),
  axis double precision,
  axis_error double precision[],
  period varchar,
  period_error varchar[]
);
