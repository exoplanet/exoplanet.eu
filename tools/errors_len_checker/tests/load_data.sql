--SQL Commands to fill table for tests

-- Insert in star table
INSERT INTO core_star(name, distance, distance_error, teff, teff_error)
VALUES
('test_star_1', 12.654, ARRAY[0.947, 0.836], 234.374, ARRAY[0.24, 0.26]),
('test_star_2', 47.264, ARRAY[0.278, 0.294], 284.947, ARRAY[0, 76, 0, 78]);

-- Insert in planet table
INSERT INTO core_planet(name, axis, axis_error, period, period_error)
VALUES
('test_planet_1', 12.654, ARRAY[0.947, 0.836], 234.374, ARRAY[0.24, 0.26]);
