# Errors len checker

Error len checker is a tool to check if all size of errors array field in database
are valid. Valid size is 0, 1 or 2.
If we have size > 2 there is a good chance that there is an
input error when register data. Check if a number had a comma `,`  instead of a dot `.`.

## Supported tables

- `core_atmospheretemperature`
- `core_atmospheremolecule`
- `core_planet`
- `core_planetarysystem`
- `core_star`

## Usage

To use the tool you need to create a `.env` file with your connection information to
the database.

Example

```.env
ERROR_CHECKER_DB_USER = toto
ERROR_CHECKER_DB_PASSWORD = totopassword
ERROR_CHECKER_DB_HOST = localhost
ERROR_CHECKER_DB_PORT = 5432
ERROR_CHECKER_DB_NAME = toto_db
```

Note: You can also export each variable.

To run the tool use the command

```sh
python errors_len_checker.py
```

If there are no erros you should have output that looks like the image below.

![all_good](./rsc/all_clear.png)

Otherwise the output looks like the following image.

![errors](./rsc/error.png)

## Add new fields

If you add errors arrays filed in the database you need to had those fields in sql files.

Examples:
You add a color_error field in table planet. You must have the following line in the file:
`tools/errors_len_checker/sql_files/planet.sql`

```SQL
SELECT id, name, color_error, cardinality(color_error) AS "color_error_len" FROM core_planet WHERE cardinality(color_error) > 2;
```

If you add error array fields in new table or a table don't support by the tool.

- Add a new sql file in `tools/errors_len_checker/sql_files/<table_name>.sql`
- Add in this file a line for each error array field.

```SQL
SELECT id, name, <error_field>, cardinality(<error_field>) AS "<error_field>_len" FROM <table_name> WHERE cardinality(<error_field>) > 2;
```

## Contacts

- Author: Ulysse CHOSSON (LESIA)
- Maintainer: Ulysse CHOSSON (LESIA)
- Email: <ulysse.chosson@obspm.fr>

## License

This project is licensed und the [EUPL v1.2 Licence](LICENCE.md)
