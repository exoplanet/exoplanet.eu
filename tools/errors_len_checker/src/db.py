"""Function related to database for the tool."""

# Standard imports
from os import getenv
from pathlib import Path
from typing import NamedTuple, cast

# External imports
from dotenv import load_dotenv
from psycopg import Connection, Cursor, Error
from psycopg import connect as psycopg_connect
from psycopg.rows import DictRow, dict_row


class PSQLConnectionError(Exception):
    """Exception raised when a PSQL connection error was catch in connection."""


class EnvInfo(NamedTuple):
    """
    Environment variables representation.

    Attributes:
        user: Database user name.
        password: Database user's password.
        host: Database host.
        port: Database connection port.
        database: Database name.

    """

    user: str
    password: str
    host: str
    port: str
    database: str


def _get_env_info() -> EnvInfo:
    """
    Get environment variables from a `.env` file.

    Returns:
        Environment variables from a `.env` file.
    """
    load_dotenv()
    env_user = getenv("ERROR_CHECKER_DB_USER")
    env_password = getenv("ERROR_CHECKER_DB_PASSWORD")
    env_host = getenv("ERROR_CHECKER_DB_HOST")
    env_port = getenv("ERROR_CHECKER_DB_PORT")
    env_database = getenv("ERROR_CHECKER_DB_NAME")

    if None in [
        env_user,
        env_password,
        env_host,
        env_port,
        env_database,
    ]:
        raise ValueError(
            "One of env variables is None, those env var are mandatory to use this "
            "tools. `ERROR_CHECKER_DB_USER`, `ERROR_CHECKER_DB_PASSWORD`, "
            "`ERROR_CHECKER_DB_HOST`, `ERROR_CHECKER_DB_PORT`, `ERROR_CHECKER_DB_NAME`."
        )
    return EnvInfo(
        user=cast(str, env_user),
        password=cast(str, env_password),
        host=cast(str, env_host),
        port=cast(str, env_port),
        database=cast(str, env_database),
    )


def connect() -> Connection:
    """
    Connect to a database.

    Args:
        user: User used to log in to the database.
        password: Password of the user.
        host: Host used for the connection.
        port: Port used for the connection.
        database: Database to which we want to connect.

    Returns:
        A connection to a database if all is working.

    Raises:
        PSQLConnectionError: If we have an error connecting to the database.
    """
    env_info = _get_env_info()
    try:
        connection = psycopg_connect(
            user=env_info.user,
            password=env_info.password,
            host=env_info.host,
            port=env_info.port,
            dbname=env_info.database,
        )
        return connection
    except (Exception, Error) as err:
        raise PSQLConnectionError(
            f"Error while connecting to postgreSQL {err}",
        ) from err


def get_cursor(connection: Connection) -> Cursor:
    """
    Get a cursor on the given connection.

    Args:
        connection: A connection to a database on which we want to execute commands.

    Returns:
        A cursor on the given connection to a data base to execute commands.
    """
    return connection.cursor(row_factory=dict_row)


def read_execute_and_fetch(path: Path, cursor: Cursor) -> list[DictRow]:
    """
    Read an SQL file, execute the content and fetch results.

    Args:
        path: Path of the file to read and execute.
        cursor: Cursor on a db for the execution.

    Returns:
        The result of all SQL commands that produced or retrieved something.
    """
    result = []
    with open(Path(__file__).parent.parent / "sql_files" / path, "rb") as sql_file:
        lines = sql_file.readlines()

    for line in lines:
        cursor.execute(line)
        if cursor.rowcount > 0:
            result.extend(cursor.fetchall())

    return result
