#!/usr/bin/env python
# type: ignore
"""
Script to export svg favicon to all the different favicon formats needed by the site.

See: https://applypixels.com/blog/designing-a-favicon-in-2019

BUG: the generation seems to have stopped working since python 3.10 or 3.11 due to
    changes in temp dir handling
"""
# Standard imports
from pathlib import Path
from typing import Sequence, cast

# External imports
import click
import colorama
import sh

# Local imports
from .gentools.cleaner import Cleaner, Svg2PngCleaner, Svg2SvgCleaner
from .gentools.exporter import (
    Exporter,
    Svg2IcoExporter,
    Svg2PngExporter,
    Svg2SvgExporter,
    get_inkscape_version,
)
from .gentools.reporter import Reporter, SimpleTask
from .gentools.yamlobj import Svg2Ico, Svg2Png, Svg2Svg, yaml

# colorama.init(autoreset=True)
FGREEN = colorama.Fore.GREEN
FRED = colorama.Fore.RED
FYELLOW = colorama.Fore.YELLOW
RESET_ALL = colorama.Style.RESET_ALL

ConfigList = list[Svg2Png | Svg2Svg | Svg2Ico]


def check_inkscape_installed(task: SimpleTask) -> sh.Command:
    inkscape = None
    install_type = None

    try:
        inkscape = sh.inkscape  # pylint: disable=no-member
        if sh.which("inkscape").startswith("/snap/"):  # pylint: disable=no-member
            install_type = "(snap)"
        else:
            install_type = "(system)"
    except sh.CommandNotFound:
        pass

    if not inkscape:
        try:
            # this should raise if inkscape is not installed via flatpak
            flatpak = sh.flatpak  # pylint: disable=no-member
            flatpak.run("org.inkscape.Inkscape", "--version")
            inkscape = flatpak.run.bake("org.inkscape.Inkscape")
            install_type = "(flatpak)"
        except (sh.CommandNotFound, sh.ErrorReturnCode_1):  # pylint: disable=no-member
            pass

    task.label(install_type)
    task.label(f"v{get_inkscape_version(inkscape).public}")
    task.check(inkscape)

    return inkscape


def check_optipng_installed(task: SimpleTask) -> sh.Command:
    try:
        optipng = sh.optipng  # pylint: disable=no-member
    except sh.CommandNotFound:
        pass

    task.check(optipng)

    return optipng


def check_convert_installed(task: SimpleTask) -> sh.Command:
    try:
        convert = sh.convert  # pylint: disable=no-member
    except sh.CommandNotFound:
        pass

    task.check(convert)

    return convert


def check_yaml_conf_exists(task: SimpleTask, conf: Path) -> None:
    task.label(f"{conf.resolve()}")
    task.check(conf.is_file())


def load_all_configurations(task: SimpleTask, conf: Path) -> ConfigList:
    RESULT = yaml.full_load(conf.open("r"))

    file_nb: list[int] = []
    for conf in RESULT:
        if isinstance(conf, Svg2Png):
            file_nb.append(len(conf.sizes))
        elif isinstance(conf, Svg2Svg) or isinstance(conf, Svg2Ico):
            file_nb.append(1)
        else:
            raise NotImplementedError(
                "No implementation of file counting for {type(conf)}"
            )

    task.label("Files to export:")
    task.label(f"{' | '.join(str(f) for f in file_nb)}")
    task.label(f"Total:{sum(file_nb)}")

    return cast(ConfigList, RESULT)


def create_all_exporters(
    task: SimpleTask,
    reporter: Reporter,
    confs: ConfigList,
    conf_dir: Path,
    inkscape: sh.Command,
    optipng: sh.Command,
    convert: sh.Command,
) -> Sequence[Exporter]:
    result = []
    for conf in confs:
        if isinstance(conf, Svg2Png):
            result.append(
                Svg2PngExporter(
                    conf=conf,
                    conf_dir=conf_dir,
                    inkscape=inkscape,
                    optipng=optipng,
                    reporter=reporter,
                )
            )
        elif isinstance(conf, Svg2Svg):
            result.append(
                Svg2SvgExporter(
                    conf=conf,
                    conf_dir=conf_dir,
                    inkscape=inkscape,
                    reporter=reporter,
                )
            )
        elif isinstance(conf, Svg2Ico):
            result.append(
                Svg2IcoExporter(
                    conf=conf,
                    conf_dir=conf_dir,
                    inkscape=inkscape,
                    convert=convert,
                    reporter=reporter,
                )
            )
        else:
            raise NotImplementedError(f"No exporter has been defined for {type(conf)}.")

    task.label(f"{len(result)} Exporters created")

    return result


def create_all_cleaners(
    task: SimpleTask,
    reporter: Reporter,
    confs: ConfigList,
    conf_dir: Path,
) -> Sequence[Cleaner]:
    result = []
    for conf in confs:
        if isinstance(conf, Svg2Png):
            result.append(
                Svg2PngCleaner(
                    conf=conf,
                    conf_dir=conf_dir,
                    reporter=reporter,
                )
            )
        elif isinstance(conf, Svg2Svg):
            result.append(
                Svg2SvgCleaner(
                    conf=conf,
                    conf_dir=conf_dir,
                    reporter=reporter,
                )
            )
        else:
            raise NotImplementedError(f"No cleaner has been defined for {type(conf)}.")

    task.label(f"{len(result)} Cleaners created")

    return result


@click.group(invoke_without_command=False)
def exogen() -> None:
    """Tool used to export ressources in various format (SVG, PNG, JPEG...) to version
    suitable for usage in the exoplanet.eu site."""
    pass


@exogen.command()
@click.argument(
    "yaml_conf",
    nargs=1,
    type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True),
)
@click.option(
    "--debug/--no-debug",
    default=False,
    show_default=True,
    help="Stop on any unexpected exception and show a detailed stack trace.",
)
def clean(yaml_conf: str, debug: bool) -> None:
    """
    Clean au file that would have been generated by the `generate command`.
    """
    YAML_CONF = Path(yaml_conf)
    CONF_DIR = YAML_CONF.parent

    with Reporter(
        title="Exoplanet.eu - Favicon generator",
        debug=debug,
        success_msg=f"Export {FGREEN}successful{RESET_ALL} 🍰🍰🍰",
        failure_msg=f"Export {FRED}failure{RESET_ALL} please try again {FYELLOW}",
    ) as rep:
        with rep.simpletask(title="Config YAML") as task:
            check_yaml_conf_exists(task, YAML_CONF)

        #############################

        with rep.simpletask(title="Load configurations") as task:
            CONFS = load_all_configurations(task, YAML_CONF)

        #############################

        with rep.simpletask(title="Create Exporters") as task:
            cleaners = create_all_cleaners(
                task=task, reporter=rep, confs=CONFS, conf_dir=CONF_DIR
            )

        #############################

        rep.break_if_failure()
        rep.split()

        #############################

        for cleaner in cleaners:
            cleaner.clean()


@exogen.command()
@click.argument(
    "yaml_conf",
    nargs=1,
    type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True),
)
@click.option(
    "--debug/--no-debug",
    default=False,
    show_default=True,
    help="Stop on any unexpected exception and show a detailed stack trace.",
)
def generate(yaml_conf: str, debug: bool) -> None:
    """
    Export files from their native format to the format needed by the exoplanet.eu site.
    """
    YAML_CONF = Path(yaml_conf)
    CONF_DIR = YAML_CONF.parent
    # TODO integrate out_dir in the base YAML instead of in each config instance
    # TODO integrate in_dir in the base YAML instead of using conf dir

    with Reporter(
        title="Exoplanet.eu - Favicon generator",
        debug=debug,
        success_msg=f"Export {FGREEN}successful{RESET_ALL} 🍰🍰🍰",
        failure_msg=f"Export {FRED}failure{RESET_ALL} please try again {FYELLOW}",
    ) as rep:
        inkscape = None
        optipng = None
        convert = None

        with rep.simpletask(
            title="Inkscape",
            success_msg="Installed",
            failure_msg="Not Installed 👉 Please install Inkscape",
        ) as task:
            inkscape = check_inkscape_installed(task)

        #############################

        with rep.simpletask(
            title="OptiPNG",
            success_msg="Installed",
            failure_msg="Not Installed 👉 Please install OptiPNG",
        ) as task:
            optipng = check_optipng_installed(task)

        #############################

        with rep.simpletask(
            title="ImageMagick convert tool",
            success_msg="Installed",
            failure_msg="Not Installed 👉 Please install ImageMagick",
        ) as task:
            convert = check_convert_installed(task)

        #############################

        with rep.simpletask(title="Config YAML") as task:
            check_yaml_conf_exists(task, YAML_CONF)

        #############################

        with rep.simpletask(title="Load configurations") as task:
            CONFS = load_all_configurations(task, YAML_CONF)

        #############################

        rep.break_if_failure()
        rep.split()

        #############################

        with rep.simpletask(title="Create Exporters") as task:
            exporters = create_all_exporters(
                task=task,
                reporter=rep,
                confs=CONFS,
                conf_dir=CONF_DIR,
                inkscape=inkscape,
                optipng=optipng,
                convert=convert,
            )

        #############################

        rep.break_if_failure()
        rep.split()

        #############################

        for exporter in exporters:
            exporter.export()


if __name__ == "__main__":
    exogen()
