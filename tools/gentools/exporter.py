# type: ignore
# Standard imports
import tempfile
from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path

# External imports
import sh
from bs4 import BeautifulSoup
from packaging.version import Version

# Local imports
from .reporter import Reporter
from .yamlobj import Svg2Ico, Svg2Png, Svg2Svg

BG_COLOR = "white"


def get_inkscape_version(inkscape: sh.Command) -> Version:
    return Version(inkscape("--version").split()[1])


class Exporter(ABC):
    @abstractmethod
    def export(self) -> None:
        pass


@dataclass
class Svg2PngExporter(Exporter):
    """
    Examples:
        >>> conf = Svg2Png("test", "in.svg", "out", "prefix", 0.0, ["16x16"])
        >>> repo = Reporter("test", False)
        >>> Svg2PngExporter(conf=conf, conf_dir=Path("."), inkscape=sh.touch,
        ... optipng=sh.touch, reporter=repo)
        Svg2PngExporter(...)
    """

    conf: Svg2Png
    conf_dir: Path
    inkscape: sh.Command
    optipng: sh.Command
    reporter: Reporter

    def __post_init__(self) -> None:
        """Convert pathes to actual Path objects relative to the config dir."""
        self.conf.in_svg = self.conf_dir / self.conf.in_svg
        self.conf.out_dir = self.conf_dir / self.conf.out_dir

    def _export_one(self, size: int, fake_size: int) -> None:
        """Export a svg file to png using inskape."""
        with self.reporter.simpletask(title="➡ Generating") as task:
            OUT_DIR = self.conf.out_dir
            PREFIX = self.conf.prefix
            EXPORT_PATH = OUT_DIR / f"{PREFIX}-{fake_size.x}x{fake_size.y}.png"
            IN_EXPORT_ID = self.conf.in_export_id

            task.label(f"{EXPORT_PATH.name}", important=True)

            EXPORT_AREA_OPTION = (
                f"--export-id={IN_EXPORT_ID}" if IN_EXPORT_ID else "--export-area-page"
            )

            task.label("[")

            # We can't generate our temp file in default temp dir since sandboxed
            # program (like inkscape have their own /tmp invisible from other processes)
            with tempfile.TemporaryDirectory(dir=Path.cwd()) as TMP_DIR:
                TMP_PNG_FILE = Path(TMP_DIR) / EXPORT_PATH.name

                if get_inkscape_version(self.inkscape) < Version("1.0"):
                    # Very useful doc about exporting png with inkscape < 1.0:
                    # http://tavmjong.free.fr/INKSCAPE/MANUAL/html/CommandLine-Export.html
                    self.inkscape(
                        EXPORT_AREA_OPTION,
                        without_gui=True,
                        export_png=str(TMP_PNG_FILE.resolve()),
                        export_width=size.x,
                        export_height=size.y,
                        export_background_opacity=self.conf.bg_opacity,
                        export_background=BG_COLOR,
                        file=str(self.conf.in_svg.resolve()),
                    )
                else:
                    self.inkscape(
                        str(self.conf.in_svg.resolve()),
                        str(EXPORT_AREA_OPTION),
                        export_filename=str(TMP_PNG_FILE.resolve()),
                        export_width=size.x,
                        export_height=size.y,
                        export_background_opacity=self.conf.bg_opacity,
                        export_background=BG_COLOR,
                    )
                task.label("PNG Generated")

                # PNG optimisation
                # See: https://www.santerref.com/blogue/2015/08/20/optimiser-les-images-avec-optipng-et-jpegoptim/ # noqa: E501
                self.optipng(
                    str(TMP_PNG_FILE.resolve()),
                    clobber=True,
                    o5=True,
                    strip="all",
                    out=str(EXPORT_PATH.resolve()),
                )
                task.label("➡ Optimised")

                # Remove any backup that optipng could have generated
                BACKUP = EXPORT_PATH.with_suffix(".png.bak")
                if BACKUP.exists():
                    BACKUP.unlink()
                    task.label("➡ Backup removed")
            task.label("]")

    def export(self) -> None:
        self.reporter.header(self.conf.title)

        with self.reporter.simpletask(title="Input SVG") as task:
            task.label(f"{self.conf.in_svg.resolve()}")
            task.check(self.conf.in_svg.is_file())

        if self.conf.fake_sizes != self.conf.sizes:
            self.reporter.warn(
                "Files purposely generated with fake size in their names"
            )

        for size, fake_size in zip(self.conf.sizes, self.conf.fake_sizes):
            self._export_one(size, fake_size)

        self.reporter.split()


@dataclass
class Svg2SvgExporter(Exporter):
    conf: Svg2Svg
    conf_dir: Path
    inkscape: sh.Command
    reporter: Reporter

    def __post_init__(self) -> None:
        """Convert pathes to actual Path objects relative to the config dir."""
        self.conf.in_svg = self.conf_dir / self.conf.in_svg
        self.conf.out_svg = self.conf_dir / self.conf.out_svg

    def export(self) -> None:
        """Export a svg file to web optimized svg using inskape."""
        self.reporter.header(self.conf.title)

        with self.reporter.simpletask(title="➡ Input SVG") as task:
            task.label(f"{self.conf.in_svg.resolve()!s}")
            task.check(self.conf.in_svg.is_file())

        with self.reporter.simpletask(title="➡ Generating") as task:
            task.label(f"{self.conf.out_svg.name}", important=True)

            if get_inkscape_version(self.inkscape) < Version("1.0"):
                # Very useful doc about exporting png with inkscape:
                # http://tavmjong.free.fr/INKSCAPE/MANUAL/html/CommandLine-Export.html
                self.inkscape(
                    without_gui=True,
                    export_area_page=True,
                    export_plain_svg=str(self.conf.out_svg.resolve()),
                    file=str(self.conf.in_svg.resolve()),
                )
            else:
                self.inkscape(
                    str(self.conf.in_svg.resolve()),
                    export_area_page=True,
                    export_filename=str(self.conf.out_svg.resolve()),
                    export_plain_svg=True,
                )

        with self.reporter.simpletask(title="➡ Fix SVG ViewBox") as task:
            with self.conf.out_svg.open("r", encoding="utf8") as fi:
                soup = BeautifulSoup(fi, "xml")

            assert soup.svg is not None
            view_box = soup.svg["viewBox"]

            task.label("Before:")
            task.label(f"{view_box}", important=True)

            view_box = "0 0 16 16"

            task.label("After:")
            task.label(f"{view_box}", important=True)

            with self.conf.out_svg.open("w", encoding="utf8") as fo:
                fo.write(soup.prettify())

        self.reporter.split()


@dataclass
class Svg2IcoExporter(Exporter):
    conf: Svg2Ico
    conf_dir: Path
    inkscape: sh.Command
    convert: sh.Command
    reporter: Reporter

    def __post_init__(self) -> None:
        """Convert pathes to actual Path objects relative to the config dir."""
        self.conf.in_svg = self.conf_dir / self.conf.in_svg
        self.conf.out_ico = self.conf_dir / self.conf.out_ico

    def export(self) -> None:
        """Export a svg file to web optimized svg using inskape."""
        OPACITY = 0.0
        self.reporter.header(self.conf.title)

        with self.reporter.simpletask(title="➡ Input SVG") as task:
            task.label(f"{self.conf.in_svg.resolve()!s}")
            task.check(self.conf.in_svg.is_file())

        ALL_TMP_PNG_FILES = []
        with tempfile.TemporaryDirectory(dir=Path.cwd()) as TMP_DIR:
            with self.reporter.simpletask(title="➡ Generating temporary PNG") as task:
                # We can't generate our temp file in default temp dir since sandboxed
                # program (like inkscape have their own /tmp invisible from other
                # processes)

                for size in self.conf.sizes:
                    TMP_PNG_FILE = Path(TMP_DIR) / f"favicon-{size.x}x{size.y}.png"
                    ALL_TMP_PNG_FILES.append(TMP_PNG_FILE)

                    if get_inkscape_version(self.inkscape) < Version("1.0"):
                        # Very useful doc about exporting png with inkscape:
                        # http://tavmjong.free.fr/INKSCAPE/MANUAL/html/CommandLine-Export.html
                        self.inkscape(
                            without_gui=True,
                            export_area_page=True,
                            export_png=str(TMP_PNG_FILE.resolve()),
                            export_width=size.x,
                            export_height=size.y,
                            export_background_opacity=OPACITY,
                            export_background=BG_COLOR,
                            file=str(self.conf.in_svg.resolve()),
                        )
                    else:
                        self.inkscape(
                            str(self.conf.in_svg.resolve()),
                            export_area_page=True,
                            export_filename=str(TMP_PNG_FILE.resolve()),
                            export_width=size.x,
                            export_height=size.y,
                            export_background_opacity=OPACITY,
                            export_background=BG_COLOR,
                        )
                    task.label(f"{size.x}x{size.y}")

            with self.reporter.simpletask(title="➡ Generating multires ICO") as task:
                # How to generate clean ICO file from SVG:
                # https://graphicdesign.stackexchange.com/a/77466
                self.convert(
                    *ALL_TMP_PNG_FILES,
                    str(self.conf.out_ico.resolve()),
                )

                task.check(self.conf.in_svg.is_file())

        self.reporter.split()
