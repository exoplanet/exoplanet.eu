# type: ignore
# Standard imports
from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path

# Local imports
from .reporter import Reporter
from .yamlobj import Svg2Png, Svg2Svg


class Cleaner(ABC):
    @abstractmethod
    def clean(self) -> None:
        pass


@dataclass
class Svg2PngCleaner(Cleaner):
    """
    Examples:
        >>> conf = Svg2Png("test", "in.svg", "out", "prefix", 0.0, ["16x16"])
        >>> repo = Reporter("test", False)
        >>> Svg2PngCleaner(conf=conf, conf_dir=Path("."), reporter=repo)
        Svg2PngCleaner(...)
    """

    conf: Svg2Png
    conf_dir: Path
    reporter: Reporter

    def __post_init__(self) -> None:
        """Convert pathes to actual Path objects relative to the config dir."""
        self.conf.out_dir = self.conf_dir / self.conf.out_dir

    def _clean_one(self, size: int, fake_size: int) -> None:
        """Remove one png file that would have been generated from a svg."""
        with self.reporter.simpletask(title="➡ Deleting") as task:
            OUT_DIR = self.conf.out_dir
            PREFIX = self.conf.prefix
            EXPORT_PATH = OUT_DIR / f"{PREFIX}-{fake_size.x}x{fake_size.y}.png"

            task.label(f"{EXPORT_PATH.resolve()!s}", important=True)

            if EXPORT_PATH.exists():
                EXPORT_PATH.unlink()
                task.label("removed")
            else:
                task.label("was not present")

            task.check(not EXPORT_PATH.exists())

    def clean(self) -> None:
        """Remove all png files that would have been generated from a svg."""
        self.reporter.header(self.conf.title)

        for size, fake_size in zip(self.conf.sizes, self.conf.fake_sizes):
            self._clean_one(size, fake_size)

        self.reporter.split()


@dataclass
class Svg2SvgCleaner(Cleaner):
    """
    Examples:
        >>> conf = Svg2Svg("test", "in.svg", "out.svg")
        >>> repo = Reporter("test", False)
        >>> Svg2SvgCleaner(conf=conf, conf_dir=Path("."), reporter=repo)
        Svg2SvgCleaner(...)
    """

    conf: Svg2Svg
    conf_dir: Path
    reporter: Reporter

    # TODO move this back to Svg2Svg
    def __post_init__(self) -> None:
        """Convert pathes to actual Path objects relative to the config dir."""
        self.conf.out_svg = self.conf_dir / self.conf.out_svg

    def clean(self) -> None:
        """
        Remove an optimized svg file that would have been generated from a source svg.
        """
        self.reporter.header(self.conf.title)

        with self.reporter.simpletask(title="➡ Deleting") as task:
            task.label(f"{self.conf.out_svg.resolve()!s}", important=True)

            if self.conf.out_svg.exists():
                self.conf.out_svg.unlink()
                task.label("removed")
            else:
                task.label("was not present")

            task.check(not self.conf.out_svg.exists())

        self.reporter.split()
