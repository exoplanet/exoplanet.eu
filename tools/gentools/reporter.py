# Standard imports
from contextlib import AbstractContextManager
from dataclasses import dataclass, field
from types import TracebackType

# External imports
import colorama

colorama.init(autoreset=True)
FGREEN = colorama.Fore.GREEN
FRED = colorama.Fore.RED
FCYAN = colorama.Fore.CYAN
FBLACK = colorama.Fore.BLACK
FYELLOW = colorama.Fore.YELLOW
FWHITE = colorama.Fore.WHITE
BYELLOW = colorama.Back.YELLOW
BRIGHT = colorama.Style.BRIGHT
DIM = colorama.Style.DIM
RESET_ALL = colorama.Style.RESET_ALL


class ReporterBreak(Exception):
    pass


class ReporterError(Exception):
    pass


@dataclass
class Reporter(AbstractContextManager):
    title: str
    debug: bool
    success_msg: str = ""
    failure_msg: str = ""
    successes: list[bool] = field(init=False, default_factory=list)

    def header(self, msg: str) -> None:
        print(f"{BYELLOW}{FBLACK}{BRIGHT}=== {msg} ===")

    def warn(self, msg: str) -> None:
        print(f"{FYELLOW}⚠ {msg} ⚠")

    def split(self) -> None:
        print()

    def break_if_failure(self) -> None:
        if not all(self.successes):
            raise ReporterBreak

    def simpletask(
        self,
        title: str,
        success_msg: str = "",
        failure_msg: str = "",
    ) -> "SimpleTask":
        return SimpleTask(
            reporter=self,
            title=title,
            success_msg=success_msg,
            failure_msg=failure_msg,
        )

    def __enter__(self) -> "Reporter":
        print(f"{BRIGHT}{self.title}")
        self.split()
        return self

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None,
    ) -> bool | None:
        if all(self.successes):
            print(f"{BRIGHT}{self.success_msg}")
        else:
            print(f"{BRIGHT}{self.failure_msg}")

        if exc_type and exc_type is ReporterBreak:
            # This is an exception just to stop the report early: do not re-raise
            return True

        return None


@dataclass
class SimpleTask(AbstractContextManager):
    reporter: Reporter
    title: str
    success_msg: str = ""
    failure_msg: str = ""
    success: bool | None = field(init=False, default=None)

    def __enter__(self) -> "SimpleTask":
        print(f"{DIM}{self.title}: ", end="")
        return self

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None,
    ) -> bool:
        if exc_type is None:
            if self.success is None:
                # No check and no exception: it's a success
                self.success = True
        else:
            # We have an exception: It's a failure
            self.success = False

        # Print result
        if self.success:
            print(f"{self.success_msg} {FGREEN}✓")
        else:
            print(f"{self.failure_msg} {FRED}✘")

        # Send result to the reporter
        self.reporter.successes.append(self.success)

        if exc_type:
            # end task line with failure mark and print error details
            print(f"{FRED}{exc_type.__name__}: {exc_value}")

        # Any exception is just task failure: no re-raise (except if we want to debug)
        # False --> re-raise   True --> do not re-raise
        return not self.reporter.debug

    def check(self, result: bool) -> None:
        if self.success is not None:
            raise ReporterError("SimpleTask can only check once.")

        self.success = bool(result)

    def label(self, msg: str, important: bool = False) -> None:
        color = FCYAN if important else FWHITE
        print(f"{color}{msg} ", end="")
