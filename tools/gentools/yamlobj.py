# Standard imports
import re
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Generator

# External imports
import yaml


@dataclass
class Size:
    """
    Examples:

        You can create a Size via classic python construction:

        >>> Size(10, 20)
        Size(x=10, y=20)

        You can also create a Size via YAML using explicit tag:

        >>> yaml.full_load("!Size 30x40")
        Size(x=30, y=40)

        And even create a Size via YAML without a tag using implicit formating:

        >>> yaml.full_load("50x60")
        Size(x=50, y=60)

    """

    x: int  # Pixels
    y: int  # Pixels


def size_constructor(loader: Any, node: Any) -> Size:
    scalar = loader.construct_scalar(node)
    x, y = map(int, scalar.split("x"))
    return Size(x, y)


yaml.add_constructor("!Size", size_constructor)
yaml.add_implicit_resolver("!Size", re.compile(r"^\d+x\d+$"))


@dataclass
class Svg2Png:
    """
    Examples:
        Without fake sizes:

        >>> alice = yaml.full_load('''
        ... !Svg2Png
        ... title: Alice
        ... in_svg: exoplanet_eu_favicon.svg
        ... out_dir: ../common/static/generated_img/
        ... prefix: favicon
        ... bg_opacity: 0.0
        ... sizes:
        ... - 16x16
        ... - 32x32
        ... - 48x48
        ... ''')
        >>> alice.title
        'Alice'
        >>> alice.sizes == alice.fake_sizes
        True
        >>> alice.sizes[1]
        Size(x=32, y=32)

        With fake sizes:

        >>> bob = yaml.full_load('''
        ... !Svg2Png
        ... title: Bob
        ... in_svg: exoplanet_eu_favicon.svg
        ... out_dir: ../common/static/generated_img/
        ... prefix: mstile
        ... bg_opacity: 0.0
        ... sizes:
        ... - 128x128
        ... - 270x270
        ... - 558x558
        ... fake_sizes:
        ... - 70x70
        ... - 150x150
        ... - 310x310
        ... ''')
        >>> bob.title
        'Bob'
        >>> bob.sizes[1]
        Size(x=270, y=270)
        >>> bob.fake_sizes[1]
        Size(x=150, y=150)
    """

    title: str
    in_svg: Path
    out_dir: Path
    prefix: str
    bg_opacity: float
    sizes: list[Size]
    fake_sizes: list[Size] | None = None
    in_export_id: str | None = None

    def __post_init__(self) -> None:
        if self.fake_sizes is None:
            self.fake_sizes = self.sizes

        # Invariant
        if len(self.fake_sizes) != len(self.sizes):
            raise ValueError(
                f"<{self.title}>"
                "'fake_sizes' and 'sizes' must have the same size if provided."
            )


def svg2png_constructor(
    loader: Any, node: Any
) -> Svg2Png | Generator[Svg2Png, None, None]:
    """
    The correct way to construct a nested YAML object is explained here:
    https://stackoverflow.com/a/35476888
    """
    result = Svg2Png.__new__(Svg2Png)
    yield result
    mapping = loader.construct_mapping(node, deep=True)
    # HACK we need to do this horrible call to __init__() since thew dev of pywaml
    # said so...
    result.__init__(**mapping)  # type: ignore[misc]
    return result


yaml.add_constructor("!Svg2Png", svg2png_constructor)


@dataclass
class Svg2Svg(yaml.YAMLObject):
    """
    Examples:
        Without fake sizes:

        >>> yaml.full_load('''
        ... !Svg2Svg
        ... title: Safari Pinned Tab Icon
        ... in_svg: exoplanet_eu_favicon_mono.svg
        ... out_svg: ../common/static/generated_img/favicon_safari_pinned_tab.svg
        ... ''')
        Svg2Svg(...)
    """

    yaml_tag = "!Svg2Svg"

    title: str
    in_svg: Path
    out_svg: Path


@dataclass
class Svg2Ico(yaml.YAMLObject):
    """
    Examples:
        Without fake sizes:

        >>> john = yaml.full_load('''
        ... !Svg2Ico
        ... title: John
        ... in_svg: in.svg
        ... out_ico: out.ico
        ... sizes:
        ... - 16x16
        ... - 32x32
        ... - 48x48
        ... ''')
        >>> john.title
        'John'
        >>> john.sizes[1]
        Size(x=32, y=32)
    """

    yaml_tag = "!Svg2Ico"

    title: str
    in_svg: Path
    out_ico: Path
    sizes: list[Size]
