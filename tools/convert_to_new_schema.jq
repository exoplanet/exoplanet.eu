map(
  if .model == "app.link" then
    .model = "editorial.link"
  elif .model == "app.meeting" then
    .model = "editorial.meeting"
  elif .model == "app.news" then
    .model = "editorial.news"
  elif .model == "app.research" then
    .model = "editorial.research"
  elif .model == "app.abstractbase" then
    .model = "core.abstractbase"
  elif .model == "app.atmosphere" then
    .model = "core.atmosphere"
  elif .model == "app.planet" then
    .model = "core.planet"
  elif .model == "app.planetview" then
    .model = "core.planetview"
  elif .model == "app.planetpublication" then
    .model = "core.planet2publication"
  elif .model == "app.starpublication" then
    .model = "core.star2publication"
  elif .model == "app.planetarysystem" then
    .model = "core.planetarysystem"
  elif .model == "app.publication" then
    .model = "core.publication"
  elif .model == "app.star" then
    .model = "core.star"
  elif .model == "app.alternateplanetname" then
    .model = "core.alternateplanetname"
  elif .model == "app.alternatestarname" then
    .model = "core.alternatestarname"
  elif .model == "app.atmospheretemperature" then
    .model = "core.atmospheretemperature"
  elif .model == "app.atmospheremolecule" then
    .model = "core.atmospheremolecule"
  elif .model == "app.molecule" then
    .model = "core.molecule"
  elif .model == "app.flatpage_update" then
    # Usage of flatpage_update is deprecated
    del(.)
  else
    .
  end
)
