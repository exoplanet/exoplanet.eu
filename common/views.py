# Standard imports
from http import HTTPStatus
from typing import Any, Callable, NamedTuple, cast

# Django 💩 imports
from django.conf import settings
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.flatpages.models import FlatPage
from django.http import HttpRequest, HttpResponse
from django.http.response import HttpResponseBase
from django.shortcuts import get_object_or_404, render
from django.views.generic.base import TemplateView

# First party imports
from common.settings import ConfigurationChoice
from core.models import Planet, PlanetStatus, WebStatus
from version import __version__


class _MenuItem(NamedTuple):
    """
    A simple utility class to represent a menu item

    Attributes:
        name: english label of the menu item
        url: url name where the menu item should ppoint to (this url name must
            appear in the urls.py file)
    """

    name: str
    url: str


class _LangMenuItem(NamedTuple):
    """
    A simple utility class to represent a language menu item.

    Attributes:
        name: english name of the language
        country_code: country code as used by the flag-icon-css library

    See:
        https://github.com/lipis/flag-icon-css
    """

    name: str
    country_code: str


class _Sponsor(NamedTuple):
    """
    A simple utility class to represent a sponsor to display un the page footer.

    Attributes:
        name: name of the sponsor
        website: website url of the sponsor
        logo: django static url of the logo image for this sponsor
    """

    name: str
    website: str
    logo: str


# TODO use a classical View in place of TemplateView
class NewDesignBase:
    """
    Base class for all the pages of the new design that need to inherit from a specific
    view (ListView...).

    For a directly usable View class use `NewDesignBaseTemplateView`.

    It handles all the header, footer and common info for all the pages :
    - menus
    - site map
    - language
    - social media integration
    - etc

    Attributes:
        site_version: Version of the site
        site_url: Base url of the site
        site_name: Name of the site
        site_menu_left: items to display in the menu at the left of the main logo
        site_menu_right: items to display in the menu at the right of the main logo
        site_lang_menu: items to display in the language menu
        sponsors_first_line: sponsors logo to display in the first line of the footer
        sponsors_second_line: sponsors logo to display in the second line of the footer
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/

    # We DO NOT define template_name here it has to be defined in the child classes

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the tamplate through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    is_beta = False
    is_dev = True if settings.IN_WHICH_CONFIG == ConfigurationChoice.DEV else False
    is_local = True if settings.IN_WHICH_CONFIG == ConfigurationChoice.LOCAL else False

    site_version = __version__
    # BUG this should be extracted from settings
    site_url = "http://exoplanet.eu/"
    site_name = "Exoplanet.eu"

    # Let's build the menu using _MenuItem
    # if one of the url does not exists, the corresponding item will be
    # disabled.
    site_menu_left = [
        _MenuItem(name="Home", url="legacy_home"),
        _MenuItem(name="Catalogue", url="legacy_catalog"),
        _MenuItem(name="Plots", url="legacy_plots"),
        _MenuItem(name="Tools", url="tools"),
    ]
    site_menu_right = [
        _MenuItem(name="News", url="legacy_news"),
        _MenuItem(name="Bibliography", url="legacy_bibliography"),
        _MenuItem(name="Meetings", url="legacy_meetings"),
        _MenuItem(name="Links", url="legacy_links"),
    ]

    # Let's build the language menu using _LangMenuItem
    site_lang_menu = [
        _LangMenuItem(name="english", country_code="gb"),
        _LangMenuItem(name="french", country_code="fr"),
        _LangMenuItem(name="spanish", country_code="es"),
        _LangMenuItem(name="portuguese", country_code="pt"),
        _LangMenuItem(name="german", country_code="de"),
        _LangMenuItem(name="polish", country_code="pl"),
        _LangMenuItem(name="italian", country_code="it"),
        _LangMenuItem(name="farsi", country_code="ir"),
        _LangMenuItem(name="arabic", country_code="sa"),
        _LangMenuItem(name="russian", country_code="ru"),
        _LangMenuItem(name="japanese", country_code="jp"),
    ]

    enable_site_lang = [
        _LangMenuItem(name="english", country_code="gb"),
    ]

    default_site_lang = _LangMenuItem(name="english", country_code="gb")

    sponsors_first_line = [
        _Sponsor(
            name="CNRS",
            website="https://www.cnrs.fr/",
            logo="common/img/logo_cnrs_web.svg",
        ),
        _Sponsor(
            name="Observatoire de Paris",
            website="https://www.obspm.fr/",
            logo="common/img/logo_observatoire_de_paris_web_light.svg",
        ),
        _Sponsor(
            name="PADC",
            website="https://padc.obspm.fr/",
            logo="common/img/logo_PADC_web.svg",
        ),
    ]
    sponsors_second_line = [
        _Sponsor(
            name="IVOA",
            website="https://www.ivoa.net/",
            logo="common/img/logo_IVOA_1500.png",
        ),
        _Sponsor(
            name="Europlanet Society",
            website="https://www.europlanet-society.org/",
            logo="common/img/logo_europlanet_web.svg",
        ),
        _Sponsor(
            name="Agence Nationale de la Recherche",
            website="https://anr.fr",
            logo="common/img/logo_anr_web.svg",
        ),
        _Sponsor(
            name="European Union",
            website="https://european-union.europa.eu/index_en",
            logo="common/img/flag_eu.svg",
        ),
    ]

    # TODO test this
    def confirmed_planet_nb(self) -> int:
        return cast(
            int,
            Planet.objects.filter(planet_status=PlanetStatus.CONFIRMED)
            .filter(status=WebStatus.ACTIVE)
            .count(),
        )


class NewDesignBaseTemplateView(NewDesignBase, TemplateView):
    """
    Base class for all the pages of the new design. Directly usable as a view class.

    Attributes:
        is_beta: Use to define if the view is used for a beta feature. Default: False.
    """

    not_implemented_template = "not_implemeted_yet.html"


class CustomFlatPage(NewDesignBaseTemplateView):
    """
    Display a flatpage with all the needed context to display the full new design.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "flatpage.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Should be set in get"
    description = ""

    # pylint: disable=arguments-differ
    def get(
        self,
        request: HttpRequest,
        url: str,
    ) -> HttpResponse:
        """
        Support all the flatpage URLs

        Args:
            request: http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.
            url: the url of the requested flatpage

        Returns:
            page showing the flatpage in the clean new design with menu, header, footer,
            etc.

        Raises:
            Http404: if url match no existing flatpage in the database
        """
        # Retreive the flatpage object
        # We nned to add the / prefix since it is not provided by django
        flatpage_obj = get_object_or_404(FlatPage, url=f"/{url}")

        # Set the title
        self.title = flatpage_obj.title

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                flatpage=flatpage_obj,
            ),
        )


class LogIn(LoginView, NewDesignBaseTemplateView):
    """
    Logging page view.

    Attributes:
        template_name: Template name.
        title: Title of the page.
        description: Description of the view.

    """

    template_name = "login.html"
    title = "Login"
    description = "login page"


class LogOut(LogoutView, NewDesignBaseTemplateView):
    """
    Logout page view.

    Attributes:
        template_name: Template name.
        title: Title of the page.
        description: Description of the view.

    """

    # template_name = "registration/logout.html"
    title = "Logout"
    description = "logout page"


class NotFoundErrorView(NewDesignBaseTemplateView):
    """
    Custom error page for 404 Not found error.
    """

    template_name = "404.html"
    title = "Error 404"
    description = "Page not found"

    def get(
        self,
        request: HttpRequest,
        exception: Exception,
    ) -> HttpResponse:
        """
        Handler for the 404 error page.

        Args:
            request: Http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.
            exception: The exception that triggered the page not found error.

        Returns:
            Custom error page.
        """
        request_path = request.path
        request_url = f"{request.scheme}://{request.META['REMOTE_ADDR']}{request_path}"

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                request=request,
                request_path=request_path,
                request_url=request_url,
            ),
            status=HTTPStatus.NOT_FOUND,
        )


class InternalErrorView(NewDesignBaseTemplateView):
    """Custom error page for 500 Not found error."""

    template_name = "500.html"
    "Template name."

    title = "Error 500"
    "Title of the page."

    description = "Internal server error"
    "Description of the page."

    def get(
        self,
        request: HttpRequest,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Handler for the 500 error page.

        Args:
            request: Http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.

        Returns:
            Custom error page.
        """
        request_path = request.path
        request_url = f"{request.scheme}://{request.META['REMOTE_ADDR']}{request_path}"

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                request=request,
                request_path=request_path,
                request_url=request_url,
            ),
            status=HTTPStatus.INTERNAL_SERVER_ERROR,
        )

    # BUG: we can not use this because of a nefarious bug in django 3 that has only
    # been fixed in django 4+
    # The very weird fix comes from: https://stackoverflow.com/a/27120035/20450444
    @classmethod
    def as_view_fixed(cls) -> Callable:
        as_view_fn = cls.as_view()

        def view_fn(request: HttpRequest) -> HttpResponseBase:
            response = as_view_fn(request)

            return response

        return view_fn
