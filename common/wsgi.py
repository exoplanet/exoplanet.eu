"""
WSGI config for exoplanet project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

# Standard imports
import os
import site
from pathlib import Path

# External imports
import dotenv

PROJECT_PATH = Path(__file__).parent.parent
site.addsitedir(str(PROJECT_PATH))


# This allow us to load the DJANGO_CONFIGURATION variable from the .env file to select
# a specific configuration **before** loading the settings.py
dotenv.load_dotenv(PROJECT_PATH / ".env")

# Location of the settings module of our Django application
# https://docs.djangoproject.com/fr/4.1/howto/deployment/wsgi/#configuring-the-settings-module # noqa: E501
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "common.settings")

# Configuration to use for Django
# if no --configuration=??? or `export DJANGO_CONFIGURATION=???` is provided on the
# command line this is the default conf
os.environ.setdefault("DJANGO_CONFIGURATION", "Prod")

# External imports
# This is a django-configuration compatible version of this line:
#   from django.core.wsgi import get_wsgi_application
from configurations.wsgi import get_wsgi_application  # noqa: E402 C0413

application = get_wsgi_application()
