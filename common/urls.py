"""exoplanet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Standard imports
from typing import Any

# Django 💩 imports
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.http import Http404, HttpResponse
from django.urls import include, path, re_path
from django.views.generic.base import RedirectView

# Local imports
from . import views
from .settings import ConfigurationChoice

# HACK we should not have to call this since Django should auto-call it when starting
# See: https://docs.djangoproject.com/en/3.2/ref/contrib/admin/#django.contrib.admin.autodiscover # noqa: E501
admin.autodiscover()


handler404 = views.NotFoundErrorView.as_view()
handler500 = views.InternalErrorView.as_view_fixed()


def _raise404(*args: Any, **kwargs: dict[str, Any]) -> HttpResponse:
    """Return a 404 error page."""
    raise Http404


urlpatterns = [
    # WARNING this only work to serve static files in DEBUG mode
    *staticfiles_urlpatterns(),
    # TODO put this in a dedicated urls file
    # All the url path to have the favicon related files appear at site root
    # They all need to be static (so served under the STATIC_ROOT url) and also need to
    # be directly available at the site root. So we need very specifics redirections to
    # prevent all static ressources to be accessible from the site root.
    path(
        route="favicon-<int:sizex>x<int:sizey>.png",
        view=RedirectView.as_view(
            url="static/common/generated_img/favicon-%(sizex)sx%(sizey)s.png",
            permanent=True,
        ),
        name="favicon_base",
    ),
    path(
        route="apple-touch-icon-<int:sizex>x<int:sizey>.png",
        view=RedirectView.as_view(
            url="static/common/generated_img/apple-touch-icon-%(sizex)sx%(sizey)s.png",
            permanent=True,
        ),
        name="favicon_apple",
    ),
    path(
        route="mstile-<int:sizex>x<int:sizey>.png",
        view=RedirectView.as_view(
            url="static/common/generated_img/mstile-%(sizex)sx%(sizey)s.png",
            permanent=True,
        ),
        name="favicon_windows8",
    ),
    path(
        route="android-chrome-<int:sizex>x<int:sizey>.png",
        view=RedirectView.as_view(
            url="static/common/generated_img/android-chrome-%(sizex)sx%(sizey)s.png",
            permanent=True,
        ),
        name="favicon_android_chrome_opera",
    ),
    path(
        route="favicon_safari_pinned_tab.svg",
        view=RedirectView.as_view(
            url="static/common/generated_img/favicon_safari_pinned_tab.svg",
            permanent=True,
        ),
        name="favicon_file_for_safari_pinned_tab",
    ),
    path(
        route="browserconfiguration.xml",
        view=RedirectView.as_view(
            url="static/common/conf/browserconfiguration.xml", permanent=True
        ),
        name="favicon_file_for_windows81",
    ),
    path(
        route="site.webmanifest",
        view=RedirectView.as_view(
            url="static/common/conf/site.webmanifest", permanent=True
        ),
        name="favicon_file_for_android",
    ),
    path(
        route="favicon.ico",
        view=RedirectView.as_view(
            url="static/common/generated_img/favicon.ico",
            permanent=True,
        ),
        name="favicon_old_school",
    ),
    # Actual urls not favicon related
    path(route="disk/", view=include("disk.urls")),
    path(route="canary/", view=include("canary.urls")),
    path(route="amdf/", view=include("amd.urls")),
    path(route="catalog_new/", view=include("catalog.urls")),
    # TODO we should add a prefix for the legacy catalog maybe with some redirections
    # TODO write a real implementation for this
    path(route="ephemeris/", view=include("ephemeris.urls")),
    path(route="", view=include("catalog_legacy.urls")),
    # TinyMCE plugin/widget urls
    # See: https://github.com/romanvm/django-tinymce4-lite#quick-start
    path("tinymce/", include("tinymce.urls")),
    re_path(r"^filer/", include("filer.urls")),
    # Admin urls should be routed here
    path(route="exoplanet_admin/", view=admin.site.urls),
    # Accounts urls
    path(route="accounts/login/", view=views.LogIn.as_view(), name="login"),
    path(route="accounts/logout/", view=views.LogOut.as_view(), name="logout"),
    # Flatpages with fixed urls (in order to be able to use the url tag in templates)
    path("readme/", views.CustomFlatPage.as_view(), {"url": "readme/"}, name="readme"),
    path("team/", views.CustomFlatPage.as_view(), {"url": "team/"}, name="team"),
    # TODO: remove when the page has been deleted
    path(
        "tutorials/",
        _raise404,
        {"url": "tutorials/"},
        name="tutorials",
    ),
    path(
        "planets_binary/",
        views.CustomFlatPage.as_view(),
        {"url": "planets_binary/"},
        name="planets_binary",
    ),
    # TODO: remove when the page has been deleted
    path(
        "theory/",
        _raise404,
        {"url": "theory/"},
        name="theory",
    ),
    path(
        "query_language/",
        views.CustomFlatPage.as_view(),
        {"url": "query_language/"},
        name="query_language",
    ),
    path(
        "observability_predictor/",
        views.CustomFlatPage.as_view(),
        {"url": "observability_predictor/"},
        name="observability_predictor",
    ),
    # HACK: temporary redirection since we do not have a real FAQ page at the moment
    path("faq/", views.CustomFlatPage.as_view(), {"url": "readme/"}, name="faq"),
    #
]

# Pages only available for tests
if settings.IN_WHICH_CONFIG in [ConfigurationChoice.TESTS, ConfigurationChoice.LOCAL]:
    urlpatterns += [path(route="for_tests/", view=include("for_tests.urls"))]

# To serve media for django-filer
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# WARNING: This must stay as the LAST PATTERN whatever happens since it is a catch all
# Catchall pattern for all other flatpages
urlpatterns += [re_path(r"^(?P<url>.*/)$", views.CustomFlatPage.as_view())]
