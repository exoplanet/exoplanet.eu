#!/usr/bin/env bash

# Get all the user defined django app as a shell array
mapfile -d " " -t apps < <(python manage.py shell --configuration=Local -c 'from django.apps import apps;ap=[a.label for a in apps.get_app_configs() if not a.name.startswith("django")];print(" ".join(ap), end="")')

# Check if any of the app needs a migration
python manage.py makemigrations "${apps[@]}" --check --dry-run --configuration=Local

# If this test fails it mean some migrations need to be generated via (for the core app) :
#
# python manage.py makemigrations core --check --configuration=Local
#
# Then rename the generated file to have a meaningful name
# Then apply `black` on it to normalize the generated code
# See: https://docs.djangoproject.com/en/4.1/topics/migrations/
#
# And remember to check if the migration haved been applied to each of your databases
# (here for the core app with the local db config) :
#
# python manage.py showmigrations core --configuration=Local
