/**
 * Get the label for a parameter.
 * We get label in this order:
 * short => long => full_ascii => parameter's name.
 * @param {String} labels Object with all label.
 * @param {String} paramName Name of the parameter.
 * @returns {String} The correct label.
 */
function getLabel(labels, paramName) {
	if (labels?.short && labels.short) return labels.short
	if (labels?.long && labels.long) return labels.long
	if (labels?.full_ascii && labels.full_ascii) return labels.full_ascii
	return paramName
}

function getUnit(unit) {
	if (Array.isArray(unit)) {
		if (Array.isArray(unit[0].symbol)) {
			return unit[0].symbol[0]
		}
		return unit[0].symbol
	}

	if (Array.isArray(unit.symbol)) {
		return unit.symbol[0]
	}

	return unit.symbol
}

/**
 * Get a label with his prefix if exist.
 * @param {String} prefixLabel The prefix label.
 * @param {String} label The label.
 * @returns The new label composed of the prefix if exist and the label.
 */
function labelWithPrefix(prefixLabel, label) {
	if (prefixLabel) {
		return `${prefixLabel}.${label}`
	}
	return label
}

/**
 * Flate an object.
 * @param {Object} obj Object to flat.
 * @param {String} prefixLabel Prefixx label to add of all nesting label.
 * @returns The given object but flatten.
 */
function flattenObject(obj, prefixLabel) {
	const result = {}
	for (const element of Object.keys(obj)) {
		if (obj[element]?.columns) {
			result[element] = { label: labelWithPrefix(prefixLabel, obj[element].label), dataName: obj[element].dataName }
			if (obj[element]?.unit && obj[element].unit) {
				result[element].unit = obj[element].unit
			}
			const flattenRecursObj = flattenObject(obj[element].columns, labelWithPrefix(prefixLabel, obj[element].label))
			for (const subElement of Object.keys(flattenRecursObj)) {
				result[`${element}_${subElement}`] = flattenRecursObj[subElement]
			}
		} else {
			result[element] = { label: labelWithPrefix(prefixLabel, obj[element].label), dataName: obj[element].dataName }
			if (obj[element]?.unit && obj[element].unit) {
				result[element].unit = obj[element].unit
			}
		}
	}
	return result
}

/**
 * Flatten an exodict but keep the first level of nesting.
 * @param {Object.<Object>} exodict Object to flat.
 * @returns The given exodict but flatten.
 */
function flattenExodict(exodict) {
	for (const element of Object.keys(exodict)) {
		if (exodict[element]?.columns) {
			exodict[element].columns = flattenObject(exodict[element].columns, "")
		}
	}
	return exodict
}

/**
 * Initialize data for exodict filtering.
 * @param {Object.<Object>} rawData Raw data who contains the exodict from the database.
 * @param {Array.<String>} catalogHeaders All catalog headers.
 * @returns {Object.<Object>} The filtered exodict.
 */
function initFilterExodict(rawData, catalogHeaders) {
	const result = {}

	for (const element of Object.keys(rawData)) {
		if (catalogHeaders.includes(element)) {
			result[element] = filterExodict(rawData[element], element, catalogHeaders)
		}
	}
	return result
}

/**
 * Recursively filter exodict.
 * @param {Object} rawExodict Raw exodcit from the database.
 * @param {String} rawExodictName Name of the given exodict.
 * @param {Array.<String>} catalogHeaders All catalog headers.
 * @returns {Object} Part or Full filtered exodict.
 */
function filterExodict(rawExodict, rawExodictName, catalogHeaders) {
	if (rawExodict?.label && catalogHeaders.includes(rawExodictName)) {
		const result = { label: getLabel(rawExodict.label, rawExodictName), dataName: rawExodictName }
		if (rawExodict?.unit) {
			result.unit = getUnit(rawExodict.unit)
		}

		const columns = {}
		for (const element of Object.keys(rawExodict)) {
			const col = filterExodict(rawExodict[element], element, catalogHeaders)
			if (col) {
				columns[element] = col
			}
		}

		if (Object.keys(columns).length > 0) {
			result.columns = columns
		}
		return result
	}
}

/**
 * Get URI of disk from his id.
 * @param {Array.<Object>} disks All disks of the database.
 * @param {Number} id Id of the disk of which we want the slug.
 * @returns {String} The URI of the disk with the given id.
 */
function getUri(disks, id) {
	for (const disk of disks) {
		if (id === disk.id) {
			return disk.uri
		}
	}
}

/**
 * Extend an array with an other.
 * @param {Array} array1 First array to extend.
 * @param {Array} array2 Second array to add to the first.
 * @returns {Array} The first array extend with the second.
 */
function extendArrays(array1, array2) {
	for (const value of array2) {
		array1.push(value)
	}
	return array1
}

/**
 * Build one column object.
 * @param {Object.<String>} data Data of a column.
 * @returns {Object.<String>} The object representing the column of the given data.
 */
function buildOneColumn(disks, data, prefix) {
	if (data.label.toLowerCase() === "name") {
		return {
			title: "Name",
			data: null,
			render: (data, type, row) => {
				return `<a href="${getUri(disks, data.id)}">${data.name}</a>`
			},
		}
	}

	let title = data.label
	if (data?.unit) {
		title = `${data.label}<br>(${data.unit})`
	}

	let finalDataName = `${prefix}.${data.dataName}`
	if (data.dataName === "identify") {
		finalDataName = `data.${data.dataName}`
	} else if (prefix.includes("identify")) {
		finalDataName = `data.identify.${data.dataName}`
	}

	return {
		title,
		data: finalDataName,
		defaultContent: '<span class="no-data">No data</span>',
	}
}

/**
 * Build columns objects for the final table.
 * @param {Object.<Object>} filteredExodict Filtered Exodict to build build columns.
 * @param {Object.<Object>} prefix prefix to add to the dataName.
 * @returns {Array.<Object>} All columns for the final table.
 */
function buildColumns(disks, filteredExodict, prefix) {
	const result = []

	for (const value of Object.values(filteredExodict)) {
		const dataColumn = { label: value.label, dataName: value.dataName }
		if (value?.unit) {
			dataColumn.unit = value.unit
		}
		const masterCol = buildOneColumn(disks, dataColumn, prefix)
		let cols = []
		if (value?.columns) {
			masterCol.nbCols = Object.keys(value.columns).length
			cols = buildColumns(disks, value.columns, `${prefix}.${value.dataName}`)
		}
		result.push(masterCol)
		if (cols) {
			extendArrays(result, cols)
		}
	}

	return result
}

/**
 *
 * @param {Array.<Object>} columnsAndColSpan All columns, with AND without colspan.
 * @param {Array.<String>} sortingOrder Sorting order of headers.
 * @returns The columns and colspan columns in the good order.
 */
function sortColumnsAndColSpan(columnsAndColSpan, sortingOrder) {
	const result = []

	// # TODO: maybe find a better sort algo
	for (const order of sortingOrder) {
		for (const col of columnsAndColSpan) {
			if (col?.nbCols && col.title === order) {
				result.push(col)
				const start = columnsAndColSpan.indexOf(col) + 1
				const end = start + col.nbCols
				for (let i = start; i < end; i++) {
					result.push(columnsAndColSpan[i])
				}
			}
		}
	}

	return result
}

/**
 * Get columns with colspan informartion from all columns.
 * @param {Array.<Object>} columnsAndColSpan All columns, with AND without colspan.
 * @returns {Array.<Object>} All columns with colspan information.
 */
function getColSpan(columnsAndColSpan) {
	const result = []
	for (const column of columnsAndColSpan) {
		if (column?.nbCols) {
			result.push(column)
		}
	}
	return result
}

/**
 * Get columns without colspan informartion from all columns.
 * @param {Array.<Object>} columnsAndColSpan All columns, with AND without colspan.
 * @returns {Array.<Object>} All columns without colspan information.
 */
function getColumns(columnsAndColSpan) {
	const result = []
	for (const column of columnsAndColSpan) {
		if (!column?.nbCols) {
			result.push(column)
		}
	}
	return result
}

/**
 * Get the html table DOM for columns without colspan.
 * @param {Number} columnsNumber Number of the columns without colspan.
 * @returns {String} Html tags to create all columns without colspan header.
 */
function domHtmlTableColumns(columnsNumber) {
	const th = '<th class="final_headers"></th>'.repeat(columnsNumber)
	return `<tr>${th}</tr>`
}

/**
 * Get the html table DOM for columns with colspan.
 * @param {Array.<Object>} colSpan All columns with colspan.
 * @returns {String} Html tags to create all columns with colspan header.
 */
function domHtmlTableColSpan(colSpan) {
	let colSpanTh = ""
	for (const column of colSpan) {
		colSpanTh += `<th colspan=${column.nbCols}>${column.title}</th>`
	}
	return `<tr>${colSpanTh}</tr>`
}

/**
 * Get the html table DOM.
 * @param {Array.<Object>} colSpan All columns with colspan.
 * @param {Number} columnsNumber Number of the columns without colspan.
 * @returns {String} Html tags for all columns header.
 */
function domHtmlTable(colSpan, columnsNumber) {
	const begin = '<table id="disk-catalog-table"><thead>'
	const colSpanTh = domHtmlTableColSpan(colSpan)
	const columnsTh = domHtmlTableColumns(columnsNumber)
	const end = "</thead></table>"
	return `${begin}${colSpanTh}${columnsTh}${end}`
}

// For tests
if (typeof exports !== "undefined") {
	module.exports = {
		getLabel,
		getUri,
		initFilterExodict,
		extendArrays,
		buildOneColumn,
		buildColumns,
		getColSpan,
		getColumns,
		domHtmlTableColumns,
		domHtmlTableColSpan,
		domHtmlTable,
		flattenObject,
		labelWithPrefix,
		flattenExodict,
		getUnit,
		sortColumnsAndColSpan,
	}
}
