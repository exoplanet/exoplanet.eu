// Need some data parameters to be transmitted when calling the script in the html:
// - data-disks: Data contains disks data info.
// - data-postgrestInfo: Contains host and port for the postgrest server.

/* eslint-env browser */
/* global $, initFilterExodict, buildColumns, extendArrays, domHtmlTable, getColSpan, getColumns, flattenExodict, sortColumnsAndColSpan */

const disks = JSON.parse(document.currentScript.dataset.disks)
const postgrestInfo = JSON.parse(document.currentScript.dataset.postgrestinfo)
const postgrestServerAdress = `http://${postgrestInfo.host}:${postgrestInfo.port}`
/**
 * Add a table element to the html in the div `disk-catalog-div`.
 * @param {Array.<Object>.<String>} colSpan All columns with colspan.
 * @param {Number} columnsNumber Number of columns without colspan (final columns).
 */
function addTableToHtml(colSpan, columnsNumber) {
	$("#disk-catalog-div").append(domHtmlTable(colSpan, columnsNumber))
}

/**
 * Initialize columns for the table.
 * @param {Array.<Object>.<String>} columnsFromData Final columns of the html table.
 */
function initTable(columnsFromData) {
	$(document).ready(() => {
		$("#disk-catalog-table").DataTable({
			ajax: {
				url: `${postgrestServerAdress}/disk`,
				data: { select: "id,data->identify->>name, data" },
				dataSrc: "",
				cache: true,
				method: "GET",
			},
			columns: columnsFromData,
			scrollX: true,
		})
	})
}

/**
 * Callback for the initialisation of data and table.
 * @param {Array.<String>} catalogHeaders All catalog headers.
 * @param {Array.<String>} sortingOrder Sorting order for headers.
 * @param {Object} data Data from ajax request.
 */
function callbackInitDataAndTable(catalogHeaders, sortingOrder, data) {
	const formattedData = data[0].parameters
	formattedData.identify = data[0].identify
	const filteredExodict = initFilterExodict(formattedData, catalogHeaders)
	const flattenFilteredExodict = flattenExodict(filteredExodict)
	const columnsAndColSpan = buildColumns(disks, flattenFilteredExodict, "data.parameters")
	const sortedcolumnsAndColSpan = sortColumnsAndColSpan(columnsAndColSpan, sortingOrder)
	const colSpan = getColSpan(sortedcolumnsAndColSpan)
	const columns = getColumns(sortedcolumnsAndColSpan)
	addTableToHtml(colSpan, columns.length)
	initTable(columns)
}

/**
 * Initialize data and table.
 */
function initDataAndTable() {
	let catalogHeaders = []
	let sortingOrder = []

	$.when(
		$.ajax({
			url: `${postgrestServerAdress}/catalogheaders`,
			data: {
				select:
					"disk_all_field:data->disk->all_field, common_all_field:data->common->all_field, disk_sorting_order:data->disk->sorting_order",
				ref: "eq.true",
				order: "modified.desc",
				limit: 1,
			},
			cache: true,
			method: "GET",
		})
			.done((data) => {
				catalogHeaders = extendArrays(data[0].disk_all_field, data[0].common_all_field)
				sortingOrder = data[0].disk_sorting_order
			})
			.fail((jqXHR, status, statusCode) => {
				$("#disk-catalog-div").append(
					'<div class="alert alert-danger" role="alert"><b>ERROR</b>: The table cannot be loaded. Retry later or contact the web site administrator.</div>',
				)
				console.log(`Ajax query fail. Status: ${status}, code: ${statusCode}.`)
			}),
	).then((data, textStatus, jqXHR) => {
		$.ajax({
			url: `${postgrestServerAdress}/exodict`,
			data: { select: "data->parameters, data->identify", ref: "eq.true", order: "modified.desc", limit: 1 },
			cache: true,
			method: "GET",
		})
			.done((data) => {
				callbackInitDataAndTable(catalogHeaders, sortingOrder, data)
			})
			.fail((jqXHR, status, statusCode) => {
				$("#disk-catalog-div").append(
					'<div class="alert alert-danger" role="alert"><b>ERROR</b>: The table cannot be loaded. Retry later or contact the web site administrator.</div>',
				)
				console.log(`Ajax query fail. Status: ${status}, code: ${statusCode}.`)
			})
	})
}

initDataAndTable()
