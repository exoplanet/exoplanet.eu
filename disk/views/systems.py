"""Individual disk view."""

# Standard imports
from typing import Any, cast

# Django 💩 imports
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

# Local imports
from .utils import (
    REQUEST_TIMEOUT,
    NewDiskDesignBaseTemplateView,
    get_objects_info,
    make_get_request,
    when_grav_sys_request_answers_ok,
    when_request_answers_error,
)


def get_description(disk_name: str, params: dict[str, Any]) -> None:
    # TODO: Replace with system description
    pass


class Systems(LoginRequiredMixin, NewDiskDesignBaseTemplateView):
    """
    Systems page view.

    Attributes:
        template_name: Template name.
        title: Title of the page.
        description: Description of the view.
    """

    template_name = "systems.html"
    title = ""
    description = ""

    def _get_page(self, request: HttpRequest, grav_sys_id: str) -> HttpResponse:
        """
        View for disk individual page.

        Args:
            request: http request for the page.
            grav_sys_id: Id of the gravitaional system.


        Returns:
            The individual page of a disk.
        """
        host = settings.POSTGREST_HOST
        port = settings.POSTGREST_PORT

        grav_sys_info = cast(
            dict[str, Any],
            make_get_request(
                f"http://{host}:{port}/gravitationalsystems",
                {"select": "data,update:modified", "id": f"eq.{grav_sys_id}"},
                REQUEST_TIMEOUT,
                when_grav_sys_request_answers_ok,
                when_request_answers_error,
            ),
        )

        info = get_objects_info(host, port, grav_sys_info["data"])

        self.title = f"{info['main']}'s system"
        self.description = (
            f"{self.title}.<br><strong>Last update:</strong> {grav_sys_info['update']}"
        )

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,  # we need this to have the context of the class view
                info=info,
            ),
        )

    def get(
        self,
        request: HttpRequest,
        grav_sys_id: str,
    ) -> HttpResponse:
        """
        View for system page for GET request.

        Args:
            request: http request for the page.
            grav_sys_id: Id of the gravitaional system.

        Returns:
            The individual page of a disk.
        """
        return self._get_page(request, grav_sys_id)
