"""Individual disk view."""

# Standard imports
from typing import Any, cast

# Django 💩 imports
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, render

# Local imports
from .utils import (
    REQUEST_TIMEOUT,
    NewDiskDesignBaseTemplateView,
    grouped_info,
    make_get_request,
    slugify,
    when_exodict_request_answers_ok,
    when_object_request_answers_ok,
    when_principal_object_request_answers_ok,
    when_request_answers_error,
)


def get_description(disk_name: str, params: dict[str, Any]) -> str:
    """
    Get description for a disk.

    Args:
        disk_name: Name of the disk.
        params: Parameters of the disk.

    Returns:
        The page description for the given disk.
    """
    # TODO: Replace with system description
    nb_params = 0
    for value in params["data"].values():
        nb_params += len(value.keys())
    return (
        f"Disk named {disk_name} who contains {nb_params} parameters.<br>"
        f"<strong>Disk Data last update:</strong> {params['update_disk']}<br>"
        f"<strong>Exodict last update:</strong> {params['update_exodict']}<br>"
    )


class Individual(LoginRequiredMixin, NewDiskDesignBaseTemplateView):
    """
    Disk individual page view.

    Attributes:
        template_name: Template name.
        title: Title of the page.
        description: Description of the view.
    """

    template_name = "individual.html"
    title = ""
    description = ""

    def _get_page(
        self, request: HttpRequest, disk_slug: str, disk_id: str
    ) -> HttpResponse:
        """
        View for disk individual page.

        Args:
            request: http request for the page.
            disk_slug: Slug of the disk.
            disk_id: Id of the disk.

        Returns:
            The individual page of a disk.
        """
        host = settings.POSTGREST_HOST
        port = settings.POSTGREST_PORT

        disk_info = cast(
            dict[str, Any],
            make_get_request(
                f"http://{host}:{port}/disk",
                {"select": "data,update:modified", "id": f"eq.{disk_id}"},
                REQUEST_TIMEOUT,
                when_object_request_answers_ok,
                when_request_answers_error,
            ),
        )
        disk_name = disk_info["data"]["identify"]["name"]

        main_star_id = make_get_request(
            f"http://{host}:{port}/gravitationalsystems",
            {
                "select": "data->principal_obj->id",
                "data->secondary_objs": f'cs.[{{"id":{disk_id},"type":"disk"}}]',
            },
            REQUEST_TIMEOUT,
            when_principal_object_request_answers_ok,
            when_request_answers_error,
        )

        system_id = make_get_request(
            f"http://{host}:{port}/gravitationalsystems",
            {
                "select": "id",
                "data->principal_obj": f'cs.{{"id":{main_star_id}, "type":"star"}}',
            },
            REQUEST_TIMEOUT,
            when_principal_object_request_answers_ok,
            when_request_answers_error,
        )

        star_info = cast(
            dict[str, Any],
            make_get_request(
                f"http://{host}:{port}/star",
                {
                    "select": "data,update:modified",
                    "id": f"eq.{main_star_id}",
                },
                REQUEST_TIMEOUT,
                when_object_request_answers_ok,
                when_request_answers_error,
            ),
        )

        slugify_name = slugify(disk_name)

        if disk_slug != slugify_name:
            return redirect(
                "disk_details",
                disk_slug=slugify_name,
                disk_id=disk_id,
            )

        exodict_info = cast(
            dict[str, Any],
            make_get_request(
                f"http://{host}:{port}/exodict",
                {
                    "select": "data,update:modified",
                    "ref": "eq.true",
                    "order": "modified.desc",
                    "limit": "1",
                },
                REQUEST_TIMEOUT,
                when_exodict_request_answers_ok,
                when_request_answers_error,
            ),
        )

        info = {
            "update_disk": disk_info["update"],
            "update_exodict": exodict_info["update"],
            "update_star": star_info["update"],
            "data": grouped_info(disk_info["data"], exodict_info["data"]),
            "star_data": grouped_info(star_info["data"], exodict_info["data"]),
            "system_id": system_id,
        }

        self.title = disk_name
        self.description = get_description(disk_name, info)

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,  # we need this to have the context of the class view
                info=info,
            ),
        )

    def get(
        self,
        request: HttpRequest,
        disk_slug: str,
        disk_id: str,
    ) -> HttpResponse:
        """
        View for disk individual page for GET request.

        Args:
            request: http request for the page.
            disk_slug: Slug of the disk.
            disk_id: Id of the disk.

        Returns:
            The individual page of a disk.
        """
        return self._get_page(request, disk_slug, disk_id)
