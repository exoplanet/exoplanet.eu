"""Disk catalogue views."""

# Standard imports
import json
from typing import cast

# Django 💩 imports
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

# Local imports
from .utils import (
    REQUEST_TIMEOUT,
    NewDiskDesignBaseTemplateView,
    make_get_request,
    when_catalog_request_answers_ok,
    when_request_answers_error,
)


class DiskCatalog(LoginRequiredMixin, NewDiskDesignBaseTemplateView):
    """
    Disk catalogue view.

    Attributes:
        template_name: Template name.
        title: Title of the page.
        description: Description of the view.
    """

    template_name = "disk_catalog.html"
    title = "Disk Catalogue"
    description = "Sortable and filterable catalogue of the exodisk discovered so far."

    def _get_page(self, request: HttpRequest) -> HttpResponse:
        """
        View for disk catalogue.

        Args:
            request: http request for the page.

        Returns:
            The page with the catalogue of disk.
        """
        host = settings.POSTGREST_HOST
        port = settings.POSTGREST_PORT

        disks = make_get_request(
            f"http://{host}:{port}/disk",
            {"select": "id,data->identify->>name"},
            REQUEST_TIMEOUT,
            when_catalog_request_answers_ok,
            when_request_answers_error,
        )

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,  # we need this to have the context of the class view
                disks=disks,
                num_disks=len(cast(str, disks)),
                postgrest_info=json.dumps({"host": host, "port": port}),
            ),
        )

    def get(self, request: HttpRequest) -> HttpResponse:
        """
        View for disk catalogue view for GET request.

        Args:
            request: http request for the page.

        Returns:
            The page with the catalogue of disk.
        """
        return self._get_page(request)
