"""Disk app's views."""

# Local imports
from .catalog import DiskCatalog  # noqa: F401
from .individual import Individual  # noqa: F401
from .systems import Systems  # noqa: F401
