"""Utils to handle planetary systems."""

# Standard imports
from typing import Any, cast

# Django 💩 imports
from django.db.models.query import QuerySet
from django.urls import reverse

# External imports
from requests import models as rq_models

# First party imports
from core.models import Star, safe_name

# Local imports
from .requests import REQUEST_TIMEOUT, make_get_request
from .utils import slugify, when_request_answers_error


def _get_main_obj_id(data: dict[str, dict[str, Any] | list[dict[str, Any]]]) -> int:
    """
    Get main object id from data of a gravitational system.

    Args:
        data: Data of a gravitational system.

    Returns:
        Id of the main object of a gravitational system.

    Examples:
        >>> _get_main_obj_id({"principal_obj": {"type": "star", "id": 1}})
        1

        >>> try:
        ...     _get_main_obj_id({"principal_obj": {"type": "toto", "id": 1}})
        ... except (NotImplementedError):
        ...     True
        True
    """
    principal_obj = cast(dict[str, Any], data["principal_obj"])
    match principal_obj["type"]:
        case "star":
            return cast(int, principal_obj["id"])
        case _:
            raise NotImplementedError()


def when_grav_sys_request_answers_ok(
    request_answer: rq_models.Response,
) -> dict[str, Any]:
    """
    Treatment when a gravitational system request answers without error.

    Args:
        request_answer: Response of a request.

    Returns:
        Formated data of the json format of the response.
    """
    json_res = request_answer.json()
    match len(json_res):
        case 0:
            raise TypeError("Only size-1 arrays accepted. Json response size: 0.")
        case 1:
            return cast(dict, json_res[0])
        case _:
            raise TypeError(
                f"Only size-1 arrays accepted. Json response size: {len(json_res)}."
            )


def _when_disk_request_answers_ok(
    request_answer: rq_models.Response,
) -> dict[str, Any]:
    """
    Treatment when a disk request answers without error.

    Args:
        request_answer: Response of a request.

    Returns:
        Formated data of the json format of the response.
    """
    json_res = request_answer.json()
    match len(json_res):
        case 0:
            raise TypeError("Only size-1 arrays accepted. Json response size: 0.")
        case 1:
            res = json_res[0].copy()
            res["url"] = reverse(
                "disk_details",
                kwargs=dict(
                    disk_slug=slugify(json_res[0]["name"]),
                    disk_id=str(json_res[0]["id"]),
                ),
            )
            return cast(dict, res)
        case _:
            raise TypeError(
                f"Only size-1 arrays accepted. Json response size: {len(json_res)}."
            )


def _when_star_request_answers_ok(
    request_answer: rq_models.Response,
) -> dict[str, Any]:
    """
    Treatment when a star request answers without error.

    Args:
        request_answer: Response of a request.

    Returns:
        Formated data of the json format of the response.
    """
    json_res = request_answer.json()
    match len(json_res):
        case 0:
            raise TypeError("Only size-1 arrays accepted. Json response size: 0.")
        case 1:
            return cast(dict, json_res[0])
        case _:
            raise TypeError(
                f"Only size-1 arrays accepted. Json response size: {len(json_res)}."
            )


def _get_disk_info(host: str, port: int, disk_id: int) -> dict[str, Any]:
    """
    Get disk informations from a disk id.

    Args:
        host: Host for the request.
        port: Port for the request.
        disk_id: Id of the disk who we when infromations.

    Returns:
        Get disk informations form a request on the id.
    """
    return cast(
        dict[str, Any],
        make_get_request(
            f"http://{host}:{port}/disk",
            {
                "select": "id, name:data->identify->name",
                "id": f"eq.{disk_id}",
            },
            REQUEST_TIMEOUT,
            _when_disk_request_answers_ok,
            when_request_answers_error,
        ),
    )


# TODO: Test this when you rebase with the test changements
def _get_planets_for_relational_star(rel_star_id: int) -> QuerySet[Star]:
    """
    Get planets form a star in relation table.

    Args:
        rel_star_id: Id of the star in relational table.

    Returns:
        All planets of the star with the given id in the relational table.
    """
    return cast(QuerySet[Star], Star.objects.get(id=rel_star_id).planets.all())


# TODO: Test this when you rebase with the test changements
def _get_planets_info(planets: QuerySet) -> list[dict[str, Any]]:
    """
    Get planets informations.

    Args:
        planets: Planets from which we want informations.

    Returns:
        Informations of given planets.
    """
    result = []

    for planet in planets:
        if planet.is_visible():
            result.append(
                {
                    "name": planet.name,
                    "id": planet.id,
                    "url": reverse(
                        "legacy_planet_detail",
                        kwargs=dict(
                            planet_safe_name=safe_name(planet.name),
                            planet_id_str=str(planet.id),
                        ),
                    ),
                }
            )

    return result


# TODO: Test this when you rebase with the test changements
def get_objects_info(
    host: str,
    port: int,
    data: dict[str, dict[str, Any] | list[dict[str, Any]]],
) -> dict[str, str | dict[str, Any]]:
    """
    Get objects informations for the system page.

    Args:
        host: Host for requests.
        port: Port for requests.
        data: Data of the system.

    Returns:
        Information on objects for a system.
    """
    disks = []
    planets = []

    # Get main obj info (star for the moment)
    star_info = cast(
        dict[str, Any],
        make_get_request(
            f"http://{host}:{port}/star",
            {
                "select": "id, name:data->identify->name,data->internal->rel_star_id",
                "id": f"eq.{_get_main_obj_id(data)}",
            },
            REQUEST_TIMEOUT,
            _when_star_request_answers_ok,
            when_request_answers_error,
        ),
    )

    # Get secondary objects info
    secondary_objects: list[dict[str, Any]] = cast(
        list[dict[str, Any]], data["secondary_objs"]
    )
    for obj in secondary_objects:
        match obj["type"]:
            case "disk":
                disks.append(_get_disk_info(host, port, obj["id"]))
            case _:
                raise NotImplementedError()

    # Get planet objects info (in relational table for the moment)
    if star_info["rel_star_id"]:
        planets_from_db = _get_planets_for_relational_star(star_info["rel_star_id"])
        planets = _get_planets_info(planets_from_db)

    childs = {
        "disks": disks if disks else [],
        "planets": planets if planets else [],
    }

    return {"main": star_info["name"], "childs": childs}
