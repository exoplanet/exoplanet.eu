"""Useful function for the individual view."""

# Standard imports
from datetime import datetime as dt
from typing import Any, cast

# External imports
from flatdict import FlatDict
from requests import models as rq_models


def _get_update_date(date: str) -> str:
    """
    Get a date from datetime in format YYYY-mm-dd.

    Args:
        date: Date to format.

    Returns:
        The date at the format: YYYY-mm-dd.

    Examples:
        >>> _get_update_date("2023-05-12 12:01:47")
        '2023-5-12'
    """
    update_as_datetime = dt.fromisoformat(date)
    return (
        f"{update_as_datetime.year}-"
        f"{update_as_datetime.month}-"
        f"{update_as_datetime.day}"
    )


# TODO: use js for nested dict
def _get_disk_info_in_good_format_for_template(
    json_result: dict[str, Any],
) -> dict[str, Any]:
    """
    Get disk information formated for the template.

    Args:
        json_result: json_result from a request on the db.

    Returns:
        Formated data.

    Examples:
        >>> _get_disk_info_in_good_format_for_template(
        ...     {
        ...         "update": "2023-05-12 12:01:47",
        ...         "data": {"titi": {"tutu": 13, "tata": {"toto": 14}}}
        ...     }
        ... )
        {'update': '2023-5-12', 'data': {'titi': {'tutu': 13, 'tata_toto': 14}}}
    """
    update = _get_update_date(json_result["update"])

    data = {
        key: dict(FlatDict(value, delimiter="_"))
        for key, value in json_result["data"].items()
    }

    return {"update": update, "data": data}


def when_principal_object_request_answers_ok(request_answer: rq_models.Response) -> int:
    """
    Treatment when a principal id object request answers without error.

    Args:
        request_answer: Response of a request.

    Returns:
        The id from the json of the response.

    """
    json_res = request_answer.json()
    match len(json_res):
        case 0:
            raise TypeError("Only size-1 arrays accepted. Json response size: 0.")
        case 1:
            return cast(int, json_res[0]["id"])
        case _:
            raise TypeError(
                f"Only size-1 arrays accepted. Json response size: {len(json_res)}."
            )


def when_object_request_answers_ok(
    request_answer: rq_models.Response,
) -> dict[str, Any]:
    """
    Treatment when an object request answers without error.

    Args:
        request_answer: Response of a request.

    Returns:
        Formated data of the json format of the response.
    """
    json_res = request_answer.json()
    match len(json_res):
        case 0:
            raise TypeError("Only size-1 arrays accepted. Json response size: 0.")
        case 1:
            raw_data = json_res[0]

            res = {
                "update": raw_data["update"],
                # `|` is the add union operator. https://peps.python.org/pep-0584/
                "data": raw_data["data"] | raw_data["data"].pop("parameters"),
            }
            return _get_disk_info_in_good_format_for_template(res)
        case _:
            raise TypeError(
                f"Only size-1 arrays accepted. Json response size: {len(json_res)}."
            )


def when_exodict_request_answers_ok(
    request_answer: rq_models.Response,
) -> dict[str, Any]:
    """
    Treatment when a exodict request answers without error.

    Args:
        request_answer: Reposnse of a request.

    Returns:
        Formated data of the json format of the response.
    """
    json_res = request_answer.json()
    return {
        "update": _get_update_date(json_res[0]["update"]),
        "data": json_res[0]["data"],
    }


def grouped_info(
    disk_info: dict[str, Any],
    exodict_info: dict[str, Any],
) -> dict[str, Any]:
    """
    Group information of disk and exodict for the template.

    Args:
        disk_info: Disk informations.
        exodict_info: Exodict informations.

    Returns:
        Grouped informations.
    """
    result = {}

    local_exodict_info = exodict_info.copy()
    # `|` is the add union operator. https://peps.python.org/pep-0584/
    local_exodict_info = local_exodict_info | local_exodict_info.pop("parameters")

    for fl_key, fl_value in disk_info.items():
        res = {}
        exodict_param = local_exodict_info.get(fl_key, {})
        for key, value in fl_value.items():
            param = exodict_param.get(key, {})
            long_label = (
                param.get("label", {}).get("long", key)
                if param and param.get("label", {}).get("long")
                else key
            )
            definition = (
                param.get("definition", None)
                if param and param.get("definition")
                else None
            )
            if param.get("unit", None) == "PureNumber":
                exodict_unit = {"name": param["unit"], "symbol": None}
            else:
                exodict_unit = param.get("unit", [{"name": "", "symbol": ""}])[0]
            if param and exodict_unit:
                if isinstance(exodict_unit["symbol"], list):
                    unit_symbol = f"{exodict_unit['symbol'][0]}"
                else:
                    unit_symbol = f"{exodict_unit['symbol']}"
                unit_name = f"{exodict_unit['name']}"
            else:
                unit_name, unit_symbol = None, None
            res[key] = {
                "value": value,
                "label": long_label,
                "definition": definition,
                "unit_symbol": unit_symbol,
                "unit_name": unit_name,
            }
        result[fl_key] = res

    return result
