"""All utils function for views."""

# Local imports
from .catalog import when_catalog_request_answers_ok  # noqa: F401
from .individual import (  # noqa: F401
    grouped_info,
    when_exodict_request_answers_ok,
    when_object_request_answers_ok,
    when_principal_object_request_answers_ok,
)
from .requests import REQUEST_TIMEOUT, make_get_request  # noqa: F401
from .system import get_objects_info, when_grav_sys_request_answers_ok  # noqa: F401
from .utils import (  # noqa: F401
    NewDiskDesignBaseTemplateView,
    slugify,
    when_request_answers_error,
)
