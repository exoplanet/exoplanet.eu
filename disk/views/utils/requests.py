"""Useful function related to requests for views."""

# Standard imports
from http import HTTPStatus
from typing import Any, Callable

# External imports
import requests
from requests import models as rq_models

REQUEST_TIMEOUT = 10


def make_get_request(
    base_url: str,
    params: dict[str, str],
    timeout_second: int,
    treatment_if_ok: Callable[[rq_models.Response], int | str | dict[str, Any]],
    treatment_if_error: Callable[[rq_models.Response], dict[str, Any]],
) -> int | str | dict[str, Any]:
    """
    Make a request.

    Args:
        base_url: Request to make.
        params: Get parameters of the request.
        timeout_second: Time on second for the timeout parameter.
        treatment_if_ok: Treatment function if the request answer without errors.
        treatment_if_error: Treatment function if the request answer with errors.

    Returns:
        The result of the treatment function.
    """
    response = requests.get(base_url, params=params, timeout=timeout_second)
    match response.status_code:
        case HTTPStatus.OK:
            return treatment_if_ok(response)
        case _:
            return treatment_if_error(response)
