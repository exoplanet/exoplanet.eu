"""Useful function for the catalog view."""

# Standard imports
import json

# External imports
from requests import models as rq_models

# Local imports
from .utils import slugify


def when_catalog_request_answers_ok(
    request_answer: rq_models.Response,
) -> str:
    """
    Treatment when a catalogue request answers without error.

    Args:
        request_answer: Response of a request.

    Returns:
        Formated data of the json format of the response.

    """
    json_res = request_answer.json()

    if len(json_res) == 0:
        return ""

    disks = json_res
    for disk in disks:
        # TODO: replace by a real slugify
        disk["uri"] = f"{slugify(disk['name'])}__{disk['id']}"

    return json.dumps(disks)
