# Standard imports
import re
from typing import Any

# External imports
from requests import models as rq_models

# First party imports
from common.views import NewDesignBaseTemplateView


def when_request_answers_error(  # type: ignore[return]
    request_answer: rq_models.Response,
) -> dict[str, Any]:
    """
    Treatment when a request answes an error.

    Args:
        request_answer: Reposnse of a request.
    """
    request_answer.raise_for_status()


def slugify(name: str) -> str:
    """
    Slugify a name for an url.

    Args:
        name: Name to slugify.

    Returns:
        The slug of the given name.
    """
    return re.sub(r"[^0-9a-zA-Z-]", "-", name)


class NewDiskDesignBaseTemplateView(NewDesignBaseTemplateView):
    """
    blabla
    """

    is_beta = True
