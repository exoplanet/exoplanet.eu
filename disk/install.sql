-- SQL to execute before the migration to create the schema
-- and give goods rights to users

CREATE SCHEMA disk_api;
GRANT CREATE, USAGE ON SCHEMA disk_api TO exo_user;
GRANT CREATE ON DATABASE exoplanet TO exo_user;
GRANT USAGE ON SCHEMA disk_api TO web_anon;
