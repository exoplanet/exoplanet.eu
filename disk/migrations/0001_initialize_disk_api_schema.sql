-- Grant privilege to the web anon user
GRANT USAGE ON SCHEMA disk_api TO exo_web_anon;
GRANT SELECT ON disk_api.disk TO exo_web_anon;
GRANT SELECT ON disk_api.exodict TO exo_web_anon;
GRANT SELECT ON disk_api.star TO exo_web_anon;

-- Alter `created` and `modified` date for default date
ALTER TABLE disk_api.disk ALTER COLUMN created SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE disk_api.exodict ALTER COLUMN created SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE disk_api.star ALTER COLUMN created SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE disk_api.disk ALTER COLUMN modified SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE disk_api.exodict ALTER COLUMN modified
SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE disk_api.star ALTER COLUMN modified SET DEFAULT CURRENT_TIMESTAMP;

-- Create function to update date in the column `modified`
CREATE OR REPLACE FUNCTION UPDATE_MODIFIED_COLUMN()
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified := now();
    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

-- Create triggers to update date for the `modified` column
CREATE TRIGGER update_disk_api__disk_modified_column
BEFORE UPDATE ON disk_api.disk
FOR EACH ROW EXECUTE PROCEDURE UPDATE_MODIFIED_COLUMN();

CREATE TRIGGER update_disk_api__exodict_modified_column
BEFORE UPDATE ON disk_api.star
FOR EACH ROW EXECUTE PROCEDURE UPDATE_MODIFIED_COLUMN();

CREATE TRIGGER update_disk_api__star_modified_column
BEFORE UPDATE ON disk_api.exodict
FOR EACH ROW EXECUTE PROCEDURE UPDATE_MODIFIED_COLUMN();
