-- Drop triggeer if exists update_disk_api__table_modified_column
-- ON disk_api schema
DROP TRIGGER IF EXISTS update_disk_api__gravitationalsystems_modified_column
ON disk_api.catalogheaders;
