# Generated by Django 3.2.16 on 2023-07-06 09:39

# Standard imports
from pathlib import Path

# Django 💩 imports
from django.db import migrations, models

# External imports
import jsonfield.fields

MIGRATION_BASE = Path(__file__).parent
GRAVITATIONAL_SYSTEMS_DATES_SQL_FILE = (
    MIGRATION_BASE / "0003_gravitational_system_table_dates.sql"
)
REVERSE_GRAVITATIONAL_SYSTEMS_DATES_SQL_FILE = (
    MIGRATION_BASE / "0003_reverse_gravitational_system_table_dates.sql"
)

# SQL scripts used to create the db view
with (
    open(GRAVITATIONAL_SYSTEMS_DATES_SQL_FILE, encoding="UTF-8") as f_init,
    open(REVERSE_GRAVITATIONAL_SYSTEMS_DATES_SQL_FILE, encoding="UTF-8") as f_uninit,
):
    GRAVITATIONAL_SYSTEMS_DATES_SQL = f_init.read()
    REVERSE_GRAVITATIONAL_SYSTEMS_DATES_SQL = f_uninit.read()


class Migration(migrations.Migration):
    dependencies = [
        ("disk", "0002_catalog_headers_table"),
    ]

    operations = [
        migrations.CreateModel(
            name="GravitationalSystems",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateField()),
                ("modified", models.DateField()),
                ("data", jsonfield.fields.JSONField(default=dict)),
            ],
            options={
                "db_table": 'disk_api"."gravitationalsystems',
                "ordering": ["id"],
            },
        ),
        migrations.RunSQL(
            sql=GRAVITATIONAL_SYSTEMS_DATES_SQL,
            reverse_sql=REVERSE_GRAVITATIONAL_SYSTEMS_DATES_SQL,
        ),
    ]
