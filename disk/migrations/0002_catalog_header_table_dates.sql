-- Grant privilege to the web anon user
GRANT SELECT ON disk_api.catalogheaders TO exo_web_anon;

-- Alter `created` and `modified` date for default date
ALTER TABLE disk_api.catalogheaders ALTER COLUMN created
SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE disk_api.catalogheaders ALTER COLUMN modified
SET DEFAULT CURRENT_TIMESTAMP;

-- Create triggers to update date for the `modified` column
CREATE TRIGGER update_disk_api__catalogheaders_modified_column
BEFORE UPDATE ON disk_api.catalogheaders
FOR EACH ROW EXECUTE PROCEDURE UPDATE_MODIFIED_COLUMN();
