-- Drop triggeer if exists update_disk_api__table_modified_column
-- ON disk_api schema
DROP TRIGGER IF EXISTS update_disk_api__disk_modified_column
ON disk_api.disk;

DROP TRIGGER IF EXISTS update_disk_api__exodict_modified_column
ON disk_api.star;

DROP TRIGGER IF EXISTS update_disk_api__star_modified_column
ON disk_api.exodict;

-- Drop the associated function

DROP FUNCTION IF EXISTS update_modified_column;

-- Revoke privilege to web anon user

REVOKE ALL
ON ALL TABLES IN SCHEMA disk_api
FROM exo_web_anon;

REVOKE ALL
ON SCHEMA disk_api
FROM exo_web_anon;
