"""Apps config file for the disk app."""

# Django 💩 imports
from django.apps import AppConfig


class DiskConfig(AppConfig):
    """
    Config disk class.
    """

    name = "disk"
