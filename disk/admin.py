"""
Configuration of the django admin pages of the models.
"""
# Django 💩 imports
from django.contrib import admin

# Local imports
from .models import CatalogHeaders, Disk, Exodict, Star


@admin.register(Star)
class StarAdmin(admin.ModelAdmin):
    pass


@admin.register(Disk)
class DiskAdmin(admin.ModelAdmin):
    pass


@admin.register(Exodict)
class ExodictAdmin(admin.ModelAdmin):
    pass


@admin.register(CatalogHeaders)
class CatalogHeadersAdmin(admin.ModelAdmin):
    pass
