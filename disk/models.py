"""Models for the disk db."""

# Django 💩 imports
from django.db.models import BooleanField, CharField, DateField, Model
from django_stubs_ext.db.models import TypedModelMeta

# External imports
import jsonfield


class AbstractJson(Model):
    """
    Abstract class for a json table.

    Attributes:
        created: Created date.
        modified: Last update date.
        data: Data field.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Database mapping info."""

        abstract = True

    created: DateField = DateField()
    modified: DateField = DateField()
    data = jsonfield.JSONField()


class Disk(AbstractJson):
    """ORM for the table disk."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Database mapping info."""

        db_table = 'disk_api"."disk'
        ordering = ["id"]


class Star(AbstractJson):
    """ORM for the table star."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Database mapping info."""

        db_table = 'disk_api"."star'
        ordering = ["id"]


class Exodict(AbstractJson):
    """
    ORM for the table exodict.

    Attributes:
        version: Version of the exodict.
        ref: Is the reference exodict.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Database mapping info."""

        db_table = 'disk_api"."exodict'
        ordering = ["id"]

    version: CharField = CharField("version", max_length=100)
    ref: BooleanField = BooleanField("ref")


class CatalogHeaders(AbstractJson):
    """
    ORM for the table catalog_headers.

    Attributes:
        ref: Is the reference exodict.
    """

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Database mapping info."""

        db_table = 'disk_api"."catalogheaders'
        ordering = ["id"]

    ref: BooleanField = BooleanField("ref")


class GravitationalSystems(AbstractJson):
    """ORM for the table gravitational_systems."""

    class Meta(TypedModelMeta):  # pylint: disable=too-few-public-methods
        """Database mapping info."""

        db_table = 'disk_api"."gravitationalsystems'
        ordering = ["id"]
