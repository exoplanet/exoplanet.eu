"""
exoplanet.disk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Django 💩 imports
from django.urls import path, re_path
from django.views.generic.base import RedirectView

# Local imports
from .views import DiskCatalog, Individual, Systems

DISK_SLUG_REGEX = r"^catalog/(?P<disk_slug>[0-9a-zA-Z-]+)__(?P<disk_id>[0-9]+)/$"
SYSTEM_SLUG_REGEX = r"^system/(?P<grav_sys_id>[0-9]+)/$"

urlpatterns = [
    path(
        route="catalog/",
        view=DiskCatalog.as_view(),
        name="disk_catalog",
    ),
    path(
        route="",
        view=RedirectView.as_view(url="catalog/", permanent=True),
        name="disk_catalog",
    ),
    re_path(
        route=DISK_SLUG_REGEX,
        view=Individual.as_view(),
        name="disk_details",
    ),
    re_path(
        route=SYSTEM_SLUG_REGEX,
        view=Systems.as_view(),
        name="system_details",
    ),
]
