"""
shortcuts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Django 💩 imports
from django.urls import path, re_path

# Local imports
from .views import ALL_SHORTCUTS, CatalogShortcut, ExportCatalogJsonForDatatables

# Those regex allow django to serve the classic json needed by the catalog even if the
# current url is a shortcut url and not the catralog base url
# Regex tested via https://pythex.org/ to ensure correct behavior
CATALOG_JSON_FOR_SHORTCUTS_REGEX = r"^[0-9a-zA-Z_]+/json/$"
CATALOG_JSON_ALL_FILEDS_FOR_SHORTCUTS_REGEX = r"^[0-9a-zA-Z_]+/all_fields/json/$"

urlpatterns = [
    path(
        route=shortcut.url_suffix,
        view=CatalogShortcut.as_view(),
        kwargs={"query_f": shortcut.query_f},
        name=shortcut.url_name,
    )
    for shortcut in ALL_SHORTCUTS
]

# We had this so that the catalog can access the json it needs in a shortcut url
urlpatterns += [
    re_path(
        route=CATALOG_JSON_FOR_SHORTCUTS_REGEX,
        view=ExportCatalogJsonForDatatables.as_view(),
        name="legacy_catalog_shortcuts_json",
    ),
    re_path(
        route=CATALOG_JSON_ALL_FILEDS_FOR_SHORTCUTS_REGEX,
        view=ExportCatalogJsonForDatatables.as_view(),
        kwargs=dict(all_fields=True),
        name="legacy_catalog_all_fields_shortcuts_json",
    ),
]
