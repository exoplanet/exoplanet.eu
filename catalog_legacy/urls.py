"""
exoplanet.canary URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Django 💩 imports
from django.urls import include, path, re_path
from django.views.generic.base import RedirectView

# Local imports
from . import views

# BUG a slash in the url ?! WTF
# BUG possible code injection here ?! WTF
# Regex tested via https://pythex.org/ to ensure correct behavior
CATALOG_PLANET_NAME_URL_REGEX = (
    r"^catalog/(?P<planet_safe_name>[0-9a-z_]+)--(?P<planet_id_str>[0-9]+)/$"
)
BIBLIOGRAPHY_SECTION_URL_REGEX = (
    r"^bibliography/(?P<section>books|reports|theses|all)/$"
)
RESEARCH_SECTION_URL_REGEX = r"^research/(?P<section>ground|space|all)/$"
NEWS_URL_REGEX = r"^news/news__(?P<news_id_str>[0-9]+)/$"

urlpatterns = [
    path(
        route="home/",
        view=views.Home.as_view(),
        name="legacy_home",
    ),
    path(
        route="",
        view=RedirectView.as_view(pattern_name="legacy_home", permanent=True),
        name="legacy_root",
    ),
    path(
        route="catalog/",
        view=views.CatalogHtml.as_view(),
        name="legacy_catalog",
    ),
    # convenience shortcuts for frequent search on the catalog
    path(route="catalog/shortcuts/", view=include("catalog_legacy.urls_shortcuts")),
    path(
        route="catalog/all_fields/",
        view=views.CatalogHtml.as_view(),
        kwargs=dict(all_fields=True),
        name="legacy_catalog_all_fields",
    ),
    path(
        route="catalog/json/",
        view=views.ExportCatalogJsonForDatatables.as_view(),
        name="legacy_catalog_json",
    ),
    path(
        route="catalog/all_fields/json/",
        view=views.ExportCatalogJsonForDatatables.as_view(),
        kwargs=dict(all_fields=True),
        name="legacy_catalog_all_fields_json",
    ),
    path(
        route="catalog/csv/",
        view=views.ExportCatalogCsv.as_view(),
        kwargs=dict(variant="csv"),
        name="legacy_export_csv",
    ),
    path(
        route="catalog/dat/",
        view=views.ExportCatalogCsv.as_view(),
        kwargs=dict(variant="dat"),
        name="legacy_export_dat",
    ),
    path(
        route="catalog/votable/",
        view=views.ExportCatalogVOtable.as_view(),
        name="legacy_export_votable",
    ),
    # BUG this is buggy since catalog/json (and probably some other) is a subcase of
    #     this regex -> catalog/detail/<slug:obj_slug> would be far better
    # TODO we should add a prefix for the planets to have clean API-like urls
    # BUG We must replace the slug here with a simple id
    re_path(
        route=CATALOG_PLANET_NAME_URL_REGEX,
        view=views.PlanetDetail.as_view(),
        name="legacy_planet_detail",
    ),
    # TODO: DOCUMENTER CETTE MERDE
    re_path(
        route=r"^catalog/(?P<planet_name>[\w|\W]+)/$",
        view=views.PlanetOldUrl.as_view(),
        name="old_url_detail",
    ),
    re_path(
        route=BIBLIOGRAPHY_SECTION_URL_REGEX,
        view=views.BibliographyListView.as_view(),
        name="legacy_bibliography_section",
    ),
    path(
        route="bibliography/",
        view=RedirectView.as_view(url="all/", permanent=True),
        name="legacy_bibliography",
    ),
    re_path(
        route=RESEARCH_SECTION_URL_REGEX,
        view=views.ResearchListView.as_view(),
        name="legacy_research_section",
    ),
    path(
        route="research/",
        view=RedirectView.as_view(url="all/", permanent=True),
        name="legacy_research",
    ),
    path(
        route="meetings/future/",
        view=views.FutureMeetingListView.as_view(),
        name="legacy_meeting_future",
    ),
    path(
        route="meetings/past/",
        view=views.PastMeetingListView.as_view(),
        name="legacy_meeting_past",
    ),
    path(
        route="meetings/",
        view=RedirectView.as_view(pattern_name="legacy_meeting_future", permanent=True),
        name="legacy_meetings",
    ),
    path(
        route="sites/",
        view=RedirectView.as_view(pattern_name="legacy_links", permanent=True),
        name="legacy_sites",
    ),
    path(
        route="links/",
        view=views.LinkListView.as_view(),
        name="legacy_links",
    ),
    re_path(
        route=NEWS_URL_REGEX,
        view=views.NewsDetail.as_view(),
        name="legacy_news_detail",
    ),
    path(
        route="news/",
        view=views.NewsListView.as_view(),
        name="legacy_news",
    ),
    path(
        route="news/rss/",
        view=views.NewsFeed(),
        name="legacy_news_rss",
    ),
    path(
        route="plots/",
        view=views.Plots.as_view(),
        name="legacy_plots",
    ),
    path(
        route="plots_request_ajax/",
        view=views.plots_request_ajax,
        name="legacy_plots_request_ajax",
    ),
    path(
        route="polar_plot/",
        view=views.PolarPlot.as_view(),
        name="legacy_polar_plot",
    ),
    path(
        route="polar_plot_ajax/",
        view=views.polar_plot_ajax,
        name="legacy_polar_plot_ajax",
    ),
]
