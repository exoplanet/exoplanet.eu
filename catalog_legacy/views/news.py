"""
Views for the news listing page and the news RSS feed.
"""

# Standard imports
from datetime import datetime
from typing import Any, cast

# Django 💩 imports
from django.contrib.syndication.views import Feed
from django.db.models.query import QuerySet
from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils.html import strip_tags
from django.views.generic.list import ListView

# First party imports
from common.views import NewDesignBase, NewDesignBaseTemplateView
from core.models import filter_query_set_status_is_not_hidden
from editorial.models import News

SHOULD_BE_SET_IN_GET = "Should be set un get"


class NewsDetail(NewDesignBaseTemplateView):
    """
    Display a page showing a specific news.

    Support for /news/news__{news_id_str}/ URLs
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "news_detail.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = SHOULD_BE_SET_IN_GET
    description = SHOULD_BE_SET_IN_GET

    # pylint: disable=arguments-differ
    def get(
        self,
        request: HttpRequest,
        news_id_str: str,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Support for /news/news__{news_id_str}/ URLs

        Args:
            request: http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.
            planet_id_str: database id of the news extracted from the url

        Returns:
            page with all the content and info about the news.

        Raises:
            Http404: if slug do not correspond to any news
        """

        # Retrieve the id
        try:
            news_id = int(news_id_str)
        except ValueError as err:
            raise Http404(f"News id {news_id_str} is not a valid numeric id.") from err

        # Get the news object from database
        news_obj = get_object_or_404(News, id=news_id)

        # Set title and description of the page
        # Star has an attribute `id` automaticly add by django.
        # django model (Django == 💩)
        self.title = f"News #{news_obj.id}"  # type: ignore[attr-defined]
        self.description = f"Publication date: {news_obj.date}"

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                news=news_obj,
            ),
        )


class NewsListView(NewDesignBase, ListView):
    """
    View for /news/ page.
    """

    # ListView attributes
    paginate_by = 20
    model = News
    ordering = "-date"

    def get_queryset(self, *args: Any, **kwargs: dict[str, Any]) -> QuerySet[News]:
        query_set = super(NewsListView, self).get_queryset(*args, **kwargs)
        # Ignore because it's a bug of django-stubs. `get_queryset()` return a QuerySet
        # but django-stubs type it with `_SupportPagination[Any]`.
        # TODO: Remove type: ignore when the bug is fixed.
        # https://github.com/typeddjango/django-stubs/issues/1928
        return filter_query_set_status_is_not_hidden(
            query_set  # type: ignore[arg-type]
        )

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "news.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "News"
    description = "Recent informations published about exoplanet encyclopaedia."


class NewsFeed(Feed):
    """
    RSS feed for latest news
    """

    title: str = "Encyclopaedia of exoplanetary systems news"
    link: str = "/news/rss/"
    description: str = (
        "Updates on changes and additions to Encyclopaedia of exoplanetary systems"
    )

    def items(self) -> QuerySet:
        return News.objects.order_by("-created")[:10]  # pylint: disable=no-member

    def item_title(self, item: News) -> str:
        # TODO generate a better title than truncated content
        striped_content = strip_tags(item.content)[:50]
        return "{}...".format(striped_content)

    def item_description(self, item: News) -> str:
        return str(item.content)

    def item_pubdate(self, item: News) -> datetime:
        return cast(datetime, item.created)
