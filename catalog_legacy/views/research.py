"""
Django view for the research listing page.
"""

# Standard imports
from typing import Any, cast

# Django 💩 imports
from django.db.models import Q
from django.db.models.query import QuerySet
from django.http import Http404
from django.views.generic.list import ListView

# First party imports
from common.views import NewDesignBase
from core.models import QUERY_NOT_HIDDEN_STATUS, ResearchType
from editorial.models import Research


class ResearchListView(NewDesignBase, ListView):
    """
    View for :
    - /research/
    - /research/all/
    - /research/ground/
    - /research/space/
    pages.
    """

    # ListView attributes
    paginate_by = 20
    model = Research

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "research.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Research"
    # TODO: view for a better description
    description = "Full research of the exoplanet catalogue."

    def get_queryset(self) -> QuerySet[Research]:
        """
        Get the query set of data to display according to the filter form in the page.

        Args passed via self.kwargs from urls.py:
            section: specify if we want all publication or just a specific subset.
                    Possible values are:

                - 'all': no filter
                - 'ground': only ground research
                - 'space': only space research

        Returns:
            A querySet of Research selected according to the filter form in the page

        Raises:
            Http404: if section is not valid.
        """
        # BUG "theses" is a french term and we should use the "thesis" english terms
        section_queries = dict(
            ground=Q(type=ResearchType.GROUND),
            space=Q(type=ResearchType.SPACE),
            all=Q(),
        )

        # Parameter passed from the urls.py
        section = self.kwargs.get("section", "all")

        try:
            q_section = section_queries[section]
        except KeyError as err:
            raise Http404(f"There is no {section} section in the research.") from err

        # Let's get the actual publication to display using pagination
        # Ignore because it's a bug of django-stubs. `get_queryset()` return a QuerySet
        # but django-stubs type it with `_SupportPagination[Any]`.
        # TODO: Remove type: ignore when the bug is fixed.
        # https://github.com/typeddjango/django-stubs/issues/1928
        all_publications = (
            super()  # type: ignore[attr-defined]
            .get_queryset()
            .order_by("-created")
            .filter(
                q_section,
                QUERY_NOT_HIDDEN_STATUS,
            )
        )

        return cast(QuerySet[Research], all_publications)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Add some custom data to the context of the page.

        Args passed via self.kwargs from urls.py:
            section: specify if we want all publication or just a specific subset.
                     Possible values are:

                - 'all': no filter
                - 'ground': only ground research
                - 'space': only space research
        """
        context = super().get_context_data(**kwargs)

        # Parameter passed from the urls.py
        section = self.kwargs.get("section", "all")

        context.update(
            dict(
                view=self,
                section=section,
            )
        )

        return context
