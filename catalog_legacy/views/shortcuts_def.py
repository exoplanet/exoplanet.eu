"""
List of all the shortcuts.

This is separated from `shortcuts.py` to prevent circular import problems with
`catalog.py`.
"""

# Standard imports
from typing import NamedTuple


# TODO: add a default field and add a test to ensure that only one of shortcut has it
class ShortcutInfo(NamedTuple):
    """Tuple to store info about shortcuts."""

    url_name: str
    """
    Name to use in the django url definition.

    Must be unique.
    """

    url_suffix: str
    """Last part of the url taht appears after /shortcuts/.

    No slash at the beginning.

    Final slash MUST be included.
    """

    query_f: str
    """The query to pass to the query_f mini language."""

    link_text: str
    """HTML text tu use for the link and the title of the shortcut page."""

    default: bool
    """
    If the Shortcut is the default one to use on exoplanet.eu.

    Only one can be true.
    """


ALL_SHORTCUTS: list[ShortcutInfo] = [
    ShortcutInfo(
        url_name="shortcut circumbinaries",
        url_suffix="circumbinary/",
        query_f=(
            "(mass:mjup < 13 OR (mass = null AND NOT radius = null)) "
            'AND NOT "Cen" in name AND NOT "Cet" in name '
            'AND (" Ab" in name '
            'OR " Ac" in name '
            'OR " Ad" in name '
            'OR " Ae" in name '
            'OR " Af" in name '
            'OR " Ag" in name '
            'OR " Ah" in name '
            'OR " Bb" in name '
            'OR " Bc" in name '
            'OR " Bd" in name '
            'OR " Be" in name '
            'OR " Bf" in name '
            'OR " Bg" in name '
            'OR " Bh" in name '
            'OR " Cb" in name '
            'OR " Cc" in name '
            'OR " Cd" in name '
            'OR " Ce" in name '
            'OR " Cf" in name '
            'OR " Cg" in name '
            'OR " Ch" in name '
            'OR " Db" in name '
            'OR " Dc" in name '
            'OR " Dd" in name '
            'OR " De" in name '
            'OR " Df" in name '
            'OR " Dg" in name '
            'OR " Dh" in name)'
        ),
        link_text="Objects of circumbinaries",
        default=False,
    ),
    ShortcutInfo(
        url_name="shortcut mass between 0.1 Mmer and 13 Mjup",
        url_suffix="between_0_1_mmer_and_13_mjup/",
        query_f=(
            "("
            "(mass:mjup < 13 and mass:mearth > 0.05) "
            "OR (mass = null AND NOT radius = null)"
            ") AND NOT star_name = null"
        ),
        link_text=(
            "Objects with <math>"
            "<mn>0.1</mn> "
            '<mspace width="0.3em"></mspace>'
            "<msub><mi>M</mi><mi>mer</mi></msub>"
            "<mo>&lt;</mo> "
            "<mi>mass</mi> "
            "<mo>&lt;</mo> "
            "<mn>13</mn> "
            '<mspace width="0.3em"></mspace>'
            "<msub><mi>M</mi><mi>jup</mi></msub>"
            "</math>"
        ),
        default=True,
    ),
    ShortcutInfo(
        url_name="shortcut mass between 13 Mjup and 30 Mjup",
        url_suffix="between_13_mjup_and_30_mjup/",
        query_f=(
            "("
            "(mass:mjup > 13 and mass:mjup < 30) "
            "OR (mass = null AND Not radius = null)"
            ") AND NOT star_name = null"
        ),
        link_text=(
            "Objects with <math>"
            "<mn>13</mn> "
            '<mspace width="0.3em"></mspace>'
            "<msub><mi>M</mi><mi>jup</mi></msub>"
            "<mo>&lt;</mo> "
            "<mi>mass</mi> "
            "<mo>&lt;</mo> "
            "<mn>30</mn> "
            '<mspace width="0.3em"></mspace>'
            "<msub><mi>M</mi><mi>jup</mi></msub>"
            "</math>"
        ),
        default=False,
    ),
    ShortcutInfo(
        url_name="shortcut mass above 13 Mjup and 30 Mjup",
        url_suffix="between_above_30_mjup/",
        query_f=(
            "(mass:mjup > 30 OR (mass=null AND NOT radius = null)) "
            "AND NOT star_name = null"
        ),
        link_text=(
            "Objects with <math>"
            "<mn>30</mn> "
            '<mspace width="0.3em"></mspace>'
            "<msub><mi>M</mi><mi>jup</mi></msub>"
            "<mo>&lt;</mo> "
            "<mi>mass</mi> "
            "</math>"
        ),
        default=False,
    ),
    ShortcutInfo(
        url_name="shortcut mass under 0.1 Mmer",
        url_suffix="between_under_0_1_mmer/",
        query_f=("(mass:mearth < 0.05 OR radius:rearth < 1) AND NOT star_name = null"),
        link_text=(
            "Objects with <math>"
            "<mi>mass</mi> "
            "<mo>&lt;</mo> "
            "<mn>0.1</mn> "
            '<mspace width="0.3em"></mspace>'
            "<msub><mi>M</mi><mi>mer</mi></msub>"
            "</math>"
        ),
        default=False,
    ),
    ShortcutInfo(
        url_name="shortcut solar system",
        url_suffix="solar_system/",
        query_f='star_name = "sun"',
        link_text="Objects of the Solar System",
        default=False,
    ),
    ShortcutInfo(
        url_name="shortcut post main sequence stars",
        url_suffix="post_main_sequence_stars/",
        query_f='"WD" in star_name OR "PSR" in star_name OR "GW" in star_name',
        link_text="Objects of stars from post main sequence",
        default=False,
    ),
    ShortcutInfo(
        url_name="shortcut free floating",
        url_suffix="freefloating/",
        query_f="star_name = null",
        link_text="Free floating exoplanets",
        default=False,
    ),
    ShortcutInfo(
        url_name="full catalog",
        url_suffix="full_catalog/",
        query_f="",
        link_text=("All objects present in the catalog"),
        default=False,
    ),
]
