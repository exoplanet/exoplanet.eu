"""All views needed to display the catalog page with a clean table."""

# Standard imports
import logging
from typing import Any

# Django 💩 imports
from django.db.models import Count
from django.http import HttpRequest, HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import View

# First party imports
from common.views import NewDesignBaseTemplateView
from core.outputs import CatalogOutput, PaginationInfo, SortInfo, UnitsInfo

# Local imports
from .shortcuts_def import ALL_SHORTCUTS, ShortcutInfo
from .views_helpers import (
    build_detection_filter_from_http_param,
    build_minimal_context_for_object_list,
    build_status_filter_from_http_param,
)

LOGGER = logging.getLogger(__name__)


class CatalogHtml(NewDesignBaseTemplateView):
    """
    Display the page with table representation of the catalogue.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "catalog_html.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title: str = "Catalogue of Exoplanets"
    description: str = (
        "Sortable and filterable catalogue of the exoplanet discovered so far."
    )
    all_shortcuts: list[ShortcutInfo] = ALL_SHORTCUTS

    def _post_or_get(
        self,
        request: HttpRequest,
        all_fields: bool = False,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Support for /catalog/ and /catalog/?q=heh URLs (main catalog page and
        searches)

        The content of the table itself is not produced or returned here: the table
        use a json data source provided by `json_object_list()` function.

        Args:
            request: http request for the page. It can
                contain the following keys:

                - status_1: the filter #1 of PLANET_STATUSES_FILTER is activated
                - status_2: the filter #2 of PLANET_STATUSES_FILTER is activated
                - status_4: the filter #4 of PLANET_STATUSES_FILTER is activated

            template_name: name of the template to be used to render the page
            all_fields: tells if all possible fields should be displayed or
                just enough for the compact view.

        Returns:
            HttpResponse: page with a table showing the catalogue.
        """
        # Let's build the context
        context = build_minimal_context_for_object_list(request.POST, all_fields)

        # we need this to have the context of the class view
        context.update(view=self)

        return render(
            request,
            template_name=self.template_name,
            context=context,
        )

    @method_decorator(ensure_csrf_cookie)
    def get(
        self,
        request: HttpRequest,
        all_fields: bool = False,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Support GET request for /catalog/ and /catalog/?q=heh URLs (main catalog page
        and searches)
        """
        return self._post_or_get(
            request,
            all_fields,
            *args,
            **kwargs,
        )

    # TODO test /catalog/all_fields/
    @method_decorator(ensure_csrf_cookie)
    def post(
        self,
        request: HttpRequest,
        all_fields: bool = False,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Support POST request for main catalogue page
        and searches:

        - /catalog/
        - /catalog/?q=heh
        - /catalog/all_fields/
        """
        return self._post_or_get(
            request,
            all_fields,
            *args,
            **kwargs,
        )


class ExportCatalogJsonForDatatables(View):
    """
    Support for /catalog/json/ URL used by datatables to dispaly the catalog.
    """

    def post(
        self,
        request: HttpRequest,
        all_fields: bool = False,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse | HttpResponseBadRequest:
        """
        Provides JSON data for the datatable
        used to display the content of the catalog.

        Args:
            request: http request for the page. It must contain the following
            parameters:

                - sSearch (str): search string to specify which planets to display
                - iSortCol_0 (int as str): index of the column to use for sorting the
                results
                - sSortDir_0 (str): direction of the sort with only 2 possible values:
                - "desc": descending
                - "asc": ascending
                - iDisplayStart (int): index of the first line to display. This is
                    used for pagination. Must be >=0.
                - iDisplayLength (int): length of the returned table in nb of
                    lines. This is used for pagination. Must be >0
                - sEcho (int): mandatory param for DataTables... it must be sent
                    back with the exact same value...
                    see http://legacy.datatables.net/usage/server-side

                and can containe the following parameters :

                - query_f (str): filter string to limit the planet to display
                - mass_unit (str): unit to use for the "mass" values
                - mass_sini_unit (str): unit to use for the "mass sin i" values
                - radius_unit (str): unit to use for the "radius" values

            all_fields: tells if all possible fields should be displayed or just enough
            for the compact view.
        Returns:
            HttpResponse: JSON data corresponding to the table to display. It contains
            the following keys:

            - aaData: a table of rows of data
            - iTotalRecords: total number of planets in the DB
            - iTotalDisplayRecords: total number of planets selected by this specific
            request
            - sEcho: the exact copy of the sEcho arg provided to this function via
            request.
            - stat: statistics about the returned values:

            - planet: number of planets
            - systems: number of systems
            - multiple: number of systems with multiple planets

            or
            HttpResponseBadRequest: if any mandatory key of the request is absent or
            malformed.
        """
        # pylint: disable=too-many-locals
        # First we retreive all (mandatory or not) params from the request
        filter_string = request.POST.get("query_f", default=None)

        try:
            search = request.POST["sSearch"]
            sorted_column_id = int(request.POST["iSortCol_0"])

            # Retreive pagination config
            pagination = PaginationInfo(
                start=int(request.POST["iDisplayStart"]),
                length=int(request.POST["iDisplayLength"]),
            )

            sort_order = request.POST["sSortDir_0"]
            echo = int(request.POST["sEcho"])
        except KeyError as err:
            err_str = f"A mandatory key was not found in the POST request: {err}"
            LOGGER.error(err_str)
            return HttpResponseBadRequest(err_str)
        except ValueError as err:
            err_str = (
                f"A mandatory key was in the POST request but was provided "
                f"with the wrong type or value: {err}"
            )
            LOGGER.error(err_str)
            return HttpResponseBadRequest(err_str)

        status_filter = build_status_filter_from_http_param(request.POST)

        detection_filter = build_detection_filter_from_http_param(request.POST)

        # Retreive sorting config
        is_desc_order = sort_order == "desc"
        sort: SortInfo = {
            "column_id": sorted_column_id,
            "desc_order": is_desc_order,
        }

        # Retrieve units config
        units = UnitsInfo()
        if "mass_unit" in request.POST:
            units["mass"] = str(request.POST["mass_unit"])

        if "mass_sini_unit" in request.POST:
            units["mass_sini"] = str(request.POST["mass_sini_unit"])

        if "radius_unit" in request.POST:
            units["radius"] = str(request.POST["radius_unit"])

        catalog_output = CatalogOutput(
            all_fields,
            filter_string,
            sort,
            pagination,
            units,
            search,
            status_filter,
            detection_filter,
        )

        total_display_records = catalog_output.get_count()

        stats = {
            "planets": catalog_output.get_count(),
            "multiple": catalog_output.planets.values("star_name")
            .annotate(num_planets=Count("star_name"))
            .filter(num_planets__gt=1)
            .count(),
            "systems": catalog_output.planets.values("star_name")
            .annotate(num_planets=Count("star_name"))
            .filter(num_planets__gt=0)
            .count(),
        }

        values_display = catalog_output.get_values()

        data_for_datatable = {
            "aaData": values_display,
            "iTotalRecords": total_display_records,
            "iTotalDisplayRecords": total_display_records,
            "sEcho": echo,
            "stats": stats,
        }

        return JsonResponse(data_for_datatable)
