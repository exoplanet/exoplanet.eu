# Standard imports
from typing import Any

# Django 💩 imports
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.views.generic.list import ListView

# First party imports
from common.views import NewDesignBase
from core.models import filter_query_set_status_is_not_hidden
from editorial.models import Link


class LinkListView(NewDesignBase, ListView):
    """
    View for /links/ page.
    """

    # ListView attributes
    paginate_by = 50
    model = Link
    ordering = "id"

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "links.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Links"
    description = "General professional Web sites relevant to extrasolar planets."

    def get_queryset(self, *args: Any, **kwargs: dict[str, Any]) -> QuerySet[Link]:
        query_set = super(LinkListView, self).get_queryset(*args, **kwargs)
        # Ignore because it's a bug of django-stubs. `get_queryset()` return a QuerySet
        # but django-stubs type it with `_SupportPagination[Any]`.
        # TODO: Remove type: ignore when the bug is fixed.
        # https://github.com/typeddjango/django-stubs/issues/1928
        return filter_query_set_status_is_not_hidden(
            query_set  # type: ignore[arg-type]
        )


# TODO make this a class view
# TODO make this a ListView*
@require_http_methods(["GET"])
def sites(request: HttpRequest) -> HttpResponse:
    """View for /sites page

    Args:
        request: http request for the page. No specific key is nedded here, it is only
            passed as an arg to the final render function.

    Returns:
        The page with the list of the requested sites.
    """
    links = Link.objects.order_by("id")  # pylint: disable=no-member

    return render(request, template_name="sites.html", context=dict(links=links))
