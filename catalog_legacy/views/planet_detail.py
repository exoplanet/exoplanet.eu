"""
Django view for the planet detail page.
"""

# Standard imports
from itertools import groupby
from typing import Any, TypedDict

# Django 💩 imports
from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie

# External imports
from dotmap import DotMap

# First party imports
from amd.star_list import AMD_STAR_LIST
from common.views import NewDesignBaseTemplateView
from core.models import AtmosphereMolecule, Planet, PlanetDBView, Publication, WebStatus
from core.outputs import PlanetFieldDict, PlanetOutput, StarDict, SystemFieldDescDict

# Helper classes #######################################################################


class ReferenceDict(TypedDict):
    """JSON representation of a reference."""

    url: str
    title: str
    id: int
    date: str
    author: str


class TemperatureParamDict(TypedDict):
    """JSON representation of a temperature parameter."""

    data_source: str
    type: str
    result_value: float
    result_value_error: list[float]
    result_figure: str
    reference: ReferenceDict
    notes: str


class MoleculeParamDict(TypedDict):
    """JSON representation of a molecule parameter."""

    data_source: str
    type: str
    result_value: float  # TODO remove it is useless: see AtmosphereAbstract
    result_value_error: list[float]  # TODO remove it is useless: See AtmosphereAbstract
    result_figure: str  # TODO remove it is useless: See AtmosphereAbstract
    reference: ReferenceDict
    notes: str


class MoleculeDescDict(TypedDict):
    """JSON représentation of a molecule description."""

    name: str
    values: list[MoleculeParamDict]


# TODO in Python 3.11+ use typing.NotRequired instead of inheritance
class _StarContextForPlanet(TypedDict, total=False):
    """Optional page context attributes for a detail page with a star."""

    main_star_name: str
    main_star_url_keyword: str
    stars: list[StarDict]
    amd_star_list: list[str]


# TODO in Python 3.11+ use typing.NotRequired instead of inheritance
class _PlanetContext(_StarContextForPlanet):
    """Mandatory page context attributes for a detail page with or without a star."""

    view: NewDesignBaseTemplateView
    object: PlanetDBView
    planet_name: str
    planet_fields: list[PlanetFieldDict]
    temperature_parameter_list: list[TemperatureParamDict]
    molecule_list: list[MoleculeDescDict]
    planet_url_keyword: str
    planet_remarks: str
    planet_other_web: str
    pubs_display: list[Publication]
    system_fields: list[SystemFieldDescDict]


def _get_title_and_desc_for_planet_detail(planet_name: str) -> DotMap:
    return DotMap(
        {
            "title": f'Planet <span class="planet-name">{planet_name}</span>',
            "description": f"Detailed information about planet {planet_name} and its "
            "parameters.",
        }
    )


# Main class ###########################################################################

SHOULD_BE_SET_IN_GET = "Should be set in get"


class PlanetOldUrl(NewDesignBaseTemplateView):
    """
    Display the transitional page for the good url detail page.

    Support for /catalog/{planet_name} URLs

    Attributes:
        template_name = "old_to_new_url.html"
        title = SHOULD_BE_SET_IN_GET
        description = SHOULD_BE_SET_IN_GET
    """

    template_name = "old_to_new_url.html"
    title = SHOULD_BE_SET_IN_GET
    description = SHOULD_BE_SET_IN_GET

    def get(self, request: HttpRequest, planet_name: str) -> HttpResponse:
        """
        Support for /catalog/{_planet_name} URLs

        Args:
            request: http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.
            planet_name: name of the planet "slugified" (space ` ` chars are replaced
                by `_`)

        Returns:
            Transitional page with the link to the new url of the given planet.

        Raises:
            Http404: if planet_name do not correspond to any planet
        """
        planets = Planet.objects.filter(
            status=WebStatus.ACTIVE, name__iexact=planet_name.replace("_", " ")
        )
        if not planets:
            raise Http404("Planet {} doesn't exist in the catalog.".format(planet_name))

        planet = planets[0]

        # Set title and description of the page
        title_and_desc = _get_title_and_desc_for_planet_detail(planet.name)
        self.title = title_and_desc.title
        self.description = title_and_desc.description

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                new_url=planet.name_url_html,
                planet_name=planet.name,
            ),
        )


class PlanetDetail(NewDesignBaseTemplateView):
    """
    Display the page with detailed information about an exoplanet.

    Support for /catalog/{_sluggified_name}--{planet_id_str} URLs
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    # TODO use only one template and set it here
    template_name = SHOULD_BE_SET_IN_GET

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = SHOULD_BE_SET_IN_GET
    description = SHOULD_BE_SET_IN_GET

    # pylint: disable=arguments-differ
    @method_decorator(ensure_csrf_cookie)
    def get(
        self,
        request: HttpRequest,
        planet_safe_name: str,
        planet_id_str: str,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Support for /catalog/{_sluggified_name}--{planet_id_str} URLs

        Args:
            request: http request for the page. No specific key is
                nedded here, it is only passed as an arg to the final render
                function.
            planet_safe_name: name of the planet "slugified" (where all non ascii and
                non-JSON/url compatible chars are replaced by _). It is present in the
                url for user friendlyness and to improve SEO that demands meaningfull
                and content related urls. We give it to the view for debugging purpose
                only. Only the id is used for querying hence you can put anything in
                this part of the url as long as is ascii and JSON/url compatible.
            planet_id_str: database id of the planet extracted from the url

        Returns:
            page with a table showing the details on the object. The page is different
            if the object is linked to a star or not.
            If the name provided does not correspond to the real name of the planet
            corresponding to the id provided we redirect to the correct page with the
            correct name

        Raises:
            Http404: if slug do not correspond to any planet
        """
        # Retrieve the id
        try:
            planet_id = int(planet_id_str)
        except ValueError as err:
            raise Http404(
                f"Planet id {planet_id_str} "
                f"(provided with planet safe name {planet_safe_name})"
                f" is not a valid numeric id."
            ) from err

        # Get the JSON like representation of the planet
        try:
            planet_output = PlanetOutput(planet_id=planet_id)
        except PlanetDBView.DoesNotExist as err:  # pylint: disable=no-member
            raise Http404(
                f"Planet with id {planet_id} doesn't exist in the catalog."
            ) from err

        # Redirect to the correct url if the name is not correct
        actual_planet_safe_name = planet_output.planet.safe_name
        if planet_safe_name != actual_planet_safe_name:
            return redirect(
                "legacy_planet_detail",
                planet_safe_name=actual_planet_safe_name,
                planet_id_str=planet_id_str,
            )

        # TODO use planet_output.planet instead of doing a useless query here...
        # TODO remove this filter: we should not retreive data only for active planets
        planet_object = Planet.objects.filter(status=WebStatus.ACTIVE).get(
            id=planet_output.planet.id
        )

        atmosphere_temperatures = (
            planet_object.atmospheretemperature_set.all()
            .prefetch_related("publication")
            .order_by("type", "-publication__date")
        )

        temperature_parameter_list = [
            TemperatureParamDict(
                # TODO use clean Enum instead of this horror
                data_source=atm_tmp.DATA_SOURCES[atm_tmp.data_source],
                # TODO use clean Enum instead of this horror
                type=atm_tmp.ATMOSPHERE_DATA_TYPES[atm_tmp.type],
                result_value=atm_tmp.result_value,
                result_value_error=atm_tmp.result_value_error,
                result_figure=atm_tmp.result_figure,
                reference=ReferenceDict(
                    url=atm_tmp.publication.url,
                    title=atm_tmp.publication.title,
                    id=atm_tmp.publication.id,
                    date=atm_tmp.publication.date,
                    author=atm_tmp.publication.author.split(",")[0],
                ),
                notes=atm_tmp.notes,
            )
            for atm_tmp in atmosphere_temperatures
        ]

        # If the type is not valid just use the "detected" datat type
        default_molecule_data_type = AtmosphereMolecule.MOLECULE_DATA_TYPES[1]
        molecule_list = [
            MoleculeDescDict(
                name=mol_name,
                values=[
                    MoleculeParamDict(
                        # TODO use clean Enum instead of this horror
                        data_source=atmospheremolecule.DATA_SOURCES[
                            atmospheremolecule.data_source
                        ],
                        # TODO use clean Enum instead of this horror
                        type=atmospheremolecule.MOLECULE_DATA_TYPES.get(
                            atmospheremolecule.type, default_molecule_data_type
                        ),
                        result_value=atmospheremolecule.result_value,
                        result_value_error=atmospheremolecule.result_value_error,
                        result_figure=atmospheremolecule.result_figure,
                        reference=ReferenceDict(
                            url=atmospheremolecule.publication.url,
                            title=atmospheremolecule.publication.title,
                            id=atmospheremolecule.publication.id,
                            date=atmospheremolecule.publication.date,
                            author=atmospheremolecule.publication.author.split(",")[0],
                        ),
                        notes=atmospheremolecule.notes,
                    )
                    for atmospheremolecule in shared_name_molecules
                ],
            )
            for mol_name, shared_name_molecules in groupby(
                planet_output.atmosphere_molecules, lambda mol: mol.molecule.name
            )
        ]

        # Set title and description of the page
        title_and_desc = _get_title_and_desc_for_planet_detail(
            planet_output.planet.name
        )
        self.title = title_and_desc.title
        self.description = title_and_desc.description

        # TODO use the same template for both cases asap
        # Set the template
        if planet_output.stars.exists():
            self.template_name = "planet_with_star_detail.html"
        else:
            self.template_name = "planet_without_star_detail.html"

        # Common context info whether we have a star or not
        context = _PlanetContext(
            view=self,
            object=planet_output.planet,
            planet_name=planet_output.planet.name,
            planet_fields=planet_output.get_planet_fields(),
            temperature_parameter_list=temperature_parameter_list,
            molecule_list=molecule_list,
            planet_url_keyword=planet_output.planet.url_keyword,
            planet_remarks=planet_output.planet.remarks,
            planet_other_web=planet_output.planet.other_web,
            pubs_display=planet_output.pubs_display,
            system_fields=planet_output.get_system_fields(),
        )

        # We had specific context info for planets with a star
        if planet_output.stars.exists():
            context.update(
                _StarContextForPlanet(
                    main_star_name=planet_output.planet.star_name,
                    main_star_url_keyword=planet_output.get_main_star_url_keyword(),
                    stars=planet_output.get_stars(),
                    amd_star_list=AMD_STAR_LIST,
                )
            )

        # we need this to have the context of the class view
        context.update({"view": self})

        return render(
            request,
            template_name=self.template_name,
            context=context,
        )
