"""
Everything to have a diagram page showing the different kinds of online graphs.
"""

# Standard imports
import json
from http import HTTPStatus
from typing import Any

# Django 💩 imports
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_http_methods

# First party imports
from common.views import NewDesignBaseTemplateView
from core.models import PlanetDetection, title_name
from core.outputs import AxisId, PlotsOutput, PolarPlotOutput, get_scatter_headers

DEFAULT_FILTER = '"confirmed" in planet_status'


class Plots(NewDesignBaseTemplateView):
    """
    Display a page with online plots representing the data of the catalog.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "plots.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Plots"
    description = "Analyze the data online. Simple plotting tool right in the browser."

    # TODO change the t and f request param for more explicit names
    @method_decorator(ensure_csrf_cookie)
    def get(
        self, request: HttpRequest, *args: Any, **kwargs: dict[str, Any]
    ) -> HttpResponse:
        """
        Deals with /plots/ url by serving a page with the different plots.

        The data for the plots themselves are served by this view. They are served
        via AJAX request as JSON object via `plots_request_ajax`.

        Args:
            request (HttpRequest): http request for the page. It can contain the
            following keys:

            - f (str): a filter string to specify which planets to display
            - t (str): "h" to request an histogram and "s" (or any other value in fact)
            to have a scatter plot.

        Returns:
            HttpResponse: page giving access to the plots.
        """
        # defining default_axis, and headers
        headers, default_axis = get_scatter_headers()

        # TODO use a as_json() method
        headers_json = json.dumps([h._asdict() for h in headers])

        detections = [title_name(detection) for detection in PlanetDetection]

        # Build the default JSON data values

        # TODO simplify the usage of this constructor since we currently need to call
        # one of its static method before calling...
        default_data = PlotsOutput(
            filter_string=DEFAULT_FILTER,
            field_ids=default_axis,
        ).get_values()

        default_json = json.dumps(default_data, allow_nan=True)

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,  # we need this to have the context of the class view
                fields=headers_json,
                default_axis=default_axis,
                detections=detections,
                # TODO Replace JSON default data by an AJAX url
                data_init=default_json,
            ),
        )


@require_http_methods(["GET"])
def plots_request_ajax(request: HttpRequest) -> HttpResponse:
    """
    Answers to AJAX request for the data of the plots displayed via the `plots` view.

    Args:
        request (HttpRequest): http request for the page. It can contain the
            following keys:

            - x (str): name of the variable to use as x axis
            - y (str): name of the variable to use as y axis
            - z (str): name of the variable to use as z axis
            - p (str): name of the variable to use as p axis
            - f (str): a filter string as defined in `PlotOutput`if
            isinstance(field["model_field"], str)

            The names of the variable come from `field_definitions.yaml`. More
            specifically the `HISTOGRAM_VAR_PLANET_FIELDS` and
            `HISTOGRAM_VAR_STARS_FIELDS` list. But their exact definition
            depends (sadly) :

            - if it's a star parameter --> `related_model_field`
            - if `model_field` is a string --> `model_field`
            - if `model_field` is a dict (because we have 2 possible units) --> id

    Returns:
        - HttpResponse: if no error occurred, a JSON representation of the
        catalogue as requested according to the request params.
        - HttpResponse: a 406 error (Not Acceptable) if a syntax error occurred
        while processing the filter string of the request params.
    """
    # We retrieve the values of each variable
    plot_vars = AxisId(
        x=request.GET.get("x", default=None),
        y=request.GET.get("y", default=None),
        z=request.GET.get("z", default=None),
        p=request.GET.get("p", default=None),
    )
    filter_string = request.GET.get("f", default=None)

    display = PlotsOutput(filter_string, plot_vars)

    if display.filter_syntax_error:
        # If ever an error occurred

        # The code for syntax error in query was chosen equal to 400 aka BAD_REQUEST.
        # It could be changed but coherently into the function getValues() in
        # histogram.js and scatter_plot.js
        return HttpResponse(
            status=HTTPStatus.BAD_REQUEST,
            reason="BAD REQUEST - syntax error in the filter string",
            content_type="application/json",
        )
    else:
        planets_values = display.get_values()

        json_data_table = json.dumps(planets_values, allow_nan=True)
        return HttpResponse(
            json_data_table,
            content_type="application/json",
        )


class PolarPlot(NewDesignBaseTemplateView):
    """
    Display a page with online polar plots representing the data of the catalog.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "polar_plot.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Polar Plots"
    description = "Analyze the data online. Simple plotting tool right in the browser."

    @method_decorator(ensure_csrf_cookie)
    def get(
        self,
        request: HttpRequest,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Deals with /polar_plot/ url by serving a page that display the (javascript
        generated) polar plot representation of the catalog.

        The data for this graph are not served by this view. They are
        served through an AJAX request as JSON object via the `polar_plot_ajax`
        view.

        Args:
            request (HttpRequest): not used.

        Returns:
            HttpResponse: page giving access to the polar plot.
        """
        # defining default_axis, and headers

        # Creating default context (now planet data are sent at loading)
        output = PolarPlotOutput(filter_string=DEFAULT_FILTER)

        # Get the interesting elements
        planets_values = output.get_values()
        parameters = output.parameters

        # Get the JSON
        json_planets_values = json.dumps(planets_values, allow_nan=True)
        json_parameters = json.dumps(parameters)

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,
                parameters=json_parameters,
                data_init=json_planets_values,
            ),
        )


@require_http_methods(["GET"])
def polar_plot_ajax(request: HttpRequest) -> HttpResponse:
    """
    Answers to AJAX request for the data of the plots displayed via the `plots` view.

    Args:
        request (HttpRequest): http request for the page. It can contain the
            following keys:

            - x (str): name of the variable to use as x axis

    Returns:
        HttpResponse: a JSON representation of the catalog as requested
            according to the request params.
    """
    x_var = request.GET.get("x", default=None)

    # Get the interesting elements
    output = PolarPlotOutput(filter_string=DEFAULT_FILTER, var=x_var)
    planets_values = output.get_values()

    # Get the JSON
    json_planets_values = json.dumps(planets_values, allow_nan=True)

    return HttpResponse(json_planets_values, content_type="application/json")
