"""All views related to download of the catalog in different formats"""

# stdlib imports
# Standard imports
import csv
from datetime import datetime
from io import BytesIO, StringIO
from typing import Any

# Django 💩 imports
from django.http import FileResponse, HttpRequest, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import View

# External imports
from astropy.io import votable as astro_votable

# First party imports
from core.outputs import CSVOutput, VOTableOutput

# Local imports
from .views_helpers import (
    build_detection_filter_from_http_param,
    build_status_filter_from_http_param,
)

CONFIRMED_PLANET_FILTER = 'planet_status="confirmed"'


def _get_date_for_file_name() -> str:
    """
    Get date for the name of a file.

    Returns:
        Date of now with the following format:
        <day>-<month>-<years>_<hour>:<minutes>:<seconds>.
    """
    return datetime.now().strftime("%d-%m-%y_%H:%M:%S")


class ExportCatalogCsv(View):
    """
    Deals with download in csv and dat format.

    - /catalog/csv
    - /catalog/dat

    Few dirty hacks here to rename column names and use different fields unlike
    in HTML output; it may be reasonable just create special list with metadata
    settings Planet.CSV_DISPLAY second the new name column was done CSV_DISPLAY
    to introduce error min and error_max. There is still some cleaning to do.
    It use the work done for votable
    """

    def _post_or_get(
        self,
        request: HttpRequest,
        variant: str,
        *_args: Any,
        **_kwargs: dict[str, Any],
    ) -> FileResponse | HttpResponseBadRequest:
        """
        Few dirty hacks here to rename column names and use different fields unlike
        in HTML output; it may be reasonable just create special list with metadata
        settings Planet.CSV_DISPLAY second the new name column was done CSV_DISPLAY
        to introduce error min and error_max. There is still some cleaning to do.
        It use the work done for votable

        Arg:
            request: http request for the page. It can contain the following keys to
                specify what should be filtered:

                - status_1: the filter #1 of PLANET_STATUSES_FILTER is activated
                - status_2: the filter #2 of PLANET_STATUSES_FILTER is activated
                - status_4: the filter #4 of PLANET_STATUSES_FILTER is activated
                - detection_#: where # is one of the value of models.DETECTION. It
                specifies if that specific detection must be filtered.
                - search (str): a search query
                - query_f (str): a filter query

            variant: determine the type of csv table returned (separator and mimetyme).
                It can only have 2 values:

                - "csv"
                - "dat"

        Returns:
            CSV-formatted catalog in HttpResponse or HttpResponseBadRequest if an
            invalid variant param was used.
        """
        # First let's handle the HTTP method params
        if request.method == "GET":
            req = request.GET
        else:
            req = request.POST

        # Set csv specificities according to variant
        if variant == "csv":
            delimiter = ","
            # See: http://www.rfc-editor.org/rfc/rfc4180.txt
            mimetype = "text/csv"
        elif variant == "dat":
            delimiter = "\t"
            # See: http://www.rfc-editor.org/rfc/rfc4180.txt
            mimetype = "text/tab-separated-values"
        else:
            return HttpResponseBadRequest(
                f"Unknown variant param value {variant} "
                "the only possible values are 'csv' and 'dat'"
            )

        # Get all the parameters needed to generate the output
        status_filter = build_status_filter_from_http_param(req)
        detection_filter = build_detection_filter_from_http_param(req)
        search = req.get("search", None) or None
        filter_string = req.get("query_f", CONFIRMED_PLANET_FILTER) or None

        # Generate the output
        csv_output = CSVOutput(
            filter_string,
            statusfilter=status_filter,
            detectionfilter=detection_filter,
            search=search,
        )

        # Creating a pseudo file and writing the CSV/DAT file in it
        csv_file = StringIO()
        writer = csv.writer(csv_file, delimiter=delimiter)

        # First the header row
        writer.writerow(csv_output.get_headers())

        # Then all the data rows
        for row in csv_output.get_data_rows():
            writer.writerow(row)

        # We need to reset the stream position in order to read it
        csv_file.seek(0)

        # Creating the file response
        response = FileResponse(
            # Calling getvalue() prevent django to stream the file line by line
            # and this is useless for us and it prevent the test to work
            csv_file.getvalue(),
        )

        # We need to set the mime type manually since the automatic one lacks precision
        response["Content-Type"] = mimetype
        response["Content-Disposition"] = (
            f"attachment; filename="
            f"exoplanet.eu_catalog_{_get_date_for_file_name()}.{variant}"
        )

        return response

    def get(
        self,
        request: HttpRequest,
        variant: str,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> FileResponse | HttpResponseBadRequest:
        """
        Support GET request for the csv and dat downloads.

        See: _post_or_get() method for arguments details.
        """
        return self._post_or_get(
            request,
            variant,
            args,
            kwargs,
        )

    @method_decorator(ensure_csrf_cookie)
    def post(
        self,
        request: HttpRequest,
        variant: str,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> FileResponse | HttpResponseBadRequest:
        """
        Support POST request for the csv and dat downloads.

        See: _post_or_get() method for arguments details.
        """
        return self._post_or_get(
            request,
            variant,
            args,
            kwargs,
        )


class ExportCatalogVOtable(View):
    """
    Deals with download in VO table format.

    Deals with :

    - /catalog/votable

    Used to deal with (but those seems not to be used any where):

    -   /catalog/conesearch
    -   /catalog/$Planet/$dataset/votable
    """

    FILENAME = f"exoplanet_catalog_{_get_date_for_file_name()}.vot"
    VOTABLE_NAME = "The Extrasolar Planet Encylopaedia catalog"
    VOTABLE_ID = "EXOPLANET_EU_CAT"
    VOTABLE_MIME = "application/x-votable+xml"

    def _post_or_get(
        self,
        request: HttpRequest,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> FileResponse:
        """
        Build and return an XML Votable file corresponding to the catalog currently
        displayed.

        Args:
            request (HttpRequest): http request for the page. It can contain the
            following keys to specify what should be filtered:

            - status_1: the filter #1 of PLANET_STATUSES_FILTER is activated
            - status_2: the filter #2 of PLANET_STATUSES_FILTER is activated
            - status_4: the filter #4 of PLANET_STATUSES_FILTER is activated
            - detection_#: where # is one of the value of models.DETECTION. It
            specifies if that specific detection must be filtered.
            - search (str): a search query
            - query_f (str): a filter query as defined in `filter_grammar.py`

        Returns:
            VOTable-formatted catalog in HttpResponse
        """
        if request.method == "GET":
            req = request.GET
        else:
            req = request.POST

        status_filter = build_status_filter_from_http_param(req)
        detection_filter = build_detection_filter_from_http_param(req)

        search = req.get("search", "") or None
        filter_string = req.get("query_f", "") or CONFIRMED_PLANET_FILTER

        votable_output = VOTableOutput(
            vo_table_desc=ExportCatalogVOtable.VOTABLE_NAME,
            vo_table_id=ExportCatalogVOtable.VOTABLE_ID,
            filter_string=filter_string,
            statusfilter=status_filter,
            detectionfilter=detection_filter,
            search=search,
        )
        votable = votable_output.get_xml()

        # Creating a pseudo file and writing the VOTable in it
        votable_file = BytesIO()
        astro_votable.writeto(votable, votable_file)
        # We need to reset the stream position in order to read it
        votable_file.seek(0)

        # Creating the file response
        response = FileResponse(
            votable_file,
            as_attachment=True,
            filename=ExportCatalogVOtable.FILENAME,
        )
        # We need to set the mime type manually since the automatic one lacks precision
        response["Content-Type"] = ExportCatalogVOtable.VOTABLE_MIME

        return response

    def get(
        self,
        request: HttpRequest,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> FileResponse:
        """
        Support GET request for the VOtable downloads.

        See: _post_or_get() method for arguments details.
        """
        return self._post_or_get(
            request,
            args,
            kwargs,
        )

    @method_decorator(ensure_csrf_cookie)
    def post(
        self,
        request: HttpRequest,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> FileResponse:
        """
        Support POST request for the VOtable downloads.

        See: _post_or_get() method for arguments details.
        """
        return self._post_or_get(
            request,
            args,
            kwargs,
        )
