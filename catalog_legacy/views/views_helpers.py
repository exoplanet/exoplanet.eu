# Standard imports
from typing import Any

# Django 💩 imports
from django.db.models import Count
from django.http import QueryDict

# First party imports
# Ignore because as the typing of the file is ignored mypy does not recognise the
# attributes of this file
from core.filter_grammar import (  # type: ignore[attr-defined]
    DetectionFilter,
    PlanetStatusFilter,
)
from core.models import PlanetDBView, PlanetDetection, Star, title_name
from core.outputs import CatalogOutput

# A list of dict describing detection type asked by the POST parameters.
DetectionDesc = list[dict[str, str | bool]]

# A dict of str keys to various types with all the needed values to render a datatable
# representation of the catalog.
DataTableConfig = dict[str, Any]


def get_complex_attr(attribute: PlanetDBView | Star, field: str) -> str | float | None:
    """
    Not a view function, but a helper for resolving arbitrary object attributes
    incl. hstore, adjacent models, etc., listed in VOTABLE_DISPLAY model
    configuration list

    Args:
        attribute (???): object to query
        field: attribute to resolve. Can be model property, hstore key,
            adjancent model property or any other computed field, etc.

    Returns:
        value of resolved attribute or None if failed
    """
    return getattr(attribute, field, None)


# TODO use this function everywhere it's possible
# TODO add some doctest
# TODO this should also return statusfilter just as
#   _build_checked_detections_from_post_param returns detectionfilter
def _build_statuses_from_http_param(http_param: QueryDict) -> DetectionDesc:
    """
    Build a description of the status filter according to the POST/GET
    parameter of a request.

    Usefull for:

    - `object_list()`
    - `json_object_list()`
    - `ExportCatalogCsv` class
    - `export_catalog_dat()`
    - `export_votable()`

    Args:
        http_param: parameters of a POST or GET request. Typically this is the content
                    of a `HttpRequest.POST` or `HttpRequest.GET`. It can contain the
                    following keys:

            - status_1: the filter #1 of PLANET_STATUSES_FILTER is activated
            - status_2: the filter #2 of PLANET_STATUSES_FILTER is activated
            - status_4: the filter #4 of PLANET_STATUSES_FILTER is activated

    Returns:
        a list of dict describing status asked by the post parameters.
    """
    statuses = []
    for name, index, _ in PlanetDBView.PLANET_STATUSES_FILTER:
        status: dict[str, str | bool] = {}
        status["name"] = name
        status["id"] = f"status_{index}"
        status["checked"] = status["id"] in http_param

        statuses.append(status)

    return statuses


def build_status_filter_from_http_param(http_param: QueryDict) -> PlanetStatusFilter:
    """
    Build a status filter according to the POST/GET parameters of a request.

    Args:
        http_param: parameters of a POST or GET request. Typically this is the content
                    of a `HttpRequest.POST` or `HttpRequest.GET`. It can contain the
                    following keys:

            - status_1: the filter #1 of PLANET_STATUSES_FILTER is activated
            - status_2: the filter #2 of PLANET_STATUSES_FILTER is activated
            - status_4: the filter #4 of PLANET_STATUSES_FILTER is activated

    Returns:
        a list of PLANET_STATUSES values.
    """
    statusfilter = []
    for _, index, planet_statuses in PlanetDBView.PLANET_STATUSES_FILTER:
        if f"status_{index}" in http_param:
            statusfilter.extend(planet_statuses)

    return statusfilter


# TODO change name to reflect that it only process planet detection
def build_detection_filter_from_http_param(http_param: QueryDict) -> DetectionFilter:
    """
    Build a detection filter according to the POST/GET parameters of a request.

    Args:
        http_param: parameters of a POST or GET request. Typically this is the content
                    of a `HttpRequest.POST` or `HttpRequest.GET`. It can
                    contain the following keys:

                    - detection_#: where # is one of the value of models.DETECTION. It
                      specifies if that specific detection must be filtered.

    Returns:
        a list of relevent PlanetDetection values.
    """
    detectionfilter = []
    for detection in PlanetDetection:
        if f"detection_{detection.value}" in http_param:
            detectionfilter.append(detection.value)

    return detectionfilter


# TODO move this utility function elsewhere
# TODO use this function everywhere it's possible
# TODO add some doctest
def _build_checked_detections_from_http_param(
    http_param: QueryDict,
) -> tuple[DetectionDesc, DetectionFilter]:
    """
    Build a description of the detections and detection filters according to
    POST/GET parameter of a request.

    Usefull for:

    - `CatalogHtml` view class
    - `ExportCatalogJsonForDatatables` view class
    - `ExportCatalogCsv` view class
    - `ExportCatalogVOtable` view class

    Args:
        http_param: parameters of a POST or GET request. Typically this
            is the content of a `HttpRequest.POST` or `HttpRequest.GET`.

    Returns:
        DetectionDesc, DetectionFilter
    """
    detections = []
    detectionfilter = []

    for detec in PlanetDetection:
        detection: dict[str, str | bool] = {}
        detection["name"] = title_name(detec)
        detection["id"] = f"detection_{detec.value}"

        if detection["id"] in http_param:
            detectionfilter.append(detec.value)
            detection["checked"] = True
        else:
            detection["checked"] = False

        detections.append(detection)

    return detections, detectionfilter


# TODO add some doctest
def build_minimal_context_for_object_list(
    http_param: QueryDict, all_fields: bool
) -> DataTableConfig:
    """
    Build a Django template context ready to use to display an object_list
    page.

    Args:
        http_param: parameters of a POST or GET request. Typically this
                    is the content of a `HttpRequest.POST` or `HttpRequest.GET`.
        all_fields: tells if all possible fields should be displayed or
                    just enough for the compact view.
    Returns:
        a dict of str keys to various types with all the needed values to render a
        datatable representation of the catalog.
    """

    # statusfilter = []
    statuses = _build_statuses_from_http_param(http_param)

    detections, _ = _build_checked_detections_from_http_param(http_param)

    filter_string = None
    if "query_f" in http_param and http_param["query_f"] != "":
        filter_string = str(http_param["query_f"])
    else:
        # TODO find a more explicit way to access the "confirmed" status
        # We retreive the first element from PlanetDBView.PLANET_STATUSES_FILTER i.e.
        # "Confirmed" status
        statuses[0]["checked"] = True

    result_headers, default_sort, filter_syntax_error = CatalogOutput.get_template_data(
        all_fields,
        filter_string,
    )

    catalog_output = CatalogOutput(all_fields, filter_string)

    stats = dict(
        planets=catalog_output.get_count(),
        multiple=catalog_output.planets.values("star_name")
        .annotate(num_planets=Count("star_name"))
        .filter(num_planets__gt=1)
        .count(),
        systems=catalog_output.planets.values("star_name")
        .annotate(num_planets=Count("star_name"))
        .filter(num_planets__gt=0)
        .count(),
    )

    return dict(
        result_headers=result_headers,
        table_data_id=f"{PlanetDBView.__name__.lower()}_list",
        syntax_error=filter_syntax_error,
        all_fields=all_fields,
        default_sort=default_sort,
        statuses=statuses,
        filterString=filter_string,  # BUG key should be in snake case
        detections=detections,
        stats=stats,
    )
