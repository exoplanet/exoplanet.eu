"""
All views needed to display the filter shorcuts.

They work by having a specialized version of the catalog html view.
"""

# Standard imports
from typing import Any
from urllib.parse import urlencode

# Django 💩 imports
from django.http import HttpRequest, HttpResponse, QueryDict
from django.shortcuts import render

# Local imports
from .catalog import CatalogHtml
from .views_helpers import build_minimal_context_for_object_list


class CatalogShortcut(CatalogHtml):
    """
    Specialized catalog preconfigured to display only certain planets.

    We simply set a specific filter to apply to the basic catalog page.
    """

    def _post_or_get(
        self,
        request: HttpRequest,
        all_fields: bool = False,
        *_args: Any,
        **kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        Overloaded method to only the planet specified by the filter.

        Keyword arguments needed:
            - `query_f` containing the exact filter string to apply.
        """
        # we need to build a query_f string to set the filter
        query_f = kwargs["query_f"]
        query_with_filter = QueryDict(query_string=urlencode({"query_f": query_f}))

        # Let's build the context
        context = build_minimal_context_for_object_list(query_with_filter, all_fields)

        # we need this to have the context of the class view
        context.update(view=self)

        return render(
            request,
            template_name=self.template_name,
            context=context,
        )
