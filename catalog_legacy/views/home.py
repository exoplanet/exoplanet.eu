"""
Everything to have a nice legacy home page.
"""

# Standard imports
from dataclasses import dataclass
from datetime import datetime
from time import gmtime
from typing import Any

# Django 💩 imports
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count, Max
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy

# First party imports
from common.views import NewDesignBaseTemplateView
from core.helpers import flatpage_last_update
from core.models import Planet, PlanetDBView, PlanetStatus, Publication, Star, WebStatus
from editorial.models import Link, Meeting, News, NewsLanguage, Research

NB_NEWS_TO_DISPLAY = 5

_LAST_UPDATE_DEFAULT = datetime(*gmtime(0)[:6])


@dataclass
class _HomeLink:
    """
    A simple utility class to represent a link in the homepage
    - the english name of the language
    - the country code as used by the flag-icon-css library
    """

    # title of the link
    title: str
    # short description of the content of the link
    desc: str
    # TODO refactor this using reverse_lazy() that prevent the circular import
    # name of the url for the target pages
    url_name: str
    # whether the link is to a flatpage or classic page
    is_flatpage: bool
    # If we have no info about a last update for a page we will tell it's epoch
    last_update: datetime | None = _LAST_UPDATE_DEFAULT


class Home(NewDesignBaseTemplateView):
    """
    Display legacy home page of the site with news and links to relevant tools.
    """

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "home.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Encyclopaedia of exoplanetary systems"
    description = (
        "This encyclopaedia provides the latest detections and data announced by "
        "professional astronomers on exoplanetary systems. It contains objects lighter "
        "than 60 masses of Jupiter, which orbit stars or are free-floating. It also "
        "provides a database on exoplanets in binary systems, a database on "
        "circumstellar disks, an exhaustive bibliography, a list of exoplanet-related "
        "meetings, and links to other resources on the subject."
    )

    # Let's build links for the home page using _HomeLink
    home_links = [
        _HomeLink(
            title="Bibliography",
            desc="Full bibliography of the catalogue",
            url_name="legacy_bibliography",
            is_flatpage=False,
        ),
        _HomeLink(
            title="Researches",
            desc="Ongoing Programmes and Future Projects",
            url_name="legacy_research",
            is_flatpage=False,
        ),
        _HomeLink(
            title="Planets in binaries",
            desc="Binary systems and exoplanets",
            url_name="planets_binary",
            is_flatpage=True,
        ),
        _HomeLink(
            title="Meetings",
            desc="Future and past meetings",
            url_name="legacy_meetings",
            is_flatpage=False,
        ),
        _HomeLink(
            title="Other sites",
            desc="Relevant professional web sites",
            url_name="legacy_sites",
            is_flatpage=False,
        ),
        _HomeLink(
            title="View of planets around us",
            desc="Polar plot of exoplanets",
            url_name="legacy_polar_plot",
            is_flatpage=False,
        ),
    ]

    def get(
        self,
        request: HttpRequest,
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> HttpResponse:
        """
        View for legacy / home page.

        Args:
            request: http request for the page.

        Returns:
            The page with the home page of the site.
        """
        # pylint: disable=no-member
        news = News.objects.filter(lang=NewsLanguage.EN).order_by("-date")[
            :NB_NEWS_TO_DISPLAY
        ]

        # Get planet statistics
        stats = (
            PlanetDBView.objects.filter(
                planet_status_string=PlanetStatus.CONFIRMED.label
            )
            .filter(status=WebStatus.ACTIVE)
            .aggregate(Count("id", distinct=True), Max("modified"))
        )

        # Update all the info about modification dates of the different pages
        for link in self.home_links:
            match link:
                case _HomeLink(title="Bibliography", is_flatpage=False):
                    publications = Publication.objects
                    if publications.count() > 0:
                        link.last_update = publications.latest("modified").modified
                case _HomeLink(title="Researches", is_flatpage=False):
                    researches = Research.objects
                    if researches.count() > 0:
                        link.last_update = researches.latest("modified").modified
                case _HomeLink(title="Meetings", is_flatpage=False):
                    meetings = Meeting.objects
                    if meetings.count() > 0:
                        link.last_update = meetings.latest("modified").modified
                case _HomeLink(title="Other sites", is_flatpage=False):
                    other_links = Link.objects
                    if other_links.count() > 0:
                        link.last_update = other_links.latest("modified").modified
                case _HomeLink(title="View of planets around us", is_flatpage=False):
                    planets = Planet.objects
                    stars = Star.objects
                    match planets.count() > 0, stars.count() > 0:
                        case True, True:
                            link.last_update = max(
                                planets.latest("modified").modified,
                                stars.latest("modified").modified,
                            )
                        case True, False:
                            link.last_update = planets.latest("modified").modified
                        case False, True:
                            link.last_update = stars.latest("modified").modified
                        case _:
                            pass
                case _HomeLink(is_flatpage=True):
                    try:
                        link.last_update = flatpage_last_update(
                            fp_url=reverse_lazy(link.url_name)
                        )
                    except ObjectDoesNotExist:
                        pass

        return render(
            request,
            template_name=self.template_name,
            context=dict(
                view=self,  # we need this to have the context of the class view
                news=news,
                global_update=stats["modified__max"],
                num_planets=stats["id__count"],
            ),
        )
