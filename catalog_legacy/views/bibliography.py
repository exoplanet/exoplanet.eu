"""
Django view for the bibliography listing page and search form.
"""

# Standard imports
import re
from calendar import monthrange
from datetime import MAXYEAR, MINYEAR, datetime
from typing import Any, cast

# Django 💩 imports
from django import forms
from django.db.models import Count, Q
from django.db.models.query import QuerySet
from django.http import Http404
from django.views.generic.list import ListView

# External imports
import dateutil.parser
import pytz
from crispy_forms.bootstrap import StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Fieldset, Layout

# First party imports
from common.views import NewDesignBase
from core.models import QUERY_NOT_HIDDEN_STATUS, Publication, PublicationType

EMPTY_LABEL = ""
MIN_DATE = datetime(MINYEAR, 1, 1, 0, 0, 0)
MAX_DATE = datetime(MAXYEAR, 12, 31, 23, 59)


def _is_not_empty(given_list: list) -> bool:
    """
    Known if a given list is empty or not.

    Args:
        given_list: Given list.

    Returns:
        True if the given list is empty, Otherwise False.
    """
    return bool(given_list)


# TODO use a better approach for the "pagination with forms" using a FormMixin
# This seems to be the most "clean solution" and would allow us to remove the stupid
# query_string recreation
# See: https://brainstorm.it/snippets/django-forms-pagination-filtered-results/
# TODO replace this using django-filter
# See: https://django-filter.readthedocs.io/en/stable/index.html
class BibliographyForm(forms.Form):
    """
    Form used for the bibliography search.
    """

    title = forms.CharField(
        label="Title",
        help_text="Title of the article (case insensitive)",
        required=False,
        widget=forms.TextInput(),
    )
    author = forms.CharField(
        label="Author",
        help_text="Name of one of the author (case insensitive)",
        required=False,
        widget=forms.TextInput(),
    )
    day_start = forms.ChoiceField(
        label=EMPTY_LABEL,
        required=False,
        widget=forms.Select(),
        initial="1",
    )
    month_start = forms.ChoiceField(
        label=EMPTY_LABEL,
        required=False,
        widget=forms.Select(),
        initial="1",
    )
    year_start = forms.ChoiceField(
        label=EMPTY_LABEL,
        required=False,
        widget=forms.Select(),
        initial="define after",
    )
    day_end = forms.ChoiceField(
        label=EMPTY_LABEL,
        required=False,
        widget=forms.Select(),
        initial="31",
    )
    month_end = forms.ChoiceField(
        label=EMPTY_LABEL,
        required=False,
        widget=forms.Select(),
        initial="12",
    )
    year_end = forms.ChoiceField(
        label=EMPTY_LABEL,
        required=False,
        widget=forms.Select(),
        initial="define after",
    )

    def _get_years_choices(self) -> list[tuple[int, int]]:
        """
        Get years choices for publications.

        Returns:
            All years with a publication that has been created or updated.
        """
        # Get all the possible values for years
        years = list(
            Publication.objects.exclude(date="")  # pylint: disable=no-member
            .values_list("date", flat=True)
            .annotate(Count("id"))
        )
        years.extend(
            list(
                Publication.objects.exclude(date="")  # pylint: disable=no-member
                .values_list("modified", flat=True)
                .annotate(Count("id"))
            )
        )

        date_years: list[datetime] = []
        for year in years:
            if isinstance(year, str):
                date_years.append(dateutil.parser.parse(year))
            else:
                date_years.append(year)

        # Take only years, remove duplicate and sort the list
        formated_years: list[int] = sorted(list(set([y.year for y in date_years])))
        return [(y, y) for y in formated_years]

    def __init__(self, *args: Any, **kwargs: dict[str, Any]) -> None:
        super().__init__(*args, **kwargs)  # type: ignore[arg-type]

        self.helper = FormHelper()
        self.helper.form_method = "get"
        self.helper.layout = Layout(
            Fieldset(
                "",
                "title",
                "author",
                Fieldset(
                    "From:",
                    Div(
                        Div("day_start", css_class="col-sm-3"),
                        Div("month_start", css_class="col-sm-3"),
                        Div("year_start", css_class="col-sm-6"),
                        css_id="start-date-field",
                        css_class="row no-gutters col-sm-12 mb-3",
                    ),
                    css_id="start-date",
                ),
                Fieldset(
                    "To:",
                    Div(
                        Div("day_end", css_class="col-sm-3"),
                        Div("month_end", css_class="col-sm-3"),
                        Div("year_end", css_class="col-sm-6"),
                        css_id="end-date-field",
                        css_class="row no-gutters col-sm-12 mb-3",
                    ),
                    css_id="end-date",
                ),
            ),
            StrictButton(
                content="Search",
                name="search_btn",
                type="submit",
                css_class="btn-primary",
            ),
        )

        # We get the minimum and maximum dates from DB and provide a default in case we
        # have no publication in DB
        default_years_choices = [
            (MIN_DATE.year, MIN_DATE.year),
            (MAX_DATE.year, MAX_DATE.year),
        ]
        years_choices = self._get_years_choices() or default_years_choices

        self.fields["year_start"].initial = str(years_choices[0][0])
        self.fields["year_end"].initial = str(years_choices[-1][0])

        # Attribute `fields` is inherite by the `Form` class and contains `fields`
        # attribute (Django == 💩)
        self.fields["year_start"].choices = years_choices  # type: ignore[attr-defined]
        self.fields["year_end"].choices = years_choices  # type: ignore[attr-defined]

        self.fields["day_start"].choices = [  # type: ignore[attr-defined]
            (x, str(x)) for x in range(1, 32)
        ]
        self.fields["day_end"].choices = [  # type: ignore[attr-defined]
            (x, str(x)) for x in range(1, 32)
        ]

        self.fields["month_start"].choices = [  # type: ignore[attr-defined]
            (x, str(x)) for x in range(1, 13)
        ]
        self.fields["month_end"].choices = [  # type: ignore[attr-defined]
            (x, str(x)) for x in range(1, 13)
        ]


def filter_id_by_date(
    id_pub_modif: list[tuple[int, datetime | None, datetime | None]],
    date_start: datetime | None,
    date_end: datetime | None,
) -> list[int]:
    """
    returns the ids that have publication date or modification date between boudaries.

    Get all publication from publication none filtered on dates.

    If we get `date_start` or `date_end` we need to filter given publication on
    those dates.

    Args:
        id_pub_modif: list of tuple containing info about publication in this order:
            - ids
            - publication date
            - modification date
        date_start: "To" date for filtration.
        date_end: "From" date for filtration.

    Returns:
        ids that have either their publication or their modification date between
        `date_start` and `date_end`.
    """
    # Early return if no sorting needed
    if date_start is None and date_end is None:
        return [the_id for the_id, _, _ in id_pub_modif]

    # We replace missing start and end by min and max values if needed
    ok_date_start = date_start if date_start is not None else MIN_DATE
    ok_date_end = date_end if date_end is not None else MAX_DATE

    # Set of ids of the publication with selected creation date
    return [
        the_id
        for the_id, pub_date, modif_dates in id_pub_modif
        if (
            (pub_date is not None and ok_date_start <= pub_date <= ok_date_end)
            or (modif_dates is not None and ok_date_start <= modif_dates <= ok_date_end)
        )
    ]


class BibliographyListView(NewDesignBase, ListView):
    """
    View for :
    - /bibliography/
    - /bibliography/all/
    - /bibliography/books/
    - /bibliography/theses/
    - /bibliography/reports/
    pages.
    """

    # ListView attributes
    paginate_by = 20
    model = Publication

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "bibliography.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Bibliography"
    description = "Full bibliography of the exoplanet catalogue."

    form: BibliographyForm | None = None

    def _get_date(self, day: str, month: str, year: str) -> datetime | None:
        """
        Get date from given day, month and years.

        If day doesn't exist for this month we take last exisiting day.
        For examples: If we give day=31, month=2, year=2023
        We take 28 for the day.

        If we don't have year return empty string.

        Args:
            day: Given day.
            month: Given month.
            years: Given year.

        Returns:
            Date of the given day, month, years or empty if year are not given.

        Examples:
            >>> blv = BibliographyListView()
            >>> date = blv._get_date('27', '10', '2023')
            >>> print(date)
            '2023-10-27 00:00:00+00:00'

            Without year
            >>> blv = BibliographyListView()
            >>> date = blv._get_date('27', '10', '')
            >>> print(date)
            None

            Month without 31
            >>> blv = BibliographyListView()
            >>> date = blv._get_date('31', '6', '2023')
            >>> print(date)
            '2023-06-30 00:00:00+00:00'

            February of a non-leap year
            >>> blv = BibliographyListView()
            >>> date = blv._get_date('29', '2', '2023')
            >>> print(date)
            '2023-02-28 00:00:00+00:00'

            February of a leap year
            >>> blv = BibliographyListView()
            >>> date = blv._get_date('29', '2', '2024')
            >>> print(date)
            '2024-02-29 00:00:00+00:00'

        """
        if not year:
            return None

        _, max_month_day = monthrange(int(year), int(month))

        return dateutil.parser.parse(
            f"{min(max_month_day, int(day))}/{month}/{year}"
        ).replace(tzinfo=pytz.timezone("UTC"))

    def _filter_on_pub_date_and_modif_date(
        self,
        all_pub_pre_filter_on_dates: QuerySet[Publication],
        date_start: datetime | None,
        date_end: datetime | None,
    ) -> QuerySet[Publication]:
        """
        returns the publication whose date or modification date are between boudaries.

        Because django can't filter on an annotation we need to make a pure python
        filtration.

        This function will be overtaken when we pass all string `publication.date` in a
        real date field in the database.

        Args:
            all_pub_pre_filter_on_dates: Publication before filtration on dates.
            date_start: "To" date for filtration.
            date_end: "From" date for filtration.

        Returns:
            Given publication with the 2 given date if the are not None.
        """
        id_pub_modif = [
            (
                cast(int, pub.id),  # type: ignore[attr-defined]
                pub.date_as_datetime,
                cast(datetime | None, pub.modified),
            )
            for pub in all_pub_pre_filter_on_dates
        ]

        filtered_ids = filter_id_by_date(
            id_pub_modif,
            date_start,
            date_end,
        )

        # pylint: disable-next=no-member
        return Publication.objects.order_by("-modified").filter(id__in=filtered_ids)

    def _get_query_string_without_page_nb(self) -> str:
        """
        Retrieve the qhery string used in the request without the page part.

        Idea taken from: https://stackoverflow.com/a/69186214/20450444
        """
        query_string = self.request.META.get("QUERY_STRING", "")

        # Get all queries excluding pages from the request's meta
        query_string_no_page = "&".join(
            [x for x in re.findall(r"(\w*=\w{1,})", query_string) if "page=" not in x],
        )
        # Avoid passing the query path to template if no search result is found using
        # the previous query
        return f"&{query_string_no_page.lower()}" if query_string_no_page else ""

    def get_queryset(self) -> QuerySet[Publication]:
        """
        Get the query set of data to display according to the filter form in the page.

        Args passed via self.kwargs from urls.py:
            section: specify if we want all publication or just a specific subset.
                    Possible values are:

                - 'all': no filter
                - 'books': only books
                - 'reports': only reports
                - 'theses': only thesis

        Returns:
            A querySet of Publications selected according to the filter form in the page

        Raises:
            Http404: if section is not valid.
        """
        # BUG "theses" is a french term and we should use the "thesis" english terms
        section_queries = dict(
            books=Q(type=PublicationType.BOOK),
            theses=Q(type=PublicationType.THESIS),
            reports=Q(type=PublicationType.REPORT),
            all=Q(),
        )

        # Parameter passed from the urls.py
        section = self.kwargs.get("section", "all")

        try:
            q_section = section_queries[section]
        except KeyError as err:
            raise Http404(
                f"There is no {section} section in the bibliography."
            ) from err

        # Obtaining the form data
        self.form = (
            BibliographyForm(self.request.GET)
            if self.request.GET
            else BibliographyForm()
        )

        if self.form.is_valid():
            # Retrieve the form params
            title = self.form.cleaned_data["title"]
            author = self.form.cleaned_data["author"]
            day_start = self.form.cleaned_data["day_start"]
            month_start = self.form.cleaned_data["month_start"]
            year_start = self.form.cleaned_data["year_start"]
            day_end = self.form.cleaned_data["day_end"]
            month_end = self.form.cleaned_data["month_end"]
            year_end = self.form.cleaned_data["year_end"]

            # Set start date
            date_start = self._get_date(day_start, month_start, year_start)
            # Set end date
            date_end = self._get_date(day_end, month_end, year_end)

        else:
            title = ""
            author = ""
            day_start = ""
            month_start = ""
            year_start = ""
            day_end = ""
            month_end = ""
            year_end = ""

            # Set start date
            date_start = None
            # Set end date
            date_end = None

        # Filter according to form params
        q_title = Q(title__icontains=title) if title else Q()
        q_author = Q(author__icontains=author) if author else Q()
        q_status = QUERY_NOT_HIDDEN_STATUS

        # Let's get the actual publication to display using pagination
        # Ignore because it's a bug of django-stubs. `get_queryset()` return a QuerySet
        # but django-stubs type it with `_SupportPagination[Any]`.
        # TODO: Remove type: ignore when the bug is fixed.
        # https://github.com/typeddjango/django-stubs/issues/1928
        all_publications_before_filter_on_dates = (
            super()  # type: ignore[attr-defined]
            .get_queryset()
            .order_by("-modified")
            .filter(
                q_section
                & q_title
                & q_author
                & q_status
                & q_section
                & q_title
                & q_author
                & q_status
            )
        )

        return self._filter_on_pub_date_and_modif_date(
            all_publications_before_filter_on_dates,
            date_start,
            date_end,
        )

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Add some custom data to the context of the page.

        Args passed via self.kwargs from urls.py:
            section: specify if we want all publication or just a specific subset.
                    Possible values are:

                - 'all': no filter
                - 'books': only books
                - 'reports': only reports
                - 'theses': only thesis
        """
        context = super().get_context_data(**kwargs)

        # Parameter passed from the urls.py
        section = self.kwargs.get("section", "all")

        context.update(
            {
                "view": self,
                "form": self.form,
                "section": section,
                "query_string": self._get_query_string_without_page_nb(),
            }
        )

        return context
