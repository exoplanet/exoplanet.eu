"""Views for the catalog_legacy app."""

# Local imports
from .bibliography import BibliographyListView  # noqa: F401
from .catalog import CatalogHtml, ExportCatalogJsonForDatatables  # noqa: F401
from .export import ExportCatalogCsv, ExportCatalogVOtable  # noqa: F401
from .home import Home  # noqa: F401
from .links import LinkListView, sites  # noqa: F401
from .meetings import FutureMeetingListView, PastMeetingListView  # noqa: F401
from .news import NewsDetail, NewsFeed, NewsListView  # noqa: F401
from .planet_detail import PlanetDetail, PlanetOldUrl  # noqa: F401
from .plots import Plots, PolarPlot, plots_request_ajax, polar_plot_ajax  # noqa: F401
from .research import ResearchListView  # noqa: F401
from .shortcuts import CatalogShortcut  # noqa: F401
from .shortcuts_def import ALL_SHORTCUTS  # noqa: F401
from .views_helpers import get_complex_attr  # noqa: F401
