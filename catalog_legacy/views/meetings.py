"""Views to display the meetings."""

# Standard imports
import datetime

# Django 💩 imports
from django.db.models.query import QuerySet
from django.views.generic.list import ListView

# First party imports
from common.views import NewDesignBase
from core.models import QUERY_NOT_HIDDEN_STATUS
from editorial.models import Meeting


class FutureMeetingListView(NewDesignBase, ListView):
    """
    View for /meeting/future/ page.
    """

    # ListView attributes
    paginate_by = 50
    model = Meeting
    ordering = "begin"

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "meetings.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Meetings"
    description = "Future meetings about extrasolar planets."

    def get_queryset(self) -> QuerySet[Meeting]:
        all_meetings = super().get_queryset()

        # Future and pas event refer to one week ago
        one_week_ago = datetime.datetime.today() - datetime.timedelta(days=7)
        # Ignore because it's a bug of django-stubs. `get_queryset()` return a QuerySet
        # but django-stubs type it with `_SupportPagination[Any]`.
        # TODO: Remove type: ignore when the bug is fixed.
        # https://github.com/typeddjango/django-stubs/issues/1928
        return all_meetings.filter(  # type: ignore[attr-defined, no-any-return]
            QUERY_NOT_HIDDEN_STATUS,
            end__gte=one_week_ago,
        )


class PastMeetingListView(NewDesignBase, ListView):
    """
    View for /meeting/past/ page.
    """

    # ListView attributes
    paginate_by = 50
    model = Meeting
    ordering = "-begin"

    # Standards attributes ####################################################
    # See:
    # https://docs.djangoproject.com/en/3.0/topics/class-based-views/generic-display/
    template_name = "meetings.html"

    # Custom attributes #######################################################
    # They are accessible in the template via the "view" parameter that
    # is always available from the template through the context.
    # See: https://docs.djangoproject.com/en/3.0/ref/class-based-views/mixins-simple/

    title = "Meetings"
    description = "Past meetings about extrasolar planets."

    def get_queryset(self) -> QuerySet[Meeting]:
        all_meetings = super().get_queryset()

        # Future and pas event refer to one week ago
        one_week_ago = datetime.datetime.today() - datetime.timedelta(days=7)

        # Ignore because it's a bug of django-stubs. `get_queryset()` return a QuerySet
        # but django-stubs type it with `_SupportPagination[Any]`.
        # TODO: Remove type: ignore when the bug is fixed.
        # https://github.com/typeddjango/django-stubs/issues/1928
        return all_meetings.filter(  # type: ignore[attr-defined, no-any-return]
            end__lte=one_week_ago,
        )
