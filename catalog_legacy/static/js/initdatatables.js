/*
Initialisation of many JS libraries used by the lengacy exoplanet.eu catalog:

- datatables

Original author: Cédric Chauvin
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* global $, Cookies */

$(() => {
	// DEPRECATED: sexagesimal data-type is not used anymore
	const sexagesimalColumnIndice = []
	$("thead th[data-type='sexagesimal']").each((index, _) => sexagesimalColumnIndice.push(index))

	const numberColumnIndice = []
	$("thead th[data-type='number']").each((index, _) => numberColumnIndice.push(index))

	const stringColumnIndice = []
	$("thead th[data-type='string']").each((index, _) => stringColumnIndice.push(index))

	const noSortColumnIndice = []
	$("thead th[data-type='nosort']").each((index, _) => noSortColumnIndice.push(index))

	const noSearchColumnIndice = []
	$("thead th")
		.not(".search")
		.each((index, _) => noSearchColumnIndice.push(index))

	const massIndice = $("thead th#mass").index()
	const allMassesFlagMSinIIndice = $("thead th#all_masses_flag_msini").index()

	const defaultSortIndex = $(".catalog").attr("default_sort")

	$(".dropdown_checkboxes").hide()

	// Retrieve the correct csrf token from the enclosing page using js-cookie lib
	const csrfToken = Cookies.get("csrftoken")

	// Build the whole datatables
	const datatable = $("#data").dataTable({
		bDeferRender: true,
		sAjaxSource: "json/",
		sServerMethod: "POST",
		sPaginationType: "full_numbers",
		bServerSide: true,
		iDisplayLength: 100,
		aLengthMenu: [10, 50, 100, 500, 1000],
		aaSorting: [[defaultSortIndex, "desc"]],
		sScrollY: "85%",
		scrollCollapse: true,
		scrollX: true,
		bSearchable: false,
		dom: 'C<"clear">lfrtip',

		aoColumnDefs: [
			{
				sType: "number",
				aTargets: numberColumnIndice,
			},
			{
				sType: "sexagesimal",
				aTargets: sexagesimalColumnIndice,
			},
			{
				sType: "string",
				aTargets: stringColumnIndice,
			},
			{
				bSortable: true,
				aTargets: noSortColumnIndice,
			},
			{
				aaData: null,
				sDefaultContent: "\u2014",
				aTargets: ["_all"],
			},
			{
				bSearchable: false,
				aTargets: noSearchColumnIndice,
			},
			{
				sClass: "name",
				aTargets: [0],
			},
			{
				bVisible: false,
				aTargets: [allMassesFlagMSinIIndice],
			},
		],
		fnServerParams: (aoData) => {
			aoData.push({ name: "mass_unit", value: $("th#mass").attr("unit") })
			aoData.push({ name: "mass_sini_unit", value: $("th#mass_sini").attr("unit") })
			aoData.push({ name: "radius_unit", value: $("th#radius").attr("unit") })
			aoData.push({ name: "query_f", value: $("#query_f").val() })
			$("input:checked.filter_checkbox").each((_, elem) => aoData.push({ name: $(elem).attr("name"), value: "on" }))
		},
		fnServerData: (sSource, aoData, fnCallback) => {
			$.post({
				url: sSource,
				data: aoData,
				success: (json) => {
					fnCallback(json)
					if (!$.isEmptyObject(json.stats)) {
						$("#stats").attr("status", "active")
						$("#stats").text(
							`Showing ${json.stats.planets} planets / ${json.stats.systems} planetary systems / ${json.stats.multiple} multiple planet systems `,
						)
					}
				},
				// Handle the django csrf token by providing it in every ajax request
				headers: { "X-CSRFToken": csrfToken },
			})
		},
		oLanguage: { sSearch: "Object Search" },
		fnRowCallback: (nRow, aData, _iDisplayIndex, _iDisplayIndexFull) => {
			$(nRow).attr("data-name", aData["data-name"])
			$(nRow).attr("data-coords", aData["data-coords"])
			if (aData[allMassesFlagMSinIIndice]) {
				$("td", nRow).eq(massIndice).addClass("mass_sini_warning")
				$("td", nRow).eq(massIndice).attr("title", "Calculated from Mass*sin(i)")
			}
		},
		oColVis: {
			sAlign: "right",
			showAll: "Show all",
			showNone: "Show none",
			order: "alpha",
			aiExclude: [allMassesFlagMSinIIndice],
		},
	})

	$(".change_view").click(() => $(".form_change_view").submit())

	$(".mass_link").click(function (event) {
		if ($(this).parent().attr("unit") === "mjup") {
			displayMassEarth($(this))
		} else {
			displayMassJup($(this))
		}
		event.stopPropagation()

		return false
	})

	$("#radius_link").click(function (event) {
		if ($(this).parent().attr("unit") === "rjup") {
			displayRadiusEarth($(this))
		} else {
			displayRadiusJup($(this))
		}
		event.stopPropagation()

		return false
	})

	function displayMassJup(object) {
		object.parent().attr("unit", "mjup")
		datatable.fnDraw()
		object.html("(M<sub>Jup</sub>)")
	}

	function displayMassEarth(object) {
		object.parent().attr("unit", "mearth")
		datatable.fnDraw()
		object.html("(M<sub>Earth</sub>)")
	}

	function displayRadiusJup(object) {
		object.parent().attr("unit", "rjup")
		datatable.fnDraw()
		object.html("(R<sub>Jup</sub>)")
	}

	function displayRadiusEarth(object) {
		object.parent().attr("unit", "rearth")
		datatable.fnDraw()
		object.html("(R<sub>Earth</sub>)")
	}

	$(".dropdown_checkboxes").hide()

	$(".filter_dropdown").click(function (event) {
		const multiselect = $(this).parent().find(".dropdown_checkboxes")
		// If you open a multiselect, you close the others
		if (!multiselect.is(":visible")) {
			$(".dropdown_checkboxes").hide()
			multiselect.show()
		} else {
			multiselect.toggle()
		}
		event.stopPropagation()
	})

	$(document).click(() => {
		$(".dropdown_checkboxes").hide()
	})

	$(".dropdown_checkboxes ul li").hover(
		function () {
			$(this).addClass("dropdown_checkboxes_hover")
		},
		function () {
			$(this).removeClass("dropdown_checkboxes_hover")
		},
	)

	$(".dropdown_checkboxes ul li input").click((event) => {
		event.stopPropagation()
	})

	$(".dropdown_checkboxes ul li, .dropdown_checkboxes ul li input").click(function (event) {
		const checkbox = $(this).find("input")
		checkbox.prop("checked", !checkbox.prop("checked"))
		datatable.fnDraw()
		event.stopPropagation()
	})

	$(".export, .export_votable").click(function () {
		const search = datatable.DataTable().search()
		$("#search_input").attr("value", search)
		$("#catalog_filter_form").attr("action", $(this).attr("base_url"))
		$("#catalog_filter_form").attr("method", "post")
		$("#catalog_filter_form").submit()
		$("#catalog_filter_form").attr("action", "")

		return false
	})
})
