/*
Exported function are added to the global scope using the "adding to window" trick.
Initialy this script was just bleeding its local var to the global scope and this was
considered an "export". At least the new solution is cleaner...
See: https://howtodoinjava.com/javascript/javascript-correct-way-to-define-global-variables/
*/

/* global $ */
;(() => {
	window.showArticle = (objId) => {
		const href = $(`#${objId} > a:last`).attr("href")
		window.open(href)
		return false
	}

	window.showAllPubs = (field, value) => {
		const labelId = `#${field}_label`
		const valueId = `#${field}_${value}`
		const otherPubClass = `.other_pub_${field}_${value}`
		const togglePubsId = `#toggle_pubs_${field}_${value}`
		let pubsAdded = 0
		$(otherPubClass).each(function (_index, _value) {
			$(this).show()
			pubsAdded++
		})

		let attr = parseInt($(labelId).attr("rowspan"))
		$(labelId).attr("rowspan", attr + pubsAdded)

		attr = parseInt($(valueId).attr("rowspan"))
		$(valueId).attr("rowspan", attr + pubsAdded)

		$(togglePubsId).html("-")
		$(togglePubsId).attr("onclick", `showOnePub('${field}','${value}');return false;`)
	}

	window.showOnePub = (field, value) => {
		const labelId = `#${field}_label`
		const valueId = `#${field}_${value}`
		const otherPubClass = `.other_pub_${field}_${value}`
		const togglePubsId = `#toggle_pubs_${field}_${value}`
		let pubsRemoved = 0
		$(otherPubClass).each(function (_index, _value) {
			$(this).hide()
			pubsRemoved++
		})

		let attr = parseInt($(labelId).attr("rowspan"))
		$(labelId).attr("rowspan", attr - pubsRemoved)

		attr = parseInt($(valueId).attr("rowspan"))
		$(valueId).attr("rowspan", attr - pubsRemoved)

		$(togglePubsId).html("+")
		$(togglePubsId).attr("onclick", `showAllPubs('${field}','${value}');return false;`)
	}

	window.selectStar = (starName) => {
		$("table.star").each(function (_index, _value) {
			if ($(this).attr("id") === `table_${starName}`) {
				$(this).show()
			} else {
				$(this).hide()
			}
		})
		$("div.star_div").each(function (_index, _value) {
			if ($(this).attr("id") === `div_${starName}`) {
				$(this).show()
			} else {
				$(this).hide()
			}
		})
		$(".star_menu_item").each(function (_index, _value) {
			if ($(this).attr("id") === `select_${starName}`) {
				$(this).attr("class", "star_menu_item star_menu_item_act")
			} else {
				$(this).attr("class", "star_menu_item")
			}
		})
	}

	$(() => {
		$(".inline_input_icon_2").click(() => {
			$("#help_window").toggle()
		})
		$("#toggle_temperature_parameters").click(function () {
			$(this).find(".toggle_temperature_img").toggleClass("toggle_img_hide")
			$(".temperature_parameter").toggle()
			return false
		})

		$("#toggle_molecule_parameters").click(function () {
			$(this).find(".toggle_molecule_img").toggleClass("toggle_img_hide")
			$(".molecules_parameters").toggle()
			return false
		})

		$(".showPlanetsInSystem").click(function () {
			const form = $(this).parent().find("form")
			form.submit()
			return false
		})
	})
})()
