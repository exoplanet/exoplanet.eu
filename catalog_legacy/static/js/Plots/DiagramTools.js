/*
Tools needed to display the plots (formerly diagrams).

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* global d3, $, saveAs */

/* Encode or decode with rfc3896 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURI#encoding_for_rfc3986 */
function decodeRFC3986URI(str) {
	return decodeURI(str)
		.replace(/%5B/g, "[")
		.replace(/%5D/g, "]")
		.replace(/%2B/g, "+")
		.replace((c) => `%${c.charCodeAt(0).toString(16).toUpperCase()}`, /[!'()*]/g)
}
// TODO: Ajouter doce sur encode decode

this.DiagramTools = {}
;((exports) => {
	// Value passed from HTML via data attribute from the script element
	// See: https://stackoverflow.com/a/15069289
	// And: https://developer.mozilla.org/fr/docs/Apprendre/HTML/Comment/Utiliser_attributs_donnes
	const urilogo = document.currentScript.dataset.urilogo

	exports.UnpackData = (dPack) => {
		/* Extract data sent by server from a json of type hpack(0)
    to array of objects
    */
		const dUnpack = []
		// fields of variables given by ajax (JSON packed)
		const field = dPack[0]
		dPack.forEach((d, i) => {
			if (d != null && i > 0) {
				// here removing null lines
				const local = {}
				// here repacking data
				field.forEach((f, j) => {
					local[f] = d[j]
				})
				local.n = decodeRFC3986URI(local.n)
				dUnpack.push(local)
			}
		})
		return dUnpack
	}

	const superscript = "⁰¹²³⁴⁵⁶⁷⁸⁹⁻"
	function formatPower(d) {
		return `${d}`
			.split("")
			.map((c) => (c === "-" ? superscript[10] : superscript[c]))
			.join("")
	}

	function formatTick(d) {
		return 10 + formatPower(Math.round(Math.log10(d)))
	}

	function formatTickPolar(d) {
		return 10 + formatPower(d)
	}

	/*
    Set format for strings
   */
	// TODO Find another way to to this: String prototype is in fact read only!
	if (!String.prototype.format) {
		String.prototype.format = function (...args) {
			return this.replace(/{(\d+)}/g, (match, number) => (typeof args[number] !== "undefined" ? args[number] : match))
		}
	}

	/*
    Add Logo to something
   */
	function AddLogoAndDate(where, position, size) {
		const newPosition = position || [-9, -12]
		newSize = size || [20, 24]

		// get dimension of the plot
		const gwidth = document.getElementById(where).getElementsByTagName("rect")[0].getBBox().width

		const today = new Date()

		const dd = today.getDate().toString().padStart(2, 0)
		const mm = (today.getMonth() + 1).toString().padStart(2, 0) // January is 0!
		const yyyy = today.getFullYear().toString().padStart(4, 0)

		const todayStr = `exoplanet.eu, ${yyyy}-${mm}-${dd}`

		const selectedFig = d3.select(`#${where}`).append("g").attr("id", "LogoDateId")

		selectedFig
			.append("svg:image")
			.attr("id", "logoImg")
			.attr("x", newPosition[0] - newSize[0])
			.attr("y", newPosition[1] - newSize[1] / 2)
			.attr("width", newSize[0])
			.attr("height", newSize[1])
			.attr("xlink:href", urilogo)

		if (gwidth < 190) {
			return
		} // else{console.log(gwidth)}

		selectedFig
			.append("text")
			.attr("text-anchor", "end")
			.attr("x", newPosition[0] - newSize[0] - 1)
			.attr("y", newPosition[1] + 5)
			.text(todayStr)
			.attr("fill", "black")
	}

	/*
    Class for Grid
   */
	function Grid(status) {
		let display = ""
		if (status) {
			this.status = status
			display = "block"
		} else {
			this.status = false
			display = "none"
		}

		this.init = function (vis, options) {
			this.vis = vis
			this.X = null
			this.Y = null
			this.width = null
			this.height = null
			this.padding = { l: 0, t: 0, r: 0, b: 0 }

			for (const ii in options) {
				this[ii] = options[ii]
			}

			this.myx = this.vis
				.append("g")
				.attr("class", "grid x")
				.attr("transform", `translate(0,${this.height - this.padding.b})`)
				.attr("display", display)
				.call(
					makeXAxis()
						.tickSize(-this.height + this.padding.b + this.padding.t, 0)
						.tickFormat(""),
				)
			this.myy = vis
				.append("g")
				.attr("class", "grid y")
				.attr("display", display)
				.attr("transform", `translate(${this.padding.l},0)`)
				.call(
					makeYAxis()
						.tickSize(-this.width + this.padding.r + this.padding.l, 0)
						.tickFormat(""),
				)
			this.move()
			this.status = !this.status
			this.toggle()
		}
		const makeXAxis = () => d3.svg.axis().scale(this.X).orient("bottom").ticks(5)

		const makeYAxis = () => d3.svg.axis().scale(this.Y).orient("left").ticks(5)

		this.move = function () {
			this.myx.call(makeXAxis().tickSize(-this.height, 0, 0).tickFormat(""))
			this.myy.call(makeYAxis().tickSize(-this.width, 0, 0).tickFormat(""))
		}

		this.toggle = function () {
			if (this.status) {
				$(".grid").fadeOut(500)
			} else {
				$(".grid").fadeIn(500)
			}
			this.status = !this.status
		}

		this.AddSelector = (where) => {
			const btn = document.getElementById(where)
			btn.parentNode.title = "Set grid on plot"
			btn.checked = this.status
			btn.onclick = () => {
				this.toggle()
			}
		}
	}

	/* Class to swap all elements horizontally */
	function HorizFlip(ID, buttonWhere, options) {
		this.container = ID
		this.where = buttonWhere
		for (const ii in options) {
			this[ii] = options[ii]
		}

		this.curr_pos = localStorage.getItem("curr_pos")

		this.curr_pos = this.curr_pos === "row" ? "row" : "row-reverse"

		// initialization
		document.getElementById(this.container).style.display = "flex"
		document.getElementById(this.container).style.flexDirection = this.curr_pos

		const swapDom = () => {
			const container = document.getElementById(this.container)

			if (this.curr_pos === "row" || window.innerWidth < 1000) {
				this.curr_pos = "row-reverse"
			} else {
				this.curr_pos = "row"
			}
			container.style.flexDirection = this.curr_pos
			localStorage.setItem("curr_pos", this.curr_pos)
		}

		if (this.curr_pos === "row") {
			this.curr_pos = "row-reverse"
			swapDom()
		}
		document.getElementById(this.where).addEventListener("click", swapDom)
	}

	/*
    Old functions to save figure. Replace by newSaveFigure for the svg.
    The png download will be re create later
    TODO: re create the download  in png
    PS: C'est vraiment de la merde.
  */
	// function saveMenu (element) {
	//   var node = element

	//   // TODO Convert this method name to camelCase ASAP
	//   function node_off () {
	//     node.style.backgroundColor = '#009900'
	//     node.selected = false
	//     $('#imgSizeDiv').fadeOut(500)
	//   }

	//   // TODO Convert this method name to camelcase ASAP
	//   function node_on () {
	//     node.style.backgroundColor = '#007700'
	//     node.selected = true
	//     $('#imgSizeDiv').fadeIn(500)
	//   }

	//   if (node.selected === undefined) {
	//     /* Initialization at the first click */
	//     var prs = [
	//       { t: 'Types ', v: 'donothing' },
	//       { t: '840 x 600 px', v: 1 },
	//       { t: '4200 x 3000 px', v: 5 },
	//       { t: '8400 x 6000 px', v: 10 },
	//       { t: 'Vector format', v: 'svgimage' }
	//     ]

	//     var parentNode = element.parentNode
	//     var div = d3
	//       .select(parentNode)
	//       .append('div')
	//       .attr('id', 'imgSizeDiv')

	//     div.append('label').text('Select type')

	//     var select = div.append('select')
	//       .attr('class', 'input_text')
	//       .attr('id', 'imgSize')
	//       .on('change', function (_d) {
	//         var text = this.options[this.selectedIndex].innerHTML
	//         text = text.replace(/ /g, '')
	//         if (this.value === 'svgimage') {
	//           saveaspng(this.value, 'exoplanet-eu.svg', 'svgimage')
	//         } else {
	//           saveaspng(this.value, 'exo_' + text + '.png')
	//         }
	//         node_off()
	//         this.value = prs[0].v
	//       })

	//     select.selectAll('option')
	//       .data(prs).enter()
	//       .append('option')
	//       .attr('value', function (d) { return d.v })
	//       .text(function (d) { return d.t })
	//   }

	//   if (node.selected) {
	//     node_off()
	//   } else {
	//     node_on()
	//   }
	// }

	// function saveaspng (pr, imgName, typeSave) {
	//   try {
	//     // Check if we can create a blob in order to save something
	//     var isFileSaverSupported = !!new Blob()
	//   } catch (e) {
	//     alert('blob not supported')
	//   }

	//   // set the type of image to save svgimage or pngimage
	//   typeSave = typeSave || 'pngimage'

	//   // pr is the Precision Ratio with respect the normal size
	//   var svg = d3.select('#mainrect')
	//   var width = svg.attr('width')
	//   var height = svg.attr('height')

	//   imgName = imgName || 'exoplanet_eu.png'

	//   // Set high resolution svg aspect ratio (precision ratio)
	//   pr = pr || 1

	//   if (pr === 'donothing') return

	//   // get grid status
	//   var gridColor = d3.select('.grid').select('line').style('stroke')
	//   var gridOpac = d3.selectAll('.grid').select('line').style('opacity')

	//   // This function clone a d3 element recursively
	//   function cloneAll (selector, outid) {
	//     var nodes = d3.selectAll(selector)
	//     nodes.each(function (d, i) {
	//       nodes[0][i] = this.parentNode.insertBefore(this.cloneNode(true),
	//         this.nextSibling)
	//     })
	//     return nodes.attr('id', outid)
	//   }

	//   var a = cloneAll('#mainrect', 'mainrectCopy')

	//   // Setting svg properties because css style file is not taken into account
	//   a.select('#plot').select('rect').style('fill', 'none')
	//   a.selectAll('text').style('stroke', 'none').attr('font-size', 10).attr('font-weight', 'normal')
	//   // a.selectAll(".label.p").attr("text-anchor","middle");
	//   a.selectAll('.axis.label').attr('font-size', 15).attr('text-anchor', 'middle')
	//   a.selectAll('.tick').selectAll('line').style({
	//     stroke: 'black', fill: 'none', 'stroke-width': '1px', 'shape-rendering': 'geometricPrecision'
	//   })
	//   a.selectAll('.domain').style('stroke', 'black').style('fill', 'none').style('stroke-width', '1px')
	//   a.selectAll('.grid').selectAll('line').style({
	//     stroke: gridColor, opacity: gridOpac, 'shape-rendering': 'geometricPrecision', 'stroke-dasharray': 3
	//   })
	//   /* selection mode planets */
	//   a.selectAll('path.selected').style({
	//     stroke: '#009900',
	//     'stroke-width': 1,
	//     fill: 'blue',
	//     opacity: 0.2,
	//     'stroke-dasharray': 5
	//   })

	//   a.selectAll('.ManualZoom').remove()
	//   /* removing logo image from figure: it has to be added to the canvas */
	//   a.select('#logoImg').remove()
	//   a.select('#LogoDateId').selectAll('text').attr('font-size', 13)
	//   a.selectAll('a').selectAll('title').remove()

	//   // Exoris
	//   a.selectAll('g.tooltip').remove()
	//   const exoleg = a.select('#exoris_lat_legend')
	//   if (!exoleg.empty) {
	//     if (exoleg.style('opacity') === '0') {
	//       exoleg.selectAll('g.lat_legend').remove()
	//     }
	//     exoleg.selectAll('g.lat_legend > text').style('font-size', 15)
	//   }

	//   svg = document.getElementById('mainrectCopy')

	//   var labels = svg.getElementsByClassName('label')
	//   for (var ii = 0; ii < labels.length; ii++) {
	//     var link = labels[ii].getElementsByClassName('linkunit')[0]
	//     if (link !== undefined) {
	//       var unit = link.innerHTML
	//       labels[ii].removeChild(link)
	//       labels[ii].innerHTML += unit
	//     }
	//   }

	//   if (typeSave === 'pngimage') {
	//     // Set high resolution svg aspect ratio (precision ratio)
	//     a.attr('width', width * pr).attr('height', height * pr)
	//     a.select('#plot').attr('transform', `translate(${pr * 70},${pr * 20})scale(${pr},${pr})`)

	//     var str = new XMLSerializer().serializeToString(svg)

	//     a.remove()

	//     d3.select('body').append('canvas')
	//       .attr('id', 'canvas')
	//       // .style("display","none")

	//     // Transformation
	//     canvg.Canvg.fromString('canvas', str, { ignoreMouse: true })

	//     var canvas = document.getElementById('canvas')

	//     /* adding logo to canvas */
	//     AddImageToCanvas(canvas, [(width - 125) * pr, 42 * pr], [24 * pr, 12 * pr])

	//     // draw to canvas...
	//     canvas.toBlob(function (blob) {
	//       saveAs(blob, imgName)
	//     })

	//     d3.select('#canvas').remove()
	//   } else {
	//     // svgimage

	//     a.select('#plot').attr('transform', `translate(${70},${50})scale(${1},${1})`)

	//     a.attr('width', width + 70)
	//       .attr('height', height + 50)
	//       .attr('title', 'test2')
	//       .attr('version', 1.1)
	//       .attr('xmlns', 'http://www.w3.org/2000/svg')

	//     var html = d3.select('#mainrectCopy').node().outerHTML

	//     var blob = new Blob([html], { type: 'image/svg+xml' })

	//     a.remove()

	//     saveAs(blob, imgName)
	//   }
	// }

	function newSaveFigure() {
		try {
			// Check if we can create a blob in order to save something
			const isFileSaverSupported = !!new Blob()
		} catch (e) {
			alert("blob not supported")
		}

		// pr is the Precision Ratio with respect the normal size
		const svg = d3.select("#mainrect")
		const width = svg.attr("width")
		const height = svg.attr("height")

		const imgName = "exoplanet-eu.svg"

		// get grid status
		const gridColor = d3.select(".grid").select("line").style("stroke")
		const gridOpac = d3.selectAll(".grid").select("line").style("opacity")

		// This function clone a d3 element recursively
		function cloneAll(selector, outid) {
			const nodes = d3.selectAll(selector)
			nodes.each(function (d, i) {
				nodes[0][i] = this.parentNode.insertBefore(this.cloneNode(true), this.nextSibling)
			})
			return nodes.attr("id", outid)
		}

		const clonedSvg = cloneAll("#mainrect", "mainrectCopy")

		// Setting svg properties because css style file is not taken into account
		clonedSvg.select("#plot").select("rect").style("fill", "none")
		clonedSvg.selectAll("text").style("stroke", "none").attr("font-size", 10).attr("font-weight", "normal")
		clonedSvg.selectAll(".axis.label").attr("font-size", 15).attr("text-anchor", "middle")
		clonedSvg.selectAll(".tick").selectAll("line").style({
			stroke: "black",
			fill: "none",
			"stroke-width": "1px",
			"shape-rendering": "geometricPrecision",
		})
		clonedSvg.selectAll(".domain").style("stroke", "black").style("fill", "none").style("stroke-width", "1px")
		clonedSvg.selectAll(".grid").selectAll("line").style({
			stroke: gridColor,
			opacity: gridOpac,
			"shape-rendering": "geometricPrecision",
			"stroke-dasharray": 3,
		})
		/* selection mode planets */
		clonedSvg.selectAll("path.selected").style({
			stroke: "#009900",
			"stroke-width": 1,
			fill: "blue",
			opacity: 0.2,
			"stroke-dasharray": 5,
		})

		clonedSvg.selectAll(".ManualZoom").remove()
		/* removing logo image from figure: it has to be added to the canvas */
		clonedSvg.select("#logoImg").remove()
		clonedSvg.select("#LogoDateId").selectAll("text").attr("font-size", 13)
		clonedSvg.selectAll("a").selectAll("title").remove()

		const newSvg = document.getElementById("mainrectCopy")

		const labels = newSvg.getElementsByClassName("label")
		for (let ii = 0; ii < labels.length; ii++) {
			const link = labels[ii].getElementsByClassName("linkunit")[0]
			if (link !== undefined) {
				const unit = link.innerHTML
				labels[ii].removeChild(link)
				labels[ii].innerHTML += unit
			}
		}

		clonedSvg.select("#plot").attr("transform", `translate(${70},${50})scale(${1},${1})`)

		clonedSvg
			.attr("width", width + 70)
			.attr("height", height + 50)
			.attr("title", "test2")
			.attr("version", 1.1)
			.attr("xmlns", "http://www.w3.org/2000/svg")

		const html = d3.select("#mainrectCopy").node().outerHTML

		const blob = new Blob([html], { type: "image/svg+xml" })

		clonedSvg.remove()

		saveAs(blob, imgName)
	}

	/*
    Class for Timing
   */
	const Timing = function () {
		// class containing web browser
		const isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)
		const isOpera = !!window.opera || navigator.userAgent.indexOf(" OPR/") >= 0
		const isFirefox = typeof InstallTrigger !== "undefined"
		// var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)

		// if it is possible to use performance
		this.use_perfomance = isChrome || isFirefox || isOpera

		this.t0 = undefined
		this.t1 = undefined
	}

	Timing.prototype = {
		start: function () {
			if (this.use_perfomance) {
				this.t0 = performance.now()
				this.t1 = 0
			}
			return this
		},

		stop: function () {
			if (this.use_perfomance) {
				this.t1 = performance.now()
			}
			return this
		},

		print: function () {
			if (this.use_perfomance) {
				return (this.t1 - this.t0).toFixed(2)
			}
			return "no timing"
		},
	}

	function Timed(text, callback, times) {
		newTimes = times || 1
		const t = new Timing().start()

		for (let jj = 0; jj < newTimes; jj++) {
			callback()
		}

		const other = newTimes > 1 ? ` (mean on ${newTimes} launchs)` : ""
		console.log(`Timing for ${text} : ${t.stop().print() / newTimes} ms${other}`)
	}

	/* Export function */
	exports.Timing = Timing
	exports.Timed = Timed
	exports.Grid = Grid
	exports.formatTick = formatTick
	exports.formatTickPolar = formatTickPolar
	exports.AddLogoAndDate = AddLogoAndDate
	exports.HorizFlip = HorizFlip
	exports.newSaveFigure = newSaveFigure
})(this.DiagramTools)
