/*
Tool for selecting planets directly on the chart.

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* global d3, Cookies */

this.SelectionTool = {}
;((exports) => {
	// An array to hold the coordinates
	// of the line drawn on each svg.
	let coords = []
	let size = 0

	const idSVG = "#my_clip-path"
	const svg = () => d3.select(idSVG)
	const line = d3.svg.line()

	let scale

	// Set the behavior for each part
	// of the drag.
	const drag = d3.behavior
		.drag()
		.on("dragstart", () => {
			// If a selection line already exists,
			// remove it, if not shift key is pressed
			if (!d3.event.sourceEvent.shiftKey) {
				svg().selectAll(".selected").remove()
				unhighlight()
				coords = []
				size = 0
			}

			coords.push([])

			// Store the mouse's current position
			size = coords.length

			// Add a new selection line.
			svg().append("path").attr("class", "inselection")
		})
		.on("drag", function () {
			// Add point with conversion in the scale of planets
			coords[size - 1].push(d3.mouse(this).map((d, i) => scale[i].invert(d)))

			// Change the path of the selection line
			// to represent the area where the mouse
			// has been dragged.
			svg()
				.select(".inselection")
				.attr({
					d: line(coords[size - 1].map((e) => [scale[0](e[0]), scale[1](e[1])])),
				})
		})
		.on("dragend", () => {
			// If the user clicks without having
			// drawn a path, remove any paths
			// that were drawn previously.
			if (coords[size - 1].length === 0) {
				svg().selectAll(".inselection").remove()
				coords.pop()
				return
			}

			// Figure out which dots are inside the
			// drawn path and highlight them.
			const selected = []

			svg()
				.selectAll("circle.points")
				.each((d, _) => {
					const point = [d.x, d.y]

					if (pointInPolygon(point, coords[size - 1])) {
						selected.push(d.n)
					}
				})

			highlight(selected, d3.event.sourceEvent.shiftKey)

			// Draw a path between the first point
			// and the last point, to close the path.
			const sel = svg().select(".inselection")
			const locD = sel.attr("d")
			sel.attr({
				d: `${locD}Z`,
				class: "selected",
			})
		})

	const init = (X, Y) => {
		scale = [X, Y]
	}

	const update = () => {
		svg()
			.selectAll(".selected")
			.attr({
				d: (ele, i) => `${line(coords[i].map((e) => [scale[0](e[0]), scale[1](e[1])]))}Z`,
			})
	}

	// from https://github.com/substack/point-in-polygon
	function pointInPolygon(point, vs) {
		let xi
		let xj
		let yi
		let yj
		let i
		let j
		let intersect
		const x = point[0]
		const y = point[1]
		let inside = false

		for (i = 0, j = vs.length - 1; i < vs.length; j = i++) {
			xi = vs[i][0]
			yi = vs[i][1]
			xj = vs[j][0]
			yj = vs[j][1]
			intersect = yi > y !== yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi

			if (intersect) {
				inside = !inside
			}
		}
		return inside
	}

	function unhighlight() {
		d3.selectAll("circle.points").classed("highlighted", false)
	}

	function highlight(ids, shiftkey) {
		// First unhighlight all the circles.
		if (!shiftkey) {
			unhighlight()
		}

		// Find the circles that have an id
		// in the array of ids given, and
		// highlight those.
		d3.selectAll("circle.points")
			.filter((d, _) => ids.indexOf(d.n) > -1)
			.classed("highlighted", true)
	}

	function getSelected(field) {
		// Return an array containing the data associated to selected elements
		let result = d3.selectAll(".highlighted").data()

		if (field) {
			result = result.map((d) => d[field])
		}

		return result
	}

	function openPlanetsLink(base, field) {
		const csrftoken = Cookies.get("csrftoken")
		const str = `{"${getSelected(field).join('","')}"} intersect NAME`

		const form = document.createElement("form")
		form.action = base
		form.method = "POST"
		form.target = "newwin"

		const inputcsrf = document.createElement("input")
		inputcsrf.type = "hidden"
		inputcsrf.name = "csrfmiddlewaretoken"
		inputcsrf.value = csrftoken
		form.appendChild(inputcsrf)

		const input = document.createElement("textarea")
		input.name = "query_f"
		input.value = str
		form.style.display = "none"
		form.appendChild(input)
		document.body.appendChild(form)
		form.submit()
	}

	exports.init = init
	exports.update = update
	exports.drag = drag
	exports.openPlanetsLink = openPlanetsLink
})(this.SelectionTool)
