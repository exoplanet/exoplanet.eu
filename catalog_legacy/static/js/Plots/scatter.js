/*
Scatter plot drawing library.

Need some data parameters to be transmitted when calling the script in th html:

- data-datas: JSON string containing the initial data
- data-uricatalog: uri to get data from the catalog
- data-uriplotrequestajax: uri to get data for the plot
- varListArray: JSON string containing header descriptions for the data

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* eslint-env browser */
/* global d3, $, DiagramTools, TypesPlot, ManualZoom, SelectionTool, ExorisCurves, DropdownCheckboxes */

;(() => {
	/* some aliases */
	const DT = DiagramTools
	const TP = TypesPlot
	const PanZoom = ManualZoom
	const ST = SelectionTool
	// const Exo = ExorisCurves

	// Value passed from HTML via data attribute from the script element
	// See: https://stackoverflow.com/a/15069289
	// And: https://developer.mozilla.org/fr/docs/Apprendre/HTML/Comment/Utiliser_attributs_donnes
	let datas = JSON.parse(document.currentScript.dataset.datas)
	const uricatalog = document.currentScript.dataset.uricatalog
	const uriPlotRequestAjax = document.currentScript.dataset.uriplotrequestajax
	const varListArray = JSON.parse(document.currentScript.dataset.varlistarray)

	DropdownCheckboxes.init()

	// Constants
	const MARGIN = {
		top: 30,
		right: 100,
		bottom: 100,
		left: 100,
	}

	const ANIMATION_DURATION = 500 // ms
	const TIMEOUT_DURATION = 200 // ms

	const defaultColor = "#0073B0"

	const MANUAL_ZOOM_DEFAULT = false

	const DETECTION_METHOD_COLOR = {
		other: $('i.other').css('color'),
		primary_transit: $('i.primary-transit').css('color'),
		secondary_transit: $('i.secondary-transit').css('color'),
		radial_velocity: $('i.radial-velocity').css('color'),
		microlensing: $('i.microlensing').css('color'),
		imaging: $('i.imaging').css('color'),
		astrometry: $('i.astrometry').css('color'),
		ttv: $('i.ttv').css('color'),
		timing: $('i.timing').css('color'),
		kinematic: $('i.kinematic').css('color'),
	}

	// TODO get rid of those global variables
	/* common variables */
	let planetTotalNm
	let planetRepresentedNm

	function isValidNumber(num) {
		return !Number.isNaN(num) && Number.isFinite(num) && num !== undefined && num !== null
	}

	function FullfyStatistic() {
		document.getElementById("Planet_total_nm").innerHTML = planetTotalNm
		document.getElementById("Planet_represented_nm").innerHTML = planetRepresentedNm
	}

	function getColorFromDetectionMethod(planet) {
		if (planet.detection_method === undefined || planet.detection_method[0] === undefined) {
			return DETECTION_METHOD_COLOR.other
		}
		switch(planet.detection_method[0]) {
			case 6: // Primary Transit
				return DETECTION_METHOD_COLOR.primary_transit
			case 13: // Secondary Transit
				return DETECTION_METHOD_COLOR.secondary_transit
			case 1: // Radial velocity
				return DETECTION_METHOD_COLOR.radial_velocity
			case 4: // Microlensing
				return DETECTION_METHOD_COLOR.microlensing
			case 5: // Imaging
				return DETECTION_METHOD_COLOR.imaging
			case 7: // Astrometry
				return DETECTION_METHOD_COLOR.astrometry
			case 8: // TTV
				return DETECTION_METHOD_COLOR.ttv
			case 2: // Timing
				return DETECTION_METHOD_COLOR.timing
			case 15: // Kinematic
				return DETECTION_METHOD_COLOR.kinematic
			default:
				return DETECTION_METHOD_COLOR.other
		}
	}

	/*
	 * Extent of our data
	 *
	 * returns array of [minimum, maximum]
	 */
	function extent(variable, data, workVariable) {
		const mydata = data.filter((d) => d[variable] !== null)

		const result =
			mydata.length === 0
				? [0, 1]
				: mydata.length === 1
				  ? [mydata[0][variable] * 0.9, mydata[0][variable] * 1.1]
				  : d3.extent(mydata, (d) => d[variable])

		if (workVariable[variable].min !== -Infinity) {
			result[0] = workVariable[variable].min
		}

		if (workVariable[variable].max !== Infinity) {
			result[1] = workVariable[variable].max
		}

		return result
	}

	class Err {
		constructor(name) {
			this.name = name
			this.status = false
			this.opacity = 0
		}

		flip() {
			this.status = !this.status
			this.opacity = this.status ? 1 : 0
		}
	}

	class ZAxisClass {
		constructor(scatter, whereCmd, wherePlot) {
			this.scatter = scatter
			this.whereplot = wherePlot

			// This will get a new values at each redraw()
			this.zExtent = undefined
			this.zz = undefined

			const table = d3.select(`#${whereCmd}`).style("display", "none")

			/* Add color selector */
			table.append("div").attr("id", "color_selector")
			this.scatter.addColorSelector("color_selector")

			/* Add Min max selector */
			table.append("div").attr("id", "input_minmax_z").attr("class", "input_double_div")

			/* Create selection menu */
			table.append("select").attr("id", "menu_choise_z").attr("class", "input_text").attr("name", "size")

			/* Add connection to the function */
			this.scatter.addVarSelector("z")

			this.scatter.mm.z.AddMinMaxInput("input_minmax_z", ScatterPlot.prototype.redraw.bind(this.scatter))
		}

		createZAxis() {
			// Plot Z axis
			const zNbTick = 22
			const extColorDomain = [...Array(zNbTick).keys()].map(
				(tick) => this.zExtent[0] + (tick / zNbTick) * (this.zExtent[1] - this.zExtent[0]),
			)
			const gradiantColorWidth = 20
			const notAvGradiantColorHeight = 15
			const offsetFromPlot = 10
			const offsetFromGradiantColor = 25
			const spaceBetweenColor = Math.ceil((this.scatter.height - notAvGradiantColorHeight) / zNbTick)

			const myformat = d3.format(",.2g")

			// cleaning
			$("#zaxisContainer").remove()

			const vis = d3.select(`#${this.whereplot}`).append("g").attr("id", "zaxisContainer").style("display", "none")

			const gradient = vis
				.append("linearGradient")
				.attr("class", "zaxis_grad")
				.attr("x1", "0")
				.attr("y1", 0)
				.attr("x2", "0")
				.attr("y2", this.scatter.height)
				.attr("id", "gradient")
				.attr("gradientUnits", "userSpaceOnUse")
				.attr("spreadMethod", "pad")

			gradient.append("stop").attr("offset", "0").attr("stop-color", this.scatter.colorMin)

			gradient.append("stop").attr("offset", "1").attr("stop-color", this.scatter.colorMax)

			// color gradiant
			vis
				.append("rect")
				.attr("id", "colorrect")
				.attr("class", "zaxis_grad")
				.attr("x", this.scatter.width + offsetFromPlot)
				.attr("y", 0)
				.attr("width", gradiantColorWidth)
				.attr("height", this.scatter.height)
				.attr("fill", "url(#gradient)")

			vis
				.append("rect")
				.attr("class", "zaxis_grad")
				.attr("x", this.scatter.width + offsetFromPlot)
				.attr("y", this.scatter.height - notAvGradiantColorHeight)
				.attr("width", gradiantColorWidth)
				.attr("height", notAvGradiantColorHeight)
				.attr("fill", "black")

			vis
				.selectAll(".axis.z")
				.data(extColorDomain)
				.enter()
				.append("text")
				.attr("class", "axis z")
				.attr("x", this.scatter.width + offsetFromPlot + offsetFromGradiantColor)
				.attr("y", (_d, i) => +(0.5 + i) * spaceBetweenColor)
				.text((d, i) => (i % 2 ? "" : myformat(d)))

			vis
				.append("text")
				.attr("class", "axis z")
				.attr("x", this.scatter.width + offsetFromPlot + offsetFromGradiantColor)
				.attr("y", this.scatter.height)
				.text("Not Av.")

			vis
				.append("text")
				.attr("class", "axis label z")
				.attr("transform", "rotate(+90)")
				.attr("x", this.scatter.height / 2)
				.attr("y", -this.scatter.wTot + MARGIN.left + offsetFromGradiantColor)
				.text(this.scatter.mm.z.desc)
				.append("a")
				.attr("class", "linkunit")
				.attr("href", "#")
				.text(this.scatter.mm.z.axisLabelUnit())
				.on("click", () => {
					TP.refactorField(datas, this.scatter.mm.z, ScatterPlot.prototype.redraw.bind(this.scatter))
				})
				.append("title")
				.text("Change units on click if other units are available")
		}

		redraw() {
			// Let's refresh the extent
			this.zExtent = extent("z", this.scatter.reducedDatas, this.scatter.mm)

			// define the z scale (color)
			const zScale = d3.scale
				.linear()
				.domain(this.zExtent)
				.range([this.scatter.colorMin, this.scatter.colorMax])
				.interpolate(d3.interpolateLab)

			this.zz = (e) => (e.z ? zScale(e.z) : "black")
			this.detectionMethodColor = (e) => {getColorFromDetectionMethod(e)}
			this.toggle(this.scatter.haveZ, 0)
		}


		toggle(haveZ, dTime) {
			const newDTime = dTime || ANIMATION_DURATION
			if (haveZ) {
				this.createZAxis()
				d3.selectAll(".points").transition().style("fill", this.zz).duration(newDTime)
				d3.selectAll(".error").transition().style("stroke", this.zz).duration(newDTime)
				$("#MenuColor").fadeIn(newDTime)
				$("#zaxisContainer").fadeIn(newDTime)
				d3.select("#color_selector").classed("hidden", false)
			} else if (!haveZ) {
				d3.selectAll(".points").transition().style("fill", this.detectionMethodColor).duration(newDTime)
				d3.selectAll(".error").transition().style("stroke", this.detectionMethodColor).duration(newDTime)
				d3.select("#color_selector").classed("hidden", true)
				$("#MenuColor").fadeOut(newDTime)
				$("#zaxisContainer").fadeOut(newDTime)
			}
		}
	}

	class PAxisClass {
		constructor(scatter, whereCmd, wherePlot) {
			this.scatter = scatter
			this.whereplot = wherePlot

			// Will be refreshed in redraw()
			this.extentp = undefined

			const table = d3.select(`#${whereCmd}`).style("display", "none")

			/* Create selection menu */
			table.append("select").attr("id", "menu_choise_p").attr("class", "input_text").attr("name", "size")

			/* Add connection to the function */
			this.scatter.addVarSelector("p")

			/* Create checkbox for scale */
			const label = table.append("label")

			label.append("input").attr("type", "checkbox").attr("id", "menu_scale_p")

			label.each(function () {
				this.appendChild(document.createTextNode(" log scale"))
			})

			/* Add connection to the function */
			this.scatter.addScaleSelector("p")

			/* Add Min max selector */
			table.append("div").attr("id", "input_minmax_p").attr("class", "input_double_div")

			this.scatter.mm.p.AddMinMaxInput("input_minmax_p", ScatterPlot.prototype.redraw.bind(this.scatter))
		}

		toggle(havep, dtime) {
			const newDtime = dtime || ANIMATION_DURATION

			if (havep) {
				this.createPAxis()

				$("#MenuSize").fadeIn(newDtime)
				$("#paxisContainer").fadeIn(newDtime)
				d3.selectAll(".points")
					.transition()
					.attr("r", (e) => (e.p ? this.pScale(e.p) : 3))
					.style("stroke", (e) => (e.p ? defaultColor : "black"))
					.duration(newDtime)
			} else if (!havep) {
				d3.selectAll(".points").transition().attr("r", 3).style("stroke", "none").duration(newDtime)
				$("#MenuSize").fadeOut(newDtime)
				$("#paxisContainer").fadeOut(newDtime)
			}
		}

		redraw() {
			// Refresh the data
			this.extentp = extent("p", this.scatter.reducedDatas, this.scatter.mm)

			// control
			if (this.scatter.mm.p.scale === "log" && this.extentp[0] <= 0) {
				alert(
					`The variable "${this.scatter.mm.p.desc}" has negative or zero values.\nLogarithmic scale will be no set.`,
				)
				this.scatter.mm.p.setscale("linear")
			}

			// define the p scale (pointsize)
			this.pScale = d3.scale[this.scatter.mm.p.scale]().domain(this.extentp).range([3, 10])

			this.toggle(this.scatter.haveP, 0)
		}

		/*
		 * Plot P axis
		 */
		createPAxis() {
			const pNbTick = 5

			const extColorDomain = []
			if (this.scatter.mm.p.scale === "linear") {
				for (const tick of Array(pNbTick + 1).keys()) {
					extColorDomain.push(this.extentp[0] + (tick / pNbTick) * (this.extentp[1] - this.extentp[0]))
				}
			} else {
				// log
				const a = Math.log(this.extentp[0])
				for (const tick of Array(pNbTick + 1).keys()) {
					extColorDomain.push(Math.exp(a + (tick / pNbTick) * (Math.log(this.extentp[1]) - a)))
				}
			}

			const spaceBetweenPoint = Math.ceil((this.scatter.wTot - MARGIN.left - MARGIN.right) / (pNbTick + 1))
			const myFormat = d3.format(",.2g")
			const offsetFromCircle = 20
			const lineOffsetFromAxisLegend = 17

			// cleaning
			d3.select("#paxisContainer").remove()

			const vis = d3.select(`#${this.whereplot}`).append("g").attr("id", "paxisContainer").style("display", "none")

			const legend = vis.selectAll(".axis.p").data(extColorDomain).enter().append("g").attr("class", "axis p")

			// scale legend
			legend
				.append("text")
				.attr("x", (_d, i) => +i * spaceBetweenPoint + MARGIN.right)
				.attr("y", this.scatter.hTot - MARGIN.bottom / 2 + offsetFromCircle)
				.style("text-anchor", "middle")
				.text((d, i) => (i % 1 ? "" : myFormat(d)))

			// scale representation
			legend
				.append("circle")
				.attr("cx", (_d, i) => +i * spaceBetweenPoint + MARGIN.right)
				.attr("cy", this.scatter.hTot - MARGIN.bottom / 2)
				.attr("r", this.pScale)
				.style({
					stroke: defaultColor,
					"stroke-width": 2,
					fill: "transparent",
				})

			// not available representation
			vis
				.append("circle")
				.attr("class", "axis p")
				.attr("cx", (pNbTick + 0.5) * spaceBetweenPoint + MARGIN.right)
				.attr("cy", this.scatter.hTot - MARGIN.bottom / 2)
				.attr("r", 3)
				.style({
					stroke: "grey",
					fill: "transparent",
				})

			// Not available legend
			vis
				.append("g")
				.attr("class", "axis p")
				.append("text")
				.attr("text-anchor", "middle")
				.attr("x", (pNbTick + 0.5) * spaceBetweenPoint + MARGIN.right)
				.attr("y", this.scatter.hTot - MARGIN.bottom / 2 + offsetFromCircle)
				.text("Not Av.")

			// Axis legend
			vis
				.append("text")
				.attr("class", "axis label p")
				.attr("x", 0)
				.attr("y", this.scatter.hTot - MARGIN.bottom / 2 - offsetFromCircle)
				.style("text-anchor", "start")
				.text(this.scatter.mm.p.desc)
				.append("a")
				.attr("class", "linkunit")
				.attr("href", "#")
				.text(this.scatter.mm.p.axisLabelUnit())
				.on("click", () => {
					TP.refactorField(datas, this.scatter.mm.p, ScatterPlot.prototype.redraw.bind(this.scatter))
				})
				.append("title")
				.text("Change units on click if other units are available")

			vis
				.append("line")
				.attr("x1", "0")
				.attr("y1", this.scatter.hTot - MARGIN.bottom / 2 - offsetFromCircle - lineOffsetFromAxisLegend)
				.attr("x2", this.scatter.width)
				.attr("y2", this.scatter.hTot - MARGIN.bottom / 2 - offsetFromCircle - lineOffsetFromAxisLegend)
				.attr("stroke", "black")
		}
	}

	// singleton for label mode
	class LabelMode {
		constructor() {
			this.button = document.getElementById("labelMode")
			this.state = false

			this.button.checked = this.state
			this.button.onclick = () => {
				this.state = !this.state
				this.AddOrRemove()
			}
		}

		AddOrRemove() {
			if (this.state) {
				d3.selectAll("#my_clip-path")
					.selectAll("g")
					.each(function (_d, _i) {
						const circle = d3.select(this).select("circle")

						if (!circle.empty()) {
							d3.select(this)
								.append("text")
								.attr("class", "labelplanetname")
								.attr("x", circle.attr("cx"))
								.attr("y", circle.attr("cy"))
								.attr("dy", 1)
								.attr("dx", 2)
								.text((d) => d.n)
						}
					})
			} else {
				d3.selectAll("#my_clip-path").selectAll(".labelplanetname").remove()
			}

			this.button.checked = this.state
		}
	}

	class ScatterPlot {
		constructor(where) {
			// This will be filled in redraw()
			this.reducedDatas = undefined

			// default graphic
			this.whereDiv = where

			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = Math.max(
				document.querySelector("#diagram_image_id").offsetHeight,
				document.querySelector("#plot-menu-id").offsetHeight,
			)
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom

			// default variables - parameters
			this.haveP = false
			this.haveZ = false
			this.errX = new Err("x")
			this.errY = new Err("y")
			this.colorMin = "#ffff00"
			this.colorMax = "#ff0000"
			this.actionMode = "zoom"

			/* List variable to plot */
			this.varList = {}
			this.varListGroup = { Planet: [], Star: [] }

			for (const variable of varListArray) {
				variable.factor = 1.0
				this.varList[variable.id] = variable
				this.varListGroup[variable.related_model].push(variable)
			}

			// This will be defined in redraw
			this.xScale = undefined
			this.yScale = undefined

			/* define grid */
			this.grid = new DT.Grid()

			/* zoom center */
			PanZoom.setCenter([this.width, this.height])

			/* associate default variable to mm */
			this.mm = new TP.WorkVariable({
				x: "mass",
				y: "period",
				z: "radius",
				p: "axis",
			})

			this.labelMode = new LabelMode()

			// Set the default variables in the work-variables
			this.mm.x.pushvar(this.varList)
			this.mm.x.setscale("log")
			this.mm.y.pushvar(this.varList)
			this.mm.y.setscale("log")
			this.mm.z.pushvar(this.varList)
			this.mm.p.pushvar(this.varList)

			this.addVarSelector("x")
			this.addScaleSelector("x")
			this.addErrorSelector("x")
			this.mm.x.AddMinMaxInput("input_minmax_x", ScatterPlot.prototype.redraw.bind(this))

			this.addVarSelector("y")
			this.addScaleSelector("y")
			this.addErrorSelector("y")
			this.mm.y.AddMinMaxInput("input_minmax_y", ScatterPlot.prototype.redraw.bind(this))

			this.grid.AddSelector("menu_grid")
			this.addActivationAxis("z")
			this.addActivationAxis("p")

			// this.addExoRisSelector('menu_onoff_exoris')

			// connect button for manual zoom to machinerie
			PanZoom.connectButton("manualZoomSelect", "plot", MANUAL_ZOOM_DEFAULT)

			document.getElementById("query_f").onchange = ScatterPlot.prototype.getValues.bind(this)

			// axes variables
			this.zAxis = new ZAxisClass(this, "MenuColor", "plot")
			this.pAxis = new PAxisClass(this, "MenuSize", "plot")

			// button for create table
			const btnGoToTable = document.getElementById("gotoPlanetTable")

			btnGoToTable.onclick = () => {
				if (this.actionMode === "zoom") {
					TP.openPlanetsLink(uricatalog, this.mm)
				} else {
					ST.openPlanetsLink(uricatalog, "n")
				}
			}
			btnGoToTable.title = "Go to the catalog of the shown planets."

			// get the total number of planet to the inital page
			// -1 because the first member are the headers
			planetTotalNm = datas.length - 1

			// unpacking datas
			datas = DT.UnpackData(datas)

			d3.select(`#${this.whereDiv}`).append("div").attr("id", "plothere")

			// We create tan horizontal flip... just for the side effect of creating it...
			// TODO change HorizFlip constructor behavior: it should not have any side effect
			// and the effect should be done using an explicit method
			// DT.HorizFlip('container_diagram', 'swapConsole')
		}

		setSize() {
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = Math.max(
				document.querySelector("#diagram_image_id").offsetHeight,
				document.querySelector("#plot-menu-id").offsetHeight,
			)
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom

			return this
		}

		getValues() {
			const query =
				`x=${this.mm.x.vn}` +
				`&y=${this.mm.y.vn}` +
				`&z=${this.mm.z.vn}` +
				`&p=${this.mm.p.vn}` +
				`&f=${document.getElementById("query_f").value}`

			$.ajax({
				url: uriPlotRequestAjax,
				type: "GET",
				data: query,
				dataType: "json",
				success: (xhr) => {
					$("#error_window").hide()
					// store the total number of planets
					// -1 because the first member are the headers
					planetTotalNm = xhr.length - 1
					// required number of planets
					datas = DT.UnpackData(xhr)
					// find in the page if selector are different from default;
					TP.refactorDataIfNotDefault(datas, this.mm.x)
					TP.refactorDataIfNotDefault(datas, this.mm.y)
					TP.refactorDataIfNotDefault(datas, this.mm.z)
					TP.refactorDataIfNotDefault(datas, this.mm.p)

					this.slowRedraw()
				},
				complete: () => {},
				error: (_jsXHR, _textStatus, errorThrown) => {
					if (errorThrown === "BAD REQUEST") {
						console.log("syntax error in query", errorThrown)
						$("#error_window").show()
					} else {
						console.log("other err", errorThrown)
					}
				},
			})
		}

		slowRedraw() {
			d3.transition().duration(100).each(ScatterPlot.prototype.redraw.bind(this))
		}

		clean() {
			d3.select("#mainrect").remove()
		}

		redraw() {
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = Math.max(
				document.querySelector("#diagram_image_id").offsetHeight,
				document.querySelector("#plot-menu-id").offsetHeight,
			)
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom

			/* filter data by limits */
			this.reducedDatas = datas.filter(
				(d) => this.mm.x.testIn(d.x) && this.mm.y.testIn(d.y) && this.mm.z.testIn(d.z) && this.mm.p.testIn(d.p),
			)

			/* filter data taking into account if the scale is log */
			if (this.mm.x.scale === "log" || this.mm.y.scale === "log") {
				this.reducedDatas = this.reducedDatas.filter((d) => {
					const nonegx = !(this.mm.x.scale === "log" && d.x <= 0)
					const nonegy = !(this.mm.y.scale === "log" && d.y <= 0)

					return nonegx && nonegy
				})
			}

			// get the represented planet number
			planetRepresentedNm = this.reducedDatas.length

			// if only a point then plot in linear-linear scale
			if (this.reducedDatas.length === 1) {
				this.mm.x.setscale("linear")
				this.mm.y.setscale("linear")
				$("#menu_scale_x").attr("checked", false)
				$("#menu_scale_y").attr("checked", false)
			}

			// define the x scale (horizontal)
			this.xScale = d3.scale[this.mm.x.scale]()
				.domain(extent("x", this.reducedDatas, this.mm))
				.range([0, this.width])

			// define the x scale (horizontal)
			this.yScale = d3.scale[this.mm.y.scale]()
				.domain(extent("y", this.reducedDatas, this.mm))
				.range([this.height, 0]) // map these to the chart height, less padding.

			// Initialize the scale function in the selection tool
			ST.init(this.xScale, this.yScale)

			const zoom = d3.behavior.zoom().x(this.xScale).y(this.yScale).scaleExtent([0.5, 20]).on("zoom", zoomed)

			// clean
			this.clean()

			// create an svg container
			const vis = d3
				.select("#plothere")
				.append("svg:svg")
				.attr("id", "mainrect")
				.attr("width", this.wTot)
				.attr("height", this.hTot)
				.append("svg:g")
				.attr("id", "plot")
				.attr("transform", `translate(${MARGIN.left},${MARGIN.top})`)
				.call(zoom)

			vis
				.append("svg:rect")
				.attr("x", 0)
				.attr("y", 0)
				.attr("width", this.width)
				.attr("height", this.height)
				.attr("class", "plot")

			/* scales applied to data */
			const xx = (e) => this.xScale(e.x)
			const yy = (e) => this.yScale(e.y)

			const colorDetetctionMethod = (e) => getColorFromDetectionMethod(e)

			// define the x axis
			const xAxis = d3.svg.axis().scale(this.xScale).orient("bottom")
			if (this.mm.x.datatype === "int") {
				xAxis.tickFormat(d3.format("d"))
			}
			const xAxis2 = d3.svg.axis().scale(this.xScale).orient("top").tickFormat("")

			// define the y axis
			const yAxis = d3.svg.axis().scale(this.yScale).orient("left")
			if (this.mm.y.datatype === "int") {
				yAxis.tickFormat(d3.format("d"))
			}
			const yAxis2 = d3.svg.axis().scale(this.yScale).orient("right").tickFormat("")

			// associate grid to the plot
			this.grid.init(vis, {
				X: this.xScale,
				Y: this.yScale,
				width: this.width,
				height: this.height,
			})

			// draw x axis with labels and move to the bottom of the chart area
			vis.append("g").attr("class", "axis x").attr("transform", `translate(0,${this.height})`).call(xAxis)
			vis.append("g").attr("class", "axis x2").attr("transform", "translate(0,0)").call(xAxis2)

			// draw y axis with labels and move in from the size by the amount of padding
			vis.append("g").attr("class", "axis y").call(yAxis)
			vis.append("g").attr("class", "axis y2").attr("transform", `translate(${this.width},0)`).call(yAxis2)

			vis.selectAll(".axis.label").remove()

			vis
				.append("text")
				.attr("class", "axis label")
				.attr("x", (this.wTot - MARGIN.left) / 2)
				.attr("y", this.hTot - MARGIN.bottom)
				.text(this.mm.x.desc)
				.append("a")
				.attr("class", "linkunit")
				.attr("href", "#")
				.text(this.mm.x.axisLabelUnit())
				.on("click", () => {
					TP.refactorField(datas, this.mm.x, ScatterPlot.prototype.redraw.bind(this))
				})
				.append("title")
				.text("Change units on click if other units are available")

			vis
				.append("text")
				.attr("class", "axis label")
				.attr("x", -(this.hTot - MARGIN.bottom - MARGIN.top) / 2)
				.attr("y", -(MARGIN.left / 2))
				.attr("transform", "rotate(-90)")
				.text(this.mm.y.desc)
				.append("a")
				.attr("class", "linkunit")
				.attr("href", "#")
				.text(this.mm.y.axisLabelUnit())
				.on("click", () => {
					TP.refactorField(datas, this.mm.y, ScatterPlot.prototype.redraw.bind(this))
				})
				.append("title")
				.text("Change units on click if other units are available")

			vis
				.append("svg:clipPath")
				.attr("id", "clip")
				.append("svg:rect")
				.attr("width", this.width)
				.attr("height", this.height)

			const chartBody = vis
				.append("g")
				.attr("id", "my_clip-path")
				.attr("clip-path", "url(#clip)")
				.selectAll(".points")
				.data(this.reducedDatas)
				.enter()
				.append("g")

			chartBody
				.append("circle")
				.attr({
					class: "points",
					cx: xx,
					cy: yy,
					r: 3,
					fill: colorDetetctionMethod,
				})
				.on("click", (d, _i) => {
					window.open(uricatalog + d.u)
				})
				.append("svg:title")
				.text(
					(d) =>
						`${d.n}\n` +
						`${this.mm.x.id} = ${d.x}${this.mm.x.axisLabelUnit()}\n` +
						`${this.mm.y.id} = ${d.y}${this.mm.y.axisLabelUnit()}`,
				)

			chartBody
				.append("line")
				.attr("class", "error x")
				.attr("x1", (e) => {
					const x1 = this.xScale(e.x - e.x_emin)
					return isValidNumber(x1) ? x1 : this.xScale(e.x)
				})

				.attr("x2", (e) => {
					const x2 = this.xScale(e.x + e.x_emax)
					return isValidNumber(x2) ? x2 : this.xScale(e.x)
				})
				.attr("y1", yy)
				.attr("y2", yy)
				.style("opacity", this.errX.opacity)
				.style("stroke", "black")

			chartBody
				.append("line")
				.attr("class", "error y")
				.attr("x1", xx)
				.attr("x2", xx)
				.attr("y1", (e) => {
					const y1 = this.yScale(e.y - e.y_emin)
					return isValidNumber(y1) ? y1 : this.yScale(e.y)
				})
				.attr("y2", (e) => {
					const y2 = this.yScale(e.y + e.y_emax)
					return isValidNumber(y2) ? y2 : this.yScale(e.y)
				})
				.style("opacity", this.errY.opacity)
				.style("stroke", "black")

			/* Add Z and P axis */
			this.zAxis.redraw()
			this.pAxis.redraw()

			const that = this
			function zoomed() {
				/* Store mouse movement in the PanZoom */
				PanZoom.trackMouseMouv()

				vis.select(".axis.x").call(xAxis)
				vis.select(".axis.y").call(yAxis)
				vis.select(".axis.x2").call(xAxis2)
				vis.select(".axis.y2").call(yAxis2)
				that.grid.move()

				vis
					.selectAll(".error.x")
					.attr("x1", (e) => {
						const x1 = that.xScale(e.x - e.x_emin)
						return isValidNumber(x1) ? x1 : that.xScale(e.x)
					})
					.attr("x2", (e) => {
						const x2 = that.xScale(e.x + e.x_emax)
						return isValidNumber(x2) ? x2 : that.xScale(e.x)
					})
					.attr("y1", yy)
					.attr("y2", yy)

				vis
					.selectAll(".error.y")
					.attr("x1", xx)
					.attr("x2", xx)
					.attr("y1", (e) => {
						const y1 = that.yScale(e.y - e.y_emin)
						return isValidNumber(y1) ? y1 : that.yScale(e.y)
					})
					.attr("y2", (e) => {
						const y2 = that.yScale(e.y + e.y_emax)
						return isValidNumber(y2) ? y2 : that.yScale(e.y)
					})

				vis.selectAll(".points").attr("cx", xx).attr("cy", yy).attr("fill", colorDetetctionMethod)

				vis.selectAll(".labelplanetname").attr("x", xx).attr("y", yy)

				ST.update()
				// Exo.update()
			}

			// Exo.make(this.mm, this.xScale, this.yScale)

			// add behaviour to selection mode
			const selectionModeButton = document.getElementById("selectionMode")
			selectionModeButton.checked = false
			selectionModeButton.onclick = () => {
				// switch
				if (this.actionMode === "drag") {
					// set zoom
					this.actionMode = "zoom"
					vis.on(".drag", null).call(zoom)
					PanZoom.trackOn()
				} else {
					// set drag
					this.actionMode = "drag"
					vis.on(".zoom", null).call(ST.drag)
					PanZoom.trackOff()
				}
			}

			selectionModeButton.parentNode.title =
				'Set "Selection-mode" :\n' +
				" Planets can be selected by drawing a closed path on the plot.\n" +
				" For more selections hold SHIFT key during selection."

			PanZoom.Connect(zoom, zoomed).reset()

			// add behaviour to Reset zoom button
			const zoomReset = document.getElementById("zoomReset")
			zoomReset.onclick = () => {
				PanZoom.reset()
			}
			zoomReset.title = "Reset the view"

			this.labelMode.AddOrRemove()

			FullfyStatistic()
		}

		addVarSelector(axis) {
			const child = d3.select(`#menu_choise_${axis}`).on("change", () => {
				// set terms of transition that will take place
				const locvar = d3.event.currentTarget.options[d3.event.currentTarget.selectedIndex].__data__
				this.mm[axis].pushvar(this.varList, locvar.id)
				this.getValues()
				// Exo.off()
			})

			child.selectAll("optgroup").remove()

			child
				.selectAll("optgroup")
				.data(Object.keys(this.varListGroup))
				.enter()
				.append("optgroup")
				.attr("label", (d) => `${d} parameters`)
				.attr("id", (d) => `groupopt_${d}`)

			for (const [grpName, grpData] of Object.entries(this.varListGroup)) {
				child
					.select(`optgroup#groupopt_${grpName}`)
					.selectAll("option")
					.data(grpData)
					.enter()
					.append("option")
					.text((d) => d.desc)
					.attr("value", (d) => d.id)
					.attr("title", (d) => d.title)
			}

			// set default on dropDown
			child.property("value", this.mm[axis].vn)
		}

		addScaleSelector(axis) {
			const locvar = this.mm[axis].scale === "log"

			d3.select(`#menu_scale_${axis}`)
				.property("checked", locvar)
				.on("click", () => {
					// set terms of transition that will take place
					// when a new economic indicator is chosen
					const locScale = d3.event.currentTarget.checked ? "log" : "linear"
					this.mm[axis].setscale(locScale)
					if (axis === "p") {
						this.pAxis.createPAxis()
						this.pAxis.redraw()
					} else {
						this.slowRedraw()
					}
				})
		}

		addActivationAxis(axis) {
			d3.select(`#menu_onoff_${axis}`).on("click", () => {
				const boxChecked = $(`#menu_onoff_${axis}`).is(":checked")
				if (axis === "p") {
					this.haveP = boxChecked
					this.pAxis.toggle(this.haveP)
				} else if (axis === "z") {
					this.haveZ = boxChecked
					if (this.haveZ) {
						$('#detectionMethodColorLegend').slideUp(1000)
					} else {
						$('#detectionMethodColorLegend').slideDown(1000)
					}
					this.zAxis.toggle(this.haveZ)
				} else {
					// Any other axes we do nothing
				}
			})
		}

		addErrorSelector(axis) {
			d3.select(`#menu_err_${axis}`)
				.property("checked", false)
				.on("click", () => {
					if (axis === "x") {
						this.errX.flip()
						d3.selectAll(".error.x").transition().style("opacity", this.errX.opacity).duration(ANIMATION_DURATION)
					} else if (axis === "y") {
						this.errY.flip()
						d3.selectAll(".error.y").transition().style("opacity", this.errY.opacity).duration(ANIMATION_DURATION)
					} else {
						// Any other axis we do nothing
					}
				})
		}

		addColorSelector(where) {
			const maxMinColor = [
				{ name: "min", col: this.colorMin },
				{ name: "max", col: this.colorMax },
			]

			const block = d3.select(`#${where}`).append("div")

			d3.selectAll("#color_input_min").on("change", () => {
				block.select(".myColor").style("background-color", maxMinColor.min)
				const newMinColor = `${d3.event.currentTarget.value}`
				console.log(`newMinColor = ${newMinColor}`)
				this.colorMin = newMinColor
				this.zAxis.redraw()
			})

			d3.selectAll("#color_input_max").on("change", () => {
				block.select(".myColor").style("background-color", maxMinColor.max)
				const newMaxColor = `${d3.event.currentTarget.value}`
				console.log(`newMaxColor = ${newMaxColor}`)
				this.colorMax = newMaxColor
				this.zAxis.redraw()
			})
		}

		//   addExoRisSelector (whereSwitch) {
		//     const controller = d3.select(`#${whereSwitch}`)

		//     controller.property('checked', false)
		//       .on('click', () => {
		//         if (this.checked) {
		//           Exo.on()
		//           Exo.make(this.mm, this.xScale, this.yScale)
		//         } else {
		//           Exo.off()
		//         }
		//       })

		//     Exo.init(whereSwitch)
		//   }
	}

	class ResizeClass {
		constructor(diagram) {
			this.diagram = diagram

			this.cont = document.getElementById("container_diagram")
			this.graph = document.getElementById("diagram_image_id")
			this.console = document.getElementById("diagram_form_id")
			// this.console2 = document.getElementById('MenuExoris')
		}

		redo() {
			// FIXME What are thos to values ???
			let Wform
			// let Wexo

			this.graph.style.height = this.diagram.hTot
			this.console.style.width = Wform
			// this.console2.style.width = Wexo

			diagram.setSize(this.diagram.wTot, this.diagram.hTot)
			diagram.redraw()
			// Exo.createRadarChart()
		}
	}

	// Let's create everything
	const diagram = new ScatterPlot("diagram_image_id")

	const resizeFunc = new ResizeClass(diagram)
	resizeFunc.redo()

	window.addEventListener(
		"resize",
		() => {
			// Announce the resize oof the window
			diagram.clean()
			setTimeout(() => {
				resizeFunc.redo()
			}, TIMEOUT_DURATION)
		},
		false,
	)
})()
