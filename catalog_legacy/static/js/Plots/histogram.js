/*
Histogram drawing library.

Need some data parameters to be transmitted when calling the script in th html:

- data-datas: JSON string containing the initial data
- data-uricatalog: uri to get data from the catalog
- data-uriplotrequestajax: uri to get data for the plot
- varListArray: JSON string containing header descriptions for the data

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* eslint-env browser */
/* global $, d3, DiagramTools, TypesPlot, ManualZoom, DropdownCheckboxes */

;(() => {
	/* some aliases */
	const DT = DiagramTools
	const TP = TypesPlot
	const PanZoom = ManualZoom

	// Value passed from HTML via data attribute from the script element
	// See: https://stackoverflow.com/a/15069289
	// And: https://developer.mozilla.org/fr/docs/Apprendre/HTML/Comment/Utiliser_attributs_donnes
	let datas = JSON.parse(document.currentScript.dataset.datas)
	const uricatalog = document.currentScript.dataset.uricatalog
	const uriPlotRequestAjax = document.currentScript.dataset.uriplotrequestajax
	const varListArray = JSON.parse(document.currentScript.dataset.varlistarray)

	DropdownCheckboxes.init()

	// Constants
	const MARGIN = {
		top: 30,
		right: 10,
		bottom: 60,
		left: 100,
	}

	const ANIMATION_DURATION = 500 // ms
	const TIMEOUT_DURATION = 200 // ms

	// TODO get rid of those global variables
	let planetTotalNm
	let planetRepresentedNm

	function FullfyStatistic() {
		document.getElementById("Planet_total_nm").innerHTML = planetTotalNm
		document.getElementById("Planet_represented_nm").innerHTML = planetRepresentedNm
	}

	class HistoPlot {
		constructor(where) {
			// default graphic
			this.whereDiv = where

			// default
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = Math.max(
				document.querySelector("#diagram_image_id").offsetHeight,
				document.querySelector("#plot-menu-id").offsetHeight,
			)
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom

			// default variables - parameters
			this.barColor = "#0073b0" // default graph color
			this.histStyle = "bars" // or 'lines'

			// number of bins for histogram
			this.nBin = 20

			/* List variable to plot */
			this.varList = {}
			this.varListGroup = { Planet: [], Star: [] }

			/* define grid */
			this.grid = new DT.Grid(true)

			/* cumulative */
			this.cumulativeMode = false

			/* zoom center */
			PanZoom.setCenter([this.width, this.height])

			/* associate default variable to mm */
			this.mm = new TP.WorkVariable({
				x: "mass",
				y: "Frequency",
			})

			this.format = this.mm.x.datatype === "int" ? d3.format(".0f") : d3.format(",.3f")

			// We create tan horizontal flip... just for the side effect of creating it...
			// TODO change HorizFlip constructor behavior: it should not have any side effect
			// and the effect should be done using an explicit method
			// DT.HorizFlip('container_diagram', 'swapConsole')
		}

		setSize() {
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = Math.max(
				document.querySelector("#diagram_image_id").offsetHeight,
				document.querySelector("#plot-menu-id").offsetHeight,
			)
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom

			return this
		}

		getValues() {
			const query = `x=${this.mm.x.vn}&f=${document.getElementById("query_f").value}`

			$.ajax({
				url: uriPlotRequestAjax,
				type: "GET",
				data: query,
				dataType: "json",
				success: (xhr) => {
					$("#error_window").hide()
					// store the total number of planets
					// -1 because the first member are the headers
					planetTotalNm = xhr.length - 1
					// required number of planets
					datas = DT.UnpackData(xhr)
					TP.refactorDataIfNotDefault(datas, this.mm.x)
					this.slowRedraw()
				},
				complete: () => {},
				error: (jsXHR, textStatus, errorThrown) => {
					if (errorThrown === "BAD REQUEST") {
						console.log("syntax error in query", errorThrown)
						$("#error_window").show()
					} else {
						console.log("other err", errorThrown)
					}
				},
			})
		}

		createPage() {
			for (const element of varListArray) {
				element.factor = 1.0
				this.varList[element.id] = element
				this.varListGroup[element.related_model].push(element)
			}

			// Set the default variables in the work-variables
			this.mm.x.pushvar(this.varList)

			this.addVarSelector("x")
			this.addYSelector()
			this.addScaleSelector("x")
			this.mm.x.AddMinMaxInput("input_minmax_x", HistoPlot.prototype.redraw.bind(this))
			this.addScaleSelector("y")
			this.addColorSelector("color_selector")
			this.grid.AddSelector("menu_grid")
			this.addNbinsInput("nBinsInput")
			this.addSwitchBarsLines("switchBarsLinesDiv")
			this.addSwitchCumulativeMode("cumulativeMode", this.cumulativeMode)
			document.getElementById("query_f").onchange = HistoPlot.prototype.getValues.bind(this)

			// connect button for manual zoom to machinerie
			PanZoom.connectButton("manualZoomSelect", "plot")

			// button for create table
			const btnGoToTable = document.getElementById("gotoPlanetTable")
			btnGoToTable.onclick = () => {
				TP.openPlanetsLink(uricatalog, this.mm)
			}
			btnGoToTable.title = "Go to the catalog of the shown planet."

			// get the total number of planet to the inital page
			planetTotalNm = datas.length - 1

			// unpacking datas
			datas = DT.UnpackData(datas)

			d3.select(`#${this.whereDiv}`).append("div").attr("id", "plothere")
		}

		slowRedraw() {
			d3.transition().duration(ANIMATION_DURATION).each(HistoPlot.prototype.redraw.bind(this))
		}

		clean() {
			d3.select("#mainrect").remove()
		}

		redraw() {
			// This will be defined later
			let titlefunc
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = Math.max(
				document.querySelector("#diagram_image_id").offsetHeight,
				document.querySelector("#plot-menu-id").offsetHeight,
			)
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom

			/* filter data by limits */
			const reducedDatas = datas
				.filter((d) => {
					let result = d.x >= this.mm.x.min && d.x <= this.mm.x.max
					if (this.mm.x.scale === "log") {
						result = result && d.x > 0
					}
					return result
				})
				.map((d) => d.x)

			// get the represented planet number
			planetRepresentedNm = reducedDatas.length

			const min = this.mm.x.min !== -Infinity ? this.mm.x.min : d3.min(reducedDatas)
			const max = this.mm.x.max !== Infinity ? this.mm.x.max : d3.max(reducedDatas)

			// define the x scale (horizontal)
			const xScale = d3.scale[this.mm.x.scale]().domain([min, max]).range([0, this.width]).nice()

			let bins = []
			if (this.mm.x.scale === "log") {
				const a = Math.log10(min)
				const b = Math.log10(max)
				const delta = (b - a) / (this.nBin - 1)
				for (let ii = a; ii <= b; ii += delta) {
					bins.push(10 ** ii)
				}
			} else if (this.mm.x.unit === "year") {
				const a = min - 0.5
				const b = max + 0.5
				const delta = (b - a) / this.nBin
				for (let ii = a; ii <= b; ii += delta) {
					bins.push(ii)
				}
			} else {
				bins = this.nBin
			}

			// Generate a histogram using twenty uniformly-spaced bins.
			let hdata = d3.layout
				.histogram()
				.frequency(this.mm.y.vn !== "Probability")
				.bins(bins)(reducedDatas)

			if (!this.cumulativeMode) {
				// Normal mode
				titlefunc = (d) => `${d.length} objects in [${this.format(d.x)},${this.format(d.x + d.dx)})`
			} else {
				// Cumulative Mode
				const hdataTmp = []
				let total = 0.0 // total count
				let norme = 1.0 // is the norme used when probability

				if (this.mm.y.vn === "Probability") {
					norme = reducedDatas.length
				}

				for (const idata of hdata) {
					total += idata.length
					const tmpArr = [total]
					tmpArr.x = idata.x
					tmpArr.dx = idata.dx
					tmpArr.dx = idata.dx
					tmpArr.y = total / norme
					hdataTmp.push(tmpArr)
				}

				titlefunc = (d) => `${d[0]} objects in [${this.format(d.x)},${this.format(d.x + d.dx)})`

				hdata = hdataTmp
			}

			const yScale = d3.scale[this.mm.y.scale]()
				.domain([1.0e-12, d3.max(hdata, (d) => d.y)])
				.range([this.height, 0])
				.nice()

			const zoom = d3.behavior.zoom().x(xScale).y(yScale).scaleExtent([0.5, 20]).on("zoom", zoomed)

			// clean
			this.clean()

			// create an svg container
			const vis = d3
				.select("#plothere")
				.append("svg:svg")
				.attr("id", "mainrect")
				.attr("width", this.wTot)
				.attr("height", this.hTot)
				.append("svg:g")
				.attr("id", "plot")
				.attr("transform", `translate(${MARGIN.left},${MARGIN.top})`)
				.call(zoom)

			vis
				.append("svg:rect")
				.attr("x", 0)
				.attr("y", 0)
				.attr("width", this.width)
				.attr("height", this.height)
				.attr("class", "plot")

			/* scales applied to data */
			const yy = (e) => (e.y <= 0 ? this.height : yScale(e.y))

			const that = this
			function zoomed() {
				/* Store mouse movement in the PanZoom */
				PanZoom.trackMouseMouv()

				vis.select(".axis.x").call(xAxis)
				vis.select(".axis.y").call(yAxis)
				vis.select(".axis.x2").call(xAxis2)
				vis.select(".axis.y2").call(yAxis2)
				that.grid.move()

				const scale = PanZoom.getScale()
				const translate = PanZoom.getTranslation()

				chartBody
					.selectAll("rect.bar,line.bar,path.bar")
					.attr("transform", `translate(${translate})scale(${scale})`)
					.attr("x2", -translate[0] / scale)
			}

			// add behaviour to Reset zoom button
			document.getElementById("zoomReset").onclick = zoomed

			// define the x axis
			const xAxis = d3.svg.axis().scale(xScale).orient("bottom")
			if (this.mm.x.datatype === "int") {
				xAxis.tickFormat(d3.format("d"))
			}
			if (this.mm.x.scale === "log") {
				xAxis.ticks(2, DT.formatTick)
			}

			const xAxis2 = d3.svg.axis().scale(xScale).orient("top").tickFormat("")

			// define the y axis
			const yAxis = d3.svg.axis().scale(yScale).orient("left")
			if (this.mm.y.scale === "log") {
				yAxis.ticks(10, DT.formatTick)
			}
			const yAxis2 = d3.svg.axis().scale(yScale).orient("right").tickFormat("")

			// associate grid to the plot
			this.grid.init(vis, {
				X: xScale,
				Y: yScale,
				width: this.width,
				height: this.height,
			})

			// draw x axis with labels and move to the bottom of the chart area
			vis.append("g").attr("class", "axis x").attr("transform", `translate(0,${this.height})`).call(xAxis)
			vis.append("g").attr("class", "axis x2").attr("transform", "translate(0,0)").call(xAxis2)

			// draw y axis with labels and move in from the size by the amount of padding
			vis.append("g").attr("class", "axis y").call(yAxis)
			vis.append("g").attr("class", "axis y2").attr("transform", `translate(${this.width},0)`).call(yAxis2)

			vis.selectAll(".axis.label").remove()

			vis
				.append("text")
				.attr("class", "axis label")
				.attr("x", (this.wTot - MARGIN.left) / 2)
				.attr("y", this.hTot - MARGIN.bottom)
				.text(this.mm.x.desc)
				.append("a")
				.attr("class", "linkunit")
				.attr("href", "#")
				.text(this.mm.x.axisLabelUnit())
				.on("click", () => {
					TP.refactorField(datas, this.mm.x, HistoPlot.prototype.redraw.bind(this))
				})
				.append("title")
				.text("Change units on click if other units are available")

			vis
				.append("text")
				.attr("class", "axis label")
				.attr("x", -(this.hTot - MARGIN.bottom) / 2)
				.attr("y", -(MARGIN.left / 2))
				.attr("transform", "rotate(-90)")
				.text(this.mm.y.vn)

			vis
				.append("svg:clipPath")
				.attr("id", "clip")
				.append("svg:rect")
				.attr("width", this.width)
				.attr("height", this.height)

			const chartBody = vis.append("g").attr("id", "my_clip-path").attr("clip-path", "url(#clip)")

			const lineFunction = d3.svg
				.line()
				.interpolate("linear")
				.x((d) => xScale(d.x + d.dx / 2))
				.y(yy)

			if (this.histStyle === "bars") {
				const bar = chartBody
					.selectAll(".bar")
					.data(hdata)
					.enter()
					.append("g")
					.attr("class", "bar")
					.attr("id", (_d, i) => `hbar_${i}`)
					.on("mousemove", () => {
						d3.select(d3.event.currentTarget).select(".horizmakeup").style("opacity", 1)
					})

					.on("mouseover", () => {
						d3.select(d3.event.currentTarget).select(".horizmakeup").style("opacity", 0)
					})
					.on("mouseout", () => {
						d3.select(d3.event.currentTarget).select(".horizmakeup").style("opacity", 0)
					})

				// width of bins
				const last = hdata.slice(-1)[0]
				const wbin = xScale(last.x + last.dx) - xScale(last.x)

				bar
					.append("rect")
					.transition()
					.attr("class", "bar")
					.attr("x", (d) => xScale(d.x))
					.attr("y", yy)
					.attr("width", wbin)
					.attr("height", (d) => this.height - yy(d))
					.style("fill", this.barColor)
					.duration(ANIMATION_DURATION)

				bar.append("svg:title").text(titlefunc)

				bar.append("line").attr({
					class: "horizmakeup bar",
					x1: (d) => xScale(d.x + d.dx),
					x2: 0,
					y1: yy,
					y2: yy,
				})
			} else {
				chartBody
					.selectAll(".bar")
					.data([hdata])
					.enter()
					.append("path")
					.transition()
					.attr("class", "bar")
					.attr("d", lineFunction)
					.style("fill", "none")
					.style("stroke", this.barColor)
					.duration(ANIMATION_DURATION)
			}

			PanZoom.Connect(zoom, zoomed).reset()
			const zoomReset = document.getElementById("zoomReset")
			zoomReset.onclick = PanZoom.reset
			zoomReset.title = "Reset the view"

			/* Add logo */
			FullfyStatistic()
		}

		addVarSelector(axis) {
			const child = d3.select(`#menu_choise_${axis}`).on("change", () => {
				// set terms of transition that will take place
				const locvar = d3.event.currentTarget.options[d3.event.currentTarget.selectedIndex].__data__
				this.mm[axis].pushvar(this.varList, locvar.id)
				this.getValues()
			})

			child.selectAll("optgroup").remove()

			child
				.selectAll("optgroup")
				.data(Object.keys(this.varListGroup))
				.enter()
				.append("optgroup")
				.attr("label", (d) => `${d} parameters`)
				.attr("id", (d) => `groupopt_${d}`)

			for (const [grpName, grpData] of Object.entries(this.varListGroup)) {
				child
					.select(`optgroup#groupopt_${grpName}`)
					.selectAll("option")
					.data(grpData)
					.enter()
					.append("option")
					.text((d) => d.desc)
					.attr("value", (d) => d.id)
					.attr("title", (d) => d.title)
			}

			// set default on dropDown
			child.property("value", this.mm[axis].vn)
		}

		addYSelector() {
			const child = d3.select("#menu_choise_y").on("change", () => {
				// set terms of transition that will take place
				const ipsilon = d3.event.currentTarget.options[d3.event.currentTarget.selectedIndex].__data__
				this.mm.y.init(ipsilon)
				this.slowRedraw()
			})

			child.selectAll("option").remove()

			child
				.selectAll("option")
				.data(["Probability", "Frequency"])
				.enter()
				.append("option")
				.text((d) => d)
				.attr("value", (d) => d)
				.attr("title", (d) => d)

			child.property("value", this.mm.y.vn)
		}

		addScaleSelector(axis) {
			const locvar = this.mm[axis].scale === "log"

			d3.select(`#menu_scale_${axis}`)
				.property("checked", locvar)
				.on("click", () => {
					// set terms of transition that will take place
					// when a new economic indicator is chosen
					const locScale = d3.event.currentTarget.checked ? "log" : "linear"
					this.mm[axis].setscale(locScale)
					this.slowRedraw()
				})
		}

		addColorSelector(where) {
			d3.select(`#${where}`).append("div")

			d3.select("#color_input").on("change", () => {
				this.barColor = `${d3.event.currentTarget.value}`
				d3.selectAll("rect.bar").style("fill", this.barColor)
				d3.selectAll("path.bar").style("stroke", this.barColor)
			})
		}

		addSwitchBarsLines(where) {
			const values = ["bars", "lines"]

			const mydiv = d3
				.select(`#${where}`)
				.append("div")
				.attr("class", "submit_btn_half")
				.selectAll("span")
				.data(values)
				.enter()
				.append("span")

			mydiv
				.append("input")
				.attr("type", "radio")
				.attr("name", "switchBarsLinesName")
				.attr("id", (d) => `${d}_BarsLines`)
				.attr("value", (d) => d)
				.property("checked", (d) => this.histStyle === d)
				.on("change", (d) => {
					this.histStyle = d
					this.slowRedraw()
				})

			mydiv
				.append("label")
				.attr("for", (d) => `${d}_BarsLines`)
				.text((d) => d)
		}

		addNbinsInput(where) {
			d3.select(`#${where}`)
				.attr("value", this.nBin)
				.attr("placeholder", this.nBin)
				.attr("min", 5)
				.attr("type", "number")
				.on("change", () => {
					this.nBin = Number(d3.event.currentTarget.value)
					this.slowRedraw()
				})
		}

		addSwitchCumulativeMode(where, status0) {
			const switchCumulativeMode = document.getElementById(where)
			this.cumulativeMode = status0 || false

			switchCumulativeMode.parentNode.title = "Switch in cumulative mode"

			switchCumulativeMode.onclick = () => {
				this.cumulativeMode = !this.cumulativeMode
				this.slowRedraw()
			}
		}
	}

	class ResizeClass {
		constructor(histogram) {
			this.histogram = histogram

			this.cont = document.getElementById("container_diagram")
			this.graph = document.getElementById("diagram_image_id")
			this.console = document.getElementById("diagram_form_id")
		}

		redo() {
			// FIXME What are thos to values ???
			let Wform

			this.graph.style.height = this.histogram.hTot
			this.console.style.width = Wform
			diagram.setSize(this.histogram.wTot, this.histogram.hTot)
			diagram.redraw()
		}
	}

	const diagram = new HistoPlot("diagram_image_id")
	diagram.createPage()

	const resize = new ResizeClass(diagram)

	resize.redo()

	window.addEventListener(
		"orientationchange",
		() => {
			// Announce the new orientation number
			diagram.clean()
			setTimeout(() => {
				resize.redo()
			}, TIMEOUT_DURATION)
		},
		false,
	)

	window.addEventListener(
		"resize",
		() => {
			// Announce the resize oof the window
			diagram.clean()
			setTimeout(() => {
				resize.redo()
			}, TIMEOUT_DURATION)
		},
		false,
	)
})()
