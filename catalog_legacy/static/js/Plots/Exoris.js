/*
Contains exoris curves traitements

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* eslint-env browser */
/* global d3, $, RadarChart */

this.ExorisCurves = {}
;((exports) => {
	// Value passed from HTML via data attribute from the script element
	// See: https://stackoverflow.com/a/15069289
	// And: https://developer.mozilla.org/fr/docs/Apprendre/HTML/Comment/Utiliser_attributs_donnes
	const uriJsonCurves = document.currentScript.dataset.urijsoncurves

	const monoLayer = [
		{ type: "Fe", color: "#FF0000", title: "", symbol: "Fe" },
		{ type: "MgSiO3", color: "#FFD700", title: "", symbol: "MgSiO₃" },
		{ type: "H2O", color: "#00CF12", title: "", symbol: "H₂O" },
		{ type: "H-He", color: "#0000FF", title: "", symbol: "H-He" },
	]
	const multiLayer = [
		{ type: "earth", color: "#FF8b00", title: "", symbol: "Earth" },
		{ type: "wet", color: "#BD9300", title: "", symbol: "Wet" },
		{ type: "neptune", color: "blueviolet", title: "", symbol: "Neptune" },
		{ type: "giant", color: "blue", title: "", symbol: "Giant" },
	]
	const exorisTypes = [
		{ name: "mono-layer", family: "mono", data: monoLayer },
		{ name: "multi-layers", family: "multi", data: multiLayer },
	]

	// contains all keys of monoLayer,multiLayer with their colors and indices in exodata
	const typeColor = {}

	monoLayer.map(function f(d) {
		typeColor[d.type] = { color: d.color, symbol: d.symbol, indices: [] }
	})
	multiLayer.map(function f(d) {
		typeColor[d.type] = { color: d.color, symbol: d.symbol, indices: [] }
	})

	let active = false
	const idSVG = "#my_clip-path"
	const idMenu = "MenuExoris"
	let paths // contains svg
	let wswitch // where switch button
	let varx
	let vary
	let massfactor
	let radiusfactor
	let exodata = []
	const line = d3.svg.line().interpolate("basis")
	let latLegend

	const init = (whereSwitch) => {
		// initialisation of parameters without plot
		wswitch = document.getElementById(whereSwitch)
		active = false
		const container = d3.select(`#${idMenu}`).style("display", "none")

		/* Menu containing commands */
		const commands = container.append("div").attr("id", `${idMenu}_commands`)
		commands.append("h3").attr("class", "variables_label").text("Exoris Simulations")

		// switch for lateral legend
		commands
			.append("label")
			.append("text")
			.attr("class", "variables_label")
			.text("Show labels in plot")
			.append("input")
			.attr("type", "checkbox")
			.on("change", function (_) {
				if (this.checked) {
					latLegend.add()
				} else {
					latLegend.remove()
				}
			})

		// creation of the menu for FAMILY
		const menu = commands
			.append("div")
			.attr("id", `${idMenu}_commandsContent`)
			.selectAll("div.exorisTypes")
			.data(exorisTypes)
			.enter()
			.append("div")
			.attr("class", "exorisTypes")
			.attr("id", (d) => `menu_${d.family}`)

		// menu to show families curves (mono/multi-layers)
		menu
			.append("label")
			.append("input")
			.attr("type", "checkbox")
			.property("checked", true)
			.on("click", function (d) {
				const selection = d3.selectAll(`.exoris_${d.family}`)
				const childrens = d3.selectAll(`#menu_${d.family} > div input`)
				if (this.checked) {
					selection.style("display", "block")
					childrens.property("checked", true)
				} else {
					selection.style("display", "none")
					childrens.property("checked", false)
				}
				latLegend.remake()
			})

		// menu to develop families menu (mono/multi-layers)
		menu
			.selectAll("label")
			.append("text")
			.html((d) => d.name)
			.append("button")
			.attr("class", "exoris_menu_button")
			.text("-")
			.on("click", function (d) {
				let status = this.innerHTML
				const childrens = $(`#menu_${d.family} > div `)
				if (status === "+") {
					childrens.show(400)
					status = "-"
				} else {
					childrens.hide(400)
					status = "+"
				}
				this.innerHTML = status
			})

		// creation of the menu for TYPE
		for (let ii = 0; ii < exorisTypes.length; ++ii) {
			const submunu = container
				.select(`#menu_${exorisTypes[ii].family}`)
				.selectAll(".exorisSubTypes")
				.data(exorisTypes[ii].data)
				.enter()
				.append("div")
				.attr("class", "exorisSubTypes")
				.attr("id", (d) => `menu_${d.type}`)
				.style("display", "block")
				.on("mouseover", (d) => {
					d3.selectAll(`.exoris_${d.type}`).classed("blink_me", true)
				})
				.on("mouseout", (d) => {
					d3.selectAll(`.exoris_${d.type}`).classed("blink_me", false)
				})

			const label = submunu.append("label")

			// checkbox for TYPE
			label
				.append("input")
				.attr("type", "checkbox")
				.property("checked", true)
				.on("click", function (d) {
					const selection = d3.selectAll(`.exoris_${d.type}`)
					const childrens = d3.selectAll(`#menu_${d.type} > div input`)
					if (this.checked) {
						selection.style("display", "block")
						childrens.property("checked", true)
					} else {
						selection.style("display", "none")
						childrens.property("checked", false)
					}
					latLegend.remake()
				})

			// dropdown menu for TYPE
			label
				.append("text")
				.html((d) => d.symbol)
				.style("color", (d) => d.color)
				.append("button")
				.attr("class", "exoris_menu_button")
				.text("+")
				.on("click", function (d) {
					let status = this.innerHTML
					const childrens = $(`#menu_${d.type} > .exoris_submenu `)
					if (status === "+") {
						childrens.show(400)
						status = "-"
					} else {
						childrens.hide(400)
						status = "+"
					}
					this.innerHTML = status
				})
		}

		/* Menu containing graph */
		container.append("div").attr("id", `${idMenu}_graph`)
		getCurves(() => {
			commutators()
			createRadarChart()
		})
	}

	const commutators = (_) => {
		/*
   This function creates the submenus for any exoplanet type in
   which the temperature is added.
   These submenus have to be connected to their main menu.
   */
		const menu = d3.select(`#${idMenu}`)
		for (let ii = 0; ii < exodata.length; ii++) {
			const a = menu
				.select(`#menu_${exodata[ii].type}`)
				.append("div")
				.attr("class", "exoris_submenu")
				.style("display", "none")
				.on("mouseover", (_) => {
					d3.select(`#exoris_${exodata[ii].name}`).classed("blink_me", true)
					d3.event.stopPropagation()
				})
				.on("mouseout", (_) => {
					d3.select(`#exoris_${exodata[ii].name}`).classed("blink_me", false)
				})

			a.append("label")
				.append("input")
				.attr("type", "checkbox")
				.property("checked", true)
				.on("click", function () {
					const selection = paths.select(`#exoris_${exodata[ii].name}`)
					const display = this.checked ? "block" : "none"
					selection.style("display", display)
					latLegend.remake()
				})

			a.select("label").append("text").html(`${exodata[ii].temp}K`)
		}

		d3.select(`#${idMenu}`)
			.selectAll("div.exorisTypes")
			.append("hr")
			.attr("class", "exoris_submenu_hr")
			.style("display", "block")
	}

	const make = (mm, xscale, yscale) => {
		// active = true;
		if (!active) {
			return
		}
		if (!(mm.x.vn === "mass" && mm.y.vn === "radius") && !(mm.y.vn === "mass" && mm.x.vn === "radius")) {
			alert("ExoRIS curves are available only for variables" + " Planeary radius and Planeary mass.")
			active = false
		} else {
			varx = mm.x
			vary = mm.y
			paths = d3.select(idSVG).append("g").attr("id", "exoris_container")

			latLegend = new LatLegend()

			if (varx.vn === "mass") {
				massfactor = newfactor(varx)
				radiusfactor = newfactor(vary)
				line.x((d) => xscale(d.mass * massfactor))
				line.y((d) => yscale(d.radius * radiusfactor))
			} else {
				massfactor = newfactor(vary)
				radiusfactor = newfactor(varx)
				line.x((d) => xscale(d.radius * radiusfactor))
				line.y((d) => yscale(d.mass * massfactor))
			}

			// d3.selectAll('.path_,.aree_').remove();
			draw()
			$(`#${idMenu}`).show(400)
		}
		wswitch.checked = active
	}

	/* Area between curves */
	const draw = () => {
		// select curves of same type
		for (const ii in typeColor) {
			if (typeColor[ii].indices.length !== 2) {
				continue
			}
			const ids = typeColor[ii].indices
			const family = exodata[ids[0]].family
			const curves = {
				array: [].concat(exodata[ids[0]].array).concat(exodata[ids[1]].array.slice(0).reverse()),
			}

			paths
				.selectAll(`path#exoris_${ii}`)
				.data([curves])
				.enter()
				.append("path")
				.attr("id", `exoris_${ii}`)
				.attr("d", (_) => line(curves.array))
				.attr("class", `aree_ exoris_${ii} exoris_${family}`)
				.style("fill", typeColor[ii].color)
				.style("opacity", 0.5)
				.append("title")
				.attr("class", "tooltip")
				.text(() => {
					// adding title for any component
					let txt = ""
					exodata[ids[0]].composition.map((comp) => {
						txt += `${comp.key} : ${comp.value}\n`
					})
					return `${typeColor[ii].symbol}\n${txt}`
				})
		}

		// all simples curves
		const curves = paths
			.selectAll("g.cont_path")
			.data(exodata)
			.enter()
			.append("g")
			.attr("class", (d) => `cont_path exoris_${d.type} exoris_${d.family}`)

		curves
			.append("path")
			.attr("class", (d) => `path_ exoris_${d.type} exoris_${d.family}`)
			.attr("id", (d) => `exoris_${d.name}`)
			.attr("d", (d, _) => line(d.array))
			.style("fill", "none")
			.style("stroke", (d, _) => d.color)
			.style("stroke-width", "2px")
			.style("opacity", 0.8)
			.style("pointer-events", "all")
			.on("mouseover", function (d) {
				this.style.cursor = "crosshair"
				const pos = d3.mouse(this)
				d3.select(`#exotip_${d.name}`)
					.attr("transform", `translate(${pos[0]},${pos[1]})`)
					.transition()
					.duration(500)
					.style("opacity", 1)
			})
			.on("mouseout", (d) => {
				d3.select(`#exotip_${d.name}`).transition().duration(500).style("opacity", 0)
			})

		// tooltips
		const tooltip = curves
			.append("g")
			.attr("class", "tooltip")
			.attr("id", (d) => `exotip_${d.name}`)
			.style("opacity", 0)

		tooltip.append("rect").attr("width", "100").attr("height", "25")
		tooltip
			.append("text")
			.attr("x", 0)
			.attr("dy", "1.3em")
			.attr("dx", "1.5em")
			.text((d) => `${d.symbol} ${d.temp}K`)
			.style("fill", (d) => d.color)
			.style("opacity", 1)
	}

	const update = () => {
		if (!active) {
			return
		}
		// console.log('update')
		paths.selectAll(".path_,.aree_").attr("d", (d) => line(d.array))
		// paths.selectAll('title').text(function(d){return d.legend;})
	}

	const remove = () => {
		// console.log('remove')
		d3.selectAll(".path_,.aree_").remove()
		$(`#${idMenu}`).hide(400)
	}

	const LatLegend = function () {
		// Class for legend on plot
		const drag = d3.behavior
			.drag()
			.on("drag", function () {
				d3.select(this).attr("transform", `translate(${d3.event.x},${d3.event.y})`)
			})
			.on("dragstart", () => {
				d3.event.sourceEvent.stopPropagation() // silence other listeners
			})

		this.legend = paths.append("g").attr("id", "exoris_lat_legend").style("opacity", 1).call(drag)

		this.added = false

		this.add = function () {
			this.legend
				.selectAll("g.lat_legend")
				.data(exodata.filter((d) => d3.select(`#exoris_${d.name}`).style("display") !== "none"))
				.enter()
				.append("g")
				.attr("class", "lat_legend")
				.attr("transform", (d, i) => `translate(0,${i * 15 + 10})`)
				.append("text")
				.attr("x", -100)
				.attr("dy", "1.3em")
				.attr("dx", "1.5em")
				.attr("id", (d) => `lat_legend_${d.name}`)
				.text((d) => `${d.symbol} ${d.temp}K`)
				.style("fill", (d) => d.color)
				.transition()
				.duration(500)
				.attr("x", 0)

			this.added = true
			return this
		}

		this.remove = function () {
			this.legend.selectAll("g.lat_legend").remove()
			this.added = false
			return this
		}

		this.remake = function () {
			if (this.added) {
				this.remove().add()
			}
		}
	}

	const newfactor = (varaxis) => {
		let factor = 1.0
		switch (varaxis.unit) {
			case "Mjup":
				factor = 1.0
				break
			case "Mearth":
				factor = 317.83
				break
			case "km":
				factor = 71492.0
				break
			case "Rjup":
				factor = 1.0
				break
			case "Rearth":
				factor = 11.209
				break
		}
		return factor
	}

	const getCurves = (callback, callback2) => {
		let data

		$.ajax({
			url: uriJsonCurves,
			type: "GET",
			dataType: "json",
			success: (xhr) => {
				data = xhr.exodata
			},
			complete: (_jqXHR, _textStatus) => {
				exodata = []
				for (let ii = 0; ii < data.length; ii++) {
					const array = []
					for (let jj = 0; jj < data[ii].radius.length; jj++) {
						array.push({
							mass: data[ii].mass[jj],
							radius: data[ii].radius[jj],
						})
					}
					// assigning color
					let color = "black"
					if (Object.hasOwn(typeColor, data[ii].type)) {
						color = typeColor[data[ii].type].color
						typeColor[data[ii].type].indices.push(exodata.length)
					}

					exodata.push({
						name: data[ii].name.replace(/\./g, "_"),
						type: data[ii].type,
						symbol: typeColor[data[ii].type].symbol,
						family: data[ii].family,
						color,
						legend: data[ii].legend,
						temp: data[ii].Temp,
						composition: monoLayer.map((e) => ({ key: e.symbol, value: data[ii][e.type] })),
						array,
					})
				}

				if (callback) {
					callback()
				}
				if (callback2) {
					callback2()
				}
			},
			error: (jsXHR, textStatus, errorThrown) => {
				if (errorThrown === "NOT ACCEPTABLE") {
					console.log("syntax error in query", errorThrown)
					$("#error_window").show()
				} else {
					alert(errorThrown)
				}
			},
		})
	}

	const createRadarChart = () => {
		const graphCont = d3.select(`#${idMenu}_graph`)

		// add title
		graphCont.selectAll("h3").remove()
		graphCont.append("h3").attr("class", "variables_label").text("Planetary Internal Composition")

		graphCont.select(`#${idMenu}_graphContent`).remove()

		// add graph div
		graphCont.append("div").attr("id", `${idMenu}_graphContent`)
		const margin = { top: 50, right: 60, bottom: 60, left: 40 }
		const width = Math.max(window.innerWidth / 7, 250) - margin.left - margin.right
		const height = Math.min(width, screen.height - margin.top - margin.bottom - 20)

		const data = []
		const connections = []
		const color = []

		exodata.map((d) => {
			data.push(d.composition)
			connections.push(
				Object.assign(
					{
						class: `exoris_${d.type} exoris_${d.family}`,
						link: `.exoris_${d.type}`,
						linkedclass: "blink_me",
						symbol: d.symbol,
					},
					d.composition,
				),
			)
			color.push(d.color)
		})

		const radarChartOptions = {
			w: width,
			h: height,
			margin,
			maxValue: 1,
			levels: 5,
			roundStrokes: true,
			color: d3.scale.ordinal().range(color),
			connections,
		}
		// Call function to draw the Radar chart
		RadarChart(`#${idMenu}_graphContent`, data, radarChartOptions)
	}

	// exports
	exports.init = init
	exports.make = make
	exports.update = update
	exports.createRadarChart = createRadarChart
	exports.off = () => {
		active = false
		remove()
		wswitch.checked = false
	}
	exports.on = () => {
		active = true
	}
})(this.ExorisCurves)
