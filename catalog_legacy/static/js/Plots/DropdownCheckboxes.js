/*
Control the behavior of the dropdown checkbox used to configure the plots.

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* global $ */

this.DropdownCheckboxes = {}
;((exports) => {
	// init
	function init() {
		// $(".dropdown_checkboxes").hide();

		$(".filter_dropdown").click(function (e) {
			const multiselect = $(this).parent().find(".dropdown_checkboxes")

			// If you open a multiselect, you close the others
			if (!multiselect.is(":visible")) {
				$(".dropdown_checkboxes").hide()
				multiselect.show()
			} else {
				multiselect.toggle()
			}
			e.stopPropagation()
		})

		$(document).click(() => {
			$(".dropdown_checkboxes").hide()
		})

		$(".dropdown_checkboxes ul li").hover(
			function () {
				$(this).addClass("dropdown_checkboxes_hover")
			},
			function () {
				$(this).removeClass("dropdown_checkboxes_hover")
			},
		)

		$(".dropdown_checkboxes ul li input").click((e) => {
			e.stopPropagation()
		})

		$(".dropdown_checkboxes ul li, .dropdown_checkboxes ul li input").click(function (e) {
			const checkbox = $(this).find("input")
			checkbox.prop("checked", !checkbox.prop("checked"))
			changeFilter()
			document.getElementById("query_f").onchange()

			e.stopPropagation()
		})

		changeFilter()
	}

	function changeFilter() {
		$("input#query_f").prop("value", "")
		const filter = []

		$(".dropdown_checkboxes ul").each(function () {
			const filterPart = []
			$(this)
				.find("li input:checked")
				.each(function () {
					filterPart.push($(this).attr("value"))
				})

			if (filterPart.length > 0) {
				filter.push(filterPart.join(" OR "))
			}
		})

		let filterRes = ""
		if (filter === []) {
			filterRes = ""
		} else if (filter.length === 1) {
			filterRes = filter[0]
		} else {
			let i
			for (i = 0; i < filter.length; i++) {
				if (i === 0) {
					filterRes = `(${filter[i]})`
				} else {
					filterRes = `${filterRes} AND (${filter[i]})`
				}
			}
		}
		$("input#query_f").prop("value", filterRes)
	}

	exports.init = init
})(this.DropdownCheckboxes)
