/*
Polar plot drawing library.

Need some data parameters to be transmitted when calling the script in th html:

- data-datas: JSON string containing the initial data
- data-parameters: JSON string containing parameters for the graph itself
- data-uricatalog: uri to get data from the catalog
- data-uriplotrequestajax: uri to get data for the plot

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* eslint-env browser */
/* global d3, $, DiagramTools, TypesPlot, decodeRFC3986URI */

;(() => {
	/* some aliases */
	const DT = DiagramTools
	const TP = TypesPlot

	// Value passed from HTML via data attribute from the script element
	// See: https://stackoverflow.com/a/15069289
	// And: https://developer.mozilla.org/fr/docs/Apprendre/HTML/Comment/Utiliser_attributs_donnes
	const datas = JSON.parse(document.currentScript.dataset.datas)
	const parameters = JSON.parse(document.currentScript.dataset.parameters)
	const uriCatalog = document.currentScript.dataset.uricatalog
	const uriPolarPlotAjax = document.currentScript.dataset.uripolarplotajax

	// Usefull constants
	const EARTH_IRRADIATION = 340.0
	const TIMEOUT_DURATION = 200 // ms

	// plot parameters
	const MARGIN = {
		top: 40,
		right: 0,
		bottom: 40,
		left: 40,
	}

	let noDistState = true
	let logScaleState = null
	const planetTypeState = {
		"mini-Earth": true,
		Earth: true,
		"Super-Earth": true,
		"mini-Neptune": true,
		Neptune: true,
		Jupiter: true,
	}

	const HALF_OPACITY = 0.5
	const LEGEND_HEIGHT = 30
	const rLimit = 5 // log10(upper bound of star_distance)
	const whereplot = "plothere"
	const plotId = "graphicID"
	const data = []
	const specialData = []
	const planetTypes = ["mini-Earth", "Earth", "Super-Earth", "mini-Neptune", "Neptune", "Jupiter"]
	const sizeTypes = [5, 10, 15, 20.0, 25, 30]
	const radiiTypes = [0, 0.5, 1.5, 2.0, 4.0, 6.0, Infinity]
	const massesTypes = [0, 0.1, 2.0, 10.0, 15.0, 30.0, Infinity]
	const listPlanet = ["TRAPPIST-1 e", "TRAPPIST-1 f", "TRAPPIST-1 g", "LHS 1140 b", "Proxima Cen b", "Ross 128 b"]
	const defaultScale = {
		mass: "log",
		period: "log",
		radius: "log",
		axis: "log",
		stars__teff: "log",
		stars__mass: "log",
		irradiation: "log",
	}
	const varListGroup = {
		Planet: [],
		Star: [],
	}
	const varList = {}

	for (param of parameters) {
		param.factor = 1
		param.scale = defaultScale[param.id] === undefined ? "linear" : "log"
		varList[param.id] = param
		varListGroup[param.related_model].push(param)
	}

	/* associate default variable to mm */
	const mm = new TP.WorkVariable({ x: "irradiation" })
	mm.x.pushvar(varList)

	const format = d3.format(",.4g")

	/* Get continuous color scale for the Rainbow */
	const coloursRainbow = [
		"#2c7bb6",
		"#00a6ca",
		"#00ccbc",
		"#90eb9d",
		"#ffff8c",
		"#f9d057",
		"#f29e2e",
		"#e76818",
		"#d7191c",
	]
	const colourRangeRainbow = d3.range(0, 1, 1.0 / (coloursRainbow.length - 1))
	colourRangeRainbow.push(1)

	// Create color gradient
	const colorScaleRainbow = d3.scale
		.linear()
		.domain(colourRangeRainbow)
		.range(coloursRainbow)
		.interpolate(d3.interpolateHcl)

	function setScalehtml() {
		mm.x.scalehtml = mm.x.desc
		if (mm.x.unit !== "") {
			mm.x.scalehtml += ` (${mm.x.unit})`
		}
	}

	/* Function to select only interessant informations and to to inital manipulation */
	function selectData(dataInArray) {
		setScalehtml()
		mm.x.pushvar(varList)

		const dataIn = DT.UnpackData(dataInArray)
		// Clear the data
		data.length = 0
		for (const curr of dataIn) {
			// position of the planet
			const angle = (curr.star_ra * Math.PI) / 180
			let logdist
			let classed = ""

			if ([0.0, "", null, Infinity, -Infinity, undefined].includes(curr.star_distance)) {
				logdist = rLimit
				classed += "no_distance "
			} else {
				logdist = Math.log10(curr.star_distance)
			}

			// plotVar calcultaion
			let plotVar = curr[mm.x.vn]
			let origVar = plotVar

			if (mm.x.vn === "irradiation") {
				plotVar = plotVar / EARTH_IRRADIATION
				origVar = plotVar
			}

			if (plotVar !== "") {
				if (mm.x.scale === "log") {
					plotVar = plotVar > 0 ? Math.log10(plotVar) : ""
				}
			}

			let size = 1
			const EARTH_RADIUS_PER_PX = 0.2
			const JUPITER_RADIUS = 11.209
			const JUPITER_MASS = 317.83
			const r = curr.radius * JUPITER_RADIUS // now r is real
			const m = curr.mass * JUPITER_MASS // now m is real

			if (r !== 0) {
				for (let ii = 0; ii < planetTypes.length; ii++) {
					if (r >= radiiTypes[ii] && r < radiiTypes[ii + 1]) {
						size = sizeTypes[ii] * EARTH_RADIUS_PER_PX
						classed += planetTypes[ii]
						break
					}
				}
			} else if (m !== 0) {
				for (let ii = 0; ii < planetTypes.length; ii++) {
					if (m >= massesTypes[ii] && m < massesTypes[ii + 1]) {
						size = sizeTypes[ii] * EARTH_RADIUS_PER_PX
						classed += planetTypes[ii]
						break
					}
				}
			}

			const row = {
				name: curr.name,
				url: curr.url_keyword,
				mass: curr.mass,
				radius: curr.radius,
				star_distance: curr.star_distance, // in parsec
				cx: logdist * Math.cos(angle),
				cy: -logdist * Math.sin(angle),
				star_ra: curr.star_ra,
				plot_var: plotVar,
				orig_var: origVar,
				size,
				classed,
			}
			data.push(row)

			if (listPlanet.indexOf(curr.name) > 0) {
				specialData.push(row)
			}
		}
	}

	/* Main Class Polar Plot */
	class PolarPlot {
		constructor() {
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = document.querySelector("#diagram_image_id").offsetHeight
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom
			this.radius = Math.min(this.width, this.height) / 2 - LEGEND_HEIGHT
			this.scale = "linear"
		}

		redraw() {
			this.wTot = document.querySelector("#diagram_image_id").offsetWidth
			this.hTot = document.querySelector("#diagram_image_id").offsetHeight
			this.width = this.wTot - MARGIN.left - MARGIN.right
			this.height = this.hTot - MARGIN.top - MARGIN.bottom
			this.radius = Math.min(this.width, this.height) / 2 - LEGEND_HEIGHT

			this.clean()
			this.plotData()
		}

		clean() {
			d3.select(`#${whereplot} > svg`).remove()
		}

		/* Create the Rainbow color gradient */
		createRainbowGradient(svg, extentz) {
			const defs = svg.append("defs")

			// Calculate the gradient
			defs
				.append("linearGradient")
				.attr("id", "gradient-rainbow-colors")
				.attr("x1", "0%")
				.attr("y1", "0%")
				.attr("x2", "100%")
				.attr("y2", "0%")
				.selectAll("stop")
				.data(coloursRainbow)
				.enter()
				.append("stop")
				.attr("offset", (_d, i) => i / (coloursRainbow.length - 1))
				.attr("stop-color", (d) => d)

			// Draw Heatmap
			const legendWidth = this.width * 0.6
			const LEGEND_GRADIANT_BAR_HEIGHT = 10
			const ANGLE_TEXT_SIZE = 20
			const MARGIN_TOP_TO_TITLE = 10

			// Color Legend container
			const legendsvg = svg
				.append("g")
				.attr("class", "legendWrapper")
				.attr("transform", `translate(${0},${this.radius + LEGEND_HEIGHT + ANGLE_TEXT_SIZE})`)

			// Draw the Rectangle
			legendsvg
				.append("rect")
				.attr("class", "legendRect")
				.attr("x", -legendWidth / 2)
				.attr("y", MARGIN_TOP_TO_TITLE)
				.attr("width", legendWidth)
				.attr("height", LEGEND_GRADIANT_BAR_HEIGHT)
				.attr("fill", "url(#gradient-rainbow-colors)")

			// Append title
			legendsvg
				.append("text")
				.attr("class", "legendTitle")
				.attr("x", 0)
				.attr("y", 0)
				.attr("fill", "black")
				.attr("stroke", "none")
				.text(mm.x.scalehtml)
				.style("text-anchor", "middle")

			const extb = [Math.floor(extentz[0]), Math.ceil(extentz[1])]

			// Set scale for x-axis
			const zScale = d3.scale.linear().range([0, legendWidth]).domain(extb)

			// Define z-axis
			const zAxis = d3.svg.axis().orient("bottom").scale(zScale)

			// Set up Z axis
			const graduata = legendsvg
				.append("g")
				.attr("class", "axis") // Assign "axis" class
				.attr("transform", `translate(${-legendWidth / 2},${MARGIN_TOP_TO_TITLE + LEGEND_GRADIANT_BAR_HEIGHT})`)

			const nbLegendTicks = 5
			if (mm.x.scale === "log") {
				const getNbTicks = (min, max) => {
					const calcNbTicks =
						Math.abs(max) > Math.abs(min) ? Math.abs(max) - Math.abs(min) : Math.abs(max) + Math.abs(min)
					return calcNbTicks >= nbLegendTicks ? calcNbTicks : nbLegendTicks
				}

				const scaleMax = extb[1]
				const scaleMin = extb[0]
				const extColorDomain = []
				const calcNbLegendTicks = getNbTicks(scaleMin, scaleMax)

				for (let ii = 0; ii < calcNbLegendTicks; ii++) {
					extColorDomain.push(
						DT.formatTickPolar(Math.round(scaleMin + (ii / calcNbLegendTicks) * (scaleMax - scaleMin))),
					)
				}

				graduata.call(zAxis.ticks(calcNbLegendTicks).tickFormat((d, i) => extColorDomain[i]))
			} else {
				graduata.call(zAxis.ticks(nbLegendTicks))
			}

			if (mm.x.vn === "irradiation") {
				$(".irradiation_text").show()
			} else {
				$(".irradiation_text").hide()
			}
		}

		plotData() {
			this.updateLogFromCheckboxState()

			const r = d3.scale.linear().domain([0, rLimit]).range([0, this.radius])

			const svg = d3
				.select(`#${whereplot}`)
				.append("svg")
				.attr("width", this.wTot)
				.attr("height", this.hTot)
				.append("g")
				.attr("id", plotId)
				/*
          F*** it, by default the fill color is black.
          The circle and legend are on the same image so if we put black,
          the graph is black,
          if we put transparent the legend and the circle limit are invisible.
          3h to fiind this.
          We need to put fill transparent here and add fill black and stoke none
          for legend
          and we need to put stroke black with 0.5 opacity to see the line
          of the circle.

          F**** it, None is better because we can click on the point, with transparent we can't click --' .
        */
				.attr("fill", "none")
				.attr("stroke", "black")
				.attr("stroke-opacity", HALF_OPACITY)
				.attr("transform", `translate(${MARGIN.left + this.radius},${this.radius + MARGIN.top})`)

			// Needed to map the values of the dataset to the color scale
			const extentz = d3.extent(data.map((d) => d.plot_var).filter((d) => d !== ""))

			// Needed to map the values of the dataset to the color scale
			const colorInterpolateRainbow = d3.scale.linear().domain(extentz).range([0, 1])

			this.createRainbowGradient(svg, extentz)

			const sgpts = svg.append("g").attr("id", "special_pts")

			sgpts
				.selectAll(".special_pts")
				.data(specialData)
				.enter()
				.append("circle")
				.attr("class", (d) => `polar_pt ${d.classed}`)
				.attr("cx", (d) => r(d.cx))
				.attr("cy", (d) => r(d.cy))
				.attr("r", (d) => d.size + 2)
				.attr("fill", "black")
				.attr("fill-opacity", HALF_OPACITY)
				.attr("stroke", "black")
				.on("click", (d, _i) => {
					window.open(uriCatalog + d.url)
				})
				.append("title")
				.html(
					(d) => `${decodeRFC3986URI(d.name)}
          distance =${format(d.star_distance)} pc
          α₂₀₀₀=${format(d.star_ra)}°
          ${mm.x.vn}=${format(d.orig_var)} ${mm.x.unit}`,
				)

			// group of points
			const gpts = svg.append("g").attr("id", "polar_pts")

			gpts
				.selectAll(".polar_pt")
				.data(data)
				.enter()
				.append("circle")
				.attr("class", (d) => `polar_pt ${d.classed}`)
				.attr("cx", (d) => r(d.cx))
				.attr("cy", (d) => r(d.cy))
				.attr("r", (d) => d.size)
				.attr("fill", (d) => (d.plot_var === "" ? "grey" : colorScaleRainbow(colorInterpolateRainbow(d.plot_var))))
				.attr("stroke", "black")
				.attr("stroke-width", 0.2)
				.on("click", (d, _i) => {
					window.open(uriCatalog + d.url)
				})
				.append("title")
				.html(
					(d) => `${decodeRFC3986URI(d.name)}
          distance =${format(d.star_distance)} pc
          α₂₀₀₀=${format(d.star_ra)}°
          ${mm.x.vn}=${format(d.orig_var)} ${mm.x.unit}`,
				)
			// axes
			const gr = svg
				.append("g")
				.attr("class", "r axis")
				.selectAll("g")
				// TODO: Ca correspond a quoi ces chiffres ?
				.data(r.ticks(5).slice(1))
				.enter()
				.append("g")

			gr.append("circle").attr("r", r)

			const LEGEND_BOT_MARGIN_TO_CIRCLE = 7
			const ANGLE_BOT_MARGIN_TO_CIRCLE = 10
			const AXIS_ANGLE_STEP = 30

			gr.append("text")
				.attr("y", (d) => -r(d) - LEGEND_BOT_MARGIN_TO_CIRCLE)
				.attr("transform", "rotate(15)")
				.style("text-anchor", "middle")
				.text((d) => (d < rLimit ? `${10 ** d} pc` : "Distance not defined"))

			const ga = svg
				.append("g")
				.attr("class", "a axis")
				.selectAll("g")
				.data(d3.range(0, 360, AXIS_ANGLE_STEP))
				.enter()
				.append("g")
				.attr("transform", (d) => `rotate(${-d})`)

			ga.append("line").attr("x2", this.radius)

			ga.append("text")
				.attr("x", this.radius + ANGLE_BOT_MARGIN_TO_CIRCLE)
				.attr("dy", ".35em")
				.style("text-anchor", (d) => (d < 270 && d > 90 ? "end" : null))
				.attr("transform", (d) =>
					d < 270 && d > 90 ? `rotate(180 ${this.radius + ANGLE_BOT_MARGIN_TO_CIRCLE},0)` : null,
				)
				.text((d) => `${d}°`)

			this.updateSelectionFromCheckboxState()
		}

		updateLogFromCheckboxState() {
			// Log
			if (logScaleState !== null) {
				const locScale = logScaleState ? "log" : "linear"
				mm.x.scale = locScale
			}
			// Else logScaleState is null we use the mm.x.scale by the data
		}

		updateSelectionFromCheckboxState() {
			// No distance
			d3.selectAll(".polar_pt.no_distance").style("display", noDistState ? "block" : "none")

			// Planet Type
			for (const pTypeState in planetTypeState) {
				d3.selectAll(`.polar_pt.${pTypeState}`).style("display", planetTypeState[`${pTypeState}`] ? "block" : "none")
			}
		}

		rescaleData() {
			if (mm.x.scale === "log") {
				for (const curr of data) {
					const currOrigVar = curr.orig_var
					curr.plot_var = currOrigVar > 0 ? Math.log10(currOrigVar) : ""
				}
			} else {
				for (const curr of data) {
					curr.plot_var = curr.orig_var
				}
			}
		}

		AddSelector(where) {
			const checkContainer = d3.select(`#${where}`).append("ul").attr("class", "pp_checkers")

			const divcheck = checkContainer.selectAll("li").data(planetTypes).enter().append("li")

			divcheck
				.append("input")
				.attr("id", (d) => `planet_type_${d}`)
				.attr("type", "checkbox")
				.attr("checked", (d) => planetTypeState[`${d}`])
				.on("change", (d) => {
					planetTypeState[`${d}`] = d3.event.currentTarget.checked
					this.updateSelectionFromCheckboxState()
				})

			divcheck
				.append("label")
				.text((d) => d)
				.style("display", "inline-block")

			const divcheck2 = checkContainer
				.append("li")
				/* To have this checkbox at right of the menu, uncomment the following line
           And comment the next following line */
				// .attr('class', 'type_checkbox_right')
				.attr("class", "")

			divcheck2
				.append("input")
				.attr("id", "no_distance")
				.attr("type", "checkbox")
				.attr("checked", noDistState)
				.on("change", () => {
					noDistState = d3.event.currentTarget.checked
					this.updateSelectionFromCheckboxState()
				})

			divcheck2.append("label").text("No distance").style("display", "inline-block")

			const divchecklog = checkContainer
				.append("li")
				/* To have this checkbox at right of the menu, uncomment the following line
           And comment the next following line */
				// .attr('class', 'type_checkbox_right')
				.attr("class", "")

			divchecklog
				.append("input")
				.attr("id", "logscale")
				.attr("type", "checkbox")
				.property("checked", mm.x.scale === "log")
				.on("change", () => {
					logScaleState = d3.event.currentTarget.checked

					this.updateLogFromCheckboxState()

					this.clean()
					this.rescaleData()
					this.plotData()
				})

			divchecklog.append("label").text("log scale").style("display", "inline-block")
		}

		create(dataIn) {
			selectData(dataIn)
			this.clean()
			this.plotData()
			this.AddSelector("diagram_form_id")
			AddVarSelector("x", this)
		}

		update(dataIn) {
			selectData(dataIn)
			this.clean()
			this.plotData()
		}

		getValues() {
			const query = `x=${mm.x.vn}`

			// get value from query input
			$.ajax({
				url: uriPolarPlotAjax,
				type: "GET",
				data: query,
				dataType: "json",
				success: (xhr) => {
					$("#error_window").hide()
					// store the total number of planets
					// -1 because the first member  are the headers
					// required number of planets
					this.update(xhr)
				},
				complete: () => {},
				error: (_jsXHR, _textStatus, errorThrown) => {
					if (errorThrown === "NOT ACCEPTABLE") {
						console.log("syntax error in query", errorThrown)
						$("#error_window").show()
					} else {
						console.log("other err", errorThrown)
					}
				},
			})
		}
	}

	function AddVarSelector(axes, element) {
		const nino = d3.select(`#menu_choise_${axes}`).on("change", change)

		nino.selectAll("optgroup").remove()

		nino
			.selectAll("optgroup")
			.data(Object.keys(varListGroup))
			.enter()
			.append("optgroup")
			.attr("label", (d) => `${d} parameters`)
			.attr("id", (d) => `groupopt_${d}`)

		for (const ii in varListGroup) {
			nino
				.select(`optgroup#groupopt_${ii}`)
				.selectAll("option")
				.data(varListGroup[ii])
				.enter()
				.append("option")
				.text((d) => d.desc)
				.attr("value", (d) => d.id)
				.attr("title", (d) => d.title)
		}

		// set default on dropDown
		nino.property("value", mm[axes].vn)

		// set terms of transition that will take place
		function change() {
			const locvar = this.options[this.selectedIndex].__data__
			mm[axes].pushvar(varList, locvar.id)
			d3.select("input#logscale").property("checked", mm.x.scale === "log")

			element.getValues()
		}
	}

	// Let's create everything
	const polar = new PolarPlot()
	polar.create(datas)

	window.addEventListener(
		"resize",
		() => {
			// Announce the resize oof the window
			setTimeout(() => {
				polar.redraw()
			}, TIMEOUT_DURATION)
		},
		false,
	)
})()
