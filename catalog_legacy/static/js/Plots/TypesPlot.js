/*
Some utilities used to plot planetary data.

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* eslint-env browser */
/* global $, d3, Cookies */

this.TypesPlot = {}
;((exports) => {
	/*
	Container for the work variables
   */
	function WorkVariable(object) {
		for (const ii in object) {
			if (["x", "y", "z", "p"].includes(ii)) {
				this[ii] = new AxisVariable(ii)
				this[ii].init(object[ii])
			}
		}
	}

	/*
	Axis Variable Type contains the complete description of a work
	variable for a axis
   */
	function AxisVariable(axis) {
		this.axis = axis
		this.scale = "linear"
	}

	AxisVariable.prototype = {
		init: function (name) {
			this.vn = name
			this.min = -Infinity
			this.max = Infinity
			this.unit = ""
			this.unit0 = "" // default unit
			$(`#input_min_${this.axis}`).val("")
			$(`#input_max_${this.axis}`).val("")
		},
		pushvar: function (varList, name) {
			// arg name is optional
			let newName = name
			if (name) {
				this.init(newName)
			} else {
				newName = this.vn
			}
			for (const ii in varList[newName]) {
				this[ii] = varList[newName][ii]
				if (ii === "unit") {
					switch (this.unit) {
						case "d":
							this.unit = "day"
							break
						case "y":
						case "yr":
							this.unit = "year"
							break
					}
				}
				this.unit0 = this.unit
			}
		},
		setscale: function (scale) {
			if (scale === "log" || scale === "linear") {
				this.scale = scale
			}
		},

		rescale: function (factor) {
			if (this.min !== -Infinity) {
				this.min *= factor
				$(`#input_min_${this.axis}`).val(this.min)
			}
			if (this.max !== Infinity) {
				this.max *= factor
				$(`#input_max_${this.axis}`).val(this.max)
			}
		},
		getMin: function () {
			return this.min === -Infinity ? "" : this.min
		},
		getMax: function (_) {
			return this.max === Infinity ? "" : this.max
		},

		testIn: function (val) {
			return val >= this.min && val <= this.max
		},

		getQuery: function () {
			let str = ""

			if (["Frequency", "Probability"].includes(this.vn)) {
				return str
			}

			const unitstr = this.unit !== "" ? `:${this.unit}` : ""

			if (["x", "y"].includes(this.axis)) {
				str += ` AND ${this.vn}${unitstr}!=null`
			}

			if (this.min !== -Infinity) {
				str += ` AND ${this.vn}${unitstr} >= ${this.min.noExponents()}`
			}

			if (this.max !== Infinity) {
				str += ` AND ${this.vn}${unitstr} <= ${this.max.noExponents()}`
			}

			return str
		},

		axisLabelUnit: function () {
			// return space between is needed by canvjs otherwise error
			// saving the image
			return this.unit === "" ? " " : ` (${this.unit})`
		},

		AddMinMaxInput: function (where, callback) {
			const block = d3.select(`#${where}`)
			const that = this
			if (that.present) {
				return
			}
			// console.log(block.select("#minmax"+that.axis))
			block
				.append("input")
				.attr("type", "text")
				.attr("class", "input_short")
				.attr("id", `input_min_${that.axis}`)
				.attr("placeholder", "min")
				.attr("title", "select the minimum value")
				.on("change", function () {
					const inval = parseFloat(this.value)

					if (Number.isNaN(inval) && this.value !== "") {
						alert("Error : only numeric values are permitted in this field.")
						this.value = that.getMin()
					} else {
						that.min = this.value === "" ? -Infinity : inval
						callback()
					}
				})

			block
				.append("input")
				.attr("type", "text")
				.attr("class", "input_short_r")
				.attr("id", `input_max_${that.axis}`)
				.attr("placeholder", "max")
				.attr("title", "select the maximum value")
				.on("change", function () {
					const inval = parseFloat(this.value)

					if (Number.isNaN(inval) && this.value !== "") {
						alert("Error : only numeric values are permitted in this field.")
						this.value = that.getMax()
					} else {
						that.max = this.value === "" ? Infinity : inval
						callback()
					}
				})

			this.present = true
		},
	}

	/*
   Open the link containing the table of the planets in the diagram
   */
	function openPlanetsLink(base, varobject) {
		const csrftoken = Cookies.get("csrftoken")
		let str = `(${document.getElementById("query_f").value})`

		for (const nn in varobject) {
			if (typeof varobject[nn] === "function") {
				continue
			}
			str += varobject[nn].getQuery()
		}

		if (str.substring(0, 5) === " AND ") {
			str = str.replace(" AND ", "")
		}

		const form = document.createElement("form")
		form.action = base
		form.method = "POST"
		form.target = "newwin"

		const inputcsrf = document.createElement("input")
		inputcsrf.type = "hidden"
		inputcsrf.name = "csrfmiddlewaretoken"
		inputcsrf.value = csrftoken
		form.appendChild(inputcsrf)

		const input = document.createElement("textarea")
		input.name = "query_f"
		input.value = str
		form.style.display = "none"
		form.appendChild(input)
		document.body.appendChild(form)
		form.submit()
	}

	/*
	 * Switch the units and conversion factor for an object containing
	 * "unit" and "factor" like varList. Return True if any change is
	 *  done.
	 */
	function unitSwitcher(varobject) {
		let factor = 1.0
		switch (varobject.unit) {
			case "Mjup":
				varobject.unit = "Mearth"
				factor = 317.83

				break
			case "Mearth":
				varobject.unit = "Mjup"
				factor = 1 / 317.83

				break
			case "Rjup":
				varobject.unit = "Rearth"
				factor = 11.21

				break
			case "Rearth":
				varobject.unit = "km"
				factor = 6371

				break
			case "km":
				varobject.unit = "Rjup"
				factor = 1 / 6371 / 11.21

				break
			case "day":
				varobject.unit = "year"
				factor = 1 / 365.242199

				break
			case "year":
				varobject.unit = "day"
				factor = 365.242199

				break
		}
		return factor
	}

	/*
	 * Transforms factor to take into account the current unit and
	 * the default one.
	 */
	function unitTransform(varobject) {
		let factor = 1
		if (varobject.unit !== varobject.unit0) {
			switch (
				varobject.unit // current unit
			) {
				case "Mjup": // default mass
					factor = 1
					break
				case "Mearth":
					factor = 317.83
					break
				case "Rjup": // default radius
					factor = 1.0
					break
				case "Rearth":
					factor = 11.209
					break
				case "km":
					factor = 71492
					break
				case "year": // default date
					factor = 1 / 365.242199
					break
				case "day":
					factor = 365.242199
					break
			}
		}

		return factor
	}

	function refactorField(arrayOfObjects, field, callback) {
		const factor = unitSwitcher(field)
		if (factor !== 1) {
			for (let ii = 0; ii < arrayOfObjects.length; ii++) {
				arrayOfObjects[ii][field.axis] *= factor
				arrayOfObjects[ii][`${field.axis}_emin`] *= factor
				arrayOfObjects[ii][`${field.axis}_emax`] *= factor
			}

			field.rescale(factor)

			if (callback) {
				callback()
			}
		}
		// else{    console.log('No refactorField')}
	}

	function refactorDataIfNotDefault(arrayOfObjects, field) {
		const factor = unitTransform(field)
		if (factor === 1) return
		for (let ii = 0; ii < arrayOfObjects.length; ii++) {
			arrayOfObjects[ii][field.axis] *= factor
			arrayOfObjects[ii][`${field.axis}_emin`] *= factor
			arrayOfObjects[ii][`${field.axis}_emax`] *= factor
		}
	}

	// Create noExponents function for string numbers in query
	// TODO Find another way to to this: String prototype is in fact read only!
	Number.prototype.noExponents = function () {
		const data = String(this).split(/[eE]/)
		if (data.length === 1) {
			return data[0]
		}

		let z = ""
		const sign = this < 0 ? "-" : ""
		const str = data[0].replace(".", "")
		let mag = Number(data[1]) + 1

		if (mag < 0) {
			z = `${sign}0.`

			while (mag++) {
				z += "0"
			}

			return z + str.replace(/^-/, "")
		}

		mag -= str.length

		while (mag--) {
			z += "0"
		}

		return str + z
	}

	exports.WorkVariable = WorkVariable
	exports.refactorField = refactorField
	exports.refactorDataIfNotDefault = refactorDataIfNotDefault
	exports.openPlanetsLink = openPlanetsLink
})(this.TypesPlot)
