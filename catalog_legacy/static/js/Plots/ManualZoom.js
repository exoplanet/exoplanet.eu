/*
Singleton for Manual zoom and man tools

Original Author: Marco Mancini <mmancini@mycompany.com>
Maintainer: Pierre-Yves Martin
*/

/* ESLint config: */
/* global d3 */

this.ManualZoom = {}
;((exports) => {
	const defaultColor = "#E42E0E" // default color
	let zoomtrans = [0, 0]
	let zoomscale = 1
	let center = [0, 0]
	const position = [0, 0]

	// activity
	let panIsVisible = false
	let trackIsActive = true

	// pointers for functions
	let zoomFunc
	let callbackmove

	function trackOn() {
		trackIsActive = true
	}

	function trackOff() {
		trackIsActive = false
	}

	function setCenter(val) {
		center = val
		return this
	}

	function reset() {
		zoomtrans = [0, 0]
		zoomscale = 1
		Move()
		return this
	}

	/* Function to zoom manually */
	function Move() {
		if (!trackIsActive) {
			return
		}
		zoomFunc.translate(zoomtrans.map((d, i) => (center[i] / 2) * (1 - zoomscale) + d * zoomscale)).scale(zoomscale)

		/* move the graphic */
		callbackmove()
		//    var translate = zoomtrans.map(
		//    function(d,i){
		//      return (center[i]/2)*(1-zoomscale)+ d*zoomscale;}
		//  );
		//   zoomFunc.translate(translate).scale(zoomscale);
		/* move the graphic */
		//   callbackmove({scale : zoomscale, translate: translate});
	}

	/* Get back translation from PanZoom */
	function getTranslation() {
		return zoomtrans.map((d, i) => (center[i] / 2) * (1 - zoomscale) + d * zoomscale)
	}

	/* Get back scale from PanZoom */
	function getScale() {
		return zoomscale
	}

	/* This function has to be inserted in the zoomed function in the
	 *  main program */
	function trackMouseMouv() {
		if (!trackIsActive) {
			return
		}
		/* Transform the mouse movement in stored variables for buttons */
		const event = d3.event
		const bo = event === null ? undefined : event.sourceEvent
		if (bo !== undefined) {
			if (bo.type === "mousemove" || bo.type === "wheel") {
				zoomscale = event.scale
				zoomtrans = event.translate.map((d, i) => (1 / zoomscale) * (d - (center[i] / 2) * (1 - zoomscale)))
			}
		}
	}

	/* Connections of PanZoom to zoom,zoomed */
	function Connect(zoomIn, callbackmoveIn) {
		zoomFunc = zoomIn
		callbackmove = callbackmoveIn
		return this
	}

	function connectButton(whereBtn, wherePan, status0) {
		const buttonMZ = document.getElementById(whereBtn)
		panIsVisible = status0 || false
		buttonMZ.parentNode.title = "Add a console to the plot for governing zoom."
		buttonMZ.checked = panIsVisible
		buttonMZ.onclick = () => {
			if (!panIsVisible) {
				CreateMouse(wherePan, {
					position: position,
				})
			} else {
				d3.select("#ManualZoomBox").remove()
			}
			panIsVisible = !panIsVisible
		}
	}

	/* Create command console */
	function CreateMouse(MouseWhereId) {
		//    for(var ii in options){this[ii]=options[ii];}
		/* define start, speedup, and step sizes */
		let start = 1000
		const speedup = 10
		const dx = 2
		const dy = 2
		const drho = 0.01
		const ddx = 0.2
		const ddy = 0.2
		const width = 120
		const height = 100
		const unit = Math.min(width / 6, height / 3)
		/* Data for button description */
		const data = [
			{
				name: "XM",
				points: [-ddx, -0.5 + ddy, -0.5, 0, -ddx, 0.5 - ddy],
				Ds: [2 * unit, 2 * unit],
				title: "Pan left",
			},
			{
				name: "XP",
				points: [+ddx, 0.5 - ddy, 0.5, 0, ddx, -0.5 + ddy],
				Ds: [3 * unit, 2 * unit],
				title: "Pan right",
			},
			{
				name: "YP",
				points: [0.5 - ddx, +0.5 - ddy, 0, 0, -0.5 + ddx, +0.5 - ddy],
				Ds: [2.5 * unit, 1.0 * unit],
				title: "Pan up",
			},
			{
				name: "YM",
				points: [0.5 - ddx, -0.5 + ddy, 0, 0, -0.5 + ddx, -0.5 + ddy],
				Ds: [2.5 * unit, 3 * unit],
				title: "Pan down",
			},
			{
				name: "zoomP",
				points: [0.5 - ddx, +0.5 - ddy, 0, 0, -0.5 + ddx, +0.5 - ddy],
				Ds: [5.1 * unit, 1.45 * unit],
				title: "Zoom in",
			},
			{
				name: "zoomM",
				points: [0.5 - ddx, -0.5 + ddy, 0, 0, -0.5 + ddx, -0.5 + ddy],
				Ds: [5.1 * unit, 2.55 * unit],
				title: "Zoom out",
			},
		]

		/* Creation of SGV background containing buttons */
		const ZoomBox = d3
			.select(`#${MouseWhereId}`)
			.append("svg:svg")
			.append("g")
			.attr("transform", `translate(${position[0]},${position[1]})`)
			.attr("id", "ManualZoomBox")

		const buttons = ZoomBox.selectAll(".ManualZoom.button")
			.data(data)
			.enter()
			.append("g")
			.style("cursor", "pointer")
			.attr("class", "ManualZoom button")
			.attr("stroke", defaultColor)
			.attr("transform", (d) => `translate(${d.Ds[0]},${d.Ds[1]})`)

		/* Creation of buttons */
		buttons
			.append("polyline")
			.attr("class", "ManualZoom")
			.attr("points", (d) => `${d.points.map((e) => e * unit)}`)
			.append("title")
			.text((e) => e.title)

		/* Animation of buttons */
		// work definitions
		let t

		let action = () => {}
		const repeat = () => {
			action()
			t = window.setTimeout(repeat, start)
			start = start / speedup
			Move()
		}
		buttons
			.on("mouseover", (d) => {
				if (!trackIsActive) {
					return
				}
				switch (d.name) {
					case "XP":
						action = () => {
							zoomtrans[0] += dx
						}
						repeat()
						break
					case "XM":
						action = () => {
							zoomtrans[0] -= dx
						}
						repeat()
						break
					case "YP":
						action = () => {
							zoomtrans[1] -= dy
						}
						repeat()
						break
					case "YM":
						action = () => {
							zoomtrans[1] += dy
						}
						repeat()
						break
					case "zoomP":
						action = () => {
							if (zoomscale <= 20) {
								zoomscale += drho
							}
						}
						repeat()
						break
					case "zoomM":
						action = () => {
							if (zoomscale >= 0.5) {
								zoomscale -= drho
							}
						}
						repeat()
						break
				}
			})
			.on("mouseup", (_) => {
				window.clearTimeout(t)
				action = () => {}
			})
			.on("mouseout", (_) => {
				window.clearTimeout(t)
				action = () => {}
			})
	}

	exports.setCenter = setCenter
	exports.trackOn = trackOn
	exports.trackOff = trackOff
	exports.trackMouseMouv = trackMouseMouv
	exports.reset = reset
	exports.Connect = Connect
	exports.connectButton = connectButton
	exports.getTranslation = getTranslation
	exports.getScale = getScale
})(this.ManualZoom)
